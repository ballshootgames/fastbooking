window.$ = window.jQuery = require('jquery');

require('../js/jquery-migrate.min');

require('bootstrap-less');

require('../js/jquery.blockUI.min');

require('../js/jquery.magnific-popup.min');

require('../js/matchHeight');

require('../js/fotorama');

require('../js/ion.rangeSlider.min');

require('../js/moment.min');

require('../js/nicescroll.min');

require('../js/sweetalert2.min');

require('../js/select2.full.min');

require('../js/infobox');

require('../js/custom');

require('../js/flickity.pkgd.min');

require('../js/owl.carousel.min');

require('../js/mb.YTPlayer.min');

require('../js/wp-embed.min');

require('../js/js_composer_front.min');

require('../js/lazysizes.min');