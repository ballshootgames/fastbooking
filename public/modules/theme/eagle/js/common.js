$(document).ready(function(){
	var html = $('#input-dd').html();
	$('#danh-muc-sect').change(function(event) {
		if($(this).val() === 'tour-du-lich-nuoc-ngoai'){
			$('#input-dd').html($('.ngoai-nuoc').html());
		} else if($(this).val() === 'tour-trong-nuoc'){
			$('#input-dd').html($('.trong-nuoc').html());
		} else{
			$('#input-dd').html(html);
		}
	});
	$("nav#menu-mobile").mmenu();

	$(".rslides").responsiveSlides({
		pager: true
	});

	$('.hk-slick').slick({
		dots: false,
		swipe: false,
		infinite: true,
		arrows: true,
		speed: 1000,
		slidesToShow: 1,
		slidesToScroll: 1
	});
	$('.tour-slick').slick({
		dots: false,
		swipe: false,
		infinite: true,
		arrows: true,
		speed: 1000,
		slidesToShow: 2,
		slidesToScroll: 2
	});

	$('ul.lst-lct').slick({
		dots: true,
		swipe: false,
		infinite: true,
		arrows: false,
		speed: 1000,
		slidesToShow: 4,
		slidesToScroll: 4,
		responsive: [{
			breakpoint: 1200,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 3
			}
		},
		{
			breakpoint: 630,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 2
			}
		},
		{
			breakpoint: 430,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		}]
	});
	
	$('#lst-kh-slider').slick({
		dots: true,
		infinite: true,
		arrows: false,
		speed: 1000,
		slidesToShow: 4,
		slidesToScroll: 4,
		autoplay: true,
		autoplaySpeed: 2000,
		responsive: [{
			breakpoint: 1200,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 3
			}
		},
		{
			breakpoint: 630,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 2
			}
		},
		{
			breakpoint: 430,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		}]
	});

	$('.lst-fdb').slick({
		dots: true,
		swipe: false,
		infinite: true,
		arrows: false,
		speed: 1000,
		slidesToShow: 1,
		slidesToScroll: 1
	});
});
jQuery(document).ready(function() {
  var offset = 220;
  var duration = 500;
  jQuery(window).scroll(function() {
    if (jQuery(this).scrollTop() > offset) {
      jQuery('.back-to-top').fadeIn(duration);
    } else {
      jQuery('.back-to-top').fadeOut(duration);
    }
  });
  jQuery('.back-to-top').click(function(event) {
    event.preventDefault();
    jQuery('html, body').animate({
      scrollTop: 0
    }, duration);
    return false;
  })
});
$(document).ready(function() {
    $('.imageGallery').lightSlider({
        gallery:true,
        item:1,
        loop:true,
        thumbItem:6,
        slideMargin:0,
        enableDrag: false,
        currentPagerPosition:'left',
        onSliderLoad: function(el) {
            el.lightGallery({
                selector: '.imageGallery .lslide'
            });
        }
    });
    $('.owl-carousel').owlCarousel({
	    loop:true,
	    margin:10,
	    nav:true,
	    dots: false,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:3
	        },
	        1000:{
	            items:5
	        },
	        1200:{
	            items:6
	        }
	    },
	    navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
	});
    $('.btn_close_dangky').click(function () {
        $('.popup_dangky').hide();
    })
});