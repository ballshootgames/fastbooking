<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group(['prefix' => 'v1','middleware' => 'auth:api'], function () {
	Route::get('/me', function () {
		return new \App\Http\Resources\UserResource(\Auth::user());
	});
	Route::post('/change-password', 'Admin\UsersController@changePassword');

//	Route::get('customer-autocomplete', 'BookingJourneysController@autocompleteCustomer');

	Route::post('fcm-token/store', 'DeviceNotifiesController@store');
	Route::delete('fcm-token/delete/{token}', 'DeviceNotifiesController@destroy');

	Route::get('notifications', 'NotificationsController@index');
	Route::get('notifications/number-unread', 'NotificationsController@numberUnread');
});
