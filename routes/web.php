<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$subdomain = \URL::getDefaultParameters()?\URL::getDefaultParameters()['subdomain']:"";
$appRoot = config('app.root').".";
if (\URL::getRequest()->getHost()!=$appRoot.config('app.domain') || !empty($subdomain))
    $appRoot = "";

Route::domain( '{subdomain}.' . $appRoot . config('app.domain'))->group(function () {
//	Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::group(['middleware' => ['subdomain']], function () {
        Route::auth();
    });
    include __DIR__.'/lfm.php';
    Route::group(['middleware' => ['subdomain', 'auth']], function () {
        //Route::auth();
//        Route::get('/', function () {
//            return redirect()->route('home');
//        });
        Route::get('/home', 'HomeController@index')->name('home');
        Route::get('admin', 'Admin\AdminController@index');
        Route::get('admin/give-role-permissions', 'Admin\AdminController@getGiveRolePermissions');
        Route::post('admin/give-role-permissions', 'Admin\AdminController@postGiveRolePermissions');
        Route::resource('admin/roles', 'Admin\RolesController');
        Route::resource('admin/permissions', 'Admin\PermissionsController');
        Route::get('admin/permissions-init', 'Admin\PermissionsController@getInitPermissions');
        Route::post('admin/permissions-init', 'Admin\PermissionsController@postInitPermissions');
        Route::resource('admin/users', 'Admin\UsersController');
        //	Route::get('admin/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@getGenerator']);
        //	Route::post('admin/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@postGenerator']);

        Route::get('profile', 'Admin\UsersController@getProfile');
        Route::post('profile', 'Admin\UsersController@postProfile');

        Route::resource('admin/company', 'Admin\CompaniesController');

        // Backup
        //	Route::get('admin/backup', 'Admin\BackupController@index');
        //	Route::put('admin/backup/create', 'Admin\BackupController@create');
        //	Route::get('admin/backup/download/{file_name?}', 'Admin\BackupController@download');
        //	Route::delete('admin/backup/delete/{file_name?}', 'Admin\BackupController@delete')->where('file_name', '(.*)');


        Route::get('settings/modules', 'ModulesController@index');
        Route::put('settings/modules/{module}', 'ModulesController@active');

        Route::resource('cities', 'CitiesController');
//        Route::resource('customers', 'CustomersController');


        Route::get('ajax/{action}', 'AjaxController@index');

        Route::post('fcm-token/store', 'DeviceNotifiesController@store');
        Route::delete('fcm-token/delete/{token}', 'DeviceNotifiesController@destroy');

        Route::get('notifications', 'NotificationsController@index');
        Route::get('notifications/number-unread', 'NotificationsController@numberUnread');

        //	Route::get('reports/booking-number/{module?}', 'ReportsController@numberBookingByDate');
        //	Route::get('reports/booking-people/{module?}', 'ReportsController@peopleBookingByDate');
        //	Route::get('reports/finance/{module?}', 'ReportsController@financeBookingByDate');

        Route::resource('user-titles', 'UserTitleController');
        Route::resource('user-departments', 'UserDepartmentController');
        Route::resource('payments', 'PaymentMethodController');

        Route::resource('countries', 'CountryController');
        Route::resource('cities', 'CityController');
        Route::resource('districts', 'DistrictController');

        Route::get('admin/settings', 'SettingController@index');
        Route::patch('admin/settings', 'SettingController@update');
        Route::get('admin/set-theme', 'SettingController@setTheme');
        Route::get('admin/set-theme/{theme_id}', 'SettingController@updateTheme');
        Route::post('admin/ajax/delete-data-demos', 'Admin\DeleteDataDemoController@deleteDataDemos');
    });
});

//Route::domain('{subdomain}.'.config('app.domain'))->group(function () {
//    Route::group(['middleware' => ['subdomain']], function () {
//        Route::resource('admin/users', 'Admin\UsersController');
//    });
//});

Route::group(['middleware' => 'auth'], function () {
	include __DIR__.'/lfm.php';
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('admin/users', 'Admin\UsersController');
    Route::get('profile', 'Admin\UsersController@getProfile');
    Route::post('profile', 'Admin\UsersController@postProfile');
    Route::resource('admin/roles', 'Admin\RolesController');
    Route::resource('admin/permissions', 'Admin\PermissionsController');
    Route::resource('trials', 'TrialController')->except([
	    'create', 'store',
    ]);
	;
});

Route::auth();

Route::post('register-trial', 'TrialController@registerTrial');
Route::get('/build-web', 'TrialController@getBuildWebsite');
Route::post('/build-web', 'TrialController@postBuildWebsite');

Route::get('/'.config('image.thumbnails_route').'/{width}x{height}/{image_path}', function ($width, $height, $image_path){
	return \App\Traits\ImageResize::setThumbnail($width, $height, $image_path);
})->where('image_path', '.*');
//Route::get('/demo', function (){
//});