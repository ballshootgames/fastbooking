<?php

namespace App;

use App\Traits\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class PaymentMethod extends Model
{
    use Sortable, SoftDeletes;
    use Authorizable;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'payment_methods';

    protected $dates = ['deleted_at'];
    public $sortable = [
        'id',
        'name',
        'code',
        'arrange',
        'active',
        'created_at'
    ];

    protected $primaryKey = 'id';

    protected $fillable = ['name', 'code', 'description', 'arrange', 'active'];

    public function bookings(){
        return $this->hasMany('Modules\Booking\Entities\Booking', 'payment_method_id');
    }

    public function user(){
        return $this->belongsTo('App\User', 'creator_id');
    }

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model){
            $model->creator_id = \Auth::user()->id;
            $model->updator_id = \Auth::user()->id;
        });
        self::updating(function ($model){
            $model->updator_id = \Auth::user()->id;
        });
    }
}
