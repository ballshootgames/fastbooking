<?php

namespace App;


use Modules\Theme\Entities\Theme;

class Menu
{
    public function getMenu($linkMod){
        $modules = \Module::all();
        $mod = [];
        foreach ($modules as $index=>$item){
            if ($item->active == 1){
                if ($item->name != 'Booking' && $item->name != 'Theme')
                    if ($linkMod == 'link'){
                        $mod[] = config($item->alias . '.' . $linkMod);
                    }else{
                        if (config($item->alias . '.' . $linkMod) == null) continue;
                        $mod[] = config($item->alias . '.' . $linkMod);
                    }
            }
        }
        return $mod;
    }
}
