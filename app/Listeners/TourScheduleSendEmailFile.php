<?php

namespace App\Listeners;

use App\Events\FileTourSchedule;
use App\Mail\SendMailTourScheduleFile;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Tour\Entities\Tour;

class TourScheduleSendEmailFile implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  FileTourSchedule  $event
     * @return void
     */
    public function handle(FileTourSchedule $event)
    {
        if (!empty($event->contact->email)){
            \Mail::to( $event->contact->email )->send(new SendMailTourScheduleFile($event->tour, $event->settings));
        }
    }
}
