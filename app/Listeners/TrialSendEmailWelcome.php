<?php

namespace App\Listeners;

use App\Events\RegisterTrial;
use App\Mail\SendMailWelcomeUseTrial;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class TrialSendEmailWelcome implements ShouldQueue
{
	public $tries = 2;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegisterTrial  $event
     * @return void
     */
    public function handle(RegisterTrial $event)
    {
    	if(!empty($event->trial->email)) {
		    \Mail::to( $event->trial->email )->send(new SendMailWelcomeUseTrial($event->trial));
	    }
    }
}
