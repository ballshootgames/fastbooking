<?php

namespace App\Listeners;

use App\Company;
use App\Events\InstallmentEvent;
use App\Mail\SendMailInstallment;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Booking\Entities\Customer;
use Modules\Installment\Entities\Installment;
use Modules\Tour\Entities\Tour;

class InstallmentMailListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  InstallmentEvent  $event
     * @return void
     */
    public function handle(InstallmentEvent $event)
    {
        $tour = Tour::where('id', $event->installment->tour_id)->first();
        $customer = Customer::where('id', $event->installment->customer_id)->first();
        $installment_mail = $customer->email;
        $email_company = $event->settings['email_contact_advisory'];
        if (!empty($installment_mail)){
            if (!empty($email_company)){
                \Mail::to($installment_mail)->cc($email_company)->send(new SendMailInstallment($event->installment, $tour, $event->settings));
            }else{
                \Mail::to($installment_mail)->send(new SendMailInstallment($event->installment, $tour, $event->settings));
            }
        }
    }
}
