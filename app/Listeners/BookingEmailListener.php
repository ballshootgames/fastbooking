<?php

namespace App\Listeners;

use App\Company;
use App\Events\BookingEvent;
use App\Mail\SendMailBooking;
use App\Role;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Booking\Entities\Booking;

class BookingEmailListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BookingEvent  $event
     * @return void
     */
    public function handle(BookingEvent $event)
    {
        $bookings = Booking::whereIn('id',$event->booking_id)->get();
        $booking_first = $bookings->first();
        $booking_mail = $booking_first->customer->email;

//        $users = User::whereHas('roles', function ($query){
//              $query->where('name', 'company_admin');
//        })->get();

        $email_company = null;
        $companies = Company::all();
        if ($companies->first()) $email_company = $companies->first()->email;

        if (!empty($booking_mail)){
            if (!empty($email_company)){
                \Mail::to($booking_mail)->cc($email_company)->send(new SendMailBooking($bookings));
            }else{
                \Mail::to($booking_mail)->send(new SendMailBooking($bookings));
            }
        }
    }
}
