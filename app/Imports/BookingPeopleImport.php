<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Carbon\Carbon;
use Modules\Booking\Entities\BookingPeople;

class BookingPeopleImport implements WithMultipleSheets
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
//    public function model(array $row)
//    {
//        return new BookingPeople([
//        ]);
//    }

    use Importable;

    public function sheets(): array
    {
        return [
            0=>new FirstSheetImport()
        ];
    }
}

class FirstSheetImport implements ToCollection
{
    public function collection(Collection $rows)
    {
        try{
            if($rows != null && count($rows) > 0)
                return $rows;
            return null;
        }catch(Exception $e){
            return null;
        }
    }
}