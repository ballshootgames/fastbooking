<?php

namespace App\Handlers;

class LfmConfigHandler extends \UniSharp\LaravelFilemanager\Handlers\ConfigHandler
{
    public function userField()
    {
    	if(isset(\URL::getDefaultParameters()['subdomain']))
    	    return \URL::getDefaultParameters()['subdomain'];
        return "root";
    }
}
