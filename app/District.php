<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class District extends Model
{
    use Sortable, SoftDeletes;

    protected $table = "districts";

    protected $dates = ['deleted_at'];
    public $sortable = [
        'id',
        'name',
        'type',
        'city_id',
        'arrange',
        'active',
        'created_at'
    ];

    protected $primaryKey = "id";

    protected $fillable = ['name', 'type', 'city_id', 'arrange', 'active'];

    public function city(){
        return $this->belongsTo('App\City');
    }

    public function wards(){
        return $this->hasMany('App\Ward');
    }

    public function user(){
        return $this->belongsTo('App\User', 'creator_id');
    }

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model){
            $model->creator_id = \Auth::user()->id;
            $model->updator_id = \Auth::user()->id;
        });
        self::updating(function ($model){
            $model->updator_id = \Auth::user()->id;
        });
    }
}
