<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class Cart
{
    public $carts = [];

    public $books = [];

    public function __construct(){
        /*$oldCart = \Session::has('cart') ? \Session::get('cart') : [];
        $this->carts = $oldCart;*/
        $oldBooking = \Session::has('book') ? \Session::get('book') : [];
        $this->books = $oldBooking;
    }

    public function getCart(){
        return $this->carts;
    }

    /*Add Booking Tour*/
    public function getBook(){
        return $this->books;
    }
    public function addBookingItem($tour,$adult = 1,$child = 0, $baby = 0){
        $id = $tour->id;
        $this->books[$id] = ['item' => $tour, 'adult' => $adult, 'child' => $child, 'baby' => $baby, 'pay' => 1];
        \Session::put('book', $this->books);
    }

    public function getTotalAdults(){
        if (empty($this->books)) return 0;

        $countAdults = 0;
        foreach ($this->books as $key => $item){
            $countAdults += $item['adult'];
        }

        return $countAdults;
    }
    public function getTotalChild(){
        if (empty($this->books)) return 0;

        $countChildren = 0;
        foreach ($this->books as $key => $item){
            $countChildren += $item['child'];
        }

        return $countChildren;
    }
    public function getTotalBaby(){
        if (empty($this->books)) return 0;

        $countBabies = 0;
        foreach ($this->books as $key => $item){
            $countBabies += $item['baby'];
        }

        return $countBabies;
    }
    public function getBookingTotalPrice(){
        if (empty($this->books)) return 0;

        $totalPrice = 0;
        foreach ($this->books as $key => $item){
            $itemTotalPrice = ($item['item']->price * $item['adult'] + $item['item']->price_children * $item['child'] + $item['item']->price_baby * $item['baby']);
            $totalPrice += $itemTotalPrice;
        }

        return $totalPrice;
    }
    public function getTotalPayment($deposit = null){
        if (empty($this->books)) return 0;

        $totalPayment = 0;
        foreach ($this->books as $key => $item){
            $totalPayment = $item['pay'] == 3 ? $this->getBookingTotalPrice()*$deposit : $this->getBookingTotalPrice();
        }
        return $totalPayment;
    }
    public function setQty($id, $qty, $name){
        $this->books[$id][$name] = $qty;
        \Session::put('book', $this->books);
    }
    /*Add Order Product*/
    public function addItem($product){
        $id = $product->id;
        if (!isset($this->carts[$id])){
            $this->carts[$id] = ['item' => $product, 'qty' => 1];
        }else{
            $this->carts[$id]['qty']+= 1;
        }
        \Session::put('cart', $this->carts);
    }
    public function addManyItems($product, $qty){
        $id = $product->id;
        if (!isset($this->carts[$id])){
            $this->carts[$id] = ['item' => $product, 'qty' => $qty];
        }else{
            $this->carts[$id]['qty'] += $qty;
        }
        \Session::put('cart', $this->carts);
    }
    public function deleteItem($id){
        $this->carts[$id]['qty']--;
        if ($this->carts[$id]['qty']<= 0){
            unset($this->carts[$id]);
        }
        if (empty($this->carts)){
            \Session::forget('cart');
        }else{
            \Session::put('cart', $this->carts);
        }
    }
    public function removeItem($id){
        unset($this->carts[$id]);
        if (empty($this->carts)){
            \Session::forget('cart');
        }else{
            \Session::put('cart', $this->carts);
        }
    }
    public function deleteCart(Request $request){
        $request->session()->forget('cart');
    }

    public function setQtyItem($id, $qty){
        $this->carts[$id]['qty'] = $qty;
        \Session::put('cart', $this->carts);
    }

    public function getTotalQty(){
        if (empty($this->carts)) return 0;

        $countQty = 0;
        foreach ($this->carts as $key => $item){
            $countQty += $item['qty'];
        }
        return $countQty;
    }
    public function getTotalPrice(){
        if (empty($this->carts)) return 0;

        $totalPrice = 0;
        foreach ($this->carts as $key => $item){
            $itemTotalPrice = ($item['item']->price * $item['qty']);
            $totalPrice += $itemTotalPrice;
        }
        return $totalPrice;
    }
}
