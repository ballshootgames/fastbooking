<?php
namespace App;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Arr;
use PayPal\Api\Amount;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Exception\PayPalConnectionException;
use PayPal\Rest\ApiContext;

class PaymentPaypal{

    private $_api_context;

    public function __construct()
    {
        /** PayPal api context **/
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
                $paypal_conf['client_id'],
                $paypal_conf['secret'])
        );
        $this->_api_context->setConfig($paypal_conf['settings']);
    }

    public function payWithPaypal($tours, $totalPrice){
        //A resource representing a Payer that funds a payment For PayPal account payments
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        //(Optional) Lets you specify item wise information
        $listItem = [];
        foreach ($tours as $key => $tour){
            $listItem[$key] = new Item();
            $listItem[$key]->setName($tour['item']->name)
            ->setCurrency('USD')
            ->setQuantity($tour['qty'])
            ->setPrice($tour['item']->price/23000);
        }
        $values = Arr::flatten($listItem);

        $item_list = new ItemList();
        $item_list->setItems($values);

        //Lets you specify a payment amount. You can also specify additional details such as shipping, tax.
        $amount = new Amount();
        $amount->setCurrency('USD')
            ->setTotal($totalPrice/23000);

        //A transaction defines the contract of a payment – what is the payment for and who is fulfilling it.
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Thánh toán sản phẩm với Paypal');

        //Set the URLs that the buyer must be redirected to after payment approval/ cancellation
        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(\URL::route('check-paypal')) /** Specify return URL **/
        ->setCancelUrl(\URL::route('check-paypal'));

        //A Payment Resource; create one using the above types and intent set to ‘sale’
        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));

        try {
            $payment->create($this->_api_context);
        } catch (PayPalConnectionException $ex) {
            if (\Config::get('app.debug')) {
                \Session::put('paypal_error', 'Đã hết thời gian kết nối, vui lòng thử lại!');
                return \redirect('thanh-toan');
            } else {
                \Session::put('paypal_error', 'Có thể một số lỗi đã xảy ra, xin lỗi vì sự bất tiện này!');
                return \redirect('thanh-toan');
            }
        }

        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
        /** add payment ID to session **/
        \Session::put('paypal_payment_id', $payment->getId());
        if (isset($redirect_url)) {
            /** redirect to paypal **/
            return Redirect::away($redirect_url);
        }
        //Unknown error occurred
        \Session::put('paypal_error', 'Có thể một số lỗi đã xảy ra, xin lỗi vì sự bất tiện này!');
        return \redirect('thanh-toan');
    }

    public function getPaymentStatus()
    {
        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');

        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
            \Session::put('paypal_error', 'Thanh toán thất bại');
            return \redirect('thanh-toan');
        }

        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));

        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);
        if ($result->getState() == 'approved') {
            \Session::put('paypal_success', 'Thanh toán thành công');
            Session::forget('cart');
            return \redirect('thanh-toan');
        }
        \Session::put('paypal_error', 'Thanh toán thất bại');
        return \redirect('thanh-toan');
    }
}