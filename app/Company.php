<?php

namespace App;

use App\Traits\ImageResize;
use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;
use Kyslik\ColumnSortable\Sortable;

class Company extends Model
{
    protected $guarded = [];

    use NodeTrait, Sortable, ImageResize;

	public static $imageFolder = "companies";
	public static $imageMaxWidth = 769;
	public static $imageMaxHeight = null;


	protected $table = 'companies';

    protected $primaryKey = 'id';

    public $sortable = ['id',
        'name',
        'trading_name',
        'updated_at'
    ];

    protected $fillable = ['name', 'trading_name', 'logo', 'birthday', 'address', 'representative', 'title_id', 'phone_number', 'phone_mobile', 'fax', 'email', 'website', 'parent_id'];

    public function title(){
        return $this->belongsTo('App\UserTitle');
    }

    public function users(){
        return $this->hasMany('App\User', 'company_id');
    }

    public static function getListCompanyInfo()
    {
        return Company::pluck('name', 'id')->all();
    }

    /**
     * Lấy ra danh sách Company theo thứ tự
     *
     * @param $listCompany: Danh sách Công ty theo Tree
     * @param null $companyId: Hiển thị khi edit
     * @param string $index
     * @param array $result
     * @return array
     */
    public function getListCompanyInfoToArray($listCompany, $companyId = null, $index = '', $result = [])
    {
        global $result;
        foreach ($listCompany as $company){
            $result[$company->id] = $index.$company->name;
            if (!empty($company->children)) {
                $this->getListCompanyInfoToArray($company->children, $companyId, $index . '-- ', $result);
            }
        }

        if (!empty($companyId)){
            if (array_key_exists($companyId, $result)){
                unset($result[$companyId]);
            }
        }

        return $result;
    }

    public function getListCompanyId($listCompany, $result_2 = []){
        global $result_2;
        if($listCompany->count()===0) return [];
        foreach ($listCompany as $company){
            $result_2[] = $company->id;
            if (!empty($company->children)) {
                $this->getListCompanyId($company->children, $result_2);
            }
        }
        return $result_2;
    }

    /**
     * Hiển thị danh sách ở giao diện chính
     *
     * @param $companies
     * @param int $level
     */
    public function showListCompanyInfo($companies, $level = 1){
//        $listCF = $this->getListCompanyInfo();
        echo '<ul class="list-tree level-'.$level.'">';
        foreach ($companies as $company){
            echo '<li class="tree-view" data-id="'.$company->id.'">';
                echo '<div class="row row-view-name"><div class="view-name col-md-9">';
                    if (!empty($company->logo)){
                        echo '<span><img style="width: 30px; height: 25px;" src="'.asset(\Storage::url($company->logo)).'"></span>';
                    }else{
                        echo '<span><img style="width: 30px; height: 25px;" src="'.asset('img/default-logo.png').'"></span>';
                    }
                        echo $company->name;
                echo '</div>';
                echo '<div class="view-action col-md-3"><span class="action">
                        <a href="'.url('admin/company/create?id='.$company->id).'" title="'.__('message.add').'"><button class="btn btn-success btn-xs"><i class="fa fa-plus" aria-hidden="true"></i></button></a>
                        <a href="'.url('admin/company/'.$company->id).'" title="'.__('message.view').'"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                        <a href="'.url('admin/company/'.$company->id.'/edit').'" title="'.__('message.edit').'"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                        <form method="POST" action="'.url('admin/company/'.$company->id).'" style="display: inline">
                            <input type="hidden" name="_method" value="delete" />
                            <input class="_token" name="_token" type="hidden" value="">
                            <button type="submit" class="com-delete btn btn-danger btn-xs" title="Xóa"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                        </form>
                    </span></div>';
                echo '</div>';
            echo '</li>';
            if (!empty($company->children)){
                $this->showListCompanyInfo($company->children, $level+1);
            }
        }
        echo '</ul>';
    }
}
