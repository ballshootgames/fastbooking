<?php

namespace App;

use App\Events\RegisterTrial;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Trial extends Model
{
	protected $connection = 'mysql';
    protected $table = 'trials';

    protected $primaryKey = 'id';

    protected $fillable = ['name', 'email', 'phone', 'company_name','db_name', 'subdomain', 'expired'];

	/**
	 * Get the trial expired
	 *
	 * @param  string  $value
	 * @return string
	 */
	public function getExpiredAttribute($value)
	{
		if(empty($value) && !empty($this->created_at)) return Carbon::parse($this->created_at)->addDays(config('trial.trial_days'))->format('Y-m-d');
		return $value;
	}

	public function getSiteUrlAttribute()
	{
		$subdomain = $this->subdomain;
		$http      = strpos( config( 'app.url' ), 'https://' ) === false ? 'http://' : 'https://';

		return str_replace( $http, "$http$subdomain.", config( 'app.url' ) );
	}

	public static function boot()
	{
		parent::boot();
		static::creating(function ($trial) {
			$trial->expired = Carbon::now()->addDays(config('trial.trial_days'))->format('Y-m-d');
		});
		static::created(function ($trial) {
			event(new RegisterTrial($trial));
		});
		static::updated(function ($trial) {

		});

		static::saving(function ($trial) {

		});
		static::deleted(function ($trial) {
			if($trial->db_name) {
				try{
					$pdo = \DB::connection()->getPdo();
					$pdo->exec( sprintf(
						'DROP DATABASE %s;',
						$trial->db_name
					) );
				} catch (\Exception $e){

				}
			}
		});
	}
}
