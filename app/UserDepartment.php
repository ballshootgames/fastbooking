<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class UserDepartment extends Model
{
    use Sortable;

    protected $table = 'user_departments';

    protected $primaryKey = 'id';

    public $sortable = ['id','name','updated_at'];

    protected $fillable = ['name','description'];

    public function user(){
        return $this->hasMany('App\User');
    }
}
