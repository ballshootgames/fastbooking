<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Setting;
use Modules\Booking\Entities\Booking;

class SendMailBooking extends Mailable
{
    use Queueable, SerializesModels;
    private $bookings;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($bookings)
    {
        $this->bookings = $bookings;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $booking = $this->bookings[0];
        $booking = Booking::with(['services'])->findOrFail($booking->id);
        // lay cac thong so truoc khi hien thi email
        // booking, customer, company config, ...
        // code, tourname, tourcode, customer info, company info.
        $customer = $booking->customer;
        $tour = $booking->services->first();
        $settings = Setting::allConfigs();
        $company = array('name'=>'', 'address'=>'', 'phone'=>'', 'email'=>'', 'website'=>'');
        foreach ($settings['company_info'] as $item)
        {
            if ($item['key']=='company_name')
            {
                $company['name'] = $item['value'];
            } else if ($item['key']=='company_address')
            {
                $company['address'] = $item['value'];
            } else if ($item['key']=='phone')
            {
                $company['phone'] = $item['value'];
            } else if ($item['key']=='email_receive_message')
            {
                $company['email'] = $item['value'];
            } else if ($item['key']=='website_link')
            {
                $company['website'] = $item['value'];
            }
        }

        return $this->view('emails.bookings.order', compact('booking', 'customer', 'tour', 'company'))->subject($company['name'] .': New Booking Tour');
    }
}
