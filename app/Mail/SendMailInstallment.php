<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailInstallment extends Mailable
{
    use Queueable, SerializesModels;

    public $installment;
    public $tour;
    public $settings;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($installment, $tour, $settings)
    {
        $this->installment = $installment;
        $this->tour = $tour;
        $this->settings = $settings;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $tour = $this->tour;
        $installment = $this->installment;
        $settings = $this->settings;
        return $this->view('emails.installments.index', compact('tour', 'installment', 'settings'))->subject('Mua trả góp tour '.$tour->name);
    }
}
