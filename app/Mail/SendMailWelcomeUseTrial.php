<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailWelcomeUseTrial extends Mailable
{
	use Queueable, SerializesModels;
	private $trial;

	/**
	 * Create a new message instance.
	 *
	 * @return void
	 */
	public function __construct($trial)
	{
		$this->trial = $trial;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{
		$trial = $this->trial;
		//TODO: check lỗi giao diện nội dung email với markdown chưa rõ nguyên nhân
		return $this->markdown('emails.trials.welcome', compact('trial'))->subject('Chào mừng '.config('app.name'). '!');
	}
}
