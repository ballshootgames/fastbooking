<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailTourScheduleFile extends Mailable
{
    use Queueable, SerializesModels;

    public $tour;
    public $settings;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($tour, $settings)
    {
        $this->tour = $tour;
        $this->settings = $settings;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $tour = $this->tour;
        $settings = $this->settings;
        return $this->view('emails.tour-schedules.files', compact('tour', 'settings'))->subject('Chương trình tour '.$tour->name);
    }
}
