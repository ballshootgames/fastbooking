<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class City extends Model
{
    use Sortable, SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cities';

    protected $dates = ['deleted_at'];
    public $sortable = [
        'id',
        'name',
        'country_id',
        'arrange',
        'active',
        'created_at'
    ];

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'country_id', 'arrange', 'active'];

    public function tours(){
        return $this->hasMany('Modules\Tour\Entities\Tour', 'start_city_id');
    }

    public function buses(){
        return $this->hasMany('Modules\Bus\Buses');
    }

    public function country(){
        return $this->belongsTo('App\Country');
    }

    public function districts(){
        return $this->hasMany('App\District');
    }

    public function schedules(){
        return $this->belongsToMany('Modules\Tour\Entities\TourSchedule', 'schedule_cities');
    }

    public function user(){
        return $this->belongsTo('App\User', 'creator_id');
    }

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model){
            $model->creator_id = \Auth::user()->id;
            $model->updator_id = \Auth::user()->id;
        });
        self::updating(function ($model){
            $model->updator_id = \Auth::user()->id;
        });
    }
}
