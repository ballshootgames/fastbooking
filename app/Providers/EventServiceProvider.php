<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
	    'Illuminate\Auth\Events\Login' => [
		    'App\Listeners\LogSuccessfulLogin',
	    ],
        'App\Events\LogEvent' => [
	        'App\Listeners\LogListenerAction',
        ],
        'App\Events\BookingEvent' => [
            'App\Listeners\BookingEmailListener',
        ],
	    'App\Events\RegisterTrial' => [
		    'App\Listeners\TrialSendEmailWelcome',
	    ],
        'App\Events\FileTourSchedule' => [
            'App\Listeners\TourScheduleSendEmailFile',
        ],
        'App\Events\InstallmentEvent' => [
            'App\Listeners\InstallmentMailListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
