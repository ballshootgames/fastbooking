<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class UserTitle extends Model
{
    use Sortable;

    protected $table = 'user_titles';

    protected $primaryKey = 'id';

    public $sortable = ['id','name','updated_at'];

    protected $fillable = ['name','description'];

    public function user(){
        return $this->hasMany('App\User');
    }

    public function companies(){
        return $this->hasMany('App\Company');
    }
}
