<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ward extends Model
{
    protected $table = "wards";

    protected $primaryKey = "id";

    protected $fillable = ["name", "type", "district_id"];

    public function district(){
        return $this->belongsTo('App\District');
    }
}
