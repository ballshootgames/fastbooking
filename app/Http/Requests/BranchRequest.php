<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BranchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'email|nullable',
            'birthday' => 'nullable|date_format:"'.config('settings.format.date').'"',
            'city_id' => 'required',
            'phone' => 'numeric|nullable|digits_between:7,13'
        ];
    }
}
