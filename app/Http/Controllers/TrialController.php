<?php

namespace App\Http\Controllers;

use App\Trial;
use App\User;
use Artisan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Session;

class TrialController extends Controller
{
	//Register trial - STEP 1: create trial record
    public function registerTrial(Request $request){
		$this->validate($request, [
			'company_name' => 'required|max:30|unique:trials',
			'name' => 'required|max:255',
			'email' => 'required|email|max:255|unique:trials',
			'phone' => 'required|min:9|max:15',
			'password' => 'required|min:6',
		], [], [
			'company_name' => "Tên công ty | Website",
			'name' => 'Tên của bạn',
			'email' => 'Email đăng ký',
			'phone' => 'Số điện thoại',
			'password' => 'Mật khẩu',
		]);
		if($request->expectsJson()){
			return response()->json('Validate OK');
		}
		$trial = Trial::create($request->all());
		return redirect('/build-web')
			->with('password', $request->password)
			->with('id', $trial->id);
    }
	//Register trial - STEP 2: show view processing LOADING
    public function getBuildWebsite(Request $request){
	    $password = $request->session()->get('password');
	    $trialID = $request->session()->get('id');

	    if(empty($password) || empty($trialID)) abort(404);

	    return view('build.index', compact('password', 'trialID'));
    }
	//Register trial - STEP 3: processing data
	public function postBuildWebsite(Request $request){
		$this->validate($request, [
			'trialID' => 'required|integer|exists:trials,id',
			'password' => 'required|min:6',
			'step' => 'required|min:1'
		]);
		$trial = Trial::findOrFail($request->trialID);
		$message = '';
		switch ($request->step){
			case 1:
				//Tạo subdomain và database
				$database = 'lv_dbauto_'.$trial->id;
				$index = 0;
				do {
					$subdomain = Str::slug( $index>0 ? $trial->company_name.$index : $trial->company_name);
				}while (Trial::where('subdomain', $subdomain)->count() > 0);
				//Main domain: update info
				$trial->update(['subdomain' => $subdomain, 'db_name' => $database]);
				//Subdomain: tạo tự động DB
				Artisan::call("db:create $database");
				$message = 'Khởi tạo cơ sở dữ liệu';
				break;
			case 2:
				$database = $trial->db_name;
				//Khởi tạo dữ liệu
				config(["database.connections.$database" => [
					"driver" => "mysql",
					"host" => config('database.connections.mysql.host'),
					"port" => config('database.connections.mysql.port'),
					"database" => $database,
					"username" => config('database.connections.mysql.username'),
					"password" => config('database.connections.mysql.password'),
					"charset" => config('database.connections.mysql.charset'),
					"collation" => config('database.connections.mysql.collation'),
					"prefix" => config('database.connections.mysql.prefix'),
					"strict" => config('database.connections.mysql.strict'),
					"engine" => config('database.connections.mysql.engine')
				]]);
				Artisan::call('migrate', ['--database' => $database]);
				$message = 'Cài đặt dữ liệu demo';
				break;
			case 3:
				$database = $trial->db_name;
				//Khởi tạo dữ liệu
				config(["database.connections.$database" => [
					"driver" => "mysql",
					"host" => config('database.connections.mysql.host'),
					"port" => config('database.connections.mysql.port'),
					"database" => $database,
					"username" => config('database.connections.mysql.username'),
					"password" => config('database.connections.mysql.password'),
					"charset" => config('database.connections.mysql.charset'),
					"collation" => config('database.connections.mysql.collation'),
					"prefix" => config('database.connections.mysql.prefix'),
					"strict" => config('database.connections.mysql.strict'),
					"engine" => config('database.connections.mysql.engine')
				]]);
				//Subdomain: seeder - tạo dữ liệu demo
				Artisan::call('module:seed Tour --database='.$database);
				Artisan::call('module:seed Theme --database='.$database);
				Artisan::call('module:seed News --database='.$database);
				Artisan::call('module:seed Booking --database='.$database);
//				Artisan::call('db:seed', ['--class' => 'AdminUserSeeder', '--database' => $database]);
//                Artisan::call('db:seed', ['--class' => 'PermissionsTableSeeder', '--database' => $database]);
//                Artisan::call('db:seed', ['--class' => 'DemoTitleDepartmentSeeder', '--database' => $database]);
//                Artisan::call('db:seed', ['--class' => 'DataCountrySeeder', '--database' => $database]);
                Artisan::call('db:seed', ['--database' => $database]);
				$message = 'Cấu hình hệ thống';
				break;
			case 4:
				$database = $trial->db_name;
				//Khởi tạo dữ liệu
				config(["database.connections.$database" => [
					"driver" => "mysql",
					"host" => config('database.connections.mysql.host'),
					"port" => config('database.connections.mysql.port'),
					"database" => $database,
					"username" => config('database.connections.mysql.username'),
					"password" => config('database.connections.mysql.password'),
					"charset" => config('database.connections.mysql.charset'),
					"collation" => config('database.connections.mysql.collation'),
					"prefix" => config('database.connections.mysql.prefix'),
					"strict" => config('database.connections.mysql.strict'),
					"engine" => config('database.connections.mysql.engine')
				]]);
				//Cập nhật lại thông tin tài khoản
				//Subdomain: update email/password admin (by seeder)
				$user = User::on($database)->where('id', 1)->first();
				if($user) {
					$user->update( [
						'name'     => $trial->name,
						'email'    => $trial->email,
						'password' => bcrypt($request->password)
					] );
					$user->profile()->create( [
						'phone' => $trial->phone
					] );
					$user->company()->create( [
						'name'         => $trial->company_name,
						'phone_mobile' => $trial->phone,
                        'email'    => $trial->email,
					] );
				}
				$message = 'Khởi tạo thành công';
				break;
			default;
		}
		if($request->step === 4) {
			return response()->json( [
				'message'   => $message,
				'subdomain' => $trial->siteUrl,
//                'url' => 'admin/set-theme',
			] );
		}else{
			return response()->json([
				'message' => $message,
				'step' => $request->step + 1
			]);
		}
	}

	/*CRUD*/
    public function index(Request $request){
        $keyword = $request->get('search');
        $perPage = config('settings.perpage');
        $trials = new Trial();
        if (!empty($keyword)){
            $trials = $trials->where('company_name','LIKE',"%$keyword%")
                            ->orWhere('subdomain','LIKE',"%$keyword%");
        }
        $trials = $trials->paginate($perPage);
        return view('trials.index', compact('trials'));
    }

    public function create(){
        return view('trials.create');
    }

    public function store(Request $request){
        $this->validate($request, [
            'company_name' => 'required|max:30|unique:trials',
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:trials',
            'phone' => 'required|min:9|max:15',
            'db_name' => 'required',
            'subdomain' => 'required',
            'expired' => 'required|date_format:"'.config('settings.format.date').'"',
        ]);

        \DB::transaction(function () use ($request){
            $requestData = $request->all();
	        if(!empty($requestData["expired"]))
		        $requestData["expired"] = Carbon::createFromFormat(config('settings.format.date'), $requestData["expired"])->format('Y-m-d');

	        Trial::create($requestData);
        });

        Session::flash('flash_message', __('trials.created_success'));
        return redirect('trials');
    }

    public function edit($id){
        $trial = Trial::findOrFail($id);
	    if(!empty($trial->expired)){
		    $trial->expired = Carbon::parse($trial->expired)->format(config('settings.format.date'));
	    }
        return view('trials.edit', compact('trial'));
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'company_name' => 'required|max:30',
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'phone' => 'required|min:9|max:15',
            'db_name' => 'required',
            'subdomain' => 'required',
            'expired' => 'required|date_format:"'.config('settings.format.date').'"',
        ]);
        $trial = Trial::findOrFail($id);

        \DB::transaction(function () use ($request, $trial){
            $requestData = $request->all();
	        if(!empty($requestData["expired"]))
		        $requestData["expired"] = Carbon::createFromFormat(config('settings.format.date'), $requestData["expired"])->format('Y-m-d');
            $trial->update($requestData);
        });

        Session::flash('flash_message', __('trials.updated_success'));
        return redirect('trials');
    }

    public function destroy($id){
        Trial::destroy($id);

        Session::flash('flash_message', __('trials.deleted_success'));
        return redirect('trials');
    }

	public function show($id)
	{
		$trial = Trial::findOrFail($id);

		return view('trials.show', compact('trial'));
	}
}
