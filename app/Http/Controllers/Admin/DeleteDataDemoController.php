<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DeleteDataDemoController extends Controller
{
    public function deleteDataDemos(Request $request){
        $tables = $request->arrTable;
        $subDomain = \URL::getDefaultParameters()['subdomain'];
        $path = 'public/images/sites/' . $subDomain;
        foreach ($tables as $table){
            \Storage::deleteDirectory($path. '/' .$table);
            \DB::table($table)->truncate();
        }
        return response()->json(['success' => 'ok']);
    }
}
