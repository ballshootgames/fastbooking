<?php

namespace App\Http\Controllers\Admin;

use App\Company;
use App\Http\Controllers\Controller;
use App\Traits\Authorizable;
use App\UserTitle;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CompaniesController extends Controller
{
    use  Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $comInfo = Company::get()->toTree();
        return view('admin.companies.index', compact('comInfo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $comInfoParentId = !empty($request->id) ? $request->id : null;
        $companyTree = Company::get()->toTree();
        $listCompany = new Company();
        $listCompany = $listCompany->getListCompanyInfoToArray($companyTree);
        $listTitle = UserTitle::pluck('name', 'id');
        $listTitle->prepend(__('message.please_select'), '')->all();
        return view('admin.companies.create', compact('listCompany', 'comInfoParentId', 'listTitle'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:companies,name',
            'parent_id' => 'numeric|nullable',
            'type_id' => 'numeric|nullable',
            'trading_name' => 'nullable',
            'email' => 'email|nullable',
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:'.(5*1024),
            'birthday' => 'nullable|date_format:"'.config('settings.format.date').'"',
            //'company_id' => \Auth::user()->isAdminCompany() ? 'required' : 'nullable',
            'phone_number' => 'numeric|nullable|digits_between:7,16',
            'phone_mobile' => 'numeric|nullable|digits_between:7,13',
            'fax' => 'numeric|nullable|digits_between:7,16',
        ]);

        $requestData = $request->all();

        \DB::transaction(function () use ($request, $requestData){
            if (!empty($requestData['birthday'])){
                $requestData['birthday'] = Carbon::createFromFormat(config('settings.format.date'),$request->birthday)->format('Y-m-d');
            }
            if ($request->hasFile('logo')){
                $requestData['logo'] = Company::saveImageResize($request->file('logo'));
            }
            //Tạo mới với parent
            Company::create($requestData);
        });

        Session::flash('flash_message', __('companies.created_success'));

        return redirect('admin/company');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comInfo = Company::findOrFail($id);
        return view('admin.companies.show', compact('comInfo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $companyTree = Company::whereNotDescendantOf($id)->get()->toTree();
        $listCompany = new Company();
        $listCompany = $listCompany->getListCompanyInfoToArray($companyTree, $id);
        $comInfo = Company::findOrFail($id);
        $comInfoParentId = $comInfo->parent_id;

        if (!empty($comInfo->birthday)){
            $comInfo->birthday = Carbon::parse($comInfo->birthday)->format(config('settings.format.date'));
        }
        $listTitle = UserTitle::pluck('name', 'id');
        $listTitle->prepend(__('message.please_select'), '')->all();

        return view('admin.companies.edit', compact('comInfo', 'listCompany', 'comInfoParentId', 'listTitle'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'parent_id' => 'numeric|nullable',
            'type_id' => 'numeric|nullable',
            'trading_name' => 'nullable',
            'email' => 'email|nullable',
            'logo' => 'image|mimes:jpeg,png,jpg,gif|max:'.(5*1024),
            'birthday' => 'nullable|date_format:"'.config('settings.format.date').'"',
            //'company_id' => \Auth::user()->isAdminCompany() ? 'required' : 'nullable',
            'phone_number' => 'numeric|nullable|digits_between:7,16',
            'phone_mobile' => 'numeric|nullable|digits_between:7,13',
            'fax' => 'numeric|nullable|digits_between:7,16',
        ]);

        $requestData = $request->all();
        $comInfo = Company::findOrFail($id);

        \DB::transaction(function () use ($request, $requestData, $comInfo){
            if (!empty($requestData['birthday'])){
                $requestData['birthday'] = Carbon::createFromFormat(config('settings.format.date'),$request->birthday)->format('Y-m-d');
            }

            if (!empty($requestData['check_delete_logo'])){
                Company::deleteFile($comInfo->logo);
                $requestData['logo'] = null;
            }

            if ($request->hasFile('logo')){
	            Company::deleteFile($comInfo->logo);
                $requestData['logo'] = Company::saveImageResize($request->file('logo'));
            }

            $comInfo->update($requestData);
        });

        Session::flash('flash_message', __('companies.updated_success'));

        return redirect('admin/company');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comInfo = Company::findOrFail($id);
	    Company::deleteFile($comInfo->logo);

        Company::destroy($id);

        Session::flash('flash_message', __('companies.deleted_success'));

        return redirect('admin/company');
    }
}
