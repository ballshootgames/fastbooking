<?php

namespace App\Http\Controllers;

use App\Country;
use App\Traits\Authorizable;
use Illuminate\Http\Request;
use Session;

class CountryController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = !empty($request->get('hidRecord')) ? $request->get('hidRecord') : Config("settings.perpage10");

        $countries = Country::sortable(['name' => 'asc']);

        if (!empty($keyword)){
            $countries = $countries->where('name', 'LIKE', "%$keyword%")->orWhere('capital', 'LIKE', "%$keyword%");
        }

        $countries = $countries->paginate($perPage);
        $totalInActive = Country::where('active', config('settings.inactive'))->count();
        $totalActive = Country::where('active', config('settings.active'))->count();

        return view('countries.index', compact('countries', 'totalInActive', 'totalActive'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $prevUrl = \URL::previous();
        return view('countries.create', compact('prevUrl'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'arrange' => 'nullable|numeric'
        ]);

        $requestData = $request->all();

        Country::create($requestData);

        Session::flash('flash_message', __('countries.created_success'));

        return redirect('countries');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $country = Country::findOrFail($id);
        $prevUrl = \URL::previous();

        return view('countries.show', compact('country', 'prevUrl'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $country = Country::findOrFail($id);
        $prevUrl = '';
        if ($request['hidUrl'] != null && $request['hidUrl']!='')
            $prevUrl = $request['hidUrl'];
        else
            $prevUrl = \URL::previous();

        return view('countries.edit', compact('country', 'prevUrl'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'arrange' => 'nullable|numeric'
        ]);
        $requestData = $request->all();
        $country = Country::findOrFail($id);
        if (empty($requestData['active'])){
            $requestData['active'] = 0;
        }
        $country->update($requestData);

        Session::flash('flash_message', __('countries.updated_success'));
        return \Redirect::to($requestData['hidUrl']);
//        return redirect('countries');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Country::destroy($id);

        Session::flash('flash_message', __('countries.deleted_success'));
        return redirect('countries');
    }
}
