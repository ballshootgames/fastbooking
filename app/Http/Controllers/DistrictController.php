<?php

namespace App\Http\Controllers;

use App\City;
use App\Country;
use App\District;
use App\Traits\Authorizable;
use Illuminate\Http\Request;
use Session;

class DistrictController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = !empty($request->get('hidRecord')) ? $request->get('hidRecord') : Config("settings.perpage10");
        $keyCity = $request->get('city');

        $districts = District::sortable(['name' => 'asc']);

        if (!empty($keyword)){
            $districts = $districts->where('name', 'LIKE', "%$keyword%");
        }
        if (!empty($keyCity)){
            $districts = $districts->whereHas('city', function ($query) use ($keyCity){
                $query->where('name','LIKE',"%$keyCity%");
            });
        }

        $districts = $districts->with('city');
        $districts = $districts->orderBy('city_id')->paginate($perPage);

        $countries = Country::pluck('name', 'id');
        $countries->prepend(__('Vui lòng chọn quốc gia'));
        $totalInActive = District::where('active', config('settings.inactive'))->count();
        $totalActive = District::where('active', config('settings.active'))->count();

        return view('districts.index', compact('districts', 'countries', 'totalInActive', 'totalActive'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::pluck('name', 'id');
        $countries->prepend(__('message.please_select'), '')->all();
        $cities = [ '' => __('message.please_select')];
        $types = config('settings.types');
        $prevUrl = \URL::previous();
        return view('districts.create', compact('countries','cities', 'types', 'prevUrl'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'country_id' => 'required',
            'city_id' => 'required',
            'arrange' => 'nullable|numeric'
        ]);

        \DB::transaction(function () use($request){
            $requestData = $request->all();
            District::create($requestData);
        });

        Session::flash('flash_message', __('districts.created_success'));
        return redirect('districts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $district = District::findOrFail($id);
        $prevUrl = \URL::previous();
        return view('districts.show', compact('district', 'prevUrl'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $district = District::findOrFail($id);
        $countries = Country::pluck('name', 'id');
        $countries->prepend(__('message.please_select'), '')->all();

        $country_id = optional(optional($district->city)->country)->id;
        $cities = City::where('country_id', $country_id)->pluck('name','id');
        $cities->prepend(__('message.please_select'), '')->all();

        $types = config('settings.types');
        $prevUrl = '';
        if ($request['hidUrl'] != null && $request['hidUrl']!='')
            $prevUrl = $request['hidUrl'];
        else
            $prevUrl = \URL::previous();

        return view('districts.edit', compact('district', 'countries','cities','types','country_id', 'prevUrl'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'country_id' => 'required',
            'city_id' => 'required',
            'arrange' => 'nullable|numeric'
        ]);

        $district = District::findOrFail($id);
        $requestData = $request->all();
        if (empty($requestData['active'])){
            $requestData['active'] = 0;
        }
        $district->update($requestData);

        Session::flash('flash_message', __('districts.updated_success'));
        return \Redirect::to($requestData['hidUrl']);
//        return redirect('districts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        District::destroy($id);

        Session::flash('flash_message', __('districts.deleted_success'));
        return redirect('districts');
    }
}
