<?php

namespace App\Http\Controllers;

use App\Traits\Authorizable;
use App\UserDepartment;
use Illuminate\Http\Request;
use Session;

class UserDepartmentController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = config('settings.perpage');
        $userDepartments = UserDepartment::sortable();
        if (!empty($keyword)){
            $userDepartments = $userDepartments->where('name','LIKE',"%$keyword%")
                ->orWhere('description','LIKE',"%$keyword%");
        }
        $userDepartments = $userDepartments->latest()->paginate($perPage);

        return view('user-departments.index', compact('userDepartments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user-departments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $requestData = $request->all();
        UserDepartment::create($requestData);

        Session::flash('flash_message', __('user_departments.created_success'));
        return redirect('user-departments');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $userDepartment = UserDepartment::findOrFail($id);

        return view('user-departments.show', compact('userDepartment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $userDepartment = UserDepartment::findOrFail($id);

        return view('user-departments.edit', compact('userDepartment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $requestData = $request->all();
        $userDepartment = UserDepartment::findOrFail($id);
        $userDepartment->update($requestData);

        Session::flash('flash_message', __('user_departments.updated_success'));
        return redirect('user-departments');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        UserDepartment::destroy($id);

        Session::flash('flash_message', __('user_departments.deleted_success'));
        return redirect('user-departments');
    }
}
