<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\ResizeImage;
use App\Setting;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;
use Modules\Theme\Entities\Theme;

class SettingController extends Controller
{
	//TODO: cần tối ưu lại và xóa file avatar củ khi cập nhật avatar mới.
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
	    $data = Setting::allConfigs();
        $tabs = array_keys($data);

        return view('settings.index', compact('data','tabs'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {
	    $settingConfigs = Setting::configsDefault();
	    //Xóa phần tử cuối cùng trong mảng đó là theme_name
//	    array_pop($settingConfigs);
	    foreach ($settingConfigs as $sc){
		    $setting = Setting::firstOrCreate(
			    ['key' => $sc['key']],
			    ['value' => $sc['value'], 'description' => $sc['description']]
		    );
	    	if($sc['type'] === 'image'){
			    if($request->hasFile($sc['key'])) {
				    Setting::deleteFile($setting->value);
				    $value = Setting::saveImageResize( $request->file( $sc['key'] ) );
				    $setting->update(['value' => $value]);
			    }
		    }else{
			    $setting->update(['value' => $request->get($sc['key'])]);
		    }
	    }
        return redirect('admin/settings')->with('flash_message', trans('settings.update_success'));
    }

    public function setTheme(Request $request){
        return view('build.theme');
    }

    public function updateTheme(Request $request){
        $themes = Theme::pluck('id')->toArray();
//        if (!in_array((int)$theme_id, $themes)){
//            return redirect('admin/set-theme');
//        }
//
//        $settingTheme = Setting::where('key', 'theme_name')->first();
//        if (empty($settingTheme)){
//            $settingTheme = new Setting();
//            $settingTheme->key = 'theme_name';
//        }
//        $settingTheme->value = Theme::find($theme_id)->name;
//        $settingTheme->save();

        return redirect('admin/settings');
    }
}
