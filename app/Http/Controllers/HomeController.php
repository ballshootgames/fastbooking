<?php

namespace App\Http\Controllers;

use App\Trial;
use App\User;
use Illuminate\Http\Request;
use Modules\Booking\Entities\Booking;
use Modules\Tour\Entities\Tour;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard of Sub_domain.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    	if (!empty(\URL::getDefaultParameters())){
            $tours = Tour::sortable();
            $tours = $tours->with('bookings');
            $tours = $tours->orderBy('updated_at', 'desc')->paginate(5);
            return view('adminlte::home', compact('tours'));
        }else{
            $trials = Trial::orderBy('updated_at', 'desc')->paginate(10);
            return view('adminlte::main', compact('trials'));
        }
    }

}
