<?php

namespace App\Http\Controllers;

use App\Traits\Authorizable;
use App\UserTitle;
use Illuminate\Http\Request;
use Session;

class UserTitleController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = config('settings.perpage');
        $userTitles = UserTitle::sortable();

        if (!empty($keyword)){
            $userTitles = $userTitles->where('name','LIKE',"%$keyword%")
                ->orWhere('description','LIKE',"%$keyword%");
        }

        $userTitles = $userTitles->paginate($perPage);
        return view('user-titles.index', compact('userTitles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user-titles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $requestData = $request->all();
        UserTitle::create($requestData);

        Session::flash('flash_message', __('user_titles.created_success'));
        return redirect('user-titles');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $userTitle = UserTitle::findOrFail($id);
        return view('user-titles.show', compact('userTitle'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $userTitle = UserTitle::findOrFail($id);
        return view('user-titles.edit', compact('userTitle'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $requestData = $request->all();
        $title = UserTitle::findOrFail($id);
        $title->update($requestData);

        Session::flash('flash_message', __('user_titles.updated_success'));
        return redirect('user-titles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        UserTitle::destroy($id);

        Session::flash('flash_message', __('user_titles.deleted_success'));
        return redirect('user-titles');
    }
}
