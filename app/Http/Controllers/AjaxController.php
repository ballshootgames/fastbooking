<?php
namespace App\Http\Controllers;

use App\Agent;
use App\City;
use App\ModuleInfo;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class AjaxController extends Controller
{
	/**
	 * Gọi ajax: sẽ gọi đến hàm = tên $action
	 * @param Request $action
	 * @param Request $request
	 * @return mixed
	 */
	public function index($action, Request $request)
	{
		return $this->{$action}($request);
	}

	/**
	 * Get list agent by company id
	 * action: getAgentsByCompanyID
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse|null
	 */
	private function getAgents(Request $request){

		$agents = Agent::select('name','id')->get();

		return response()->json($agents);
	}

    /**
     * Get list service in view report
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse|null
     */
	public function getServiceReport(Request $request){
	    if ($request->module == null){
	        return \response()->json();
        }
        $moduleInfo = new ModuleInfo($request->module);
        $service = $moduleInfo->getBookingServiceInfo();
        $list_services = $service['namespaceModel']::pluck('name', 'id');

        return \response()->json($list_services);
    }
    /*List Countries*/
    public function getListCountries(Request $request){
        $country_id = $request->country_id;
        $cities = City::where('country_id', $country_id)->pluck('name', 'id');
        return \response()->json($cities);
    }
    /*AutoComplete City*/
    public function getAutoCompleteCity(Request $request){
        return \response()->json(City::where('name','LIKE',"%$request->city%")->orderBy('updated_at','desc')->get());
    }
}