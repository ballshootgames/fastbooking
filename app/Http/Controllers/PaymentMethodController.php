<?php

namespace App\Http\Controllers;

use App\PaymentMethod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PaymentMethodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = !empty($request->get('hidRecord')) ? $request->get('hidRecord') : Config("settings.perpage10");
        $payments = PaymentMethod::sortable(['arrange' => 'asc']);
        if (!empty($keyword)){
            $payments = $payments->where(function ($query) use ($keyword){
                $query->where('name','LIKE',"%$keyword%")
                    ->orWhere('code','LIKE',"%$keyword%");
            });
        }
        $payments = $payments->paginate($perPage);
        $totalInActive = PaymentMethod::where('active', config('settings.inactive'))->count();
        $totalActive = PaymentMethod::where('active', config('settings.active'))->count();

        return view('payments.index', compact('payments', 'totalInActive', 'totalActive'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $prevUrl = \URL::previous();
        return view('payments.create', compact('prevUrl'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'arrange' => 'nullable|numeric'
        ]);
        $requestData = $request->all();
        PaymentMethod::create($requestData);
        Session::flash('flash_message', __('payments.created_success'));

        return redirect('payments');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PaymentMethod  $payments
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $payment = PaymentMethod::findOrFail($id);
        $prevUrl = \URL::previous();

        return view('payments.show', compact('payment', 'prevUrl'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PaymentMethod  $payments
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $payment = PaymentMethod::findOrFail($id);
        $prevUrl = '';
        if ($request['hidUrl'] != null && $request['hidUrl']!='')
            $prevUrl = $request['hidUrl'];
        else
            $prevUrl = \URL::previous();

        return view('payments.edit', compact('payment', 'prevUrl'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PaymentMethod  $payments
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'arrange' => 'nullable|numeric'
        ]);

        $requestData = $request->all();

        $requestData['status'] = array_key_exists('status', $requestData) ? $requestData['status'] : 0;
        $payment = PaymentMethod::findOrFail($id);
        if (empty($requestData['active'])){
            $requestData['active'] = 0;
        }
        $payment->update($requestData);

        Session::flash('flash_message', __('payments.updated_success'));

        return \Redirect::to($requestData['hidUrl']);
//        return redirect('payments');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PaymentMethod  $payments
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PaymentMethod::destroy($id);
        Session::flash('flash_message', __('payments.deleted_success'));

        return redirect('payments');
    }
}
