<?php

namespace App\Http\Controllers;

use App\City;
use App\Country;
use App\Traits\Authorizable;
use function foo\func;
use Illuminate\Http\Request;
use Session;

class CityController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = !empty($request->get('hidRecord')) ? $request->get('hidRecord') : Config("settings.perpage10");
        $keyCountry = $request->get('country_id');

        $cities = City::sortable(['name' => 'asc']);

        if (!empty($keyword)){
            $cities = $cities->where('name', 'LIKE', "%$keyword%");
        }
        if (!empty($keyCountry)){
            $cities = $cities->where('country_id',$keyCountry);
        }

        $cities = $cities->with('country');
        $cities = $cities->paginate($perPage);

        $countries = Country::pluck('name','id');
        $countries->prepend(__('message.please_select'), '')->all();
        $totalInActive = City::where('active', config('settings.inactive'))->count();
        $totalActive = City::where('active', config('settings.active'))->count();


        return view('cities.index', compact('cities', 'countries', 'totalInActive', 'totalActive'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::pluck('name','id');
        $countries->prepend(__('message.please_select'), '')->all();
        $prevUrl = \URL::previous();
        return view('cities.create', compact('countries', 'prevUrl'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'country_id' => 'required',
            'arrange' => 'nullable|numeric'
        ]);

        $city = new City();

        \DB::transaction(function () use($request, $city){
            $requestData = $request->all();
            $city->create($requestData);
        });

        Session::flash('flash_message', __('cities.created_success'));
        return redirect('cities');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $city = City::findOrFail($id);
        $prevUrl = \URL::previous();
        return view('cities.show', compact('city', 'prevUrl'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $city = City::findOrFail($id);
        $countries = Country::pluck('name','id');
        $countries->prepend(__('message.please_select'), '')->all();
        $prevUrl = '';
        if ($request['hidUrl'] != null && $request['hidUrl']!='')
            $prevUrl = $request['hidUrl'];
        else
            $prevUrl = \URL::previous();
        return view('cities.edit', compact('city', 'countries', 'prevUrl'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'country_id' => 'required',
            'arrange' => 'nullable|numeric'
        ]);

        $city = City::findOrFail($id);
        $requestData = $request->all();
        if (empty($requestData['active'])){
            $requestData['active'] = 0;
        }
        $city->update($requestData);

        Session::flash('flash_message', __('cities.updated_success'));
        return \Redirect::to($requestData['hidUrl']);
//        return redirect('cities');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        City::destroy($id);

        Session::flash('flash_message', __('cities.deleted_success'));
        return redirect('cities');
    }
}
