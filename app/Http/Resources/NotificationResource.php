<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\Resource;

class NotificationResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
	    $type = optional(optional(optional($this->data['booking'])['detail'])['bookingable'])['start_time'] ? "journeys" : "tours";
	    $avatar = optional(optional($this->data['user'])['profile'])['avatar'];

        return [
	        'content' => __('bookings.notification_body_'.$this->data['type']),
	        'content_data' => [
		        'user' => optional($this->data['user'])['name'],
		        'item' =>  optional(optional(optional($this->data['booking'])['detail'])['bookingable'])['name'],
		        'code' => optional($this->data['booking'])['code'],
		        'phone' => optional(optional($this->data['booking'])['customer'])['phone'],
		        'name' => optional(optional($this->data['booking'])['customer'])['name'],
	        ],
	        'avatar' => $avatar,
	        'unread' => $this->read_at ? false : true,
	        'booking_type' => $type,
	        'booking_id' => optional($this->data['booking'])['id'],
	        'created_at' => Carbon::parse($this->created_at)->gt(Carbon::now()->subDay()) ? Carbon::parse($this->created_at)->diffForHumans() : Carbon::parse($this->created_at)->format(config('settings.format.datetime'))
        ];
    }
}
