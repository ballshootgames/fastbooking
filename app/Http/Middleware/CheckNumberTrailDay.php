<?php

namespace App\Http\Middleware;

use Closure;
use App\Trial;
use Carbon\Carbon;

class CheckNumberTrailDay
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!empty(\URL::getDefaultParameters()['subdomain']))
        {
            $trialDays = 0;            
            if(!session()->has('trialDays'))
            {
                $subdomain = \URL::getDefaultParameters()['subdomain'];
                $trial = Trial::where('subdomain', $subdomain)->first();
                if(!$trial) abort(404);

                $expired = Carbon::parse($trial->expired)->copy()->endOfDay();
                $diffDays = $expired->diffInDays(Carbon::now());
                $diffHours = $expired->diffInHours(Carbon::now())/24;
                //Nếu ngày hết hạn là ngày hiện tại thì set số ngày còn lại là 1
                $trialDays = $expired < Carbon::now() ? 0 : $diffHours < 1 && $diffHours > 0 ? 1 : $diffDays ;
                session(['trialDays' => $trialDays]);
            }else{
                $trialDays =  session('trialDays');
            }
            
            if($trialDays <= 0){
                if(!$request->ajax() && !$request->is('home'))
                    return redirect('home');
            }
        }
        return $next($request);
    }
}
