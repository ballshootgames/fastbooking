<?php

namespace App\Http\Middleware;

use App\Trial;
use Closure;
use App\Permission;
use Gate;

class Subdomain
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
	    $subdomain = $request->route('subdomain');
	    if(empty($subdomain)) return $next($request);

	    \URL::defaults(['subdomain' => $subdomain]);
		//remove param subdomain on controller function ($subdomain, $id)
	    $request->route()->forgetParameter('subdomain');		
//		if(!\session()->has($subdomain))
//		{
//			$account = Trial::where('subdomain', $subdomain)->first();
//			if(!$account) abort(404);
//			session([$subdomain=>$account->db_name]);
//		}
        $account = Trial::where('subdomain', $subdomain)->first();
        if(!$account) abort(404);

		//config database by subdomain
		config(["database.connections.$subdomain" => [
			"driver" => "mysql",
			"host" => config('database.connections.mysql.host'),
			"port" => config('database.connections.mysql.port'),
			"database" => $account->db_name,
			"username" => config('database.connections.mysql.username'),
			"password" => config('database.connections.mysql.password'),
			"charset" => config('database.connections.mysql.charset'),
			"collation" => config('database.connections.mysql.collation'),
			"prefix" => config('database.connections.mysql.prefix'),
			"strict" => config('database.connections.mysql.strict'),
			"engine" => config('database.connections.mysql.engine')
		]]);
//	    \Config::set('database.default', $subdomain);
		\DB::setDefaultConnection($subdomain);

	    \DB::purge($subdomain);
		\DB::reconnect($subdomain);
//	    dump(\DB::getDatabaseName());

        try {
            if (\Schema::hasTable('permissions')) {
                // Dynamically register permissions with Laravel's Gate.
                foreach ($this->getPermissions() as $permission) {
                    Gate::define($permission->name, function ($user) use ($permission) {
                        return $user->hasPermission($permission);
                    });
                }
            }
        } catch (\Illuminate\Database\QueryException $ex) {
            return;
        }

        return $next($request);
    }

    protected function getPermissions()
    {
        return Permission::with('roles')->get();
    }
}
