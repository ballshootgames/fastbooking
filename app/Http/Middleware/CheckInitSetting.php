<?php

namespace App\Http\Middleware;

use Closure;
use App\Setting;

class CheckInitSetting
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //Kiểm tra nếu subdomain mới cho vào kiểm tra
        if(!empty(\URL::getDefaultParameters()['subdomain']))
        {
            if (\Auth::check() && !strpos($request->getRequestUri(), 'admin/settings')){
                $settingThemeName = Setting::where('key', 'theme_name')->first();
                if (empty($settingThemeName)){
                    return redirect('admin/settings');
                }else{
                    if (empty($settingThemeName->value)) return redirect('admin/settings');

                    $setting = Setting::where('key', '!=', 'theme_name')->first();
                    if (empty($setting))
                        return redirect('admin/settings');
                }
            }
//            $settingThemeName = Setting::where('key', 'theme_name')->first();
//            //Kiểm tra nếu chưa set theme
//            if (empty($settingThemeName)){
//                if (\Auth::check() && !strpos($request->getRequestUri(), 'admin/set-theme')){
//                    return redirect('admin/set-theme');
//                }
//            }else{
//                //Nếu setting chưa có value
//                if (empty($settingThemeName->value) && !strpos($request->getRequestUri(), 'admin/set-theme')){
//                    return redirect('admin/set-theme');
//                }
//                //Nếu Chưa thiết lập các Setting (Ngoại trừ tham số theme_name)
//                $setting = Setting::where('key', '!=', 'theme_name')->first();
//                if (empty($setting) && \Auth::check() && !strpos($request->getRequestUri(), 'admin/settings')){
//                    return redirect('admin/settings');
//                }
//            }
        }
        return $next($request);
    }
}
