<?php

namespace App;


use Carbon\Carbon;
use Modules\Booking\Entities\Booking;

class ChartJs
{
	const COLOR = [
		"red" => '#f44336',
		"green" => '#66bb6a',
		"blue" => '#55acee',
		"aqua" => '#26c6da',
		"navy" => "#001F3F",
		"teal" => "#39CCCC",
		"olive" => "#01FF70",
		"orange" => "#FF851B",
		"fuchsia" => "#F012BE",
		"purple" => "#605ca8",
		"maroon" => "#D81B60",
		"black" => "#111",
	];

	public static function init(){
		echo '<script type="text/javascript" src="'.asset('plugins/chartjs/chartjs.min.js').'"></script>';
	}

	public static function dashboardLineCharBooking30DaysAgo($from = null, $to = null){
		/*$user = \Auth::user();
		if($user->isAdminCompany()) return;*/
		?>
		<!--<div class="box box-success">
			<div class="box-header with-border">
				<i class="fa fa-line-chart"></i>
				<div class="box-title text-bold">Booking 30 ngày gần đây</div>
			</div>
			<div class="box-body">
				<?php /*self::lineChartBooking30DaysAgo() */?>
			</div>
		</div>-->
        <?php self::lineChartBooking30DaysAgo($from, $to) ?>
<?php
	}
	public static function lineChartBooking30DaysAgo($from = null, $to = null){
		/*$user = \Auth::user();
		if(!$user->isAdminCompany()) return;*/
		//prepare
        $date30Ago = empty($from) ? date('Y-m-d', strtotime('-29 days')) : $from;
        $from = Carbon::parse($date30Ago);
        $today = empty($to) ? date('Y-m-d') : $to;
		$to = Carbon::parse($today);
		//chart data
		$dates = Utils::generateDateRange($from, $to);
        $dateBooking30DaysAgo['datasets'][0] = [
            'label' => __('booking::bookings.services'),
            'borderColor' => ChartJs::COLOR['red'],
            'backgroundColor' => ChartJs::COLOR['red'],
            'fill' => false,
            'data' => []
        ];
		/*$dateBooking30DaysAgo = [
			'labels' => $dates,
			'datasets' => [
				[
					'label' => __('bookings.journeys'),
					'borderColor' => self::COLOR['green'],
					'backgroundColor' => self::COLOR['green'],
					'fill' => false,
					'data' => []
				],
				[
					'label' => __('bookings.tours'),
					'borderColor' => self::COLOR['blue'],
					'backgroundColor' => self::COLOR['blue'],
					'fill' => false,
					'data' => []
				]
			]
		];*/
        //get data
		/*$bookBus30DaysAgo = Booking::reportBookingByDate(Booking::REPORT_TYPE['NUMBER'],'journeys', $date30Ago, $today);
		$bookTours30DaysAgo = Booking::reportBookingByDate(Booking::REPORT_TYPE['NUMBER'],'tours', $date30Ago, $today);*/
        $type_date = config('booking.type_report.type_date');
		$report_company = config('booking.report_btn_total');
		$financeBooking = Booking::reportBooking(Booking::REPORT_TYPE['FINANCE'], $type_date,'tour', $date30Ago, $today, null, null, null, 'tour', $report_company);
		//prepare data chart
		$datesFormat = [];
		foreach ($dates as $date) {
			$datesFormat[] = Carbon::parse($date)->format(config('settings.format.date'));
			/*$dateBooking30DaysAgo['datasets'][0]['data'][] = ! empty( $bookBus30DaysAgo[ $date ] ) ? $bookBus30DaysAgo[ $date ] : 0;
			$dateBooking30DaysAgo['datasets'][1]['data'][] = ! empty( $bookTours30DaysAgo[ $date ] ) ? $bookTours30DaysAgo[ $date ] : 0;*/
            $dateBooking30DaysAgo['datasets'][0]['data'][] = ! empty( $financeBooking[ $date ] ) ? $financeBooking[ $date ] : 0;
		}

		$dateBooking30DaysAgo['labels'] = $datesFormat;

		self::lineChart('30daysAgo', $dateBooking30DaysAgo);
	}
	public static function lineChart($id, $data, $height = '50vh'){
		print('
			<div style="position: relative; height:'.$height.';">
				<canvas id="'.$id.'"></canvas>
			</div>
				<script type="text/javascript">
				var numberWithCommas = function(x) {
                  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                };
		        var _30daysAgo = document.getElementById("'.$id.'");
		        var myLineChart = new Chart(_30daysAgo, {
		            type: "line",
		            data: '.json_encode($data).',
		            options: {
		                legend :{
                          display: false //Ẩn Labels
                        },
		                maintainAspectRatio: false,
		                responsive: true,
		//                title: {
		//                    display: true,
		//                    text: "Booking 30 ngày gần đây"
		//                },
		                tooltips: {
		                    mode: "index",
		                    intersect: false,
		                    callbacks: {
                                label: function(tooltipItem, data) {
                                    var label = data.datasets[tooltipItem.datasetIndex].label || \'\';
                
                                    if (label) {
                                        label += \': \';
                                    }
                                    label += numberWithCommas(tooltipItem.yLabel);
                                    return label;
                                }
                            }
		                },
		                hover: {
		                    mode: "nearest",
		                    intersect: true
		                },
		                scales: {
                            yAxes: [{
                                ticks: {
                                    // Include a dollar sign in the ticks
                                    callback: function(value, index, values) {
                                        return numberWithCommas(value);
                                    }
                                }
                            }]
                        }
		//                ,
		//                scales: {
		//                    xAxes: [{
		//                        display: true,
		//                        scaleLabel: {
		//                            display: true,
		//                            labelString: "Ngày"
		//                        }
		//                    }],
		//                    yAxes: [{
		//                        display: true,
		//                        scaleLabel: {
		//                            display: true,
		//                            labelString: "Đặt vé"
		//                        }
		//                    }]
		//                }
		            }
		        });
		
			</script>
		');
	}
}
