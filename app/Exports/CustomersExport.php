<?php

namespace App\Exports;

use App\Customer;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;

class CustomersExport implements FromCollection,WithHeadings,ShouldAutoSize,WithMapping,WithEvents
{

    private $customers;

    public function __construct($customers)
    {
        $this->customers = $customers;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $data = $this->customers;
        $dataBuild = [];
        foreach ($data as $index => $row){
            $dataBuild[$index] = $row;
            $dataBuild[$index]->index = $index + 1;
        }

        return (collect($dataBuild));
    }

    public function headings(): array
    {
        return ['Stt', 'Đơn vị', 'Họ tên', 'Chức vụ', 'Số điện thoại', 'Email', 'Facebook', 'Ngày sinh',
            'Tỉnh', 'Thành phố/ huyện/ quận', 'Xã/ phường', 'Địa chỉ'];
    }

    public function map($item): array
    {
        $value = [
            $item->index,
            optional($item->office)->name,
            $item->name,
            $item->position,
            $item->phone,
            $item->email,
            $item->facebook,
            Carbon::parse($item->birthday)->format(config('settings.format.date')),
            optional(optional(optional($item->ward)->district)->city)->name,
            optional(optional($item->ward)->district)->name,
            optional($item->ward)->name,
            $item->address
        ];
        return $value;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $count = $this->customers->count();
                $count++;//them header
                $allColumn = ['A','B','C','D','E','F','G','H','I','J','K','L'];
                //set font header
                $cellRange = 'A1:L1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14)->setBold(true);
                $styleArray = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                    ]
                ];
                foreach ($allColumn as $col){
                    $event->sheet->getStyle($col."1:$col$count")->applyFromArray($styleArray);
                }
            },
        ];
    }
}
