<?php

namespace App\Events;

use App\Trial;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class RegisterTrial
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $trial;
    /**
     * Create a new event instance.
     * @param  Trial $trial
     * @return void
     */
    public function __construct(Trial $trial)
    {
        $this->trial = $trial;
    }

}
