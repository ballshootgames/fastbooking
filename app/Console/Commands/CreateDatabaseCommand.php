<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CreateDatabaseCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
	protected $signature = 'db:create {dbname}';


	/**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command creates a new database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
	    try {
		    $dbname = $this->argument('dbname');
		    $pdo = \DB::connection()->getPdo();

		    $pdo->exec(sprintf(
			    'CREATE DATABASE IF NOT EXISTS %s CHARACTER SET utf8 COLLATE utf8_general_ci;',
			    $dbname
		    ));
			$this->info(sprintf('Successfully created %s database', $dbname));
	    } catch (\Exception $e) {
			$this->error($e->getMessage());
	    }
    }
}
