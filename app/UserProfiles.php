<?php

namespace App;

use App\Traits\ImageResize;
use Illuminate\Database\Eloquent\Model;

class UserProfiles extends Model
{
    use BaseModel, ImageResize;

	public static $imageFolder = "avatar";
	public static $imageMaxHeight = 100;
	public static $imageMaxWidth = 100;

    protected static $recordEvents = ['created', 'updated'];
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    protected $primaryKey = 'user_id';
    /**
     * Get the user record associated with the profile
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo("App/User");
    }
}
