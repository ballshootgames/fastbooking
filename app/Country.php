<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Country extends Model
{
    use Sortable, SoftDeletes;

    protected $table = "countries";

    protected $dates = ['deleted_at'];
    public $sortable = [
        'id',
        'name',
        'capital',
        'phonecode',
        'currency',
        'arrange',
        'active',
        'created_at'
    ];

    protected $primaryKey = "id";

    protected $fillable = ['name', 'iso3', 'iso2', 'phonecode', 'capital','currency', 'arrange', 'active'];

    public function cities(){
        return $this->hasMany('App\City');
    }

    public function user(){
        return $this->belongsTo('App\User', 'creator_id');
    }

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model){
            $model->creator_id = \Auth::user()->id;
            $model->updator_id = \Auth::user()->id;
        });
        self::updating(function ($model){
            $model->updator_id = \Auth::user()->id;
        });
    }
}