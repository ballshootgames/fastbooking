<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $table = 'modules';

    protected $primaryKey = 'id';

    protected $fillable = ['name', 'alias', 'description', 'active'];
	//TODO: thử nghiệm
    public static function active($alias){
    	$module = Module::where('alias', $alias)->first();
    	if(!$module) return true;
    	return $module->active;
    }
}
