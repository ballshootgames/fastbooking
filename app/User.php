<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Storage;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
	use Notifiable, HasRoles, Sortable, SoftDeletes, HasApiTokens, BaseModel;

	public $sortable = [
		'id',
		'name',
		'email',
		'active',
		'username',
		'company_id'
	];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name', 'email', 'password', 'active', 'username', 'company_id', 'user_title_id', 'user_department_id'
	];
	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = ['deleted_at'];
	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	/**
	 * Get the profile record associated with the user.
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function profile(){
		return $this->hasOne('App\UserProfiles');
	}
//	public function agent() {
//		return $this->belongsTo('App\Agent');
//	}
//    public function branch() {
//        return $this->belongsTo('App\Branch','branch_id');
//    }
    public function user_title() {
	    return $this->belongsTo('App\UserTitle');
    }
    public function user_department(){
	    return $this->belongsTo('App\UserDepartment');
    }
    public function company(){
        return $this->belongsTo('App\Company');
    }
	public function tour_managers(){
        return $this->belongsToMany('Modules\Tour\Entities\Tour', 'tour_managers');
    }
    public function tour(){
	    return $this->hasMany('Modules\Tour\Entities\Tour', 'creator_id', 'id');
    }
    public function guides(){
        return $this->hasMany('Modules\Tour\Entities\TourGuide');
    }

    public function news(){
	    return $this->hasMany('Modules\News\Entities\News');
    }

	/**
	 * Scope a query to only include active users.
	 *
	 * @param \Illuminate\Database\Eloquent\Builder $query
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeActive($query)
	{
		return $query->where('active', Config("settings.active"));
	}
	/**
	 * Show avatar
	 * @return string|void
	 */
	public function showAvatar($attrs = null,$default = ""){
		if(isset($this->profile) && !empty($this->profile->avatar)){
			$avatar = $this->profile->avatar;
			if(\Storage::exists($avatar))
				return '<img alt="avatar" class="'.$attrs["class"].'" src="'.asset(\Storage::url($avatar)).'" />';
		}
		if(!empty($default)) return '<img alt="avatar" class="'.$attrs["class"].'" src="'.$default.'" />';
		return;
	}

	public function findForPassport($username) {
		return $this->where('username', $username)->where('active', Config("settings.active"))->first();
	}

	/**
	 * Token api
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function tokens() {
		return $this->hasMany('Laravel\Passport\Token');
	}

	/**
	 * Revoke tokens - this user
	 */
	public function revokeAllTokens() {
		$tokens = $this->tokens()->where('revoked', 0)->get();
		foreach ($tokens as $token){
			$token->revoke();
		}
	}
	public function deviceNotifies() {
		return $this->hasMany('App\DeviceNotify');
	}
	public function roleBelongToCompany(){
		return $this->hasRole(config('settings.roles.company_1'))
		       || $this->hasRole(config('settings.roles.company_2'))
		       || $this->hasRole(config('settings.roles.company_3'));
	}
	public function roleBelongToAgent(){
		return $this->hasRole(config('settings.roles.agent_1')) || $this->hasRole(config('settings.roles.agent_2'));
	}
	public function roleBelongToBranch(){
	    return $this->hasRole(config('settings.roles.branch_1')) || $this->hasRole(config('settings.roles.branch_2'));
    }

	public function isAdminCompany(){
		return $this->hasRole(config('settings.roles.company_1'));
	}
	public function isAdminAgent(){
		return $this->hasRole(config('settings.roles.agent_1'));
	}
	public function isAdminBranch(){
	    return $this->hasRole(config('settings.roles.branch_1'));
    }
	public function isCompanyManagerBooking(){
		return $this->hasRole(config('settings.roles.company_3'));
	}


	public function isEmployeeCompany(){
		return $this->hasRole(config('settings.roles.company_2'));
	}
	public function isEmployeeAgent(){
		return $this->hasRole(config('settings.roles.agent_2'));
	}
	public function isEmployeeBranch(){
	    return $this->hasRole(config('settings.roles.branch_2'));
    }

	public static function boot()
	{
		parent::boot();
		static::creating(function ($user) {
			static::bootCreatingByRole($user);
		});

		static::updated(function ($user) {
			//when inactive user, revoke all tokens of this user
			if($user->active === \Config::get("settings.inactive"))
				$user->revokeAllTokens();
		});

		static::saving(function ($user) {

		});
		static::deleted(function ($user) {
//			$user->revokeAllTokens();
			//check xóa hoàn toàn trong CSDL
			if ($user->isForceDeleting()) {
				$profile = $user->profile;
				if ( ! empty( $profile->avatar ) ) {
					\Storage::delete( $profile->avatar );
				}
			}
		});
	}

	/**
	 * Users belong to my company (not agent)
	 * @return mixed
	 */
	public static function getUsersByOnlyCompany(){
//		return User::whereNull('agent_id')->whereNull('branch_id')->pluck( 'name', 'id' );
		return User::pluck( 'name', 'id' );
	}

	/**
	 * Users belong to my agent
	 * @return mixed
	 */
//	public static function getUsersByOnlyAgent(){
//		return User::where( 'agent_id', '=', \Auth::user()->agent_id )
//		           ->pluck( 'name', 'id' );
//	}

    /**
     * Users belong to my branch
     * @return mixed
     */
//    public static function getUsersByOnlyBranch(){
//        return User::where( 'branch_id', '=', \Auth::user()->branch_id )
//            ->pluck( 'name', 'id' );
//    }
}
