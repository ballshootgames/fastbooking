<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    protected $table = 'sessions';

    protected $primaryKey = 'id';

    protected $fillable = ['model_type', 'model_id', 'last_activity', 'user_id'];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public static function delSession($model)
    {
        Session::where('model_id', $model->id)->delete();
    }
}
