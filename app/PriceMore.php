<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PriceMore extends Model
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'price_more';
	public $timestamps = false;

	/**
	 * The database primary key value.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id';

	/**
	 * Attributes that should be mass-assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['price', 'priceable_id', 'priceable_type'];


}
