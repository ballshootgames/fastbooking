<?php

namespace Modules\Tour\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class GuideResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "name" => optional($this->user)->name,
            "phone" => optional(optional($this->user)->profile)->phone,
        ];
    }
}
