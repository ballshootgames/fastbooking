<?php

namespace Modules\Tour\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class TourResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "code" => $this->code,
            "name" => $this->name,
            "journey" => $this->journey,
            "description" =>  $this->description,
            "transport" => $this->transport,
            "price" => $this->getPriceBooking(),
            "creator_id" =>  $this->creator_id,
//            "number_customers" => $this->customersNumber ? $this->customersNumber : 0,
//            "tmp_bookings_count" => $this->tmp_bookings_count ? $this->tmp_bookings_count : 0,
            "guide" => new GuideResource($this->guides->first())
        ];
    }
}
