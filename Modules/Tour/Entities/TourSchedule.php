<?php

namespace Modules\Tour\Entities;

use App\Traits\ImageResize;
use Illuminate\Database\Eloquent\Model;

class TourSchedule extends Model
{
	use ImageResize;
	public static $imageFolder = "tours-schedule";
	public static $imageMaxWidth = 300;
	public static $imageMaxHeight = 200;

	protected $table = 'tour_schedules';

    protected $primaryKey = 'id';

    protected $fillable = ['day', 'name', 'image', 'content', 'tour_id'];

    public function tour(){
        return $this->belongsTo('Modules\Tour\Entities\Tour');
    }

    public function cities(){
        return $this->belongsToMany('Modules\Tour\Entities\TourProperty', 'schedule_cities','tour_schedule_id', 'city_id');
    }

    public static function getListArrSchedule($day){
        $arr = [];
        if($day === 0.5) {
            $arr[] = ['day' => $day];
        }else{
            for ($i = 1; $i <= $day; $i++){
                $arr[] = ['day' => $i];
            }
        }
        return $arr;
    }

    public static function updateTourScheduleWithDay($oldDayNumber = null, $newDayNumber, $tourId){
        if ($oldDayNumber == null) $oldDayNumber = 0;
        if ($oldDayNumber == $newDayNumber) return;
        $arr = [];
        if ($oldDayNumber < $newDayNumber){
            if ($oldDayNumber == 0.5){
                \DB::table('tour_schedules')->where('day',$oldDayNumber)->delete();
                $oldDayNumber = 0;
            }
            $number = $newDayNumber - $oldDayNumber;
            for ($i = 1; $i <= $number; $i++){
                $arr[] = ['day' => $oldDayNumber + $i];
            }

            $tour = Tour::find($tourId);
            $tour->schedules()->createMany($arr);
        }else{
            if ($newDayNumber == 0.5){
                $arr[] = ['day' => $newDayNumber];
                $tour = Tour::find($tourId);
                $tour->schedules()->createMany($arr);
                $newDayNumber = 0;
            }
            $number = $oldDayNumber - $newDayNumber;
            $listScheduleId = TourSchedule::where('tour_id', $tourId)->orderBy('day', 'desc')->take($number)->pluck('id');
            $del = \DB::table('tour_schedules')->whereIn('id', $listScheduleId)->delete();
            return $del;
        }
    }

    public static function getListTourSchedules($idTour){
        return TourSchedule::where('tour_id', $idTour)->orderBy('day', 'ASC')->get();
    }
}
