<?php

namespace Modules\Tour\Entities;

use Illuminate\Database\Eloquent\Model;

class TourLinked extends Model
{
    protected $table = 'tour_linked';
    protected $fillable = ['tour_id', 'tour_right', 'tour_related'];

    public function tour(){
        return $this->belongsTo('Modules\Tour\Entities\Tour');
    }
}
