<?php

namespace Modules\Tour\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class TourMedia extends Model
{
    protected $table = 'tour_media';

    protected $primaryKey = 'id';

    protected $fillable = ['name', 'file_path', 'tour_id'];

    public function tour(){
        return $this->belongsTo('Modules\Tour\Entities\Tour');
    }

    static public function saveMediaFiles($file)
    {
        if(empty($file)) return;
        $folder = "/file/sites/".\URL::getDefaultParameters()['subdomain']."/tour-media";
        if(!\Storage::disk(config('filesystems.disks.public.visibility'))->has($folder)){
            \Storage::makeDirectory(config('filesystems.disks.public.visibility').$folder);
        }
        $timestamp = Carbon::now()->toDateTimeString();
        $fileExt = $file->getClientOriginalExtension();
        $filename = Str::slug(basename($file->getClientOriginalName(), '.'.$fileExt));
        $pathImage = str_replace([' ', ':'], '-', $timestamp. '-' .$filename.'.'.$fileExt);


        $path = public_path('/storage').$folder;
        $file->move($path, $pathImage);
        return [
            'type' => $fileExt,
            'name' => config('filesystems.disks.public.visibility').$folder."/".$pathImage
        ];
    }

    public static function deleteFile($file){
        if (!empty($file) && \Storage::exists($file) && strpos($file, \URL::getDefaultParameters()['subdomain'])){
            \Storage::delete($file);
        }
    }
}
