<?php

namespace Modules\Tour\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Kyslik\ColumnSortable\Sortable;

class Supplier extends Model
{
    use Sortable;

    protected $table = 'suppliers';

    protected $primaryKey='id';

    protected $sortable = ['name'];

    protected $fillable = ['name','phone','address','email'];

    public function tours(){
        return $this->belongsToMany('Modules\Tour\Entities\Tour','tour_suppliers');
    }

    static public function saveSupplierFiles($file)
    {
        if(empty($file)) return;
        $folder = "/file/sites/".\URL::getDefaultParameters()['subdomain']."/suppliers";
        if(!\Storage::disk(config('filesystems.disks.public.visibility'))->has($folder)){
            \Storage::makeDirectory(config('filesystems.disks.public.visibility').$folder);
        }
        $timestamp = Carbon::now()->toDateTimeString();
        $fileExt = $file->getClientOriginalExtension();
        $filename = Str::slug(basename($file->getClientOriginalName(), '.'.$fileExt));
        $pathImage = str_replace([' ', ':'], '-', $timestamp. '-' .$filename.'.'.$fileExt);


        $path = public_path('/storage').$folder;
        $file->move($path, $pathImage);
        return [
            'type' => $fileExt,
            'name' => config('filesystems.disks.public.visibility').$folder."/".$pathImage
        ];
    }

    public static function deleteFile($file){
        if (!empty($file) && \Storage::exists($file) && strpos($file, \URL::getDefaultParameters()['subdomain'])){
            \Storage::delete($file);
        }
    }
}
