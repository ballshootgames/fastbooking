<?php

namespace Modules\Tour\Entities;

use Illuminate\Database\Eloquent\Model;

class TourDeposit extends Model
{

    protected $table = 'tour_deposits';

    protected $primaryKey = 'id';

    protected $fillable = ['name', 'description', 'value', 'tour_id'];
}
