<?php

namespace Modules\Tour\Entities;

use App\Traits\ImageResize;
use Illuminate\Database\Eloquent\Model;

class TourGallery extends Model
{
	use ImageResize;
	public static $imageFolder = "tour_gallery";
	public static $imageMaxWidth = 1024;
	public static $imageMaxHeight = 1024;

    protected $table = 'tour_gallery';

    protected $primaryKey = 'id';

    protected $fillable = ['name', 'image', 'tour_id', 'image_order'];

    public function tour(){
        return $this->belongsTo('Modules\Tour\Entities\Tour');
    }
}
