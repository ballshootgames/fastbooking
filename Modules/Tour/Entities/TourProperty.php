<?php

namespace Modules\Tour\Entities;

use App\Traits\ImageResize;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class TourProperty extends Model
{
    use Sortable, SoftDeletes, ImageResize;

	public static $imageFolder = "tour-properties";
	public static $imageMaxHeight = 512;
	public static $imageMaxWidth = 512;

    protected $table = 'tour_properties';
    protected $dates = ['deleted_at'];
    public $sortable = [
        'code',
        'name',
        'slug',
        'active',
        'prop_order',
        'prop_top',
        'created_at'
    ];

    protected $primaryKey = 'id';

//    public $timestamps = false;

    protected $fillable = ['code', 'name', 'description', 'type', 'slug', 'image', 'active', 'prop_order',
                            'prop_top', 'type_id', 'parent_id', 'show_home','meta_keyword'];

    public function user(){
        return $this->belongsTo('App\User', 'creator_id');
    }

    public function userUpdate(){
        return $this->belongsTo('App\User', 'updator_id');
    }

    public function tours(){
        return $this->belongsToMany('Modules\Tour\Entities\Tour', 'tour_property_values');
    }

    public function tour_type(){
        return $this->belongsTo('Modules\Tour\Entities\TourProperty', 'type_id');
    }

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model){
            $model->creator_id = \Auth::user()->id;
            $model->updator_id = \Auth::user()->id;
        });
        self::updating(function ($model){
            $model->updator_id = \Auth::user()->id;
        });
    }
}
