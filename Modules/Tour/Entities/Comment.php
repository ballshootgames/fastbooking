<?php

namespace Modules\Tour\Entities;

use App\Traits\ImageResize;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Comment extends Model
{
    use Sortable,ImageResize;
    public static $imageFolder = "comments";
    public static $imageMaxWidth = 130;
    public static $imageMaxHeight = 130;
    protected $table = 'comments';

    protected $primaryKey = 'id';

    protected $sortable = [
        'user_name',
        'user_email',
        'tour_id',
        'updated_at'
    ];

    protected $fillable = ['user_name', 'user_email', 'comment', 'tour_id', 'avatar', 'active', 'arrange'];

    public function tour(){
        return $this->belongsTo('Modules\Tour\Entities\Tour', 'tour_id');
    }
}
