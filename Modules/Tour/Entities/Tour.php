<?php

namespace Modules\Tour\Entities;

use App\Setting;
use App\Traits\ImageResize;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Tour extends Model
{
    use Sortable, SoftDeletes, ImageResize;
	public static $imageFolder = "tours";
	public static $imageMaxWidth = 1024;
	public static $imageMaxHeight = 1024;

    protected $table = 'tours';
    protected $dates = ['deleted_at'];
    public $sortable = [
        'code',
        'name',
        'price',
        'created_at'
    ];

    protected $primaryKey = 'id';

    protected $fillable = ['prefix_code', 'code', 'name', 'day_number', 'night_number', 'tour_status', 'limit',
        'limit_start_date', 'limit_end_date', 'departure_date', 'price', 'intro', 'note', 'departure_schedule',
        'start_city', 'hot', 'slug', 'image', 'price_children', 'price_baby', 'active', 'price_discount',
        'installment','meta_keyword','departure_type', 'is_promotion'];

    public function user(){
        return $this->belongsTo('App\User', 'creator_id');
    }

    public function userUpdate(){
        return $this->belongsTo('App\User', 'updator_id');
    }

    public function bookings(){
        return $this->morphToMany('Modules\Booking\Entities\Booking', 'bookingable', 'booking_detail');
    }

    public function properties(){
        return $this->belongsToMany('Modules\Tour\Entities\TourProperty', 'tour_property_values');
    }

    public function property() {
        return $this->belongsTo('Modules\Tour\Entities\TourProperty', 'start_city');
    }

    public function suppliers(){
        return $this->belongsToMany('Modules\Tour\Entities\Supplier','tour_suppliers')->withPivot('files');
    }

    public function tourLinked(){
        return $this->hasOne('Modules\Tour\Entities\TourLinked');
    }

    public function schedules(){
        return $this->hasMany('Modules\Tour\Entities\TourSchedule');
    }

    public function deposits(){
        return $this->hasMany('Modules\Tour\Entities\TourDeposit');
    }

    public function city(){
        return $this->belongsTo('App\City', 'start_city_id');
    }

    public function comments(){
        return $this->hasMany('Modules\Tour\Entities\Comment', 'tour_id');
    }

    public function galleries(){
        return $this->hasMany('Modules\Tour\Entities\TourGallery');
    }

    public function medias(){
        return $this->hasMany('Modules\Tour\Entities\TourMedia');
    }

    /**
     * Lấy số lượng khách hàng đã đặt tour này
     * Get number customers of this Tour
     * @return int
     */
    public function getCustomersNumberAttribute() {
        return $this->bookings->sum( 'totalNumber' );
    }

    /**
     * Get price booking - by agent or not agent
     * Lấy giá bán - giá theo đại lý hoặc ko
     * @return mixed
     */
    public function getPriceBooking(){
        return $this->price;
    }

    public static function getListDay($par = 1){
        //1: ngày; 0: đêm
        $par == 1 ? $arr = [0.5] : $arr = [0];
        for ($i = 1; $i <= 100; $i++ ){
            $arr[] = $i;
        }
        return $arr;
    }

    public function scopeCheckLimit($query){
        $query = $query->whereNull('limit')
                        ->orWhere('limit', '<>', 0);
        return $query;
    }

    /*autoCopy*/
    public static function autoCopy($field, $code){
        $tours = Tour::pluck($field)->toArray();
        if (in_array($code, $tours)){
            $newCode = '';
            $copy = $field === 'name' ? ' - Copy' : '';
            for ($i = 1; $i <= 100; $i++){
                $newCode = $code . $copy . '('. $i .')';
                if (!in_array($newCode, $tours)) break;
            }
            return $newCode;
        } else {
            return $code;
        }
    }

    /*autoCode*/
    public static function autoCode($prefixCode = null){
        $settings = Setting::allConfigsKeyValue();
        switch ($settings['drpAscNumber']){
            case 'code_prefix':
                $count = \DB::table('tours')->selectRaw("MAX(CAST(code AS UNSIGNED)) as code")->where('prefix_code','LIKE', "%$prefixCode%" )->pluck('code')->toArray()[0];
                break;
            default:
                $count = \DB::table('tours')->selectRaw("MAX(CAST(code AS UNSIGNED)) as code")->pluck('code')->toArray()[0];
        }
        if (empty((int)$count)){
            $count = 1;
        }else{
            $count += 1;
        }
        $code = str_pad($count, (int)$settings['code_num'], '0', STR_PAD_LEFT);
        return $code;
    }
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model){
            $model->creator_id = \Auth::user()->id;
            $model->updator_id = \Auth::user()->id;
        });
        self::updating(function ($model){
            $model->updator_id = \Auth::user()->id;
        });
    }
}
