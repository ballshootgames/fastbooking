<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
$subdomain = \URL::getDefaultParameters()?\URL::getDefaultParameters()['subdomain']:"";
$appRoot = config('app.root').".";
if (\URL::getRequest()->getHost()!=$appRoot.config('app.domain') || !empty($subdomain))
    $appRoot = "";

Route::domain('{subdomain}.'.$appRoot.config('app.domain'))->group(function () {
    Route::group(['prefix' => 'admin/control/', 'middleware' => 'subdomain'], function (){
        Route::group(['middleware' => 'auth'], function (){
            Route::resource('tours', 'ToursController');
            Route::resource('tour-galleries', 'TourGalleryController');
            Route::post('delete-gallery/{id}', 'TourGalleryController@deleteGallery');
            Route::resource('tour-media', 'TourMediaController');
            Route::post('delete-media/{id}', 'TourMediaController@deleteMedia');
            Route::resource('tour-schedule', 'TourScheduleController');
            Route::resource('tour-properties', 'TourPropertyController');
            Route::resource('comments', 'CommentController');
            Route::resource('suppliers', 'SupplierController');
            Route::get('tours/ajax/{action}', 'AjaxController@index');
            Route::post('tours/{id}/copy', 'ToursController@getCopyTour');
            Route::post('tour-schedule-upload/{id}', 'TourScheduleController@uploadFile');
            Route::post('tour-gallery-delete', 'TourGalleryController@deleteImage');
            Route::post('tour-media-delete', 'TourMediaController@deleteFile');
        });
        Route::post('store-comment', 'CommentController@store');
    });
});
