<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->float('day')->unsigned();
            $table->string('name')->nullable();
            $table->string('image')->nullable();
            $table->text('content')->nullable();
            $table->integer('tour_id')->unsigned();
            $table->foreign('tour_id')->references('id')->on('tours')->onDelete('cascade');

            $table->timestamps();
        });

        Schema::create('schedule_cities', function (Blueprint $table){
            $table->integer('city_id')->unsigned();
            $table->integer('tour_schedule_id')->unsigned();

//            $table->foreign('city_id')->references('id')->on('tour_properties')->onDelete('set null');
            $table->foreign('tour_schedule_id')->references('id')->on('tour_schedules')->onDelete('cascade');

            $table->primary(['city_id','tour_schedule_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedule_cities');
        Schema::dropIfExists('tour_schedules');
    }
}
