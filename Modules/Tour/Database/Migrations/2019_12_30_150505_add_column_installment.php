<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInstallment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::table('tours', function (Blueprint $table) {
            $table->tinyInteger('installment')->default(0); //1: active; 0: inactive
            $table->text('deposit')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::table('tours', function (Blueprint $table) {
            $table->drop(['installment','deposit']);
        });
    }
}
