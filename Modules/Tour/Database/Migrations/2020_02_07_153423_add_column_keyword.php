<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnKeyword extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::table('tours', function (Blueprint $table) {
            $table->text('meta_keyword')->nullable();
        });
        \Schema::table('news', function (Blueprint $table) {
            $table->text('meta_keyword')->nullable();
        });
        \Schema::table('tour_properties', function (Blueprint $table) {
            $table->text('meta_keyword')->nullable();
        });
        \Schema::table('news_type', function (Blueprint $table) {
            $table->text('meta_keyword')->nullable();
        });
        \Schema::table('pages', function (Blueprint $table) {
            $table->text('meta_keyword')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::table('tours', function (Blueprint $table) {
            $table->dropColumn('meta_keyword');
        });
        \Schema::table('news', function (Blueprint $table) {
            $table->dropColumn('meta_keyword');
        });
        \Schema::table('tour_properties', function (Blueprint $table) {
            $table->dropColumn('meta_keyword');
        });
        \Schema::table('news_type', function (Blueprint $table) {
            $table->dropColumn('meta_keyword');
        });
        \Schema::table('pages', function (Blueprint $table) {
            $table->dropColumn('meta_keyword');
        });
    }
}
