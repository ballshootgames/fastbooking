<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnImageComments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::table('comments', function (Blueprint $table) {
            $table->string('avatar')->nullable();
            $table->tinyInteger('arrange')->nullable()->unsigned();//sắp xếp
            $table->tinyInteger('active')->default(0);//0: Không duyệt; 1: Duyệt
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::table('comments', function (Blueprint $table) {
            $table->dropColumn(['avatar','arrange','active']);
        });
    }
}
