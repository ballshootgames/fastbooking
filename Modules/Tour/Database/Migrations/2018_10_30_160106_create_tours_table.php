<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tours', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->nullable();
            $table->string('name')->nullable();
            $table->double('price')->unsigned()->nullable();
            $table->double('price_discount')->unsigned()->nullable();

            $table->float('day_number')->unsigned()->nullable();//Số ngày đi
            $table->tinyInteger('night_number')->unsigned()->nullable();//Số đêm đi
            $table->tinyInteger('tour_status')->nullable()->default(1);//1: Mở - 0: Đóng
            $table->integer('limit')->unsigned()->nullable();//null: không có giới hạn - 0: Thông báo hết chỗ - 11, 12, 13: Số lượng limit là 11, 12, 13, mỗi lần booking sẽ - 1
            $table->date('limit_start_date')->nullable();
            $table->date('limit_end_date')->nullable();
            $table->date('departure_date')->nullable();
            $table->text('intro')->nullable();
            $table->text('note')->nullable();
            $table->text('departure_schedule')->nullable();
//            $table->text('includes')->nullable();
//            $table->text('excludes')->nullable();
//            $table->text('cancel_policy')->nullable();
//            $table->text('payment_policy')->nullable();

//            $table->integer('start_city_id')->unsigned()->nullable();
//            $table->foreign('start_city_id')
//                ->references('id')
//                ->on('cities')
//                ->onDelete('set null');
            $table->string('start_city')->nullable();

            $table->tinyInteger('hot')->nullable()->default(0);
            $table->string('slug')->nullable();
            $table->string('image')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('tour_properties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->nullable();
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->string('type')->nullable();
            $table->string('image')->nullable();
            $table->string('slug')->nullable();
            $table->tinyInteger('active')->default(0);
            $table->integer('prop_order')->unsigned()->nullable();
            $table->tinyInteger('prop_top')->default(0);
            $table->integer('type_id')->default(0);

            $table->softDeletes();
            $table->timestamps();

            $table->integer('creator_id')->unsigned()->nullable();
            $table->foreign('creator_id')->references('id')->on('users')->onDelete('set null');
            $table->integer('updator_id')->unsigned()->nullable();
            $table->foreign('updator_id')->references('id')->on('users')->onDelete('set null');
        });

        Schema::create('tour_property_values', function (Blueprint $table) {
            $table->integer('tour_id')->unsigned();
            $table->integer('tour_property_id')->unsigned();

            $table->foreign('tour_id')
                ->references('id')
                ->on('tours')
                ->onDelete('cascade');
            $table->foreign('tour_property_id')
                ->references('id')
                ->on('tour_properties')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tour_property_values');
        Schema::drop('tour_properties');
        Schema::dropIfExists('tours');
    }
}
