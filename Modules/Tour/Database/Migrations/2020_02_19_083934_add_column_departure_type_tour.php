<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDepartureTypeTour extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::table('tours', function (Blueprint $table) {
            $table->tinyInteger('departure_type')->default(0)->nullable(); // 0: hàng ngày
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::table('tours', function (Blueprint $table) {
            $table->dropColumn('departure_type');
        });
    }
}
