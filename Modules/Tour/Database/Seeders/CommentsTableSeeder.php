<?php

namespace Modules\Tour\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        for ($i = 1; $i <= 10; $i++){
            \DB::table('comments')->insert([
                'id' => $i,
                'user_name' => 'Eagle Tourist ' . $i,
                'user_email' => 'abc'.$i.'@gmail.com',
                'tour_id' => $i,
                'comment' => 'Dịch vụ tốt, nhiệt tình ' .$i
            ]);
        }
    }
}
