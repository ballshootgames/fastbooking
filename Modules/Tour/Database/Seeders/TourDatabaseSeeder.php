<?php

namespace Modules\Tour\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class TourDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        \DB::table('booking_properties')->insert([
            ['key' => 'departure_date', 'name' => 'Ngày khởi hành', 'type' => 'date', 'data' => '{"validate":"required","input_attr":{"class":"form-control input-sm","required":"required"},"label_attr":{"class":"control-label label-required"}}', 'module' => 'tour'],
            ['key' => 'picking_up_place', 'name' => 'Địa điểm đón', 'type' => 'text', 'data' => '{"validate":"required","input_attr":{"class":"form-control input-sm","required":"required"},"label_attr":{"class":"control-label"}}', 'module' => 'tour'],
        ]);

        \DB::table('tour_properties')->insert([
            ['id' => '1', 'code' => 'tc_001', 'name' => 'Standard', 'description' => 'Tour tiêu chuẩn', 'type' => 'tour_class', 'slug' => Str::slug('Standard'), 'image' => '', 'active' => '1', 'prop_order' => '1', 'prop_top' => 0],
            ['id' => '2', 'code' => 'tc_002', 'name' => 'Suites', 'description' => 'Tour suites', 'type' => 'tour_class', 'slug' => Str::slug('Suites'), 'image' => '', 'active' => '1', 'prop_order' => '2', 'prop_top' => 0],
            ['id' => '3', 'code' => 'tc_003', 'name' => 'Deluxe', 'description' => 'Tour Deluxe', 'type' => 'tour_class', 'slug' => Str::slug('Deluxe'), 'image' => '', 'active' => '1', 'prop_order' => '3', 'prop_top' => 0],

            ['id' => '4', 'code' => 'tt_001', 'name' => 'Du lịch văn hóa', 'description' => 'Du lịch văn hóa', 'type' => 'tour_type', 'slug' => Str::slug('Du lịch văn hóa'), 'image' => '', 'active' => '1', 'prop_order' => '1', 'prop_top' => 0],
            ['id' => '5', 'code' => 'tt_002', 'name' => 'Du lịch khám phá', 'description' => '', 'type' => 'tour_type', 'slug' => Str::slug('Du lịch khám phá'), 'image' => '', 'active' => '1', 'prop_order' => '2', 'prop_top' => 0],
            ['id' => '6', 'code' => 'tt_003', 'name' => 'Du lịch mạo hiểm', 'description' => '', 'type' => 'tour_type', 'slug' => Str::slug('Du lịch mạo hiểm'), 'image' => '', 'active' => '1', 'prop_order' => '3', 'prop_top' => 0],
            ['id' => '7', 'code' => 'tt_004', 'name' => 'Du lịch sinh thái', 'description' => '', 'type' => 'tour_type', 'slug' => Str::slug('Du lịch sinh thái'), 'image' => '', 'active' => '1', 'prop_order' => '4', 'prop_top' => 0],

            ['id' => '9', 'code' => 'tca_002', 'name' => 'Tour du lịch nước ngoài', 'description' => '', 'type' => 'tour_category', 'slug' => Str::slug('Tour du lịch nước ngoài'), 'image' => '', 'active' => '1', 'prop_order' => '2', 'prop_top' => 0],
            ['id' => '10', 'code' => 'tca_003', 'name' => 'Tour du lịch trong nước', 'description' => '', 'type' => 'tour_category', 'slug' => Str::slug('Tour du lịch trong nước'), 'image' => '', 'active' => '1', 'prop_order' => '3', 'prop_top' => 0],

            ['id' => '11', 'code' => 'tl_001', 'name' => 'Chùm tour HOT tháng 5', 'description' => '', 'type' => 'tour_list','image' => 'public/images/demo/tours/tour-hot-thang.png', 'slug' => Str::slug('Chùm tour HOT tháng 5'), 'active' => '1', 'prop_order' => '1', 'prop_top' => 0],
            ['id' => '12', 'code' => 'tl_002', 'name' => 'Chùm tour trăng mật', 'description' => '', 'type' => 'tour_list', 'image' => 'public/images/demo/tours/tour-trang-mat.png', 'slug' => Str::slug('Chùm tour trăng mật'), 'active' => '1', 'prop_order' => '2', 'prop_top' => 0],
            ['id' => '13', 'code' => 'tl_003', 'name' => 'Du lịch hành hương', 'description' => '', 'type' => 'tour_list', 'image' => 'public/images/demo/tours/du-lich-hanh-huong.png', 'slug' => Str::slug('Du lịch hành hương'), 'active' => '1', 'prop_order' => '3', 'prop_top' => 0],
            ['id' => '14', 'code' => 'tl_004', 'name' => 'Du lịch Mỹ', 'description' => '', 'type' => 'tour_list', 'image' => 'public/images/demo/tours/du-lich-my.png', 'slug' => Str::slug('Du lịch Mỹ'), 'active' => '1', 'prop_order' => '4', 'prop_top' => 0],
            ['id' => '15', 'code' => 'tl_005', 'name' => 'Du lịch trả góp', 'description' => '', 'type' => 'tour_list', 'image' => 'public/images/demo/tours/du-lich-tra-gop.png', 'slug' => Str::slug('Du lịch trả góp'), 'active' => '1', 'prop_order' => '5', 'prop_top' => 0],
            ['id' => '16', 'code' => 'tl_006', 'name' => 'Tết nguyên dán', 'description' => '', 'type' => 'tour_list', 'image' => 'public/images/demo/tours/tet.png', 'slug' => Str::slug('Tết nguyên dán'), 'active' => '1', 'prop_order' => '6', 'prop_top' => 0],
            ['id' => '17', 'code' => 'tl_007', 'name' => 'Tour giảm giá sốc', 'description' => '', 'type' => 'tour_list', 'image' => 'public/images/demo/tours/sale-off.png', 'slug' => Str::slug('Tour giảm giá sốc'), 'active' => '1', 'prop_order' => '7', 'prop_top' => 0],
            ['id' => '18', 'code' => 'tl_008', 'name' => 'Tour giờ chót', 'description' => '', 'type' => 'tour_list', 'image' => 'public/images/demo/tours/hot.png', 'slug' => Str::slug('Tour giờ chót'), 'active' => '1', 'prop_order' => '8', 'prop_top' => 0],
            ['id' => '19', 'code' => 'tl_009', 'name' => 'Tour tiêu chuẩn', 'description' => '', 'type' => 'tour_list', 'image' => 'public/images/demo/tours/tour-tieu-chuan.png', 'slug' => Str::slug('Tour tiêu chuẩn'), 'active' => '1', 'prop_order' => '9', 'prop_top' => 0],
            ['id' => '8', 'code' => 'tl_010', 'name' => 'Tour mới cực hot', 'description' => '', 'type' => 'tour_list', 'image' => 'public/images/demo/tours/tour-hot-thang.png', 'slug' => Str::slug('Tour mới cực hot'), 'active' => '1', 'prop_order' => '10', 'prop_top' => 0],

            ['id' => '20', 'code' => 'HAN', 'name' => 'Hà Nội', 'description' => '', 'type' => 'tour_place', 'image' => 'public/images/demo/destinations/'.Str::slug('Hà Nội').'.jpg', 'slug' => Str::slug('Hà Nội'), 'active' => '1', 'prop_order' => '1', 'prop_top' => 1],
            ['id' => '21', 'code' => 'HLN', 'name' => 'Hạ Long', 'description' => '', 'type' => 'tour_place', 'image' => 'public/images/demo/destinations/'.Str::slug('Hạ Long').'.jpg', 'slug' => Str::slug('Hạ Long'), 'active' => '1', 'prop_order' => '2', 'prop_top' => 0],
            ['id' => '22', 'code' => 'SPN', 'name' => 'Sapa', 'description' => '', 'type' => 'tour_place', 'image' => 'public/images/demo/destinations/'.Str::slug('Sapa').'.jpg', 'slug' => Str::slug('Sapa'), 'active' => '1', 'prop_order' => '3', 'prop_top' => 1],
            ['id' => '23', 'code' => 'NBN', 'name' => 'Ninh Bình', 'description' => '', 'type' => 'tour_place', 'image' => 'public/images/demo/destinations/'.Str::slug('Ninh Bình').'.jpg', 'slug' => Str::slug('Ninh Bình'), 'active' => '1', 'prop_order' => '4', 'prop_top' => 0],
            ['id' => '24', 'code' => 'HBN', 'name' => 'Hòa Bình', 'description' => '', 'type' => 'tour_place', 'image' => 'public/images/demo/destinations/'.Str::slug('Hòa Bình').'.jpg', 'slug' => Str::slug('Hòa Bình'), 'active' => '1', 'prop_order' => '5', 'prop_top' => 0],
            ['id' => '25', 'code' => 'PTN', 'name' => 'Phan Thiết', 'description' => '', 'type' => 'tour_place', 'image' => 'public/images/demo/destinations/'.Str::slug('Phan Thiết').'.jpg', 'slug' => Str::slug('Phan Thiết'), 'active' => '1', 'prop_order' => '6', 'prop_top' => 0],
            ['id' => '26', 'code' => 'CAM', 'name' => 'Campuchia', 'description' => '', 'type' => 'tour_place', 'image' => 'public/images/demo/destinations/'.Str::slug('Campuchia').'.jpg', 'slug' => Str::slug('Campuchia'), 'active' => '1', 'prop_order' => '7', 'prop_top' => 0],
            ['id' => '27', 'code' => 'DLI', 'name' => 'Đà Lạt', 'description' => '', 'type' => 'tour_place', 'image' => 'public/images/demo/destinations/'.Str::slug('Đà Lạt').'.jpg', 'slug' => Str::slug('Đà Lạt'), 'active' => '1', 'prop_order' => '8', 'prop_top' => 1],
            ['id' => '28', 'code' => 'TNN', 'name' => 'Tây Nguyên', 'description' => '', 'type' => 'tour_place', 'image' => 'public/images/demo/destinations/'.Str::slug('Tây Nguyên').'.jpg', 'slug' => Str::slug('Tây Nguyên'), 'active' => '1', 'prop_order' => '9', 'prop_top' => 0],
            ['id' => '29', 'code' => 'HUI', 'name' => 'Huế', 'description' => '', 'type' => 'tour_place', 'image' => 'public/images/demo/destinations/'.Str::slug('Huế').'.jpg', 'slug' => Str::slug('Huế'), 'active' => '1', 'prop_order' => '10', 'prop_top' => 1],
            ['id' => '30', 'code' => 'DAD', 'name' => 'Đà Nẵng', 'description' => '', 'type' => 'tour_place', 'image' => 'public/images/demo/destinations/'.Str::slug('Đà Nẵng').'.jpg', 'slug' => Str::slug('Đà Nẵng'), 'active' => '1', 'prop_order' => '11', 'prop_top' => 1],
            ['id' => '31', 'code' => 'HAD', 'name' => 'Hội An', 'description' => '', 'type' => 'tour_place', 'image' => 'public/images/demo/destinations/'.Str::slug('Hội An').'.jpg', 'slug' => Str::slug('Hội An'), 'active' => '1', 'prop_order' => '12', 'prop_top' => 0],
            ['id' => '32', 'code' => 'QND', 'name' => 'Quảng Nam', 'description' => '', 'type' => 'tour_place', 'image' => 'public/images/demo/destinations/'.Str::slug('Quảng Nam').'.jpg', 'slug' => Str::slug('Quảng Nam'), 'active' => '1', 'prop_order' => '13', 'prop_top' => 0],
            ['id' => '33', 'code' => 'UIH', 'name' => 'Bình Định', 'description' => '', 'type' => 'tour_place', 'image' => 'public/images/demo/destinations/'.Str::slug('Bình Định').'.jpg', 'slug' => Str::slug('Bình Định'), 'active' => '1', 'prop_order' => '14', 'prop_top' => 0],
            ['id' => '34', 'code' => 'QNG', 'name' => 'Quảng Ngãi', 'description' => '', 'type' => 'tour_place', 'image' => 'public/images/demo/destinations/'.Str::slug('Quảng Ngãi').'.jpg', 'slug' => Str::slug('Quảng Ngãi'), 'active' => '1', 'prop_order' => '15', 'prop_top' => 0],
            ['id' => '35', 'code' => 'TBB', 'name' => 'Phú Yên', 'description' => '', 'type' => 'tour_place', 'image' => 'public/images/demo/destinations/'.Str::slug('Phú Yên').'.jpg', 'slug' => Str::slug('Phú Yên'), 'active' => '1', 'prop_order' => '16', 'prop_top' => 0],
            ['id' => '36', 'code' => 'CXR', 'name' => 'Nha Trang', 'description' => '', 'type' => 'tour_place', 'image' => 'public/images/demo/destinations/'.Str::slug('Nha Trang').'.jpg', 'slug' => Str::slug('Nha Trang'), 'active' => '1', 'prop_order' => '17', 'prop_top' => 1],
            ['id' => '37', 'code' => 'TSA', 'name' => 'Ninh Thuận', 'description' => '', 'type' => 'tour_place', 'image' => 'public/images/demo/destinations/'.Str::slug('Ninh Thuận').'.jpg', 'slug' => Str::slug('Ninh Thuận'), 'active' => '1', 'prop_order' => '18', 'prop_top' => 0],
            ['id' => '38', 'code' => 'PTA', 'name' => 'Bình Thuận', 'description' => '', 'type' => 'tour_place', 'image' => 'public/images/demo/destinations/'.Str::slug('Bình Thuận').'.jpg', 'slug' => Str::slug('Bình Thuận'), 'active' => '1', 'prop_order' => '19', 'prop_top' => 0],
            ['id' => '39', 'code' => 'VCS', 'name' => 'Vũng Tàu', 'description' => '', 'type' => 'tour_place', 'image' => 'public/images/demo/destinations/'.Str::slug('Vũng Tàu').'.jpg', 'slug' => Str::slug('Vũng Tàu'), 'active' => '1', 'prop_order' => '20', 'prop_top' => 0],
            ['id' => '40', 'code' => 'SGN', 'name' => 'Hồ Chí Minh', 'description' => '', 'type' => 'tour_place', 'image' => 'public/images/demo/destinations/'.Str::slug('Hồ Chí Minh').'.jpg', 'slug' => Str::slug('Hồ Chí Minh'), 'active' => '1', 'prop_order' => '21', 'prop_top' => 0],
            ['id' => '41', 'code' => 'MTA', 'name' => 'Miền Tây', 'description' => '', 'type' => 'tour_place', 'image' => 'public/images/demo/destinations/'.Str::slug('Miền Tây').'.jpg', 'slug' => Str::slug('Miền Tây'), 'active' => '1', 'prop_order' => '22', 'prop_top' => 0],
            ['id' => '42', 'code' => 'TWA', 'name' => 'Đài Loan', 'description' => '', 'type' => 'tour_place', 'image' => 'public/images/demo/destinations/'.Str::slug('Đài Loan').'.jpg', 'slug' => Str::slug('Đài Loan'), 'active' => '1', 'prop_order' => '23', 'prop_top' => 1],
            ['id' => '43', 'code' => 'HKG', 'name' => 'Hồng Kông', 'description' => '', 'type' => 'tour_place', 'image' => 'public/images/demo/destinations/'.Str::slug('Hồng Kông').'.jpg', 'slug' => Str::slug('Hồng Kông'), 'active' => '1', 'prop_order' => '24', 'prop_top' => 0],
            ['id' => '44', 'code' => 'MAS', 'name' => 'Malaysia', 'description' => '', 'type' => 'tour_place', 'image' => 'public/images/demo/destinations/'.Str::slug('Malaysia').'.jpg', 'slug' => Str::slug('Malaysia'), 'active' => '1', 'prop_order' => '25', 'prop_top' => 1],
            ['id' => '45', 'code' => 'MYA', 'name' => 'Myanmar', 'description' => '', 'type' => 'tour_place', 'image' => 'public/images/demo/destinations/'.Str::slug('Myanmar').'.jpg', 'slug' => Str::slug('Myanmar'), 'active' => '1', 'prop_order' => '26', 'prop_top' => 0],
            ['id' => '46', 'code' => 'THA', 'name' => 'Thái Lan', 'description' => '', 'type' => 'tour_place', 'image' => 'public/images/demo/destinations/'.Str::slug('Thái Lan').'.jpg', 'slug' => Str::slug('Thái Lan'), 'active' => '1', 'prop_order' => '27', 'prop_top' => 1],
            ['id' => '47', 'code' => 'BRU', 'name' => 'Brunei', 'description' => '', 'type' => 'tour_place', 'image' => 'public/images/demo/destinations/'.Str::slug('Brunei').'.jpg', 'slug' => Str::slug('Brunei'), 'active' => '1', 'prop_order' => '28', 'prop_top' => 0],
            ['id' => '48', 'code' => 'LAO', 'name' => 'Lào', 'description' => '', 'type' => 'tour_place', 'image' => 'public/images/demo/destinations/'.Str::slug('Lào').'.jpg', 'slug' => Str::slug('Lào'), 'active' => '1', 'prop_order' => '29', 'prop_top' => 0],
            ['id' => '49', 'code' => 'INA', 'name' => 'Indonesia', 'description' => '', 'type' => 'tour_place', 'image' => 'public/images/demo/destinations/'.Str::slug('Indonesia').'.jpg', 'slug' => Str::slug('Indonesia'), 'active' => '1', 'prop_order' => '30', 'prop_top' => 0],
            ['id' => '50', 'code' => 'CHN', 'name' => 'Trung Quốc', 'description' => '', 'type' => 'tour_place', 'image' => 'public/images/demo/destinations/'.Str::slug('Trung Quốc').'.jpg', 'slug' => Str::slug('Trung Quốc'), 'active' => '1', 'prop_order' => '31', 'prop_top' => 1],
            ['id' => '51', 'code' => 'JPN', 'name' => 'Nhật Bản', 'description' => '', 'type' => 'tour_place', 'image' => 'public/images/demo/destinations/'.Str::slug('Nhật Bản').'.jpg', 'slug' => Str::slug('Nhật Bản'), 'active' => '1', 'prop_order' => '32', 'prop_top' => 0],
            ['id' => '52', 'code' => 'HPH', 'name' => 'Hải Phòng', 'description' => '', 'type' => 'tour_place', 'image' => 'public/images/demo/destinations/'.Str::slug('Hải Phòng').'.jpg', 'slug' => Str::slug('Hải Phòng'), 'active' => '1', 'prop_order' => '33', 'prop_top' => 0],
            ['id' => '53', 'code' => 'CLC', 'name' => 'Cù Lao Chàm', 'description' => '', 'type' => 'tour_place', 'image' => 'public/images/demo/destinations/'.Str::slug('Cù Lao Chàm').'.jpg', 'slug' => Str::slug('Cù Lao Chàm'), 'active' => '1', 'prop_order' => '34', 'prop_top' => 0],
            ['id' => '54', 'code' => 'BNH', 'name' => 'Bà Nà Hills', 'description' => '', 'type' => 'tour_place', 'image' => 'public/images/demo/destinations/'.Str::slug('Bà Nà Hills').'.jpg', 'slug' => Str::slug('Bà Nà Hills'), 'active' => '1', 'prop_order' => '35', 'prop_top' => 1],
            ['id' => '55', 'code' => 'PQC', 'name' => 'Phú Quốc', 'description' => '', 'type' => 'tour_place', 'image' => 'public/images/demo/destinations/'.Str::slug('Phú Quốc').'.jpg', 'slug' => Str::slug('Phú Quốc'), 'active' => '1', 'prop_order' => '36', 'prop_top' => 0],
            ['id' => '56', 'code' => 'SIN', 'name' => 'Singapore', 'description' => '', 'type' => 'tour_place', 'image' => 'public/images/demo/destinations/'.Str::slug('Singapore').'.jpg', 'slug' => Str::slug('Singapore'), 'active' => '1', 'prop_order' => '37', 'prop_top' => 1],
            ['id' => '57', 'code' => 'KOR', 'name' => 'Hàn Quốc', 'description' => '', 'type' => 'tour_place', 'image' => 'public/images/demo/destinations/'.Str::slug('Hàn Quốc').'.jpg', 'slug' => Str::slug('Hàn Quốc'), 'active' => '1', 'prop_order' => '38', 'prop_top' => 1],
        ]);

        $datenow = date( 'Y-m-d H:i:s' );
        // $this->call("OthersTableSeeder");
        DB::table( 'tours' )->insert( [
            [ 'id' => 1, 'name' => 'KHÁM PHÁ JEJU-HÒN ĐẢO THIÊN ĐƯỜNG (4N3Đ)', 'slug' => Str::slug('KHÁM PHÁ JEJU-HÒN ĐẢO THIÊN ĐƯỜNG (4N3Đ)'), 'image' => 'public/images/demo/tours/kham-pha-jeju-hon-dao-thien-duong.jpg',
                'prefix_code' => 'OUT','code' => str_pad(1, 6, '0', STR_PAD_LEFT), 'price' => 8490000, 'day_number' => 4, 'night_number' => 3, 'tour_status' => 1,
                'limit' => null, 'limit_start_date' => null, 'limit_end_date' => null, 'departure_date' => '2019-06-05',
                'hot' => 1, 'created_at' => $datenow, 'updated_at' => $datenow, 'active'=>1
            ],
            [ 'id' => 2, 'name' => 'KHÁM PHÁ VƯƠNG QUỐC SƯ TỬ BIỂN (13/06-4N3Đ)', 'slug' => Str::slug('KHÁM PHÁ VƯƠNG QUỐC SƯ TỬ BIỂN (13/06-4N3Đ)'), 'image' => 'public/images/demo/tours/kham-pha-vuong-quoc-su-tu-bien.jpg',
                'prefix_code' => 'OUT','code' => str_pad(2, 6, '0', STR_PAD_LEFT), 'price' => 14990000, 'day_number' => 4, 'night_number' => 3, 'tour_status' => 1,
                'limit' => null, 'limit_start_date' => null, 'limit_end_date' => null, 'departure_date' => null,
                'hot' => 1, 'created_at' => $datenow, 'updated_at' => $datenow, 'active'=>1
            ],
            [ 'id' => 3, 'name' => 'TOUR LỄ ĐÀ LẠT TỪ HUẾ (3N2Đ) TRỌN GÓI VÉ MÁY BAY KHỨ HỒI', 'slug' => Str::slug('TOUR LỄ ĐÀ LẠT TỪ HUẾ (3N2Đ) TRỌN GÓI VÉ MÁY BAY KHỨ HỒI'), 'image' => 'public/images/demo/tours/hue-da-lat-hue.jpg',
                'prefix_code' => 'OUT','code' => str_pad(3, 6, '0', STR_PAD_LEFT), 'price' => 3690000, 'day_number' => 3, 'night_number' => 2, 'tour_status' => 1,
                'limit' => null, 'limit_start_date' => null, 'limit_end_date' => null, 'departure_date' => null,
                'hot' => 1, 'created_at' => $datenow, 'updated_at' => $datenow, 'active'=>1
            ],
            [ 'id' => 4, 'name' => 'SERI TOUR 2019: ĐÀ NẴNG-ĐẢO NGỌC PHÚ QUỐC-ĐÀ NẴNG (3N2Đ – KHÔNG BAO GỒM VÉ MÁY BAY)', 'slug' => Str::slug('SERI TOUR 2019: ĐÀ NẴNG-ĐẢO NGỌC PHÚ QUỐC-ĐÀ NẴNG (3N2Đ – KHÔNG BAO GỒM VÉ MÁY BAY)'), 'image' => 'public/images/demo/tours/seri-tour-2019-da-nang-dao-ngoc-phu-quoc-da-nang-3n2d.jpg',
                'prefix_code' => 'OUT','code' => str_pad(4, 6, '0', STR_PAD_LEFT), 'price' => 1900000, 'day_number' => 3, 'night_number' => 2, 'tour_status' => 1,
                'limit' => null, 'limit_start_date' => null, 'limit_end_date' => null, 'departure_date' => null,
                'hot' => 1, 'created_at' => $datenow, 'updated_at' => $datenow, 'active'=>1
            ],
            [ 'id' => 5, 'name' => 'DKhám phá BaLi-Thiên đường du lịch (4N3Đ)', 'slug' => Str::slug('Khám phá BaLi-Thiên đường du lịch (4N3Đ)'), 'image' => 'public/images/demo/tours/kham-pha-bali-thien-duong-du-lich.jpg',
                'prefix_code' => 'OUT','code' => str_pad(5, 6, '0', STR_PAD_LEFT), 'price' => 12999000, 'day_number' => 4, 'night_number' => 3, 'tour_status' => 1,
                'limit' => null, 'limit_start_date' => null, 'limit_end_date' => null, 'departure_date' => null,
                'hot' => 0, 'created_at' => $datenow, 'updated_at' => $datenow, 'active'=>1
            ],
            [ 'id' => 6, 'name' => 'KHÁM PHÁ ĐẢO LÝ SƠN (2N1Đ)', 'slug' => Str::slug('KHÁM PHÁ ĐẢO LÝ SƠN (2N1Đ)'), 'image' => 'public/images/demo/tours/kham-pha-dao-ly-son-2n1d.jpg',
                'prefix_code' => 'OUT','code' => str_pad(6, 6, '0', STR_PAD_LEFT), 'price' => 1990000, 'day_number' => 2, 'night_number' => 1, 'tour_status' => 1,
                'limit' => null, 'limit_start_date' => null, 'limit_end_date' => null, 'departure_date' => null,
                'hot' => 1, 'created_at' => $datenow, 'updated_at' => $datenow, 'active'=>1
            ],
            [ 'id' => 7, 'name' => 'KHÁM PHÁ ĐẤT NƯỚC-CON RỒNG CHÂU Á: HONGKONG-THẨM QUYẾN (5N4Đ)', 'slug' => Str::slug('KHÁM PHÁ ĐẤT NƯỚC-CON RỒNG CHÂU Á: HONGKONG-THẨM QUYẾN (5N4Đ)'), 'image' => 'public/images/demo/tours/kham-pha-dat-nuoc-con-rong-chau-a-hongkong-tham-quyen.jpg',
                'prefix_code' => 'OUT','code' => str_pad(7, 6, '0', STR_PAD_LEFT), 'price' => 15990000, 'day_number' => 5, 'night_number' => 4, 'tour_status' => 1,
                'limit' => null, 'limit_start_date' => null, 'limit_end_date' => null, 'departure_date' => null,
                'hot' => 0, 'created_at' => $datenow, 'updated_at' => $datenow, 'active'=>1
            ],
            [ 'id' => 8, 'name' => 'DU LỊCH ĐÀI LOAN: ĐÀ NẴNG-CAO HÙNG-ĐÀI BẮC-ĐÀI TRUNG-GIA NGHĨA-ĐÀ NẴNG', 'slug' => Str::slug('DU LỊCH ĐÀI LOAN: ĐÀ NẴNG-CAO HÙNG-ĐÀI BẮC-ĐÀI TRUNG-GIA NGHĨA-ĐÀ NẴNG'), 'image' => 'public/images/demo/tours/du-lich-dai-loan-da-nang-cao-hung-dai-bac-dai-trung-gia-nghia-da-nang.png',
                'prefix_code' => 'OUT','code' => str_pad(8, 6, '0', STR_PAD_LEFT), 'price' => 10690000, 'day_number' => 5, 'night_number' => 4, 'tour_status' => 1,
                'limit' => null, 'limit_start_date' => null, 'limit_end_date' => null, 'departure_date' => null,
                'hot' => 1, 'created_at' => $datenow, 'updated_at' => $datenow, 'active'=>1
            ],
            [ 'id' => 9, 'name' => 'Du lịch Hàn Quốc: Seoul-Đảo Nami-Công viên Everland (5N4Đ)', 'slug' => Str::slug('Du lịch Hàn Quốc: Seoul-Đảo Nami-Công viên Everland (5N4Đ)'), 'image' => 'public/images/demo/tours/du-lich-han-quoc-seoul-dao-nami-cong-vien-everland.jpg',
                'prefix_code' => 'OUT','code' => str_pad(9, 6, '0', STR_PAD_LEFT), 'price' => 13940000, 'day_number' => 5, 'night_number' => 4, 'tour_status' => 1,
                'limit' => null, 'limit_start_date' => null, 'limit_end_date' => null, 'departure_date' => null,
                'hot' => 0, 'created_at' => $datenow, 'updated_at' => $datenow, 'active'=>1
            ],
            [ 'id' => 10, 'name' => 'KHÁM PHÁ THÁI LAN HÈ 2019: BANGKOK-PATTAYA-CHỢ NỔI-ĂN TRƯA BAIYOKE SKY (5N4Đ)', 'slug' => Str::slug('KHÁM PHÁ THÁI LAN HÈ 2019: BANGKOK-PATTAYA-CHỢ NỔI-ĂN TRƯA BAIYOKE SKY (5N4Đ)'), 'image' => 'public/images/demo/tours/kham-pha-thai-lan-he-2019-bangkok-pattaya-cho-noi-an-trua-baiyoke-sky.jpg',
                'prefix_code' => 'OUT','code' => str_pad(10, 6, '0', STR_PAD_LEFT), 'price' => 6990000, 'day_number' => 5, 'night_number' => 4, 'tour_status' => 1,
                'limit' => null, 'limit_start_date' => null, 'limit_end_date' => null, 'departure_date' => null,
                'hot' => 1, 'created_at' => $datenow, 'updated_at' => $datenow, 'active'=>1
            ],
            [ 'id' => 11, 'name' => 'Bắc Kinh-Thượng Hải-Hàng Châu-Tô Châu (7N7Đ)', 'slug' => Str::slug('Bắc Kinh-Thượng Hải-Hàng Châu-Tô Châu (7N7Đ)'), 'image' => 'public/images/demo/tours/bac-kinh-thuong-hai-hang-chau-to-chau.jpg',
                'prefix_code' => 'INB','code' => str_pad(11, 6, '0', STR_PAD_LEFT), 'price' => 15600000, 'day_number' => 7, 'night_number' => 7, 'tour_status' => 1,
                'limit' => null, 'limit_start_date' => null, 'limit_end_date' => null, 'departure_date' => null,
                'hot' => 1, 'created_at' => $datenow, 'updated_at' => $datenow, 'active'=>1
            ],
            [ 'id' => 12, 'name' => 'Hà Nội-Sapa-Cát Cát-Hàm Rồng (2 Ngày 1 Đêm)', 'slug' => Str::slug('Hà Nội-Sapa-Cát Cát-Hàm Rồng (2 Ngày 1 Đêm)'), 'image' => 'public/images/demo/tours/ha-noi-sapa-cat-cat-ham-rong.jpg',
                'prefix_code' => 'INB','code' => str_pad(12, 6, '0', STR_PAD_LEFT), 'price' => 1750000, 'day_number' => 2, 'night_number' => 1, 'tour_status' => 1,
                'limit' => null, 'limit_start_date' => null, 'limit_end_date' => null, 'departure_date' => null,
                'hot' => 1, 'created_at' => $datenow, 'updated_at' => $datenow, 'active'=>1
            ],
			[ 'id' => 13, 'name' => 'Huế-Nha Trang-Đà Lạt-Huế (4N4Đ)', 'slug' => Str::slug('Huế-Nha Trang-Đà Lạt-Huế (4N4Đ)'), 'image' => 'public/images/demo/tours/lang-bi-ang-da-lat.png',
                'prefix_code' => 'INB','code' => str_pad(13, 6, '0', STR_PAD_LEFT), 'price' => 4750000, 'day_number' => 4, 'night_number' => 4, 'tour_status' => 1,
                'limit' => null, 'limit_start_date' => null, 'limit_end_date' => null, 'departure_date' => null,
                'hot' => 1, 'created_at' => $datenow, 'updated_at' => $datenow, 'active'=>1
            ],
			[ 'id' => 14, 'name' => 'Seri tour 2019: Đà Nẵng-Đảo Ngọc Phú Quốc-Đà Nẵng (4N3Đ – không bao gồm vé máy bay)', 'slug' => Str::slug('Seri tour 2019: Đà Nẵng-Đảo Ngọc Phú Quốc-Đà Nẵng (4N3Đ – không bao gồm vé máy bay)'), 'image' => 'public/images/demo/tours/seri-tour-2019-da-nang-dao-ngoc-phu-quoc-da-nang.jpg',
                'prefix_code' => 'INB','code' => str_pad(14, 6, '0', STR_PAD_LEFT), 'price' => 2690000, 'day_number' => 4, 'night_number' => 3, 'tour_status' => 1,
                'limit' => null, 'limit_start_date' => null, 'limit_end_date' => null, 'departure_date' => null,
                'hot' => 0, 'created_at' => $datenow, 'updated_at' => $datenow, 'active'=>1
            ],
			[ 'id' => 15, 'name' => 'Huế-Khám phá Quy Nhơn Bình Định-Huế (3N2Đ)', 'slug' => Str::slug('Huế-Khám phá Quy Nhơn Bình Định-Huế (3N2Đ)'), 'image' => 'public/images/demo/tours/hue-kham-pha-quy-nhon-binh-dinh-hue.jpg',
                'prefix_code' => 'INB','code' => str_pad(15, 6, '0', STR_PAD_LEFT), 'price' => 3890000, 'day_number' => 3, 'night_number' => 2, 'tour_status' => 1,
                'limit' => null, 'limit_start_date' => null, 'limit_end_date' => null, 'departure_date' => null,
                'hot' => 0, 'created_at' => $datenow, 'updated_at' => $datenow, 'active'=>1
            ],
			[ 'id' => 16, 'name' => 'Quy Nhơn-Phú Yên thiên đường biển đảo (3N2Đ)', 'slug' => Str::slug('Quy Nhơn-Phú Yên thiên đường biển đảo (3N2Đ)'), 'image' => 'public/images/demo/tours/quy-nhon-phu-yen-thien-duong-bien-dao.jpg',
                'prefix_code' => 'INB','code' => str_pad(16, 6, '0', STR_PAD_LEFT), 'price' => 2890000, 'day_number' => 3, 'night_number' => 2, 'tour_status' => 1,
                'limit' => null, 'limit_start_date' => null, 'limit_end_date' => null, 'departure_date' => null,
                'hot' => 1, 'created_at' => $datenow, 'updated_at' => $datenow, 'active'=>1
            ],
			[ 'id' => 17, 'name' => 'Du lịch Hòn Khô-Quy Nhơn (1/2 Ngày)', 'slug' => Str::slug('Du lịch Hòn Khô-Quy Nhơn (1/2 Ngày)'), 'image' => 'public/images/demo/tours/du-lich-hon-kho-quy-nhon.jpg',
                'prefix_code' => 'INB','code' => str_pad(17, 6, '0', STR_PAD_LEFT), 'price' => 790000, 'day_number' => 1, 'night_number' => 0, 'tour_status' => 1,
                'limit' => null, 'limit_start_date' => null, 'limit_end_date' => null, 'departure_date' => null,
                'hot' => 0, 'created_at' => $datenow, 'updated_at' => $datenow, 'active'=>1
            ],
			[ 'id' => 18, 'name' => 'Phú Yên-Kỳ Co-Thiên đường biển đảo (4N3Đ)', 'slug' => Str::slug('Phú Yên-Kỳ Co-Thiên đường biển đảo (4N3Đ)'), 'image' => 'public/images/demo/tours/phu-yen-ky-co-thien-duong-bien-dao.jpg',
                'prefix_code' => 'INB','code' => str_pad(18, 6, '0', STR_PAD_LEFT), 'price' => 2890000, 'day_number' => 4, 'night_number' => 3, 'tour_status' => 1,
                'limit' => null, 'limit_start_date' => null, 'limit_end_date' => null, 'departure_date' => null,
                'hot' => 1, 'created_at' => $datenow, 'updated_at' => $datenow, 'active'=>1
            ],
			[ 'id' => 19, 'name' => 'Khám phá Trung Hoa: Hành trình “Trương Gia Giới-Phượng Hoàng Cổ Trấn-Đồng Nhân-Vĩnh Thuận (6N5Đ)', 'slug' => Str::slug('Khám phá Trung Hoa: Hành trình “Trương Gia Giới-Phượng Hoàng Cổ Trấn-Đồng Nhân-Vĩnh Thuận (6N5Đ)'),
				'image' => 'public/images/demo/tours/kham-pha-trung-hoa-hanh-trinh-truong-gia-gioi-phuong-hoang-co.jpg',
                'prefix_code' => 'INB','code' => str_pad(19, 6, '0', STR_PAD_LEFT), 'price' => 12666000, 'day_number' => 6, 'night_number' => 5, 'tour_status' => 1,
                'limit' => null, 'limit_start_date' => null, 'limit_end_date' => null, 'departure_date' => null,
                'hot' => 1, 'created_at' => $datenow, 'updated_at' => $datenow, 'active'=>1
            ],
			[ 'id' => 20, 'name' => 'Khám phá Trung Hoa: Hành trình “Côn Minh-Đại Lý-Lệ Giang (6N5Đ)', 'slug' => Str::slug('Khám phá Trung Hoa: Hành trình “Côn Minh-Đại Lý-Lệ Giang (6N5Đ)'),
				'image' => 'public/images/demo/tours/kham-pha-trung-hoa-hanh-trinh-con-minh-dai-ly-le-giang.jpg',
                'prefix_code' => 'INB','code' => str_pad(20, 6, '0', STR_PAD_LEFT), 'price' => 15990000, 'day_number' => 6, 'night_number' => 5, 'tour_status' => 1,
                'limit' => null, 'limit_start_date' => null, 'limit_end_date' => null, 'departure_date' => null,
                'hot' => 1, 'created_at' => $datenow, 'updated_at' => $datenow, 'active'=>1
            ],
        ] );

        DB::table( 'tour_property_values' )->insert( [
            ['tour_id' => 1, 'tour_property_id' => 1],
            ['tour_id' => 1, 'tour_property_id' => 5],
            ['tour_id' => 1, 'tour_property_id' => 9],
            ['tour_id' => 1, 'tour_property_id' => 17],
            ['tour_id' => 1, 'tour_property_id' => 8],
            ['tour_id' => 1, 'tour_property_id' => 57],

            ['tour_id' => 2, 'tour_property_id' => 1],
            ['tour_id' => 2, 'tour_property_id' => 5],
            ['tour_id' => 2, 'tour_property_id' => 9],
            ['tour_id' => 2, 'tour_property_id' => 12],
            ['tour_id' => 2, 'tour_property_id' => 16],
            ['tour_id' => 2, 'tour_property_id' => 18],
            ['tour_id' => 2, 'tour_property_id' => 56],

            ['tour_id' => 3, 'tour_property_id' => 1],
            ['tour_id' => 3, 'tour_property_id' => 5],
            ['tour_id' => 3, 'tour_property_id' => 10],
            ['tour_id' => 3, 'tour_property_id' => 12],
            ['tour_id' => 3, 'tour_property_id' => 8],
            ['tour_id' => 3, 'tour_property_id' => 29],

            ['tour_id' => 4, 'tour_property_id' => 1],
            ['tour_id' => 4, 'tour_property_id' => 10],
            ['tour_id' => 4, 'tour_property_id' => 17],
            ['tour_id' => 4, 'tour_property_id' => 18],
            ['tour_id' => 4, 'tour_property_id' => 30],

            ['tour_id' => 5, 'tour_property_id' => 1],
            ['tour_id' => 5, 'tour_property_id' => 4],
            ['tour_id' => 5, 'tour_property_id' => 9],
            ['tour_id' => 5, 'tour_property_id' => 12],
            ['tour_id' => 5, 'tour_property_id' => 18],
            ['tour_id' => 5, 'tour_property_id' => 19],
            ['tour_id' => 5, 'tour_property_id' => 49],

            ['tour_id' => 6, 'tour_property_id' => 1],
            ['tour_id' => 6, 'tour_property_id' => 10],
            ['tour_id' => 6, 'tour_property_id' => 17],
            ['tour_id' => 6, 'tour_property_id' => 8],
            ['tour_id' => 6, 'tour_property_id' => 34],

            ['tour_id' => 7, 'tour_property_id' => 2],
            ['tour_id' => 7, 'tour_property_id' => 9],
            ['tour_id' => 7, 'tour_property_id' => 17],
            ['tour_id' => 7, 'tour_property_id' => 8],
            ['tour_id' => 7, 'tour_property_id' => 43],

            ['tour_id' => 8, 'tour_property_id' => 2],
            ['tour_id' => 8, 'tour_property_id' => 9],
            ['tour_id' => 8, 'tour_property_id' => 12],
            ['tour_id' => 8, 'tour_property_id' => 18],
            ['tour_id' => 8, 'tour_property_id' => 42],

            ['tour_id' => 9, 'tour_property_id' => 2],
            ['tour_id' => 9, 'tour_property_id' => 9],
            ['tour_id' => 9, 'tour_property_id' => 12],
            ['tour_id' => 9, 'tour_property_id' => 18],
            ['tour_id' => 9, 'tour_property_id' => 57],

            ['tour_id' => 10, 'tour_property_id' => 2],
            ['tour_id' => 10, 'tour_property_id' => 9],
            ['tour_id' => 10, 'tour_property_id' => 12],
            ['tour_id' => 10, 'tour_property_id' => 18],
            ['tour_id' => 10, 'tour_property_id' => 46],

            ['tour_id' => 11, 'tour_property_id' => 2],
            ['tour_id' => 11, 'tour_property_id' => 9],
            ['tour_id' => 11, 'tour_property_id' => 12],
            ['tour_id' => 11, 'tour_property_id' => 18],
            ['tour_id' => 11, 'tour_property_id' => 50],

            ['tour_id' => 12, 'tour_property_id' => 1],
            ['tour_id' => 12, 'tour_property_id' => 10],
            ['tour_id' => 12, 'tour_property_id' => 12],
            ['tour_id' => 12, 'tour_property_id' => 18],
            ['tour_id' => 12, 'tour_property_id' => 19],
            ['tour_id' => 12, 'tour_property_id' => 20],
			
			['tour_id' => 13, 'tour_property_id' => 1],
            ['tour_id' => 13, 'tour_property_id' => 10],
            ['tour_id' => 13, 'tour_property_id' => 12],
            ['tour_id' => 13, 'tour_property_id' => 18],
            ['tour_id' => 13, 'tour_property_id' => 19],
            ['tour_id' => 13, 'tour_property_id' => 29],
			
			['tour_id' => 14, 'tour_property_id' => 1],
            ['tour_id' => 14, 'tour_property_id' => 10],
            ['tour_id' => 14, 'tour_property_id' => 12],
            ['tour_id' => 14, 'tour_property_id' => 18],
            ['tour_id' => 14, 'tour_property_id' => 19],
            ['tour_id' => 14, 'tour_property_id' => 30],
			
			['tour_id' => 15, 'tour_property_id' => 1],
            ['tour_id' => 15, 'tour_property_id' => 10],
            ['tour_id' => 15, 'tour_property_id' => 12],
            ['tour_id' => 15, 'tour_property_id' => 17],
            ['tour_id' => 15, 'tour_property_id' => 19],
            ['tour_id' => 15, 'tour_property_id' => 29],
			
			['tour_id' => 16, 'tour_property_id' => 1],
            ['tour_id' => 16, 'tour_property_id' => 10],
            ['tour_id' => 16, 'tour_property_id' => 12],
            ['tour_id' => 16, 'tour_property_id' => 18],
            ['tour_id' => 16, 'tour_property_id' => 17],
            ['tour_id' => 16, 'tour_property_id' => 35],
			
			['tour_id' => 17, 'tour_property_id' => 1],
            ['tour_id' => 17, 'tour_property_id' => 10],
            ['tour_id' => 17, 'tour_property_id' => 12],
            ['tour_id' => 17, 'tour_property_id' => 18],
            ['tour_id' => 17, 'tour_property_id' => 17],
            ['tour_id' => 17, 'tour_property_id' => 33],
			
			['tour_id' => 18, 'tour_property_id' => 1],
            ['tour_id' => 18, 'tour_property_id' => 10],
            ['tour_id' => 18, 'tour_property_id' => 12],
            ['tour_id' => 18, 'tour_property_id' => 16],
            ['tour_id' => 18, 'tour_property_id' => 18],
            ['tour_id' => 18, 'tour_property_id' => 35],
			
			['tour_id' => 19, 'tour_property_id' => 1],
            ['tour_id' => 19, 'tour_property_id' => 9],
            ['tour_id' => 19, 'tour_property_id' => 12],
            ['tour_id' => 19, 'tour_property_id' => 17],
            ['tour_id' => 19, 'tour_property_id' => 8],
            ['tour_id' => 19, 'tour_property_id' => 50],
			
			['tour_id' => 20, 'tour_property_id' => 1],
            ['tour_id' => 20, 'tour_property_id' => 9],
            ['tour_id' => 20, 'tour_property_id' => 12],
            ['tour_id' => 20, 'tour_property_id' => 17],
            ['tour_id' => 20, 'tour_property_id' => 8],
            ['tour_id' => 20, 'tour_property_id' => 50],
        ] );

        DB::table( 'tour_schedules' )->insert( [
            //tour_id: 1 => 4N-3Đ
            ['day' => 1, 'name' => 'DA NANG – JEJU',
                'content' => '<p><strong>21h00</strong>&nbsp;Hướng dẫn vi&ecirc;n đ&oacute;n qu&yacute; kh&aacute;ch tại tầng 2 s&acirc;n bay quốc tế Da Nang, đ&aacute;p chuyến bay đi Jeju l&uacute;c&nbsp;<strong>01:45</strong>. Qu&yacute; kh&aacute;ch nghỉ đ&ecirc;m tr&ecirc;n m&aacute;y bay.</p>',
                'tour_id' => 1,'created_at' => $datenow, 'updated_at' => $datenow
            ],
            ['day' => 2, 'name' => 'JEJU (Ăn sáng, trưa, tối)',
                'content' => '<p><strong>S&aacute;ng:</strong>&nbsp;6h45 Qu&yacute; kh&aacute;ch đến&nbsp;<strong>S&acirc;n bay quốc tế Jeju</strong>&nbsp;v&agrave; l&agrave;m thủ tục nhập cảnh. Hướng dẫn vi&ecirc;n v&agrave; t&agrave;i xế địa phương sẽ đ&oacute;n v&agrave; đưa đo&agrave;n đi ăn s&aacute;ng. Sau khi ăn s&aacute;ng, đo&agrave;n khởi h&agrave;nh tham quan một số địa điểm nổi tiếng tại Jeju&nbsp;:</p>

                    <p>Tham quan c&ocirc;ng vi&ecirc;n&nbsp;<strong>Love land&nbsp;</strong>&ndash;&nbsp;<em>c&ocirc;ng vi&ecirc;n chủ đề về t&igrave;nh y&ecirc;u của đảo Jeju l&agrave; nơi l&yacute; tưởng cho c&aacute;c cặp đ&ocirc;i y&ecirc;u nhau đến tham quan.</em></p>
                    
                    <p><strong>Tảng đ&aacute; h&igrave;nh đầu rồng</strong>&nbsp;&ndash;&nbsp;<em>một trong những cảnh quan g&acirc;y được kh&aacute; nhiều ấn tượng nơi du kh&aacute;ch tham quan bởi rặng đ&aacute; nh&ocirc; ra biển c&oacute; h&igrave;nh đầu rồng tr&ocirc;ng rất kỳ th&uacute;,</em></p>
                    
                    <p><strong>Trưa:</strong>&nbsp;Sau khi ăn trưa tại nh&agrave; h&agrave;ng địa phương, đo&agrave;n tham quan<strong>&nbsp;b&atilde;i cột đ&aacute; Jusan Jelly</strong>&nbsp;&ndash; một kiệt t&aacute;c thi&ecirc;n nhi&ecirc;n, được h&igrave;nh th&agrave;nh từ dung nham n&uacute;i lửa Hallasan chảy xuống b&atilde;i biển Jungmun.</p>
                    
                    <p>Tự do tham quan mua sắm&nbsp;<strong>khu phố Tapdong</strong>&nbsp;nhộn nhịp.</p>
                    
                    <p>Ăn tối v&agrave; nhận ph&ograve;ng kh&aacute;ch sạn. Qu&yacute; kh&aacute;ch nghỉ ngơi tại kh&aacute;ch sạn.</p>',
                'tour_id' => 1, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            ['day' => 3, 'name' => 'JEJU (Ăn sáng, trưa, tối)',
                'content' => '<p><strong>S&aacute;ng:</strong>&nbsp;Sau khi ăn s&aacute;ng, tham quan&nbsp;<strong>Đỉnh Seongsan</strong>&nbsp;&ndash;&nbsp;<em>nơi c&oacute; thể nh&igrave;n thấy &aacute;nh mặt trời đầu ti&ecirc;n ở H&agrave;n Quốc.</em>&nbsp;Tiếp đến l&agrave;&nbsp;<strong>Seopjikoji &ndash;</strong>&nbsp;<em>nổi tiếng với những c&aacute;nh đồng hoa cải dầu xanh mướt &amp; dạo chơi tr&ecirc;n con đường m&ograve;n Olle</em>. &nbsp;Ăn trưa tại nh&agrave; h&agrave;ng.</p>

                    <p><strong>Trưa:</strong>&nbsp;Tiếp tục h&agrave;nh tr&igrave;nh, đo&agrave;n mua sắm tại&nbsp;<strong>cửa h&agrave;ng nh&acirc;n s&acirc;m.</strong>&nbsp;Tại những cửa h&agrave;ng n&agrave;y, du kh&aacute;ch c&oacute; dịp biết đến kh&aacute; nhiều sản phẩm nh&acirc;n s&acirc;m tốt cho sức khỏe, v&agrave; nhiều mặt h&agrave;ng đa dạng kh&aacute;c m&agrave; kh&aacute;ch c&oacute; thể tự do mua sắm ph&ugrave; hợp với &yacute; th&iacute;ch</p>
                    
                    <p>Tham quan mua sắm mỹ phẩm v&agrave; tinh dầu th&ocirc;ng.</p>
                    
                    <p><strong>Trải nghiệm Kimchi v&agrave; mặc Hanbok.</strong></p>
                    
                    <p>Ăn tối v&agrave; nghỉ ngơi tại kh&aacute;ch sạn.</p>',
                'tour_id' => 1, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            ['day' => 4, 'name' => 'JEJU – DA NANG (Ăn sáng, trưa, tối)',
                'content' => '<p><strong>S&aacute;ng&nbsp;:</strong>&nbsp;Sau bữa s&aacute;ng, Mua sắm tại cửa h&agrave;ng S&acirc;m tươi v&agrave; nấm linh chi.</p>

                    <p><strong>Tham quan&nbsp; l&agrave;ng d&acirc;n tộc Seongup&nbsp;</strong><strong>&ndash;</strong>&nbsp;<em>từng l&agrave; thủ phủ của bộ tộc Cheongeui &ndash; Hyeon giai đoạn từ năm 1410 &ndash; 1914, l&agrave;&nbsp;<strong>phim trường nổi tiếng của bộ phim &lsquo;N&agrave;ng Dae Chang Geum&rsquo;</strong>. Ng&ocirc;i l&agrave;ng c&oacute; đến 260 kiểu nh&agrave; truyền thống, x&acirc;y dựng từ những năm cuối của triều đại Joseon.</em></p>
                    
                    <p>Qu&yacute; kh&aacute;ch tham quan&nbsp;&nbsp;<strong>bảo t&agrave;ng tr&agrave; xanh Ohseolluk museum<em>,</em></strong><em>&nbsp;nơi tập trung của c&aacute;c loại tr&agrave; với nhiều hương vị kh&aacute;c nhau v&agrave; đều mang n&eacute;t đặc trưng ri&ecirc;ng biệt.</em></p>
                    
                    <p><strong>Trưa:</strong>&nbsp;Ăn trưa&nbsp; tại nh&agrave; h&agrave;ng địa phương.</p>
                    
                    <p>Thăm quan v&agrave; mua sắm tại trung t&acirc;m miễn thuế.</p>
                    
                    <p>Xe đưa đo&agrave;n ra s&acirc;n bay Cheju về Da Nang. Qu&yacute; kh&aacute;ch ăn nhẹ Gimbab cho bữa tối. Đo&agrave;n đ&aacute;p chuyến bay l&uacute;c 21:30. Đến Đ&agrave; Nẵng, chia tay qu&yacute; kh&aacute;ch v&agrave; kết th&uacute;c chương tr&igrave;nh, hẹn gặp lại qu&yacute; kh&aacute;ch.</p>',
                'tour_id' => 1, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            //tour_id: 2 => 4N-3Đ
            ['day' => 1, 'name' => 'ĐÀ NẴNG – SINGAPORE',
                'content' => '<p><strong>Chiều:</strong>&nbsp;17:15 Qu&yacute; kh&aacute;ch c&oacute; mặt tại s&acirc;n bay Đ&agrave; Nẵng, HDV đ&oacute;n đo&agrave;n v&agrave; gi&uacute;p qu&yacute; kh&aacute;ch l&agrave;m thủ tục đ&aacute;p chuyến bay&nbsp;<strong>3K542 (20:15 &ndash; 23:45)</strong>&nbsp;đi Singapore.</p>

                    <ul>
                        <li>Đến s&acirc;n bay Changi &ndash; Singapore xe v&agrave; hướng dẫn địa phương đ&oacute;n đo&agrave;n về kh&aacute;ch sạn.</li>
                        <li>Nhận ph&ograve;ng kh&aacute;ch sạn nghỉ ngơi.</li>
                    </ul>',
                'tour_id' => 2, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            ['day' => 2, 'name' => 'JEJU (Ăn sáng, trưa, tối)',
                'content' => '<p><strong>S&aacute;ng:</strong>&nbsp;Ăn s&aacute;ng tại kh&aacute;ch sạn.&nbsp;Qu&yacute; kh&aacute;ch sẽ được hướng dẫn vi&ecirc;n địa phương thuyết minh v&agrave; chụp ảnh kỷ niệm b&ecirc;n ngo&agrave;i c&aacute;c điểm:</p>

                    <ul>
                        <li><strong>T&ograve;a nh&agrave; Quốc hội lịch sử / T&ograve;a &aacute;n tối cao / T&ograve;a thị ch&iacute;nh.</strong></li>
                        <li><strong>Nh&agrave; h&aacute;t Esplanade/ Vịnh Marina</strong></li>
                        <li><strong>Tượng Merlion Park &ndash;&nbsp;</strong>biểu tượng của đất nước Singapore<strong>.</strong></li>
                    </ul>
                    
                    <p><strong>Trưa:</strong>&nbsp; Thưởng thức&nbsp;<strong>lẩu &amp; thịt nướng BBQ H&agrave;n Quốc</strong>, Sau đ&oacute;, xe đưa cả đo&agrave;n ra&nbsp;<strong>đảo Sentosa</strong></p>
                    
                    <ul>
                        <li>Qu&yacute; kh&aacute;ch tự do tham quan khu&nbsp;<strong>Resort World Casino, Festive Walk</strong>,</li>
                        <li>Tham quan v&agrave; chụp h&igrave;nh b&ecirc;n ngo&agrave;i&nbsp;<strong>Khu Universal&nbsp;</strong>&ndash; Với diện t&iacute;ch khoảng 20 hecta, khu giải tr&iacute; bao gồm c&aacute;c khu vui chơi hiện đại v&agrave; c&aacute;c cảnh quay, tạo h&igrave;nh nh&acirc;n vật nổi tiếng trong c&aacute;c bộ phim: C&ocirc;ng vi&ecirc;n kỷ Jura, Thế giới phim hoạt h&igrave;nh Shrek, Madagascar, Ai cập huyền b&iacute;&hellip; C&ocirc;ng vi&ecirc;n được chia th&agrave;nh 7 khu giải tr&iacute; lớn với c&aacute;c cảnh quan v&agrave; dịch vụ kh&aacute;c nhau. Khi đ&atilde; đến đ&acirc;y, bạn kh&ocirc;ng n&ecirc;n bỏ qua bất kỳ địa điểm n&agrave;o v&igrave; n&oacute; cực kỳ hấp dẫn. V&agrave; cũng đừng qu&ecirc;n chụp h&igrave;nh lưu niệm với c&aacute;c ng&ocirc;i sao điện ảnh Hollywood được dựng giống y như thật tại đ&acirc;y.</li>
                        <li>Thăm thuỷ cung lớn nhất thế giới&nbsp;<strong>E.A Aquarium</strong>&nbsp;&ndash; rất ấn tượng với thế giới đại dương sinh động. Đ&acirc;y được xem l&agrave; thủy cung lớn nhất thế giới, trưng b&agrave;y hơn 100.000 sinh v&acirc;̣t biển thuộc 800 lo&agrave;i. Ngo&agrave;i ra, c&ograve;n c&oacute; tới 20.000 loại san h&ocirc;. Nơi đ&acirc;y cũng c&oacute; dòng s&ocirc;ng lười d&agrave;i nhất thế giới, khoảng 620 m&eacute; t.</li>
                        <li>Chụp h&igrave;nh b&ecirc;n ngo&agrave;i<strong>&nbsp;Merlion tower.</strong></li>
                    </ul>
                    
                    <p><strong>Tối:</strong>&nbsp;Ăn tối v&agrave; thưởng thức chương tr&igrave;nh nhạc nước ho&agrave;nh tr&aacute;ng&nbsp;<strong>&ldquo;Wings Of Time&rdquo;&nbsp;</strong>với sự pha trộn giữa hiệu ứng lửa, nước c&ugrave;ng với đ&egrave;n LED đầy m&agrave;u sắc v&agrave; hệ thống đ&egrave;n Lazer hiện đại tạo n&ecirc;n hiệu ứng 3D sống động, độc đ&aacute;o v&agrave; tuyệt đẹp. Về lại kh&aacute;ch sạn nghỉ ngơi.</p>',
                'tour_id' => 2, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            ['day' => 3, 'name' => 'JEJU (Ăn sáng, trưa, tối)',
                'content' => '<p><strong>S&aacute;ng:</strong>&nbsp;D&ugrave;ng bữa s&aacute;ng tại kh&aacute;ch sạn. Qu&yacute; kh&aacute;ch khởi h&agrave;nh tham quan:</p>

                    <ul>
                        <li><strong>Cửa h&agrave;ng b&aacute;n dầu gi&oacute;</strong>&nbsp;&ndash; sản phẩm truyền thống của Singapore.</li>
                        <li><strong>Thăm đỉnh n&uacute;i Mount Faber</strong>&nbsp;&ndash; nơi c&oacute; thể ngắm to&agrave;n cảnh đất nước Singapore xinh đẹp</li>
                    </ul>
                    
                    <p><strong>Trưa:</strong>&nbsp;D&ugrave;ng cơm trưa tại nh&agrave; h&agrave;ng, sau bữa trưa, đo&agrave;n tham quan:</p>
                    
                    <ul>
                        <li><strong>Tham quan Vườn thực vật &ndash; Gardens by the Bay &ndash;</strong>&nbsp;l&agrave; khu vườn si&ecirc;u c&acirc;y độc lạ nổi tiếng tr&ecirc;n to&agrave;n thế giới. Khu vườn nh&acirc;n tạo n&agrave;y rộng hơn 100ha trải d&agrave;i với 3 khu vườn b&ecirc;n bờ vịnh: Bay South (54 ha) v&agrave; Bay East (32 ha) ở&nbsp;hai b&ecirc;n, nằm giữa l&agrave; Bay Central (15 ha). Qu&yacute; kh&aacute;ch tham quan v&agrave; chụp ảnh b&ecirc;n ngo&agrave;i khu vực nh&agrave; k&iacute;nh.</li>
                    </ul>
                    
                    <p><strong>Tối:</strong>&nbsp;Ăn tối sau đ&oacute; qu&yacute; kh&aacute;ch&nbsp;<strong>tự do kh&aacute;m ph&aacute; th&agrave;nh phố về đ&ecirc;m</strong>&nbsp;với c&aacute;c điểm tham quan gợi &yacute; như:&nbsp;<strong><em>(Chi ph&iacute; tự t&uacute;c)</em></strong></p>
                    
                    <ul>
                        <li><strong>Clarke&nbsp;Quay,</strong>&nbsp;điểm đến cho sinh hoạt ẩm thực v&agrave; hoạt động giải tr&iacute; tại Singapore, một cảnh quan sinh động với những t&ograve;a nh&agrave; đầy m&agrave;u sắc l&agrave; cửa h&agrave;ng đồ cổ, nh&agrave; h&agrave;ng, qu&aacute;n cafe s&agrave;nh điệu, c&acirc;u lạc bộ jazz v&agrave; nhiều hơn nữa. Đ&ecirc;m xuống, to&agrave;n bộ nơi n&agrave;y trở n&ecirc;n bắt mắt với tất cả năm khu nh&agrave; kho được t&acirc;n trang s&aacute;ng đ&egrave;n v&agrave; trở n&ecirc;n rạng rỡ với một d&atilde;y c&aacute;c c&acirc;u lạc bộ lạ kỳ thay cho c&aacute;c c&acirc;u lạc bộ phổ biến. Với nhiều lựa chọn cho họat động tụ tập, giải tr&iacute; mang đẳng cấp quốc tế, nơi đ&acirc;y thực sự l&agrave; điểm hội tụ l&yacute; tưởng cho mọi người đến từ khắp nơi tr&ecirc;n thế giới<strong><em>.(Chi ph&iacute; tự t&uacute;c)</em></strong></li>
                        <li>Qu&yacute; kh&aacute;ch tự do kh&aacute;m ph&aacute; cuộc sống trong l&ograve;ng đất của người d&acirc;n Singapore bằng&nbsp;<strong>T&agrave;u điện ngầm MRT</strong></li>
                        <li>Trải nghiệm Du thuyền tr&ecirc;n d&ograve;ng s&ocirc;ng Singapore ngắm cảnh&nbsp;<strong>vịnh Marina Bay về đ&ecirc;m</strong>&nbsp;.</li>
                        <li>Về lại kh&aacute;ch sạn. Nghỉ đ&ecirc;m tại Singapore.</li>
                    </ul>',
                'tour_id' => 2, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            ['day' => 4, 'name' => 'JEJU – DA NANG (Ăn sáng, trưa, tối)',
                'content' => '<p><strong>S&aacute;ng:&nbsp;</strong>D&ugrave;ng bữa s&aacute;ng tại kh&aacute;ch sạn, đo&agrave;n tham quan:</p>

                    <ul>
                        <li><strong>Ch&ugrave;a Răng Phật</strong></li>
                        <li>Mua sắm tại&nbsp;<strong>Cửa h&agrave;ng Chocolate, Cửa h&agrave;ng v&agrave;ng bạc, đ&aacute; qu&iacute;</strong>&nbsp;nổi tiếng của Singapore.</li>
                    </ul>
                    
                    <p><strong>Trưa:</strong>&nbsp;Ăn trưa. Qu&yacute; kh&aacute;ch thăm quan,</p>
                    
                    <ul>
                        <li>Tự do mua sắm tại&nbsp;<strong>TTTM Marina Square/ Orchard/ Bugis</strong>&hellip;, c&aacute;c cửa h&agrave;ng miễn thuế&hellip;</li>
                        <li>Xe v&agrave; hướng dẫn đưa qu&yacute; kh&aacute;ch ra s&acirc;n bay, qu&yacute; kh&aacute;ch c&oacute; mặt tại s&acirc;n bay đ&aacute;p chuyến bay 3K541 (18:10-19:35) về Đ&agrave; Nẵng (kh&ocirc;ng bao gồm ăn tối tr&ecirc;n m&aacute;y bay). Chia tay qu&yacute; kh&aacute;ch v&agrave; hẹn gặp lại<em>.</em></li>
                    </ul>',
                'tour_id' => 2, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            //tour_id: 3 => 3N-2Đ
            ['day' => 1, 'name' => 'DA NANG – JEJU',
                'content' => '<p><strong>S&aacute;ng:&nbsp;</strong>Xe v&agrave; hướng dẫn vi&ecirc;n của EagleTourist đ&oacute;n qu&yacute; kh&aacute;ch tại điểm hẹn đưa ra s&acirc;n bay Ph&uacute; B&agrave;i l&agrave;m thủ tục đ&aacute;p chuyến bay đi Đ&agrave; Lạt (<strong>08h25 &ndash;</strong><strong>&nbsp;9h30)</strong>. Khoảng 9h30 đo&agrave;n đ&aacute;p s&acirc;n bay Li&ecirc;n Khương, xe v&agrave; hdv đ&oacute;n đo&agrave;n khởi h&agrave;nh về trung t&acirc;m th&agrave;nh phố Đ&agrave; Lạt.</p>

                    <p>Điểm tham quan đầu ti&ecirc;n của đo&agrave;n l&agrave;:</p>
                    
                    <ul>
                        <li><strong>Thiền Viện Tr&uacute;c L&acirc;m &ndash; Trường Đại học về thiền của Việt Nam</strong><em>&nbsp;&ndash;&nbsp;</em>Nơi đ&acirc;y l&agrave; Thiền Viện lớn nhất của Phật gi&aacute;o Việt Nam, do Thiền Sư Th&iacute;ch Thanh Từ l&agrave;m viện trưởng. Thiền Viện tọa lạc tr&ecirc;n quả đồi Phượng Ho&agrave;ng hướng về hồ Tuyền L&acirc;m l&agrave; nơi thu h&uacute;t nhiều du kh&aacute;ch trong v&agrave; ngo&agrave;i nước.</li>
                    </ul>
                    
                    <p>Xe tiếp tục đưa qu&yacute; kh&aacute;ch về trung t&acirc;m th&agrave;nh phố, ăn trưa, nhận ph&ograve;ng, nghỉ ngơi.&nbsp;<strong>Ăn trưa tại nh&agrave; h&agrave;ng.</strong></p>
                    
                    <p><strong>Chiều</strong>: Đo&agrave;n khởi h&agrave;nh tiếp tục chương tr&igrave;nh tham quan Đ&agrave; Lạt.</p>
                    
                    <p><strong>&ndash; Thung Lũng T&igrave;nh Y&ecirc;u:&nbsp;</strong>Nơi hội tụ đầy đủ n&uacute;i rừng, s&ocirc;ng suối, th&ocirc;ng, hoa v&agrave; l&agrave; điểm nhấn kh&ocirc;ng thể bỏ qua khi đến Đ&agrave; Lạt. Đến đ&acirc;y, qu&yacute; kh&aacute;ch&nbsp;sẻ được trải nghiệm những hoạt động th&uacute; vị sau:</p>
                    
                    <ul>
                        <li>Kh&aacute;m ph&aacute;&nbsp;<strong>m&ecirc; cung t&igrave;nh y&ecirc;u&nbsp;</strong>&ndash; được biết đến l&agrave; m&ecirc; cung sinh th&aacute;i lớn nhất Việt Nam</li>
                        <li>Trải nghiệm dịch vụ đi&nbsp;<strong>xe điện</strong>&nbsp;tham quan to&agrave;n bộ khu du lịch (ho&agrave;n to&agrave;n miễn ph&iacute;).</li>
                        <li>Trải nghiệm những gi&acirc;y ph&uacute;t&nbsp;<strong>bồng bềnh tr&ecirc;n Pedalo</strong>&nbsp;(đạp vịt) tham quan một v&ograve;ng hồ Đa Thiện trữ t&igrave;nh, thơ mộng chắc chắn sẻ tạo cho du kh&aacute;ch một cảm gi&aacute;c m&aacute;t mẻ sảng kho&aacute;i. (ho&agrave;n to&agrave;n miễn ph&iacute;).</li>
                        <li>Dừng ch&acirc;n, chụp h&igrave;nh tại&nbsp;<strong>khu 30 kỳ quan thế giới, vườn hồng hạc, vườn hoa cẩm t&uacute; cầu</strong>&hellip; V&agrave; rất nhiều hoạt động th&uacute; vị kh&aacute;c đang chờ đ&oacute;n qu&yacute; kh&aacute;ch.</li>
                    </ul>
                    
                    <p><strong><em>&ndash;</em>&nbsp;Quảng trường L&acirc;m Vi&ecirc;n</strong>: Kh&ocirc;ng chỉ mang đến kh&ocirc;ng gian rộng lớn, tho&aacute;ng m&aacute;t với nhiều hoạt động giải tr&iacute; m&agrave; Quảng Trường L&acirc;m Vi&ecirc;n c&ograve;n ấn tượng với c&ocirc;ng tr&igrave;nh nghệ thuật khổng lồ với khối b&ocirc;ng hoa d&atilde; quỳ v&agrave; khối nụ hoa được thiết kế bằng k&iacute;nh m&agrave;u lạ mắt. Từ Quảng trường qu&yacute; kh&aacute;ch tự do thoải m&aacute;i ngắm&nbsp;<strong>Hồ Xu&acirc;n Hương</strong>&nbsp;l&uacute;c ho&agrave;ng h&ocirc;n xuống. Đừng bỏ lỡ những bức h&igrave;nh tuyệt đẹp về một Đ&agrave; Lạt l&uacute;c ho&agrave;ng h&ocirc;n.</p>
                    
                    <p><strong>Tối:&nbsp;</strong>Đo&agrave;n d&ugrave;ng bữa tối nh&agrave; h&agrave;ng. Sau bữa tối, xe đưa qu&yacute; kh&aacute;ch tới:</p>
                    
                    <ul>
                        <li><strong>Chợ đ&ecirc;m Đ&agrave; Lạt</strong>&nbsp;tham quan, mua sắm hoặc thưởng thức ẩm thực đường phố nơi đ&acirc;y. Qu&yacute; kh&aacute;ch tự t&uacute;c về kh&aacute;ch sạn. Nghỉ đ&ecirc;m tại Đ&agrave; Lạt.</li>
                    </ul>',
                'tour_id' => 3, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            ['day' => 2, 'name' => 'JEJU (Ăn sáng, trưa, tối)',
                'content' => '<p><strong>S&aacute;ng</strong>: Sau khi thưởng thức buổi s&aacute;ng tại kh&aacute;ch sạn, đo&agrave;n khởi h&agrave;nh tham quan chương tr&igrave;nh ng&agrave;y thứ 2 với chủ đề:&nbsp;<strong>&ldquo;Đ&agrave; Lạt- T&igrave;nh y&ecirc;u v&agrave; nổi nhớ&rdquo;</strong></p>

                    <ul>
                        <li><strong>Ch&ugrave;a Linh Phước (Ch&ugrave;a Ve Chai):&nbsp;</strong>Một c&ocirc;ng tr&igrave;nh kiến tr&uacute;c độc đ&aacute;o với nghệ thuật gh&eacute;p s&agrave;nh, sứ v&agrave; c&aacute;c mảnh ve chai. Ng&ocirc;i ch&ugrave;a cũng giữ 11 kỷ lục của Việt Nam đang chờ qu&yacute; kh&aacute;ch đến t&igrave;m hiểu.</li>
                        <li><strong>Check in Cầu Thang V&ocirc; Cực &ndash; N&ocirc;ng Trại Vui Vẻ</strong>, một điểm đến rất được giới trẻ y&ecirc;u th&iacute;ch. Qu&yacute; kh&aacute;ch sẽ được thưởng thức hương vị c&agrave; ph&ecirc; T&acirc;y Nguy&ecirc;n qua ng&ocirc;i nh&agrave; k&iacute;nh &ocirc;m trọn khung cảnh n&ocirc;ng trại b&ecirc;n ngo&agrave;i. Đặc biệt nhất, qu&yacute; kh&aacute;ch sẽ được trải nghiệm Nấc thang l&ecirc;n Thi&ecirc;n Đường &ndash; chắc chắn qu&yacute; kh&aacute;ch sẽ c&oacute; những tấm h&igrave;nh y&ecirc;u th&iacute;ch.</li>
                        <li><strong>Cổng Trời thần th&aacute;nh &ndash;&nbsp;</strong>nếu bạn đ&atilde; xem MV Lạc Tr&ocirc;i rất đ&igrave;nh đ&aacute;m của ca sỹ trẻ Sơn T&ugrave;ng MTP, ch&uacute;ng t&ocirc;i sẽ gi&uacute;p bạn c&oacute; những bức h&igrave;nh cực chất của cảnh quay trong MV n&agrave;y (Nhưng kh&ocirc;ng phải chạy về Bảo Lộc đ&acirc;u nh&eacute;???)</li>
                    </ul>
                    
                    <p><strong>Qu&yacute; kh&aacute;ch ăn trưa tại nh&agrave; h&agrave;ng sau đ&oacute; về kh&aacute;ch sạn nghỉ ngơi.</strong></p>
                    
                    <p><strong>Chiều:&nbsp;</strong>Đo&agrave;n khởi h&agrave;nh tham quan:</p>
                    
                    <ul>
                        <li><strong>Trung t&acirc;m lưu trữ quốc gia IV:</strong>&nbsp;Trước kia l&agrave;&nbsp;<strong>Biệt điện Trần Lệ Xu&acirc;n,&nbsp;&nbsp;</strong>ng&agrave;y nay đ&acirc;y l&agrave; điểm đến độc đ&aacute;o &ndash; kết hợp nhiều ng&agrave;nh: Lưu trữ &ndash; Lịch sử &ndash; Bảo t&agrave;ng v&agrave; Du lịch. Khi viếng thăm, qu&yacute; kh&aacute;ch c&oacute; cơ hội được xem sự t&aacute;i hiện sinh động cuộc đấu tranh ki&ecirc;n cường trong cuộc chiến gi&agrave;nh độc lập của T&acirc;y Nguy&ecirc;n cũng như tham quan t&igrave;m hiểu nơi lưu trữ khối t&agrave;i liệu&nbsp;<strong>Mộc bản triều Nguyễn</strong>&nbsp;được UNESCO c&ocirc;ng nhận l&agrave; di sản tư liệu thế giới.</li>
                        <li><strong>Đường Hầm Đi&ecirc;u Khắc &ndash;&nbsp;</strong>Nơi t&aacute;i hiện về lịch sử th&agrave;nh phố Đ&agrave; Lạt từ thủa ban sơ cho tới một Đ&agrave; Lạt năng động v&agrave; hiện đại như b&acirc;y giờ. Qu&yacute; kh&aacute;ch sẽ thấy được một Đ&agrave; Lạt thu nhỏ qua chất liệu đặc biệt v&agrave; kỷ thuật đỉnh cao của đi&ecirc;u khắc.</li>
                    </ul>
                    
                    <p><strong>Tối:</strong>&nbsp;Qu&yacute; kh&aacute;ch thưởng thức&nbsp;<strong>buffet rau&nbsp;</strong>c&oacute; một kh&ocirc;ng hai tại nh&agrave; h&agrave;ng Leguda tọa lạc tr&ecirc;n đồi Robin thơ mộng. Sau bữa tối, qu&yacute; kh&aacute;ch khởi h&agrave;nh về kh&aacute;ch sạn. (hoặc lựa chọn chương tr&igrave;nh giao lưu Cồng Chi&ecirc;ng T&acirc;y Nguy&ecirc;n &ndash; chi ph&iacute; tự t&uacute;c. Xe v&agrave; hdv hổ trợ đo&agrave;n di chuyển đến khu vực cồng chi&ecirc;ng c&aacute;ch th&agrave;nh phố 14km).</p>',
                'tour_id' => 3, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            ['day' => 3, 'name' => 'JEJU (Ăn sáng, trưa, tối)',
                'content' => '<p><strong>S&aacute;ng:&nbsp;</strong>Qu&yacute; kh&aacute;ch ăn s&aacute;ng tại kh&aacute;ch sạn sau đ&oacute; l&agrave;m thủ tục trả ph&ograve;ng. Xe đ&oacute;n đo&agrave;n đưa ra s&acirc;n bay Li&ecirc;n Khương l&agrave;m thủ tục đ&aacute;p chuyến bay về Huế (<strong>10:05</strong><strong>&nbsp;&ndash; 1</strong><strong>1</strong><strong>h15</strong>). Đến Huế, xe v&agrave; hdv đ&oacute;n đo&agrave;n về trung t&acirc;m th&agrave;nh phố về điểm đ&oacute;n ban đầu. Kết th&uacute;c chương tr&igrave;nh, Xin ch&agrave;o v&agrave; hẹn gặp lại qu&yacute; kh&aacute;ch trong c&aacute;c chương tr&igrave;nh sau!</p>',
                'tour_id' => 3, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            //tour_id: 4 => 3N-2Đ
            ['day' => 1, 'name' => 'ĐẢO NGỌC PHÚ  QUỐC CHÀO ĐÓN (ĂN TỐI)',
                'content' => '<p>18:20: Xe v&agrave; HDV đ&oacute;n qu&yacute; kh&aacute;ch tr&ecirc;n chuyến bay Đ&agrave; Nẵng Ph&uacute; Quốc (16:30 &ndash; 18:20). Đến Ph&uacute; Quốc, xe đ&oacute;n qu&yacute; kh&aacute;ch về kh&aacute;ch sạn nhận ph&ograve;ng, ăn tối, nghỉ ngơi</p>

                    <p><strong>Tối:</strong>&nbsp;Tự do thư gi&atilde;n hoặc Tham quan&nbsp;<strong>chợ đ&ecirc;m Dinh Cậu</strong>.</p>',
                'tour_id' => 4, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            ['day' => 2, 'name' => 'CÂU CÁ VÀ NGẮM SAN HÔ TẠI QUẦN ĐẢO AN THỚI (ĂN SÁNG, TRƯA, TỐI)',
                'content' => '<p><strong>S&aacute;ng:</strong>&nbsp;Sau khi d&ugrave;ng điểm t&acirc;m s&aacute;ng tại kh&aacute;ch sạn, xe đưa đo&agrave;n tiến về ph&iacute;a Nam, l&ecirc;n t&agrave;u c&acirc;u c&aacute;.</p>

                    <ul>
                        <li><strong>Tr&ecirc;n t&agrave;u:</strong>&nbsp; Với đội ngủ thủy thủ đo&agrave;n &amp; HDV địa phương gi&agrave;u kinh nghiệm, qu&yacute; kh&aacute;ch sẽ tận hưởng từng khoảnh khắc đ&aacute;ng nhớ khi ch&iacute;nh tay m&igrave;nh bu&ocirc;ng c&acirc;u bắt được những ch&uacute; c&aacute; bồng m&uacute; ph&agrave;m ăn trong c&aacute;c rạn san h&ocirc; tại h&ograve;n Dừa, h&ograve;n Rỏi, h&ograve;n Thơm &hellip;.T&agrave;u được trang bi đầy đủ dụng cụ: &aacute;o phao, k&iacute;nh lặn, ch&acirc;n vịt, ống thở&hellip; để qu&yacute; kh&aacute;ch&nbsp;<strong>lặn</strong>&nbsp;<strong>ngắm san h&ocirc;.</strong></li>
                    </ul>
                    
                    <p><strong>Trưa:&nbsp;</strong>Đo&agrave;n d&ugrave;ng cơm trưa tr&ecirc;n t&agrave;u với hải sản v&ugrave;ng biển.</p>
                    
                    <ul>
                        <li>Tr&ecirc;n đường trở về, kh&aacute;ch tiếp tục dừng ch&acirc;n tại một trong những b&atilde;i biển đẹp nhất Ph&uacute; Quốc với b&atilde;i c&aacute;t d&agrave;i thẳng tắp, trắng mịn:&nbsp;<strong>B&atilde;i Sao</strong>. Đo&agrave;n tiếp tục tham quan</li>
                        <li>&nbsp;<strong>Nh&agrave; t&ugrave; Ph&uacute; Quốc:</strong>&nbsp;khu di t&iacute;ch lịch sử &ndash; nơi chứng kiến bao tội &aacute;c của thực d&acirc;n Ph&aacute;p v&agrave; đế quốc Mỹ khi giam cầm hơn 32 ng&agrave;n t&ugrave; binh chiến tranh (c&oacute; l&uacute;c l&ecirc;n đến con số 40 ng&agrave;n t&ugrave; binh) trong khoảng thời gian tồn tại chưa đầy 6 năm.</li>
                        <li><strong>Thiền Viện Tr&uacute;c L&acirc;m&nbsp;</strong><strong>Hộ Quốc&nbsp; :</strong>&nbsp;Ng&ocirc;i ch&ugrave;a c&oacute; vị tr&iacute; đắc địa &ndash; lưng tựa n&uacute;i mặt hướng ra biển với kiến tr&uacute;c cảnh quan đặc sắc, chắc chắn sẻ l&agrave; nơi để lại cho qu&yacute; kh&aacute;ch những ấn tượng tốt đẹp. Đo&agrave;n trở về kh&aacute;ch sạn tự do tắm biển/ hồ bơi.</li>
                    </ul>
                    
                    <p><strong>Tối:</strong>&nbsp;Xe đưa đo&agrave;n đi ăn tối với những m&oacute;n ăn đặc sản Ph&uacute; Quốc. Đo&agrave;n về lại kh&aacute;ch sạn tự do kh&aacute;m ph&aacute; Đảo Ngọc về đ&ecirc;m.</p>',
                'tour_id' => 4, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            ['day' => 3, 'name' => 'ẤN TƯỢNG ĐẢO NGỌC (ĂN SÁNG, TRƯA)',
                'content' => '<p><strong>S&aacute;ng:</strong>&nbsp;Q&uacute;y kh&aacute;ch d&ugrave;ng điểm t&acirc;m s&aacute;ng. Q&uacute;y kh&aacute;ch tự do tham quan:</p>

                    <ul>
                        <li><strong>Chợ Dương Đ&ocirc;ng</strong>, l&agrave; trung t&acirc;m trao đổi, vận chuyển h&agrave;ng h&oacute;a ch&iacute;nh của đảo, c&oacute; lịch sử ph&aacute;t triển l&acirc;u đời v&agrave; lớn nhất Ph&uacute; Quốc.</li>
                        <li>Q&uacute;y kh&aacute;ch tự do mua sắm đặc sản về l&agrave;m qu&agrave; cho gia đ&igrave;nh, người th&acirc;n. Hoặc qu&yacute; kh&aacute;ch c&oacute; thể lựa chọn c&aacute;c chương tr&igrave;nh sau (C&ocirc;ng ty du lịch Đại B&agrave;ng hổ trợ chi ph&iacute; vận chuyển đến 1 trong 2 khu du lịch b&ecirc;n dưới)</li>
                        <li><strong>Lựa chọn 1</strong><strong>:</strong>&nbsp;Trải Nghiệm C&aacute;p Treo h&ograve;n thơm ( Chi Ph&iacute; Tự t&uacute;c )</li>
                        <li><strong>Lựa chọn 2</strong><strong>:</strong>&nbsp;Qu&yacute; Kh&aacute;ch tự do Trải nghiệm tắm b&ugrave;n Galina ( Chi Ph&iacute; Tự t&uacute;c )</li>
                    </ul>
                    
                    <p><strong>Trưa:</strong>&nbsp;Đo&agrave;n ăn trưa, trả ph&ograve;ng kh&aacute;ch sạn</p>
                    
                    <ul>
                        <li><strong>Chiều:</strong>Tham quan c&aacute;c l&agrave;ng nghề truyền thống nổi tiếng tr&ecirc;n đảo.</li>
                        <li><strong>Hồ ti&ecirc;u Ph&uacute; Quốc:</strong>&nbsp;t&igrave;m hiểu về c&aacute;ch trồng ti&ecirc;u tại vườn. Ti&ecirc;u Ph&uacute; Quốc nổi tiếng với hạt to, đen v&agrave; cay thơm. Du kh&aacute;ch c&oacute; thể mua ti&ecirc;u về l&agrave;m qu&agrave; tại nh&agrave; vườn.</li>
                        <li><strong>Nh&agrave; th&ugrave;ng nước mắm:</strong>&nbsp;T&igrave;m hiểu c&aacute;ch ủ v&agrave; chế biến nước mắm c&aacute; cơm rất nổi tiếng trong v&agrave; ngo&agrave;i nước theo c&aacute;ch l&agrave;m truyền thống của người d&acirc;n Ph&uacute; Quốc với h&agrave;m lượng dinh dưỡng rất cao.</li>
                        <li><strong>Rượu sim :</strong>&nbsp;loại rượu vang l&agrave;m từ tr&aacute;i sim rừng ch&iacute;n.Gặp gỡ v&agrave; tr&ograve; chuyện với người đầu ti&ecirc;n chế biến ra rượu sim sơn tại cơ sở sản xuất rượu sim Bảy G&aacute;o.</li>
                        <li><strong>Suối Tranh :</strong>&nbsp;Con suối bắt nguồn từ d&atilde;y n&uacute;i H&agrave;m Ninh v&agrave; chỉ c&oacute; nước từ th&aacute;ng 05 &ndash; th&aacute;ng 10.</li>
                        <li><strong>Khu nu&ocirc;i trồng Nấm</strong>&nbsp;&ndash; Nơi nu&ocirc;i trồng nấm &ndash; v&agrave; đ&ocirc;ng tr&ugrave;ng hạ thảo &ndash; Qu&yacute; kh&aacute;ch tham quan v&agrave; mua l&agrave;m qu&agrave; tại đ&acirc;y.</li>
                        <li><strong>L&agrave;ng ch&agrave;i H&agrave;m Ninh:</strong>&nbsp;l&agrave;ng ch&agrave;i cổ xưa của người d&acirc;n tr&ecirc;n đảo. Nơi du kh&aacute;ch c&oacute; thể mua hải sản kh&ocirc;, tươi,qu&agrave; lưu niệm ở đ&acirc;y với gi&aacute; rất rẻ.(Chi ph&iacute; tự t&uacute;c)</li>
                        <li><strong>Ch&ugrave;a Sư Mu&ocirc;n:</strong>&nbsp;ng&ocirc;i ch&ugrave;a cổ xinh đẹp với kh&ocirc;ng gian tho&aacute;ng m&aacute;t, thanh tịnh.</li>
                    </ul>
                    
                    <p><strong>Tối:</strong>&nbsp;Đo&agrave;n khởi h&agrave;nh ra s&acirc;n bay Dương Đ&ocirc;ng l&agrave;m thủ tục cho chuyến bay Ph&uacute; Quốc &ndash; Đ&agrave; Nẵng (19:05-20;55) ( tự t&uacute;c ăn tối tại s&acirc;n bay).</p>',
                'tour_id' => 4, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            //tour_id: 5 => 1N
            ['day' => 1, 'name' => 'DU LỊCH HÒN KHÔ – QUY NHƠN',
                'content' => '<p><strong>S&aacute;ng:&nbsp;</strong>Đo&agrave;n d&ugrave;ng bữa s&aacute;ng.</p>

                    <ul>
                        <li><strong>08h00:&nbsp;</strong>Tham quan quan<strong>&nbsp;Nhơn L&yacute;,&nbsp;</strong>một địa danh nổi tiếng của&nbsp; xinh đẹp của th&agrave;nh phố Quy Nhơn.Tr&ecirc;n h&agrave;nh tr&igrave;nh qu&yacute; kh&aacute;ch tham quan v&agrave; chụp ảnh tại&nbsp;<strong>cầu Thị Nại&nbsp;</strong>&ndash; cầu vượt biển d&agrave;i nhất Việt Nam (2500m).</li>
                    </ul>
                    
                    <p>Tiếp tục h&agrave;nh tr&igrave;nh sang<strong>&nbsp;Nhơn L&yacute; &ndash;&nbsp;</strong>nơi ngự trị của những đồi c&aacute;t d&agrave;i v&agrave; đẹp.</p>
                    
                    <ul>
                        <li><strong>09h30:&nbsp;</strong>Qu&yacute; kh&aacute;ch đến nh&agrave; h&agrave;ng, qu&yacute; kh&aacute;ch thay đồ. HDV đưa qu&yacute; kh&aacute;ch xuống cano.Qu&yacute; kh&aacute;ch được trải nghiệm thuyền th&uacute;ng để di chuyển ra Cano cao tốc<br />
                        Bắt đầu h&agrave;nh tr&igrave;nh 20 ph&uacute;t để đến&nbsp;<strong>B&atilde;i Biển Kỳ Co Hoang sơ.</strong></li>
                        <li>Qu&yacute; kh&aacute;ch thoải m&aacute;i tắm, lặn biển, chụp ảnh, bắt Nhum biển.&nbsp;<strong>(Trương hợp nếu thời tiết xấu th&igrave; Qu&yacute; kh&aacute;ch sẽ sang đảo KỲ CO bằng đường &ocirc; t&ocirc;)</strong></li>
                    </ul>
                    
                    <p>D&ugrave;ng bữa trưa ngay tr&ecirc;n x&atilde; Nhơn L&yacute; với thực đơn tham khảo như sau:</p>
                    
                    <ul>
                        <li><strong>Soup rong biển nấu t&ocirc;m</strong></li>
                        <li><strong>G&oacute;i ốc trộn rau c&acirc;u ch&acirc;n vịt</strong></li>
                        <li><strong>Mực tươi hấp gừng</strong></li>
                        <li><strong>H&agrave;u nướng mỡ h&agrave;nh</strong></li>
                        <li><strong>T&ocirc;m s&uacute; hấp l&aacute; dứa</strong></li>
                        <li><strong>Ch&aacute;o hải sản</strong></li>
                        <li><strong>Cơm chi&ecirc;n muối ớt</strong></li>
                        <li><strong>C&aacute; chẽm hấp hongkong + b&aacute;nh tr&aacute;ng / Lẫu c&aacute; g&aacute;y + b&uacute;n</strong></li>
                        <li><strong>Tr&aacute;i c&acirc;y tr&aacute;ng miệng</strong></li>
                    </ul>
                    
                    <p><strong>Chiều:&nbsp;</strong>Sau&nbsp;<strong>13h</strong>&nbsp;nghỉ ngơi, qu&yacute; kh&aacute;ch khởi h&agrave;nh tham quan<strong>&nbsp;Eo Gi&oacute;,&nbsp;</strong>chụp h&igrave;nh lưu niệm tại đ&acirc;y.</p>
                    
                    <p>Sau đ&oacute; xe đưa qu&yacute; kh&aacute;ch về lại trung t&acirc;m th&agrave;nh phố<strong>.&nbsp;</strong>Kết th&uacute;c chương tr&igrave;nh tour. Ch&agrave;o tạm biệt. Hẹn gặp lại.</p>',
                'tour_id' => 5, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            //tour_id: 6 => 2N-1Đ
            ['day' => 1, 'name' => 'HUẾ/ ĐÀ NẴNG/ SÂN BAY CHU LAI – QUẢNG NGÃI – ĐẢO LÝ SƠN (ĂN TRƯA, TỐI)',
                'content' => '<p><strong>S&aacute;ng:</strong>&nbsp;Xe v&agrave; Hdv đ&oacute;n đo&agrave;n tại điểm hẹn (Đ&oacute;n tại Huế l&uacute;c:&nbsp;<strong>06:00</strong>, Đ&agrave; Nẵng l&uacute;c:&nbsp;<strong>08:00</strong>, Chu Lai:&nbsp;<strong>10:30</strong>) Khoảng 12h đến Quảng Ng&atilde;i, ăn trưa tại nh&agrave; h&agrave;ng b&atilde;i biển Mỹ Kh&ecirc;.</p>

                    <p><strong>Chiều:</strong>&nbsp;13h00 xe đưa qu&yacute; kh&aacute;ch ra&nbsp;<strong>Cảng Sa Kỳ</strong>&nbsp;l&agrave;m thủ tục chuyến t&agrave;u cao tốc ra đảo L&yacute; Sơn chuyến. Đến L&yacute; Sơn, xe đ&oacute;n đo&agrave;n đưa về nhận ph&ograve;ng, nghỉ ngơi. Xe đưa đo&agrave;n tham quan:</p>
                    
                    <ul>
                        <li><strong>Ch&ugrave;a hang</strong>&nbsp;&ndash; Gọi l&agrave; ch&ugrave;a Hang v&igrave; ch&ugrave;a nằm trong một hang đ&aacute; lớn nhất trong hệ thống hang động ở L&yacute; Sơn, l&agrave; một trong những ng&ocirc;i ch&ugrave;a độc đ&aacute;o nhất Việt Nam.</li>
                        <li><strong>Hang C&acirc;u</strong>&nbsp;&ndash; Hang C&acirc;u được s&oacute;ng v&agrave; gi&oacute; biển b&agrave;o m&ograve;n, &ldquo;kho&eacute;t s&acirc;u&rdquo; v&agrave;o l&ograve;ng n&uacute;i v&agrave; h&igrave;nh th&agrave;nh c&aacute;ch nay h&agrave;ng ng&agrave;n năm từ nham thạch. Khung cảnh ở đ&acirc;y&nbsp; hoang sơn v&agrave; mang một vẻ đẹp rất thơ mộng, quyến rũ h&uacute;t hồn du kh&aacute;ch.</li>
                        <li><strong>Nh&agrave; trưng b&agrave;y Hải Đội Ho&agrave;ng Sa Ki&ecirc;m quản Bắc Hải</strong>&nbsp;&ndash; nơi lưu giữ nhiều chứng cứ về chủ quyền biển đảo của Việt Nam.</li>
                    </ul>
                    
                    <p><strong>Tối:</strong>&nbsp;Qu&yacute; kh&aacute;ch ăn tối với đặc sản L&yacute; Sơn như: gỏi tỏi L&yacute; Sơn, ốc s&agrave; cừ, &hellip;.Qu&yacute; kh&aacute;ch tự do tham quan L&yacute; Sơn về đ&ecirc;m, thưởng thức hải sản hoặc tham gia:</p>
                    
                    <ul>
                        <li><strong>Chương tr&igrave;nh c&acirc;u c&aacute; đ&ecirc;m</strong>, C&acirc;u c&aacute;. Đ&acirc;y l&agrave; một chương tr&igrave;nh mới v&agrave; hết sức hấp dẫn d&agrave;nh cho du kh&aacute;ch khi ở lại tại đảo L&yacute; Sơn. (Chi ph&iacute; 170.000Đ/NGƯỜI)</li>
                    </ul>',
                'tour_id' => 6, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            ['day' => 2, 'name' => 'LÝ SƠN – QUẢNG NGÃI – ĐÀ NẴNG – HUẾ (ĂN SÁNG, TRƯA, TỐI)',
                'content' => '<p><strong>8h00:</strong>&nbsp;Qu&yacute; kh&aacute;ch l&ecirc;n Ca N&ocirc; khởi h&agrave;nh sang tham quan&nbsp;<strong>Đảo B&eacute; &ndash; hay c&ograve;n gọi l&agrave; đảo An B&igrave;nh</strong></p>

                    <ul>
                        <li>Đến Đảo B&eacute;<strong>&nbsp;&ndash;&nbsp;</strong>qu&yacute; kh&aacute;ch sẽ được tham quan: cuộc sống sinh hoạt của người d&acirc;n l&agrave;ng ch&agrave;i, tham quan ruộng h&agrave;nh tỏi&hellip;</li>
                        <li>Qu&yacute; kh&aacute;ch sử dụng dịch vụ xe điện để đến với&nbsp;<strong>b&atilde;i biển B&igrave;nh An,&nbsp;</strong>qu&yacute; kh&aacute;ch sẽ được tự do tắm biển, vui chơi thoải m&aacute;i tại b&atilde;i biển đẹp nhất ở L&yacute; Sơn với c&aacute;t trắng mịn trải d&agrave;i.</li>
                        <li>Sau đ&oacute;, qu&yacute; kh&aacute;ch sẽ được tiếp tục chương tr&igrave;nh&nbsp;<strong>lặn ngắm san h&ocirc;</strong>&nbsp;(chi ph&iacute; tự t&uacute;c 80.000đ/người) &ndash; qu&yacute; kh&aacute;ch sẽ được người d&acirc;n địa phương trang bị với những chiếc thuyền th&uacute;ng, &aacute;o phao v&agrave; bộ lặn để thỏa sức ngắm v&ocirc; v&agrave;n lo&agrave;i thủy, hải sản dưới đại dương m&ecirc;nh m&ocirc;ng bao la. 11h30 quay về lại đảo lớn.</li>
                    </ul>
                    
                    <p>Ăn trưa tại nh&agrave; h&agrave;ng.</p>
                    
                    <p><strong>Chiều:&nbsp;</strong>13h30 xe đưa qu&yacute; kh&aacute;ch ra cảng L&yacute; Sơn,l&agrave;m thủ tục chuyến t&agrave;u cao tốc về th&agrave;nh phố Quảng Ng&atilde;i l&uacute;c&nbsp;<strong>14h00</strong>. Khoảng&nbsp;<strong>15h00</strong>&nbsp;đến cảng Sa Kỳ, xe đ&oacute;n qu&yacute; kh&aacute;ch khởi h&agrave;nh về Đ&agrave; Nẵng/ Huế. Khoảng 18h đến Đ&agrave; Nẵng trả kh&aacute;ch.</p>
                    
                    <p>Đo&agrave;n tiếp tục khởi h&agrave;nh về Huế dừng ăn nhẹ tại Lăng C&ocirc;.&nbsp;<strong>21h00</strong>&nbsp;đo&agrave;n đến Huế.</p>',
                'tour_id' => 6, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            //tour_id: 7 => 5N-4Đ
            ['day' => 1, 'name' => 'ĐÀ NẴNG – HỒNG KONG (ĂN TỐI)',
                'content' => '<p><strong>S&aacute;ng</strong>: Qu&yacute; kh&aacute;ch c&oacute; mặt tại s&acirc;n bay Đ&agrave; Nẵng l&agrave;m thủ tục đ&aacute;p chuyến bay&nbsp;<strong>(</strong><strong>10:10 &ndash; 12:55</strong><strong>)</strong>&nbsp;đi HongKong (Q&uacute;y kh&aacute;ch tự t&uacute;c bữa trưa tr&ecirc;n m&aacute;y bay)</p>

                    <p><strong>Chiều</strong>: Đến s&acirc;n bay quốc tế tại Hong Kong, xe c&ugrave;ng hướng dẫn địa phương sẽ đ&oacute;n đo&agrave;n đi tham quan:</p>
                    
                    <ul>
                        <li><strong>Cầu Thanh M&atilde;</strong>&nbsp;&ndash; một trong những cầu treo đẹp nhất Ch&acirc;u &Aacute;.</li>
                        <li><strong>Cảng biển Tsim Sha Tsui (Ti&ecirc;m Sa Chủy)</strong></li>
                        <li>Tham quan đại lộ ng&ocirc;i sao&nbsp;<strong>Avenue of the stars &ndash;&nbsp;</strong>đại lộ vinh danh những ng&ocirc;i sao lớn của ng&agrave;nh điện ảnh HongKong</li>
                    </ul>
                    
                    <p><strong>Tối</strong>: Ăn tối tại nh&agrave; h&agrave;ng địa phương. Sau bữa tối qu&yacute; kh&aacute;ch tự do mua sắm tại Chợ Qu&yacute; B&agrave;.</p>',
                'tour_id' => 7, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            ['day' => 2, 'name' => 'HỒNG KONG – DISNEYLAND (ĂN SÁNG,TRƯA,TỐI)',
                'content' => '<p><strong>S&aacute;ng:</strong>&nbsp;Ăn s&aacute;ng tại kh&aacute;ch sạn. Đo&agrave;n đ&oacute;n ng&agrave;y mới tại Hongkong với chương tr&igrave;nh tham quan:</p>

                    <ul>
                        <li><strong>N&uacute;i Th&aacute;i B&igrave;nh &ndash;&nbsp;</strong>Ngắm to&agrave;n cảnh Hongkong từ tr&ecirc;n cao</li>
                        <li><strong>Vịnh nước cạn, Miếu Thần T&agrave;i, Miếu huỳnh đại ti&ecirc;n Thiền viện Chi Li&ecirc;n &ndash;&nbsp;</strong>được x&acirc;y dựng độc đ&aacute;o với to&agrave;n bộ c&ocirc;ng tr&igrave;nh bằng gỗ kh&ocirc;ng dung một c&acirc;y định n&agrave;o để x&acirc;u kết c&aacute;c mối nối với nhau theo kiến tr&uacute;c đời Đường</li>
                        <li><strong>Vườn Nam Li&ecirc;n &ndash;</strong>&nbsp;mang n&eacute;t kiến tr&uacute;c Nhật Bản nổi bật với một ng&ocirc;i đền h&igrave;nh b&aacute;t gi&aacute;c tọa lạc tr&ecirc;n một hồ nước nhỏ với m&agrave;u v&agrave;ng &oacute;ng ảnh, tại đ&acirc;y Qu&yacute; kh&aacute;ch c&ograve;n c&oacute; thể chi&ecirc;m ngưỡng h&agrave;ng ng&agrave;n c&acirc;y bonsai c&aacute;c loại</li>
                    </ul>
                    
                    <p><strong>Trưa:</strong>&nbsp;Sau khi ăn trưa, đo&agrave;n khởi h&agrave;nh đi tham quan:</p>
                    
                    <ul>
                        <li><strong>Disneyland</strong>&nbsp;&ndash; c&ocirc;ng vi&ecirc;n chủ đề giải tr&iacute; nổi tiếng tr&ecirc;n thế giới. Qu&yacute; kh&aacute;ch sẽ đắm m&igrave;nh trong vũ hội h&oacute;a trang, tham quan c&aacute;c cửa h&agrave;ng đồ chơi, chụp h&igrave;nh c&ugrave;ng c&aacute;c nh&acirc;n vật hoạt h&igrave;nh vui&nbsp; nhộn, thưởng thức những vở kịch vui nhộn.</li>
                        <li><strong>Lion King</strong>&nbsp;&ndash; show diễn ho&agrave;nh tr&aacute;ng với những nh&acirc;n vật rối ngộ nghĩnh.</li>
                        <li>Đi xe điện tr&ecirc;n kh&ocirc;ng để ngắm nh&igrave;n to&agrave;n cảnh khu Disneyland</li>
                        <li><strong>Khu rừng Amazon</strong>&nbsp;&ndash; ngồi thuyền kh&aacute;m ph&aacute; thế giới động vật hoang d&atilde; với voi, h&agrave; m&atilde;, khỉ Gorilla v&agrave; đối mặt với c&aacute;c anh ch&agrave;ng thổ S&acirc;n hiếu chiến.</li>
                        <li><strong>Tarzan&rsquo;s Treehouse</strong>&nbsp;&ndash; tham quan ng&ocirc;i nh&agrave; tr&ecirc;n c&acirc;y của ch&uacute;a tể rừng xanh</li>
                        <li><strong>Space Mountain</strong>&nbsp;&ndash; th&aacute;m hiểm những h&agrave;nh tinh ngo&agrave;i kh&ocirc;ng gian bằng phi thuyền.</li>
                    </ul>
                    
                    <p>Tối: Qu&yacute; kh&aacute;ch ăn tối tại nh&agrave; h&agrave;ng. Qu&yacute; kh&aacute;ch nghỉ ngơi tại kh&aacute;ch sạn.</p>',
                'tour_id' => 7, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            ['day' => 3, 'name' => 'THẨM QUYẾN – TRUNG HOA CẨM TÚ (ĂN SÁNG, TRƯA, TÔI)',
                'content' => '<p><strong>S&aacute;ng</strong>: Qu&yacute; kh&aacute;ch ăn s&aacute;ng v&agrave; trả ph&ograve;ng kh&aacute;ch sạn. Khởi h&agrave;nh đi&nbsp;<strong>Th&acirc;m Quyến&nbsp;</strong>&ndash; đặc khu kinh tế mới của Trung Quốc. Đến nơi, hướng dẫn sẽ đ&oacute;n kh&aacute;ch tai cửa khẩu, l&agrave;m thủ tục nhập cảnh, v&agrave; đưa đo&agrave;n ăn trưa tại nh&agrave; h&agrave;ng địa phương.</p>

                    <p><strong>Chiều</strong>: Qu&yacute; kh&aacute;ch tham quan:</p>
                    
                    <ul>
                        <li><strong>C&ocirc;ng vi&ecirc;n Trung Hoa Cẩm T&uacute;</strong>&nbsp;&ndash; l&agrave; một trong những điểm du lịch nổi tiếng của th&agrave;nh phố&nbsp;Th&acirc;m Quyến. Đ&acirc;y l&agrave; nơi tập trung c&aacute;c di sản, kiến tr&uacute;c, danh thắng ti&ecirc;u biểu của đất nước Trung Quốc được thu nhỏ theo tỷ lệ 1/15 so với cảnh thật. C&ocirc;ng vi&ecirc;n được x&acirc;y dựng tr&ecirc;n diện t&iacute;ch khoảng 30ha với 120 m&ocirc; h&igrave;nh lớn nhỏ. C&aacute;c m&ocirc; h&igrave;nh kiến tr&uacute;c trong c&ocirc;ng vi&ecirc;n được b&agrave;i tr&iacute; h&agrave;i ho&agrave; với khung cảnh s&ocirc;ng hồ trong xanh uốn kh&uacute;c, n&uacute;i non tr&ugrave;ng điệp đẹp như tranh vẽ.</li>
                        <li>Sau đ&oacute; tự do mua sắm tại&nbsp;<strong>chợ Đ&ocirc;ng M&ocirc;n</strong>&nbsp;&ndash; một phố đi bộ tập trung c&aacute;c hoạt động mua sắm, thư gi&atilde;n v&agrave; thăm quan du lịch th&agrave;nh một thể thống nhất. Trong khu phố c&oacute; c&aacute;c trung t&acirc;m b&aacute;ch h&oacute;a lớn như b&aacute;ch h&oacute;a Mậu Nghiệp, chợ Thi&ecirc;n Hồng&hellip; b&ecirc;n cạnh đ&oacute; l&agrave; những cửa h&agrave;ng chuy&ecirc;n b&aacute;n c&aacute;c loại mặt h&agrave;ng đặc sắc ri&ecirc;ng với nhiều h&igrave;nh thức kh&aacute;c nhau..v..v</li>
                    </ul>
                    
                    <p><strong>Tối</strong>:&nbsp; Ăn tối tại nh&agrave; h&agrave;ng địa phương. Qu&yacute; kh&aacute;ch tự do nghỉ ngơi tại kh&aacute;ch sạn.</p>',
                'tour_id' => 7, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            ['day' => 4, 'name' => 'THẨM QUYẾN – CỦA SỔ THẾ GIỚI (ĂN SÁNG, TRƯA, TỐI)',
                'content' => '<p><strong>S&aacute;ng</strong>: Ăn s&aacute;ng tại kh&aacute;ch sạn. Qu&yacute; kh&aacute;ch tiếp tục chương tr&igrave;nh &nbsp;tham quan:</p>

                    <ul>
                        <li><strong>Bảo t&agrave;ng Thẩm Quyến</strong></li>
                        <li>Mua sắm tại cửa h&agrave;ng thuốc Bắc&nbsp;<strong>Bảo Đường</strong></li>
                    </ul>
                    
                    <p><strong>Trưa</strong>: Sau bữa trưa tại nh&agrave; h&agrave;ng, qu&yacute; kh&aacute;ch tham quan:</p>
                    
                    <ul>
                        <li><strong>C&ocirc;ng vi&ecirc;n Cửa sổ thế giới &ndash;&nbsp;</strong>với bộ sưu tập những m&ocirc; h&igrave;nh thu nhỏ, Ở đ&acirc;y c&oacute; khoảng tr&ecirc;n 100 c&ocirc;ng tr&igrave;nh x&acirc;y dựng m&ocirc; phỏng những kỳ quan văn ho&aacute;, di t&iacute;ch lịch&nbsp; sử, danh lam thắng cảnh, kiến tr&uacute;c nghệ thuật ti&ecirc;u biểu tr&ecirc;n thế giới. Điều kh&aacute; ấn tượng, c&aacute;c c&ocirc;ng tr&igrave;nh n&agrave;y kh&ocirc;ng chỉ được x&acirc;y dựng theo kiến tr&uacute;c nguy&ecirc;n bản, thu nhỏ k&iacute;ch thước n&ecirc;n tr&ocirc;ng giống thật, tỉ mỉ đến từng chi tiết m&agrave; c&ograve;n được sắp xếp một c&aacute;ch khoa học theo c&aacute;c khu vực tham quan ri&ecirc;ng như: Khu ch&acirc;u &Acirc;u, khu ch&acirc;u &Aacute;, khu ch&acirc;u Đại Dương, khu ch&acirc;u Phi, khu ch&acirc;u Mỹ, khu giải tr&iacute; khoa học hiện đại, khu đi&ecirc;u khắc, đường phố thế giới v&agrave; quảng trường thế giới. Qu&yacute; kh&aacute;ch c&oacute; thể chi&ecirc;m ngưỡng những m&ocirc; h&igrave;nh kỳ quan thế giới sống động, những di sản Lịch sử v&agrave; những cảnh quan nổi tiếng</li>
                    </ul>
                    
                    <p><strong>Tối</strong>: Ăn tối tại nh&agrave; h&agrave;ng, đo&agrave;n nghỉ ngơi hoặc tự do mua sắm tại c&aacute;c trung t&acirc;m thương mại</p>',
                'tour_id' => 7, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            ['day' => 5, 'name' => 'THẨM QUYẾN – HỒNG KONG – ĐÀ NẴNG (ĂN SÁNG)',
                'content' => '<p><strong>S&aacute;ng</strong>: Ăn s&aacute;ng tại kh&aacute;ch sạn. Qu&yacute; kh&aacute;ch l&agrave;m thủ tục trả ph&ograve;ng. Xe v&agrave; HDV đưa qu&yacute; kh&aacute;ch khởi h&agrave;nh về lại Hongkong, đ&aacute;p chuyển bay&nbsp;<strong>(14:05 &ndash; 14:50)</strong>&nbsp;về lại Đ&agrave; Nẵng.</p>

                    <p><strong>Kết th&uacute;c chương tr&igrave;nh tham quan!</strong></p>',
                'tour_id' => 7, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            //tour_id: 8 => 5N-4Đ
            ['day' => 1, 'name' => 'ĐÀ NẴNG – CAO HÙNG – ĐÀI TRUNG (ĂN TRƯA, TỐI)',
                'content' => '<p><strong>07h30:</strong>&nbsp;Hướng dẫn vi&ecirc;n đ&oacute;n Qu&yacute; kh&aacute;ch tại điểm hẹn trong&nbsp;<strong>s&acirc;n bay Quốc tế Đ&agrave; nẵng</strong>, l&agrave;m thủ tục cho đo&agrave;n đ&aacute;p chuyến bay đi&nbsp;<strong>Cao H&ugrave;ng &ndash; Đ&agrave;i Loan, DAD KHH &nbsp;(9:30 &ndash; 12h30)</strong>&nbsp;của h&atilde;ng H&agrave;ng kh&ocirc;ng Bamboo Airways.</p>

                    <p><strong>12h30, giờ địa phương:</strong>&nbsp;M&aacute;y bay hạ c&aacute;nh xuống s&acirc;n bay Cao H&ugrave;ng,&nbsp; xe v&agrave; HDV đ&oacute;n đo&agrave;n đi ăn trưa tại nh&agrave; h&agrave;ng địa phương, sau đ&oacute; đi thăm quan:</p>
                    
                    <ul>
                        <li><strong>Hồ Nhật Nguyệt</strong>&nbsp;&ndash; đ&acirc;y l&agrave; hồ nước thi&ecirc;n&nbsp;&nbsp;nhi&ecirc;n lớn nhất Đ&agrave;i Loan với một hồ lớn v&agrave; một hồ nhỏ liền nhau được bao bọc bởi m&agrave;u xanh bạt ng&agrave;n của những d&atilde;y n&uacute;i xung quanh, nơi đ&acirc;y đ&atilde; từng l&agrave; điểm nghỉ dưỡng ưng &yacute; nhất của Tưởng Giới Thạch.</li>
                        <li><strong>Miếu Văn V&otilde; &ndash;</strong>&nbsp;l&agrave; nơi thờ&nbsp;<strong>Khổng Tử (Văn Miếu) v&agrave; Quan C&ocirc;ng (V&otilde; Miếu)</strong>&nbsp;&ndash; Miếu Văn V&otilde; ấn tượng du kh&aacute;ch bởi được x&acirc;y dựng th&agrave;nh hệ thống kiến tr&uacute;c b&ecirc;n triền đồi, trải d&agrave;i từ thấp đến cao. Miếu Văn V&otilde; b&ecirc;n hồ Nhật Nguyệt tuyệt đẹp tạo n&ecirc;n một khung cảnh lung linh, huyền ảo nhuốm m&agrave;u linh thi&ecirc;ng.</li>
                        <li><strong>Ph&ograve;ng trưng b&agrave;y c&aacute;c loại nấm Linh Chi nổi tiếng của Đ&agrave;i Loan.</strong></li>
                    </ul>
                    
                    <p><strong>18h00:</strong>&nbsp; Qu&yacute; kh&aacute;ch d&ugrave;ng bữa tối tại nh&agrave; h&agrave;ng.</p>
                    
                    <ul>
                        <li><strong>Nghỉ đ&ecirc;m tại kh&aacute;ch sạn 3 sao tại Đ&agrave;i Trung.</strong></li>
                    </ul>',
                'tour_id' => 8, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            ['day' => 2, 'name' => 'ĐÀI TRUNG – ĐÀI BẮC (ĂN SÁNG, TRƯA, TỐI)',
                'content' => '<p><strong>08h00:</strong>&nbsp;Sau bữa s&aacute;ng, đo&agrave;n di chuyển về Đ&agrave;i Bắc. Xe v&agrave; HDV đưa đo&agrave;n đi tham quan:</p>

                    <ul>
                        <li><strong>L&agrave;ng cổ Thập Phần (Shifen Old Street)</strong>nơi Qu&yacute; kh&aacute;ch cũng c&oacute; thể tự tay viết những lời cầu nguyện l&ecirc;n đ&egrave;n lồng v&agrave; đốt nến để thả đ&egrave;n l&ecirc;n trời, cầu may mắn, b&igrave;nh an. ( 4 người được chung nhau 1 đ&egrave;n lồng miễn ph&iacute;)</li>
                        <li><strong>Th&aacute;c nước Thập Phần:&nbsp;</strong>l&agrave; điểm đến l&yacute; tưởng cho những ai muốn phi&ecirc;u lưu mạo hiểm với những con đường n&uacute;i đầy th&aacute;ch thức v&agrave; c&oacute; ch&uacute;t mạo hiểm. Hai b&ecirc;n đường đi xuống th&aacute;c, bạn c&oacute; thể ngắm những d&ograve;ng th&aacute;c trắng x&oacute;a đổ xuống mặt hồ xanh biếc.</li>
                    </ul>
                    
                    <p><strong>12h00:</strong>&nbsp;Ăn trưa tại nh&agrave; h&agrave;ng địa phương.</p>
                    
                    <p><strong>14h00:</strong>&nbsp;Buổi chiều, đo&agrave;n tham quan:</p>
                    
                    <ul>
                        <li><strong>Cao Ốc 101 Tầng</strong>&ndash;&nbsp;biểu tượng mới của Đ&agrave;i Loan, đ&acirc;y l&agrave; t&ograve;a th&aacute;p cao&nbsp;thứ hai&nbsp;thế giới.&nbsp;Qu&yacute; kh&aacute;ch ngắm nh&igrave;n to&agrave;n cảnh th&agrave;nh phố Đ&agrave;i Bắc v&agrave; sử dụng thang m&aacute;y nhanh nhất thế giới<strong>( kh&ocirc;ng bao gồm ph&iacute; l&ecirc;n th&aacute;p),&nbsp;</strong>mua sắm tại cửa h&agrave;ng mua sắm miễn thuế với nhiều nh&atilde;n h&agrave;ng nổi tiếng</li>
                        <li><strong>Tham quan mua sắm tại cửa h&agrave;ng tỳ hưu.</strong></li>
                    </ul>
                    
                    <p><strong>18h30:</strong>&nbsp;Ăn tối tại nh&agrave; h&agrave;ng. Tự do tham quan v&agrave; mua sắm tại&nbsp;<strong>chợ đ&ecirc;m T&acirc;y M&ocirc;n Đinh</strong></p>
                    
                    <p><strong>Nghỉ đ&ecirc;m tại kh&aacute;ch sạn 3 sao ở Đ&agrave;i Bắc.</strong></p>',
                'tour_id' => 8, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            ['day' => 3, 'name' => 'ĐÀI BẮC – ĐÀI TRUNG – GIA NGHĨA  (ĂN SÁNG, TRƯA, TỐI)',
                'content' => '<p><strong>8h00:</strong>&nbsp;Sau bữa s&aacute;ng tại kh&aacute;ch sạn, đo&agrave;n trả ph&ograve;ng, tiếp tục đi thăm quan:</p>

                    <ul>
                        <li><strong>CKS Memorial &ndash;&nbsp;</strong><strong>Đ&agrave;i tưởng niệm Tưởng Giới Thạch</strong>&nbsp;&ndash;&nbsp; đ&acirc;y l&agrave; quảng trường h&ugrave;ng vĩ nhất tr&ecirc;n diện t&iacute;ch 25 h&eacute;c ta gồm cả đ&agrave;i tưởng niệm anh h&ugrave;ng liệt sĩ,&nbsp;c&ocirc;ng vi&ecirc;n&nbsp;Trung Ch&iacute;nh, nh&agrave; h&aacute;t kịch Quốc Gia, ph&ograve;ng h&ograve;a nhạc. Đ&acirc;y cũng l&agrave; nơi vui chơi giải tr&iacute; của người d&acirc;n trong những ng&agrave;y nghỉ.</li>
                        <li><strong>Long Sơn Tự</strong>&ndash;&nbsp;một trong những điểm tham quan nổi tiếng ở&nbsp;<strong>Đ&agrave;i Bắc.</strong>&nbsp;Đến đ&acirc;y, du kh&aacute;ch kh&ocirc;ng chỉ c&oacute; cơ hội t&igrave;m hiểu về đời sống t&iacute;n ngưỡng của người&nbsp;<strong>Đ&agrave;i Loan&nbsp;</strong>m&agrave; c&ograve;n cảm nhận được một kh&ocirc;ng gian tĩnh lặng tuyệt vời.</li>
                        <li><strong>Thăm quan mua sắm tại cửa h&agrave;ng đặc sản B&aacute;nh Dứa của Đ&agrave;i Loan</strong></li>
                    </ul>
                    
                    <p>Sau bữa trưa, thăm quan:&nbsp;<strong>l&agrave;ng Cầu Vồng</strong><strong>.&nbsp;</strong>Đo&agrave;n di chuyển về Gia Nghĩa.</p>
                    
                    <p><strong>Nghỉ đ&ecirc;m tại kh&aacute;ch sạn 3 sao Gia Nghĩa.</strong></p>',
                'tour_id' => 8, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            ['day' => 4, 'name' => 'GIA NGHĨA –  CAO HÙNG  (ĂN SÁNG, TRƯA, TỐI)',
                'content' => '<p><strong>07h00:</strong>Ăn s&aacute;ng tại kh&aacute;ch sạn. Đo&agrave;n thăm quan:</p>

                    <ul>
                        <li><strong>Cầu Thi&ecirc;n Trường địa cửu,&nbsp;</strong>nằm dưới ch&acirc;n n&uacute;i A l&yacute; Sơn, được x&acirc;y v&agrave;o năm Showa thứ 12 (tức năm 1937) trong thời k&igrave; chiếm đ&oacute;ng của qu&acirc;n Nhật. Người d&acirc;n Nhật Bản đ&atilde; cho x&acirc;y dựng cầu Thi&ecirc;n Trường Địa Cửu nhằm ch&uacute;c mừng sinh nhật của Thi&ecirc;n ho&agrave;ng Showa (Hirohito) v&agrave; Ho&agrave;ng hậu (Ryoko, Nagako). Tương truyền rằng, đ&ocirc;i nam nữ nắm tay nhau đi qua C&acirc;y cầy n&agrave;y sẽ được ở b&ecirc;n nhau hạnh ph&uacute;c trọn đời.</li>
                        <li><strong>Đo&agrave;n gh&eacute; tiệm tr&agrave;,</strong>&nbsp;thưởng thức những&nbsp;<strong>ly tr&agrave; A L&yacute; Sơn đặc biệt</strong>. A L&yacute; Sơn c&oacute; lẽ l&agrave; v&ugrave;ng n&uacute;i nổi tiếng nhất v&agrave; được tham quan nhiều nhất ở Đ&agrave;i Loan. Tr&agrave; A L&yacute; Sơn l&agrave; một c&aacute;i t&ecirc;n kh&aacute; quen thuộc với những ai ghiền tr&agrave; v&agrave; tr&agrave; sữa. Được chế biến từ l&aacute; v&agrave; b&uacute;p của những c&acirc;y ch&egrave; trồng tr&ecirc;n độ cao hơn 1000m, tr&agrave; A L&yacute; Sơn l&agrave; sự kết tinh ho&agrave;n hảo của địa thế tr&ocirc;ng đặc biệt v&agrave; điều kiện thời tiết rất ri&ecirc;ng của A L&yacute; Sơn.</li>
                        <li><strong>Di chuyển về Cao H&ugrave;ng, thăm quan mua sắm tại si&ecirc;u thị đ&aacute; qu&yacute;.</strong></li>
                        <li><strong>Phật Quang Sơn</strong>, c&aacute;i n&ocirc;i của &ldquo;Phật ph&aacute;p nh&acirc;n gian&rdquo;, được mệnh danh l&agrave; &ldquo;kinh đ&ocirc; phật gi&aacute;o Nam Đ&agrave;i, Bức tượng Phật Quang l&agrave; biểu tượng của ng&ocirc;i ch&ugrave;a n&agrave;y, cao 108 m&eacute;t, được đ&uacute;c bằng 1872 tấn đồng, hiện l&agrave; pho tượng đồng cao nhất thế giới.</li>
                    </ul>
                    
                    <p><strong>18h00:</strong>&nbsp; Qu&yacute; kh&aacute;ch d&ugrave;ng bữa tối tại nh&agrave; h&agrave;ng. Sau đ&oacute;, Qu&yacute; kh&aacute;ch tự do mua sắm tại&nbsp;<strong>Chợ đ&ecirc;m Lục Hợp</strong>&nbsp;&ndash;&nbsp;một khu chợ sầm uất nhất của Th&agrave;nh phố Cao H&ugrave;ng.</p>
                    
                    <p><strong>Nghỉ đ&ecirc;m tại kh&aacute;ch sạn 3 sao tại Cao H&ugrave;ng.</strong></p>',
                'tour_id' => 8, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            ['day' => 5, 'name' => 'CAO HÙNG – ĐÀ NẴNG (ĂN SÁNG, TRƯA)',
                'content' => '<p><strong>8h00:</strong>&nbsp;Sau bữa s&aacute;ng tại kh&aacute;ch sạn, đo&agrave;n trả ph&ograve;ng, tiếp tục đi thăm quan:</p>

                    <ul>
                        <li><strong>Đầm Li&ecirc;n Tr&igrave;</strong>&ndash;&nbsp;l&agrave; thắng cảnh tr&ecirc;n hồ tuyệt đẹp, nơi bạn c&oacute; thể ph&oacute;ng tầm mắt ra xa ngắm cảnh Cao H&ugrave;ng v&agrave; h&iacute;t thở kh&ocirc;ng kh&iacute; trong l&agrave;nh m&aacute;t mẻ với m&ugrave;i thơm của hoa sen nở trong hồ v&agrave;o m&ugrave;a sen. Q&uacute;y kh&aacute;ch tham quan&nbsp;<strong>Th&aacute;p Long Hổ, Xu&acirc;n Thu C&aacute;c.</strong></li>
                    </ul>
                    
                    <p><strong>11h00:</strong>&nbsp;Sau bữa trưa tại nh&agrave; h&agrave;ng địa phương đo&agrave;n di chuyển ra s&acirc;n bay Đ&agrave;o Vi&ecirc;n, đ&aacute;p chuyến bay về Việt Nam.&nbsp;<strong>KHH &ndash; DAD ( 12h30:13h45). Đo&agrave;n ăn trưa tr&ecirc;n m&aacute;y bay.</strong></p>
                    
                    <p><strong>13h45:</strong>&nbsp;Đo&agrave;n về đến s&acirc;n bay Đ&agrave; Nẵng.</p>',
                'tour_id' => 8, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            //tour_id: 9 => 5N-4Đ
            ['day' => 1, 'name' => 'ĐÀ NẴNG – SEOUL (TRÊN MÁY BAY)',
                'content' => '<p><strong>Lưu &yacute;: Khoảng 7h30 tối với h&agrave;ng kh&ocirc;ng ZE v&agrave; 11h tối với h&agrave;ng kh&ocirc;ng RS,</strong>&nbsp;Trưởng đo&agrave;n đ&oacute;n qu&yacute; kh&aacute;ch tại s&acirc;n bay quốc tế đ&agrave; nẵng l&agrave;m thủ tục đ&aacute;p chuyến bay đi H&agrave;n Quốc. Q&uacute;y kh&aacute;ch nghỉ đ&ecirc;m tr&ecirc;n m&aacute;y bay</p>',
                'tour_id' => 9, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            ['day' => 2, 'name' => 'ĐẢO NAMI – THÁP TRUYỀN HÌNH NAM SAN (ĂN 3 BỮA)',
                'content' => '<p>Đến s&acirc;n bay quốc tế Incheon, đo&agrave;n l&agrave;m thủ tục nhập cảnh v&agrave; khởi h&agrave;nh tham quan:</p>

                    <ul>
                        <li><strong>Đảo Nami</strong>&nbsp;&ndash;&nbsp;<em>nơi quay bộ phim Chuyện t&igrave;nh m&ugrave;a đ&ocirc;ng (2001). Nơi khởi nguồn cho l&agrave;n s&oacute;ng văn h&oacute;a H&agrave;n (Hallyu), từ Nhật Bản lan sang c&aacute;c nước trong khu vực v&agrave; b&ugrave;ng nổ tr&ecirc;n to&agrave;n thế giới. Từ đ&acirc;y, người d&acirc;n tr&ecirc;n thế giới biết đến c&ocirc;ng nghệ l&agrave;m phim truyền h&igrave;nh H&agrave;n Quốc, dẫn lối cho một nền c&ocirc;ng nghiệp kh&ocirc;ng kh&oacute;i mới, đưa t&ecirc;n tuổi H&agrave;n Quốc l&ecirc;n một tầm cao mới.</em></li>
                    </ul>
                    
                    <p>Sau khi ăn trưa, đo&agrave;n quay về thủ đ&ocirc; Seoul, tham quan:</p>
                    
                    <ul>
                        <li><strong>Th&aacute;p truyền h&igrave;nh N Seoul</strong>&nbsp;(kh&ocirc;ng bao gồm chi ph&iacute; thang m&aacute;y l&ecirc;n đ&agrave;i quan s&aacute;t) &ndash;&nbsp;<em>l&agrave; th&aacute;p truyền h&igrave;nh nằm tr&ecirc;n ngọn n&uacute;i Namsan, vừa l&agrave; điểm tham quan, vừa l&agrave; th&aacute;p ph&aacute;t s&oacute;ng truyền h&igrave;nh. Nơi đ&acirc;y được chọn để quay kh&ocirc;ng biết bao nhi&ecirc;u c&acirc;u chuyện t&igrave;nh y&ecirc;u trong drama xứ H&agrave;n, nơi đ&acirc;y kh&ocirc;ng biết đ&atilde; c&oacute; bao nhi&ecirc;u cặp đ&ocirc;i c&ugrave;ng viết những lời hẹn ước.</em></li>
                    </ul>
                    
                    <p>Ăn tối v&agrave;về kh&aacute;ch sạn nhận ph&ograve;ng nghỉ ngơi.</p>',
                'tour_id' => 9, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            ['day' => 3, 'name' => 'SEOUL CITY TOUR – MẶC HANBOK – LÀM KIMCHI – XEM SHOW DIỄN (ĂN 3 BỮA)',
                'content' => '<p>Sau khi ăn s&aacute;ng tại kh&aacute;ch sạn, Qu&yacute; kh&aacute;ch tham quan v&agrave; mua sắm tại:</p>

                    <ul>
                        <li><strong>Cửa h&agrave;ng nh&acirc;n s&acirc;m ch&iacute;nh phủ</strong></li>
                        <li><strong>Cửa h&agrave;ng mỹ phẩm nội địa</strong></li>
                    </ul>
                    
                    <p>Ăn trưa v&agrave; tiếp tục tham quan:</p>
                    
                    <ul>
                        <li><strong>Nh&agrave; Xanh (chụp h&igrave;nh b&ecirc;n ngo&agrave;i) &ndash;&nbsp;</strong><em>nơi sinh sống v&agrave; l&agrave;m việc của c&aacute;c đời Tổng Thống H&agrave;n Quốc.</em></li>
                        <li><strong>Cung điện Gyeongbok &ndash;</strong><em>&nbsp;Một trong năm cung điện của thủ đ&ocirc; Seoul c&ograve;n đang mở cửa cho du kh&aacute;ch tham quan với nhiều c&ocirc;ng tr&igrave;nh kiến tr&uacute;c được phục chế sau khi cung điện cũ bị t&agrave;n ph&aacute; bởi chiến tranh H&agrave;n &ndash; Nhật.</em></li>
                        <li><strong>Bảo t&agrave;ng d&acirc;n tộc d&acirc;n gian H&agrave;n Quốc</strong><em>&ndash; bảo t&agrave;ng được chia l&agrave;m nhiều ph&ograve;ng kh&aacute;c nhau như lịch sử, văn h&oacute;a, truyền thống, khu trẻ em,&hellip; l&agrave; nơi người H&agrave;n Quốc t&igrave;m về với cội nguồn, với bản sắc d&acirc;n tộc m&igrave;nh. L&agrave; nơi trưng b&agrave;y v&agrave; tr&igrave;nh diễn những c&ocirc;ng nghệ được ứng dụng trong bảo t&agrave;ng nhằm mang đến những hiểu biết cho kh&aacute;ch tham quan.</em></li>
                        <li><strong>Trải nghiệm l&agrave;m Kimchi v&agrave; mặc thử trang phục truyền thống H&agrave;n Quốc (Hanbok)</strong><em>&ndash; Kimchi, m&oacute;n ăn h&agrave;ng ng&agrave;y, m&oacute;n ăn sức khỏe của người d&acirc;n H&agrave;n Quốc v&agrave; Hanbok, biểu tượng văn h&oacute;a H&agrave;n.</em></li>
                    </ul>
                    
                    <p>Ăn tối v&agrave; thưởng thức show diễn&nbsp;<strong>Hero Painters Show</strong><em>&ndash; một trong những show biểu diễn đặc sắc nhất tại H&agrave;n Quốc.</em></p>
                    
                    <p>Về kh&aacute;ch sạn nghỉ ngơi.</p>
                    
                    <p>Nghỉ đ&ecirc;m tại Seoul.</p>',
                'tour_id' => 9, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            ['day' => 4, 'name' => 'SEOUL – CÔNG VIÊN EVERLAND (ĂN 3 BỮA)',
                'content' => '<p>Sau khi ăn s&aacute;ng tại kh&aacute;ch sạn, Qu&yacute; kh&aacute;ch tham quan v&agrave; mua sắm tại:</p>

                    <ul>
                        <li><strong>Cửa h&agrave;ng tinh dầu th&ocirc;ng đỏ</strong></li>
                        <li><strong>Cửa h&agrave;ng nh&acirc;n s&acirc;m tươi</strong></li>
                    </ul>
                    
                    <p>Ăn trưa v&agrave; di chuyển v&agrave;o tham quan:</p>
                    
                    <ul>
                        <li><strong>Everland Theme Park</strong>&nbsp;&ndash;<em>&nbsp;C&ocirc;ng vi&ecirc;n giải tr&iacute; chủ đề lớn nhất tại H&agrave;n Quốc với v&ocirc; v&agrave;n những tr&ograve; chơi cảm gi&aacute;c mạnh, tham quan Vườn th&uacute; Safari v&agrave; chi&ecirc;m ngưỡng vườn hoa bốn m&ugrave;a.</em></li>
                    </ul>
                    
                    <p>Ăn tối v&agrave; về kh&aacute;ch sạn nghỉ ngơi.</p>',
                'tour_id' => 9, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            ['day' => 5, 'name' => 'SEOUL – ĐÀ NẴNG (ĂN 3 BỮA)',
                'content' => '<p>Sau khi ăn s&aacute;ng tại kh&aacute;ch sạn, Qu&yacute; kh&aacute;ch tham quan v&agrave; mua sắm tại:</p>

                    <ul>
                        <li><strong>Cửa h&agrave;ng si&ecirc;u thị miễn thuế</strong></li>
                        <li><strong>Phim trường quay show Running Man</strong></li>
                        <li><strong>Quảng trường Gwanghwamun</strong>&ndash; quảng trường trung t&acirc;m thủ đ&ocirc; Seoul, tọa lạc ngay đại lộ Sejong, nơi đặt tượng vị vua vĩ đại của Triều Ti&ecirc;n &ndash; vua Sejong Đại Đế v&agrave; tướng Yi Sun Shin &ndash; người c&oacute; c&ocirc;ng đ&aacute;nh đuổi qu&acirc;n x&acirc;m lược Nhật Bản.</li>
                        <li><strong>Suối nh&acirc;n tạo Cheonggye</strong>&ndash; con suối được h&igrave;nh th&agrave;nh tr&ecirc;n nền của đường cao tốc th&agrave;nh phố Seoul, nhằm việc giảm nhiệt cho th&agrave;nh phố n&agrave;y v&agrave;o m&ugrave;a h&egrave;. C&aacute;c chuy&ecirc;n gia ước t&iacute;nh, từ khi c&oacute; d&ograve;ng suối n&agrave;y, nhiệt độ th&agrave;nh phố được giảm từ 2-3 độ khi m&ugrave;a h&egrave; đến.</li>
                    </ul>
                    
                    <p>Ăn trưa v&agrave; tham quan mua sắm tại&nbsp;<strong>khu mua sắm tổng hợp Đ&ocirc;ng Đại M&ocirc;n</strong>&nbsp;(Dongdaemun).</p>
                    
                    <ul>
                        <li><strong>Cửa h&agrave;ng si&ecirc;u thị tạp h&oacute;a</strong>&nbsp;&ndash; nơi Qu&yacute; Kh&aacute;ch chọn lựa những m&oacute;n qu&agrave; d&agrave;nh cho bạn b&egrave; v&agrave; người th&acirc;n.</li>
                    </ul>
                    
                    <p>Ăn tối v&agrave; khởi h&agrave;nh ra s&acirc;n bay Quốc tế Incheon l&agrave;m thủ tục l&ecirc;n m&aacute;y bay. Đến s&acirc;n bay Quốc tế Đ&agrave; Nẵng, Qu&yacute; kh&aacute;ch l&agrave;m thủ tục nhập cảnh, nhận lại h&agrave;nh l&yacute;.</p>',
                'tour_id' => 9, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            //tour_id: 10 => 5N-4Đ
            ['day' => 1, 'name' => 'HUE – DA NANG –- BANGKOK (ĂN TỐI)',
                'content' => '<p><strong>S&aacute;ng 07h30</strong>: Xe v&agrave; Hướng Dẫn Vi&ecirc;n kinh nghiệm của C&Ocirc;NG TY đ&oacute;n kh&aacute;ch tại 115 Phạm Văn Đồng &ndash; Tp Huế khởi h&agrave;nh đi Đ&agrave; Nẵng. Đo&agrave;n tự t&uacute;c ăn trưa tại s&acirc;n bay. Sau đ&oacute; l&agrave;m thủ tục bay sang Bangkok.</p>

                    <p>M&aacute;y bay hạ c&aacute;nh tại s&acirc;n bay&nbsp;<strong>Suvarnabhumi</strong>. L&agrave;m thủ tục giấy tờ nhập cảnh. Xe v&agrave; HDV địa phương đưa đo&agrave;n đến kh&aacute;ch sạn, đo&agrave;n nhận ph&ograve;ng v&agrave; tự do nghỉ ngơi. Ăn tối, tự do kh&aacute;m ph&aacute; Bangkok về đ&ecirc;m.</p>',
                'tour_id' => 10, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            ['day' => 2, 'name' => 'BANGKOK – PATTAYA  (ĂN SÁNG, TRƯA, TỐI)',
                'content' => '<p><strong>S&aacute;ng:</strong>&nbsp;Đo&agrave;n ăn s&aacute;ng v&agrave; l&agrave;m thủ tục trả ph&ograve;ng kh&aacute;ch sạn. Sau bữa s&aacute;ng, xe đ&oacute;n đo&agrave;n khởi h&agrave;nh về Pattaya tr&ecirc;n đường đo&agrave;n gh&eacute; thăm quan:</p>
                    
                    <ul>
                        <li><strong>Trại Hổ (Tiger Zoo)</strong>&nbsp;v&agrave; xem c&aacute;c chương tr&igrave;nh biểu diễn đặc biệt &ndash; bắt c&aacute; sấu bắng tay kh&ocirc;ng, chi&ecirc;m ngưỡng v&agrave; thưởng thức những m&agrave;n tr&igrave;nh diễn nguy hiểm.</li>
                        <li>Đo&agrave;n tiếp tục tham quan&nbsp;<strong>Chợ Nổi Bốn Miền.</strong></li>
                    </ul>
                    
                    <p>Trưa qu&yacute; kh&aacute;ch d&ugrave;ng cơm trưa tại nh&agrave; h&agrave;ng.</p>
                    
                    <ul>
                        <li>Sau bữa trưa, đo&agrave;n tham quan&nbsp;<strong>Khau Phratamak</strong>. Nơi đ&acirc;y, du kh&aacute;ch c&oacute; thể ngắm to&agrave;n cảnh th&agrave;nh phố biển Pattaya.</li>
                    </ul>
                    
                    <p><strong>Chiều:</strong>&nbsp;Đo&agrave;n khởi h&agrave;nh về nhận ph&ograve;ng kh&aacute;ch sạn v&agrave; đi ăn tối tại nh&agrave; h&agrave;ng. Sau khi ăn tối, đo&agrave;n về lại kh&aacute;ch sạn v&agrave; tự do nghỉ ngơi hoặc tham quan Th&agrave;nh phố đ&ecirc;m Pattaya.</p>
                    
                    <p><strong>Nghỉ đ&ecirc;m tại Pattaya.</strong></p>',
                'tour_id' => 10, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            ['day' => 3, 'name' => 'PATTAYA – ĐẢO CORAL (ĂN SÁNG, TRƯA, TỐI)',
                'content' => '<p><strong>S&aacute;ng:</strong>&nbsp; S&aacute;ng: Đo&agrave;n ăn s&aacute;ng tại kh&aacute;ch sạn. Sau bữa s&aacute;ng, xe v&agrave; HDV đưa đo&agrave;n ra:</p>
                    
                    <ul>
                        <li><strong>Đảo San H&ocirc; ( Koh Lan Island )</strong>&nbsp;bằng can&ocirc; si&ecirc;u tốc, du kh&aacute;ch c&oacute; thể tự do vui th&uacute; với c&aacute;c tr&ograve; chơi biển như : lướt v&aacute;n, nhảy d&ugrave;, bơi, lặn biển, ngồi can&ocirc; si&ecirc;u tốc ngắm đảo, b&oacute;ng đ&aacute; b&atilde;i biển &hellip; (Chi ph&iacute; tham gia c&aacute;c tr&ograve; chơi tự t&uacute;c)</li>
                    </ul>
                    
                    <p><strong>Trưa:</strong>&nbsp;Đo&agrave;n trở về kh&aacute;ch sạn ăn trưa và nghỉ nghơi.</p>
                    
                    <p><strong>Chiều:</strong>&nbsp;Đo&agrave;n đi tham quan:</p>
                    
                    <ul>
                        <li><strong>Trung t&acirc;m Đ&aacute; Qu&yacute;</strong>&nbsp;&ndash; đ&acirc;y l&agrave; một trong những trung t&acirc;m lớn nhất Đ&ocirc;ng Nam &Aacute;, đến nơi n&agrave;y qu&yacute; kh&aacute;ch sẽ được chi&ecirc;m ngưỡng v&agrave; t&igrave;m hiểu về rất nhiều loại đ&aacute; qu&yacute; lạ v&agrave; đặc sắc, đồng thời c&oacute; cơ hội mua những m&oacute;n qu&agrave; được chế t&aacute;c từ c&aacute;c loại đ&aacute; qu&yacute; chất lượng để l&agrave;m qu&agrave; cho gia đ&igrave;nh v&agrave; bản th&acirc;n.</li>
                        <li>Tiếp tục tham quan&nbsp;<strong>Chợ đồ da</strong>&nbsp;&ndash; nơi trưng b&agrave;y những sản phầm l&agrave;m từ da c&aacute;c loại động vật bền bỉ, chất lượng.</li>
                        <li>Đo&agrave;n tiếp tục di chuyển tới tham quan&nbsp;<strong>N&uacute;i Phật V&agrave;ng</strong>, viếng tượng Phật cao 130m được vị Ho&agrave;ng Tử cho người tạc thẳng v&agrave;o v&aacute;ch n&uacute;i v&agrave; được d&aacute;t v&agrave;ng để tặng cho VUA RAMA 9 nh&acirc;n dịp 50 năm trị v&igrave; vương quốc.</li>
                        <li>Tham quan<strong>&nbsp;Silver Lake</strong>, vườn nho.</li>
                        <li>Đo&agrave;n quay ngược về Trung t&acirc;m th&agrave;nh phố để tham quan một trong những c&ocirc;ng tr&igrave;nh kiến trức đồ sộ nơi n&agrave;y đ&oacute; l&agrave;&nbsp;<strong>L&acirc;u Đ&agrave;i Tỷ Ph&uacute;</strong>&nbsp;&ndash; đ&acirc;y l&agrave; một khu&acirc;n vi&ecirc;n lớn với những ng&ocirc;i biệt thự mang phong c&aacute;ch hiện đại nhưng theo &acirc;m hưởng của Phật gi&aacute;o linh thi&ecirc;ng, những t&ograve;a nh&agrave; đồ sộ kết hợp với những vườn hoa rực rỡ c&agrave;ng tạo n&ecirc;n sự h&agrave;i h&ograve;a v&agrave; y&ecirc;n b&igrave;nh nơi n&agrave;y, ph&iacute;a trước l&agrave; một b&atilde;i biển rộng lớn ng&uacute;t ng&agrave;n khiến khu khu&acirc;n vi&ecirc;n&nbsp;<strong>L&acirc;u Đ&agrave;i Tỷ Ph&uacute;</strong>&nbsp;nổi bật l&ecirc;n như một thi&ecirc;n đường cổ t&iacute;ch l&atilde;ng mạn v&agrave; nhưng rất linh thi&ecirc;ng.</li>
                        <li>Sau đ&oacute;, đo&agrave;n d&ugrave;ng bữa tối Buffet với rất nhiều lựa chọn kh&aacute;c nhau ngay tại khu&ocirc;n vi&ecirc;n&nbsp;<strong>L&acirc;u Đ&agrave;i Tỷ Ph&uacute;</strong>, được thưởng thức nhiều m&oacute;n ăn đặc trưng d&acirc;n gi&atilde; v&agrave; được thưởng thức chương tr&igrave;nh nghệ thuật d&acirc;n tộc đằm thắm v&agrave; s&acirc;u sắc.</li>
                        <li>Sau đ&oacute;, đo&agrave;n trải nghiệm thư giản với&nbsp;<strong>1h Massage Th&aacute;i cổ truyền</strong>.</li>
                    </ul>
                    
                    <p><strong>Sau bữa tối:</strong>&nbsp;xe sẽ đưa đo&agrave;n đi xem chương trình biểu diễn nghệ thuật:</p>
                    
                    <ul>
                        <li><strong>Alcazad Show/Colloseum Show</strong>&nbsp;&ndash; chương tr&igrave;nh do những c&ocirc; b&ecirc;đ&ecirc; đẹp nổi tiếng của vương qu&ocirc;́c Th&aacute;i Lan bi&ecirc;̉u di&ecirc;̃n, đ&acirc;y l&agrave; một n&eacute;t đặc trung thu h&uacute;t nhiều kh&aacute;ch du lịch đến với Th&aacute;i Lan n&oacute;i chung v&agrave; Th&agrave;nh phố biển Pattaya n&oacute;i ri&ecirc;ng. Đo&agrave;n trở về kh&aacute;ch sạn nghỉ nghơi v&agrave; tự do t&igrave;m hiểu th&ecirc;́ giới v&ecirc;̀ đ&ecirc;m của thành ph&ocirc;́ Pattaya.</li>
                    </ul>',
                'tour_id' => 10, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            ['day' => 4, 'name' => 'PATTAYA – BANGKOK   (ĂN SÁNG, TRƯA)',
                'content' => '<p><strong>S&aacute;ng:</strong>&nbsp;Đo&agrave;n trả ph&ograve;ng kh&aacute;ch sạn v&agrave; d&ugrave;ng bữa s&aacute;ng, Sau đ&oacute;,. Đo&agrave;n tiếp tục tham quan:</p>
                    
                    <ul>
                        <li><strong>Vườn Bướm</strong>&nbsp;v&agrave;&nbsp;<strong>Cửa h&agrave;ng Mật Ong</strong>&nbsp;&ndash; c&ugrave;ng t&igrave;m hiểu v&agrave; kh&aacute;m ph&aacute; đặc sản 3 miền đặc trưng của Th&aacute;i Lan như: Mật Ong, Cao Hổ, huyết Yến,&hellip; Sau đ&oacute;, đo&agrave;n gh&eacute; tham quan Trung t&acirc;m b&aacute;nh kẹo &ndash; để thưởng thức v&agrave; mua những sản phẩm hoa quả kh&ocirc; ngon, chất lượng về l&agrave;m qu&agrave; cho gia đ&igrave;nh v&agrave; bạn b&egrave;.</li>
                        <li>Sau đ&oacute;, xe đưa đo&agrave;n đi tham quan<strong>&nbsp;Trại rắn&nbsp;</strong>&ndash; xem chương tr&igrave;nh biểu diễn bắt rắn bằng tay kh&ocirc;ng v&agrave; c&ugrave;ng t&igrave;m hiểu về nhiều loại thuốc chiết xuất từ rắn tốt v&agrave; bồi bổ.</li>
                    </ul>
                    
                    <p><strong>Trưa:</strong>&nbsp;Đo&agrave;n ăn trưa Buffet với hơn 100 m&oacute;n ăn &Acirc;u &ndash; &Aacute; tại&nbsp;<strong>Baiyoke Sky</strong>&nbsp;&ndash; M&aacute;i nh&agrave; trọc trời nơi đất Th&aacute;i. Tại đ&acirc;y, Qu&yacute; kh&aacute;ch kh&ocirc;ng chỉ được thưởng thức hương vị của c&aacute;c m&oacute;n ăn tới từ nhiều nơi m&agrave; c&ograve;n được chi&ecirc;m ngưỡng một Bangkok thu nhỏ từ tr&ecirc;n cao, một th&agrave;nh phố ồn &agrave;o, n&aacute;o nhiệt bỗng chốc trở n&ecirc;n y&ecirc;n b&igrave;nh v&agrave; tĩnh lặng, mọi thứ nhỏ b&eacute; lại khi được ngắm nh&igrave;n từ tầng xoay cao nhất của t&ograve;a nh&agrave;..</p>
                    
                    <ul>
                        <li>Sau đ&oacute;, xe tiếp tục đưa đo&agrave;n tới khu trung t&acirc;m mua sắm phức hợp n&aacute;o nhiệt nhất Bangkok, đo&agrave;n tự do mua sắm tại một số trung t&acirc;m lớn như: Big C, Central World, Siam Paragon, MBK,&hellip;</li>
                    </ul>
                    
                    <p><strong>Tối:</strong>&nbsp;Đo&agrave;n ăn tối tự t&uacute;c , sau đ&oacute; về lại kh&aacute;ch sạn nhận ph&ograve;ng v&agrave; tự do nghỉ ngơi.</p>',
                'tour_id' => 10, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            ['day' => 5, 'name' => 'BANGKOK – DANANG – HUE   (ĂN SÁNG, TRƯA)',
                'content' => '<p><strong>S&aacute;ng:</strong>&nbsp;Đo&agrave;n l&agrave;m thủ tục trả ph&ograve;ng v&agrave; ăn s&aacute;ng tại kh&aacute;ch sạn.</p>
                    
                    <ul>
                        <li>Sau đ&oacute;, xe đưa đo&agrave;n ra bến t&agrave;u, đo&agrave;n l&ecirc;n chiếc thuyền nhỏ v&agrave; bắt đầu cuộc h&agrave;nh tr&igrave;nh &ldquo;<strong>Dạo thuyền tr&ecirc;n d&ograve;ng s&ocirc;ng huyền thoại Chao Phraya</strong>&rdquo;, c&ugrave;ng nghe giới thiệu về lịch sử vĩ đại cũng như kh&aacute;m ph&aacute; về đời sống của người d&acirc;n dọc bờ s&ocirc;ng. Đo&agrave;n tiếp tục dừng lại đ&ocirc;i ch&uacute;t tại nơi được gọi l&agrave; &ldquo;Ng&ocirc;i nh&agrave; của những ch&uacute; c&aacute; linh thi&ecirc;ng&rdquo; với h&agrave;ng ng&agrave;n ch&uacute; c&aacute; bơi lượn tr&ecirc;n mặt s&ocirc;ng, qu&yacute; kh&aacute;ch c&oacute; thể trực tiếp cho c&aacute;c ch&uacute; c&aacute; ăn những mẩu b&aacute;nh m&igrave; nhỏ v&agrave; chụp lại những khoảnh khắc ấn tượng n&agrave;y.</li>
                        <li>Đo&agrave;n l&ecirc;n bờ v&agrave; tiếp tuc gh&eacute; tham quan&nbsp;<strong>Ch&ugrave;a Thuyền &ndash; Wat Yanawa</strong>, nơi lưu giữ rất nhiều X&aacute; Lợi Phật linh thi&ecirc;ng của sứ sở ch&ugrave;a v&agrave;ng.</li>
                        <li>Đo&agrave;n tham quan&nbsp;<strong>Ch&ugrave;a Phật V&agrave;ng</strong>.</li>
                    </ul>
                    
                    <p>Xe đưa qu&yacute; kh&aacute;ch ra s&acirc;n bay d&ugrave;ng cơm trưa. Sau đ&oacute; đo&agrave;n l&agrave;m thủ tục đ&aacute;p chuyến bay trở về Đ&agrave; Nẵng. Đến s&acirc;n bay Đ&agrave; Nẵng, xe đ&oacute;n qu&yacute; kh&aacute;ch trả về điểm hẹn ở Tp Huế, chương tr&igrave;nh kết th&uacute;c.</p>
                    
                    <p><em><strong>Hẹn gặp lại qu&yacute; kh&aacute;ch!</strong></em></p>
                    
                    <p><strong>Ch&uacute; &yacute;:</strong>&nbsp;<strong>Thứ tự chương tr&igrave;nh c&oacute; thể thay đổi theo sự sắp xếp của C&ocirc;ng ty để ph&ugrave; hợp với t&igrave;nh h&igrave;nh thực tế nhưng vẫn đảm bảo đầy đủ c&aacute;c điểm tham quan đ&atilde; n&ecirc;u trong chương tr&igrave;nh.</strong></p>',
                'tour_id' => 10, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            //tour_id: 11 => 5N-4Đ
            ['day' => 1, 'name' => 'ĐÀ NẴNG – TRƯỜNG SA – PHƯỢNG HOÀNG (ĂN TRƯA, TỐI KS: PHƯỢNG ĐÌNH/ QUỐC KHÁCH/ 4*)',
                'content' => '<p>Trưởng đo&agrave;n c&ocirc;ng ty du lịch đ&oacute;n qu&yacute; kh&aacute;ch tại s&acirc;n bay quốc tế Đ&agrave; Nẵng khởi h&agrave;nh đi Trường Sa.&nbsp;<strong>T</strong><strong>heo giờ bay tham khảo VJ8358 [07:20 &ndash; 10:55],</strong>đến Trường Sa v&agrave; d&ugrave;ng bữa trưa tại đ&acirc;y.</p>
                    
                    <p>Sau đ&oacute; đo&agrave;n di chuyển đến Phượng Ho&agrave;ng Cổ Trấn, tham quan Cảnh đẹp hai b&ecirc;n bờ&nbsp;<strong>Đ&agrave; Giang,&nbsp;</strong>nơi c&oacute; c&acirc;y cầu đ&aacute; nhịp bước ch&acirc;n nổi tiếng, tiếp tục tham quan:</p>
                    
                    <ul>
                        <li><strong>Hồng Kiều, Đ&ocirc;ng M&ocirc;n Th&agrave;nh Lầu, Điếu Cước Lầu Quần, Th&aacute;p Vạn Danh, Phố Th&agrave;nh B&agrave;n</strong>. Sau khi d&ugrave;ng bữa tối, đo&agrave;n c&oacute; thể đi dạo v&agrave; cảm nhận&nbsp;<strong>Phượng Ho&agrave;ng Cổ Trấn</strong>&nbsp;về đ&ecirc;m.</li>
                    </ul>
                    
                    <p>Kết th&uacute;c tham quan, đo&agrave;n nhận ph&ograve;ng kh&aacute;ch sạn nghỉ ngơi.</p>',
                'tour_id' => 11, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            ['day' => 2, 'name' => 'PHƯỢNG HOÀNG – ĐỒNG NHÂN (ĂN SÁNG, TRƯA, TỐI KS: VẠN SƠN HỒNG/ CHÂU SA/ 4*)',
                'content' => '<p>Sau bữa s&aacute;ng, đo&agrave;n tự do tham quan phong cảnh&nbsp;<strong>Phượng Ho&agrave;ng</strong><strong>&nbsp;Cổ Trấn</strong>. Sau bữa trưa, đo&agrave;n di chuyển đến Đồng Nh&acirc;n tham quan:</p>
                    
                    <ul>
                        <li><strong>Mi&ecirc;u Vương Th&agrave;nh</strong>&nbsp;, l&agrave; một trong mười điểm danh lam thắng cảnh h&agrave;ng đầu ở Qu&yacute; Ch&acirc;u &ndash; c&ograve;n được v&iacute; như Vạn L&yacute; Trường Th&agrave;nh Phương Nam, đ&acirc;y l&agrave; nơi lưu giữ di sản văn ho&aacute; d&acirc;n tộc Mi&ecirc;u với phong cảnh thi&ecirc;n nhi&ecirc;n tuyệt đẹp, phong tục d&acirc;n gian rất phong ph&uacute;, nh&acirc;n văn s&acirc;u sắc v&agrave; c&oacute; một lịch sử l&acirc;u đời.</li>
                    </ul>
                    
                    <p>Kết th&uacute;c tham quan, đo&agrave;n nhận ph&ograve;ng kh&aacute;ch sạn nghỉ ngơi.</p>',
                'tour_id' => 11, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            ['day' => 3, 'name' => 'JEJU (Ăn sáng, trưa, tối)',
                'content' => '<p>Sau bữa s&aacute;ng, đo&agrave;n khởi h&agrave;nh đi:</p>
                    
                    <ul>
                        <li><strong>Ph&ugrave; Dung Trấn</strong>&nbsp;&ndash; một cổ trấn c&oacute; hơn 2000 năm lịch sử, nổi tiếng với t&ecirc;n gọi &ldquo;cổ trấn ng&agrave;n năm treo tr&ecirc;n d&ograve;ng th&aacute;c&rdquo;, bởi nơi đ&acirc;y c&oacute; th&aacute;c nước h&ugrave;ng vĩ chảy qua. Th&ecirc;m nữa, những cảnh đẹp ngỡ ng&agrave;ng nơi đ&acirc;y đ&atilde; từng l&agrave; bối cảnh của bộ phim &ldquo;Thị trấn Ph&ugrave; Dung&rdquo; nổi tiếng thế giới của Khương Văn v&agrave; Lưu Hiểu Kh&aacute;nh.</li>
                        <li>Sau đ&oacute;, đo&agrave;n di chuyển đến trung t&acirc;m&nbsp;<strong>Trương Gia Giới,</strong>&nbsp;<strong>tham quan Học viện Qu&acirc;n Thanh</strong>&nbsp; &ndash; nơi truyền cảm hứng cho h&agrave;ng vạn người trong giới văn nghệ sĩ h&agrave;ng năm.</li>
                    </ul>
                    
                    <p>Sau bữa tối,&nbsp;<strong>đo&agrave;n c&oacute; thể tự ph&iacute; tham gia chương tr&igrave;nh biểu diễn nghệ thuật M&ecirc; Li Tương Tư</strong>. Sau đ&oacute; về kh&aacute;ch sạn nghỉ ngơi.</p>',
                'tour_id' => 11, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            ['day' => 4, 'name' => 'TRƯƠNG GIA GIỚI – THƯỜNG ĐỨC (ĂN SÁNG, TRƯA, TỐI KS: THIÊN TỬ/ VIENA/ 4*)',
                'content' => '<p>Sau bữa s&aacute;ng, đo&agrave;n di chuyển đi Trương Gia Giới, tham quan phong:</p>
                    
                    <ul>
                        <li><strong>&ldquo;Hồn của Vũ Lăng&rdquo;</strong>&nbsp;Tương Tư Thần Sơn&nbsp;<strong>Thi&ecirc;n M&ocirc;n Sơn</strong>&nbsp;(Bao gồm v&eacute; c&aacute;p treo, thang cuốn, xe điện) với nền văn ho&aacute; l&acirc;u đời, nơi phong thuỷ hữu t&igrave;nh như tranh Thuỷ Mặc (thời gian tham quan kh&ocirc;ng dưới 3 tiếng), đi con đường l&ecirc;n n&uacute;i cao nhất thế giới. Đỉnh n&uacute;i cao, đ&aacute;, suối khe, m&acirc;y, rừng, tất cả tạo n&ecirc;n vẻ h&ugrave;ng vĩ của Trương Gia Giới.</li>
                        <li>Dạo bước tr&ecirc;n con đường thuỷ tinh hay c&ograve;n gọi l&agrave;&nbsp;<strong>H&agrave;nh Lang K&iacute;nh</strong>&nbsp;(bao gồm bọc gi&agrave;y) được l&agrave;m to&agrave;n bộ bằng k&iacute;nh trong suốt. Tại đ&acirc;y qu&yacute; kh&aacute;ch c&oacute; thể ngắm nh&igrave;n n&uacute;i non tr&ugrave;ng tr&ugrave;ng điệp điệp ở độ cao 1400m, cảm gi&aacute;c như đang tr&ecirc;n m&acirc;y. Cảm nhận vẻ đẹp h&ugrave;ng vĩ của&nbsp;<strong>H&agrave;nh lang Quỷ Cốc</strong>, sau đ&oacute; tham quan&nbsp;<strong>Thi&ecirc;n M&ocirc;n Động</strong>, chinh phục con đường 99 kh&uacute;c quanh co.</li>
                    </ul>
                    
                    <p>Buổi chiều đo&agrave;n di chuyển về Thường Đức d&ugrave;ng bữa tối v&agrave; về kh&aacute;ch sạn nghỉ ngơi.</p>',
                'tour_id' => 11, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            ['day' => 5, 'name' => 'JEJU – DA NANG (Ăn sáng, trưa, tối)',
                'content' => '<p>Sau bữa s&aacute;ng, đo&agrave;n l&ecirc;n xe ra s&acirc;n bay Trường Sa về lại Đ&agrave; Nẵng theo giờ chuyến bay&nbsp;<strong>VJ8359&nbsp;</strong><strong>[11:55 &ndash; 13:30].</strong>&nbsp;Kết th&uacute;c chuyến đi 5 ng&agrave;y vui vẻ, cảm ơn v&agrave; hẹn gặp lại qu&yacute; kh&aacute;ch!</p>
                    
                    <p><em>Tr&ecirc;n đ&acirc;y l&agrave; h&agrave;nh tr&igrave;nh chi tiết của chuyến du lịch Phượng Ho&agrave;ng cổ trấn 5 ng&agrave;y 4 đ&ecirc;m, song để đảm bảo thời gian tham quan những cảnh điểm ch&iacute;nh kh&ocirc;ng bị giảm bớt, Hướng dẫn vi&ecirc;n c&oacute; thể điều ch&igrave;nh tr&igrave;nh tự của c&aacute;c điểm tham quan theo t&igrave;nh h&igrave;nh thực tế.</em></p>',
                'tour_id' => 11, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            //tour_id: 12 => 2N-1Đ
            ['day' => 1, 'name' => 'GHỀNH ĐÁ ĐĨA – TÔI THẤY HOA VÀNG TRÊN CỎ XANH  (ĂN TRƯA, TỐI)',
                'content' => '<p><strong>S&aacute;ng:&nbsp;</strong><strong>08h00: Xe v&agrave; HDV&nbsp;</strong>đ&oacute;n kh&aacute;ch tại ga Tuy H&ograve;a khởi h&agrave;nh d&ugrave;ng bữa s&aacute;ng.</p>
                    
                    <ul>
                        <li>Sau đ&oacute; Qu&yacute; kh&aacute;ch đến với&nbsp;<strong>KDL B&atilde;i X&eacute;p</strong>&nbsp;một nơi c&oacute; B&atilde;i c&aacute;t v&agrave;ng &oacute;ng trải d&agrave;i. Bối cảnh trong phim&nbsp;<strong>&ldquo; T&ocirc;i thấy hoa v&agrave;ng tr&ecirc;n cỏ xanh&rdquo;</strong>khiến du kh&aacute;ch phải l&ograve;ng ngay c&aacute;i nh&igrave;n đầu ti&ecirc;n. Qu&yacute; kh&aacute;ch tự do tắm biển tại đ&acirc;y.</li>
                    </ul>
                    
                    <p><strong>10h00:&nbsp;</strong>Đo&agrave;n tham quan thắng cảnh quốc gia:</p>
                    
                    <ul>
                        <li><strong>Ghềnh Đ&aacute; Dĩa,</strong>&nbsp;nơi c&oacute; mỏm đ&aacute; nh&ocirc; ra biển với cấu tạo đặc biệt, đ&aacute; ở đ&acirc;y được dựng đứng theo từng cột liền kh&iacute;t nhau, đều tăm tắp, c&aacute;c cột đ&aacute; c&oacute; tiết diện h&igrave;nh lục gi&aacute;c hoặc h&igrave;nh tr&ograve;n giống như c&aacute;i đĩa. Qu&yacute; kh&aacute;ch h&ograve;a m&igrave;nh v&agrave; l&agrave;n nước biển trong xanh c&ugrave;ng b&atilde;i c&aacute;t d&agrave;i thoai thoải, đ&acirc;y sẽ l&agrave; trải nghiệm th&uacute; vị d&agrave;nh cho du kh&aacute;ch.</li>
                    </ul>
                    
                    <p><strong>Trưa: 11h30&nbsp;</strong>Đo&agrave;n tiếp tục khởi h&agrave;nh gh&eacute;&nbsp;<strong>Đầm &Ocirc; Loan</strong>, thưởng thức c&aacute;c m&oacute;n hải sản nổi tiếng tham khảo như sau:</p>
                    
                    <ul>
                        <li><strong>Ngh&ecirc;u đầm hấp sả</strong></li>
                        <li><strong>S&ograve; huyết &Ocirc; Loan nướng</strong></li>
                        <li><strong>Mực ướp ớt xi&ecirc;m nướng</strong></li>
                        <li><strong>T&ocirc;m đất nướng</strong></li>
                        <li><strong>Ghẹ ram</strong></li>
                        <li><strong>Ch&aacute;o nhum hoặc ch&aacute;o h&agrave;u</strong></li>
                        <li><strong>Cơm chi&ecirc;n hải sản</strong></li>
                    </ul>
                    
                    <p><strong>&nbsp;14h00:&nbsp;</strong>Đo&agrave;n khởi h&agrave;nh về Quy Nhơn nhận ph&ograve;ng kh&aacute;ch sạn nghỉ ngơi.</p>
                    
                    <p><strong>18h00:&nbsp;</strong>D&ugrave;ng bữa tối tại nh&agrave; h&agrave;ng với m&oacute;n đặc sản B&igrave;nh Định&nbsp;<strong>&ldquo; B&Aacute;NH X&Egrave;O T&Ocirc;M NHẢY&rdquo;</strong>&nbsp;hoặc&nbsp; thực đơn tham khảo như sau:</p>
                    
                    <ul>
                        <li><strong>M&iacute;t trộn t&ocirc;m thịt</strong></li>
                        <li><strong>Rau rừng Gia lai x&agrave;o tỏi</strong></li>
                        <li><strong>C&aacute; bống kho tộ</strong></li>
                        <li><strong>B&ograve; x&agrave;o l&aacute; lốt</strong></li>
                        <li><strong>C&aacute; biển kho</strong></li>
                        <li><strong>Canh r&ecirc;u rau đay</strong></li>
                        <li><strong>Cơm ni&ecirc;u</strong></li>
                        <li><strong>B&aacute;nh &iacute;t l&aacute; gai</strong></li>
                        <li><strong>Khăn lạnh, tr&agrave; đ&aacute;</strong></li>
                    </ul>
                    
                    <p>Buổi tối đo&agrave;n tự do kh&aacute;m ph&aacute; th&agrave;nh phố Quy Nhơn về đ&ecirc;m, thưởng thức caf&eacute; biển Sufbar, nghe h&aacute;t B&agrave;i Ch&ograve;i&hellip;</p>',
                'tour_id' => 12, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            ['day' => 2, 'name' => 'JEJU (Ăn sáng, trưa, tối)',
                'content' => '<p><strong>S&aacute;ng</strong><strong>:</strong>&nbsp;Đo&agrave;n d&ugrave;ng bữa s&aacute;ng&nbsp;<strong>Đ</strong><strong>Ặ</strong><strong>C SẢN QUY NHƠN</strong>.</p>
                    
                    <p><strong>08h00: tham quan qua Nhơn L&yacute;, một địa danh nổi tiếng của&nbsp; xinh đẹp của th&agrave;nh phố Quy Nhơn. Tr&ecirc;n h&agrave;nh tr&igrave;nh qu&yacute; kh&aacute;ch tham quan v&agrave; chụp ảnh tại cầu Thị Nại &ndash; cầu vượt biển d&agrave;i nhất Việt Nam (2500m)</strong></p>
                    
                    <p><strong>Tiếp tục h&agrave;nh tr&igrave;nh sang Nhơn L&yacute; &ndash; nơi ngự trị của những đồi c&aacute;t d&agrave;i v&agrave; đẹp.</strong></p>
                    
                    <p><strong>09h30: Qu&yacute; kh&aacute;ch đến nh&agrave; h&agrave;ng, qu&yacute; kh&aacute;ch thay đồ. HDV đưa qu&yacute; kh&aacute;ch xuống cano.&nbsp;</strong>Qu&yacute; kh&aacute;ch được trải nghiệm thuyền th&uacute;ng để di chuyển ra Cano cao tốc<br />
                    Bắt đầu h&agrave;nh tr&igrave;nh 20 ph&uacute;t để đến&nbsp;<strong>B&atilde;i Kỳ Co Hoang sơ.</strong></p>
                    
                    <p>Qu&yacute; kh&aacute;ch thoải m&aacute;i tắm, lặn biển, chụp ảnh, bắt Nhum biển. ( Trương hợp nếu thời tiết xấu th&igrave; Qu&yacute; kh&aacute;ch sẽ sang đảo KỲ CO bằng đường &ocirc; t&ocirc;)</p>
                    
                    <p><strong>Trưa</strong>: D&ugrave;ng bữa trưa ngay tr&ecirc;n x&atilde; Nhơn L&yacute; với thực đơn tham khảo như sau:</p>
                    
                    <ul>
                        <li>Soup rong biển nấu t&ocirc;m</li>
                        <li>G&oacute;i ốc trộn rau c&acirc;u ch&acirc;n vịt</li>
                        <li>Mực tươi hấp gừng</li>
                        <li>H&agrave;u nướng mỡ h&agrave;nh</li>
                        <li>T&ocirc;m s&uacute; hấp l&aacute; dứa</li>
                        <li>Ch&aacute;o hải sản</li>
                        <li>Cơm chi&ecirc;n muối ớt</li>
                        <li>C&aacute; chẽm hấp hongkong + b&aacute;nh tr&aacute;ng / Lẫu c&aacute; g&aacute;y + b&uacute;n</li>
                        <li>Tr&aacute;i c&acirc;y tr&aacute;ng miệng</li>
                    </ul>
                    
                    <p><strong>Chiều:&nbsp;</strong><strong>Sau 1</strong><strong>3</strong><strong>h&nbsp; nghỉ ngơi, qu&yacute; kh&aacute;ch khởi h&agrave;nh tham quan Eo Gi&oacute;, chụp h&igrave;nh lưu niệm tại đ&acirc;y.</strong></p>
                    
                    <p><strong>14h30: Khởi h&agrave;nh tham quan&nbsp;</strong><strong>Khu D&atilde; Ngoại Trung Lương, b&atilde;i biển được mệnh danh l&agrave; Đảo JEJU PHI&Ecirc;N BẢN VIỆT NAM.</strong></p>
                    
                    <p><strong>16h00:&nbsp;</strong>Kết th&uacute;c chương tr&igrave;nh, HDV chia tay v&agrave; hẹn gặp lại qu&yacute; kh&aacute;ch.</p>',
                'tour_id' => 12, 'created_at' => $datenow, 'updated_at' => $datenow
            ],
            //tour_id: 13 => 4N-4Đ
            ['day' => 1, 'name' => '', 'content' => '', 'tour_id' => 13, 'created_at' => $datenow, 'updated_at' => $datenow],
            ['day' => 2, 'name' => '', 'content' => '', 'tour_id' => 13, 'created_at' => $datenow, 'updated_at' => $datenow],
            ['day' => 3, 'name' => '', 'content' => '', 'tour_id' => 13, 'created_at' => $datenow, 'updated_at' => $datenow],
            ['day' => 4, 'name' => '', 'content' => '', 'tour_id' => 13, 'created_at' => $datenow, 'updated_at' => $datenow],
            //tour_id: 14 => 4N-3Đ
            ['day' => 1, 'name' => '', 'content' => '', 'tour_id' => 14, 'created_at' => $datenow, 'updated_at' => $datenow],
            ['day' => 2, 'name' => '', 'content' => '', 'tour_id' => 14, 'created_at' => $datenow, 'updated_at' => $datenow],
            ['day' => 3, 'name' => '', 'content' => '', 'tour_id' => 14, 'created_at' => $datenow, 'updated_at' => $datenow],
            ['day' => 4, 'name' => '', 'content' => '', 'tour_id' => 14, 'created_at' => $datenow, 'updated_at' => $datenow],
            //tour_id: 15 => 3N-2Đ
            ['day' => 1, 'name' => '', 'content' => '', 'tour_id' => 15, 'created_at' => $datenow, 'updated_at' => $datenow],
            ['day' => 2, 'name' => '', 'content' => '', 'tour_id' => 15, 'created_at' => $datenow, 'updated_at' => $datenow],
            ['day' => 3, 'name' => '', 'content' => '', 'tour_id' => 15, 'created_at' => $datenow, 'updated_at' => $datenow],
            //tour_id: 16 => 3N-2Đ
            ['day' => 1, 'name' => '', 'content' => '', 'tour_id' => 16, 'created_at' => $datenow, 'updated_at' => $datenow],
            ['day' => 2, 'name' => '', 'content' => '', 'tour_id' => 16, 'created_at' => $datenow, 'updated_at' => $datenow],
            ['day' => 3, 'name' => '', 'content' => '', 'tour_id' => 16, 'created_at' => $datenow, 'updated_at' => $datenow],
            //tour_id: 17 => 1N
            ['day' => 1, 'name' => '', 'content' => '', 'tour_id' => 17, 'created_at' => $datenow, 'updated_at' => $datenow],
            //tour_id: 18 => 4N-3Đ
            ['day' => 1, 'name' => '', 'content' => '', 'tour_id' => 18, 'created_at' => $datenow, 'updated_at' => $datenow],
            ['day' => 2, 'name' => '', 'content' => '', 'tour_id' => 18, 'created_at' => $datenow, 'updated_at' => $datenow],
            ['day' => 3, 'name' => '', 'content' => '', 'tour_id' => 18, 'created_at' => $datenow, 'updated_at' => $datenow],
            ['day' => 4, 'name' => '', 'content' => '', 'tour_id' => 18, 'created_at' => $datenow, 'updated_at' => $datenow],
            //tour_id: 19 => 6N-5Đ
            ['day' => 1, 'name' => '', 'content' => '', 'tour_id' => 19, 'created_at' => $datenow, 'updated_at' => $datenow],
            ['day' => 2, 'name' => '', 'content' => '', 'tour_id' => 19, 'created_at' => $datenow, 'updated_at' => $datenow],
            ['day' => 3, 'name' => '', 'content' => '', 'tour_id' => 19, 'created_at' => $datenow, 'updated_at' => $datenow],
            ['day' => 4, 'name' => '', 'content' => '', 'tour_id' => 19, 'created_at' => $datenow, 'updated_at' => $datenow],
            ['day' => 5, 'name' => '', 'content' => '', 'tour_id' => 19, 'created_at' => $datenow, 'updated_at' => $datenow],
            ['day' => 6, 'name' => '', 'content' => '', 'tour_id' => 19, 'created_at' => $datenow, 'updated_at' => $datenow],
            //tour_id: 20 => 6N-5Đ
            ['day' => 1, 'name' => '', 'content' => '', 'tour_id' => 20, 'created_at' => $datenow, 'updated_at' => $datenow],
            ['day' => 2, 'name' => '', 'content' => '', 'tour_id' => 20, 'created_at' => $datenow, 'updated_at' => $datenow],
            ['day' => 3, 'name' => '', 'content' => '', 'tour_id' => 20, 'created_at' => $datenow, 'updated_at' => $datenow],
            ['day' => 4, 'name' => '', 'content' => '', 'tour_id' => 20, 'created_at' => $datenow, 'updated_at' => $datenow],
            ['day' => 5, 'name' => '', 'content' => '', 'tour_id' => 20, 'created_at' => $datenow, 'updated_at' => $datenow],
            ['day' => 6, 'name' => '', 'content' => '', 'tour_id' => 20, 'created_at' => $datenow, 'updated_at' => $datenow],
        ] );
    }
}
