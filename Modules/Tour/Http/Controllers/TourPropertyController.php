<?php

namespace Modules\Tour\Http\Controllers;

use App\Traits\Authorizable;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Modules\Theme\Entities\Menu;
use Modules\Tour\Entities\TourProperty;
use Illuminate\Support\Str;

class TourPropertyController extends Controller
{
    use Authorizable,ValidatesRequests;
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = !empty($request->hidRecord) ? $request->hidRecord : Config("settings.perpage10");
        $type = $request->get('type');

        $tourProperties = TourProperty::sortable('prop_order', 'asc');

        if (!empty($keyword)){
            $tourProperties = $tourProperties->where('name','LIKE',"%$keyword%");
        }
        if (!empty($request->type_id)){
            $type_id = $request->type_id;
            $tourProperties = $tourProperties->where('type_id', $type_id);
        }
        $tourProperties = $tourProperties->where('type', $type);

        if ($request->ajax()){
            $tourProperties = $tourProperties->paginate($perPage)->appends($request->except('page'));
            return $tourProperties->toHtml();
        }

        $tourProperties = $tourProperties->paginate($perPage);
        $totalInActive = TourProperty::where('active', config('settings.inactive'))->where('type', $type)->count();
        $totalActive = TourProperty::where('active', config('settings.active'))->where('type', $type)->count();
        if ($type==='tour_place')
        {
            $totalTop = TourProperty::where('prop_top', 1)->where('type', $type)->count();
        } else
        {
            $totalTop = 0;
        }

        return view('tour::tour-properties.index', compact('tourProperties', 'type', 'totalInActive', 'totalActive', 'totalTop'));
    }

    /**
     * Show the form for creating a new resource.
     * * @param Request $request
     * @return Response
     */
    public function create(Request $request)
    {
        $type = $request->get('type');
        $prevUrl = \URL::previous();
        $tourCategories = TourProperty::where('type','tour_category')->pluck('name','id')->toArray();
        $tourCategories = \Arr::prepend($tourCategories,__('message.please_select'),'');

        $parentList = TourProperty::where('type','tour_place')
            ->whereNull('parent_id')
            ->pluck('name','id')->toArray();
        $parentList = \Arr::prepend($parentList,__('message.please_select'),'');

        $showHomes = \Arr::prepend(trans('tour::tour_properties.show_home_arr'), __('message.please_select'), '');

        return view('tour::tour-properties.create', compact('type', 'prevUrl', 'tourCategories', 'parentList', 'showHomes'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'code' => 'required',
            'name' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:'.(5*1024),
            'show_home' => 'nullable|unique:tour_properties'
        ]);
        $requestData = $request->all();

        \DB::transaction(function () use ($request,$requestData){
            $requestData['slug'] = Str::slug($requestData['name']);
            $requestData['slug'] = Menu::setSlug($requestData['slug']);
            if ($request->hasFile('image')){
                $requestData['image'] = TourProperty::saveImageResize($request->file('image'));
            }
            TourProperty::create($requestData);
        });

        \Session::flash('flash_message', __('tour::tour_properties.created_success'));
        return redirect('admin/control/tour-properties?type='.$requestData['type']);

    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $tourProperty = TourProperty::findOrFail($id);
        $type = $tourProperty->type;
        $prevUrl = \URL::previous();

        return view('tour::tour-properties.show', compact('tourProperty', 'type', 'prevUrl'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id, Request $request)
    {
        $tourProperty = TourProperty::findOrFail($id);
        $type = $tourProperty->type;
        $tourCategories = TourProperty::where('type','tour_category')->pluck('name','id');
        $parentList = TourProperty::where('type','tour_place')
            ->whereNull('parent_id')
            ->pluck('name','id')->toArray();
        $parentList = \Arr::prepend($parentList,__('message.please_select'),'');

        $showHomes = \Arr::prepend(trans('tour::tour_properties.show_home_arr'), __('message.please_select'), '');

        $prevUrl = '';
        if ($request['hidUrl'] != null && $request['hidUrl']!='')
            $prevUrl = $request['hidUrl'];
        else
            $prevUrl = \URL::previous();

        return view('tour::tour-properties.edit', compact('tourProperty', 'tourCategories', 'parentList', 'prevUrl', 'type','showHomes'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $tourProperty = TourProperty::findOrFail($id);
        $showHome = $request->show_home == $tourProperty->show_home ? '' : '|unique:tour_properties';
        $this->validate($request, [
            'code' => 'required',
            'name' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:'.(5*1024),
            'show_home' => 'nullable' . $showHome
        ]);

        $requestData = $request->all();

        if (empty($request->get('slug'))){
            $requestData['slug'] = Str::slug($requestData['name']);
            $requestData['slug'] = Menu::setSlug($requestData['slug']);
        }

        if ($request->hasFile('image')){
        	TourProperty::deleteFile($tourProperty->image);
            $requestData['image'] = TourProperty::saveImageResize($request->file('image'));
        }

        if ($requestData['type']=='tour_place' && empty($requestData['prop_top'])){
            $requestData['prop_top'] = 0;
        }

        if (empty($requestData['active'])){
            $requestData['active'] = 0;
        }

        $tourProperty->update($requestData);
        \Session::flash('flash_message', __('tour::tour_properties.updated_success'));

        return \Redirect::to($requestData['hidUrl']);
//        return redirect('admin/control/tour-properties?type='.$requestData['type']);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $tourProperty = TourProperty::findOrFail($id);
        $type = $tourProperty->type;
	    TourProperty::deleteFile($tourProperty->image);

        TourProperty::destroy($id);
        \Session::flash('flash_message', __('tour::tour_properties.deleted_success'));
        return redirect('admin/control/tour-properties?type='.$type);
    }
}
