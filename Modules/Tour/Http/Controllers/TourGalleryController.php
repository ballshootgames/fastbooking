<?php

namespace Modules\Tour\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Modules\News\Entities\News;
use Modules\Tour\Entities\TourGallery;

class TourGalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('tour::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('tour::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'tour_id' => 'required',
            'file' => 'mimes:jpeg,png,jpg'
        ]);

        $file = $request->file('file');
        if ($path_image = TourGallery::saveImageResize($file)){
            $info['file_id'] = 0;
            if ($request->tour_id > 0){
                $tour_gallery = new TourGallery();
                $tour_gallery = $tour_gallery->create(['tour_id' => $request->tour_id, 'image' => $path_image ]);
                $info['file_id'] = $tour_gallery->id;
            }
            $info['name'] = $path_image;
            $info['src'] = asset(\Storage::url($path_image));
            return \response()->json($info);
        }
        return \response()->json('error');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('tour::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('tour::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $image = TourGallery::findOrFail($id);
        TourGallery::deleteFile($image->image);
        TourGallery::destroy($id);
        return \Response::json('success', 200);
    }

    public function deleteGallery($id)
    {
        $image = TourGallery::findOrFail($id);
        TourGallery::deleteFile($image->image);
        TourGallery::destroy($id);
        return \Response::json('success', 200);
    }


    public function deleteImage(Request $request){
        $folder = $request->folder;
        TourGallery::deleteFile($folder);

        return \Response::json('success', 200);
    }
}
