<?php

namespace Modules\Tour\Http\Controllers;

use App\City;
use App\Country;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Tour\Entities\Tour;
use Modules\Tour\Entities\TourProperty;
use Modules\Tour\Entities\TourSchedule;

class AjaxController extends Controller
{
    use ValidatesRequests;
    /**
     * Gọi ajax: sẽ gọi đến hàm = tên $action
     * @param Request $action
     * @param Request $request
     * @return mixed
     */
    public function index($action, Request $request)
    {
        return $this->{$action}($request);
    }


    /**
     * Get guides tour by date start -> date end
     * @param Request $request
     *
     * @return mixed
     */
    public function getGuidesByDay(Request $request){
        $this->validate($request, [
            'id' => 'required|integer',
            'type' => 'required|in:tours',
            'start' => 'required',
            'end' => 'required',
        ]);
        $startDate = gmdate("Y-m-d", $request->start);
        $endDate = date('Y-m-d', strtotime(gmdate("Y-m-d", $request->end).' -1 day'));
        $item = Tour::findOrFail($request->id);
        return $item->guides()->with('user:id,name')->where(function ($query) use ($startDate, $endDate){
            $query->whereBetween('date_start', [$startDate, $endDate])
                ->orWhereBetween('date_end', [$startDate, $endDate])
                ->orWhere([
                    ['date_start', '<', $startDate],
                    ['date_end', '>', $endDate],
                ])
                ->orWhere(function ($query) use ($endDate){
                    $query->whereNull('date_end')->where('date_start', '<=', $endDate);
                });
        })->get();
    }

    public function searchComplete(Request $request){
        $idPlace = $request->get('idPlace');
        $cities = TourProperty::where(['type' => 'tour_place', 'parent_id' => $idPlace])->get();
        $result = [];
        foreach ($cities as $key){
            $result[] = ['id' => $key->id, 'name' => $key->name];
        }

        return \response()->json($result);
    }

    public function getListCities(Request $request){
        $country_id = $request->country_id;
        $cities = City::where('country_id', $country_id)->pluck('name', 'id');
        return \response()->json($cities);
    }

    public function getAutoCompleteCity(Request $request){
        return \response()->json(City::where('name','LIKE',"%$request->city%")->orderBy('updated_at','desc')->get());
    }
    public function getAutoCompleteCountry(Request $request){
        return \response()->json(Country::where('name','LIKE',"%$request->country%")->orderBy('updated_at','desc')->get());
    }
    //Delete Item
    public function delItem(Request $request){
        $ids = $request->ids;
        $arrId = explode(',', $ids,-1);
        foreach ($arrId as $item){
            Tour::destroy($item);
        }
        return \response()->json(['success' => 'ok']);
    }
    //Active Item
    public function activeItem(Request $request){
        $ids = $request->ids;
        $arrId = explode(',', $ids,-1);
        foreach ($arrId as $item){
            $tour = Tour::findOrFail($item);
            $active = $tour->active == config('settings.active') ? config('settings.inactive') : config('settings.active');
            \DB::table('tours')->where('id',$tour->id)->update(['active' => $active, 'updator_id' => \Auth::user()->id]);
        }
        return \response()->json(['success' => 'ok']);
    }
    //Save Day Schedule
    public function updateDaySchedules(Request $request){
        $newDay = (float)$request->newDay;
        $oldDay = (float)$request->oldDay;
        $idTour = (int)$request->idTour;
        TourSchedule::updateTourScheduleWithDay($oldDay, $newDay, $idTour);
        \DB::table('tours')->where('id', $idTour)->update(['day_number' => $newDay]);
        $schedules = TourSchedule::where('tour_id',$idTour)->orderBy('day')->get();
        return \response()->json(['success' => 'ok', 'data' => $schedules]);
    }
    //Delete Property Items
    public function delPropertyItem(Request $request){
        $ids = $request->ids;
        $arrId = explode(',', $ids,-1);
        foreach ($arrId as $item){
            $tourProperty = TourProperty::findOrFail($item);
            //delete image
            TourProperty::deleteFile($tourProperty->image);

            TourProperty::destroy($item);
        }
        return \response()->json(['success' => 'ok']);
    }
    //Active Property Items
    public function activePropertyItem(Request $request){
        $ids = $request->ids;
        $arrId = explode(',', $ids,-1);
        foreach ($arrId as $item){
            $property = TourProperty::findOrFail($item);
            $active = $property->active == config('settings.active') ? config('settings.inactive') : config('settings.active');
            \DB::table('tour_properties')->where('id',$property->id)->update(['active' => $active, 'updator_id' => \Auth::user()->id]);
        }
        return \response()->json(['success' => 'ok']);
    }
    /*
     * Get Auto Code Follow Prefix
     * param: request
     * */
    public function getAutoCodePrefix(Request $request){
        $prefixCode = $request->get('prefixCode');
        $code = Tour::autoCode($prefixCode);
        return \response()->json($code);
    }
}
