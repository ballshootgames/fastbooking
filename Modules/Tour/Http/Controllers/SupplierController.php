<?php

namespace Modules\Tour\Http\Controllers;

use App\Traits\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Modules\Tour\Entities\Supplier;
use Session;

class SupplierController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $keywordSearch = $request->get('search');
        $perPage = config('settings.perpage10');
        $suppliers = new Supplier();
        if (!empty($keywordSearch)){
            $suppliers = $suppliers->where('name','LIKE',"%$keywordSearch%");
        }
        $suppliers = $suppliers->paginate($perPage);
        return view('tour::suppliers.index', compact('suppliers'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $prevUrl = \URL::previous();
        return view('tour::suppliers.create', compact('prevUrl'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);
        $requestData = $request->all();
        Supplier::create($requestData);
        Session::flash('flash_message', __('tour::suppliers.created_success'));
        return redirect('admin/control/suppliers');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $supplier = Supplier::findOrFail($id);
        $prevUrl = \URL::previous();
        return view('tour::suppliers.show', compact('supplier','prevUrl'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id, Request $request)
    {
        $supplier = Supplier::findOrFail($id);
        $prevUrl = '';
        if ($request['hidUrl'] != null && $request['hidUrl']!='')
            $prevUrl = $request['hidUrl'];
        else
            $prevUrl = \URL::previous();
        return view('tour::suppliers.edit', compact('supplier','prevUrl'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);
        $requestData = $request->all();
        $supplier = Supplier::findOrFail($id);
        $supplier->update($requestData);
        Session::flash('flash_message', __('tour::suppliers.updated_success'));
        return redirect('admin/control/suppliers');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        Supplier::destroy($id);
        Session::flash('flash_message', __('tour::suppliers.deleted_success'));
        return redirect('admin/control/suppliers');
    }
}
