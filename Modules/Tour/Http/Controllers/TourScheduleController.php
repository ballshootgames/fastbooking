<?php

namespace Modules\Tour\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Modules\Tour\Entities\TourProperty;
use Modules\Tour\Entities\TourSchedule;

class TourScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('tour::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('tour::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('tour::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id, Request $request)
    {
        $tour_schedule = TourSchedule::with('cities')->findOrFail($id);
        $tourPlaces = TourProperty::where(['type'=>'tour_place','parent_id' => null])->get();
        $resultPlaces = [];
        foreach ($tourPlaces as $key){
            $resultPlaces[] = ['id' => $key->id, 'name' => $key->name];
        }
        $url = url(\Storage::url($tour_schedule->image));
        $idTourPlaces = $tour_schedule->cities()->pluck('city_id')->all();
        $resultDestinations = [];
        foreach ($idTourPlaces as $val){
            $resultDestinations[] = TourProperty::where([ ['id','=', $val], ['parent_id','<>', null] ])->first();
        }
        if ($request->wantsJson()){
            return \response()->json(['schedule' => $tour_schedule,'tourPlaces' => $resultPlaces, 'idTourPlaces' => $idTourPlaces,'resultDestinations' => $resultDestinations, 'url_image' => $url]);
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();
        $requestSchedule = $requestData['schedule'];

        $tourSchedule = TourSchedule::findOrFail($id);

        \DB::transaction(function () use ($request, $requestData, $requestSchedule, $tourSchedule){
            $tourSchedule->update($requestSchedule);
            if (!empty($requestData['schedule_places'])){
                if (!empty($requestData['schedule_cities']) && count($requestData['schedule_cities'])>0){
                    $requestData['schedule_places'] = explode(" ", $request->schedule_places);
                    $requestData['schedule_cities'] = array_merge($requestData['schedule_cities'],$requestData['schedule_places']);
                    $tourSchedule->cities()->sync($requestData['schedule_cities']);
                }else{
                    $tourSchedule->cities()->sync($requestData['schedule_places']);
                }
            }else{
                $tourSchedule->cities()->detach();
            }
        });
        $tourSchedules = TourSchedule::where('tour_id', $requestData['tour_id'])->orderBy('day')->get();
        return \response()->json($tourSchedules);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function uploadFile(Request $request, $id){
        $this->validate($request, [
            'file' => 'image|mimes:jpeg,png,jpg,gif|max:'.(5*1024),
        ]);
        $tourSchedule = TourSchedule::find($id);
        //delete old image
	    TourSchedule::deleteFile($tourSchedule->image);

        //save new image
	    $nameFile = null;
        if ($request->hasFile('file')){
            $nameFile = TourSchedule::saveImageResize($request->file('file'));
        }
        $tourSchedule->image = $nameFile;
        $tourSchedule->save();
        return \response()->json($nameFile);
    }
}
