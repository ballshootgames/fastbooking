<?php

namespace Modules\Tour\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Modules\News\Entities\News;
use Modules\Tour\Entities\TourGallery;
use Modules\Tour\Entities\TourMedia;

class TourMediaController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('tour::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('tour::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'tour_id' => 'required',
        ]);
        $file = $request->file('file');

        if ($path_image = TourMedia::saveMediaFiles($file)){
            $type= $path_image['type'];
            $file_path= $path_image['name'];

            $info['file_id'] = 0;
            if ($request->tour_id > 0){
                $tour_media = new TourMedia();
                $tour_media = $tour_media->create(['tour_id' => $request->tour_id, 'file_path' => $file_path ]);
                $info['file_id'] = $tour_media->id;
            }
            $info['name'] = $file_path;
            $info['type'] = $type;
            $d = asset(\Storage::url($file_path));
            $info['src'] = asset(\Storage::url($file_path));
            return \response()->json($info);
        }
        return \response()->json('error');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('tour::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('tour::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $image = TourMedia::findOrFail($id);
        TourMedia::deleteFile($image->file_path);
        TourMedia::destroy($id);
        return \Response::json('success', 200);
    }

    public function deleteMedia($id)
    {
        $image = TourMedia::findOrFail($id);
        TourMedia::deleteFile($image->file_path);
        TourMedia::destroy($id);
        return \Response::json('success', 200);
    }

    public function deleteFile(Request $request){
        $folder = $request->folder;
        TourMedia::deleteFile($folder);

        return \Response::json('success', 200);
    }
}
