<?php

namespace Modules\Tour\Http\Controllers;

use App\BaseModel;
use App\Country;
use App\Http\Controllers\Controller;
use App\City;
use App\Setting;
use App\Traits\Authorizable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Support\Facades\Date;
use Illuminate\Validation\Rule;
use Modules\Booking\Entities\Booking;
use Modules\Theme\Entities\Menu;
use Modules\Tour\Entities\Supplier;
use Modules\Tour\Entities\Tour;
use Modules\Tour\Entities\TourDeposit;
use Modules\Tour\Entities\TourGallery;
use Modules\Tour\Entities\TourLinked;
use Modules\Tour\Entities\TourProperty;
use Modules\Tour\Entities\TourSchedule;
use Modules\Tour\Transformers\TourResource;
use phpseclib\Crypt\Random;
use Session;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class ToursController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    use Authorizable,ValidatesRequests;

    public function index(Request $request)
    {
        if (Session::has('userEdit')){
            \DB::table('sessions')->where('user_id', \Auth::id())->delete();
            Session::forget('userEdit');
        }
        $keyword = $request->get('search');
        $perPage = !empty($request->record) ? $request->record : Config("settings.perpage10");

        $tours = Tour::sortable();
        if (!empty($keyword)) {
            $tours = $tours->where('name', 'LIKE', "%$keyword%")
                ->orWhere('code', 'LIKE', "%$keyword%")->orWhere('prefix_code', 'LIKE', "%$keyword%");
        }
        if (!empty($request->start_city)){
            $start_city = $request->start_city;
            $tours = $tours->with('property')->whereHas('property', function ($query) use ($start_city){
                $query->where('type', 'tour_place')
                    ->where('id', $start_city);
            });
        }
        if (!empty($request->tour_category)){
            $tour_category_id = $request->tour_category;
            $tours = $tours->whereHas('properties', function ($query) use ($tour_category_id){
                $query->where('type', 'tour_category')
                    ->where('id', $tour_category_id);
            });
        }
        if (!empty($request->tour_place)){
            $tour_place_id = $request->tour_place;
            $tours = $tours->with('properties')->whereHas('properties', function ($query) use ($tour_place_id){
                $query->where('type', 'tour_place')
                    ->where('id', $tour_place_id);
            });
        }
        if (!empty($request->tour_list)){
            $tour_list_id = $request->tour_list;
            $tours = $tours->with('properties')->whereHas('properties', function ($query) use ($tour_list_id){
                $query->where('type', 'tour_list')
                    ->where('id', $tour_list_id);
            });
        }
        if (!empty($request->tour_type)){
            $tour_type_id = $request->tour_type;
            $tours = $tours->with('properties')->whereHas('properties', function ($query) use ($tour_type_id){
                $query->where('type', 'tour_type')
                    ->where('id', $tour_type_id);
            });
        }
        if ($request->ajax()){
            $tours = $tours->paginate($perPage)->appends($request->except('page'));
            return $tours->toHtml();
        }

        if($request->wantsJson()){
            $tours = $tours->paginate($perPage)->appends($request->except('page'));
            return TourResource::collection($tours);
        }

        $tours = $tours->with('bookings');
        $totalInActive = Tour::where('active', config('settings.inactive'))->count();
        $totalActive = Tour::where('active',config('settings.active'))->count();
        $tours = $tours->orderBy('updated_at','DESC')->paginate($perPage);

        return view('tour::tours.index', compact('tours','totalActive','totalInActive'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $tour_class = $this->getTourProperties('tour_class', true);
        $tour_type = $this->getTourProperties('tour_type', true);
        $tour_category = $this->getTourProperties('tour_category', true);
        $tour_place = $this->getTourProperties('tour_place');
        $tour_list = $this->getTourProperties('tour_list');
        $tour_start_city = TourProperty::where(['type' => 'tour_place', 'parent_id' => null])->get();
        $countries = Country::pluck('name', 'id');
        $cities = [];
        $codePrefix = Setting::allConfigsKeyValue();
        $arrCodePrefix = explode(',', $codePrefix['code_prefix']);
        $code = Tour::autoCode($arrCodePrefix[0]);
        $prevUrl = \URL::previous();
        $tour_right = Tour::where('active',config('settings.active'))->pluck('name', 'id')->all();
        $tour_related = Tour::where('active',config('settings.active'))->pluck('name', 'id')->all();
        $settingDeposit = Setting::allConfigsKeyValue();
        $departureTypes = trans('tour::tours.departure_type');
        $suppliers = Supplier::pluck('name','id')->all();
        $suppliers = Arr::prepend($suppliers, __('message.please_select'), '');
        return view('tour::tours.create', compact('tour_right','tour_related','tour_class', 'tour_type',
            'tour_category', 'tour_place', 'tour_list', 'countries', 'cities', 'code', 'arrCodePrefix',
            'tour_start_city', 'prevUrl', 'settingDeposit','departureTypes','suppliers'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'day_number' => 'required',
            'night_number' => 'required|numeric',
            'price' => 'required|numeric',
            'start_city' => 'required',
            'limit_start_date' => 'nullable|date_format:"'.config('settings.format.date').'"',
            'limit_end_date' => 'nullable|after:limit_start_date|date_format:"'.config('settings.format.date').'"',
            'departure_date' => 'nullable|after_or_equal:today|date_format:"'.config('settings.format.date').'"',
        ]);

        $tour = new Tour();
        \DB::transaction(function () use ($request, &$tour){
            $requestData = $request->all();
            $codePrefix = !empty($request->get('prefix_code')) ? $request->get('prefix_code') . '-' : '';
            $codePrefixPlace = !empty($request->prefix_code_place) ? $request->prefix_code_place . '-' : '';
            $requestData['prefix_code'] =  $codePrefix . $codePrefixPlace;
            $arrTourDeposits = array_combine(array_values($requestData['deposit_name']), array_values($requestData['deposit_value']));
            if (!empty($requestData['limit_start_date'])){
                $requestData['limit_start_date'] = Carbon::createFromFormat(config('settings.format.date'),$requestData['limit_start_date'])->format('Y-m-d');
            }
            if (!empty($requestData['limit_end_date'])){
                $requestData['limit_end_date'] = Carbon::createFromFormat(config('settings.format.date'),$requestData['limit_end_date'])->format('Y-m-d');
            }
            if (!empty($requestData['departure_date'])){
                $requestData['departure_date'] = Carbon::createFromFormat(config('settings.format.date'),$requestData['departure_date'])->format('Y-m-d');
            }

            if (empty($request->get('slug'))){
                $requestData['slug'] = Str::slug($requestData['name']);
            }
            $requestData['slug'] = Menu::setSlug($requestData['slug']);

            if ($request->hasFile('image')){
                $requestData['image'] = Tour::saveImageResize($request->file('image'));
            }

            $arrSchedule = TourSchedule::getListArrSchedule((float)$requestData['day_number']);

            //Tạo mới Tour + lịch trình tour theo số ngày
            $tour = $tour->create($requestData);
            $tour->schedules()->createMany($arrSchedule);
            $this->saveTourDeposit($arrTourDeposits, $tour->id);

            if (!empty($requestData['files'])){
                if (count($requestData["files"]) > 0) {
                    $count = 1;
                    foreach ($requestData['files'] as $file) {
                        $tour->galleries()->create([
                            'image' => $file,
                            'tour_id' => $tour->id,
                            'image_order' => $count++
                        ]);
                    }
                }
            }

            //Thêm TourProperty
            $arr = [];
            foreach ($requestData['tour_property'] as $value){
                if (!empty($value)){
                    if (is_array($value)){
                        foreach ($value as $item){
                            $arr[] = $item;
                        }
                    }else{
                        $arr[] = $value;
                    }
                }
            }
            if (!empty($arr)) $tour->properties()->attach($arr);

            if ($request->hasFile('supplier_files')){
                $supplierFiles = $request->file('supplier_files');
                $files = Supplier::saveSupplierFiles($supplierFiles);
                $tour->suppliers()->attach($requestData['supplier'], ['files' => $files['name']]);
            }

            //Thêm tour bán kèm
            $fieldsTourLinked = [
                'tour_id' => $tour->id,
                'tour_right' => implode("$", !empty($requestData['tour_right']) ? array_slice($requestData['tour_right'], 0, 3) : []),
                'tour_related' => implode("$", !empty($requestData['tour_related']) ? array_slice($requestData['tour_related'], 0, 8) : [])
            ];
            $tour->tourLinked()->create($fieldsTourLinked);
        });

        if ($request->wantsJson()){
            return \response()->json([
                'message' => __('tour::tours.created_success')
            ]);
        }
        \Session::flash('flash_message', __('tour::tours.created_success'));
        $prevUrl = $request->get('hidUrl');

        return redirect('admin/control/tours/'.$tour->id.'/edit?continue='.$prevUrl);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id, Request $request)
    {
        $tour = Tour::findOrFail($id);
        $existLimitPeople = Booking::getExistPeopleLimitTour($tour);
        $tour_schedules = $tour->schedules()->orderBy('day', 'asc')->get();
        if ($request->wantsJson()){
            return new TourResource($tour);
        }
        $prevUrl = \URL::previous();
        return view('tour::tours.show', compact('tour', 'existLimitPeople','tour_schedules', 'prevUrl'));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id, Request $request)
    {
        $tour = Tour::findOrFail($id);
        \App\Session::create([
            'model_type' => Tour::class,
            'model_id' => $tour->id,
            'last_activity' => \Str::random(20),
            'user_id' => \Auth::id()
        ]);
        Session::put('userEdit', \Str::random(20));
        $settings = Setting::allConfigsKeyValue();
        $arrCodePrefix = explode(',', $settings['code_prefix']);
        $codePrefix = explode('-', $tour->prefix_code);
        if ($settings['chk_code_prefix'] == config('settings.active')){
            $prefix_code = in_array($codePrefix[0],$arrCodePrefix) ? $codePrefix[0] : $arrCodePrefix[0];
            $prefix_code_text = in_array($codePrefix[0],$arrCodePrefix) ? $codePrefix[1] : $codePrefix[0];
        }else{
            $prefix_code = '';
            $prefix_code_text = $codePrefix[0];
        }

        $tour_type_id = $tour->properties->where('type', 'tour_type')->first();
        $tour_class_id = $tour->properties->where('type', 'tour_class')->first();
        $tour_category_id = $tour->properties->where('type', 'tour_category')->first();
        $tour_place_id = $tour->properties->where('type', 'tour_place')->pluck('id')->all();
        $tour_list_id = $tour->properties->where('type', 'tour_list')->pluck('id')->all();
        $tour_class = $this->getTourProperties('tour_class', true);
        $tour_type = $this->getTourProperties('tour_type', true);
        $tour_category = $this->getTourProperties('tour_category', true);
        $tour_place = $this->getTourProperties('tour_place');
        $tour_list = $this->getTourProperties('tour_list');
        $tour_start_city = TourProperty::where(['type' => 'tour_place', 'parent_id' => null])->get();
        $countries = Country::pluck('name', 'id');
        $country_id = optional(optional($tour->city)->country)->id;
        $cities = City::where('country_id', $country_id)->pluck('name', 'id');
        $tour_schedules = $tour->schedules;
        $code = $tour->code;
        $tour_right = Tour::where([['id','<>',$id],['active','=',config('settings.active')]])->pluck('name', 'id')->all();
        $tour_related = Tour::where([['id','<>',$id],['active','=',config('settings.active')]])->pluck('name', 'id')->all();
        $tourLink = TourLinked::where('tour_id', $id)->first();
        $tour_right_id = [];
        $tour_related_id = [];
        $settingDeposit = Setting::allConfigsKeyValue();
        if (!empty($tourLink)){
            $tour_right_id = explode('$', $tourLink->tour_right);
            $tour_related_id = explode('$', $tourLink->tour_related);
        }
        $departureTypes = trans('tour::tours.departure_type');
        $suppliers = Supplier::pluck('name','id')->all();
        $suppliers = Arr::prepend($suppliers, __('message.please_select'), '');
        $prevUrl = '';
        $tmp = explode("continue=",$_SERVER['REQUEST_URI']);
        if ($request['hidUrl'] != null && $request['hidUrl']!='')
        {
            $prevUrl = $request['hidUrl'];
            if (isset($tmp) && count($tmp) > 1)
            {
                $prevUrl = $tmp[1];
            }
        }
        else{
            $prevUrl = \URL::previous();
            if (isset($tmp) && count($tmp) > 1)
            {
                $prevUrl = $tmp[1];
            }
        }
        return view('tour::tours.edit', compact('settingDeposit','tour_related','tour_right','tour',
            'tour_class', 'tour_class_id', 'tour_type', 'tour_type_id', 'tour_category', 'tour_category_id',
            'tour_place', 'tour_place_id', 'tour_list', 'tour_list_id', 'tour_schedules', 'countries',
            'country_id', 'cities', 'code', 'prefix_code', 'prefix_code_text','arrCodePrefix', 'tour_start_city',
            'prevUrl', 'tour_right_id', 'tour_related_id','departureTypes','suppliers'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'day_number' => 'required|numeric',
            'night_number' => 'required|numeric',
            'price' => 'numeric|nullable',
            'start_city' => 'required',
            'limit_start_date' => 'nullable|date_format:"'.config('settings.format.date').'"',
            'limit_end_date' => 'nullable|after:limit_start_date|date_format:"'.config('settings.format.date').'"',
            'departure_date' => 'nullable|date_format:"'.config('settings.format.date').'"',
        ]);

        $tour = Tour::findOrFail($id);

        \DB::transaction(function () use ($request, &$tour, $id){
            $requestData = $request->all();
            $codePrefix = !empty($request->get('prefix_code')) ? $request->get('prefix_code') . '-' : '';
            $codePrefixPlace = !empty($request->prefix_code_place) ? $request->prefix_code_place . '-' : '';
            $requestData['prefix_code'] =  $codePrefix . $codePrefixPlace;

            $arrTourDeposits = array_combine(array_values($requestData['deposit_name']), array_values($requestData['deposit_value']));

            //Cập nhật lịch trình Tour nếu thay đổi số ngày
            TourSchedule::updateTourScheduleWithDay($tour->day_number, (float)$requestData['day_number'], $tour->id);

            if (isset($requestData['check_limit'])){
                if (!empty($requestData['limit_start_date'])){
                    $requestData['limit_start_date'] = Carbon::createFromFormat(config('settings.format.date'),$requestData['limit_start_date'])->format('Y-m-d');
                }
                if (!empty($requestData['limit_end_date'])){
                    $requestData['limit_end_date'] = Carbon::createFromFormat(config('settings.format.date'),$requestData['limit_end_date'])->format('Y-m-d');
                }
            }else{
                $requestData['limit'] = null;
                $requestData['limit_start_date'] = null;
                $requestData['limit_end_date'] = null;
            }

            if (empty($requestData['hot'])){
                $requestData['hot'] = 0;
            }

            if (empty($requestData['is_promotion'])){
                $requestData['is_promotion'] = 0;
            }

            if (empty($requestData['active'])){
                $requestData['active'] = 0;
            }

            if (empty($requestData['installment'])){
                $requestData['installment'] = 0;
            }

            if (!empty($requestData['departure_date'])){
                $requestData['departure_date'] = Carbon::createFromFormat(config('settings.format.date'),$requestData['departure_date'])->format('Y-m-d');
            }else{
                $requestData['departure_date'] = null;
            }

            if (empty($request->get('slug'))){
                $requestData['slug'] = \Str::slug($requestData['name']);
                $requestData['slug'] = Menu::setSlug($requestData['slug']);
            }elseif($request->get('slug') !== (string)$tour->slug){
                $requestData['slug'] = $request->get('slug');
                $requestData['slug'] = Menu::setSlug($requestData['slug']);
            }

            if (!empty($requestData['check_delete_image'])){
                Tour::deleteFile($tour->image);
                $requestData['image'] = null;
            }

            if ($request->hasFile('image')){
	            Tour::deleteFile($tour->image);
                $requestData['image'] = Tour::saveImageResize($request->file('image'));
            }

            $tour->update($requestData);
            if (!empty($requestData['files'])){
                if (count($requestData["files"]) > 0) {
                    $count = 1;
                    $tourGallery = TourGallery::where('tour_id', $tour->id)->orderBy('image_order', 'ASC')->get();
                    if (!empty($tourGallery)){
                        foreach ($tourGallery as $file){
                            TourGallery::destroy($file->id);
                        }
                    }

                    foreach ($requestData['files'] as $file) {
                        $tour->galleries()->create([
                            'image' => $file,
                            'tour_id' => $tour->id,
                            'image_order' => $count++
                        ]);
                    }

                }
            }

            $this->updateTourDeposit($request->delete_deposit, $arrTourDeposits,$request->deposit_id, $tour);

            $arr = [];
            foreach ($requestData['tour_property'] as $value){
                if (!empty($value) > 0){
                    if (is_array($value)){
                        foreach ($value as $item){
                            $arr[] = $item;
                        }
                    }else{
                        $arr[] = $value;
                    }
                }
            }
            if (!empty($arr) >0){
                $tour->properties()->sync($arr);
            }else{
                $tour->properties()->detach();
            }
            if ($request->hasFile('supplier_files')){
                $supplierFiles = $request->file('supplier_files');
                $files = Supplier::saveSupplierFiles($supplierFiles);
                if ($tour->suppliers->count() > 0 && $requestData['supplier'] == optional($tour->suppliers[0]->pivot)->supplier_id){
                    $tour->suppliers()->sync([$requestData['supplier'] => ['files' => $files['name']]]);
                }else{
                    $tour->suppliers()->attach($requestData['supplier'], ['files' => $files['name']]);
                }
            }else{
                $tour->suppliers()->detach();
            }
            $fieldsTourLinked = [
                'tour_id' => $tour->id,
                'tour_right' => implode("$", !empty($requestData['tour_right']) ? array_slice($requestData['tour_right'], 0, 3) : []),
                'tour_related' => implode("$", !empty($requestData['tour_related']) ? array_slice($requestData['tour_related'], 0, 8) : [])
            ];
            $tourLink = TourLinked::where('tour_id', $id)->first();
            if (!empty($tourLink)){
                $tour->tourLinked()->update($fieldsTourLinked);
            }else{
                $tour->tourLinked()->create($fieldsTourLinked);
            }
        });

        if ($request->wantsJson()){
            return \response()->json([
                'message' => __('tour::tours.updated_success'),
                'id' => $tour->id
            ]);
        }
        \App\Session::delSession($tour);
        Session::flash('flash_message', __('tour::tours.updated_success'));
        if ($request->has('btn-continue')){
            return redirect()->back();
        }
        return \Redirect::to($request->get('hidUrl'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id, Request $request)
    {
        Tour::destroy($id);

        if ($request->wantsJson()){
            return \response()->json([
                'message' => __('tour::tours.deleted_success'),
                'id' => $id
            ]);
        }

        Session::flash('flash_message', __('tour::tours.deleted_success'));
        return redirect('admin/control/tours');
    }

    /*
     * Copy 1 row tour
     * */
    public function getCopyTour(Request $request){
        $tour = Tour::with('schedules')->where('id',$request->id)->first();
        $newTour = $tour->replicate();
        $newTour->code = Tour::autoCode();
        $newTour->name = Tour::autoCopy('name', $tour->name) ;
        $newTour->slug = Menu::setSlug($tour->slug);
        if (\Auth::user()->username !== 'admin'){
            $newTour->active = config('settings.inactive');
        }
        $newTour->save();
        $tourType = $tour->properties->where('type', 'tour_type')->first();
        $tourClass = $tour->properties->where('type', 'tour_class')->first();
        $tourCategory = $tour->properties->where('type', 'tour_category')->first();
        $tourLists = $tour->properties->where('type', 'tour_list');
        $tourPlaces = $tour->properties->where('type', 'tour_place');
        $tourTypeId = !empty($tourType->id) ? $tourType->id : 0;
        $tourClassId = !empty($tourClass->id) ? $tourClass->id : 0;
        $tourCategoryId = !empty($tourCategory->id) ? $tourCategory->id : 0;
        $arrTour = [$tourTypeId,$tourClassId,$tourCategoryId];
        if (!empty($tourLists) && count($tourLists)){
            foreach ($tourLists as $tourProperty){
                $arrTour[] = $tourProperty->id;
            }
        }
        if (!empty($tourPlaces) && count($tourPlaces)){
            foreach ($tourPlaces as $tourProperty){
                $arrTour[] = $tourProperty->id;
            }
        }
        $newTour->properties()->sync($arrTour);
        // duplicate tour schedule
        foreach($tour->schedules()->get() as $t)
        {
            $tourSchedule = TourSchedule::where('id',$t->id)->first();
            $newTourSchedule = $tourSchedule->replicate();
            $newTourSchedule->tour_id = $newTour->id;
            $newTourSchedule->save();
            if (!empty($tourSchedule->cities) && count($tourSchedule->cities)){
                $arr = [];
                foreach ($tourSchedule->cities as $city){
                    $arr[] = $city->id;
                }
                $newTourSchedule->cities()->sync($arr);
            }
        }
        // duplicate tour gallery
        foreach($tour->galleries()->get() as $t)
        {
            $tourGallery = TourGallery::where('id',$t->id)->first();
            $newTourGallery = $tourGallery->replicate();
            $newTourGallery->tour_id = $newTour->id;
            $newTourGallery->save();
        }

        Session::flash('flash_message', __($tour->name . ' đã được sao chép!'));
        return redirect('admin/control/tours');
    }

    /*Get Tour Properties*/
    public function getTourProperties($defineTourProperty, $please = false, $arg1 = 'name', $arg2 = 'id'){
        $tourProperty = TourProperty::where(['type' => $defineTourProperty, 'parent_id' => null])->pluck($arg1, $arg2);
        if ($please){
            $tourProperty->prepend(__('message.please_select'), '')->all();
        }
        return $tourProperty;
    }

    /*Save tour Deposit*/
    private function saveTourDeposit($arrTourDeposits, $idTour){
        if (!is_null($arrTourDeposits)){
            foreach ($arrTourDeposits as $key => $value){
                if (!empty($key) && !empty($value)){
                    $tourDeposit = new TourDeposit();
                    $tourDeposit->name = $key;
                    $tourDeposit->value = $value;
                    $tourDeposit->tour_id = $idTour;
                    $tourDeposit->save();
                }

            }
        }
    }
    /*Update tour Deposit*/
    private function updateTourDeposit($deleteDeposit, $arrTourDeposits, $arrIdTourDeposits, $tour){
        if (!is_null($deleteDeposit)){
            $str = rtrim($deleteDeposit, ';');
            $arrDeleteDeposit = explode(';', $str);
            for($i = 0; $i < count($arrDeleteDeposit); $i++) {
                $tourDeposit = TourDeposit::find($arrDeleteDeposit[$i]);
                if (!is_null($tourDeposit)){
                    $tourDeposit->delete();
                }
            }
        }
        if (isset($arrIdTourDeposits) && count($arrIdTourDeposits) > 0){
            foreach ($arrIdTourDeposits as $item){
                TourDeposit::where('id', $item)->delete();
            }
        }
        $this->saveTourDeposit($arrTourDeposits,$tour->id);
    }
}
