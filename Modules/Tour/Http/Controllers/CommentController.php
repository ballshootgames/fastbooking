<?php

namespace Modules\Tour\Http\Controllers;

use App\Traits\Authorizable;
use App\Traits\ImageResize;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Modules\Tour\Entities\Comment;
use Modules\Tour\Entities\Tour;
use Illuminate\Support\Facades\Session;

class CommentController extends Controller
{
//    use Authorizable;
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $tour_id = $request->tour_id;
        $perPage = Config("settings.perpage");

        $comments = new Comment();
        if (!empty($tour_id)){
            $comments = $comments->where('tour_id', $tour_id);
        }
        if (!empty($keyword)){
            $comments = $comments->where('content', 'LIKE', "%$keyword%");
        }
        $comments = $comments->orderBy('created_at', 'desc')->orderBy('tour_id')->paginate($perPage);

        return view('tour::comments.index', compact('comments'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $tours = Tour::pluck('name','id')->all();
        $tours = \Arr::prepend($tours, __('message.please_select'), '');
        $prevUrl = \URL::previous();
        $arrange = Comment::max('arrange');
        $arrange = $arrange == 0 ? 1 : $arrange+1;
        return view('tour::comments.create', compact('tours', 'prevUrl', 'arrange'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)    {
        $this->validate($request, [
            'user_name' => 'required',
            'user_email' => 'required',
            'comment' => 'required'
        ]);
        \DB::transaction(function() use ($request){
            $requestData = $request->all();
            if ($request->hasFile('avatar')){
                $requestData['avatar'] = Comment::saveImageResize($request->file('avatar'));
            }
            Comment::create($requestData);
        });
        Session::flash('flash_message', __('tour::comments.created_success'));
        return redirect('admin/control/comments');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $comment = Comment::findOrFail($id);
        return view('tour::comments.show', compact('comment'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id, Request $request)
    {
        $comment = Comment::findOrFail($id);
        $tours = Tour::pluck('name','id')->all();
        $tours = \Arr::prepend($tours, __('message.please_select'), '');
        if ($request['hidUrl'] != null && $request['hidUrl']!='')
            $prevUrl = $request['hidUrl'];
        else
            $prevUrl = \URL::previous();
        return view('tour::comments.edit', compact('comment', 'tours', 'prevUrl'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'user_name' => 'required',
            'user_email' => 'required',
            'comment' => 'required'
        ]);
        $comment = Comment::findOrFail($id);
        $requestData = $request->all();
        \DB::transaction(function() use ($request, &$comment, $requestData){
            if (empty($requestData['active'])){
                $requestData['active'] = 0;
            }
            if ($request->hasFile('avatar')){
                Comment::deleteFile($comment->avatar);
                $requestData['avatar'] = Comment::saveImageResize($request->file('avatar'));
            }
            if ($request->get('img-hidden') == 1){
                Comment::deleteFile($comment->image);
                $requestData = \Arr::prepend($requestData, null, 'avatar');
            }
            $comment->update($requestData);
        });
        Session::flash('flash_message', __('tour::comments.updated_success'));
        return \Redirect::to($requestData['hidUrl']);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id, Request $request)
    {
        $comment = Comment::findOrFail($id);
        if (!empty($comment->avatar)){
            Comment::deleteFile($id);
        }
        $comment->delete();
        return redirect('admin/control/comments')->with('flash_message', trans('tour::comments.deleted_success'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function saveComment(Request $request)    {
        $this->validate($request, [
            'user_name' => 'required',
            'user_email' => 'required',
            'comment' => 'required'
        ]);
        $requestData = $request->all();
        Comment::create($requestData);
        return response()->json([
            'message' => 'thêm thành công'
        ]);
    }
}
