<?php
return [
    'name' => 'Tour',
	/**
	 * Cấu hình Booking cho module nếu có sử dụng
	 */
    'booking' => [
	    'active' => true,//false nếu ko sử dụng booking
	    'model' => 'Tour',//default: module name - tên bảng dịch vụ, mặc đinh lấy theo tên module
	    'column_name' => 'name',//tên trường cần hiển thị ở bảng dịch vụ, default: name
        'multiple' => false,//true: cho phép đặt nhiều dịch vụ 1 lần; false: chỉ đặt 1 dịch vu 1 lần.
        'check_quantity' => true,//true: tính theo số lượng người lớn và trẻ em; false: chỉ tính cho 1 dịch vụ
        'price' => 'getPriceBooking',
        'properties' => [
            [
                'key' => 'departure_date',
                'name' => 'Ngày khởi hành',
                'type' => 'date',//text,textarea,email,date,file,image,number
                'data' => [
                    'validate' => 'required',
                    'input_attr' => ['class' => 'form-control input-sm', 'required' => 'required'],
                    'label_attr' => ['class' => "control-label label-required"]
                ],
            ],
            [
                'key' => 'picking_up_place',
                'name' => 'Địa điểm đón',
                'type' => 'text',//text,textarea,email,date,file,image,number
                'data' => [
                    'validate' => 'required',
                    'input_attr' => ['class' => 'form-control input-sm', 'required' => 'required'],
                    'label_attr' => ['class' => "control-label label-required"]
                ],
            ],
        ]
    ],
    'link' => [
        'icon' => 'fa fa-umbrella',
        'title' => __('Quản lý Tour'),
        'child' => [
            [
                'icon' => 'fa fa-umbrella',
                'title' => __('Sản phẩm'),
                'href' => 'admin/control/tours',
                'permission' => 'ToursController@index'
            ],
            [
                'icon' => 'fa fa-umbrella',
                'title' => __('Hạng tour'),
                'href' => 'admin/control/tour-properties?type=tour_class',
                'permission' => 'TourPropertyController@index',
                'type' => 'tour_class',
                'href_current' => 'admin/control/tour-properties'
            ],
            [
                'icon' => 'fa fa-umbrella',
                'title' => __('Loại hình du lịch'),
                'href' => 'admin/control/tour-properties?type=tour_type',
                'permission' => 'TourPropertyController@index',
                'type' => 'tour_type',
                'href_current' => 'admin/control/tour-properties'
            ],
            [
                'icon' => 'fa fa-umbrella',
                'title' => __('Danh mục tour'),
                'href' => 'admin/control/tour-properties?type=tour_category',
                'permission' => 'TourPropertyController@index',
                'type' => 'tour_category',
                'href_current' => 'admin/control/tour-properties'
            ],
            [
                'icon' => 'fa fa-umbrella',
                'title' => __('Loại tour'),
                'href' => 'admin/control/tour-properties?type=tour_list',
                'permission' => 'TourPropertyController@index',
                'type' => 'tour_list',
                'href_current' => 'admin/control/tour-properties'
            ],
            [
                'icon' => 'fa fa-umbrella',
                'title' => __('Địa điểm'),
                'href' => 'admin/control/tour-properties?type=tour_place',
                'permission' => 'TourPropertyController@index',
                'type' => 'tour_place',
                'href_current' => 'admin/control/tour-properties'
            ],
            [
                'icon' => 'fa fa-umbrella',
                'title' => __('Bình luận'),
                'href' => 'admin/control/comments',
                'permission' => 'CommentController@index'
            ],
        ]
    ],
    /*'book' => [
        'icon' => 'fa fa-cart-plus',
        'title' => __('Đặt tour'),
        'href' => 'bookings/tour',
        'permission' => 'BookingController@index'
    ],*/
    'tour_status' => [
        1 => 'Mở',
        0 => 'Đóng'
    ],
    'tour_code' => ['OUT','INB'],
    'tour_categories' => [
        1 => 'Trong nước',
        2 => 'Nước ngoài'
    ]
];
