<div class="pull-right">
    {!! Form::open(['method' => 'GET', 'url' => '/admin/control/tours', 'class' => 'pull-left form-inline', 'role' => 'search', 'id' => 'search', 'style' => 'position: relative'])  !!}
{{--    <div class="form-group">--}}
{{--        <input autocomplete="off" type="text" value="{{\Request::get('country')}}" class="form-control input-sm" id="country_auto" name="country" placeholder="{{ __('Tìm kiếm quốc gia') }}">--}}
{{--        <div id="list_country"></div>--}}
{{--    </div>--}}
{{--    <div class="form-group">--}}
{{--        <input autocomplete="off" type="text" value="{{\Request::get('city')}}" class="form-control input-sm" id="city_auto" name="city" placeholder="{{ __('Tìm kiếm tỉnh, thành phố') }}">--}}
{{--        <div id="list_city"></div>--}}
{{--    </div>--}}
    <button type="button" class="btn-dropdown form-control input-sm">Lọc <i class="fa fa-plus" aria-hidden="true"></i></button>
    <div class="dropdown-menu dropdown-filter" style="display: none">
        <select name="tour_list" class="form-control input-sm select2">
            <option value="">--{{ __('tour::tours.tour_list') }}--</option>
            @foreach(\Modules\Tour\Entities\TourProperty::where('type', 'tour_list')->pluck('name', 'id') as $id => $name)
                <option {{ Request::get('tour_list') == $id ? "selected" : "" }} value="{{ $id }}"> {{ $name }}</option>
            @endforeach
        </select>
        <select name="tour_type" class="form-control input-sm select2">
            <option value="">--{{ __('tour::tours.tour_type') }}--</option>
            @foreach(\Modules\Tour\Entities\TourProperty::where('type', 'tour_type')->pluck('name', 'id') as $id => $name)
                <option {{ Request::get('tour_type') == $id ? "selected" : "" }} value="{{ $id }}"> {{ $name }}</option>
            @endforeach
        </select>
    </div>
    <select name="tour_category" class="form-control input-sm select2">
        <option value="">--{{ __('tour::tours.tour_category') }}--</option>
        @foreach(\Modules\Tour\Entities\TourProperty::where('type', 'tour_category')->pluck('name', 'id') as $id => $name)
            <option {{ Request::get('tour_category') == $id ? "selected" : "" }} value="{{ $id }}"> {{ $name }}</option>
        @endforeach
    </select>
    <select name="start_city" class="form-control input-sm select2">
        <option value="">--{{ __('tour::tours.start_city') }}--</option>
        @foreach(\Modules\Tour\Entities\TourProperty::where('type', 'tour_place')->pluck('name', 'id') as $id => $name)
            <option {{ Request::get('start_city') == $id ? "selected" : "" }} value="{{ $id }}"> {{ $name }}</option>
        @endforeach
    </select>
    <select name="tour_place" class="form-control input-sm select2" id="tour_place">
        <option value="">--{{ __('tour::tours.tour_place') }}--</option>
        @foreach(\Modules\Tour\Entities\TourProperty::where('type', 'tour_place')->pluck('name', 'id') as $id => $name)
            <option {{ Request::get('tour_place') == $id ? "selected" : "" }} value="{{ $id }}"> {{ $name }}</option>
        @endforeach
    </select>
{{--    <select name="tour_cities" class="form-control input-sm select2" id="tour_cities">--}}
{{--        <option value="">--{{ trans('tour::tour_schedules.schedule_destinations') }}--</option>--}}
{{--        @if(Request::get('tour_place'))--}}
{{--            @foreach(\Modules\Tour\Entities\TourProperty::where(['type' => 'tour_place', 'parent_id' => Request::get('tour_place')])->pluck('name', 'id') as $id => $name)--}}
{{--                <option {{ Request::get('tour_cities') == $id ? "selected" : "" }} value="{{ $id }}"> {{ $name }}</option>--}}
{{--            @endforeach--}}
{{--        @endif--}}
{{--    </select>--}}
    <div class="input-group">
        <input style="width: 200px;" type="text" value="{{\Request::get('search')}}" class="form-control input-sm" name="search" placeholder="{{ __('message.search_keyword') }}">
        <span class="input-group-btn">
            <button class="btn btn-default btn-sm" type="submit">
                <i class="fa fa-search"></i> {{ __('message.search') }}
            </button>
        </span>
    </div>
    <input type="hidden" name="record" value="{{ Request::has('record') ? Request::get('record') : config('settings.total_rec')[0] }}">
    {!! Form::close() !!}
    {{--@can('ToursController@store')
        <a href="{{ url('/admin/control/tours/create') }}" class="btn btn-success btn-sm" title="{{ __('message.new_add') }}">
            <i class="fa fa-plus" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.new_add') }}</span>
        </a>
    @endcan--}}
</div>