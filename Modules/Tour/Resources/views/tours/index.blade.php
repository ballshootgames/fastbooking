@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __('tour::tours.Tour') }}
@endsection
@section('contentheader_title')
    {{ __('tour::tours.Tour') }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li class="active">{{ __('tour::tours.Tour') }}</li>
    </ol>
@endsection
@section('main-content')
    <div id="alert"></div>
    <div class="statis">
        <ul>
            <li>Có {{ !empty($totalInActive) ? $totalInActive : '0' }} tour du lịch chưa được phê duyệt</li>
            <li>Có {{ !empty($totalActive) ? $totalActive : '0' }} tour du lịch đã được phê duyệt</li>
        </ul>
    </div>
    <div class="box box-primary">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-2">
                    @can('ToursController@store')
                        <a href="{{ url('/admin/control/tours/create') }}" class="btn btn-success btn-sm" title="{{ __('message.add') }}">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                        </a>
                    @endcan
                </div>
                <div class="col-sm-10">
                    @include('tour::tours.search')
                </div>
            </div>
        </div>
        @php($index = ($tours->currentPage()-1)*$tours->perPage())
        <div class="box-body table-responsive no-padding">
            <table id="table-copy" class="table--layout table table-condensed table-bordered">
                <thead>
                    <tr class="active">
                        <th class="text-center" style="width: 3.5%;">{{ trans('message.index') }}</th>
                        <th class="text-center" style="width: 2.5%;">
                            <input type="checkbox" name="chkAll" id="chkAll"/>
                        </th>
                        <th style="width: 11%;">@sortablelink('code', trans('tour::tours.code'))</th>
                        <th style="width: 25%;">@sortablelink('name',trans('tour::tours.name'))</th>
{{--                        <th class="text-center">{{ __('Ảnh') }}</th>--}}
                        <th class="text-center" style="width: 8%;">{{ trans('tour::tours.journey') }}</th>
                        <th class="text-center" style="width: 11%;">{{ trans('tour::tours.departure_date') }}</th>
                        <th class="text-right" style="width: 8%;">@sortablelink('price', __('Giá tour'))</th>
{{--                        <th style="min-width: 75px">{{ trans('tour::tours.limit') }}</th>--}}
                        {{--<th style="min-width: 75px;">{{ trans('tour::tours.limit_start_date') }}</th>
                        <th style="min-width: 75px;">{{ trans('tour::tours.limit_end_date') }}</th>--}}
{{--                        <th>{{ trans('tour::tours.booking') }}</th>--}}
                        <th class="text-center" style="width: 5%;">{{ __('Duyệt') }}</th>
                        <th class="text-center" style="width: 8%;">@sortablelink('created_at', __('Ngày tạo'))</th>
                        <th style="width: 10%;">{{ trans('tour::tours.update_people') }}</th>
{{--                        <th class="text-center">{{ __('Ngày cập nhật') }}</th>--}}
{{--                        <th>{{ __('Người cập nhật') }}</th>--}}
                        <th style="width: 9%;"></th>
                    </tr>
                </thead>
                <tbody>
{{--                @php($status = config('tour.tour_status'))--}}
                @foreach($tours as $item)
                    <tr>
                        <td class="text-center">{{ ++$index }}</td>
                        <td class="text-center">
                            <input type="checkbox" name="chkId" id="chkId" value="{{ $item->id }}" data-id="{{ $item->id }}"/>
                        </td>
                        <td>{{ $item->prefix_code . $item->code }}</td>
                        <td class="text-title">
                            @php($session = \App\Session::where('model_id', $item->id)->first())
                            @if(!empty($session) && Auth::id() != $session->user_id)
                                <i class="fa fa-lock text-success" aria-hidden="true"></i> <b>{{ $session->user->name . ' đang sửa' }}</b> <br/>
                            @endif
                            {{ $item->name }}
                        </td>
{{--                        <td class="text-center"><img src="{{ asset(Storage::url($item->image)) }}" alt="{{ $item->name }}" style="width: 40px; height: 40px; object-fit: cover;"></td>--}}
                        @if(!empty($item->night_number) && $item->day_number === 0.5)
                            <td class="text-center">1/2N - {{ $item->night_number }}Đ</td>
                        @elseif(empty($item->night_number) && $item->day_number === 0.5)
                            <td class="text-center">1/2N</td>
                        @elseif(empty($item->day_number) && !empty($item->night_number))
                            <td class="text-center">{{ $item->night_number }}Đ</td>
                        @else
                            <td class="text-center">{{ $item->day_number }}N - {{ $item->night_number }}Đ</td>
                        @endif
                        <td class="text-center">
                            {{ !empty($item->departure_date) ? Carbon\Carbon::parse($item->departure_date)->format(config('settings.format.date')) : trans('tour::tours.departure_type')[$item->departure_type] }}
                        </td>
                        <td class="text-right">{{ number_format($item->price) }}</td>
{{--                        <td>--}}
{{--                            @if(!empty($item->limit))--}}
{{--                                @php($peopleLimit = \Modules\Booking\Entities\Booking::getExistPeopleLimitTour($item))--}}
{{--                                @if($peopleLimit === 0)--}}
{{--                                    <span class="label label-warning"> {{ $item->limit - $peopleLimit }} / {{ $item->limit }}</span>--}}
{{--                                @else--}}
{{--                                    <span class="label label-success"> {{ $item->limit - $peopleLimit }} / {{ $item->limit }}</span>--}}
{{--                                @endif--}}
{{--                            @else--}}
{{--                                <span class="label label-default"> không</span>--}}
{{--                            @endif--}}

{{--                        </td>--}}
                        {{--<td>{{ !empty($item->limit_start_date) ? Carbon\Carbon::parse($item->limit_start_date)->format(config('settings.format.date')) : '-' }}</td>
                        <td>{{ !empty($item->limit_end_date) ? Carbon\Carbon::parse($item->limit_end_date)->format(config('settings.format.date')) : '-' }}</td>--}}
{{--                        <td style="text-align: center">{{ count($item->bookings) }}</td>--}}
                        {{--@if(!empty($item->tour_status))
                            <td>{{ $status[$item->tour_status] }}</td>
                        @else
                            <td>{{ $status[0] }}</td>
                        @endif--}}
                        {{--<td>@php($status = config('tour.tour_status')) {{ $status[$item->tour_status] }}</td>--}}
                        <td class="text-center">{!! $item->active == config('settings.active') ? '<i class="fa fa-check text-primary"></i>' : '' !!}</td>
                        <td class="text-center">{{ \Carbon\Carbon::parse($item->created_at)->format(config('settings.format.date')) }}</td>
                        <td>{{ optional($item->userUpdate)->name }}</td>
{{--                        <td class="text-center">{{ \Carbon\Carbon::parse($item->updated_at)->format(config('settings.format.date')) }}</td>--}}
{{--                        <td>{{ optional($item->userUpdate)->name }}</td>--}}
                        <td class="text-center">
                            @can('ToursController@getCopyTour')
                            {!! Form::open([
                                'method' => 'POST',
                                'url' => ['admin/control/tours/'.$item->id.'/copy'],
                                'style' => 'display:inline'
                            ]) !!}
                            {!! Form::button('<i class="fa fa-copy" aria-hidden="true"></i> ', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-primary btn-xs',
                                    'title' => __('copy')
                            )) !!}
                            {!! Form::close() !!}
                            @endcan
                            @can('ToursController@show')
                                <a href="{{ url('/tour/' . $item->slug . '.html') }}" title="{{ __('message.view') }}" target="_blank"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                            @endcan
                            @if(empty($session) || Auth::id() == $session->user_id)
                                @can('ToursController@update')
                                    <a href="{{ url('admin/control/tours/' . $item->id . '/edit') }}" title="{{ __('message.edit') }}"><button class="btn btn-warning btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                @endcan
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                    <td colspan="12" class="text-right">
                        Hiển thị
                            <select name="record" id="record">
                                @php($total_rec = config('settings.total_rec'))
                                @foreach($total_rec as $item)
                                    <option value="{{$item}}" {{ Request::get('record') == $item ? 'selected' : '' }}>{{$item}}</option>
                                @endforeach
                            </select>
                        dòng/1 trang
                    </td>
                </tfoot>
            </table>
        </div>
        <div class="box-footer">
            <div class="row">
                <div id="btn-act" class="col-sm-6 text-left">
                    @can('ToursController@destroy')
                        <a href="#" id="delItem" data-action="delItem" class="btn-act btn btn-danger btn-sm" title="{{ __('message.delete') }}">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </a>
                    @endcan
                    @can('ToursController@active')
                        <a href="#" id="activeItem" data-action="activeItem" class="btn-act btn btn-primary btn-sm" title="{{ __('message.user.active') }}">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </a>
                    @endcan
                </div>
                <div class="col-sm-6 text-right">
                    {!! $tours->appends(\Request::except('page'))->render() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts-footer')
    <script>
        $( function() {
            $('.btn-dropdown').on('click', () => {
                $('.dropdown-filter').toggle();
            });
            {{--$( "#city_auto" ).autocomplete({--}}
            {{--    paramName: 'city',--}}
            {{--    dataType: 'JSON',--}}
            {{--    serviceUrl: '{{ url('admin/control/tours/ajax/getAutoCompleteCity') }}',--}}
            {{--    transformResult: function(response) {--}}
            {{--        return {--}}
            {{--            suggestions: $.map(response, function(item) {--}}
            {{--                return { value: item.name };--}}
            {{--            })--}}
            {{--        };--}}
            {{--    }--}}
            {{--});--}}
            {{--$( "#country_auto" ).autocomplete({--}}
            {{--    paramName: 'country',--}}
            {{--    dataType: 'JSON',--}}
            {{--    serviceUrl: '{{ url('admin/control/tours/ajax/getAutoCompleteCountry') }}',--}}
            {{--    transformResult: function(response) {--}}
            {{--        return {--}}
            {{--            suggestions: $.map(response, function(item) {--}}
            {{--                return { value: item.name };--}}
            {{--            })--}}
            {{--        };--}}
            {{--    }--}}
            {{--});--}}
            {{--$('#tour_place').on('change', function(){--}}
            {{--    let idPlace = $(this).val();--}}
            {{--    $("#tour_cities").html('<option value="">{{ __('message.loading') }}</option>');--}}
            {{--    axios.get('{{ url('admin/control/tours/ajax/searchComplete?idPlace=') }}'+idPlace)--}}
            {{--        .then(function (response) {--}}
            {{--            console.log(response.data);--}}
            {{--            $('#tour_cities').html('<option value="">{{ '--'.__('tour::tour_schedules.schedule_destinations').'--' }}</option>');--}}
            {{--            $.each(response.data, function(key, item){--}}
            {{--                $('#tour_cities').append('<option value="'+ item.id +'">'+ item.name +'</option>');--}}
            {{--            });--}}
            {{--        })--}}
            {{--        .catch(function (error) {--}}
            {{--            console.log(error);--}}
            {{--        });--}}
            {{--});--}}
            $('#chkAll').on('click', function () {
                $("input:checkbox").prop('checked', $(this).prop("checked"));
            });
            $('#record').on('change', function () {
                let form = $('#search');
                let record = $(this).val();
                form.find('input[name="record"]').val(record);
                axios.get(form.attr('action')).then(function (res) {
                        form.submit();
                    }).catch(function () {
                        alert('Có lỗi xảy ra vui lòng thử lại!')
                    })
                return false;
            });
            $('#btn-act').on('click', '.btn-act', function(e){
                e.preventDefault();
                let action = $(this).data('action');
                ajaxListTour(action);
            });
            function ajaxListTour(action){
                let chkId = $("input[name='chkId']:checked");
                let actTxt = '', successAlert = '', classAlert = '';
                switch (action) {
                    case 'delItem':
                        actTxt = 'xóa';
                        successAlert = '{{trans('tours.deleted_success')}}';
                        classAlert = 'alert-danger';
                        break;
                    case 'activeItem':
                        actTxt = 'duyệt';
                        successAlert = '{{trans('tours.updated_success')}}';
                        classAlert = 'alert-success';
                        break;
                }
                if (chkId.length != 0){
                    let notificationConfirm = 'Bạn có muốn '+actTxt+' hồ sơ này không?';
                    let notification = confirm(notificationConfirm);
                    if (notification){
                        var arrId = '';
                        $("input[name='chkId']:checked").map((val,key) => {
                            arrId += key.value + ',';
                        });
                        axios.get('{{url('/admin/control/tours/ajax/')}}/'+action, {
                            params: {
                                ids: arrId
                            }
                        })
                            .then((response) => {
                                // console.log(response);
                                if (response.data.success === 'ok'){
                                    $('#alert').html('<div class="alert '+classAlert+'">' +
                                        '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                                        successAlert +
                                        ' </div>');
                                    {{--window.location = '{{ url('/admin/control/tours') }}'--}}
                                    location.reload(true);
                                }
                            })
                            .catch((error) => {
                                console.log(error);
                            })
                    }
                }else{
                    let notificationAlert = 'Vui lòng chọn hồ sơ để '+actTxt+'!';
                    alert(notificationAlert);
                }
            }
        } );
    </script>
@endsection
