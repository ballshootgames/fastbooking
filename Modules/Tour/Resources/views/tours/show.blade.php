@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __('tour::tours.Tour') }}
@endsection
@section('contentheader_title')
    {{ __('tour::tours.Tour') }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li><a href="{{ url('/admin/control/tours') }}">{{ __('tour::tours.Tour') }}</a></li>
        <li class="active">{{ __("message.detail") }}</li>
    </ol>
@endsection
@section('main-content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __("message.detail") }}</h3>
            <div class="box-tools">
                @if (strpos($prevUrl, url('/admin/control/tours') )!==false)
                    <a href="{{ $prevUrl }}" class="btn btn-warning btn-sm" title="{{ trans('message.lists') }}"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
                @else
                    <a href="{{ url('/admin/control/tours') }}" class="btn btn-success btn-sm" title="{{ trans('message.lists') }}"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
                @endif
                @can('ToursController@update')
                    {!! Form::open([
                        'method'=>'GET',
                        'url' => '/admin/control/tours/' . $tour->id . '/edit',
                        'style' => 'display:inline'
                    ]) !!}
                    <input type="hidden" id="hidUrl" name="hidUrl" value={{ $prevUrl }}>
                    {!! Form::button('<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-primary btn-sm',
                            'title' => __('message.edit'),
                            'onclick'=>'return true'
                    ))!!}
                    {!! Form::close() !!}
{{--                    <a href="{{ url('/admin/control/tours/' . $tour->id . '/edit') }}" class="btn btn-warning btn-sm" title="{{ __('message.edit') }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>--}}
                @endcan
                @can('ToursController@destroy')
                    {!! Form::open([
                        'method'=>'DELETE',
                        'url' => ['admin/control/tours', $tour->id],
                        'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-sm',
                            'title' => __('message.delete'),
                            'onclick'=>'return confirm("'.__('message.confirm_delete').'")'
                    ))!!}
                    {!! Form::close() !!}
                @endcan
            </div>
        </div>
        <div class="table-responsive">
            <table class="table-layout table-view table table-striped">
                <tbody>
                <tr>
                    <td colspan="4" class="text-danger">{{ trans('tour::tours.tour_general') }}</td>
                </tr>
                <tr>
                    <td> {{ trans('tour::tours.code') }} </td><td> {{ $tour->code }} </td>
                    <td> {{ trans('tour::tours.tour_status') }}</td><td> {{ config('tour.tour_status')[$tour->tour_status] }} </td>
                </tr>
                <tr>
                    <td> {{ trans('tour::tours.name') }} </td><td colspan="3"> {{ $tour->name }} </td>
                </tr>
                <tr>
                    <td> {{ trans('tour::tours.tour_type') }} </td><td> {{ optional(optional(optional($tour->properties)->where('type', 'tour_type'))->first())->name }} </td>
                    <td> {{ trans('tour::tours.tour_class') }}</td><td> {{ optional(optional(optional($tour->properties)->where('type', 'tour_class'))->first())->name }} </td>
                </tr>
                <tr>
                    <td> {{ trans('tour::tours.journey') }} </td>
                    @if($tour->night_number !== 0)
                        <td>{{ $tour->day_number }} ngày - {{ $tour->night_number }} đêm</td>
                    @elseif($tour->day_number === 0)
                        <td>1/2 ngày</td>
                    @elseif($tour->night_number !== 0 && $tour->day_number === 0)
                        <td>1/2 ngày - {{ $tour->night_number }} đêm</td>
                    @else
                        <td>{{ $tour->day_number }} ngày</td>
                    @endif
                    <td> {{ trans('tour::tours.departure_date') }} </td>
                    <td> {{ !empty($tour->departure_date) ? Carbon\Carbon::parse($tour->departure_date)->format(config('settings.format.date')) : trans('tour::tours.departure_type')[$tour->departure_type] }} </td>
                </tr>
                <tr>
                    <td> {{ trans('tour::tours.limit') }} </td>
                    <td colspan="3">
                        @if(!empty($tour->limit))
                            <span class="label label-success">{{ $tour->limit }}</span>
                            <i class="fa fa-arrow-right" aria-hidden="true" style="padding: 0 5px;"></i>
                            Số vé còn lại:
                            @if($existLimitPeople < 10)
                                <span class="label label-warning" style="margin-left: 4px"> {{ $existLimitPeople }}</span>
                            @else
                                <span class="label label-success" style="margin-left: 4px"> {{ $existLimitPeople }}</span>
                            @endif
                        @else
                            Không
                        @endif
                    </td>
                </tr>
                <tr>
                    <td> {{ trans('tour::tours.limit_start_date') }} </td><td> {{ !empty($tour->limit_start_date) ? Carbon\Carbon::parse($tour->limit_start_date)->format(config('settings.format.date')) : '' }} </td>
                    <td> {{ trans('tour::tours.limit_end_date') }} </td><td> {{ !empty($tour->limit_end_date) ? Carbon\Carbon::parse($tour->limit_end_date)->format(config('settings.format.date')) : '' }} </td>
                </tr>
                <tr>
                    <td> {{ trans('tour::tours.updated_at') }} </td>
                    <td colspan="3"> {{ Carbon\Carbon::parse($tour->updated_at)->format(config('settings.format.datetime')) }} </td>
                </tr>
                <tr>
                    <td> {{ trans('tour::tours.tour_note') }} </td>
                    <td colspan="3"> {!! $tour->note !!} </td>
                </tr>
                </tbody>
            </table>
            <table class="table-layout table-view table table-striped">
                <tr class="item-tour-row">
                    <td>
                        <div style="display: flex; justify-content: space-between">
                            <span class="text-left text-danger">{{ trans('tour::tours.tour_schedule') }}</span>
                            <span class="text-right showInfo" style="cursor: pointer"><i class="fa-show fa fa-plus"></i></span>
                        </div>
                    </td>
                </tr>
                <tr class="item-tour">
                    <td class="table-content">
                        <div class="item-content">
                            <table class="table-layout_child table-view table table-striped table-view__2">
                            @if(isset($tour_schedules))
                                @foreach($tour_schedules as $item)
                                        <tr>
                                            <td><span style="cursor: pointer;" onclick="showSchedule(this,'day{{$item->id}}')"><i class="fa fa-chevron-down" aria-hidden="true"></i>&nbsp;{{  trans('tour::tour_schedules.day') .' '. $item->day }}</span></td>
                                            <td>{{ !empty($item->name) ? $item->name : 'Chưa rõ' }}</td>
                                        </tr>
                                        <tr id="day{{$item->id}}" class="collapse ">
                                            <td colspan="2" style="padding: 0">
                                                <table class="table-layout_child table-view table">
                                                    <tr>
                                                        <td>{{ trans('tour::tour_schedules.content') }}</td>
                                                        <td>{!! $item->content !!}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>{{ trans('tour::tour_schedules.schedule_cities') }}</td>
                                                        <td>
                                                            @php($cities = $item->cities()->pluck('city_id')->all())
                                                            @foreach($cities as $city)
                                                                @if($cityPlace = \Modules\Tour\Entities\TourProperty::where(['id' => $city,'type' => 'tour_place', 'parent_id' => null])->first())
                                                                    {{ $cityPlace->name }}
                                                                @endif
                                                            @endforeach
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>{{ trans('tour::tour_schedules.schedule_destinations') }}</td>
                                                        <td>
                                                            @php($i = 0)
                                                            @php($count = count($cityPlaces = $item->cities()->pluck('city_id')->all()))
                                                            @foreach($cityPlaces as $destination)
                                                                @php($i++)
                                                                @php($s = $count == $i ? '' : ',')
                                                                @if($cityDestination = \Modules\Tour\Entities\TourProperty::where([ ['id', '=', $destination], ['parent_id', '<>', null]])->first())
                                                                    {{ $cityDestination->name . $s }}
                                                                @endif
                                                            @endforeach
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                @endforeach
                            @endif
                        </table>
                        </div>
                    </td>
                </tr>
            </table>
{{--            <table class="table-layout table-view table table-striped">--}}
{{--                <tr class="item-tour-row">--}}
{{--                    <td>--}}
{{--                        <div style="display: flex; justify-content: space-between">--}}
{{--                            <span class="text-left text-danger">{{ trans('tour::tours.tour_info') }}</span>--}}
{{--                            <span class="text-right showInfo" style="cursor: pointer"><i class="fa-show fa fa-plus"></i></span>--}}
{{--                        </div>--}}
{{--                    </td>--}}
{{--                </tr>--}}
{{--                <tr class="item-tour" style="display: none">--}}
{{--                    <td class="table-content" >--}}
{{--                        <div class="item-content">--}}
{{--                            <table class="table-layout_child table-view table table-striped table-view__info">--}}
{{--                            <tr>--}}
{{--                                <td>{{ trans('tour::tours.intro') }}</td>--}}
{{--                                <td>--}}
{{--                                    {!! $tour->intro !!}--}}
{{--                                </td>--}}
{{--                            </tr>--}}
{{--                            <tr>--}}
{{--                                <td>{{ trans('tour::tours.includes') }}</td>--}}
{{--                                <td>--}}
{{--                                    {!! $tour->includes !!}--}}
{{--                                </td>--}}
{{--                            </tr>--}}
{{--                            <tr>--}}
{{--                                <td>{{ trans('tour::tours.excludes') }}</td>--}}
{{--                                <td>--}}
{{--                                    {!! $tour->excludes !!}--}}
{{--                                </td>--}}
{{--                            </tr>--}}
{{--                            <tr>--}}
{{--                                <td>{{ trans('tour::tours.cancel_policy') }}</td>--}}
{{--                                <td>--}}
{{--                                    {!! $tour->cancel_policy !!}--}}
{{--                                </td>--}}
{{--                            </tr>--}}
{{--                            <tr>--}}
{{--                                <td>{{ trans('tour::tours.payment_policy') }}</td>--}}
{{--                                <td>--}}
{{--                                    {!! $tour->payment_policy !!}--}}
{{--                                </td>--}}
{{--                            </tr>--}}
{{--                        </table>--}}
{{--                        </div>--}}
{{--                    </td>--}}
{{--                </tr>--}}
{{--            </table>--}}
        </div>
    </div>
@endsection
@section('css')
    <style>
        .item-tour {
            display: none;
        }
    </style>
@endsection
@section('scripts-footer')
    <script type="text/javascript">
        function showSchedule(ob,id) {
            // $(ob).find('.fa').toggleClass('fa-chevron-down').toggleClass('fa-chevron-up');
            $('#'+id).toggleClass('in');
            $('#'+id).siblings($('#'+id)).removeClass('in');
        }
        $(function () {
            $(".showInfo").on("click",
                function() {
                    let itemRow = $(this).parents('.item-tour-row').next(".item-tour");
                    $(this).find('.fa').toggleClass('fa-plus').toggleClass('fa-minus');
                    if (!itemRow.is(":visible")) {
                        itemRow.show().find(".item-content").slideDown();
                    } else {
                        itemRow.find(".item-content").slideUp(function() {
                            if (!$(this).is(':visible')) {
                                itemRow.hide();
                            }
                        });
                    }
                });
        });
    </script>
@endsection