@section('css')
    <link href="{{ asset('plugins/dropzone/dropzone.min.css') }}">
    <style>
        #tour_info .table-layout tr td:first-child{
            width: 18%;
        }
        #tour_info .table-layout tr td:last-child{
            width: 82%;
        }
    </style>
@endsection
<div class="box-body">
    @if ($errors->any())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <p><i class="fa fa-fw fa-check"></i> {{ $error }}</p>
            @endforeach
        </div>
    @endif
    <div id="overlay" style="display: none"></div>
    <div class="fa-loading text-center" style="display: none;">
        <span class="fa fa-spinner fa-spin fa-3x text-primary" aria-hidden="true" ></span>
    </div>
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#tour_general">{{ trans('tour::tours.tour_general') }}</a></li>
        <li><a data-toggle="tab" href="#tour_gallery">Media</a></li>
        <li><a data-toggle="tab" href="#tour_linked">Tour liên kết</a></li>
        @if(isset($tour))
        <li><a data-toggle="tab" href="#tour_schedule">{{ trans('tour::tours.tour_schedule') }}</a></li>
        <li><a data-toggle="tab" href="#tour_info">{{ trans('tour::tours.tour_note') }}</a></li>
        @endif
    </ul>
    <div class="tab-content">
        <div id="tour_general" class="tab-pane fade in active">
            <input type="hidden" name="tour_id" value="{{ isset($tour) ? $tour->id : '' }}">
            <table class="table-layout table table-striped table-bordered">
                <tbody>
                <tr>
                    <td>{{ trans('tour::tours.start_city') }} <span class="label-required"></span></td>
                    <td>
                        <div class="{{ $errors->has('start_city') ? 'has-error' : '' }}">
                            <select name="start_city" id="start_city" class="form-control input-sm select2 prefix-code" required>
                                <option value="">{{ __('message.please_select') }}</option>
                                @foreach($tour_start_city as $item)
                                    <option value="{{ $item->id }}" data-code="{{  $item->code }}" {{ isset($tour) && ((int)$tour->start_city === $item->id) ? 'selected' : ''  }}>{{ $item->name }}</option>
                                @endforeach
                            </select>
                            {!! $errors->first('start_city', '<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                    <td>{{ trans('tour::tours.tour_place') }}</td>
                    <td>
                        <div class="{{ $errors->has('tour_place') ? 'has-error' : '' }}">
                            {!! Form::select('tour_property[tour_place][]', $tour_place, isset($tour_place_id) && !empty($tour_place_id) ? $tour_place_id : null, ['class' => 'form-control input-sm select2', 'multiple' => true]) !!}
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>{{ trans('tour::tours.code') }} <span class="label-required"></span></td>
                    <td>
                        <div class="{{ $errors->has('prefix_code') ? 'has-error' : '' }}" style="display: flex;">
                            @php($settings = \App\Setting::allConfigsKeyValue())
                            @if($settings['chk_code_prefix'] == config('settings.active'))
                                <select name="prefix_code" id="prefix_code" class="form-control input-sm prefix-code" style="width: 40%">
                                    @foreach($arrCodePrefix as $val)
                                        <option value="{{ $val }}" {{ isset($tour) && ($val === $prefix_code) ? 'selected' : null }}>{{ $val }}</option>
                                    @endforeach
                                </select>
                            @endif
                            @if($settings['code_place'] == config('settings.active'))
                                <input id="prefix_code_place" type="text" value="{{ isset($tour) ? $prefix_code_text : 'DKH' }}" name="prefix_code_place" class="form-control input-sm" style="width: 40%" readonly/>
                            @endif
                            {!! Form::text('code', $code, ['class' => 'form-control input-sm ', 'required' => 'required', 'readonly' => true, 'id' => 'code']) !!}
                        </div>
                        {!! $errors->first('code', '<p class="help-block">:message</p>') !!}
                    </td>
                    <td>{{ trans('tour::tours.tour_status') }}</td>
                    <td>
                        <div class="{{ $errors->has('tour_status') ? 'has-error' : '' }}">
                            {!! Form::select('tour_status', config('tour.tour_status'), null, ['class' => 'form-control input-sm']) !!}
                            {!! $errors->first('tour_status', '<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>{{ trans('tour::tours.name') }} <span class="label-required"></span></td>
                    <td colspan="3">
                        <div class="{{ $errors->has('name') ? 'has-error' : '' }}">
                            {!! Form::text('name', null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
                            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>{{ trans('message.meta_keyword') }}</td>
                    <td colspan="3">
                        <div>
                            {!! Form::textarea('meta_keyword', null, ['class' => 'form-control input-sm', 'rows' => 3]) !!}
                            {!! $errors->first('meta_keyword', '<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                </tr>
                @if(isset($tour))
                    <tr>
                        <td>{{ trans('tour::tours.slug') }}</td>
                        <td colspan="3">
                            <div class="{{ $errors->has('slug') ? 'has-error' : '' }}">
                                {!! Form::text('slug', $tour->slug, ['class' => 'form-control input-sm']) !!}
                                {!! $errors->first('slug', '<p class="help-block">:message</p>') !!}
                            </div>
                        </td>
                    </tr>
                @endif
                <tr>
                    <td>{{ trans('tour::tours.price') }} <span class="label-required"></span></td>
                    <td style="border-right-width: 0">
                        <div class="{{ $errors->has('price') ? 'has-error' : '' }}">
                            {!! Form::number('price', null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
                            {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                    <td>{{ trans('tour::tours.price_discount') }}</td>
                    <td>
                        <div class="{{ $errors->has('price_discount') ? 'has-error' : '' }}">
                            {!! Form::number('price_discount', null, ['class' => 'form-control input-sm']) !!}
                            {!! $errors->first('price_discount', '<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>{{ trans('tour::tours.price_children') }}</td>
                    <td style="border-right-width: 0">
                        <div class="{{ $errors->has('price_children') ? 'has-error' : '' }}">
                            {!! Form::number('price_children', null, ['class' => 'form-control input-sm']) !!}
                            {!! $errors->first('price_children', '<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                    <td>{{ trans('tour::tours.price_baby') }}</td>
                    <td>
                        <div class="{{ $errors->has('price_baby') ? 'has-error' : '' }}">
                            {!! Form::number('price_baby', null, ['class' => 'form-control input-sm']) !!}
                            {!! $errors->first('price_baby', '<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>{{ trans('tour::tours.tour_category') }}</td>
                    <td>
                        <div class="{{ $errors->has('tour_category') ? 'has-error' : '' }}">
                            {!! Form::select('tour_property[tour_category]', $tour_category, isset($tour_category_id) && !empty($tour_category_id) ? $tour_category_id->id : null, ['class' => 'form-control input-sm select2']) !!}
                        </div>
                    </td>
                    <td>{{ trans('tour::tours.tour_type') }}</td>
                    <td>
                        <div class="{{ $errors->has('tour_type') ? 'has-error' : '' }}">
                            {!! Form::select('tour_property[tour_type]', $tour_type, isset($tour_type_id) && !empty($tour_type_id) ? $tour_type_id->id : null, ['class' => 'form-control input-sm select2']) !!}
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>{{ trans('tour::tours.tour_list') }}</td>
                    <td>
                        <div class="{{ $errors->has('tour_list') ? 'has-error' : '' }}">
                            {!! Form::select('tour_property[tour_list][]', $tour_list, isset($tour_list_id) && !empty($tour_list_id) ? $tour_list_id : null, ['class' => 'form-control input-sm select2', 'multiple' => true]) !!}
                        </div>
                    </td>
                    <td>{{ trans('tour::tours.installment') }}</td>
                    <td>
                        {!! Form::checkbox('installment', 1, isset($tour->installment) && $tour->installment === 1 ? true : false, ['class' => 'flat-blue', 'id' => 'installment']) !!}
                    </td>
                </tr>
                <tr>
                    <td>{{ trans('tour::tours.journey') }}</td>
                    <td>
                        <div class="" style="display: inline-block;">
                            <div class="day">
                                <select class="input-sm" name="day_number" {{ isset($tour) ? 'onchange=changeDaySchedules(this)' : '' }} >
                                    @foreach(\Modules\Tour\Entities\Tour::getListDay(1) as $key => $value)
                                        <option value="{{ $value }}" {{ !empty($tour->day_number) && $tour->day_number == $value ? 'selected' : '' }}>{{ $value == "0.5" ? '1/2' : $value }}</option>
                                    @endforeach
                                </select>
                                <input type="hidden" id="tour_number" name="tour_number" value="{{ $tour->day_number ?? null }}">
                                <span>Ngày</span>
                                {!! Form::select('night_number', \Modules\Tour\Entities\Tour::getListDay(0), null, ['class' => 'input-sm']) !!}
                                <span style="padding-left: 5px">Đêm</span>
                            </div>
                        </div>
                    </td>
                    <td>{{ trans('tour::tours.departure_date') }}</td>
                    <td>
                        <div class="{{ $errors->has('departure_date') ? 'has-error' : '' }}" style="display: flex">
                            {!! Form::select('departure_type',$departureTypes,null, ['class' => 'form-control input-sm select2', 'id' => 'departure-js']) !!}
                            {!! Form::text('departure_date', !empty($tour->departure_date) ? Carbon\Carbon::parse($tour->departure_date)->format(config('settings.format.date')) : null, !empty($tour->departure_date) ? ['class' => 'form-control input-sm datepicker', 'id' => 'departure_date','placeholder' => 'dd/mm/yyyy'] : ['class' => 'form-control input-sm datepicker', 'id' => 'departure_date','placeholder' => 'dd/mm/yyyy','disabled' => 'disabled']) !!}
                            {!! $errors->first('departure_date', '<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>{{ trans('tour::suppliers.supplier') }}</td>
                    <td style="border-right-width: 0">
                        <div class="{{ $errors->has('supplier') ? 'has-error' : '' }}">
                            {!! Form::select('supplier',$suppliers,isset($tour) && !empty($tour->suppliers->first()) ? $tour->suppliers->first()->id : null, ['class' => 'form-control select2 input-sm', 'id' => 'supplier-js']) !!}
                            {!! $errors->first('supplier', '<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                    <td>
                        {{ trans('tour::tours.attach_file') }}
                    </td>
                    <td class="toggle-file-js">
                        <div class="{{ $errors->has('supplier_files') ? 'has-error' : ''}}" style="display: {{ (isset($tour) && $tour->suppliers->count() > 0) ?'block':'none' }};">
                            <div class="input-group inputfile-wrap ">
                                <input type="text" class="form-control input-sm" readonly>
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-danger btn-sm">
                                        <i class=" fa fa-upload"></i>
                                        {{ __('message.upload') }}
                                    </button>
                                    {!! Form::file('supplier_files', array_merge(['class' => 'form-control input-sm', "accept" => "file/*", 'id' => 'files'])) !!}
                                </div>
                            </div>
                            {!! $errors->first('supplier_files', '<p class="help-block">:message</p>') !!}
                            <div class="clearfix"></div>
                            <div class="imgprev-wrap fileprev-wrap" style="display:{{ isset($tour) && $tour->suppliers->count() > 0?'block':'none' }}">
                                <input type="hidden" name="filehidden"/>
                                <span class="file-preview">
                                    @if(isset($tour) && $tour->suppliers->count() > 0)
                                        @php($fileName = explode('/', optional($tour->suppliers[0]->pivot)->files))
                                        @php($fileName = $fileName[count($fileName) - 1])
                                        {{ $fileName }}
                                    @endif
                                </span>
                                <i class="fa fa-trash text-danger" style="margin-top: 7px;"></i>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>{{ trans('tour::tours.overview') }}</td>
                    <td colspan="3">
                        <div class="{{ $errors->has('departure_schedule') ? 'has-error' : ''}}">
                            {!! Form::textarea('departure_schedule', null, ['class' => 'form-control input-sm ckeditor', 'rows' => 5]) !!}
                            {!! $errors->first('departure_schedule', '<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        {{ trans('tour::tours.deposit') }}
                        <button type="button" class="btn btn-default btn-sm js-add-default">{{ __('Có sẵn') }}</button>
                        <input type="hidden" value="{{ $settings['deposit'] }}" class="js-default" />
                    </td>
                    <td class="js-add-deposit">
                        <div class="js-deposit" style="display: flex;">
                            <input type="text" name="deposit_name[]" class="form-control input-sm js-val-name-default" placeholder="{{ __('Số lần đặt cọc') }}" />
                            <input type="text" name="deposit_value[]" class="form-control input-sm js-val-default" placeholder="{{ __('Giá trị đặt cọc') }}" />
                            <button type="button" class="btn btn-xs btn-info js-btn-add">
                                <i class="fa fa-plus"></i>
                            </button>
                        </div>
                        @if(isset($tour) && !empty($tour->deposits()))
                            @foreach($tour->deposits as $item)
                                <div class="js-deposit" style="display: flex; margin-top: 5px;">
                                    <input type="text" name="deposit_name[]" class="form-control input-sm" value="{{ $item->name }}" />
                                    <input type="text" name="deposit_value[]" class="form-control input-sm" value="{{ $item->value }}" />
                                    <input type="hidden" name="deposit_id[]" class="form-control input-sm" value="{{ $item->id }}" />
                                    <button type="button" class="btn btn-xs btn-warning js-btn-remove" data-id="{{ $item->id }}">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                </div>
                            @endforeach
                        @endif
                        <input type="hidden" name="delete_deposit" id="delete_deposit"/>
                    </td>
                    <td>{{ trans('tour::tours.hot') }}</td>
                    <td>
                        {!! Form::checkbox('hot', 1, isset($tour->hot) && $tour->hot === 1 ? true : false, ['class' => 'flat-blue', 'id' => 'hot']) !!}
                    </td>
                </tr>
                <tr>
                    <td>{{ trans('tour::tours.limit') }}</td>
                    <td>
                        <div class="row">
                            <div class="col-md-1 col-xs-2">
                                {!! Form::checkbox('check_limit', 1, isset($tour->limit) && $tour->limit !== null ? true : false, ['class' => 'flat-blue', 'id' => 'check_limit']) !!}
                            </div>
                            <div class="col-md-10 col-xs-10">
                                {!! Form::number('limit', null, ['min' => '0', 'class' => 'form-control input-sm', 'disabled' => true, 'id' => 'limit', 'placeholder' => '20']) !!}
                            </div>
                        </div>
                    </td>
                    <td>{{ __('Tour khuyến mãi') }}</td>
                    <td>
                        {!! Form::checkbox('is_promotion', 1, isset($tour->is_promotion) && $tour->is_promotion === 1 ? true : false, ['class' => 'flat-blue', 'id' => 'promotion']) !!}
                    </td>
                </tr>
                <tr>
                    <td>{{ trans('tour::tours.limit_start_date_limit') }}</td>
                    <td>
                        <div class="{{ $errors->has('limit_start_date') ? 'has-error' : '' }}">
                            {!! Form::text('limit_start_date', !empty($tour->limit_start_date) ? Carbon\Carbon::parse($tour->limit_start_date)->format(config('settings.format.date')) : null, ['class' => 'form-control input-sm datepicker', 'disabled' => true, 'id' => 'limit_start_date']) !!}
                            {!! $errors->first('limit_start_date', '<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                    <td>{{ trans('tour::tours.limit_end_date_limit') }}</td>
                    <td>
                        <div class="{{ $errors->has('limit_end_date') ? 'has-error' : '' }}">
                            {!! Form::text('limit_end_date', !empty($tour->limit_end_date) ? Carbon\Carbon::parse($tour->limit_end_date)->format(config('settings.format.date')) : null, ['class' => 'form-control input-sm datepicker', 'disabled' => true, 'id' => 'limit_end_date']) !!}
                            {!! $errors->first('limit_end_date', '<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div id="tour_gallery" class="tab-pane fade">
            <table class="table-layout table table-slide table-striped table-bordered">
                <tbody>
                    <tr>
                        <td>
                            {{ trans('tour::tours.image') }}
                            <p style="margin-top: 10px; margin-bottom: 0;"><b>Chú ý:</b> Kích thước ảnh 800x600</p>
                        </td>
                        <td>
                            <div class="{{ $errors->has('image') ? 'has-error' : '' }}">
                                <div class="input-group tour-inputfile-wrap inputfile-wrap" style="width: 100%;">
                                    <input type="hidden" name="check_delete_image" id="check_delete_image">
                                    <input type="text" class="form-control input-sm" readonly>
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-danger btn-sm">
                                            <i class=" fa fa-upload"></i>
                                            {{ __('message.upload') }}
                                        </button>
                                        {!! Form::file('image', array_merge(['class' => 'form-control input-sm', "accept" => "image/*", 'id' => 'tour_image'])) !!}
                                    </div>
                                </div>
                                {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
                                <div class="clearfix"></div>
                                <div class="tour-imgprev-wrap imgprev-wrap" style="display:{{ !empty($tour->image)?'block':'none' }}">
                                    <img class="tour-img-preview" src="{{ !empty($tour->image)?asset(\Storage::url($tour->image)):'' }}" alt="{{ trans('company_information.logo') }}"/>
                                    <i class="fa fa-trash text-danger"></i>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Slide
                            <p style="margin-top: 10px; margin-bottom: 0;"><b>Chú ý:</b> Kích thước ảnh 800x600</p>
                        </td>
                        <td>
                            <div class="dropzone">
                                <div id="actions" class="">
                                    <div class="input-group">
                                        <input type="text" class="form-control input-sm" readonly>
                                        <input type="text" id="files" style="opacity: 0;position: absolute;width: 77px;">
                                        <div class="input-group-btn">
                                            <span class="btn btn-danger btn-sm fileinput-button">
                                                <i class="fa fa-upload"></i>
                                                <span>{{ __('message.upload') }}</span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                @isset($tour->galleries)
                                    <div id="previews" class="galleries">
                                        @if(!empty($tour->galleries))
                                            @foreach(\Modules\Tour\Entities\TourGallery::where('tour_id', $tour->id)->orderBy('image_order', 'ASC')->get() as $file)
                                                <div class="gallery imgprev-wrap imgprev-wrap-gallery" style="display:block">
                                                    <input type="hidden" name="files[]" value="{{ $file->image }}" required>
                                                    <img class="img-preview" src="{{ asset(\Storage::url($file->image)) }}" alt="">
                                                    <i class="fa fa-trash text-danger" onclick="return deleteFile(this,{{ $file->id }})"></i>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                @endisset
                                <div class="files galleries" id="previews">
                                    <div id="template" class="gallery imgprev-wrap imgprev-wrap-gallery">
                                        <input type="hidden" name="files[]" required>
                                        <img data-dz-thumbnail/>
                                        <div class="progress progress-striped progress-xs no-margin active" role="progressbar" aria-valuemin="0" aria-valuemax="100"
                                             aria-valuenow="0">
                                            <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                                        </div>
                                        <div>
                                            <strong class="error text-danger" data-dz-errormessage></strong>
                                        </div>
                                        <i class="fa fa-trash text-danger" ></i>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{ trans('tour::tours.attach_file') }}
                        </td>
                        <td>
                            <div id="media_dropzone" class="dropzone">
                                <div id="actions_files" class="">
                                    <div class="input-group">
                                        <input type="text" class="form-control input-sm" readonly>
                                        <input type="text" id="media_files" style="opacity: 0;position: absolute;width: 77px;">
                                        <div class="input-group-btn">
                                            <span class="btn btn-danger btn-sm media-input-button">
                                                <i class="fa fa-upload"></i>
                                                <span>{{ __('message.upload') }}</span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @isset($tour->medias)
                                @php($arrIcons = array("pdf"=>"fa fa-file-pdf-o custom", "doc"=>"fa fa-file-word-o custom", "docx"=>"fa fa-file-word-o custom", "xls"=>"fa fa-file-excel-o custom", "ppt"=>"fa fa-file-powerpoint-o custom", "pptx"=>"fa fa-file-powerpoint-o custom", "txt"=>"fa fa-file-text-o custom", "png"=>"fa fa-file-photo-o custom", "jpg"=>"fa fa-file-photo-o custom", "jpeg"=>"fa fa-file-picture-o custom", "mp4"=>"fa fa-file-video-o custom", "zip"=>"fa fa-file-zip-o custom"))
                                <div id="media-list" class="galleries">
                                @foreach($tour->medias as $file)
                                    @php($extFile = explode(".", $file->file_path.""))
                                    @php($nameFile = explode("/", $file->file_path.""))
                                    @php($extFile = $extFile[count($extFile) - 1])
                                    @php($nameFile = $nameFile[count($nameFile) - 1])
                                    @php($className=array_key_exists($extFile, $arrIcons)?$arrIcons[$extFile] : "fa fa-file")
                                    <div class="gallery imgprev-wrap imgprev-wrap-gallery">
                                        <a download href="{{ asset(Storage::url($file->file_path)) }}" style="padding-right: 5px;" title="{{ $nameFile }}"><i class="{{ $className." fa-3x" }} "></i></a>
                                        <div>
                                            <p id="file_name" class="text-title">{{ $nameFile }}</p>
                                        </div>
                                        <i class="fa fa-trash text-danger"  onclick="return deleteMedia(this,{{ $file->id }})"></i>
                                    </div>
                                @endforeach
                                </div>
                            @endisset
                            <div class="files galleries" id="media_previews">
                                <div id="media_template" class="gallery imgprev-wrap imgprev-wrap-gallery">
                                    <input type="hidden" name="media_files[]" required>
                                    <a download href="#" style="padding-right: 5px;"><i class="avatar_icon" ></i></a>
                                    <div class="progress progress-striped progress-xs no-margin active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                                        <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress>
                                        </div>
                                    </div>
                                    <div>
                                        <strong class="error text-danger" data-dz-errormessage></strong>
                                    </div>

                                    <div>
                                        <p id="file_name" class="text-title"></p>
                                    </div>
                                    <i class="fa fa-trash text-danger" ></i>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div id="tour_linked" class="tab-pane fade">
            <table class="table-layout table table-striped table-bordered">
                <tbody>
                <tr>
                    <td>{{ 'Cột phải (Cùng địa điểm)' }}</td>
                    <td colspan="3">
                        <div class="{{ $errors->has('tour_right') ? 'has-error' : '' }}">
                            {!! Form::select('tour_right[]', $tour_right, isset($tour_right_id) && !empty($tour_right_id) ? $tour_right_id : null, ['class' => 'form-control input-sm select2', 'id' => 'tour_right','multiple' => true]) !!}
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>{{ 'Liên quan' }}</td>
                    <td colspan="3">
                        <div class="{{ $errors->has('tour_related') ? 'has-error' : '' }}">
                            {!! Form::select('tour_related[]', $tour_related, isset($tour_related_id) && !empty($tour_related_id) ? $tour_related_id : null, ['class' => 'form-control input-sm select2', 'id' => 'tour_related', 'multiple' => true]) !!}
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        @if(isset($tour))
        <div id="tour_schedule" class="tab-pane fade">
            <table class="table-layout-schedule table table-striped table-bordered">
                <thead>
                    <tr>
                        <td colspan="2">{{ trans('tour::tours.description') }}</td>
                        <td colspan="3">
                            <div class="{{ $errors->has('intro') ? 'has-error' : ''}}">
                                {!! Form::textarea('intro', null, ['class' => 'form-control input-sm ckeditor', 'rows' => 5]) !!}
                                {!! $errors->first('intro', '<p class="help-block">:message</p>') !!}
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th style="width: 20px !important;"></th>
                        <th>{{ trans('tour::tour_schedules.day') }}</th>
                        <th>{{ trans('tour::tour_schedules.name') }}</th>
                        <th>{{ trans('tour::tour_schedules.schedule_service') }}</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @if(isset($tour_schedules))
                    @foreach($tour_schedules as $item)
                        <tr>
                            <td><span class="text-right showInfo" style="cursor: pointer"><i class="fa-show fa fa-plus"></i></span></td>
                            <td><span class="text-left text-danger">{{  $item->day == 0.5 ? '1/2 ' . trans('tour::tour_schedules.day') :trans('tour::tour_schedules.day') .' '. $item->day }}</span></td>
                            <td>{{ !empty($item->name) ? $item->name : 'Chưa rõ' }}</td>
                            <td>Không có dịch vụ</td>
                            {{--<td style="text-align: center"><span style="cursor: pointer" class="btn-schedule-edit btn btn-primary btn-xs" data="{{ $item->id }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></td>--}}
                            <td style="text-align: center"><a class="btn-schedule-edit btn btn-primary btn-xs" data-toggle="modal" target="#myModal" data-schedule="{{ $item->id }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                        </tr>
                        <tr>
                            <td style="display: none"></td>
                            <td class="table-content" style="display: none" colspan="4">
                                {!! $item->content !!}
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">{{ trans('tour::tour_schedules.tour_schedules') }}</h4>
                        </div>
                        <div class="modal-body"></div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="update-schedule">Cập nhật</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="tour_info" class="tab-pane fade">
            <table class="table-layout table table-striped table-bordered">
                <tbody>
                <tr>
                    <td colspan="2">
                        <div class="{{ $errors->has('note') ? 'has-error' : ''}}">
                            {!! Form::textarea('note', null, ['class' => 'form-control input-sm ckeditor', 'rows' => 5]) !!}
                            {!! $errors->first('note', '<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        @endif
    </div>
    <table class="table-active table-layout table table-striped table-bordered">
        @can('ToursController@active')
            <tr>
                <td>{{ trans('tour::tours.active') }}</td>
                <td style="border-right-width: 0; ">
                    {!! Form::checkbox('active', config('settings.active'), isset($tour->active) && $tour->active === config('settings.active') ? true : false, ['class' => 'flat-blue', 'id' => 'active']) !!}
                </td>
                <td style="border-right-width: 0; border-left-width: 0"></td>
                <td style="border-left-width: 0"></td>
            </tr>
        @endcan
    </table>
    <div class="table-footer">
        {!! Form::submit(isset($tour) ? __('message.save') : __('message.continue'), ['class' => 'btn btn-primary']) !!}
        <a href="{{ $prevUrl }}" class="btn btn-default">{{ __('message.cancel') }}</a>
        <input type="hidden" id="hidUrl" name="hidUrl" value={{ $prevUrl }}>
{{--        <a href="{{ url('/admin/control/tours') }}" class="btn btn-default">{{ __('message.cancel') }}</a>--}}
    </div>
</div>
@section('scripts-footer')
    <style>
        .table-slide tr td:first-child{
            width: 8%;
        }
        .modal-open{
            overflow: auto;
        }
        #myModal .modal-lg{
            width: 1024px;
            max-width: 100%;
        }
        .image-schedule {
            display: flex;
            flex-direction: row;
            align-items: center;
        }
        .parent-imgprev-wrap{
            min-height: 70px;
            padding: 0;
            border: 1px dashed #ccc;
            border-radius: 2px;
        }
        .imgprev-wrap{
            margin-top: 0;
             border: none;
        }
        .imgprev-wrap-gallery{
            margin-top: 10px;
            border: 1px dashed #ccc;
        }
        .imgprev-wrap img{
            width: 100px;
            height: 70px;
        }
        .bar {
            height: 18px;
            background: green;
        }
        .table-layout-schedule tr td{
            border-color: #d2d6de!important;
        }
        .table-layout-schedule tr th{
            font-weight: 500;
            border-color: #d2d6de!important;
        }
        .select2{
            width: 100% !important;
        }
        table.table-layout td label {
            font-weight: 500;
        }
        .label-required:before {
            content: none;
        }
        .label-required:after {
            content: " *";
            color: red;
        }
        .fa-loading{
            z-index: 1200;
            position: absolute;
            left: 0px;
            top: 0px;
            bottom: 0px;
            right: 0px;
            width: 100%;
            height: 100%;
        }
        .fa-loading span{
            position: absolute;
            left: 50%;
            top: 50%;
        }
        .fa-3x{
            font-size: 5em;
        }
        .left-35{
            left: 35% !important;
        }
        .fa-loading-2{
            z-index: 1200;
            position: absolute;
            left: 0px;
            top: 0px;
            bottom: 0px;
            right: 0px;
            width: 100%;
            height: 100%;
        }
        .fa-loading-2 span{
            position: absolute;
            left: 50%;
            top: 50%;
        }
        .fa-3x-2{
            font-size: 5em;
        }
        #overlay {
            position: fixed;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgba(0,0,0,0.5);
            z-index: 4;
            cursor: pointer;
        }
        .table-active{

        }
    </style>
    <script type="text/javascript" src="{{ asset('plugins/ckeditor/ckeditor.js') }}" ></script>
    <script>
        CKEDITOR.replace( 'departure_schedule', ckeditor_options);
    </script>
    @if(isset($tour))
        <script>
        CKEDITOR.replace( 'intro', ckeditor_options);
        CKEDITOR.replace( 'note', ckeditor_options);
        function changeDaySchedules(ob){
            let newDay = ob.value;
            let idTour = parseFloat({{ $tour->id }});
            let oldDay = $('#tour_number').val();
            // console.log(oldDay);
            axios.get('{{ url('admin/control/tours/ajax/updateDaySchedules') }}', {
                params: {
                    oldDay: oldDay,
                    newDay: newDay,
                    idTour: idTour
                }
            }).then((respon) => {
                $('#tour_number').attr('value', newDay);
                let html = '';
                const schedules = respon.data.data;
                // console.log(schedules);
                if (schedules.length > 0 ){
                    schedules.map((item,key) => {
                        //console.log(item);
                        let name = item.name === null ? 'Chưa rõ' : item.name;
                        let schedule_day = item.day == 0.5 ? '1/2 ngày' : '{{ trans('tour::tour_schedules.day') }} ' + item.day;
                        html += `<tr>
                                <td><span class="text-right showInfo" style="cursor: pointer"><i class="fa-show fa fa-plus"></i></span></td>
                                <td><span class="text-left text-danger">${schedule_day}</span></td>
                                <td>${name}</td>
                                <td>Không có dịch vụ</td>
                                <td style="text-align: center">
                                    <a class="btn-schedule-edit btn btn-primary btn-xs" data-toggle="modal" target="#myModal" data-schedule="${item.id}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td style="display: none"></td>
                                <td class="table-content" style="display: none" colspan="4">${item.content}</td>
                            </tr>`;
                    })
                }
                $('#tour_schedule table tbody').html(html);
            }).catch((error) => {
                console.log(error);
            })
        }
    </script>
    @endif
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="{{ asset('plugins/datepicker/datepicker3.css') }}">
    <script type="text/javascript" src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('plugins/moment.min.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('plugins/dropzone/dropzone.min.js') }}"></script>
    @if($settings['code_place'] == config('settings.active'))
    <script>
        $(function() {
            $('#start_city').change(function () {
                var prefixCodePlace = $(this).find(':selected').data('code');
                $('#prefix_code_place').val(prefixCodePlace);
            });
            $('.js-add-deposit').on('click','.js-btn-add', function () {
                let html = `
                    <div class="js-deposit" style="display: flex; margin-top: 5px;">
                        <input type="text" name="deposit_name[]" class="form-control input-sm" placeholder="Số lần đặt cọc" />
                        <input type="text" name="deposit_value[]" class="form-control input-sm" placeholder="Giá trị đặt cọc" />
                        <button type="button" class="btn btn-xs btn-warning js-btn-remove">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                `
                $('.js-add-deposit').append(html);
            });

            let element_delete = '';
            $('.js-add-deposit').on('click','.js-btn-remove', function () {
                element_delete =$(this).data('id')+';'+element_delete;
                $(this).parents('.js-deposit').remove();
            })

            $('form').submit(function(){
                $('#delete_deposit').val(element_delete);
            });

            $('.js-add-default').on('click', function () {
                let deposit = $('.js-default').val();
                $('.js-val-name-default').attr('value', 'Lần 1');
                $('.js-val-default').attr('value', deposit);
                // CKEDITOR.instances['deposit'].setData(html);
            });
        });
    </script>
    @endif
    @if(($settings['drpAscNumber'] === 'code_prefix') && ($settings['chk_code_prefix'] == config('settings.active') || $settings['code_place'] == config('settings.active')))
        <script>
            $(function() {
                $('.prefix-code').bind('change', function(){
                    let codePlace = '{{ $settings['code_place'] == config('settings.active') }}' ? $('#start_city').find(':selected').data('code') + '-' : '';
                    let codePrefix = '{{ $settings['chk_code_prefix'] == config('settings.active') }}' ? $('#prefix_code').val()+'-' : '';
                    let prefix_code = `${codePrefix + codePlace}`;
                    $.ajax({
                        url: '{{ url('admin/control/tours/ajax/getAutoCodePrefix?prefixCode=') }}'+prefix_code,
                        type: "get",
                        dataType: "json",
                        success: function (data) {
                            $('#code').val(data);
                        }
                    })
                })
            });
        </script>
    @endif
    <script type="text/javascript">
        $(function() {
            $("#tour_right").select2({ maximumSelectionLength: 3 });
            $("#tour_related").select2({ maximumSelectionLength: 8 });
            $('.datepicker').datepicker({
                autoclose: true,
                language: '{{ app()->getLocale() }}',
                format: '{{ config('settings.format.date_js') }}',
                @if(!isset($tour))
                startDate: '{{ date('d/m/Y') }}'
                @endif
            });

            $('form').submit(function (e) {
                let start_date = moment.parseZone($('#limit_start_date').val(), 'DD/MM/YYYY');
                let end_date = moment.parseZone($('#limit_end_date').val(), 'DD/MM/YYYY');
                if ($('#check_limit').is(":checked")) {
                    if (Date.parse(end_date) > Date.parse(start_date)){
                        return true
                    }else {
                        alert('Thời gian kêt thúc giới hạn không được sớm hơn hoặc bằng ngày bắt đầu!');
                        e.preventDefault();
                    }
                }else{
                    return true;
                }
            });

            if ($('#check_limit').is(":checked")){
                $("#limit").prop('disabled', false);
                $("#limit_start_date").prop('disabled', false);
                $("#limit_end_date").prop('disabled', false);
            } else{
                $("#limit").prop('disabled', true);
                $("#limit_start_date").prop('disabled', true);
                $("#limit_end_date").prop('disabled', true);
            }

            $('#check_limit').on('ifChecked', function () {
                $("#limit").prop('disabled', false);
                $("#limit_start_date").prop('disabled', false);
                $("#limit_end_date").prop('disabled', false);
                $("#limit").prop('required',true);
            });
            $('#check_limit').on('ifUnchecked', function () {
                $("#limit").prop('disabled', true);
                $("#limit_start_date").prop('disabled', true);
                $("#limit_end_date").prop('disabled', true);
                $("#limit").prop('required',false);
            });

            $('#country_id').change(function () {
                $("#start_city_id").html('<option value="">{{ __('message.loading') }}</option>');
                let country_id = $(this).val();
                $.ajax({
                    url: '{{ url('admin/control/tours/ajax/getListCities?country_id=') }}'+country_id,
                    type: "get",
                    dataType: "json",
                    success: function (data) {
                        $("#start_city_id").html('<option value="">{{ '-- '.__('tour::tours.city').' --' }}</option>');
                        $.each(data, function (key, value) {
                            $("#start_city_id").append('<option value="'+ key +'">'+ value +'</option>');
                        })
                    }
                })
            });

            $('#departure-js').change(function () {
                let departureVal = $(this).val();
                let departureDate = $('#departure_date');
                if(departureVal == 4){
                    departureDate.removeAttr('disabled');
                }else{
                    departureDate.attr('disabled','disabled');
                    departureDate.removeAttr('value');
                }
            })
        });
        $(function () {
            //Fix lỗi load modal không thể nhập các ô input ở CKEditor
            $('#myModal').on('shown.bs.modal', function() {
                $(document).off('focusin.modal');
            });
            //Show Content
            function showContent() {
                $('.showInfo').click(function () {
                    if ($(this).children().hasClass('fa-plus')) {
                        $(this).children().removeClass('fa-plus').addClass('fa-minus');
                    }else{
                        $(this).children().removeClass('fa-minus').addClass('fa-plus');
                    }
                    $(this).parents('tr').next().children().toggle();
                })
            }

            //thêm, xóa, cập nhật ảnh đại diện TourSchedule
            function imageChange(schedule_id){
                $('#image').change(function () {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    var fileUpload = $(this).get(0);
                    var files = fileUpload.files;
                    if (files.length) {
                        var data = new FormData();
                        for (var i = 0; i < files.length ; i++) {
                            data.append('file', files[i]);
                        }
                        $.ajax({
                            xhr: function () {
                                var xhr = $.ajaxSettings.xhr();
                                xhr.upload.onprogress = function (e) {
                                    //Tiến độ load
                                    console.log(Math.floor(e.loaded / e.total * 100) + '%');
                                };
                                return xhr;
                            },
                            contentType: false,
                            processData: false,
                            type: 'POST',
                            data: data,
                            url: '{{ url('admin/control/tour-schedule-upload') }}/'+schedule_id,
                            beforeSend: function(){
                                $('.fa-loading span').addClass('left-35');
                                $('.fa-loading').show();
                            },
                            success: function (response) {
                                $('.fa-loading').hide();
                                $('.fa-loading span').removeClass('left-35');
                                var preview = document.querySelector('.parent-imgprev-wrap .img-preview');
                                var file    = document.querySelector('#image').files[0];
                                var reader  = new FileReader();

                                if ( /\.(jpe?g|png|gif)$/i.test(file.name) ) {
                                    reader.addEventListener("load", function () {
                                        preview.src = reader.result;
                                        $('.parent-imgprev-wrap .imgprev-wrap').css('display','block');
                                        $('.image-schedule .inputfile-wrap').find('input[type=text]').val(file.name);
                                    }, false);

                                    if (file) {
                                        reader.readAsDataURL(file);
                                    }
                                }else{
                                    alert('Hãy đảm bảo file upload là file ảnh');
                                    document.querySelector('#image').value = '';
                                    $('.parent-imgprev-wrap .imgprev-wrap').find('input[type=text]').val('');
                                }
                            },
                            error: function (error) {
                                $('.fa-loading').hide();
                                $('.fa-loading span').removeClass('left-35');
                                alert('Đã có lỗi xảy ra, hãy thử lại và chắc chắn răng file bạn tải lên là hình ảnh');
                                console.log(error);
                            }
                        });
                    }
                });

                $('.imgprev-wrap .fa-trash').click(function () {
                    var preview = document.querySelector('img.img-preview');

                    if(confirm('{{ __('message.confirm_delete') }}')){
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: '{{ url('admin/control/tour-schedule-upload') }}/'+schedule_id,
                            type: 'POST',
                            data: '',
                            beforeSend: function(){
                                $('.fa-loading span').addClass('left-35');
                                $('.fa-loading').show();
                            },
                            success: function (response){
                                $('.fa-loading').hide();
                                $('.fa-loading span').removeClass('left-35');
                                //$('#avatar').val('').attr('required', 'required');
                                $('.imgprev-wrap').find('input[type=text]').val('');
                                preview.src = '';
                                $('.imgprev-wrap').css('display','none');
                                $('.inputfile-wrap').find('input[type=text]').val('');
                            },
                            error: function (error) {
                                alert('Đã có lỗi xảy ra, hãy thử lại!');
                                $('.fa-loading').hide();
                                $('.fa-loading span').removeClass('left-35');
                                console.log(error);
                            }
                        })
                    }
                });
            }

            //Hiển thị chi tiết chương trình Tour theo ngày
            // $('.btn-schedule-edit').click(function () {
            $('#tour_schedule').on('click', '.btn-schedule-edit', function () {
                let schedule_id = $(this).attr("data-schedule");
                $.ajax({
                    url: '{{ url('admin/control/tour-schedule') }}/'+schedule_id+'/edit',
                    type: "GET",
                    dataType: "json",
                    beforeSend: function(){
                        $('.fa-loading').show();
                        if (CKEDITOR.instances['schedule[content]']) {
                            CKEDITOR.remove(CKEDITOR.instances['schedule[content]']);
                        }
                    },
                    success: function (data) {
                        $('.fa-loading').hide();
                        let schedule = data.schedule;
                        let url_image = data.url_image;
                        let tourPlaces = data.tourPlaces;
                        var idTourPlaces = data.idTourPlaces;
                        let resultDestinations = data.resultDestinations;
                        const name = (schedule.name == null) ? "" : schedule.name ;
                        const content = (schedule.content == null) ? "" : schedule.content ;
                        const image = (schedule.image == null || schedule.image === "") ? "none" : "block";
                        const image_src = (schedule.image == null) ? "" : url_image;
                        const day = schedule.day == 0.5 ? '1/2' : schedule.day;
                        let html = '';
                        html += "<table class=\"table-layout table table-striped table-bordered\"><tbody>";
                        html += "<input type='hidden' name='schedule[id]' value='"+schedule.id+"'>";
                        html += "<tr><td>{{ trans('tour::tour_schedules.day') }}</td><td>"+day+"</td>";
                        html += "<td rowspan='2'>";
                            html += "<div class='image-schedule'>";
                                html += "<div class='col-md-6 parent-imgprev-wrap'>";
                                // html += "<div>";
                                    html += "<div class='imgprev-wrap' style='display: "+image+"'>";
                                        html += "<img class='img-preview' src='"+image_src+"' alt='{{ trans('tour::tour_schedules.image') }}'/>";
                                        html += "<i class='fa fa-trash text-danger'></i>";
                                    html += "</div>";
                                html += "</div>";
                                html += "<div style='padding-left: 20px'";
                                // html += "<div class='col-md-4' style='margin-top: 25px;'>";
                                    html += "<div class='input-group inputfile-wrap'>";
                                        html += "<div class='input-group-btn'><button type='button' class='btn btn-danger btn-sm'><i class='fa fa-upload' style='padding-right: 4px'></i>{{ __('message.upload') }}</button>";
                                            html += "<input class='form-control input-sm' accept='image/*' id='image' name='schedule[image]' type='file'>";
                                        html += "</div>";
                                    html += "</div>";
                                html += "</div>";
                                    // html+= "<div class='clearfix'></div>";
                            html += "</div>";
                        html += "</td></tr>";
                            html += "<tr><td >{{ trans('tour::tour_schedules.name') }}</td><td>";
                                html += "<input value='"+name+"' placeholder='Chưa rõ' type='text' class='form-control input-sm' name='schedule[name]'>";
                            html += "</td></tr>";
                            html += "<tr><td>{{ trans('tour::tour_schedules.content') }}</td><td colspan='2'><div class='{{ $errors->has('content') ? 'has-error' : ''}}''>";
                                html += "<textarea class='form-control input-sm ckeditor' name='schedule[content]' rows='5'>"+content+"</textarea>";
                            html += "{!! $errors->first('content', '<p class=\"help-block\">:message</p>') !!}</div></td></tr>";
                        html += "<tr><td>{{ trans('tour::tour_schedules.schedule_cities') }}</td><td colspan='2'>";
                            html += "<select id='schedule_places' class='form-control input-sm select2' name='schedule_places'>";
                                html += "<option value=''>Vui lòng chọn</option>";
                                $.each(tourPlaces, function (key, item) {
                                    let active = idTourPlaces.find(id => id === item.id) != undefined ? 'selected' : '';
                                    html += "<option value='"+item.id+"' "+active+">"+item.name+"</option>";
                                })
                            html += "</select>";
                        html += "</td></tr>";
                        html += "<tr><td>{{ trans('tour::tour_schedules.schedule_destinations') }}</td><td colspan='2'>";
                        html += "<select id='schedule_cities' class='form-control input-sm select2' multiple='multiple' name='schedule_cities[]'>";
                            $.each(resultDestinations, function (key, item) {
                                if(item != null){
                                html += "<option value='" + item.id + "' selected='selected'>" + item.name + "</option>";
                                }
                            })
                        html += "</select>";
                        html += "</td></tr>";
                        html += "</tbody></table>";

                        $('.modal-body').html(html);
                        $('#myModal').modal('show');
                        $('#schedule_places').on('change', function(){
                            let idPlace = $(this).val();
                            $("#schedule_cities").html('<option value="">{{ __('message.loading') }}</option>');
                            axios.get('{{ url('admin/control/tours/ajax/searchComplete?idPlace=') }}'+idPlace)
                                .then(function (response) {
                                    $('#schedule_cities').html('');
                                    $.each(response.data, function(key, item){
                                        $('#schedule_cities').append('<option value="'+ item.id +'">'+ item.name +'</option>');
                                    });
                                })
                                .catch(function (error) {
                                    console.log(error);
                                });
                        });
                        CKEDITOR.replace( 'schedule[content]', ckeditor_options);
                        $(".select2").select2();
                        imageChange(schedule_id);
                    },
                    error: function (error) {
                        console.log(error);
                        $('.fa-loading').hide();
                        alert('Đã có một số sự cố xảy ra, xin hãy thử lại!');
                    }
                })
            });

            //Cập nhật chương trình Tour và reload html dữ liệu
            // $('#update-schedule').click(function () {
            $('#tour_schedule').on('click', '#update-schedule', function () {
                $('.fa-loading span').addClass('left-35');
                $('.fa-loading').show();
                let id = $("[name='schedule[id]']").val();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                //Send dữ liệu textarea với ckediter khi dùng serialize()
                for ( instance in CKEDITOR.instances )
                {
                    CKEDITOR.instances[instance].updateElement();
                }

                $.ajax({
                    url: '{{ url('admin/control/tour-schedule') }}/'+id,
                    type: "POST",
                    dataType: "json",
                    data: $('form').serialize(),
                    success: function (data) {
                        let html = "";
                        // html += "<tbody>";
                            {{--html += "<tr><th style='width: 20px !important;'></th>'<th>{{ trans('tour::tour_schedules.day') }}</th>";--}}
                            {{--html += "<th>{{ trans('tour::tour_schedules.name') }}</th><th>{{ trans('tour::tour_schedules.schedule_service') }}</th><th></th></tr>";--}}
                            $.each(data, function (key, item) {
                                let name = item.name == null || item.name === "" ? "Chưa rõ" : item.name;
                                let content = item.content == null || item.content === "" ? "" : item.content;
                                let day = item.day == 0.5 ? '1/2 ngày' : '{{ trans('tour::tour_schedules.day') }} ' + item.day;
                                html += "<tr>";
                                    html += "<td><span class=\"text-right showInfo\" style=\"cursor: pointer\"><i class=\"fa-show fa fa-plus\"></i></span></td>";
                                    html += "<td><span class=\"text-left text-danger\">"+day+"</span></td>";
                                    // html += "<td>"+ item.name == null || item.name === "" ? 'Chưa rõ' : item.name +"</td>";
                                    html += "<td>"+ name +"</td>";
                                    html += "<td>Không có dịch vụ</td>";
                                    html += "<td style=\"text-align: center\"><a class=\"btn-schedule-edit btn btn-primary btn-xs\" data-toggle=\"modal\" target=\"#myModal\" data-schedule='"+item.id+"'><i class=\"fa fa-pencil-square-o\" aria-hidden=\"true\"></i></a></td>";
                                html += "</tr>";
                                html += "<tr>";
                                    html += "<td style='display: none'></td>";
                                    html += "<td class='table-content' style='display: none' colspan='4'>"+content+"</td>";
                                html += "</tr>";
                            })
                        // html += "</tbody>";

                        $('.table-layout-schedule tbody').html(html);
                        $('.fa-loading').hide();
                        $('.fa-loading span').removeClass('left-35');
                        $('#myModal').modal('hide');
                        showContent();
                    },
                    error: function (error) {
                        alert('Đã có một số sự cố xảy ra, xin hãy thử lại!');
                        console.log(error);
                        $('.fa-loading').hide();
                        $('#myModal').modal('hide');
                    }
                })
            })

            showContent();
        });
        $(function () {
            $('#tour_image').change(function () {
                var preview = document.querySelector('img.tour-img-preview');
                var file    = document.querySelector('#tour_image').files[0];
                var reader  = new FileReader();

                if ( /\.(jpe?g|png|gif)$/i.test(file.name) ) {

                    reader.addEventListener("load", function () {
                        preview.src = reader.result;
                        $('.tour-imgprev-wrap').css('display','block');
                        $('.tour-inputfile-wrap').find('input[type=text]').val(file.name);
                    }, false);

                    if (file) {
                        reader.readAsDataURL(file);
                    }
                }else{
                    document.querySelector('#tour_image').value = '';
                    $('.tour-imgprev-wrap').find('input[type=text]').val('');
                }
            });
            $('.tour-imgprev-wrap .fa-trash').click(function () {

                var preview = document.querySelector('img.tour-img-preview');

                if(confirm('{{ __('message.confirm_delete') }}')){

                    //$('#avatar').val('').attr('required', 'required');
                    $('.tour-imgprev-wrap').find('input[type=text]').val('');
                    preview.src = '';
                    $('.tour-imgprev-wrap').css('display','none');
                    $('.tour-inputfile-wrap').find('input[type=text]').val('');

                    $('#check_delete_image').val('1');
                }
            })

            $('#files').change(function () {
                let file = document.querySelector('#files').files[0];
                if (/\.(doc|docx|xlsx|xls|pdf)$/i.test(file.name)){
                    $('.fileprev-wrap').css('display','block');
                    $('.fileprev-wrap').find('input[type=hidden]').val('');
                    $('.inputfile-wrap').find('input[type=text]').val(file.name);
                    $('.file-preview').text(file.name);
                } else {
                    alert('Vui lòng upload file có đuôi DOC,DOCX,XLS,XLSX,PDF');
                    document.querySelector('#file').value = '';
                    $('.fileprev-wrap').find('input[type=hidden]').val('');
                }
            });
            $('.fileprev-wrap .fa-trash').click(function () {
                var preview = document.querySelector('span.file-preview');

                if(confirm('{{ __('message.confirm_delete') }}')){
                    $('.fileprev-wrap').find('input[type=hidden]').val(1);
                    preview = '';
                    $('.fileprev-wrap').css('display','none');
                    $('.inputfile-wrap').find('input[type=text]').val('');
                }
            });
            $('#supplier-js').on('change', function () {
                let attr = $(this).val() > 0 ? 'block' : 'none';
                $('.toggle-file-js').children().css('display', attr);
            });
        });

        function deleteFile(ob, id, folder = null) {
            if(confirm('{{ __('Bạn có muốn xóa file này không?') }}')) {
                $(ob).closest('.imgprev-wrap').addClass('deleting');
                if(id>0) {
                    axios({
                        method: 'post',
                        url: '{{ url('admin/control/delete-gallery/') }}'+'/'+id,
                    }).then(function (response) {
                        $(ob).closest('.imgprev-wrap').remove();
                    }).catch(function (error) {
                        console.log(error);
                        $(ob).closest('.imgprev-wrap').removeClass('deleting');
                        alert('Error!');
                    });
                }else{
                    axios({
                        method: 'post',
                        url: '{{ url('admin/control/tour-gallery-delete') }}',
                        data: {
                            folder: folder
                        }
                    }).then(function (respon) {
                        $(ob).closest('.imgprev-wrap').remove();
                        //checkRequiredFile();
                    })
                }
            }
            return false;
        }

        //dropzone
        $(function() {
            $("#previews,#files-list").sortable({
                items: '.gallery',
                cursor: 'move',
                opacity: 0.5,
                containment: '#previews,#files-list',
                distance: 20,
                tolerance: 'pointer',
            });
            $("#previews,#files-list").disableSelection();
        });
        Dropzone.autoDiscover = false;
        var previewNode = document.querySelector("#template");
        previewNode.id = "";
        var previewTemplate = previewNode.parentNode.innerHTML;
        previewNode.parentNode.removeChild(previewNode);

        var myDropzone = new Dropzone(document.body, { // Make the whole body a dropzone
            url: "{{ url('admin/control/tour-galleries') }}", // Set the url
            params: {
                _token: '{{ csrf_token() }}',
                tour_id: '{{ !empty($tour->id)?$tour->id:0 }}',
            },
            acceptedFiles: 'image/*',
            timeout: 20000, /*milliseconds*/
            maxThumbnailFilesize: 100,
            maxFilesize: 200, // MB
            thumbnailWidth: 80,
            thumbnailHeight: 80,
            thumbnailMethod: 'contain',
            parallelUploads: 20,
            previewTemplate: previewTemplate,
            autoProcessQueue: true,
            autoQueue: true, // Make sure the files aren't queued until manually added
            previewsContainer: "#previews", // Define the container to display the previews
            clickable: ".fileinput-button" // Define the element that should be used as click trigger to select files.
        });

        myDropzone.on("complete", function(file) {
            var obj = JSON.parse(file.xhr.response);
            file.previewElement.querySelector("img").src = obj.src;
            file.previewElement.querySelector('[name="files[]"]').value = obj.name;
            file.previewElement.querySelector(".progress").style.display = 'none';
            file.previewElement.querySelector(".fa-trash").setAttribute('onclick','return deleteFile(this,'+obj.file_id+',"'+obj.name+'")');
            $('#files').removeAttr('required');
        });

        // for media file attach
        // prevNode: media_template

        function deleteMedia(ob, id, folder = null) {
            if(confirm('{{ __('Bạn có muốn xóa file này không?') }}')) {
                $(ob).closest('.imgprev-wrap').addClass('deleting');
                if(id>0) {
                    axios({
                        method: 'post',
                        url: '{{ url('admin/control/delete-media/') }}'+'/'+id,
                    }).then(function (response) {
                        $(ob).closest('.imgprev-wrap').remove();
                    }).catch(function (error) {
                        console.log(error);
                        $(ob).closest('.imgprev-wrap').removeClass('deleting');
                        alert('Error!');
                    });
                }else{
                    axios({
                        method: 'post',
                        url: '{{ url('admin/control/tour-media-delete') }}',
                        data: {
                            folder: folder
                        }
                    }).then(function (respon) {
                        $(ob).closest('.imgprev-wrap').remove();
                        //checkRequiredFile();
                    })
                }
            }
            return false;
        }

        var icons = [
            {'type':'pdf', 'name': 'pdf'},
            {'type':'doc', 'name': 'word'},
            {'type':'docx', 'name': 'word'},
            {'type':'xls', 'name': 'excel'},
            {'type':'xlsx', 'name': 'excel'},
            {'type':'ppt', 'name': 'powerpoint'},
            {'type':'pptx', 'name': 'powerpoint'},
            {'type':'txt', 'name': 'text'},
            {'type':'png', 'name': 'image'},
            {'type':'jpg', 'name': 'image'},
            {'type':'jpeg', 'name': 'image'},
            {'type':'rar', 'name': 'archive'},
            {'type':'zip', 'name': 'zip'},
            {'type':'mp4', 'name': 'video'},
            {'type':'mp3', 'name': 'audio'},
            {'type':'php', 'name': 'code'},
            {'type':'html', 'name': 'code'},
            {'type':'sql', 'name': 'code'},
        ];

        Dropzone.autoDiscover = false;
        var mediaPreviewNode = document.querySelector("#media_template");
        mediaPreviewNode.id = "";
        var mediaPreviewTemplate = mediaPreviewNode.parentNode.innerHTML;
        mediaPreviewNode.parentNode.removeChild(mediaPreviewNode);

        var mediaDropzone = new Dropzone("#media_dropzone", {
            url: "{{ url('admin/control/tour-media') }}", // Set the url
            params: {
                _token: '{{ csrf_token() }}',
                tour_id: '{{ !empty($tour->id)?$tour->id:0 }}',
            },
            timeout: 20000, /*milliseconds*/
            maxThumbnailFilesize: 100,
            maxFilesize: 200, // MB
            thumbnailWidth: 80,
            thumbnailHeight: 80,
            thumbnailMethod: 'contain',
            parallelUploads: 20,
            dictDefaultMessage:"",
            previewTemplate: mediaPreviewTemplate,
            autoProcessQueue: true,
            autoQueue: true, // Make sure the files aren't queued until manually added
            previewsContainer: "#media_previews", // Define the container to display the previews
            clickable: ".media-input-button" // Define the element that should be used as click trigger to select files.
        });

        mediaDropzone.on("complete", function (file) {
            var obj = JSON.parse(file.xhr.response);
            var item = icons.find(({type})=>type===obj.type);
            var className = "";
            if (item==null || item==="" || item==="undefined")
                className = "fa fa-file-o fa-3x text-center";
            else
                className = "fa fa-file-"+ item.name + "-o fa-3x text-center";
            file.previewElement.querySelector(".avatar_icon").className = className;
            file.previewElement.querySelector("a").href = obj.src;
            file.previewElement.querySelector('[name="media_files[]"]').value = obj.name;
            var fName = obj.name.split("/");
            fName = (fName === undefined || fName.length === 0) ? "" : fName[fName.length - 1];
            file.previewElement.querySelector(".text-title").innerHTML = fName;
            file.previewElement.querySelector(".progress").style.display = 'none';

            file.previewElement.querySelector(".fa-trash").setAttribute('onclick','return deleteMedia(this,'+obj.file_id+',"'+obj.name+'")');
            $('#media_files').removeAttr('required');
        });
    </script>

@endsection