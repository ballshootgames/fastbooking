@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ $tour->name.": ".__('tour::tours.guides') }}
@endsection
@section('contentheader_title')
    {{ $tour->name.": ".__('tour::tours.guides') }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li><a href="{{ url('/tours') }}">{{ __('tour::tours.Tour') }}</a></li>
        <li class="active">{{ __("tours.guides") }}</li>
    </ol>
@endsection
@section('main-content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __('tour::tours.guides') }}</h3>
            <div class="box-tools">
                <a href="{{ url('/tours') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ trans('message.lists') }}</span></a>
                @can('ToursController@postGuides')
                    <button type="button" data-toggle="modal" data-target=".guide-modal" class="btn btn-success btn-sm" title="{{ __('message.new_add') }}">
                        <i class="fa fa-plus" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.new_add') }}</span>
                    </button>
                @endcan
            </div>
        </div>

        @include('tour::guides.index',['item'=>$tour, 'users' => $users, 'action' => 'tours'])

    </div>
@endsection