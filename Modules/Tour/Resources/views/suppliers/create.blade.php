@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __('tour::suppliers.supplier') }}
@endsection
@section('contentheader_title')
    {{ __('tour::suppliers.supplier') }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li><a href="{{ url('/admin/control/suppliers') }}">{{ __('tour::suppliers.supplier') }}</a></li>
        <li class="active">{{ __('message.new_add') }}</li>
    </ol>
@endsection

@section('main-content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __('message.new_add') }}</h3>
            <div class="box-tools">
                @if (strpos($prevUrl, url('/admin/control/suppliers'))!==false)
                    <a href="{{ $prevUrl }}" class="btn btn-warning btn-sm" title="{{ __('message.lists') }}">
                        <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    </a>
                @else
                    <a href="{{ url('/admin/control/suppliers') }}" class="btn btn-warning btn-sm" title="{{ __('message.lists') }}">
                        <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    </a>
                @endif
            </div>
        </div>

        {!! Form::open(['url' => '/admin/control/suppliers', 'class' => 'form-horizontal', 'files' => true]) !!}

        @include ('tour::suppliers.form')

        {!! Form::close() !!}
    </div>
@endsection