@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __('tour::suppliers.supplier') }}
@endsection
@section('contentheader_title')
    {{ __('tour::suppliers.supplier') }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li><a href="{{ url('/admin/control/suppliers') }}">{{ __('tour::suppliers.supplier') }}</a></li>
        <li class="active">{{ __("message.detail") }}</li>
    </ol>
@endsection

@section('main-content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __("message.detail") }}</h3>
            <div class="box-tools">
                @if (strpos($prevUrl, url('/admin/control/suppliers') )!==false)
                    <a href="{{ $prevUrl }}" class="btn btn-warning btn-sm" title="{{ __('message.lists') }}">
                        <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    </a>
                @else
                    <a href="{{ url('/admin/control/suppliers') }}" class="btn btn-warning btn-sm" title="{{ __('message.lists') }}">
                        <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    </a>
                @endif
                @can('SupplierController@update')
                    {!! Form::open([
                        'method'=>'GET',
                        'url' => '/admin/control/suppliers' . $supplier->id . '/edit',
                        'style' => 'display:inline'
                    ]) !!}
                    <input type="hidden" id="hidUrl" name="hidUrl" value={{ $prevUrl }}>
                    {!! Form::button('<i class="fa fa-pencil-square-o" aria-hidden="true"></i> <span class="hidden-xs">', array(
                            'type' => 'submit',
                            'class' => 'btn btn-primary btn-sm',
                            'title' => __('message.edit'),
                            'onclick'=>'return true'
                    ))!!}
                    {!! Form::close() !!}
                @endcan
                @can('SupplierController@destroy')
                    {!! Form::open([
                        'method'=>'DELETE',
                        'url' => ['/admin/control/suppliers', $supplier->id],
                        'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-sm',
                            'title' => __('message.delete'),
                            'onclick'=>'return confirm("'.__('message.confirm_delete').'")'
                    ))!!}
                    {!! Form::close() !!}
                @endcan
            </div>
        </div>
        <div class="box-body table-responsive">
            <table class="table-layout table table-striped table-bordered">
                <tbody>
                <tr>
                    <td> {{ trans('tour::suppliers.name') }} </td>
                    <td> {{ $supplier->name }} </td>
                </tr>
                <tr>
                    <td> {{ trans('tour::suppliers.phone') }} </td>
                    <td>{{ $supplier->phone }}</td>
                </tr>
                <tr>
                    <td> {{ trans('tour::suppliers.email') }} </td>
                    <td>{{ $supplier->email }}</td>
                </tr>
                <tr>
                    <td> {{ trans('tour::suppliers.address') }} </td>
                    <td>{{ $supplier->address }}</td>
                </tr>
                <tr>
                    <td> {{ trans('message.created_at') }} </td>
                    <td> {{ \Carbon\Carbon::parse($supplier->created_at)->format(config('settings.format.datetime')) }} </td>
                </tr>
                <tr>
                    <td> {{ trans('message.updated_at') }} </td>
                    <td> {{ \Carbon\Carbon::parse($supplier->updated_at)->format(config('settings.format.datetime')) }} </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
@section('css')
    <style>
        .table-layout tr td:first-child{
            width: 18%;
        }
        .table-layout tr td:last-child{
            width: 82%;
        }
    </style>
@endsection