@section('css')
    <style>
        .table-layout tr td:first-child{
            width: 18%;
        }
        .table-layout tr td:last-child{
            width: 82%;
        }
    </style>
@endsection
<div class="box-body">
    @if ($errors->any())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <p><i class="fa fa-fw fa-check"></i> {{ $error }}</p>
            @endforeach
        </div>
    @endif
    <input type="hidden" name="type" value="{{ isset($type) ? $type : '' }}">
    <table class="table-layout table table-striped table-bordered">
        <tbody>
        <tr>
            <td>{{ trans('tour::tour_properties.code') }} <span class="label-required"></span></td>
            <td>
                <div>
                    {!! Form::text('code', null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
                    {!! $errors->first('code', '<p class="help-block">:message</p>') !!}
                </div>
            </td>
        </tr>
        @if ($type=='tour_place')
            <tr>
                <td>{{ trans('tour::tour_properties.tour_properties.tour_place') }} </td>
                <td>
                    <div>
                        {!! Form::select('parent_id', $parentList, null, ['class' => 'form-control input-sm select2', 'id' => 'parent_id']) !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>{{ trans('tour::tour_properties.tour_properties.tour_category') }} <span class="label-required"></span></td>
                <td>
                    <div>
                        {!! Form::select('type_id', $tourCategories, null, ['class' => 'form-control input-sm select2', 'id' => 'type_id', 'required' => 'required']) !!}
                    </div>
                </td>
            </tr>
        @endif
        <tr>
            <td>{{ trans('tour::tour_properties.name') }} <span class="label-required"></span></td>
            <td>
                <div>
                    {!! Form::text('name', null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
            </td>
        </tr>
        @if(isset($tourProperty))
            <tr>
                <td>{{ trans('tour::tour_properties.slug') }}</td>
                <td>
                    <div>
                        {!! Form::text('slug', null, ['class' => 'form-control input-sm']) !!}
                        {!! $errors->first('slug', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
        @endif
        <tr>
            <td>{{ trans('tour::tour_properties.image') }}</td>
            <td>
                <div class="{{ $errors->has('image') ? ' has-error' : ''}}">
                    <div class="input-group inputfile-wrap ">
                        <input type="text" class="form-control input-sm" readonly>
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-danger btn-sm">
                                <i class=" fa fa-upload"></i>
                                {{ __('message.upload') }}
                            </button>
                            {!! Form::file('image', array_merge(['class' => 'form-control input-sm', "accept" => "image/*", 'id' => 'image'])) !!}
                        </div>
                    </div>
                    {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
                    <div class="clearfix"></div>
                    <div class="imgprev-wrap" style="display:{{ !empty($tourProperty->image)?'block':'none' }}">
                        <img class="img-preview" src="{{ !empty($tourProperty) ? asset(\Storage::url($tourProperty->image)) : '' }}" alt="{{ !empty($tourProperty) ? $tourProperty->name : '' }}"/>
                        <i class="fa fa-trash text-danger"></i>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>{{ trans('tour::tour_properties.description') }}</td>
            <td>
                <div>
                    {!! Form::textarea('description', null, ['class' => 'form-control input-sm']) !!}
                    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                </div>
            </td>
        </tr>
        <tr>
            <td>{{ trans('tour::tour_properties.show_home') }}</td>
            <td>
                <div>
                    {!! Form::select('show_home', $showHomes, null, ['class' => 'form-control input-sm select2']) !!}
                    {!! $errors->first('show_home', '<p class="help-block">:message</p>') !!}
                </div>
            </td>
        </tr>
        <tr>
            <td>{{ trans('message.index') }}</td>
            <td>
                <div>
                    {!! Form::number('prop_order', null, ['class' => 'form-control input-sm']) !!}
                </div>
            </td>
        </tr>
        @if (isset($type) && $type=='tour_place')
            <tr>
                <td>{{ trans('tour::tour_properties.top') }}</td>
                <td>
                    <div>
                        {!! Form::checkbox('prop_top', 1, (isset($tourProperty) && $tourProperty->prop_top===1)?true:false, ['class' => 'flat-blue', 'id' => 'prop_top']) !!}
                    </div>
                </td>
            </tr>
        @endif
        <tr>
            <td>{{ trans('message.meta_keyword') }}</td>
            <td>
                <div>
                    {!! Form::textarea('meta_keyword', null, ['class' => 'form-control input-sm', 'rows' => 3]) !!}
                    {!! $errors->first('meta_keyword', '<p class="help-block">:message</p>') !!}
                </div>
            </td>
        </tr>
        <tr>
            <td>{{ trans('tour::tour_properties.active') }}</td>
            <td>
                <div>
                    {!! Form::checkbox('active', 1, (isset($tourProperty) && $tourProperty->active===1)?true:false, ['class' => 'flat-blue', 'id' => 'active']) !!}
                </div>
            </td>
        </tr>
        </tbody>
    </table>
    <div style="margin-top: 10px;">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : __('message.save'), ['class' => 'btn btn-primary']) !!}
        <a href="{{ $prevUrl }}" class="btn btn-default">{{ __('message.cancel') }}</a>
        <input type="hidden" id="hidUrl" name="hidUrl" value={{ $prevUrl }}>
{{--        <a href="{{ url('/admin/control/tour-properties?type='.$type) }}" class="btn btn-default">{{ __('message.close') }}</a>--}}
    </div>
</div>
@section('scripts-footer')
    <script type="text/javascript">
        $(function(){
            $('#image').change(function () {
                let preview = document.querySelector('img.img-preview');
                let file    = document.querySelector('#image').files[0];
                let reader  = new FileReader();

                if ( /\.(jpe?g|png|gif)$/i.test(file.name) ) {
                    reader.addEventListener("load", function () {
                        preview.src = reader.result;
                        $('.imgprev-wrap').css('display','block');
                        $('.inputfile-wrap').find('input[type=text]').val(file.name);
                    }, false);

                    if (file) {
                        reader.readAsDataURL(file);
                    }
                }else{
                    document.querySelector('#image').value = '';
                }
            });

            $('.imgprev-wrap .fa-trash').click(function () {
                let preview = document.querySelector('img.img-preview');

                if(confirm('{{ __('message.confirm_delete') }}')){
                    preview.src = '';
                    $('.imgprev-wrap').css('display','none');
                    $('.inputfile-wrap').find('input[type=file]').attr('required','required');
                    $('.inputfile-wrap').find('input[type=text]').val('');
                }
            })
        });
    </script>
@endsection