@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __('tour::tour_properties.tour_properties.'.$type) }}
@endsection
@section('contentheader_title')
    {{ __('tour::tour_properties.tour_properties.'.$type) }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li><a href="{{ url('/admin/control/tour-properties?type='.$type) }}">{{ __('tour::tour_properties.tour_properties.'.$type) }}</a></li>
        <li class="active">{{ __("message.detail") }}</li>
    </ol>
@endsection
@section('main-content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __("message.detail") }}</h3>
            <div class="box-tools">
                @if (strpos($prevUrl, url('/admin/control/tour-properties?') )!==false)
                    <a href="{{ $prevUrl }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.lists') }}</span></a>
                @else
                    <a href="{{ url('/admin/control/tour-properties?type='.$type) }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.lists') }}</span></a>
                @endif
                @can('TourPropertyController@update')
                {!! Form::open([
                    'method'=>'GET',
                    'url' => '/admin/control/tour-properties/' . $tourProperty->id . '/edit?type='.$type,
                    'style' => 'display:inline'
                ]) !!}
                <input type="hidden" id="hidUrl" name="hidUrl" value={{ $prevUrl }}>
                {!! Form::button('<i class="fa fa-pencil-square-o" aria-hidden="true"></i> <span class="hidden-xs">'.__('message.edit').'</span>', array(
                        'type' => 'submit',
                        'class' => 'btn btn-primary btn-sm',
                        'title' => __('message.edit'),
                        'onclick'=>'return true'
                ))!!}
                {!! Form::close() !!}
                @endcan
                @can('TourPropertyController@destroy')
                {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['admin/control/tour-properties', $tourProperty->id],
                    'style' => 'display:inline'
                ]) !!}
                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> <span class="hidden-xs">'.__('message.delete').'</span>', array(
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-sm',
                        'title' => __('message.delete'),
                        'onclick'=>'return confirm("'.__('message.confirm_delete').'")'
                ))!!}
                {!! Form::close() !!}
                @endcan
            </div>
        </div>
        <div class="box-body table-responsive">
            <table class="table-layout table table-striped table-bordered">
                <tbody>
                    <tr>
                        <td> {{ trans('message.index') }} </td>
                        <td> {{ $tourProperty->prop_order }} </td>
                    </tr>
                    <tr>
                        <td> {{ trans('tour::tour_properties.code') }} </td>
                        <td> {{ $tourProperty->code }} </td>
                    </tr>
                    <tr>
                        <td> {{ trans('tour::tour_properties.name') }} </td>
                        <td> {{ $tourProperty->name }} </td>
                    </tr>
                    <tr>
                        <td> {{ trans('tour::tour_properties.slug') }} </td>
                        <td> {{ $tourProperty->slug }} </td>
                    </tr>
                    <tr>
                        <td> {{ trans('tour::tour_properties.description') }} </td>
                        <td> {{ $tourProperty->description }} </td>
                    </tr>
                    <tr>
                        <td> {{ trans('message.created_at') }} </td>
                        <td> {{ $tourProperty->created_at }} </td>
                    </tr>
                    <tr>
                        <td> {{ trans('message.creator') }} </td>
                        <td> {{ optional($tourProperty->user)->name }}</td>
                    </tr>
                    @if ($tourProperty->type==='tour_place' || $tourProperty->type==='tour_category')
                        <tr>
                            <td> {{ trans('tour::tour_properties.tour_properties.tour_category') }} </td>
                            <td>{{ optional($tourProperty->tour_type)->name }}</td>
{{--                            <td> {!!  $tourProperty->type_id===1? '<i class="fas fa-check text-primary"></i>' : '<i class="fas fa-check"></i>' !!} </td>--}}
                        </tr>
                    @endif
                    @if ($tourProperty->type==='tour_place')
                        <tr>
                            <td> {{ trans('tour::tour_properties.top') }} </td>
                            <td> {!!  $tourProperty->prop_top===1? '<i class="fas fa-check text-primary"></i>' : '<i class="fas fa-check"></i>' !!} </td>
                        </tr>
                    @endif
                    <tr>
                        <td> {{ trans('tour::tour_properties.active') }} </td>
                        <td> {!! $tourProperty->active===1?'<i class="fas fa-check text-primary"></i>' : '<i class="fas fa-check"></i>' !!} </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
@section('css')
    <style>
        .table-layout tr td:first-child{
            width: 18%;
        }
        .table-layout tr td:last-child{
            width: 82%;
        }
    </style>
@endsection