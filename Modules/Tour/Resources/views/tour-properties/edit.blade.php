@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __('tour::tour_properties.tour_properties.'.$type) }}
@endsection
@section('contentheader_title')
    {{ __('tour::tour_properties.tour_properties.'.$type) }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li><a href="{{ url('/admin/control/tour-properties?type='.$type) }}">{{ __('tour::tour_properties.tour_properties.'.$type) }}</a></li>
        <li class="active">{{ __('message.edit_title') }}</li>
    </ol>
@endsection

@section('main-content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __('message.edit_title') }}</h3>
            <div class="box-tools">
                @if (strpos($prevUrl, url('/admin/control/tour-properties?'))!==false)
                    <a href="{{ $prevUrl }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.lists') }}</span></a>
                @else
                    <a href="{{ url('/admin/control/tour-properties?type='.$type) }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.lists') }}</span></a>
                @endif
            </div>
        </div>

        {!! Form::model($tourProperty, [
            'method' => 'PATCH',
            'url' => ['/admin/control/tour-properties', $tourProperty->id],
            'class' => 'form-horizontal',
            'files' => true
        ]) !!}

        @include ('tour::tour-properties.form', ['submitButtonText' => __('message.update')])

        {!! Form::close() !!}
    </div>
@endsection