<div class="box-body">
    @if ($errors->any())
	    <div class="alert alert-danger">
	        @foreach ($errors->all() as $error)
	            <p><i class="fa fa-fw fa-check"></i> {{ $error }}</p>
	        @endforeach
	    </div>
	@endif
    <table class="table-layout table table-striped table-bordered">
        <tbody>
        <tr>
            <td>{{ trans('tour::comments.user_name') }} <span class="label-required"></span></td>
            <td>
                <div>
                    {!! Form::text('user_name', null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
                    {!! $errors->first('user_name', '<p class="help-block">:message</p>') !!}
                </div>
            </td>
        </tr>
        <tr>
            <td>{{ trans('tour::comments.user_email') }} <span class="label-required"></span></td>
            <td>
                <div>
                    {!! Form::text('user_email', null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
                    {!! $errors->first('user_email', '<p class="help-block">:message</p>') !!}
                </div>
            </td>
        </tr>
        <tr>
            <td>{{ trans('tour::comments.tour_id') }}</td>
            <td>
                <div>
                    {!! Form::select('tour_id', $tours, null, ['class' => 'form-control input-sm select2', 'id' => 'tours']) !!}
                    {!! $errors->first('tour_id', '<p class="help-block">:message</p>') !!}
                </div>
            </td>
        </tr>
        <tr>
            <td>{{ trans('tour::comments.avatar') }}</td>
            <td>
                <div>
                    <div class="input-group inputfile-wrap ">
                        <input type="text" class="form-control input-sm" readonly>
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-danger btn-sm">
                                <i class=" fa fa-upload"></i>
                                {{ __('message.upload') }}
                            </button>
                            {!! Form::file('avatar', array_merge(['id'=>'image', 'class' => 'form-control input-sm', "accept" => "image/*"])) !!}
                        </div>
                        {!! $errors->first('avatar', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="clearfix"></div>
                    <div class="imgprev-wrap" style="display:{{ !empty($comment->avatar)?'block':'none' }}">
                        <input type="hidden" value="" name="img-hidden"/>
                        <img class="img-preview" src="{{ !empty($comment->avatar)?asset(\Storage::url($comment->avatar)):'' }}" alt="{{ trans('tour::comments.avatar') }}"/>
                        <i class="fa fa-trash text-danger"></i>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>{{ trans('tour::comments.comment') }}</td>
            <td>
                <div>
                    {!! Form::textarea('comment', null, ['class' => 'form-control', 'rows' => 3]) !!}
                    {!! $errors->first('comment', '<p class="help-block">:message</p>') !!}
                </div>
            </td>
        </tr>
        <tr>
            <td>{{ trans('message.arrange') }}</td>
            <td>
                <div>
                    {!! Form::number('arrange', isset($comment) ? $comment->arrange : $arrange, ['class' => 'form-control input-sm']) !!}
                    {!! $errors->first('arrange', '<p class="help-block">:message</p>') !!}
                </div>
            </td>
        </tr>
        <tr>
            <td>{{ trans('message.active') }}</td>
            <td>
                <div>
                    {!! Form::checkbox('active', 1, (isset($comment) && $comment->active===1)?true:false, ['class' => 'flat-blue', 'id' => 'active']) !!}
                </div>
            </td>
        </tr>
        </tbody>
    </table>
    <div style="margin-top: 10px;">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : __('message.save'), ['class' => 'btn btn-primary']) !!}
        <a href="{{ $prevUrl }}" class="btn btn-default">{{ __('message.cancel') }}</a>
        <input type="hidden" id="hidUrl" name="hidUrl" value={{ $prevUrl }}>
    </div>
</div>
@section('scripts-footer')
    <script type="text/javascript">
        $(function(){
            $("#tours").select2({ width: '100%' });
            $('#image').change(function () {
                var preview = document.querySelector('img.img-preview');
                var file    = document.querySelector('#image').files[0];
                var reader  = new FileReader();
                console.log(preview + '-' + file + '-' + reader);

                if ( /\.(jpe?g|png|gif)$/i.test(file.name) ) {

                    reader.addEventListener("load", function () {
                        preview.src = reader.result;
                        $('.imgprev-wrap').css('display','block');
                        $('.inputfile-wrap').find('input[type=text]').val(file.name);
                    }, false);

                    if (file) {
                        reader.readAsDataURL(file);
                    }
                }else{
                    document.querySelector('#image').value = '';
                    $('.imgprev-wrap').find('input[type=hidden]').val('');
                }
            });

            $('.imgprev-wrap .fa-trash').click(function () {
                var preview = document.querySelector('img.img-preview');

                if(confirm('{{ __('message.confirm_delete') }}')){
                    //$('#avatar').val('').attr('required', 'required');
                    $('.imgprev-wrap').find('input[type=hidden]').val(1);
                    preview.src = '';
                    $('.imgprev-wrap').css('display','none');
                    $('.inputfile-wrap').find('input[type=text]').val('');
                }
            })
        });
    </script>
@endsection
