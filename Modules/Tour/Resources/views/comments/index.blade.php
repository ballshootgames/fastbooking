@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ trans('tour::comments.comment') }}
@endsection
@section('contentheader_title')
    {{ trans('tour::comments.comment') }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li class="active">{{ trans('tour::comments.comment') }}</li>
    </ol>
@endsection
@section('main-content')
    <div class="box">
        <div class="box-header">
            <div class="box-title">
                <a href="{{ url('admin/control/comments/create') }}" class="btn btn-success btn-sm">
                    <i class="fa fa-plus"></i>
                </a>
            </div>
            <div class="box-tools">
                {!! Form::open(['method' => 'GET', 'url' => '/admin/control/comments', 'class' => 'pull-left', 'role' => 'search', 'style="display: flex"'])  !!}
                <div class="input-group">
                    <input type="text" value="{{ \Request::get('search') }}" class="form-control input-sm" name="search" placeholder="{{ __('message.search_keyword') }}" style="width: 150px;">
                    <span class="input-group-btn">
                        <button class="btn btn-default btn-sm" type="submit">
                            <i class="fa fa-search"></i>  {{ __('message.search') }}
                        </button>
                    </span>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        @php($index = ($comments->currentPage()-1)*$comments->perPage())
        <div class="box-body table-responsive no-padding">
            <table class="table table-striped">
                <tbody>
                    <tr>
                        <th class="text-center">{{ trans('message.index') }}</th>
                        <th style="min-width: 110px;">{{ trans('tour::comments.user_name') }}</th>
                        <th style="min-width: 110px;">{{ trans('tour::comments.user_email') }}</th>
                        <th style="min-width: 110px;">{{ trans('tour::comments.tour_id') }}</th>
                        <th>{{ trans('tour::comments.comment') }}</th>
                        <th>{{ trans('message.active') }}</th>
                        <th class="text-center" style="min-width: 120px;">{{ trans('tour::comments.created_at') }}</th>
                        <th style="min-width: 120px;"></th>
                    </tr>
                @foreach($comments as $item)
                    <tr>
                        <td class="text-center">{{ ++$index }}</td>
                        <td>{{ $item->user_name }}</td>
                        <td>{{ $item->user_email }}</td>
                        <td><a href="{{ url('theme/eagle/tour/'.optional($item->tour)->slug.'.html') }}">{{ optional($item->tour)->name }}</a></td>
                        <td>{!! $item->comment !!}</td>
                        <td class="text-center">{!! $item->active == config('settings.active') ? '<i class="fa fa-check text-primary"></i>' : '' !!}</td>
                        <td class="text-center">{{ Carbon\Carbon::parse($item->created_at)->format(config('settings.format.date')) }}</td>
                        <td>
                            {!! Form::open(['method' => 'GET', 'url' => '/admin/control/comments/' . $item->id, 'style' => 'display:inline'])  !!}
                                {!! Form::button('<i class="fa fa-eye" aria-hidden="true"></i> ', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-info btn-xs',
                                    'title' => __('message.view')
                                )) !!}
                            {!! Form::close() !!}
                            <a href="{{ url('admin/control/comments/' . $item->id . '/edit') }}">
                                <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                            </a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['/admin/control/comments', $item->id],
                                'style' => 'display:inline'
                            ]) !!}
                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> ', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => __('message.delete'),
                                        'onclick'=>'return confirm("'.__('message.confirm_delete').'")'
                                )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="box-footer clearfix">
                {!! $comments->appends(\Request::except('page'))->render() !!}
            </div>
        </div>
    </div>

@endsection
