@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ trans('tour::comments.comment') }}
@endsection
@section('contentheader_title')
    {{ trans('tour::comments.comment') }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li><a href="{{ url('/admin/control/comments') }}">{{ trans('tour::comments.comment') }}</a></li>
        <li class="active">{{ __('message.edit_title') }}</li>
    </ol>
@endsection

@section('main-content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __('message.edit_title') }}</h3>
            <div class="box-tools">
                @if (strpos($prevUrl, url('/news'))!==false)
                    <a href="{{ $prevUrl }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.lists') }}</span></a>
                @else
                    <a href="{{ url('/admin/control/comments') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.lists') }}</span></a>
                @endif
            </div>
        </div>

        {!! Form::model($comment, [
            'method' => 'PATCH',
            'url' => ['/admin/control/comments', $comment->id],
            'class' => 'form-horizontal',
            'files' => true
        ]) !!}

        @include ('tour::comments.form', ['formMode' => 'edit'])

        {!! Form::close() !!}
    </div>
@endsection
