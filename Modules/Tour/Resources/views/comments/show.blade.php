@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ trans('comments.comment') }}
@endsection
@section('contentheader_title')
    {{ trans('comments.comment') }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li><a href="{{ url('/admin/control/comments?id_post='.$id_post) }}">{{ trans('comments.comment') }}</a></li>
        <li class="active">{{ __("message.detail") }}</li>
    </ol>
@endsection
@section('main-content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">{{ __("message.detail") }}</h3>
            <div class="box-tools">
                <a href="{{ url('/admin/control/comments?id_post=' . $id_post) }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ trans('message.lists') }}</span></a>
                {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['admin/control/comments', $comment->id],
                    'style' => 'display:inline'
                ]) !!}
                    {!! Form::hidden('id_post', $id_post) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> <span class="hidden-xs">'.__('message.delete').'</span>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-sm',
                            'title' => __('message.delete'),
                            'onclick'=>'return confirm("'.__('message.confirm_delete').'")'
                    ))!!}
                {!! Form::close() !!}
            </div>
        </div>
        <div class="box-body table-responsive no-padding">
            <table class="table table-striped">
                <tbody>
                    <tr>
                        <th class="col-md-2">ID</th>
                        <td>{{ $comment->id }}</td>
                    </tr>
                    <tr>
                        <th class="col-md-2"> {{ trans('comments.user_id') }} </th>
                        <td> {{ optional($comment->user)->name }} </td>
                    </tr>
                    <tr>
                        <th class="col-md-2"> {{ trans('comments.content') }} </th>
                        <td> {!! $comment->content !!} </td>
                    </tr>
                    <tr>
                        <th class="col-md-2"> {{ trans('comments.created_at') }} </th>
                        <td> {{ Carbon\Carbon::parse($comment->created_at)->format(config('settings.format.date')) }} </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
