<?php

return [
    'tour_schedules' => 'Chương trình tour',
    'day' => 'Ngày',
    'name' => 'Tên hành trình',
    'content' => 'Nội dung',
    'image' => 'Ảnh đại diện',
    'schedule_cities' => 'Điểm đến',
    'updated_at' => 'Ngày cập nhật',
    'created_success' => 'Tour đã được thêm!',
    'updated_success' => 'Tour đã được cập nhật!',
    'deleted_success' => 'Tour đã được xóa!',
];
