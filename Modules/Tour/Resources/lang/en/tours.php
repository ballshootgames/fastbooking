<?php

return [
    'Tour' => 'Tour',
    'code' => 'Mã Tour',
    'name' => 'Name',
    'journey' => 'Journey',
    'price' => 'Price',
    'price_agent' => 'Price Agent',
    'creator_id' => 'Creator Id',
    'description' => 'Description',
    'transport' => 'Transport',
    'updated_at' => 'Postdate',
    'created_success' => 'Tour added!',
    'updated_success' => 'Tour updated!',
    'deleted_success' => 'Tour deleted!',
];
