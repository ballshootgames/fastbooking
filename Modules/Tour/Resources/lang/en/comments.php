<?php

return [
    'comment' => 'Bình luận',
    'news_id' => 'Tin tức',
    'news' => 'Danh sách bài viết',
    'user_id' => 'Người tạo',
    'content' => 'Nội dung',
    'creator' => 'Người tạo',
    'updated_at' => 'Ngày cập nhật',
    'created_at' => 'Ngày cập nhật',
    'created_success' => 'Bình luận đã được thêm!',
    'updated_success' => 'Bình luận đã được cập nhật!',
    'deleted_success' => 'Bình luận đã được xóa!',
];
