<?php

return [
    'comment' => 'Bình luận',
    'avatar' => 'Ảnh đại diện',
    'tour_id' => 'Tour',
    'user_name' => 'Tên người đăng',
    'user_email' => 'Email người đăng',
    'tours' => 'Danh sách tour',
    'updated_at' => 'Ngày cập nhật',
    'created_at' => 'Ngày cập nhật',
    'created_success' => 'Bình luận đã được thêm!',
    'updated_success' => 'Bình luận đã được cập nhật!',
    'deleted_success' => 'Bình luận đã được xóa!',
];
