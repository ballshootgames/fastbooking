<?php

return [
    'supplier' => 'Nhà cung cấp',
    'name' => 'Tên',
    'phone' => 'Điện thoại',
    'address' => 'Địa chỉ',
    'email' => 'Email',
    'index' => 'Số thứ tự',
    'updated_at' => 'Ngày cập nhật',
    'created_success' => 'Nhà cung cấp đã được thêm!',
    'updated_success' => 'Nhà cung cấp đã được cập nhật!',
    'deleted_success' => 'Nhà cung cấp đã được xóa!',
];