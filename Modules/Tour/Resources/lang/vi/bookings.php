<?php

return [
    'tour' => 'Tour',
    'departure_date' => 'Ngày khởi hành',
    'picking_up_place' => 'Địa điểm đón',
    'file_word' => 'Tài liệu',
    'image_file' => 'Hình ảnh'
];