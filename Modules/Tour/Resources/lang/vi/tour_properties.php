<?php

return [
    'tour_properties' => [
        'tour_class' => 'Hạng Tour',
        'tour_type' => 'Loại hình du lịch',
        'tour_category' => 'Loại tour',
        'tour_list' => 'Dòng Tour',
        'tour_place' => 'Điểm đến'
    ],
    'code' => 'Code',
    'name' => 'Tên',
    'slug' => 'Slug',
    'image' => 'Image',
    'description' => 'Mô tả',
    'schedule_service' => 'Dịch vụ',
    'created_success' => 'Thêm thành công!',
    'updated_success' => 'Cập nhật thành công!',
    'deleted_success' => 'Xóa thành công!',
    'active' => 'Duyệt',
    'top' => 'Nổi bật',
    'domestic' => 'Trong nước',
    'foreign' => 'Nước ngoài',
    'show_home' => 'Vị trí hiển thị trang chủ',
    'show_home_arr' => ['Top tour', 'Second tour', 'Third tour', 'Four tour']
];
