<?php

namespace Modules\Theme\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Theme\Entities\Theme;
use Modules\Theme\Entities\ThemeBlock;

class SeedThemeBlocksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        \DB::table( 'theme_block' )->insert([
            ['id' => 1, 'name' => 'Slide', 'key' => 'slide', 'value' => '', 'theme_id' => 1],
            ['id' => 2, 'name' => 'Banner', 'key' => 'banner', 'value' => '', 'theme_id' => 1],

            ['id' => 3, 'name' => 'Tour giá sốc', 'key' => 'list_tour', 'value' => '', 'theme_id' => 1],
            ['id' => 4, 'name' => 'Tour trong nước được mua nhiều', 'key' => 'list_tour', 'value' => '', 'theme_id' => 1],
            ['id' => 5, 'name' => 'Tour nước ngoài được mua nhiều', 'key' => 'list_tour', 'value' => '', 'theme_id' => 1],
            ['id' => 6, 'name' => 'Tour mới cực hot', 'key' => 'list_tour', 'value' => '', 'theme_id' => 1],

            ['id' => 7, 'name' => 'Điểm đến nổi bật', 'key' => 'slide_custom', 'value' => '', 'theme_id' => 1],
            ['id' => 8, 'name' => 'Khách hàng Eagle Tourist', 'key' => 'partner', 'value' => '', 'theme_id' => 1],
            ['id' => 9, 'name' => 'Vì sao chọn Eagle Tourist', 'key' => 'partner', 'value' => '', 'theme_id' => 1],
            ['id' => 10, 'name' => 'Đối tác', 'key' => 'partner', 'value' => '', 'theme_id' => 1]
        ]);

        // $this->call("OthersTableSeeder");
    }
}
