<?php

namespace Modules\Theme\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Theme\Entities\Menu;

class ThemeDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        \DB::table( 'themes' )->insert([
            ['name' => 'eagle'],
            ['name' => 'tomap'],
            ['name' => 'dulichdaibang']
        ]);

        $listMenu = [
            ['id' => 1, 'type_id' => 0, 'title' => '<i class="fa fa-home" style="font-size: 40px;"></i>', 'slug' => \Str::slug('Trang chủ'), 'position' => 1, 'arrange' => 1, 'children' => [] ],
            [
                'id' => 2, 'type_id' => 0, 'title' => 'Tour', 'slug' => 'tour', 'position' => 1, 'arrange' => 2,
                'children' => [
                    [ 'id' => 3, 'type_id' => 0, 'title' => 'Tour du lịch nước ngoài', 'slug' => \Str::slug('Tour du lịch nước ngoài'), 'position' => 1, 'arrange' => 3],
                    [ 'id' => 24, 'type_id' => 0, 'title' => 'Tour du lịch trong nước', 'slug' => \Str::slug('Tour du lịch trong nước'), 'position' => 1, 'arrange' => 4],
                ]
            ],
            [
                'id' => 4, 'type_id' => 0, 'title' => 'Tin tức du lịch', 'slug' => \Str::slug('Tin tức du lịch'), 'position' => 1, 'arrange' => 4,
                'children' => [
                    [ 'id' => 5, 'type_id' => 0, 'title' => 'Điểm đến', 'slug' => \Str::slug('Điểm đến'), 'position' => 1, 'arrange' => 5],
                    [ 'id' => 6, 'type_id' => 0, 'title' => 'Ẩm thực', 'slug' => \Str::slug('Ẩm thực'), 'position' => 1, 'arrange' => 6],
                    [ 'id' => 7, 'type_id' => 0, 'title' => 'Kinh nghiệm du lịch', 'slug' => \Str::slug('Kinh nghiệm du lịch'), 'position' => 1, 'arrange' => 7],
                ]
            ],
            [ 'id' => 8, 'type_id' => 2, 'title' => 'Liên hệ', 'slug' => \Str::slug('Liên hệ'), 'position' => 1, 'arrange' => 8, 'children' => []],
            [ 'id' => 9, 'type_id' => 2, 'title' => 'Giới thiệu', 'slug' => \Str::slug('Giới thiệu'), 'position' => 2, 'arrange' => 9, 'children' => []],
            [ 'id' => 10, 'type_id' => 2, 'title' => 'Chính sách - Quy định chung', 'slug' => \Str::slug('Chính sách - Quy định chung'), 'position' => 2, 'arrange' => 10, 'children' => []],
            [ 'id' => 11, 'type_id' => 2, 'title' => 'Chính sách đổi/trả - Hoàn tiền', 'slug' => \Str::slug('Chính sách đổi/trả - Hoàn tiền'), 'position' => 2, 'arrange' => 11, 'children' => []],
            [ 'id' => 12, 'type_id' => 2, 'title' => 'Chính sách bảo mật thông tin', 'slug' => \Str::slug('Chính sách bảo mật thông tin'), 'position' => 2, 'arrange' => 12, 'children' => []],
            [ 'id' => 13, 'type_id' => 2, 'title' => 'Chính sách giao nhận - Thanh toán', 'slug' => \Str::slug('Chính sách giao nhận - Thanh toán'), 'position' => 2, 'arrange' => 13,'children' => []],
            [
                'id' => 14, 'type_id' => 0, 'title' => 'Địa điểm du lịch', 'slug' => \Str::slug('Địa điểm du lịch'), 'position' => null, 'arrange' => 14,
                'children' => [
                    [ 'id' => 15, 'type_id' => 0, 'title' => 'Du lịch Huế', 'slug' => \Str::slug('Du lịch Huế'), 'position' => null, 'arrange' => 15],
                    [ 'id' => 16, 'type_id' => 0, 'title' => 'Du lịch Đà nẵng', 'slug' => \Str::slug('Du lịch Đà nẵng'), 'position' => null, 'arrange' => 16],
                    [ 'id' => 17, 'type_id' => 0, 'title' => 'Du lịch Nha trang', 'slug' => \Str::slug('Du lịch Nha Trang'), 'position' => null, 'arrange' => 17],
                    [ 'id' => 18, 'type_id' => 0, 'title' => 'Du lịch Đà lạt', 'slug' => \Str::slug('Du lịch Đà lạt'), 'position' => null, 'arrange' => 18],
                    [ 'id' => 19, 'type_id' => 0, 'title' => 'Du lịch Sài gòn', 'slug' => \Str::slug('Du lịch Sài gòn'), 'position' => null, 'arrange' => 19],
                    [ 'id' => 20, 'type_id' => 0, 'title' => 'Du lịch Vũng tàu', 'slug' => \Str::slug('Du lịch Vũng tàu'), 'position' => null, 'arrange' => 20],
                    [ 'id' => 21, 'type_id' => 0, 'title' => 'Du lịch Quảng bình', 'slug' => \Str::slug('Du lịch Quảng bình'), 'position' => null, 'arrange' => 21],
                    [ 'id' => 22, 'type_id' => 0, 'title' => 'Du lịch Hạ long', 'slug' => \Str::slug('Du lịch Hạ long'), 'position' => null, 'arrange' => 22],
                    [ 'id' => 23, 'type_id' => 0, 'title' => 'Du lịch Hà nội', 'slug' => \Str::slug('Du lịch Hà nội'), 'position' => null, 'arrange' => 23],
                ]
            ],
        ];
        foreach ($listMenu as $key => $value){
            Menu::create($value);
        }
        \DB::table( 'pages' )->insert( [
            [ 'id' => 8, 'title' => 'Liên hệ', 'slug' => \Str::slug('Liên hệ'), 'content' => 'Nhập nội dung Liên hệ vào đây!'],
            [ 'id' => 9, 'title' => 'Giới thiệu', 'slug' => \Str::slug('Giới thiệu'),'content' => 'Nhập nội dung giới thiệu vào đây!'],
            [ 'id' => 10, 'title' => 'Chính sách - Quy định chung', 'slug' => \Str::slug('Chính sách - Quy định chung'),'content' => 'Nhập nội dung chính sách quy định chung vào đây!'],
            [ 'id' => 11, 'title' => 'Chính sách đổi/trả - Hoàn tiền', 'slug' => \Str::slug('Chính sách đổi/trả - Hoàn tiền'),'content' => 'Nhập nội dung Chính sách đổi/trả - Hoàn tiền vào đây!'],
            [ 'id' => 12, 'title' => 'Chính sách bảo mật thông tin', 'slug' => \Str::slug('Chính sách bảo mật thông tin'),'content' => 'Nhập nội dung Chính sách bảo mật thông tin vào đây!'],
            [ 'id' => 13, 'title' => 'Chính sách giao nhận - Thanh toán', 'slug' => \Str::slug('Chính sách giao nhận - Thanh toán'),'content' => 'Nhập nội dung Chính sách giao nhận - Thanh toán vào đây!'],
            [ 'id' => 14, 'title' => 'Tìm kiếm', 'slug' => \Str::slug('Tìm kiếm'), 'content' => ''],
        ] );

        \DB::table( 'partners' )->insert( [
            [ 'id' => 1, 'name' => 'Huế','ptype_id' => 1, 'image' => 'public/images/demo/partners/hue.jpg', 'description' => null, 'active' => 1, 'arrange' => 1],
            [ 'id' => 2, 'name' => 'FLC Lux City','ptype_id' => 1, 'image' => 'public/images/demo/partners/flc-lux-city-samson-khu-du-lich-nghi-duong-flc-samson-beach-golf-resort.png', 'description' => null, 'active' => 1, 'arrange' => 2],
            [ 'id' => 3, 'name' => 'Khách sạn Cần Thơ','ptype_id' => 1, 'image' => 'public/images/demo/partners/hotel.png', 'description' => null, 'active' => 1, 'arrange' => 3],
            [ 'id' => 4, 'name' => 'Vietinbank','ptype_id' => 1, 'image' => 'public/images/demo/partners/logo_vietinbank.png', 'description' => null, 'active' => 1, 'arrange' => 4],
            [ 'id' => 5, 'name' => 'Việt nam','ptype_id' => 1, 'image' => 'public/images/demo/partners/du-lich-viet-nam-o-trong-nuoc.jpg', 'description' => null, 'active' => 1, 'arrange' => 5],
            [ 'id' => 6, 'name' => 'Eagle tourist','ptype_id' => 1, 'image' => 'public/images/demo/partners/logo.gif', 'description' => null, 'active' => 1, 'arrange' => 6],
            [ 'id' => 7, 'name' => 'Mạng bán tour số 1 việt nam','ptype_id' => 2, 'image' => null,'description' => 'Hơn 10.000+ chương trình đặc sắc khởi hành 24/7', 'active' => 1, 'arrange' => 1],
            [ 'id' => 8, 'name' => 'Đặt tour dễ dàng và có thể trả góp','ptype_id' => 2, 'image' => null, 'description' => 'Đặt tour trong 2 phút và "Đi tour trước, trả tiền sau"', 'active' => 1, 'arrange' => 2],
            [ 'id' => 9, 'name' => 'Công ty du lịch quốc tế được cấp phép','ptype_id' => 2, 'image' => null, 'description' => 'Giấy phép lữ hành quốc tế được bộ VH- TT&DL cấp !', 'active' => 1, 'arrange' => 3],
            [ 'id' => 10, 'name' => '25.000 khách hàng tin tưởng mỗi năm','ptype_id' => 2, 'image' => null, 'description' => 'Hơn 15.000 ý kiến đánh giá của quý khách hàng đã sử dụng dịch vụ', 'active' => 1, 'arrange' => 4],
            [ 'id' => 11, 'name' => 'Hơn 20+ báo lớn đã nói về chúng tôi','ptype_id' => 2, 'image' => null, 'description' => 'Khám Phá Di Sản, Dân Trí, Lao Động đã nói về những dịch vụ tuyệt vời của Eagle Tourist', 'active' => 1, 'arrange' => 5],
            [ 'id' => 12, 'name' => 'Nhận được nhiều giải thường danh giá','ptype_id' => 2, 'image' => null, 'description' => 'Với sự phát triển không ngừng, Du Lịch Đại Bàng nhận được nhiều giải thường danh giá', 'active' => 1, 'arrange' => 6],
        ] );

        \DB::table( 'banners' )->insert( [
            [ 'id' => 1, 'name' => 'Series Tour Phú Quốc 2019','image' => 'public/images/demo/banners/WEB-TOUR-01.png'],
            [ 'id' => 2, 'name' => 'Series Tour Thái Lan 2019','image' => 'public/images/demo/banners/WEB-TOUR-02.png'],
            [ 'id' => 3, 'name' => 'Charter Hàn Quốc Siêu Rẻ','image' => 'public/images/demo/banners/WEB-TOUR-03.png'],
        ] );
        \DB::table( 'slides' )->insert( [
            [ 'id' => 1, 'name' => 'Huế','image' => 'public/images/demo/slides/hue-tomap.jpg'],
            [ 'id' => 2, 'name' => 'Sài Gòn','image' => 'public/images/demo/slides/saigon-tomap.jpg'],
            [ 'id' => 3, 'name' => 'Hạ Long','image' => 'public/images/demo/slides/tomap-halong.jpg'],
        ] );

        \DB::table('galleries')->insert([
            [ 'id' => 1, 'title' => 'Đoàn Du Lịch Lào', 'slug' => \Str::slug('Đoàn Du Lịch Lào'), 'name' => 'Đoàn du lịch lào$', 'files' => 'public/images/demo/customers/doan-du-lich-lao.jpg$','active' => 1, 'arrange' => 1],
            [ 'id' => 2, 'title' => 'Đoàn Du Lịch Sapa', 'slug' => \Str::slug('Đoàn Du Lịch Sapa'), 'name' => 'Đoàn Du Lịch Sapa$', 'files' => 'public/images/demo/customers/doan-du-lich-sapa.jpg$','active' => 1, 'arrange' => 2],
            [ 'id' => 3, 'title' => 'Đoàn Du Lịch Đà Lạt', 'slug' => \Str::slug('Đoàn Du Lịch Đà Lạt'), 'name' => 'Đoàn Du Lịch Đà Lạt$', 'files' => 'public/images/demo/customers/doan-du-lich-da-lat.jpg$','active' => 1, 'arrange' => 3],
            [ 'id' => 4, 'title' => 'Đoàn Du Lịch Team Building', 'slug' => \Str::slug('Đoàn Du Lịch Team Building'), 'name' => 'Đoàn Du Lịch Team Building$', 'files' => 'public/images/demo/customers/doan-du-lich-teambuilding.jpg$','active' => 1, 'arrange' => 4],
            [ 'id' => 5, 'title' => 'Đoàn Du Lịch Thái Lan', 'slug' => \Str::slug('Đoàn Du Lịch Thái Lan'), 'name' => 'Đoàn Du Lịch Thái Lan$', 'files' => 'public/images/demo/customers/doan-du-lich-thai-lan.jpg$','active' => 1, 'arrange' => 5],
            [ 'id' => 6, 'title' => 'Đoàn Du Lịch Singapore', 'slug' => \Str::slug('Đoàn Du Lịch Singapore'), 'name' => 'Đoàn Du Lịch Singapore$', 'files' => 'public/images/demo/customers/doan-du-lich-singapore.jpg$','active' => 1, 'arrange' => 6],
        ]);
    }
}
