<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Kalnoy\Nestedset\NestedSet;

class AddColumnMenus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::table('menus', function (Blueprint $table) {
            $table->nestedSet();
            $table->tinyInteger('active')->default(1); //1: active
            $table->string('keywords')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::table('menus', function (Blueprint $table) {
            NestedSet::dropColumns($table);
            $table->dropColumn('active');
        });
    }
}
