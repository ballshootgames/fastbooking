@extends("theme::$themeName.frontend.master")

@section('style-css')
    <style>
        .is-invalid{
            border-color: #ff0000 !important;
        }
    </style>
@endsection
@section('breadcrumb')
    <div class="st-breadcrumb hidden-xs">
        <div class="container">
            <ul>
                <li><a href="{{ url('/') }}">{{ trans('theme::frontend.home') }}</a></li>
                <li class="active">{{ trans('theme::frontend.booking.confirm') }}</li>
            </ul>
        </div>
    </div>
@endsection

@section('content')
    <div class="container">
        <div class="st-checkout-page" style="margin-top: 30px;">
            @php
                $user = Auth::check() ? Auth::user() : '';
            @endphp
            @if(Session::has('paypal_error'))
                <div class="alert alert-danger">
                    <span><i class="fa fa-exclamation-circle" aria-hidden="true" style="padding-right: 10px"></i></span>{{ Session::get('paypal_error') }}
                </div>
                <?php Session::forget('paypal_error');?>
            @elseif(Session::has('paypal_success'))
                @if(Session::has('paypal_success_error'))
                    <div class="alert alert-info">
                        <span><i class="fa fa-check" aria-hidden="true" style="padding-right: 10px"></i></span>{{ Session::get('paypal_success_error') }}
                    </div>
                    <?php Session::forget('paypal_success_error');?>
                @endif
                <div class="booking-success-notice">
                    <div class="alert alert-success" style="font-size: 16px">
                        <i class="fa fa-check" aria-hidden="true" style="padding-right: 10px"></i>
                        @php($booking_code="")
                        @foreach(Session::get('paypal_success') as $booking_id)
                            @php($booking = \Modules\Booking\Entities\Booking::find($booking_id))
                            @php($booking_code = ($booking_code==="")?$booking->code:$booking_code.", ".$booking->code)
                        @endforeach
                        XÁC NHẬN BOOKING {{ $booking_code }}
                    </div>
                    @php($books = Session::get('book'))
                    @php(Session::forget('book'))
                    @php($tName="")
                    @php($tCode="")
                    @foreach($books as $key => $tour)
                        @php($tName .= $tour['item']->name)
                        @php($tCode .= $tour['item']->prefix_code.$tour['item']->code)
                    @endforeach
                    @foreach(Session::get('paypal_success') as $booking_id)
                        @php($booking = \Modules\Booking\Entities\Booking::find($booking_id))
                        @php($customer = $booking->customer)
                        <p style="padding-left: 5px;">Xin chào <b>{{ $customer->name }}</b>,</p>
                        <p style="padding-left: 5px;">Cám ơn Quý khách đã đặt dịch vụ tại {{ $settings['company_name'] }}. Dưới đây là thông tin booking.</p>
                        <p style="padding-left: 5px;">Chúng tôi sẽ kiểm tra ngay booking của Quý khách và liên hệ với Quý khách trong thời gian sớm nhất (trong vòng 24h) theo các thông tin mà Quý khách đã cung cấp. Vui lòng kiểm tra email để xem lại thông tin booking của Quý khách.</p>
                        <h4 class="title" style="color: #4d9200; font-weight: bold; font-size: 18px; padding-top: 5px; padding-left: 5px;">THÔNG TIN NGƯỜI ĐẶT</h4>
                        <div class="info-form" style="border:none;padding: 0px;margin-top: 10px;">
                            <ul>
                                <li style="padding:5px; margin:0px; border-bottom-style:dashed;">
                                    <span class="label order__label" style="font-weight: bold;">Họ tên</span>
                                    <span class="value">{{ $customer->name }}</span>
                                </li>
                                <li style="padding:5px; margin:0px; border-bottom-style:dashed;">
                                    <span class="label order__label" style="font-weight: bold;">Địa chỉ</span>
                                    <span class="value">{{ $customer->address }}</span>
                                </li>
                                <li style="padding:5px; margin:0px; border-bottom-style:dashed;">
                                    <span class="label order__label" style="font-weight: bold;">Điện thoại</span>
                                    <span class="value">{{ $customer->phone }}</span>
                                </li>
                                <li style="padding:5px; margin:0px;">
                                    <span class="label order__label" style="font-weight: bold;">Email</span>
                                    <span class="value">{{ $customer->email }}</span>
                                </li>
                            </ul>
                        </div>
                        <h4 class="title" style="color: #4d9200; font-weight: bold; margin-top:20px; font-size: 18px; padding-left: 5px;">DỊCH VỤ ĐÃ ĐẶT</h4>
                        <div class="info-form" style="border:none;padding: 0px;margin-top: 10px;">
                            <ul>
                                <li style="padding:5px; margin:0px; border-bottom-style:dashed;">
                                    <span class="label order__label" style="font-weight: bold;">Mã tour</span>
                                    <span class="value">{{ $tCode }}</span>
                                </li>
                                <li style="padding:5px; margin:0px; border-bottom-style:dashed;">
                                    <span class="label order__label" style="font-weight: bold;">Tên tour</span>
                                    <span class="value">{{ $tName }}</span>
                                </li>
                                <li style="padding:5px; margin:0px; border-bottom-style:dashed;">
                                    <span class="label order__label" style="font-weight: bold;">Tổng tiền</span>
                                    <span class="value" style="color: #ee0000">{{ number_format($booking->total_price) }} VNĐ</span>
                                </li>
                                <li style="padding:5px; margin:0px; border-bottom-style:dashed;">
                                    <span class="label order__label" style="font-weight: bold;">Yêu cầu khác</span>
                                    <span class="value">{{ $booking->note }}</span>
                                </li>
                                <li style="padding:5px; margin:0px; border-bottom-style:dashed;">
                                    <span class="label order__label" style="font-weight: bold;">Ngày đặt</span>
                                    <span class="value">{{ Carbon\Carbon::parse($booking->created_at)->format(config('settings.format.date')) }}</span>
                                </li>
                                <li style="padding:5px; margin:0px; border-bottom-style:dashed;">
                                    <span class="label order__label" style="font-weight: bold;">Hình thức thanh toán</span>
                                    <span class="value">{{ optional($booking->payment)->name }}</span>
                                </li>
                                <li style="padding:5px; margin:0px;">
                                    <span class="label order__label" style="font-weight: bold;">Trạng thái</span>
                                    <span class="value" style="color: #ee0000">{{ optional($booking->status)->name }}</span>
                                </li>
                            </ul>
                        </div>
                        <h4 class="title" style="color: #4d9200; font-weight: bold; margin-top:20px; font-size: 18px; padding-left: 5px;">THÔNG TIN THANH TOÁN</h4>
                        <div class="info-form" style="padding: 10px;">
                            <p>Sau khi nhận được thông tin đặt dịch vụ, nhân viên chăm sóc khách hàng của chúng tôi sẽ liên hệ với Quý khách để xác nhận và hướng dẫn thêm về thủ tục thanh toán. Mọi thắc mắc, Quý khách có thể liên hệ tổng đài {{ $settings['phone'] }}.</p>
                            <p>QUÝ KHÁCH SAU KHI THỰC HIỆN VIỆC CHUYỂN KHOẢN VUI LÒNG FAX UỶ NHIỆM CHI VỀ CÔNG TY {{ strtoupper($settings['company_name']) }} VÀ LIÊN HỆ VỚI NHÂN VIÊN PHỤ TRÁCH TUYẾN ĐỂ NHẬN ĐƯỢC VỀ DU LỊCH CHÍNH THỨC TỪ CÔNG TY CHÚNG TÔI. {{ strtoupper($settings['company_name']) }} SẼ KHÔNG GIẢI QUYẾT CÁC TRƯỜNG HỢP BOOKING TỰ ĐỘNG HUỶ NẾU QUÝ KHÁCH KHÔNG THỰC HIỆN ĐÚNG HƯỚNG DẪN TRÊN.</p>
                        </div>
                        <p style="margin-top: 20px; font-weight:bold;"><span style="color: #ee0000; align-items: center;">*</span>Quý khách lưu ý: Dịch vụ vẫn chưa được xác nhận cho tới khi booking của Quý khách được thanh toán và được xác nhận thành công từ {{ $settings['company_name'] }}.</p>
                    @endforeach
                </div>
                <?php Session::forget('paypal_success');?>
            @endif
        </div>
    </div>
@endsection
