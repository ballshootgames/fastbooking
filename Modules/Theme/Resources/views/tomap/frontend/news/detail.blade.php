@extends("theme::$themeName.frontend.master")
@section('title')
    <title>{!! $news->title !!} - {{ $settings['website_name'] }}</title>
    <meta name="description" content="{{ !empty($news->description) ? Str::limit($news->description, 200) : Str::limit($news->content, 200)}}"/>
@endsection
@section('facebook')
    <meta property="og:title" content="{!! $news->title !!} - {{ $settings['website_name'] }}"/>
    <meta property="og:description" content="{{ !empty($news->description) ? Str::limit($news->description, 200) : Str::limit($news->content, 200)}}"/>
    <meta property="og:image" content="{{ !empty($news->image) ? asset(Storage::url($news->image)) : asset('img/noimage.gif') }}"/>
    <meta property="og:image:secure_url" content="{{ !empty($news->image) ? asset(Storage::url($news->image)) : asset('img/noimage.gif') }}"/>
@endsection
@section('twitter')
    <meta name="twitter:title" content="{!! $news->title !!} - {{ $settings['website_name'] }}"/>
    <meta name="twitter:description" content="{{ !empty($news->description) ? Str::limit($news->description, 200) : Str::limit($news->content, 200)}}"/>
    <meta name="twitter:image" content="{{ !empty($news->image) ? asset(Storage::url($news->image)) : asset('img/noimage.gif') }}"/>
@endsection
@section('breadcrumb')
    @php
        $background = !empty($slides->first()) ? asset(Storage::url($slides->first()->image)) : asset('img/hue-tomap.jpg');
    @endphp
    <div class="banner" style="background-image: url('{{ $background }}')">
        <div class="container">
            <h1>{{ optional($news->type)->title }}</h1>
        </div>
    </div>
    <div class="st-breadcrumb hidden-xs">
        <div class="container">
            <ul>
                <li><a href="{{ url('/') }}">{{ trans('theme::frontend.home') }}</a></li>
                <li>
                    @if(!empty($newsType->parent_id))
                        <a href="{{ url( $newsTypeParent->slug . '/' . optional($news->type)->slug) }}">{{ optional($news->type)->title }}</a>
                    @else
                        <a href="{{ url( $newsTypeParent->slug ) }}">{{ $newsTypeParent->title }}</a>
                    @endif
                </li>
                <li class="active">{!! $news->title !!}</li>
            </ul>
        </div>
    </div>
@endsection
@section('content')
    <div class="st-blog">
        <div class="container">
            <div class="blog-content content">
                <div class="row">
                    <div class="col-xs-12 col-sm-8 col-md-9">
                        <div class="article">
                            <div class="header">
                                <div class="post-header text-center">
                                    <img src="{{asset(Storage::url($news->image))}}" alt="{{ $news->title }}" class="attachment-full size-full wp-post-image" />
                                </div>
                            </div>
                            <h2 class="title">
                                {!! $news->title !!}
                            </h2>
                            <div class="post-info">
                                <span class="date">
                                    <i class="fa fa-calendar-plus-o" aria-hidden="true"></i> {{ Carbon\Carbon::parse($news->updated)->format(config('settings.format.date')) }}
                                </span>
                            </div>
                            <div class="post-content">
                                <p>
                                    <i>{!! $news->description !!}</i>
                                </p>
                                {!! $news->content !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-3">
                        <aside class="sidebar-right">
                            <div class="sidebar-widget widget_search">
                                {!! Form::open(['method' => 'GET', 'url' => 'tim-kiem.html', 'role' => 'form']) !!}
                                <input type="text" value="{{\Request::get('title')}}" class="form-control input-sm" name="title" autocomplete="off" placeholder="{{ __('message.search_keyword') }}">
                                <button type="submit">
                                    <i class="input-icon field-icon fa"><svg height="15px" width="15px" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
                                    <g fill="#ffffff">
                                        <path d="M23.245,23.996c-0.2,0-0.389-0.078-0.53-0.22L16.2,17.26c-0.761,0.651-1.618,1.182-2.553,1.579
                                            c-1.229,0.522-2.52,0.787-3.837,0.787c-1.257,0-2.492-0.241-3.673-0.718c-2.431-0.981-4.334-2.849-5.359-5.262
                                            c-1.025-2.412-1.05-5.08-0.069-7.51S3.558,1.802,5.97,0.777C7.199,0.254,8.489-0.01,9.807-0.01c1.257,0,2.492,0.242,3.673,0.718
                                            c2.431,0.981,4.334,2.849,5.359,5.262c1.025,2.413,1.05,5.08,0.069,7.51c-0.402,0.996-0.956,1.909-1.649,2.718l6.517,6.518
                                            c0.292,0.292,0.292,0.768,0,1.061C23.634,23.918,23.445,23.996,23.245,23.996z M9.807,1.49c-1.115,0-2.209,0.224-3.25,0.667
                                            C4.513,3.026,2.93,4.638,2.099,6.697c-0.831,2.059-0.81,4.318,0.058,6.362c0.869,2.044,2.481,3.627,4.54,4.458
                                            c1.001,0.404,2.048,0.608,3.112,0.608c1.115,0,2.209-0.224,3.25-0.667c0.974-0.414,1.847-0.998,2.594-1.736
                                            c0.01-0.014,0.021-0.026,0.032-0.037c0.016-0.016,0.031-0.029,0.045-0.039c0.763-0.771,1.369-1.693,1.786-2.728
                                            c0.831-2.059,0.81-4.318-0.059-6.362c-0.868-2.044-2.481-3.627-4.54-4.458C11.918,1.695,10.871,1.49,9.807,1.49z"></path>
                                    </g>
                                    </svg></i>
                                </button>
                                {!! Form::close() !!}
                            </div>
                            <div class="sidebar-widget widget_text">
                                <div class="sidebar-title">
                                    <h4>{{ trans('theme::frontend.news.random') }}</h4>
                                </div>
                                <div class="textwidget">
                                    @foreach($newsRandoms as $itemRandom)
                                        <div class="service-border">
                                            <div class="thumb">
                                                <a href="{{ url( optional($itemRandom->type)->slug) . '/' . $itemRandom->slug }}.html">
                                                    <img src="{{ empty($itemRandom->image) ? asset('img/noimage.gif') : \Modules\News\Entities\News::getThumbnail($itemRandom->image, 268, 197) }}" alt="{!! $itemRandom->title !!}" class="img-responsive wp-post-image" />
                                                </a>
                                            </div>
                                            <h4 class="service-title plr15" style="height: auto;">
                                                <a href="{{ url( optional($itemRandom->type)->slug) . '/' . $itemRandom->slug }}.html">{!! $itemRandom->title !!}</a>
                                            </h4>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
                <div class="article__other">
                    <h3>{{ trans('theme::frontend.news.related') }}</h3>
                    <div class="row news--row">
                        @foreach($newsRelates as $item)
                            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 item-service grid-item has-matchHeight mb-10">
                                <div class="service-border">
                                    <div class="thumb">
                                        <a href="{{ url( optional($item->type)->slug) . '/' . $item->slug }}.html">
                                            <img src="{{ empty($item->image) ? asset('img/noimage.gif') : \Modules\News\Entities\News::getThumbnail($item->image, 268, 197) }}" alt="{{ $item->title }}" class="img-responsive wp-post-image" />
                                        </a>
                                    </div>
                                    <div class="news--body">
                                        <h4 class="service-title plr15">
                                            <a href="{{ url( optional($item->type)->slug) . '/' . $item->slug }}.html">{!! $item->title !!}</a>
                                        </h4>
                                        <div class="service-description plr15">
                                            {!! Str::limit($item->description, 100) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--<section class="single">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-9 ">
                    <div class="content-single">
                        <h1 class="title">{{ $news->title }}</h1>
                        <div class="meta">
                            <span><strong>Ngày đăng: </strong>{{ \Carbon\Carbon::parse($news->updated_at)->format(config('settings.format.date_')) }}</span>
                            @if($news->user)
                            <span><strong>Bởi: </strong>{{ optional($news->user)->name }}</span>
                            @endif
                            <span><a href="{{ url(  $newsTypeParent->slug . '/' . optional($news->type)->slug) }}" rel="category tag">{{ optional($news->type)->title }}</a></span>
                        </div>
                        <span class="like">
                            @include('theme::layouts.facebook-like', ['link' => $linkSocial])
                        </span>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-9 col-md-push-3">
                                <article class="post-content">
                                    <p style="text-align: justify;"><strong>{{ $news->description }}</strong></p>
                                    {!! $news->content !!}
                                </article>

                                <div class="meta-s">
                                    <span class="like">
                                        <p><img src="{{ asset('img/like_click.gif') }}" alt="Click like"></p>
                                        @include('theme::layouts.facebook-like', ['link' => $linkSocial])
                                    </span>
                                </div>
                                <div class="content-cmt">
                                    @include('theme::layouts.facebook-comment', ['link' => $linkSocial])
                                </div>
                                <div class="related"><h3 class="title-related">Bài viết liên quan</h3>
                                    <ul class="row">
                                        @foreach($newsRelates as $itemRelate)
                                            <li class="col-xs-12 col-sm-6 col-md-4">
                                                <a class="link-img" href="{{ url( optional($itemRelate->type)->slug . '/' . $itemRelate->slug) }}.html">
                                                    <img data-sizes="auto" class="lazyload" data-expand="-10" data-src="{{ empty($itemRelate->image) ? asset('img/noimage.gif') : \Modules\News\Entities\News::getThumbnail($itemRelate->image, 200, 150) }}" alt="{{ $itemRelate->title }}">
                                                </a>
                                                <h4>
                                                    <a href="{{ url( optional($itemRelate->type)->slug . '/' . $itemRelate->slug) }}.html">{{ $itemRelate->title }}</a>
                                                </h4>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-3 col-md-pull-9">
                                <div class="left-single">
                                    <h3 class="ran">Bài viết ngẫu nhiên</h3>
                                    <ul>
                                        @foreach($newsRandoms as $itemRandom)
                                            <li>
                                                <a class='img' href="{{ url( optional($itemRandom->type)->slug . '/' . $itemRandom->slug) }}.html">
                                                    <img data-sizes="auto" class="lazyload" data-expand="-10" data-src="{{ empty($itemRandom->image) ? asset('img/noimage.gif') : \Modules\News\Entities\News::getThumbnail($itemRandom->image, 190, 143) }}"
                                                         alt="{{ $itemRandom->title }}"/>
                                                </a>
                                                <h4><a href="{{ url( optional($itemRandom->type)->slug . '/' . $itemRandom->slug) }}.html">{{ $itemRandom->title }}</a></h4>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>--}}
@endsection

