@extends("theme::$themeName.frontend.master")
@section('title')
    <title>{{ $newsType->title }} - {{ $settings['website_name'] }}</title>
    <meta name="description" content="{{ !empty($newsType->description) ? strip_tags(Str::limit($newsType->description, 200)) : strip_tags(Str::limit($newsType->content, 200))}}"/>
@endsection
@section('breadcrumb')
    @php
        $background = !empty($slides->first()) ? asset(Storage::url($slides->first()->image)) : asset('img/hue-tomap.jpg');
    @endphp
    <div class="banner" style="background-image: url('{{ $background }}')">
        <div class="container">
            <h1>{{ $newsType->title }}</h1>
        </div>
    </div>
    <div class="st-breadcrumb hidden-xs">
        <div class="container">
            <ul>
                <li><a href="{{ url('/') }}">{{ trans('theme::frontend.home') }}</a></li>
                <li><a href="{{ url( $idNewsType->slug) }}">{{ $idNewsType->title }}</a></li>
                <li class="active">{{ $newsType->title }}</li>
            </ul>
        </div>
    </div>
@endsection
@section('content')
    <div class="container">
        <div class="st-hotel-result tour-top-search">
            <div id="modern-search-result" class="modern-search-result">
                <div class="row row-wrapper news--row">
                    @foreach($newsLists as $item)
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 item-service grid-item has-matchHeight">
                            <div class="service-border">
                                <div class="thumb thumb-responsive">
                                    <a href="{{ url( optional($item->type)->slug) . '/' . $item->slug }}.html">
                                        <img data-sizes="auto" class="lazyload img-responsive wp-post-image" data-expand="-10" data-src="{{ empty($item->image) ? asset('img/noimage.gif') : asset(Storage::url($item->image)) }}"
                                             alt="{{ $item->title }}"/>
                                    </a>
                                </div>
                                <div class="news--body">
                                    <h4 class="service-title plr15">
                                        <a href="{{ url( optional($item->type)->slug) . '/' . $item->slug }}.html">{!! $item->title !!}</a>
                                    </h4>
                                    <div class="service-description plr15">
                                        {!! Str::limit($item->description, 100) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="col-xs-12" style="display: flex; justify-content: center">
                        {!! $newsLists->appends(Request::except('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

