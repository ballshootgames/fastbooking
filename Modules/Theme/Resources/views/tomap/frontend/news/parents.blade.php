@extends("theme::$themeName.frontend.master")
@section('title')
    <title>{{ $newsTypeParent->title }} - {{ $settings['website_name'] }}</title>
    <meta name="description" content="{{ !empty($newsTypeParent->description) ? strip_tags(Str::limit($newsTypeParent->description, 200)) : strip_tags(Str::limit($newsTypeParent->content, 200))}}"/>
@endsection
@section('breadcrumb')
    @php
        $background = !empty($slides->first()) ? asset(Storage::url($slides->first()->image)) : asset('img/hue-tomap.jpg');
    @endphp
    <div class="banner" style="background-image: url('{{ $background }}')">
        <div class="container">
            <h1>{{ $newsTypeParent->title }}</h1>
        </div>
    </div>
    <div class="st-breadcrumb hidden-xs">
        <div class="container">
            <ul>
                <li><a href="{{ url('/') }}">{{ trans('theme::frontend.home') }}</a></li>
                <li class="active">{{ $newsTypeParent->title }}</li>
            </ul>
        </div>
    </div>
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="wpb_column column_container col-md-12 vc_custom_1542167696382">
                <div class="vc_column-inner wpb_wrapper">
                    <div class="wpb_text_column wpb_content_element  fs-28 fs-normal">
                        @foreach($newsTypes as $item)
                        <div class="wpb_wrapper">
                            <h2><a href="{{ url($newsTypeParent->slug . '/' . $item->slug) }}" style="color: #1A2B48">{{ $item->title }}</a></h2>
                        </div>
                        <div class="search-result-page st-tours service-slider-wrapper">
                            <div class="st-hotel-result" style="margin-top: 0;">
                                <div class="row row-wrapper news--row">
                                    @php
                                        $newsLists = \Modules\News\Entities\News::with('type')->where('news_type_id',$item->id)->take(8)->get();
                                    @endphp
                                    @foreach($newsLists as $item)
                                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 item-service grid-item has-matchHeight">
                                            <div class="service-border">
                                                <div class="thumb thumb-responsive">
                                                    <a href="{{ url( optional($item->type)->slug) . '/' . $item->slug }}.html">
                                                        <img data-sizes="auto" class="lazyload img-responsive wp-post-image" data-expand="-10" data-src="{{ empty($item->image) ? asset('img/noimage.gif') : asset(Storage::url($item->image)) }}"
                                                             alt="{{ $item->title }}"/>
                                                    </a>
                                                </div>
                                                <div class="news--body">
                                                    <h4 class="service-title plr15">
                                                        <a href="{{ url( optional($item->type)->slug) . '/' . $item->slug }}.html">{!! $item->title !!}</a>
                                                    </h4>
                                                    <div class="service-description plr15">
                                                        {!! Str::limit($item->description, 100) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection