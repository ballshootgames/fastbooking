@extends("theme::$themeName.frontend.master")

@section('breadcrumb')
    @php($background = !empty($slides->first()) ? asset(Storage::url($slides->first()->image)) : asset('img/ba-na-1900x420.jpg'))
    <div class="banner" style="background-image: url('{{ $background }}')">
        <div class="container">
            <h1>{{ $page->title }}</h1>
        </div>
    </div>
    <div class="st-breadcrumb hidden-xs">
        <div class="container">
            <ul>
                <li><a href="{{ url('/') }}">{{ trans('theme::frontend.home') }}</a></li>
                <li class="active">{{ $page->title }}</li>
            </ul>
        </div>
    </div>
@endsection
@section('content')
    <div class="container">
        <div class="st-hotel-result tour-top-search">
            <div id="modern-search-result" class="modern-search-result">
                <div class="row row-wrapper news--row">
                    @if(!empty($newsLists->count()))
                        @foreach($newsLists as $item)
                            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 item-service grid-item has-matchHeight">
                                <div class="service-border">
                                    <div class="thumb">
                                        <a href="{{ url( optional($item->type)->slug) . '/' . $item->slug }}.html">
                                            <img src="{{ empty($item->image) ? asset('img/noimage.gif') : \Modules\News\Entities\News::getThumbnail($item->image, 268, 197) }}" alt="{{ $item->title }}" class="img-responsive wp-post-image" />
                                        </a>
                                    </div>
                                    <div class="news--body">
                                        <h4 class="service-title plr15">
                                            <a href="{{ url( optional($item->type)->slug) . '/' . $item->slug }}.html">{{ $item->title }}</a>
                                        </h4>
                                        <div class="service-description plr15">
                                            {!! Str::limit($item->description, 100) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                            <div class="quatrang">
                                {!! $newsLists->appends(\Request::except('page'))->render() !!}
                            </div>
                    @else
                        <p class="text-center" style="font-size: 16px">{{ __('Không có kết quả nào phù hợp. Vui lòng tìm kiếm từ khóa khác!') }}</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

