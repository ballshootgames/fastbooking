@extends('theme::'.$themeName.'.frontend.master')
@section('title')
    <title>{{ $page->title }} - {{ $settings['website_name'] }}</title>
    <meta name="description" content="{{ !empty($page->description) ? strip_tags(Str::limit($page->description, 200)) : strip_tags(Str::limit($page->content, 200))}}"/>
@endsection
@section('styles')
    <style>
        .hs-contact form{
            background-color: transparent;
        }
        .hs-contact form label{
            margin-bottom: 5px;
        }
    </style>
@endsection
@section('breadcrumb')
    @php($background = !empty($slides->first()) ? asset(Storage::url($slides->first()->image)) : asset('img/ba-na-1900x420.jpg'))
    <div class="banner" style="background-image: url('{{ $background }}')">
        <div class="container">
            <h1>{{ $page->title }}</h1>
        </div>
    </div>
    <div class="st-breadcrumb hidden-xs">
        <div class="container">
            <ul>
                <li><a href="{{ url('/') }}">{{ trans('theme::frontend.home') }}</a></li>
                <li class="active">{{ $page->title }}</li>
            </ul>
        </div>
    </div>
@endsection
@section('content')
    <div class="container">
        <div class="row wpb_row st bg-holder" style="height: 75px">
        </div>
        <div class="row wpb_row st bg-holder">
            @if($slugPage === 'lien-he')
                <div class="wpb_column column_container col-md-5">
                    <div class="vc_column-inner wpb_wrapper">
                        <div class="wpcf7">
                            {!! Form::open(['method' => 'POST', 'url' => '', 'role' => 'contact', 'id' => 'contact'])  !!}
                                <div class="st-contact-form">
                                    <div class="contact-header">
                                        <h3>{{ trans('theme::frontend.contact.request') }}</h3>
                                        <p>{{ trans('theme::frontend.contact.description') }}</p>
                                    </div>
                                    <div class="contact-form">
                                        <div class="hs-form-success text-center" style="display: none">
                                            {{ trans('theme::eagles.success_contact') }}
                                        </div>
                                        <div class="hs-form-contact row">
                                            <div class="form-group col-md-6">
                                                {!! Form::text('fullname', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Họ và tên']) !!}
                                                {!! $errors->first('fullname', '<p class="help-block">:message</p>') !!}
                                            </div>
                                            <div class="form-group col-md-6">
                                                {!! Form::email('email', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Email']) !!}
                                                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                                            </div>
                                            <div class="form-group col-md-6">
                                                {!! Form::text('address', null, ['class' => 'form-control', 'placeholder' => 'Địa chỉ']) !!}
                                            </div>
                                            <div class="form-group col-md-6">
                                                {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Điện thoại']) !!}
                                            </div>
                                            <div class="form-group col-xs-12">
                                                {!! Form::textarea('message', null, ['class' => 'form-control', 'required' => 'required', 'rows' => 5, 'placeholder' => 'Nội dung']) !!}
                                                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                                            </div>
                                            <div class="col-xs-12">
                                                <input type="submit" class="wpcf7-form-control wpcf7-submit btn btn-primary text-uppercase" value="{{ __('theme::eagles.send') }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <div class="wpb_column column_container col-md-2"></div>
                <div class="wpb_column column_container col-md-5">
                    <div class="vc_column-inner wpb_wrapper">
                        <div class="st-contact-info">
                            <div class="info-bg">
                                <img src="https://tomap.travelerwp.com/wp-content/uploads/2018/12/bg-contact-1.jpg" class="img-responsive" alt="Background Contact Info">
                            </div>
                            <div class="info-content">
                                <h3>{{ $settings['website_name'] }}</h3>
                                <div class="sub">
                                    {!! $page->content !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <div class="wpb_column column_container col-xs-12">
                    {!! $page->content !!}
                </div>
            @endif
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function(){
            $('#contact').submit(function(e){
                e.preventDefault();
                $.ajax({
                    url: '{{ url('/lien-he/ajax') }}',
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize(),
                    success: function (res) {
                        if (res.success === "ok"){
                            $('.hs-form-contact').hide();
                            $('.hs-form-success').show();
                        }else{
                            console.log(data.errors);
                        }
                    },
                    error: function (error) {
                        alert("{{ __('theme::eagles.error') }}");
                    }
                });
            });
        });
    </script>
@endsection
