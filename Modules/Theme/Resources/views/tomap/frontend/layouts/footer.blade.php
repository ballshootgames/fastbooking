<div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row st bg-holder vc_custom_1542265299369 vc_row-has-fill">
    <div class='container'>
        <div class='row'>
            <div class="wpb_column column_container col-md-3">
                <div class="vc_column-inner wpb_wrapper">
                    <div class="wpb_text_column wpb_content_element  vc_custom_1543220007372" >
                        <div class="wpb_wrapper">
                            <h4 style="font-size: 14px; text-transform: uppercase">{{ trans('theme::frontend.footer.advisory') }}</h4>
                        </div>
                    </div>
                    <div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_40 vc_sep_pos_align_left vc_separator_no_text" >
                        <span class="vc_sep_holder vc_sep_holder_l">
                            <span  style="border-color:#eaeaea;" class="vc_sep_line"></span>
                        </span>
                        <span class="vc_sep_holder vc_sep_holder_r">
                            <span  style="border-color:#eaeaea;" class="vc_sep_line"></span>
                        </span>
                    </div>
                    <div class="wpb_text_column wpb_content_element  vc_custom_1543221092622" >
                        <div class="wpb_wrapper">
                            @if(!empty($contactInfo['phone_contact_tour']))
                                <p><span style="color: #5e6d77;">{{ trans('theme::frontend.footer.call_us') }}</span></p>
                                <h4>{{ $contactInfo['phone_contact_tour'] }}</h4>
                            @endif
                            @if(!empty($contactInfo['phone_contact_advisory']))
                                <h4>{{ $contactInfo['phone_contact_advisory'] }}</h4>
                            @endif
                        </div>
                    </div>
                    <div class="wpb_text_column wpb_content_element  vc_custom_1543221080532" >
                        <div class="wpb_wrapper">
                            @if(!empty($contactInfo['email_contact_tour']))
                                <p><span style="color: #5e6d77;">{{ trans('theme::frontend.footer.send_mail') }}</span></p>
                                <h4>{{ $contactInfo['email_contact_tour'] }}</h4>
                            @endif
                            @if(!empty($contactInfo['email_contact_advisory']))
                                <h4>{{ $contactInfo['email_contact_advisory'] }}</h4>
                            @endif
                        </div>
                    </div>
                    <div class="wpb_text_column wpb_content_element  vc_custom_1544512778823" >
                        <div class="wpb_wrapper">
                            <p><span style="color: #5e6d77; margin-bottom: 5px;">{{ trans('theme::frontend.footer.follow_us') }}<br /></span></p>
                            <ul class="st-list footer-socials">
                                <li>
                                    @if(!empty($socialInfo['facebook_link']))
                                        <a href="{{ $socialInfo['facebook_link'] }}" target="_blank">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    @endif
                                    @if(!empty($socialInfo['twitter_link']))
                                        <a href="{{ $socialInfo['twitter_link'] }}" target="_blank">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    @endif
                                    @if(!empty($socialInfo['linkedin_link']))
                                        <a href="{{ $socialInfo['linkedin_link'] }}" target="_blank">
                                            <i class="fa fa-linkedin"></i>
                                        </a>
                                    @endif
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wpb_column column_container col-md-3">
                <div class="vc_column-inner wpb_wrapper">
                    <div class="wpb_text_column wpb_content_element  vc_custom_1543220015251" >
                        <div class="wpb_wrapper">
                            <h4 style="font-size: 14px; text-transform: uppercase">{{ trans('theme::frontend.footer.company_info') }}</h4>
                        </div>
                    </div>
                    <div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_40 vc_sep_pos_align_left vc_separator_no_text" >
                        <span class="vc_sep_holder vc_sep_holder_l">
                            <span  style="border-color:#eaeaea;" class="vc_sep_line"></span>
                        </span>
                        <span class="vc_sep_holder vc_sep_holder_r">
                            <span  style="border-color:#eaeaea;" class="vc_sep_line"></span>
                        </span>
                    </div>
                    <div  class="vc_wp_custommenu wpb_content_element">
                        <div class="widget widget_nav_menu">
                            <div class="menu-footer-new-container">
                                <ul id="menu-footer-new" class="menu">
                                    @foreach($companyInfo as $key => $value)
                                        @foreach(config('theme.settings_site') as $config)
                                            @if($config['key'] === $key)
                                                <li id="menu-item-8049" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-8049">
                                                    <span>{{ $config['description'] }}: {{ $value }}</span>
                                                </li>
                                            @endif
                                        @endforeach
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wpb_column column_container col-md-3">
                <div class="vc_column-inner wpb_wrapper">
                    <div class="wpb_text_column wpb_content_element  vc_custom_1543220015251" >
                        <div class="wpb_wrapper">
                            <h4 style="font-size: 14px; text-transform: uppercase">{{ trans('theme::frontend.footer.company_general') }}</h4>
                        </div>
                    </div>
                    <div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_40 vc_sep_pos_align_left vc_separator_no_text" >
                        <span class="vc_sep_holder vc_sep_holder_l">
                            <span  style="border-color:#eaeaea;" class="vc_sep_line"></span>
                        </span>
                        <span class="vc_sep_holder vc_sep_holder_r">
                            <span  style="border-color:#eaeaea;" class="vc_sep_line"></span>
                        </span>
                    </div>
                    <div  class="vc_wp_custommenu wpb_content_element">
                        <div class="widget widget_nav_menu">
                            <div class="menu-footer-new-container">
                                <ul id="menu-footer-new" class="menu">
                                    @foreach($generalInfo as $key => $value)
                                        @foreach(config('theme.settings_site') as $config)
                                            @if($config['key'] === $key)
                                                <li id="menu-item-8049" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-8049">
                                                    <span>{{ $config['description'] }}: {{ $value }}</span>
                                                </li>
                                            @endif
                                        @endforeach
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wpb_column column_container col-md-3">
                <div class="vc_column-inner wpb_wrapper">
                    <div class="wpb_text_column wpb_content_element  vc_custom_1543220022219" >
                        <div class="wpb_wrapper">
                            <h4 style="font-size: 14px; text-transform: uppercase">{{ trans('theme::frontend.footer.support_customer') }}</h4>
                        </div>
                    </div>
                    <div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_40 vc_sep_pos_align_left vc_separator_no_text" >
                        <span class="vc_sep_holder vc_sep_holder_l"><span  style="border-color:#eaeaea;" class="vc_sep_line"></span></span><span class="vc_sep_holder vc_sep_holder_r"><span  style="border-color:#eaeaea;" class="vc_sep_line"></span></span>
                    </div>
                    <div  class="vc_wp_custommenu wpb_content_element">
                        <div class="widget widget_nav_menu">
                            <div class="menu-footer-new-2-container">
                                <ul id="menu-footer-new-2" class="menu">
                                    @foreach($botMenus as $itemMenu)
                                        <li id="menu-item-8049" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-8049">
                                            <a href="{{ url($itemMenu->slug) }}.html"><span>{{ $itemMenu->title }}</span></a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--End .row-->
    </div><!--End .container-->
</div>
<div class="vc_row-full-width vc_clearfix"></div>
