<div id="topbar">
    <div class="topbar-left">
        <ul class="st-list socials">
            <li>
                @if(!empty($socialInfo['facebook_link']))
                <a href="{{ $socialInfo['facebook_link'] }}" target="_blank">
                    <i class="fa fa-facebook"></i>
                </a>
                @endif
                @if(!empty($socialInfo['twitter_link']))
                <a href="{{ $socialInfo['twitter_link'] }}" target="_blank">
                    <i class="fa fa-twitter"></i>
                </a>
                @endif
                @if(!empty($socialInfo['linkedin_link']))
                <a href="{{ $socialInfo['linkedin_link'] }}" target="_blank">
                    <i class="fa fa-linkedin"></i>
                </a>
                @endif
            </li>
        </ul>
        <ul class="st-list topbar-items">
            @if(!empty($companyInfo['phone']))
            <li class="hidden-xs hidden-sm">
                <a href="tel:{{ $companyInfo['phone'] }}" target="_blank">{{ $companyInfo['phone'] }}</a>
            </li>
            @endif
            @if(!empty($companyInfo['email_receive_message']))
            <li class="hidden-xs hidden-sm">
                <a href="mailto:{{ $companyInfo['email_receive_message'] }}" target="_blank">
                    <span class="__cf_email__">{{ $companyInfo['email_receive_message'] }}</span>
                </a>
            </li>
            @endif
        </ul>
    </div>
    <div class="topbar-right">
        <ul class="st-list topbar-items">
            @if(Auth::check())
                <li class="topbar-item login-item">
                    <a href="{{ url('home') }}" class="login">{{ trans('theme::frontend.users.hello') }}, {{ \Auth::user()->name }}</a>
                </li>
                <li class="topbar-item login-item">
                    <a href="{{ url('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="login">{{ trans('theme::frontend.users.logout') }}</a>
                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                        <input type="submit" value="{{ trans('theme::frontend.users.logout') }}" style="display: none;">
                    </form>
                </li>
            @else
                <div class="modal fade" id="st-forgot-form" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document" style="max-width: 450px;">
                        <div class="modal-content">
                            <div class="loader-wrapper">
                                <div class="st-loader"></div>
                            </div>
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <i class="input-icon field-icon fa">
                                        <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                            <!-- Generator: Sketch 49 (51002) - http://www.bohemiancoding.com/sketch -->
                                            <desc>Created with Sketch.</desc>
                                            <defs></defs>
                                            <g id="Ico_close" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                                                <g id="Group" stroke="#1A2B48" stroke-width="1.5">
                                                    <g id="close">
                                                        <path d="M0.75,23.249 L23.25,0.749" id="Shape"></path>
                                                        <path d="M23.25,23.249 L0.75,0.749" id="Shape"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </svg>
                                    </i>
                                </button>
                                <h4 class="modal-title">{{ trans('theme::frontend.users.get_password') }}</h4>
                            </div>
                            <div class="modal-body">
                                <form action="{{ url('/') }}" class="form" method="post">
                                    <p class="c-grey f14">
                                        Enter the e-mail address associated with the account.                            <br/>
                                        We'll e-mail a link to reset your password.</p>
                                    <div class="form-group">
                                        <input type="email" class="form-control" name="email"
                                               placeholder="Email">
                                        <i class="input-icon field-icon fa">
                                            <svg width="24px" height="24px" viewBox="0 0 24 19" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                <!-- Generator: Sketch 49 (51002) - http://www.bohemiancoding.com/sketch -->
                                                <desc>Created with Sketch.</desc>
                                                <defs></defs>
                                                <g id="Hotel-layout" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                                                    <g id="Log-In" transform="translate(-912.000000, -220.000000)" stroke="#A0A9B2">
                                                        <g id="login" transform="translate(466.000000, 80.000000)">
                                                            <g id="input" transform="translate(30.000000, 119.000000)">
                                                                <g id="Group" transform="translate(416.000000, 22.000000)">
                                                                    <g id="ico_email_login_form">
                                                                        <rect id="Rectangle-path" x="0.5" y="0.0101176471" width="23" height="16.9411765" rx="2"></rect>
                                                                        <polyline id="Shape" points="22.911 0.626352941 12 10.0689412 1.089 0.626352941"></polyline>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" name="submit" class="form-submit"
                                               value="{{ trans('theme::frontend.users.send') }}">
                                    </div>
                                    <div class="message-wrapper mt20"></div>
                                    <div class="text-center mt20">
                                        <a href="" class="st-link font-medium open-login"
                                           data-toggle="modal">{{ trans('theme::frontend.users.back_login') }}</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="st-login-form" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document" style="max-width: 450px;">
                        <div class="modal-content relative">
                            <div class="loader-wrapper">
                                <div class="st-loader"></div>
                            </div>
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <i class="input-icon field-icon fa">
                                        <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                            <!-- Generator: Sketch 49 (51002) - http://www.bohemiancoding.com/sketch -->
                                            <desc>Created with Sketch.</desc>
                                            <defs></defs>
                                            <g id="Ico_close" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                                                <g id="Group" stroke="#1A2B48" stroke-width="1.5">
                                                    <g id="close">
                                                        <path d="M0.75,23.249 L23.25,0.749" id="Shape"></path>
                                                        <path d="M23.25,23.249 L0.75,0.749" id="Shape"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </svg>
                                    </i>
                                </button>
                                <h4 class="modal-title">{{ trans('theme::frontend.users.login') }}</h4>
                            </div>
                            <div class="modal-body relative">
                                <form action="{{ url('login') }}" id="hs-modal__login" class="form" method="post">
                                    @csrf
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="username" required autocomplete="off"
                                               placeholder="{{ trans('theme::frontend.users.account_name') }}">
                                        <i class="input-icon field-icon fa">
                                            <svg width="18px" viewBox="0 0 24 19" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                <!-- Generator: Sketch 49 (51002) - http://www.bohemiancoding.com/sketch -->
                                                <desc>Created with Sketch.</desc>
                                                <defs></defs>
                                                <g id="Hotel-layout" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                                                    <g id="Log-In" transform="translate(-912.000000, -220.000000)" stroke="#A0A9B2">
                                                        <g id="login" transform="translate(466.000000, 80.000000)">
                                                            <g id="input" transform="translate(30.000000, 119.000000)">
                                                                <g id="Group" transform="translate(416.000000, 22.000000)">
                                                                    <g id="ico_email_login_form">
                                                                        <rect id="Rectangle-path" x="0.5" y="0.0101176471" width="23" height="16.9411765" rx="2"></rect>
                                                                        <polyline id="Shape" points="22.911 0.626352941 12 10.0689412 1.089 0.626352941"></polyline>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" name="password" required autocomplete="off"
                                               placeholder="{{ trans('theme::frontend.users.password') }}">
                                        <i class="input-icon field-icon fa">
                                            <svg width="16px" viewBox="0 0 18 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                <!-- Generator: Sketch 49 (51002) - http://www.bohemiancoding.com/sketch -->
                                                <desc>Created with Sketch.</desc>
                                                <defs></defs>
                                                <g id="Hotel-layout" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                                                    <g id="Log-In" transform="translate(-918.000000, -307.000000)" stroke="#A0A9B2">
                                                        <g id="login" transform="translate(466.000000, 80.000000)">
                                                            <g id="input" transform="translate(30.000000, 209.000000)">
                                                                <g id="Group" transform="translate(422.000000, 18.000000)">
                                                                    <g id="ico_pass_login_form">
                                                                        <path d="M3.5,6 C3.5,2.96243388 5.96243388,0.5 9,0.5 C12.0375661,0.5 14.5,2.96243388 14.5,6 L14.5,9.5" id="Shape"></path>
                                                                        <path d="M17.5,11.5 C17.5,10.3954305 16.6045695,9.5 15.5,9.5 L2.5,9.5 C1.3954305,9.5 0.5,10.3954305 0.5,11.5 L0.5,21.5 C0.5,22.6045695 1.3954305,23.5 2.5,23.5 L15.5,23.5 C16.6045695,23.5 17.5,22.6045695 17.5,21.5 L17.5,11.5 Z" id="Shape"></path>
                                                                        <circle id="Oval" cx="9" cy="16.5" r="1.25"></circle>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="form-group" id="form-error"></div>
                                    <div class="form-group">
                                        <input type="submit" name="submit" class="form-submit"
                                               value="{{ trans('theme::frontend.users.login') }}">
                                    </div>
                                    <div class="message-wrapper mt20"></div>
                                    <div class="mt20 st-flex space-between st-icheck">
                                        <div class="st-icheck-item">
                                            <label for="remember-me" class="c-grey">
                                                <input type="checkbox" name="remember" id="remember-me"
                                                       value="1"> {{ trans('theme::frontend.users.remember') }}
                                                <span class="checkmark fcheckbox"></span>
                                            </label>
                                        </div>
                                        <a href="" class="st-link open-loss-password"
                                           data-toggle="modal">{{ trans('theme::frontend.users.forget_password') }}</a>
                                    </div>
                                    <div class="mt20 c-grey font-medium f14 text-center">
                                        {{ trans('theme::frontend.users.not_account') }}
                                        <a href=""
                                           class="st-link open-signup"
                                           data-toggle="modal">{{ trans('theme::frontend.users.register') }}</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <li class="topbar-item login-item">
                    <a href="" class="login" data-toggle="modal"
                       data-target="#st-login-form">{{ trans('theme::frontend.users.login') }}</a>
                </li>
                <div class="modal fade" id="st-register-form" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document" style="max-width: 520px;">
                        <div class="modal-content relative">
                            <div class="loader-wrapper">
                                <div class="st-loader"></div>
                            </div>
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <i class="input-icon field-icon fa"><svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                            <!-- Generator: Sketch 49 (51002) - http://www.bohemiancoding.com/sketch -->
                                            <desc>Created with Sketch.</desc>
                                            <defs></defs>
                                            <g id="Ico_close" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                                                <g id="Group" stroke="#1A2B48" stroke-width="1.5">
                                                    <g id="close">
                                                        <path d="M0.75,23.249 L23.25,0.749" id="Shape"></path>
                                                        <path d="M23.25,23.249 L0.75,0.749" id="Shape"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </svg>
                                    </i>
                                </button>
                                <h4 class="modal-title">{{ trans('theme::frontend.users.register') }}</h4>
                            </div>
                            <div class="modal-body">
                                <form action="" class="form" method="post">
                                    <div class="form-group">
                                        <input type="text" class="form-control" required name="username" autocomplete="off"
                                               placeholder="{{ trans('theme::frontend.users.account_name') }} *">
                                        <i class="input-icon field-icon fa"><svg width="20px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                <!-- Generator: Sketch 49 (51002) - http://www.bohemiancoding.com/sketch -->
                                                <desc>Created with Sketch.</desc>
                                                <defs></defs>
                                                <g id="Hotel-layout" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                                                    <g id="Sign-Up" transform="translate(-912.000000, -207.000000)" stroke="#A0A9B2">
                                                        <g id="sign-up" transform="translate(466.000000, 80.000000)">
                                                            <g id="input" transform="translate(30.000000, 109.000000)">
                                                                <g id="ico_username_form_signup" transform="translate(416.000000, 18.000000)">
                                                                    <g id="Light">
                                                                        <circle id="Oval" cx="12" cy="12" r="11.5"></circle>
                                                                        <path d="M8.338,6.592 C10.3777315,8.7056567 13.5128387,9.33602311 16.211,8.175" id="Shape"></path>
                                                                        <circle id="Oval" cx="12" cy="8.75" r="4.25"></circle>
                                                                        <path d="M18.317,18.5 C17.1617209,16.0575309 14.7019114,14.4999182 12,14.4999182 C9.29808863,14.4999182 6.83827906,16.0575309 5.683,18.5" id="Shape"></path>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" required name="name" autocomplete="off"
                                               placeholder="{{ trans('theme::frontend.users.full_name') }} *">
                                        <i class="input-icon field-icon fa"><svg width="20px" viewBox="0 0 23 25" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                <!-- Generator: Sketch 49 (51002) - http://www.bohemiancoding.com/sketch -->
                                                <desc>Created with Sketch.</desc>
                                                <defs></defs>
                                                <g id="Hotel-layout" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                                                    <g id="Sign-Up" transform="translate(-914.000000, -295.000000)" stroke="#A0A9B2">
                                                        <g id="sign-up" transform="translate(466.000000, 80.000000)">
                                                            <g id="input" transform="translate(30.000000, 199.000000)">
                                                                <g id="Group" transform="translate(418.000000, 17.000000)">
                                                                    <g id="ico_fullname_signup">
                                                                        <path d="M14.5,23.5 L14.5,20.5 L15.5,20.5 C17.1568542,20.5 18.5,19.1568542 18.5,17.5 L18.5,14.5 L21.313,14.5 C21.4719994,14.4989403 21.6210158,14.4223193 21.7143842,14.2936169 C21.8077526,14.1649146 21.8343404,13.9994766 21.786,13.848 C19.912,8.048 18.555,1.813 12.366,0.681 C9.63567371,0.151893606 6.80836955,0.784892205 4.56430871,2.42770265 C2.32024786,4.07051309 0.862578016,6.57441697 0.542,9.337 C0.21983037,12.7556062 1.72416582,16.0907612 4.5,18.112 L4.5,23.5" id="Shape"></path>
                                                                        <path d="M7.5,8 C7.49875423,6.44186837 8.69053402,5.14214837 10.2429354,5.00863533 C11.7953368,4.87512228 13.1915367,5.95226513 13.4563532,7.48772858 C13.7211696,9.02319203 12.7664402,10.5057921 11.259,10.9 C10.8242888,10.9952282 10.5108832,11.3751137 10.5,11.82 L10.5,13.5" id="Shape"></path>
                                                                        <path d="M10.5,15.5 C10.6380712,15.5 10.75,15.6119288 10.75,15.75 C10.75,15.8880712 10.6380712,16 10.5,16 C10.3619288,16 10.25,15.8880712 10.25,15.75 C10.25,15.6119288 10.3619288,15.5 10.5,15.5" id="Shape"></path>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="form-group">
                                        <input type="email" class="form-control" required name="email" autocomplete="off"
                                               placeholder="Email *">
                                        <i class="input-icon field-icon fa"><svg width="18px" viewBox="0 0 24 19" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                <!-- Generator: Sketch 49 (51002) - http://www.bohemiancoding.com/sketch -->
                                                <desc>Created with Sketch.</desc>
                                                <defs></defs>
                                                <g id="Hotel-layout" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                                                    <g id="Log-In" transform="translate(-912.000000, -220.000000)" stroke="#A0A9B2">
                                                        <g id="login" transform="translate(466.000000, 80.000000)">
                                                            <g id="input" transform="translate(30.000000, 119.000000)">
                                                                <g id="Group" transform="translate(416.000000, 22.000000)">
                                                                    <g id="ico_email_login_form">
                                                                        <rect id="Rectangle-path" x="0.5" y="0.0101176471" width="23" height="16.9411765" rx="2"></rect>
                                                                        <polyline id="Shape" points="22.911 0.626352941 12 10.0689412 1.089 0.626352941"></polyline>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" required name="password" autocomplete="off"
                                               placeholder="{{ trans('theme::frontend.users.password') }} *">
                                        <i class="input-icon field-icon fa"><svg width="16px" viewBox="0 0 18 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                <!-- Generator: Sketch 49 (51002) - http://www.bohemiancoding.com/sketch -->
                                                <desc>Created with Sketch.</desc>
                                                <defs></defs>
                                                <g id="Hotel-layout" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                                                    <g id="Log-In" transform="translate(-918.000000, -307.000000)" stroke="#A0A9B2">
                                                        <g id="login" transform="translate(466.000000, 80.000000)">
                                                            <g id="input" transform="translate(30.000000, 209.000000)">
                                                                <g id="Group" transform="translate(422.000000, 18.000000)">
                                                                    <g id="ico_pass_login_form">
                                                                        <path d="M3.5,6 C3.5,2.96243388 5.96243388,0.5 9,0.5 C12.0375661,0.5 14.5,2.96243388 14.5,6 L14.5,9.5" id="Shape"></path>
                                                                        <path d="M17.5,11.5 C17.5,10.3954305 16.6045695,9.5 15.5,9.5 L2.5,9.5 C1.3954305,9.5 0.5,10.3954305 0.5,11.5 L0.5,21.5 C0.5,22.6045695 1.3954305,23.5 2.5,23.5 L15.5,23.5 C16.6045695,23.5 17.5,22.6045695 17.5,21.5 L17.5,11.5 Z" id="Shape"></path>
                                                                        <circle id="Oval" cx="9" cy="16.5" r="1.25"></circle>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" required name="password_confirmation" autocomplete="off"
                                               placeholder="{{ trans('theme::frontend.users.repeat_password') }} *">
                                        <i class="input-icon field-icon fa">
                                            <svg width="16px" viewBox="0 0 18 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                <!-- Generator: Sketch 49 (51002) - http://www.bohemiancoding.com/sketch -->
                                                <desc>Created with Sketch.</desc>
                                                <defs></defs>
                                                <g id="Hotel-layout" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                                                    <g id="Log-In" transform="translate(-918.000000, -307.000000)" stroke="#A0A9B2">
                                                        <g id="login" transform="translate(466.000000, 80.000000)">
                                                            <g id="input" transform="translate(30.000000, 209.000000)">
                                                                <g id="Group" transform="translate(422.000000, 18.000000)">
                                                                    <g id="ico_pass_login_form">
                                                                        <path d="M3.5,6 C3.5,2.96243388 5.96243388,0.5 9,0.5 C12.0375661,0.5 14.5,2.96243388 14.5,6 L14.5,9.5" id="Shape"></path>
                                                                        <path d="M17.5,11.5 C17.5,10.3954305 16.6045695,9.5 15.5,9.5 L2.5,9.5 C1.3954305,9.5 0.5,10.3954305 0.5,11.5 L0.5,21.5 C0.5,22.6045695 1.3954305,23.5 2.5,23.5 L15.5,23.5 C16.6045695,23.5 17.5,22.6045695 17.5,21.5 L17.5,11.5 Z" id="Shape"></path>
                                                                        <circle id="Oval" cx="9" cy="16.5" r="1.25"></circle>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="form-group st-icheck-item">
                                        <label for="term">
                                            <input id="term" type="checkbox" name="term"
                                                   class="mr5"> Tôi đã đọc và đồng ý với <a class="st-link">các điều khoản</a><span class="checkmark fcheckbox"></span>
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" name="submit" class="form-submit"
                                               value="Sign Up">
                                    </div>
                                    <div class="message-wrapper mt20"></div>

                                    <div class="mt20 c-grey f14 text-center">
                                        {{ trans('theme::frontend.users.accounted') }}
                                        <a href="" class="st-link open-login"
                                           data-toggle="modal">{{ trans('theme::frontend.users.login') }}</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <li class="topbar-item signup-item">
                    <a href="" class="signup" data-toggle="modal"
                       data-target="#st-register-form">{{ trans('theme::frontend.users.register') }}</a>
                </li>
            @endif
        </ul>
    </div>
</div>