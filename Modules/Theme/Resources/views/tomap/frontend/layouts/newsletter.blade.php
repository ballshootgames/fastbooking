<div class="mailchimp">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-lg-10 col-lg-offset-1">
                <div class="row">
                    <div class="col-xs-12  col-md-7 col-lg-6">
                        <div class="media ">
                            <div class="media-left pr30 hidden-xs">
                                <img class="media-object"
                                     src="{{ asset('img/ico_email_subscribe.svg') }}"
                                     alt="">
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading st-heading-section f24">{{ trans('theme::frontend.newsletter.notification') }}</h4>
                                <p class="f16 c-grey">{{ trans('theme::frontend.newsletter.register_box') }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-5 col-lg-6">
                        <form action="" class="subcribe-form" id="newsletter">
                            @csrf
                            <div class="form-group">
                                <input type="text" class="form-control" name="newsletter_email" id="newsletter_email" placeholder="Email của bạn" />
                                <input type="submit" name="submit" value="Đăng ký" />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>