@include('theme::tomap.frontend.layouts.topbar')
<div class="header">
    <a href="#" class="toggle-menu">
        <i class="input-icon field-icon fa">
            <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <!-- Generator: Sketch 49 (51002) - http://www.bohemiancoding.com/sketch -->
                <desc>Created with Sketch.</desc>
                <defs></defs>
                <g id="Ico_off_menu" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                    <g id="Group" stroke="#1A2B48" stroke-width="1.5">
                        <g id="navigation-menu-4">
                            <rect id="Rectangle-path" x="0.75" y="0.753" width="22.5" height="22.5" rx="1.5"></rect>
                            <path d="M6.75,7.503 L17.25,7.503" id="Shape"></path>
                            <path d="M6.75,12.003 L17.25,12.003" id="Shape"></path>
                            <path d="M6.75,16.503 L17.25,16.503" id="Shape"></path>
                        </g>
                    </g>
                </g>
            </svg>
        </i>
    </a>
    <div class="header-left">
        <a href="{{ url('/') }}" class="logo hidden-xs">
            <img src="{{ asset(Storage::url($settings['url_logo'])) }}" alt="{{ $settings['website_name'] }}" width="164px">
        </a>
        <a href="{{ url('/') }}" class="logo hidden-lg hidden-md hidden-sm">
            <img src="{{ asset(Storage::url($settings['url_logo'])) }}" alt="{{ $settings['website_name'] }}" width="164px">
        </a>
        <nav id="st-main-menu">
            <a href="" class="back-menu"><i class="fa fa-angle-left"></i></a>
            @php
                $menu = new \Modules\Theme\Entities\Menu();
                $menu->showMainMenu($mainMenus, Request::url());
            @endphp
        </nav>
    </div>
    {{--<div class="header-right">
        <ul class="st-list">
            <li class="dropdown dropdown-minicart">
                <div id="d-minicart" class="mini-cart" data-toggle="dropdown" aria-haspopup="true"
                     aria-expanded="false">
                    <i class="input-icon field-icon fa"><svg width="26px" height="26px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <!-- Generator: Sketch 49 (51002) - http://www.bohemiancoding.com/sketch -->
                            <desc>Created with Sketch.</desc>
                            <defs></defs>
                            <g id="ico_card" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                                <g id="Group" transform="translate(1.000000, 0.000000)" stroke="#1A2B48" stroke-width="1.5">
                                    <g id="shopping-basket-handle">
                                        <path d="M17.936,23.25 L4.064,23.25 C3.39535169,23.2378444 2.82280366,22.7675519 2.681,22.114 L0.543,13.114 C0.427046764,12.67736 0.516308028,12.2116791 0.785500181,11.8488633 C1.05469233,11.4860476 1.47449596,11.2656135 1.926,11.25 L20.074,11.25 C20.525504,11.2656135 20.9453077,11.4860476 21.2144998,11.8488633 C21.483692,12.2116791 21.5729532,12.67736 21.457,13.114 L19.319,22.114 C19.1771963,22.7675519 18.6046483,23.2378444 17.936,23.25 Z" id="Shape"></path>
                                        <path d="M6.5,14.25 L6.5,20.25" id="Shape"></path>
                                        <path d="M11,14.25 L11,20.25" id="Shape"></path>
                                        <path d="M15.5,14.25 L15.5,20.25" id="Shape"></path>
                                        <path d="M8,2.006 C5.190705,2.90246789 3.1556158,5.34590097 2.782,8.271" id="Shape"></path>
                                        <path d="M19.221,8.309 C18.8621965,5.36812943 16.822685,2.90594951 14,2.006" id="Shape"></path>
                                        <rect id="Rectangle-path" x="8" y="0.75" width="6" height="3" rx="1.125"></rect>
                                    </g>
                                </g>
                            </g>
                        </svg></i>        </div>
                <ul class="dropdown-menu" aria-labelledby="d-minicart">
                    <li class="heading">
                        <h4 class="st-heading-section">Your Cart</h4>
                    </li>
                    <div class="col-lg-12 cart-text-empty text-warning">Your cart is empty</div>
                </ul>
            </li>
        </ul>
    </div>--}}
</div>