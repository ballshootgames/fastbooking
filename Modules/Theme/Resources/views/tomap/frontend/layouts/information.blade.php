<div class="vc_row wpb_row st bg-holder vc_custom_1542955832597">
    <div class='container '>
        <div class='row'>
            <div class="wpb_column column_container col-md-4">
                <div class="vc_column-inner wpb_wrapper">
                    <div class="st-featured-item image-left">
                        <div class="image">
                            <img src="https://tomap.travelerwp.com/wp-content/uploads/2019/01/ico_localguide.svg" class="img-responsive">    </div>
                        <div class="content">
                            <h4 class="title">25,000+ Khách hàng</h4>
                            <div class="desc">Phục vụ hơn 25,000 khách hàng mỗi năm</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wpb_column column_container col-md-4"><div class="vc_column-inner wpb_wrapper">
                    <div class="st-featured-item image-left">
                        <div class="image">
                            <img src="https://tomap.travelerwp.com/wp-content/uploads/2019/01/ico_maps.svg" class="img-responsive">    </div>
                        <div class="content">
                            <h4 class="title">1,000+ Chương trình</h4>
                            <div class="desc">Hơn 1,000 chương trình tour hấp dẫn hoạt động liên tục</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wpb_column column_container col-md-4"><div class="vc_column-inner wpb_wrapper">
                    <div class="st-featured-item image-left">
                        <div class="image">
                            <img src="https://tomap.travelerwp.com/wp-content/uploads/2019/01/ico_adventurous.svg" class="img-responsive">    </div>
                        <div class="content">
                            <h4 class="title">96% Phản hồi hài lòng</h4>
                            <div class="desc">Hơn 96% khách hàng hài lòng với chất lượng dịch vụ</div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--End .row-->
    </div><!--End .container-->
</div>