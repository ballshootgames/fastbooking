<div class="vc_row wpb_row st bg-holder">
    <div class='container '>
        <div class='row'>
            <div class="wpb_column column_container col-md-12 vc_custom_1547112475891">
                <div class="vc_column-inner wpb_wrapper">
                    <div class="vc_empty_space"   style="height: 32px" ><span class="vc_empty_space_inner"></span></div>
                    <div class="vc_row wpb_row vc_inner vc_custom_1547204285551 vc_row-has-fill">
                        <div class="wpb_column column_container col-md-6">
                            <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                    <div class="wpb_text_column wpb_content_element  st-become-local" >
                                        <div class="wpb_wrapper">
                                            <h2><span class="f48">Know your city?</span></h2>
                                            <p><span class="f18 lh18">Join 2000+ locals & 1200+ contributors from 3000 cities</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wpb_column column_container col-md-6">
                            <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                    <div class="vc_btn3-container  st-become-local-btn vc_btn3-right" >
                                        <button class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-modern vc_btn3-color-white">Become Local Expert</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="vc_empty_space"   style="height: 32px" ><span class="vc_empty_space_inner"></span></div>
                </div>
            </div>
        </div><!--End .row-->
    </div><!--End .container-->
</div>