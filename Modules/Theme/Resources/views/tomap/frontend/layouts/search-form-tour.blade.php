<div class='container'>
    <div class='row'>
        <div class="wpb_column column_container col-md-12">
            <div class="vc_column-inner wpb_wrapper">
                <div class="search-form-wrapper normal  st-search-form-st_tours">
                    {{--<h1 class="st-heading text-center">Love where you're going</h1>--}}
                    {{--<div class="sub-heading text-center">Book incredible things to do around the world.</div>--}}
                    <div class="row">
                        <div class="col-lg-9 tour-search-form-home">
                            <div class="search-form" id=&quot;sticky-nav&quot;>
                                <form action="{{ url('tour') }}" class="form" method="get" id="tour-search">
                                    <div class="row">
                                        <div class="col-md-4 border-right">
                                            <div class="form-group form-extra-field dropdown clearfix field-detination has-icon" id="tour_places">
                                                <i class="input-icon field-icon fa">
                                                    <svg width="24px" height="24px" viewBox="0 0 17 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                        <!-- Generator: Sketch 49 (51002) - http://www.bohemiancoding.com/sketch -->
                                                        <desc>Created with Sketch.</desc>
                                                        <defs></defs>
                                                        <g id="Hotel-layout" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                                                            <g id="Search_Result_1_Grid" transform="translate(-165.000000, -328.000000)" stroke="#A0A9B2">
                                                                <g id="form_search_hotel_row" transform="translate(135.000000, 290.000000)">
                                                                    <g id="input" transform="translate(30.000000, 0.000000)">
                                                                        <g id="where" transform="translate(0.000000, 26.000000)">
                                                                            <g id="Group" transform="translate(0.000000, 12.000000)">
                                                                                <g id="ico_maps_search_box">
                                                                                    <path d="M15.75,8.25 C15.75,12.471 12.817,14.899 10.619,17.25 C9.303,18.658 8.25,23.25 8.25,23.25 C8.25,23.25 7.2,18.661 5.887,17.257 C3.687,14.907 0.75,12.475 0.75,8.25 C0.75,4.10786438 4.10786438,0.75 8.25,0.75 C12.3921356,0.75 15.75,4.10786438 15.75,8.25 Z" id="Shape"></path>
                                                                                    <circle id="Oval" cx="8.25" cy="8.25" r="3"></circle>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </i>
                                                <div class="dropdown" data-toggle="dropdown" id="dropdown-destination">
                                                    <label>Địa điểm</label>
                                                    <div class="render">
                                                        <span class="destination">{{ Request::get('tour_place') ? \Modules\Tour\Entities\TourProperty::find(Request::get('tour_place'))->name : 'Bạn muốn đến đâu?' }}</span>
                                                    </div>
                                                    <input type="hidden" name="tour_place" value="{{ Request::get('tour_place') ?? '' }}"/>
                                                </div>
                                                <ul class="dropdown-menu" aria-labelledby="dropdown-destination">
                                                    @foreach(\Modules\Tour\Entities\TourProperty::where('type', 'tour_place')->pluck('name', 'id') as $id => $name)
                                                        <li style="padding-left: 20px;" data-value="{{ $id }}"
                                                            class="item" {{ Request::get('tour_place') == $id ? "selected" : "" }}>
                                                            <i class="input-icon field-icon fa">
                                                                <svg width="16px" height="16px" viewBox="0 0 17 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                                    <!-- Generator: Sketch 49 (51002) - http://www.bohemiancoding.com/sketch -->
                                                                    <desc>Created with Sketch.</desc>
                                                                    <defs></defs>
                                                                    <g id="Hotel-layout" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                                                                        <g id="Search_Result_1_Grid" transform="translate(-165.000000, -328.000000)" stroke="gray">
                                                                            <g id="form_search_hotel_row" transform="translate(135.000000, 290.000000)">
                                                                                <g id="input" transform="translate(30.000000, 0.000000)">
                                                                                    <g id="where" transform="translate(0.000000, 26.000000)">
                                                                                        <g id="Group" transform="translate(0.000000, 12.000000)">
                                                                                            <g id="ico_maps_search_box">
                                                                                                <path d="M15.75,8.25 C15.75,12.471 12.817,14.899 10.619,17.25 C9.303,18.658 8.25,23.25 8.25,23.25 C8.25,23.25 7.2,18.661 5.887,17.257 C3.687,14.907 0.75,12.475 0.75,8.25 C0.75,4.10786438 4.10786438,0.75 8.25,0.75 C12.3921356,0.75 15.75,4.10786438 15.75,8.25 Z" id="Shape"></path>
                                                                                                <circle id="Oval" cx="8.25" cy="8.25" r="3"></circle>
                                                                                            </g>
                                                                                        </g>
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                    </g>
                                                                </svg>
                                                            </i>
                                                            <span class="lv2">{{ $name }}</span>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-4 border-right">
                                            <div class="form-group form-extra-field dropdown clearfix field-detination field-category has-icon">
                                                <i class="input-icon field-icon fa">
                                                    <svg xmlns="http://www.w3.org/2000/svg" style="width: 24px; height: 24px" aria-hidden="true" focusable="false" data-prefix="far" data-icon="list-alt" class="svg-inline--fa fa-list-alt fa-w-16" role="img" viewBox="0 0 512 512"><path fill="currentColor" d="M464 32H48C21.49 32 0 53.49 0 80v352c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48V80c0-26.51-21.49-48-48-48zm-6 400H54a6 6 0 0 1-6-6V86a6 6 0 0 1 6-6h404a6 6 0 0 1 6 6v340a6 6 0 0 1-6 6zm-42-92v24c0 6.627-5.373 12-12 12H204c-6.627 0-12-5.373-12-12v-24c0-6.627 5.373-12 12-12h200c6.627 0 12 5.373 12 12zm0-96v24c0 6.627-5.373 12-12 12H204c-6.627 0-12-5.373-12-12v-24c0-6.627 5.373-12 12-12h200c6.627 0 12 5.373 12 12zm0-96v24c0 6.627-5.373 12-12 12H204c-6.627 0-12-5.373-12-12v-24c0-6.627 5.373-12 12-12h200c6.627 0 12 5.373 12 12zm-252 12c0 19.882-16.118 36-36 36s-36-16.118-36-36 16.118-36 36-36 36 16.118 36 36zm0 96c0 19.882-16.118 36-36 36s-36-16.118-36-36 16.118-36 36-36 36 16.118 36 36zm0 96c0 19.882-16.118 36-36 36s-36-16.118-36-36 16.118-36 36-36 36 16.118 36 36z"/></svg>
                                                </i>
                                                <div class="dropdown" data-toggle="dropdown" id="dropdown-category">
                                                    <label><option value="">{{ trans('tour::tour_schedules.schedule_destinations') }}</option></label>
                                                    <div class="render">
                                                        <span class="category">{{ Request::get('tour_cities') ? \Modules\Tour\Entities\TourProperty::find(Request::get('tour_cities'))->name : 'Hãy chọn ' . trans('tour::tour_schedules.schedule_destinations') }}</span>
                                                    </div>
                                                    <input type="hidden" name="tour_cities" value="{{ Request::get('tour_cities') ?? '' }}"/>
                                                </div>
                                                <ul class="dropdown-menu" aria-labelledby="dropdown-category" id="tour_cities">
                                                    @if(Request::get('tour_place'))
                                                        @foreach(\Modules\Tour\Entities\TourProperty::where(['type' => 'tour_place', 'parent_id' => Request::get('tour_place')])->pluck('name', 'id') as $id => $name)
                                                            <li style="padding-left: 20px;" data-value="{{ $id }}"
                                                                class="item" {{ Request::get('tour_cities') == $id ? "selected" : "" }}>
                                                                <i class="input-icon field-icon fa">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" style="width: 24px; height: 24px" aria-hidden="true" focusable="false" data-prefix="far" data-icon="list-alt" class="svg-inline--fa fa-list-alt fa-w-16" role="img" viewBox="0 0 512 512"><path fill="currentColor" d="M464 32H48C21.49 32 0 53.49 0 80v352c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48V80c0-26.51-21.49-48-48-48zm-6 400H54a6 6 0 0 1-6-6V86a6 6 0 0 1 6-6h404a6 6 0 0 1 6 6v340a6 6 0 0 1-6 6zm-42-92v24c0 6.627-5.373 12-12 12H204c-6.627 0-12-5.373-12-12v-24c0-6.627 5.373-12 12-12h200c6.627 0 12 5.373 12 12zm0-96v24c0 6.627-5.373 12-12 12H204c-6.627 0-12-5.373-12-12v-24c0-6.627 5.373-12 12-12h200c6.627 0 12 5.373 12 12zm0-96v24c0 6.627-5.373 12-12 12H204c-6.627 0-12-5.373-12-12v-24c0-6.627 5.373-12 12-12h200c6.627 0 12 5.373 12 12zm-252 12c0 19.882-16.118 36-36 36s-36-16.118-36-36 16.118-36 36-36 36 16.118 36 36zm0 96c0 19.882-16.118 36-36 36s-36-16.118-36-36 16.118-36 36-36 36 16.118 36 36zm0 96c0 19.882-16.118 36-36 36s-36-16.118-36-36 16.118-36 36-36 36 16.118 36 36z"/></svg>
                                                                </i>
                                                                <span class="lv2">{{ $name }}</span>
                                                            </li>
                                                        @endforeach
                                                    @endif
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-button">
                                                <div class="advance">
                                                    <div class="form-group form-extra-field dropdown clearfix field-advance">
                                                        <div class="dropdown" data-toggle="dropdown" id="dropdown-advance">
                                                            <label class="hidden-xs">Nâng cao</label>
                                                            <div class="render">
                                                                <span class="hidden-xs">Hơn nữa <i class="fa fa-caret-down"></i></span>
                                                                <span class="hidden-lg hidden-md hidden-sm">More option <i
                                                                            class="fa fa-caret-down"></i></span>
                                                            </div>
                                                        </div>
                                                        <div class="dropdown-menu" aria-labelledby="dropdown-advance">
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="advance-item search-tour-name">
                                                                        <div class="item-title">
                                                                            <h4>Tên Tour</h4>
                                                                        </div>
                                                                        <div class="item-content">
                                                                            <input type="text" value="{{\Request::get('name')}}"
                                                                                   name="name" class="form-control" placeholder="Tên tour...">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                {{--<div class="col-lg-12">--}}
                                                                    {{--<div class="advance-item range-slider">--}}
                                                                        {{--<div class="item-title">--}}
                                                                            {{--<h4>Filter Price</h4>--}}
                                                                        {{--</div>--}}
                                                                        {{--<div class="item-content">--}}
                                                                            {{--<input type="text" class="price_range" name="price_range"--}}
                                                                               {{--value="89;1650" data-symbol="€" data-min="89" data-max="1650" data-step="0"/>--}}
                                                                        {{--</div>--}}
                                                                    {{--</div>--}}
                                                                {{--</div>--}}
                                                                {{--<div class="col-lg-12">--}}
                                                                    {{--<div class="advance-item facilities st-icheck">--}}
                                                                        {{--<div class="item-title">--}}
                                                                            {{--<h4>Tour Categories</h4>--}}
                                                                        {{--</div>--}}
                                                                        {{--<div class="item-content">--}}
                                                                            {{--<div class="row">--}}
                                                                                {{--<div class="ovscroll" tabindex="1">--}}
                                                                                    {{--<div class="col-lg-4 col-sm-6">--}}
                                                                                        {{--<div class="st-icheck-item">--}}
                                                                                            {{--<label>City trips<input--}}
                                                                                                        {{--type="checkbox"--}}
                                                                                                        {{--name="" value="112" ><span--}}
                                                                                                        {{--class="checkmark fcheckbox"></span>--}}
                                                                                            {{--</label></div>--}}
                                                                                    {{--</div>--}}
                                                                                    {{--<div class="col-lg-4 col-sm-6">--}}
                                                                                        {{--<div class="st-icheck-item">--}}
                                                                                            {{--<label>Ecotourism<input--}}
                                                                                                        {{--type="checkbox"--}}
                                                                                                        {{--name="" value="113" ><span--}}
                                                                                                        {{--class="checkmark fcheckbox"></span>--}}
                                                                                            {{--</label></div>--}}
                                                                                    {{--</div>--}}
                                                                                    {{--<div class="col-lg-4 col-sm-6">--}}
                                                                                        {{--<div class="st-icheck-item">--}}
                                                                                            {{--<label>Escorted Tour<input--}}
                                                                                                        {{--type="checkbox"--}}
                                                                                                        {{--name="" value="114" ><span--}}
                                                                                                        {{--class="checkmark fcheckbox"></span>--}}
                                                                                            {{--</label></div>--}}
                                                                                    {{--</div>--}}
                                                                                    {{--<div class="col-lg-4 col-sm-6">--}}
                                                                                        {{--<div class="st-icheck-item">--}}
                                                                                            {{--<label>Group Tour<input--}}
                                                                                                        {{--type="checkbox"--}}
                                                                                                        {{--name="" value="115" ><span--}}
                                                                                                        {{--class="checkmark fcheckbox"></span>--}}
                                                                                            {{--</label></div>--}}
                                                                                    {{--</div>--}}
                                                                                    {{--<div class="col-lg-4 col-sm-6">--}}
                                                                                        {{--<div class="st-icheck-item">--}}
                                                                                            {{--<label>Hosted tours<input--}}
                                                                                                        {{--type="checkbox"--}}
                                                                                                        {{--name="" value="117" ><span--}}
                                                                                                        {{--class="checkmark fcheckbox"></span>--}}
                                                                                            {{--</label></div>--}}
                                                                                    {{--</div>--}}
                                                                                    {{--<div class="col-lg-4 col-sm-6">--}}
                                                                                        {{--<div class="st-icheck-item">--}}
                                                                                            {{--<label>Ligula<input--}}
                                                                                                        {{--type="checkbox"--}}
                                                                                                        {{--name="" value="116" ><span--}}
                                                                                                        {{--class="checkmark fcheckbox"></span>--}}
                                                                                            {{--</label></div>--}}
                                                                                    {{--</div>--}}
                                                                                {{--</div>--}}
                                                                            {{--</div>--}}
                                                                        {{--</div>--}}
                                                                        {{--<input type="hidden" class="data_taxonomy" name="taxonomy[st_tour_type]" value="">--}}
                                                                    {{--</div>--}}
                                                                {{--</div>--}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="tour_order" id="tour_order" value="{{ !empty(Request::get('tour_order')) ? Request::get('tour_order') : '' }}"/>
                                                <button class="btn btn-primary btn-search" type="submit">Tìm kiếm</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--End .row-->
</div><!--End .container-->