<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, maximum-scale=2, minimum-scale=1">
    <meta http-equiv="x-ua-compatible" content="IE=edge">
    <meta name="geo.position" content="Huế"/>
    <meta name="geo.region" content="VN"/>
    @section('title')
        <title>{{ $settings['seo_title'] }}</title>
        <META NAME="KEYWORDS" content="{{ $settings['seo_keywords'] }}"/>
        <META NAME="description" CONTENT="{{ $settings['seo_description'] }}" />
    @show
    <link rel="shortcut icon" href="{{ !empty($settings['favicon']) ? asset(Storage::url($settings['favicon'])) : url('favicon.ico') }}" />
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
        .carousel,.item,.active{height:100%;}
        .carousel-inner{height:100%;}
        .fill{width:100% !important;height:100%;background-position:center;background-size:cover;}
        .vc_image_slider .carousel .item img {
            width: 100%;
            object-fit: cover;
        }
    </style>
    <link rel='stylesheet' id='google-font-css-css'  href='https://fonts.googleapis.com/css?family=Poppins%3A400%2C500%2C600' type='text/css' media='all' />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans|Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ url(mix('css/web-tomap.css')) }}"/>
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="https://tomap.travelerwp.com/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen">
    <![endif]-->
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    @yield('style-css')
</head>
<body class="home page-template page-template-template-home-modern page-template-template-home-modern-php page page-id-7414  woocommerce-no-js wide menu_style1 topbar_position_default  enable_nice_scroll search_enable_preload wpb-js-composer js-comp-ver-5.7 vc_responsive">
<header id="header">
    @include('theme::tomap.frontend.layouts.header')
</header>
<div id="st-content-wrapper" class="search-result-page st-single-tour">
    @section('breadcrumb')
    @show
    @yield('content')
</div>
@include('theme::tomap.frontend.layouts.newsletter')
<footer id="main-footer" class="clearfix">
    @include('theme::tomap.frontend.layouts.footer')
</footer>
<div class="container main-footer-sub">
    <div class="st-flex space-between">
        <div class="left mt20">
            <div class="f14">Copyright © 2019 by <a href="{{ url('/') }}" class="st-link">{{ $settings['company_name'] }}</a></div>
        </div>
        <div class="right mt20">
            <img src="{{ asset('img/ico_paymethod.svg') }}" alt="" class="img-responsive">
        </div>
    </div>
</div>
<style id="stassets_footer_css"></style>
<script type="text/javascript" src="{{ url(mix('js/web-tomap.js')) }}"></script>
@yield('scripts')
<script type="text/javascript">
    $('.st-service-slider').each(function () {
        $(this).owlCarousel({
            loop: false,
            items: 4,
            margin: 20,
            responsiveClass: true,
            dots: true,
            responsive: {
                0: {
                    items: 2,
                    nav: false,
                    margin: 15,
                },
                992: {
                    items: 3,
                    nav: false,
                },
                1200: {
                    items: 4,
                    nav: false,
                }
            }
        });
    });
    $('.st-service-rental-slider').each(function () {
        $(this).owlCarousel({
            loop: false,
            items: 3,
            margin: 20,
            responsiveClass: true,
            dots: true,
            responsive: {
                0: {
                    items: 2,
                    nav: false,
                    margin: 15,
                },
                992: {
                    items: 3,
                    nav: false,
                }
            }
        });
    });
    $('.st-testimonial-slider').each(function () {
        $(this).owlCarousel({
            loop: false,
            items: 6,
            margin: 30,
            responsiveClass: true,
            dots: true,
            nav: false,
            responsive: {
                0: {
                    items: 1,
                    margin: 15,
                },
                575: {
                    items: 2,
                    margin: 15,
                },
                992: {
                    items: 3,
                },
                1200: {
                    items: 3,
                }
            }
        });
    });
    $('.owl-tour-program').each(function () {
        var parent = $(this).parent();
        var owl = $(this);
        owl.owlCarousel({
            loop: false,
            items: 3,
            margin: 20,
            responsiveClass: true,
            dots: false,
            nav: false,
            responsive: {
                0: {
                    items: 1,
                    margin: 15,
                },
                992: {
                    items: 2,
                },
                1200: {
                    items: 3,
                }
            }
        });
        $('.next', parent).click(function (ev) {
            ev.preventDefault();
            owl.trigger('next.owl.carousel');
        });
        $('.prev', parent).click(function (ev) {
            ev.preventDefault();
            owl.trigger('prev.owl.carousel');
        });
        owl.on('resized.owl.carousel', function () {
            setTimeout(function () {
                if ($('.ovscroll').length) {
                    $('.ovscroll').getNiceScroll().resize();
                }
            }, 1000);
        });
    });

    (function ($) {
        $('#hs-modal__login').submit(function (e) {
            e.preventDefault();
            $('#st-login-form .loader-wrapper').css('display', 'block');
            $.ajax({
                url: '{{ url('login-user/ajax') }}',
                type: 'POST',
                dataType: 'JSON',
                data: $(this).serialize(),
                success: function(res){
                    if (res.success === 'ok') {
                        window.location.href = '{{url('/home')}}';
                    }else{
                        $('#st-login-form .loader-wrapper').css('display', 'none');
                        let errorsHtml = '<div class="alert alert-danger" style="text-align: center">';
                        errorsHtml += '<li style="padding-left: 0">' + res.error + '</li>';
                        errorsHtml += '</div>';
                        $('#form-error').html(errorsHtml);
                    }
                },
                error: function (res) {
                    $('#st-login-form .loader-wrapper').css('display', 'none');
                    let errors = res.responseJSON.errors;
                    $.each(errors, function (key, value) {
                        $('#' + key).html(value);
                    })
                }
            });
        });
        $('.field-category').on('click', 'li', function () {
            $('input[name="tour_cities"]').attr('value', $(this).data('value'));
            $('.category').text($(this).find('.lv2').text());
            $(this).parent().css('display','none');
        });
        $('#tour_places').on('click', 'li', function(){
            let idPlace = $(this).data('value');
            $("#tour_cities").html('');
            $.ajax({
                url: '{{ url('ajax/searchComplete?idPlace=') }}'+idPlace,
                type: 'GET',
                success: function (response) {
                    let html = '';
                    $.each(response, function(key, item){
                        html += '<li style="padding-left: 20px;" data-value="'+ item.id +'" class="item">'
                                +  '<i class="input-icon field-icon fa">'
                                    +  '<svg xmlns="http://www.w3.org/2000/svg" style="width: 24px; height: 24px" aria-hidden="true" focusable="false" data-prefix="far" data-icon="list-alt" class="svg-inline--fa fa-list-alt fa-w-16" role="img" viewBox="0 0 512 512"><path fill="currentColor" d="M464 32H48C21.49 32 0 53.49 0 80v352c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48V80c0-26.51-21.49-48-48-48zm-6 400H54a6 6 0 0 1-6-6V86a6 6 0 0 1 6-6h404a6 6 0 0 1 6 6v340a6 6 0 0 1-6 6zm-42-92v24c0 6.627-5.373 12-12 12H204c-6.627 0-12-5.373-12-12v-24c0-6.627 5.373-12 12-12h200c6.627 0 12 5.373 12 12zm0-96v24c0 6.627-5.373 12-12 12H204c-6.627 0-12-5.373-12-12v-24c0-6.627 5.373-12 12-12h200c6.627 0 12 5.373 12 12zm0-96v24c0 6.627-5.373 12-12 12H204c-6.627 0-12-5.373-12-12v-24c0-6.627 5.373-12 12-12h200c6.627 0 12 5.373 12 12zm-252 12c0 19.882-16.118 36-36 36s-36-16.118-36-36 16.118-36 36-36 36 16.118 36 36zm0 96c0 19.882-16.118 36-36 36s-36-16.118-36-36 16.118-36 36-36 36 16.118 36 36zm0 96c0 19.882-16.118 36-36 36s-36-16.118-36-36 16.118-36 36-36 36 16.118 36 36z"/></svg>'
                                +  '</i>'
                                +  '<span class="lv2">'+ item.name +'</span>'
                             +  '</li>';
                    });
                    $('#tour_cities').append(html);
                },
                error: function () {
                    alert("Đã có lỗi xả ra, vui lòng thử lại!");
                }
            });
        });
        $('#newsletter').submit(function (e) {
            e.preventDefault();
            $.ajax({
                url  : '{{ url('/newsletters/ajax') }}',
                type : 'POST',
                data : $(this).serialize(),
                success: function (response) {
                    if (response.success == "ok"){
                        alert("{{ __('theme::frontend.newsletter.success') }}");
                        $('#newsletter')[0].reset();
                    }else{
                        alert(response.errors);
                        $('#newsletters_email').focus();
                    }
                },
                error: function () {
                    alert("{{ __('theme::frontend.newsletter.error') }}");
                }
            });
        });
    })(jQuery);
</script>
</body>
</html>
