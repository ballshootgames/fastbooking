@extends('theme::tomap.frontend.master')

@section('breadcrumb')
    <div class="st-breadcrumb hidden-xs">
        <div class="container">
            <ul>
                <li><a href="{{ url('/') }}">{{ trans('theme::frontend.home') }}</a></li>
                <li class="active">{{ trans('theme::frontend.error_page.not_found') }}</li>
            </ul>
        </div>
    </div>
@endsection
@section('content')
    <div class="container text-center error">
        <h1 class="error__title">
            {{ trans('theme::frontend.error_page.not_found') }}
        </h1>
        <div class="error__image">
            <img src="{{ asset('img/404.jpg') }}" alt="{{ trans('theme::frontend.error_page.not_found') }}">
        </div>
        <article class="error__description">
            {{ trans('theme::frontend.error_page.sorry_page') }} <a href="{{ url('/') }}">{{ trans('theme::frontend.home') }}</a>
        </article>
    </div>
@endsection

