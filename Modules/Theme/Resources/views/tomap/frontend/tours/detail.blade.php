@extends("theme::$themeName.frontend.master")
@section('title')
    <title>{{ $tour->name }} - {{ $settings['website_name'] }}</title>
    <META NAME="KEYWORDS" content="{{ !empty($tour->meta_keyword) ? $tour->meta_keyword : $settings['seo_keywords'] }}"/>
    <meta name="description" content="{!! !empty($tour->name) ? $tour->name : trans('theme::eagles.seo.des_tour') !!}"/>
@endsection
@section('facebook')
    <meta property="og:title" content="{{ $tour->name }} - {{ trans('theme::eagles.site_name')}}"/>
    <meta name="description" content="{!! !empty($tour->name) ? $tour->name : trans('theme::eagles.seo.des_tour') !!}"/>
    <meta property="og:image" content="{{ !empty($tour->image) ? asset(Storage::url($tour->image)) : asset('img/logo_eagle_tourist.png') }}"/>
    <meta property="og:image:secure_url" content="{{ !empty($tour->image) ? asset(Storage::url($tour->image)) : asset('img/logo_eagle_tourist.png') }}"/>
@endsection
@section('twitter')
    <meta name="twitter:title" content="{{ $tour->name }} - {{ trans('theme::eagles.site_name')}}"/>
    <meta name="description" content="{!! !empty($tour->name) ? $tour->name : trans('theme::eagles.seo.des_tour') !!}"/>
    <meta name="twitter:image" content="{{ !empty($tour->image) ? asset(Storage::url($tour->image)) : asset('img/logo_eagle_tourist.png') }}"/>
@endsection
@section('content')
    <div class="st-tour-content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-9">
                    <div class="st-hotel-header">
                        <div class="left">
                            <h2 class="st-heading">{{ $tour->name }}</h2>
                            <div class="sub-heading">
                                <i class="input-icon field-icon fa">
                                    <svg width="16px" height="16px" viewBox="0 0 11 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                        <!-- Generator: Sketch 49 (51002) - http://www.bohemiancoding.com/sketch -->
                                        <desc>Created with Sketch.</desc>
                                        <defs></defs>
                                        <g id="Hotel-layout" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                                            <g id="Hotel_Search_Hover" transform="translate(-170.000000, -553.000000)" stroke="#A0A9B2">
                                                <g id="location-select" transform="translate(135.000000, 359.000000)">
                                                    <g id="hover" transform="translate(0.000000, 42.000000)">
                                                        <g id="Group" transform="translate(35.000000, 149.000000)">
                                                            <g id="ico_maps_add_2" transform="translate(0.000000, 3.000000)">
                                                                <g id="Group">
                                                                    <g id="pin-1">
                                                                        <path d="M10.5,5.5 C10.5,8.314 8.54466667,9.93266667 7.07933333,11.5 C6.202,12.4386667 5.5,15.5 5.5,15.5 C5.5,15.5 4.8,12.4406667 3.92466667,11.5046667 C2.458,9.938 0.5,8.31666667 0.5,5.5 C0.5,2.73857625 2.73857625,0.5 5.5,0.5 C8.26142375,0.5 10.5,2.73857625 10.5,5.5 Z" id="Shape"></path>
                                                                        <circle id="Oval" cx="5.5" cy="5.5" r="2"></circle>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                </i>{{ !empty($tour->city) ? optional($tour->city)->name : 'Không xác định' }}
                            </div>
                        </div>
                    </div>

                    <!--Tour Info-->
                    <div class="st-tour-feature">
                        <div class="row">
                            <div class="col-xs-6 col-lg-3">
                                <div class="item">
                                    <div class="icon">
                                        <i class="input-icon field-icon fa">
                                            <svg width="32px" height="32px" viewBox="0 0 34 34" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                <!-- Generator: Sketch 49 (51002) - http://www.bohemiancoding.com/sketch -->
                                                <desc>Created with Sketch.</desc>
                                                <defs></defs>
                                                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                                                    <g id="Tour_Detail_1" transform="translate(-134.000000, -1005.000000)" stroke="#5191FA">
                                                        <g id="tour-detail" transform="translate(0.000000, 211.000000)">
                                                            <g id="feauture" transform="translate(135.000000, 765.000000)">
                                                                <g id="Group-3">
                                                                    <g id="Group" transform="translate(0.000000, 25.000000)">
                                                                        <g id="ico_clock" transform="translate(0.000000, 5.000000)">
                                                                            <circle id="Oval" cx="16" cy="16" r="16"></circle>
                                                                            <circle id="Oval" cx="16" cy="17.3333333" r="2.28571429"></circle>
                                                                            <path d="M16,15.047619 L16,7.04761905" id="Shape"></path>
                                                                            <path d="M17.6167619,18.9500952 L21.7142857,23.047619" id="Shape"></path>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="info">
                                        <h4 class="name">Hành trình</h4>
                                        <p class="value">
                                            @if($tour->night_number !== 0)
                                                {{ $tour->day_number }} ngày - {{ $tour->night_number }} đêm
                                            @elseif($tour->day_number === 0)
                                                1/2 ngày
                                            @elseif($tour->night_number !== 0 && $tour->day_number === 0)
                                                1/2 ngày - {{ $tour->night_number }} đêm
                                            @else
                                                {{ $tour->day_number }} ngày
                                            @endif
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-lg-3">
                                <div class="item">
                                    <div class="icon">
                                        <i class="input-icon field-icon fa"><svg width="32px" height="32px" viewBox="0 0 34 34" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                <!-- Generator: Sketch 49 (51002) - http://www.bohemiancoding.com/sketch -->

                                                <desc>Created with Sketch.</desc>
                                                <defs></defs>
                                                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                                                    <g id="Tour_Detail_1" transform="translate(-335.000000, -1005.000000)" stroke="#5191FA">
                                                        <g id="tour-detail" transform="translate(0.000000, 211.000000)">
                                                            <g id="feauture" transform="translate(135.000000, 765.000000)">
                                                                <g id="Group" transform="translate(201.000000, 25.000000)">
                                                                    <g id="ico_tour_type" transform="translate(0.000000, 5.000000)">
                                                                        <path d="M13.4810667,23.7283556 C13.7772954,25.6164963 13.043711,27.5192464 11.5566476,28.7198584 C10.0695841,29.9204704 8.05496197,30.2365427 6.27166976,29.549014 C4.48837756,28.8614852 3.20733983,27.2748074 2.91111111,25.3866667 L2.74613333,24.3299556 C2.65661545,23.7692095 2.79434998,23.1959129 3.12883306,22.7370331 C3.46331614,22.2781533 3.96694361,21.9715605 4.52817778,21.8851556 L10.8712889,20.8896 C11.432035,20.8000821 12.0053315,20.9378166 12.4642113,21.2722997 C12.9230912,21.6067828 13.2296839,22.1104103 13.3160889,22.6716444 L13.4810667,23.7283556 Z" id="Shape"></path>
                                                                        <path d="M5.11555556,0.1408 C1.71134814,0.982901045 -0.457506858,4.31950538 0.154844444,7.77244444 L1.41777778,15.8691556 C1.60071,17.0365699 2.69505968,17.8348913 3.86257778,17.6526222 L10.2042667,16.6570667 C10.7657612,16.571 11.2697435,16.2645497 11.6045222,15.8056305 C11.9393009,15.3467113 12.0772269,14.7732252 11.9877333,14.2122667 L11.9592889,14.0088889 C11.7659953,12.7663569 11.9436025,11.4944126 12.4698667,10.3523556 C12.9981751,9.20988894 13.1758566,7.93642212 12.9804444,6.69297778 L12.7827556,5.4272 C12.5097684,3.68767687 11.5342848,2.1366314 10.0846439,1.13712946 C8.63500309,0.137627517 6.8384634,-0.222588541 5.11555556,0.1408 Z" id="Shape"></path>
                                                                        <path d="M18.2583111,25.6995556 C17.8428835,28.5534868 19.7641913,31.223136 22.6023163,31.7355326 C25.4404413,32.2479292 28.1739846,30.4186694 28.7827556,27.5996444 L28.9719111,26.5457778 C29.1807578,25.3831626 28.4089448,24.2709374 27.2467556,24.0597333 L20.9292444,22.9219556 C20.3703881,22.8193233 19.7936791,22.9438755 19.3269888,23.2679956 C18.8602984,23.5921158 18.5421913,24.0890206 18.4432,24.6485333 L18.2583111,25.6995556 Z" id="Shape"></path>
                                                                        <path d="M27.1770667,2.31253333 C30.5568647,3.23734789 32.6432678,6.62146013 31.9514667,10.0565333 L30.4965333,18.1233778 C30.3971763,18.68204 30.079348,19.1781018 29.6133417,19.5018508 C29.1473353,19.8255998 28.5715511,19.9503581 28.0133333,19.8485333 L21.6958222,18.7107556 C21.1363095,18.6117642 20.6394047,18.2936571 20.3152845,17.8269668 C19.9911643,17.3602764 19.8666122,16.7835675 19.9692444,16.2247111 L20.0062222,16.0170667 C20.2290597,14.7795819 20.0806008,13.5038248 19.5795556,12.3505778 C19.0784767,11.1968384 18.9300215,9.92061854 19.1528889,8.68266667 L19.3804444,7.42257778 C19.6924113,5.68717624 20.7045511,4.15653375 22.1792495,3.18999024 C23.6539479,2.22344673 25.4613226,1.90612953 27.1770667,2.31253333 Z" id="Shape"></path>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="info">
                                        <h4 class="name">Ngày khởi hành</h4>
                                        <p class="value">{{ !empty($tour->departure_date) ? Carbon\Carbon::parse($tour->departure_date)->format(config('settings.format.date')) : 'Hàng ngày' }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-lg-3">
                                <div class="item">
                                    <div class="icon">
                                        <i class="input-icon field-icon fa">
                                            <svg width="32px" height="32px" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                <!-- Generator: Sketch 49 (51002) - http://www.bohemiancoding.com/sketch -->
                                                <desc>Created with Sketch.</desc>
                                                <defs></defs>
                                                <g id="Hotel-layout" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                                                    <g id="Room_Detail_1" transform="translate(-544.000000, -803.000000)" stroke="#5191FA">
                                                        <g id="room-detail" transform="translate(0.000000, 211.000000)">
                                                            <g id="Group-3" transform="translate(135.000000, 562.000000)">
                                                                <g id="Group" transform="translate(409.000000, 30.000000)">
                                                                    <g id="ico_adults">
                                                                        <g id="Group" transform="translate(1.000000, 1.000000)">
                                                                            <g id="Regular">
                                                                                <circle id="Oval" cx="7" cy="4" r="4"></circle>
                                                                                <path d="M14,17 C14,13.1340068 10.8659932,10 7,10 C3.13400675,10 4.4408921e-16,13.1340068 0,17 L0,20 L3,20 L4,30 L10,30 L11,20 L14,20 L14,17 Z" id="Shape"></path>
                                                                                <path d="M16,24 L18,24 L19,30 L25,30 L26,24 L30,24 L27,15 C26,12 24.7613333,10 22,10 C20.1015957,10.0018584 18.4126862,11.2059289 17.792,13" id="Shape"></path>
                                                                                <circle id="Oval" cx="22" cy="4" r="4"></circle>
                                                                            </g>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="info">
                                        <h4 class="name">Loại tour</h4>
                                        <p class="value">{{ !empty($tour->properties->where('type', 'tour_type')->first()) ? $tour->properties->where('type', 'tour_type')->first()->name : optional(optional($tour->properties)->first())->name }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-lg-3">
                                <div class="item">
                                    <div class="icon">
                                        <i class="input-icon field-icon fa">
                                            <svg width="32px" height="32px" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                <!-- Generator: Sketch 49 (51002) - http://www.bohemiancoding.com/sketch -->
                                                <desc>Created with Sketch.</desc>
                                                <defs></defs>
                                                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                                                    <g id="Tour_Detail_1" transform="translate(-735.000000, -1005.000000)" stroke="#5191FA">
                                                        <g id="tour-detail" transform="translate(0.000000, 211.000000)">
                                                            <g id="feauture" transform="translate(135.000000, 765.000000)">
                                                                <g id="Group" transform="translate(601.000000, 25.000000)">
                                                                    <g transform="translate(0.000000, 5.000000)" id="Regular">
                                                                        <g>
                                                                            <circle id="Oval" cx="9" cy="5.5" r="5.5"></circle>
                                                                            <path d="M10,13.0773333 C9.66846827,13.0319989 9.33455994,13.0061766 9,13 C4.02943725,13 5.92118946e-16,17.0294373 0,22" id="Shape"></path>
                                                                            <path d="M28,26 L24,26 L18,30 L18,26 L16,26 C14.8954305,26 14,25.1045695 14,24 L14,16 C14,14.8954305 14.8954305,14 16,14 L28,14 C29.1045695,14 30,14.8954305 30,16 L30,24 C30,25.1045695 29.1045695,26 28,26 Z" id="Shape"></path>
                                                                            <path d="M18,18 L26,18" id="Shape"></path>
                                                                            <path d="M18,22 L26,22" id="Shape"></path>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="info">
                                        <h4 class="name">Mã tour</h4>
                                        <p class="value">{{ $tour->prefix_code.$tour->code }} </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End Tour info-->
                    <div class="st-gallery" data-width="100%" data-nav="thumbs" data-allowfullscreen="true">
                        <style>.fotorama1563075755738 .fotorama__nav--thumbs .fotorama__nav__frame{
                                padding:12px;
                                height:135px}
                            .fotorama1563075755738 .fotorama__thumb-border{
                                height:131px;
                                border-width:2px;
                                margin-top:12px}
                        </style>
                        <div class="fotorama--hidden"></div>
                        <div class="fotorama">
                            @if(!empty(optional($tour->galleries)->first))
                                @foreach($tour->galleries as $item)
                                    <a href="{{ asset(Storage::url($item->image)) }}">
                                        <img src="{{ asset(Storage::url($item->image)) }}" class="fotorama__img" style="width: 870px; height: 555px; left: 0px; top: 0px;"/></a>
                                @endforeach
                            @else
                                <a href="{{ asset(Storage::url($tour->image)) }}">
                                    <img src="{{ asset(Storage::url($tour->image)) }}" class="fotorama__img" style="width: 870px; height: 555px; left: 0px; top: 0px;"/></a>
                            @endif
                        </div>
                        {{--<div class="fotorama fotorama1563075755738" data-auto="false">--}}
                            {{--<div class="fotorama__wrap fotorama__wrap--css3 fotorama__wrap--slide fotorama__wrap--toggle-arrows fotorama__wrap--no-controls" style="width: 100%; min-width: 0px; max-width: 100%;">--}}
                                {{--<div class="fotorama__stage" style="width: 870px; height: 555px;">--}}
                                    {{--<div class="fotorama__fullscreen-icon" tabindex="0" role="button"></div>--}}
                                    {{--<div class="fotorama__stage__shaft" style="transition-duration: 0ms; transform: translate3d(0px, 0px, 0px); width: 870px; margin-left: 0px;">--}}
                                        {{--<div class="fotorama__stage__frame fotorama__loaded fotorama__loaded--img fotorama__active" style="left: 0px;">--}}
                                            {{--<img src="https://tomap.travelerwp.com/wp-content/uploads/2015/01/Indipendant-Travel1-2000x1250-870x555.jpg" class="fotorama__img" style="width: 870px; height: 555px; left: 0px; top: 0px;">--}}
                                        {{--</div>--}}
                                        {{--<div class="fotorama__stage__frame fotorama__loaded fotorama__loaded--img" style="left: 882px;">--}}
                                            {{--<img src="https://tomap.travelerwp.com/wp-content/uploads/2015/01/tour_img-1355873-145-3-870x555.jpg" class="fotorama__img" style="width: 870px; height: 555px; left: 0px; top: 0px;">--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="fotorama__arr fotorama__arr--prev fotorama__arr--disabled" tabindex="-1" role="button" disabled="disabled"></div>--}}
                                    {{--<div class="fotorama__arr fotorama__arr--next" tabindex="0" role="button"></div>--}}
                                    {{--<div class="fotorama__video-close"></div>--}}
                                {{--</div>--}}
                                {{--<div class="fotorama__nav-wrap">--}}
                                    {{--<div class="fotorama__nav fotorama__nav--thumbs fotorama__shadows--right" style="width: 870px;">--}}
                                        {{--<div class="fotorama__nav__shaft fotorama__grab" style="transition-duration: 0ms; transform: translate3d(0px, 0px, 0px);">--}}
                                            {{--<div class="fotorama__thumb-border" style="transition-duration: 0ms; transform: translate3d(0px, 0px, 0px); width: 131px;"></div>--}}
                                            {{--<div class="fotorama__nav__frame fotorama__nav__frame--thumb fotorama__active" tabindex="0" role="button" style="width: 135px;">--}}
                                                {{--<div class="fotorama__thumb fotorama__loaded fotorama__loaded--img">--}}
                                                    {{--<img src="https://tomap.travelerwp.com/wp-content/uploads/2015/01/Indipendant-Travel1-2000x1250-870x555.jpg" class="fotorama__img" style="width: 211.622px; height: 135px; left: -38.3108px; top: 0px;"></div></div><div class="fotorama__nav__frame fotorama__nav__frame--thumb" tabindex="0" role="button" style="width: 135px;"><div class="fotorama__thumb fotorama__loaded fotorama__loaded--img"><img src="https://tomap.travelerwp.com/wp-content/uploads/2015/01/tour_img-1355873-145-3-870x555.jpg" class="fotorama__img" style="width: 211.622px; height: 135px; left: -38.3108px; top: 0px;"></div></div><div class="fotorama__nav__frame fotorama__nav__frame--thumb" tabindex="0" role="button" style="width: 135px;"><div class="fotorama__thumb fotorama__loaded fotorama__loaded--img"><img src="https://tomap.travelerwp.com/wp-content/uploads/2015/01/tour_img-1355897-145-1-870x555.jpg" class="fotorama__img" style="width: 211.622px; height: 135px; left: -38.3108px; top: 0px;"></div></div><div class="fotorama__nav__frame fotorama__nav__frame--thumb" tabindex="0" role="button" style="width: 135px;"><div class="fotorama__thumb fotorama__loaded fotorama__loaded--img"><img src="https://tomap.travelerwp.com/wp-content/uploads/2015/01/tour_img-1355906-145-1-870x555.jpg" class="fotorama__img" style="width: 211.622px; height: 135px; left: -38.3108px; top: 0px;"></div></div><div class="fotorama__nav__frame fotorama__nav__frame--thumb" tabindex="0" role="button" style="width: 135px;"><div class="fotorama__thumb fotorama__loaded fotorama__loaded--img"><img src="https://tomap.travelerwp.com/wp-content/uploads/2017/06/3DSKY_697_x_448-2-870x555.jpg" class="fotorama__img" style="width: 211.622px; height: 135px; left: -38.3108px; top: 0px;"></div></div><div class="fotorama__nav__frame fotorama__nav__frame--thumb" tabindex="0" role="button" style="width: 135px;"><div class="fotorama__thumb fotorama__loaded fotorama__loaded--img"><img src="https://tomap.travelerwp.com/wp-content/uploads/2017/06/hlag_697_448-1-870x555.jpg" class="fotorama__img" style="width: 211.622px; height: 135px; left: -38.3108px; top: 0px;"></div></div><div class="fotorama__nav__frame fotorama__nav__frame--thumb" tabindex="0" role="button" style="width: 135px;"><div class="fotorama__thumb"></div></div><div class="fotorama__nav__frame fotorama__nav__frame--thumb" tabindex="0" role="button" style="width: 135px;"><div class="fotorama__thumb"></div></div><div class="fotorama__nav__frame fotorama__nav__frame--thumb" tabindex="0" role="button" style="width: 135px;"><div class="fotorama__thumb"></div></div><div class="fotorama__nav__frame fotorama__nav__frame--thumb" tabindex="0" role="button" style="width: 135px;"><div class="fotorama__thumb"></div></div><div class="fotorama__nav__frame fotorama__nav__frame--thumb" tabindex="0" role="button" style="width: 135px;"><div class="fotorama__thumb"></div></div><div class="fotorama__nav__frame fotorama__nav__frame--thumb" tabindex="0" role="button" style="width: 135px;"><div class="fotorama__thumb"></div></div></div></div></div></div></div>--}}
                        {{--<div class="shares dropdown">--}}
                            {{--<a href="https://www.youtube.com/watch?v=AmZ0WrEaf34" class="st-video-popup share-item">--}}
                                {{--<i class="input-icon field-icon fa">--}}
                                    {{--<svg height="20px" width="20px" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">--}}
                                    {{--<g fill="#FFFFFF">--}}
                                        {{--<path d="M2.25,24C1.009,24,0,22.991,0,21.75V2.25C0,1.009,1.009,0,2.25,0h19.5C22.991,0,24,1.009,24,2.25v19.5--}}
                                            {{--c0,1.241-1.009,2.25-2.25,2.25H2.25z M2.25,1.5C1.836,1.5,1.5,1.836,1.5,2.25v19.5c0,0.414,0.336,0.75,0.75,0.75h19.5--}}
                                            {{--c0.414,0,0.75-0.336,0.75-0.75V2.25c0-0.414-0.336-0.75-0.75-0.75H2.25z"></path>--}}
                                        {{--<path d="M9.857,16.5c-0.173,0-0.345-0.028-0.511-0.084C8.94,16.281,8.61,15.994,8.419,15.61c-0.11-0.221-0.169-0.469-0.169-0.716--}}
                                            {{--V9.106C8.25,8.22,8.97,7.5,9.856,7.5c0.247,0,0.495,0.058,0.716,0.169l5.79,2.896c0.792,0.395,1.114,1.361,0.719,2.153--}}
                                            {{--c-0.154,0.309-0.41,0.565-0.719,0.719l-5.788,2.895C10.348,16.443,10.107,16.5,9.857,16.5z M9.856,9C9.798,9,9.75,9.047,9.75,9.106--}}
                                            {{--v5.788c0,0.016,0.004,0.033,0.011,0.047c0.013,0.027,0.034,0.044,0.061,0.054C9.834,14.998,9.845,15,9.856,15--}}
                                            {{--c0.016,0,0.032-0.004,0.047-0.011l5.788-2.895c0.02-0.01,0.038-0.027,0.047-0.047c0.026-0.052,0.005-0.115-0.047-0.141l-5.79-2.895--}}
                                            {{--C9.889,9.004,9.872,9,9.856,9z"></path>--}}
                                    {{--</g>--}}
                                    {{--</svg>--}}
                                {{--</i>--}}
                            {{--</a>--}}
                            {{--<a href="#" class="share-item social-share">--}}
                                {{--<i class="input-icon field-icon fa"><svg width="20px" height="20px" viewBox="0 0 18 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">--}}
                                        {{--<!-- Generator: Sketch 49 (51002) - http://www.bohemiancoding.com/sketch -->--}}

                                        {{--<desc>Created with Sketch.</desc>--}}
                                        {{--<defs></defs>--}}
                                        {{--<g id="Hotel-layout" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">--}}
                                            {{--<g id="Room_Only_Detail_1" transform="translate(-921.000000, -251.000000)" stroke="#FFFFFF">--}}
                                                {{--<g id="room-detail" transform="translate(0.000000, 211.000000)">--}}
                                                    {{--<g id="img">--}}
                                                        {{--<g id="share" transform="translate(910.000000, 30.000000)">--}}
                                                            {{--<g id="ico_share" transform="translate(10.000000, 10.000000)">--}}
                                                                {{--<g id="Group" transform="translate(1.666667, 0.000000)">--}}
                                                                    {{--<g id="share-2" stroke-width="1.5">--}}
                                                                        {{--<path d="M16.4583333,4.375 L9.58333333,4.375 C8.20262146,4.375 7.08333333,5.49428813 7.08333333,6.875 L7.08333333,10" id="Shape"></path>--}}
                                                                        {{--<polyline id="Shape" points="12.7083333 8.125 16.4583333 4.375 12.7083333 0.625"></polyline>--}}
                                                                        {{--<path d="M13.9583333,11.875 L13.9583333,18.125 C13.9583333,18.8153559 13.3986893,19.375 12.7083333,19.375 L1.45833333,19.375 C0.767977396,19.375 0.208333333,18.8153559 0.208333333,18.125 L0.208333333,8.125 C0.208333333,7.43464406 0.767977396,6.875 1.45833333,6.875 L3.33333333,6.875" id="Shape"></path>--}}
                                                                    {{--</g>--}}
                                                                {{--</g>--}}
                                                            {{--</g>--}}
                                                        {{--</g>--}}
                                                    {{--</g>--}}
                                                {{--</g>--}}
                                            {{--</g>--}}
                                        {{--</g>--}}
                                    {{--</svg>--}}
                                {{--</i>--}}
                            {{--</a>--}}
                            {{--<ul class="share-wrapper">--}}
                                {{--<li><a class="facebook" href="https://www.facebook.com/sharer/sharer.php?u=https://tomap.travelerwp.com/st_tour/10-days-of-vacation-in-florence-resorts/&amp;title=Northern%20California%20Summer%202019" target="_blank" rel="noopener" original-title="Facebook"><i class="fa fa-facebook fa-lg"></i></a></li>--}}
                                {{--<li><a class="twitter" href="https://twitter.com/share?url=https://tomap.travelerwp.com/st_tour/10-days-of-vacation-in-florence-resorts/&amp;title=Northern%20California%20Summer%202019" target="_blank" rel="noopener" original-title="Twitter"><i class="fa fa-twitter fa-lg"></i></a></li>--}}
                                {{--<li><a class="google" href="https://plus.google.com/share?url=https://tomap.travelerwp.com/st_tour/10-days-of-vacation-in-florence-resorts/&amp;title=Northern%20California%20Summer%202019" target="_blank" rel="noopener" original-title="Google+"><i class="fa fa-google-plus fa-lg"></i></a></li>--}}
                                {{--<li><a class="no-open pinterest" href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','../../../assets.pinterest.com/js/pinmarklet2a3b.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());" target="_blank" rel="noopener" original-title="Pinterest"><i class="fa fa-pinterest fa-lg"></i></a></li>--}}
                                {{--<li><a class="linkedin" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=https://tomap.travelerwp.com/st_tour/10-days-of-vacation-in-florence-resorts/&amp;title=Northern%20California%20Summer%202019" target="_blank" rel="noopener" original-title="LinkedIn"><i class="fa fa-linkedin fa-lg"></i></a></li>--}}
                            {{--</ul>--}}
                            {{--<a href="#" class="share-item like-it" data-toggle="modal" data-target="#st-login-form" data-type="st_tours" data-id="1616"><i class="fa fa-heart-o"></i></a>--}}
                        {{--</div>--}}
                    </div>
                    <!--Tour Overview-->
                    <div class="st-overview">
                        <h3 class="st-section-title">Overview</h3>
                        <div class="st-description" data-toggle-section="st-description">
                            <p style="text-align: justify;">{!! $tour->intro !!}</p>
                        </div>
                    </div>
                    <!--End Tour Overview-->

                    <!--Tour program-->
                    <div class="st-program">
                        <div class="st-title-wrapper">
                            <h3 class="st-section-title">Hành trình</h3>
                        </div>
                        <div class="st-program-list">
                            @php($q = 0)
                            @foreach($tour->schedules as $schedule)
                                <div class="item {{ $q === 0 ? 'active' : '' }}">
                                    <div class="header">
                                        <h5>Day {{ $schedule->day }}: {{ $schedule->name }}</h5>
                                        <span class="arrow"><i class="fa fa-angle-down"></i></span>
                                    </div>
                                    <div class="body">
                                        <div class="row">
                                            @if(!empty($schedule->image))
                                            <div class="col-lg-4">
                                                <img src="{{ asset(Storage::url($schedule->image)) }}" alt="Day {{ $schedule->day }}: {{ $schedule->name }}" class="img-responsive">
                                            </div>
                                            @endif
                                            <div class="{{ @!empty($schedule->image) ? 'col-lg-8' : 'col-lg-12' }}">
                                                {!! $schedule->content !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @php($q++)
                            @endforeach
                        </div>
                    </div>
                    <!--End Tour program-->

                    <!--Tour Include/Exclude-->
                    @if(!empty($tour->includes))
                    <div class="st-include">
                        <h3 class="st-section-title">Giá tour bao gồm</h3>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="include">
                                    {!! $tour->includes !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @if(!empty($tour->excludes))
                    <div class="st-include">
                        <h3 class="st-section-title">Giá tour không bao gồm</h3>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="exclude">
                                    {!! $tour->excludes !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    <!--End Tour Include/Exclude-->

                    <!--Tour Map-->
                    {{--<div class="st-hr large st-height2"></div>--}}
                    {{--<div class="st-map-wrapper">--}}
                        {{--<div class="st-flex space-between">--}}
                            {{--<h2 class="st-heading-section mg0">Tour's Location</h2>--}}
                            {{--<div class="c-grey">--}}
                                {{--<i class="input-icon field-icon fa">--}}
                                    {{--<svg width="18px" height="18px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">--}}
                                        {{--<!-- Generator: Sketch 49 (51002) - http://www.bohemiancoding.com/sketch -->--}}
                                        {{--<desc>Created with Sketch.</desc>--}}
                                        {{--<defs></defs>--}}
                                        {{--<g id="Ico_maps" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">--}}
                                            {{--<g id="Group" transform="translate(4.000000, 0.000000)" stroke="#A0A9B2">--}}
                                                {{--<g id="pin-1" transform="translate(-0.000000, 0.000000)">--}}
                                                    {{--<path d="M15.75,8.25 C15.75,12.471 12.817,14.899 10.619,17.25 C9.303,18.658 8.25,23.25 8.25,23.25 C8.25,23.25 7.2,18.661 5.887,17.257 C3.687,14.907 0.75,12.475 0.75,8.25 C0.75,4.10786438 4.10786438,0.75 8.25,0.75 C12.3921356,0.75 15.75,4.10786438 15.75,8.25 Z" id="Shape"></path>--}}
                                                    {{--<circle id="Oval" cx="8.25" cy="8.25" r="3"></circle>--}}
                                                {{--</g>--}}
                                            {{--</g>--}}
                                        {{--</g>--}}
                                    {{--</svg>--}}
                                {{--</i>California, USA--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="st-map mt30">--}}
                            {{--<div class="google-map" data-lat="36.617642193886944" data-lng="-120.2541205702891" data-icon="../../../travelerdemo.wpengine.com/wp-content/uploads/st_uploadfont/tour.png" data-zoom="6" data-disablecontrol="true" data-showcustomcontrol="true" data-style="normal" style="position: relative; overflow: hidden;"><div style="height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; background-color: rgb(229, 227, 223);"><div class="gm-err-container"><div class="gm-err-content"><div class="gm-err-icon"><img src="https://maps.gstatic.com/mapfiles/api-3/images/icon_error.png" draggable="false" style="user-select: none;"></div><div class="gm-err-title">Rất tiếc! Đã xảy ra lỗi.</div><div class="gm-err-message">Trang này đã không tải Google Maps đúng cách. Hãy xem bảng điều khiển JavaScript để biết chi tiết kỹ thuật.</div></div></div></div></div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <!--End Tour Map-->

                    <!--Review Option-->
                    <div class="st-hr large st-height2 st-hr-comment"></div>
                    <h2 class="st-heading-section">Đánh giá</h2>
                    <div id="reviews" data-toggle-section="st-reviews" class=" stoped-scroll-section">
                        <div class="review-pagination" style="position: relative">
                            <div id="overlay" style="display: none"></div>
                            <div class="fa-loading text-center" style="display: none">
                                <span class="fa fa-spinner fa-spin fa-3x text-primary" aria-hidden="true"></span>
                            </div>
                            <div class="summary"></div>
                            <div id="list-comment" class="review-list">
                            </div>
                        </div>
                        <div id="write-review">
                            <h4 class="heading">
                                <a href="#" class="toggle-section c-main f16" data-target="st-review-form">Viết cảm nhận<i class="fa fa-angle-down ml5"></i></a>
                            </h4>
                            <div id="respond" class="comment-respond" data-toggle-section="st-review-form" style="display: none">
                                <form action="{{ url('admin/control/store-comment') }}" method="post" id="comment-form" class="review-form" novalidate="">
                                    <div class="form-wrapper">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="user_name" placeholder="Tên bạn *" required>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                    <input type="email" class="form-control" name="user_email" placeholder="Email *" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <textarea name="comment" class="form-control has-matchHeight" required placeholder="Nội dung" style="height: 342px;"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="tour_id" value="{{ $tour->id }}">
                                        <input id="submit" type="submit" name="submit" class="btn btn-green font-medium" value="Gửi nhận xét">
                                    </div>
                                </form>
                            </div><!-- #respond -->
                        </div>
                    </div>
                    <!--End Review Option-->
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="widgets">
                        <div class="fixed-on-mobile" id="booking-request">
                            <div class="close-icon hide">
                                <i class="input-icon field-icon fa">
                                    <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                        <!-- Generator: Sketch 49 (51002) - http://www.bohemiancoding.com/sketch -->
                                        <desc>Created with Sketch.</desc>
                                        <defs></defs>
                                        <g id="Ico_close" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                                            <g id="Group" stroke="#1A2B48" stroke-width="1.5">
                                                <g id="close">
                                                    <path d="M0.75,23.249 L23.25,0.749" id="Shape"></path>
                                                    <path d="M23.25,23.249 L0.75,0.749" id="Shape"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                </i>
                            </div>
                            <div class="form-book-wrapper relative">
                                <div class="loader-wrapper">
                                    <div class="st-loader"></div>
                                </div>
                                <div class="form-head">
                                    <div class="price">
                                        <span class="value"><span class="text-lg lh1em item "> {{ number_format($tour->price) }} Đ</span></span>
                                    </div>
                                </div>
                                <form id="form-booking-inpage" method="get" action="{{ url('booking/add-item') }}">
                                    <input type="hidden" name="id" value="{{ $tour->id }}" />
                                    {{--<input type="hidden" name="action" value="tours_add_to_cart">
                                    <input type="hidden" name="item_id" value="1616">
                                    <input type="hidden" name="type_tour" value="daily_tour">
                                    <div class="form-group form-date-field form-date-search clearfix " data-format="DD/MM/YYYY" data-availability-date="07/14/2019">
                                        <div class="date-wrapper clearfix">
                                            <div class="check-in-wrapper">
                                                <label>Date</label>
                                                <div class="render check-in-render">14/07/2019</div>
                                                <span class="sts-tour-checkout-label hidden"><span> - </span>
                                                    <div class="render check-out-render">14/07/2019</div>
                                                </span>
                                            </div>
                                            <i class="fa fa-angle-down arrow"></i>
                                        </div>
                                        <input type="text" class="check-in-input" value="14/07/2019" name="check_in">
                                        <input type="hidden" class="check-out-input" value="14/07/2019" name="check_out">
                                        <input type="text" class="check-in-out-input" value="14/07/2019 12:00 am-14/07/2019 12:00 am" name="check_in_out" data-action="st_get_availability_tour_frontend" data-tour-id="1616" data-posttype="st_tours">
                                    </div>
                                    <div class="form-group form-more-extra st-form-starttime" style="display: none">
                                        <input type="hidden" data-starttime="" data-checkin="14/07/2019" data-checkout="14/07/2019" data-tourid="1616" id="starttime_hidden_load_form" data-posttype="st_tours">
                                        <div class="" id="starttime_box">
                                            <label>Start time</label>
                                            <select class="form-control st_tour_starttime" name="starttime_tour" id="starttime_tour"></select>
                                        </div>
                                    </div>--}}
                                    <!--End starttime-->

                                    <div class="form-group form-guest-search clearfix ">
                                        <div class="guest-wrapper clearfix">
                                            <div class="check-in-wrapper">
                                                <label>{{ trans('theme::frontend.booking.adult') }}</label>
                                                <div class="render">{{ trans('theme::frontend.booking.age') }} 18+</div>
                                            </div>
                                            <div class="select-wrapper">
                                                <div class="st-number-wrapper">
                                                    <input type="text" name="adult" value="1" class="form-control st-input-number" autocomplete="off" readonly="" data-min="1" data-max="10"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="guest-wrapper clearfix">
                                            <div class="check-in-wrapper">
                                                <label>{{ trans('theme::frontend.booking.child') }}</label>
                                                <div class="render">{{ trans('theme::frontend.booking.age') }} 6-17</div>
                                            </div>
                                            <div class="select-wrapper">
                                                <div class="st-number-wrapper">
                                                    <input type="text" name="child" value="0" class="form-control st-input-number" autocomplete="off" readonly="" data-min="0" data-max="10">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="guest-wrapper clearfix">
                                            <div class="check-in-wrapper">
                                                <label>{{ trans('theme::frontend.booking.baby') }}</label>
                                                <div class="render">{{ trans('theme::frontend.booking.age') }} 0-5</div>
                                            </div>
                                            <div class="select-wrapper">
                                                <div class="st-number-wrapper">
                                                    <input type="text" name="baby" value="0" class="form-control st-input-number" autocomplete="off" readonly="" data-min="0" data-max="10">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="submit-group">
                                        <input class="btn btn-green btn-large btn-full upper" type="submit" name="submit" value="{{ trans('theme::frontend.booking.book_now') }}">
                                        {{--<input style="display:none;" type="submit" class="btn btn-default btn-send-message" data-id="1616" name="st_send_message" value="Send message">--}}
                                    </div>
                                    <div class="message-wrapper mt30">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="st-hr large"></div>
            @if(!empty($tourLink->tour_related))
                @include('theme::tomap.frontend.tours.tour-link-related')
            @else
                @include('theme::tomap.frontend.tours.tour-related')
            @endif
        </div>
    </div>
@endsection

@section('scripts')
    <style>
        #st-content-wrapper.st-single-tour.style-2 .st-tour-feature {
            border-bottom: none;
            margin-bottom: 0;
        }
        #st-content-wrapper .st-tour-feature {
            padding-top: 25px;
            padding-bottom: 15px;
            border-top: 1px solid #D7DCE3;
            border-bottom: 1px solid #D7DCE3;
            margin-bottom: 30px;
        }
        #st-content-wrapper .st-tour-feature .item {
            display: flex;
            align-items: center;
            margin-bottom: 10px;
        }
        #st-content-wrapper .st-tour-feature .item .icon {
            margin-right: 20px;
        }
        #st-content-wrapper .st-tour-feature .item .info .name {
            font-size: 14px;
            color: #1A2B48;
            margin: 0;
            margin-bottom: 3px;
        }
        #st-content-wrapper .st-tour-feature .item .info .value {
            font-size: 14px;
            color: #5E6D77;
            margin: 0;
        }

        #st-content-wrapper.st-single-tour .st-program .st-program-list .item:first-child {
            border-left-color: #2ECC71;
        }
        #st-content-wrapper.st-single-tour .st-program .st-program-list .item {
            border: 1px solid #D7DCE3;
            border-left: 6px solid #5191FA;
            background: #FCFCFC;
            border-radius: 3px;
            overflow: hidden;
            margin-bottom: 10px;
        }
        #st-content-wrapper.st-single-tour .st-program .st-program-list .item .header {
            padding: 17px 30px;
            overflow: hidden;
            cursor: pointer;
        }
        #st-content-wrapper.st-single-tour .st-program .st-program-list .item .header h5 {
            font-size: 16px;
            color: #1A2B48;
            margin: 0;
            float: left;
            line-height: 23px;
        }
        #st-content-wrapper.st-single-tour .st-program .st-program-list .item.active .header .arrow {
            transform: rotate(180deg);
        }
        #st-content-wrapper.st-single-tour .st-program .st-program-list .item .header .arrow {
            float: right;
            font-size: 25px;
            line-height: 5px;
        }
        #st-content-wrapper.st-single-tour .st-program .st-program-list .item.active .body {
            height: auto;
            padding: 15px 30px 30px 30px;
        }
        #st-content-wrapper.st-single-tour .st-program .st-program-list .item .body {
            height: 0px;
            overflow: hidden;
            font-size: 14px;
            color: #5E6D77;
            padding: 0 30px 0 30px;
            -webkit-transition: all 0.3s;
            -moz-transition: all 0.3s;
            -ms-transition: all 0.3s;
            -o-transition: all 0.3s;
            transition: all 0.3s;
        }

        .fa-loading {
            position: absolute;
            left: 0;
            right: 0;
            top: 50%;
            z-index: 1000;
        }
        .comment-review {
            position: relative;
        }
        #overlay {
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgba(255,255,255,0.6);
            z-index: 4;
            cursor: pointer;
        }
        #list-comment {
            background-color: #fdfdfd;
        }
        #list-comment h4 {
            font-size: 18px;
            margin: 10px;
        }
        #list-comment .row::after {
            content: '';
            width: 90%;
            text-align: center;
            margin: 0 auto;
            padding-top: 10px;
            border-bottom: 1px solid #b7b7b7;
        }
        #list-comment p {
            width: 93%;
            margin: 0 auto;
        }
        #list-comment .loading i {
            color: #1295ff;
        }
        #review_form_wrapper {
            padding-top: 16px;
        }
        .comment-mb-10{
            margin-bottom: 10px;
        }
        p.form-submit input#submit{
            border: 1px solid #2d9bf3;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
        }
        .comment-form input {
            padding: 5px;
        }
    </style>
    {{--<script type="text/javascript" src="{{ asset('plugins/moment.min.js') }}" ></script>--}}
    <script>
        var body = $('body');
        $('.fixed-on-mobile', body).each(function () {
            var t = $(this);
            var screen = t.data('screen');
            var width = t.width(),top = t.offset().top;
            $(window).scroll(function () {
                if ($(window).scrollTop() >= top && window.matchMedia('(min-width: ' + screen + ')').matches) {
                    if (t.css('position') != 'fixed') {
                        t.css({
                            width: width,
                            position: 'fixed',
                            top: 0,
                            'z-index': 9
                        });
                    }
                    if ($('.stoped-scroll-section', body).length) {
                        var room_position = $('.stoped-scroll-section', body).offset().top;
                        console.log($(window).scrollTop());
                        console.log(room_position);
                        if ($(window).scrollTop() >= room_position && t.css('position') == 'fixed') {
                            t.css({
                                width: width,
                                position: 'static',
                                top: room_position - $(window).scrollTop(),
                                'z-index': 9
                            });
                        } else {
                            t.css({
                                width: width,
                                position: 'static',
                                top: 0,
                                'z-index': 9
                            });
                        }
                    }
                } else {
                    t.css({
                        position: '',
                        top: '',
                        width: 'auto',
                        'z-index': ''
                    })
                }
            });
        });
        // let loading = "<div class='text-center loading'><p><i class='fa fa-spinner fa-spin fa-5x'></i></p></div>";
        const getListComments = (tourId = {{ $tour->id }}, page = 1) => {
            $('#overlay').show();
            $('.fa-loading').show();
            // $('#list-comment').html(loading);
            var text = '{{ $tour->name }}';
            $.ajax({
                // url: $('#comment-form').attr('action'),
                url: "{{ url('/ajax/getComments') }}",
                type: 'GET',
                dataType: "JSON",
                data: {
                    tourId: tourId,
                    page: page
                },success: function(response){
                    let html = '';
                    if (response.total >= 1) {
                        let data = response.data;
                        $.each(data, function (key, value) {
                            html += '<div class="comment-item">';
                                html += '<div class="comment-item-head">';
                                    html += '<div class="media">';
                                        html += '<h4 class="media-heading">'+value.user_name+'<span class="date">'+moment(value.updated_at).format('DD/MM/YYYY')+'</span></h4>';
                                    html += '</div>';
                                html += '</div>';
                                html += '<div class="comment-item-body">';
                                    html += '<div class="detail">';
                                        html += '<div class="st-description">'+value.comment+'</div>';
                                    html += '</div>';
                                html += '</div>';
                            html += '</div>';
                        });
                    }else{
                        html += '<span id="reply-title" class="comment-reply-title" style="font-size: 18px">'+'Hãy là người đầu tiên nhận xét “'+text+'”</span>';
                    }
                    $('#overlay').hide();
                    $('.fa-loading').hide();
                    $('#list-comment').html(html);
                }
            })
        };
        getListComments({{$tour->id}});

        $('#comment-form').submit(function (e) {
            e.preventDefault();
            $('#overlay').show();
            $('.fa-loading').show();
            $.ajax({
                // url: $('#comment-form').attr('action'),
                url: "{{ url('admin/control/store-comment') }}",
                type: 'POST',
                dataType: "JSON",
                data: $(this).serialize(),
                success: function(response){
                    getListComments({{$tour->id}});

                },
                error: function (error) {
                    console.log(error);
                    alert('Đã có lỗi xảy ra, vui lòng thử lại!');
                    $('#overlay').hide();
                    $('.fa-loading').hide();
                }
            })
        })
    </script>
@endsection