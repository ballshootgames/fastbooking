@extends("theme::$themeName.frontend.master")
@section('content')
    @php($background = !empty($slides->first()) ? asset(Storage::url($slides->first()->image)) : asset('img/ba-na-1900x420.jpg'))
    <div class="list-tour-search" style="background: url('{{ $background }}') center no-repeat; background-size: cover">
        @include('theme::tomap.frontend.layouts.search-form-tour')
    </div>
    <div class="container">
        <div class="tour__items tour__items-sort">
            <select name="drpOrderTour" id="drpOrderTour" class="tour-order">
                <option value="">{{ trans('theme::frontend.sort_tours.sort') }}</option>
                <option value="asc" {{ Request::get('tour_order') === 'asc' ? 'selected' : '' }}>
                    {{ trans('theme::frontend.sort_tours.a_z') }}
                </option>
                <option value="desc" {{ Request::get('tour_order') === 'desc' ? 'selected' : '' }}>
                    {{ trans('theme::frontend.sort_tours.z_a') }}
                </option>
                <option value="ascPrice" {{ Request::get('tour_order') === 'ascPrice' ? 'selected' : '' }}>
                    {{ trans('theme::frontend.sort_tours.min_max') }}
                </option>
                <option value="descPrice" {{ Request::get('tour_order') === 'descPrice' ? 'selected' : '' }}>
                    {{ trans('theme::frontend.sort_tours.max_min') }}
                </option>
            </select>
        </div>
        <div class="tour__items">
            @if(!empty($tours->first()))
                @foreach($tours as $item)
                    @if($item->night_number !== 0)
                        @php($day_night = $item->day_number . ' ngày ' . $item->night_number .' đêm')
                    @elseif($item->day_number === 0.5)
                        @php($day_night = '1/2 ngày')
                    @elseif($item->night_number !== 0 && $item->day_number === 0.5)
                        @php($day_night = '1/2 ngày ' . $item->night_number .' đêm')
                    @else
                        @php($day_night = $item->day_number . ' ngày')
                    @endif
                    <div class="tour__item">
                        <div class="row item__row">
                            <div class="col-md-3 item__header">
                                @if($item->hot == 1)
                                    <div class="item__featured">{{ __('Nổi bật') }}</div>
                                @endif
                                <a href="{{ url('tour/'.$item->slug.'.html') }}" class="item__link">
                                    <img src="{{ !empty($item->image) ? \App\Traits\ImageResize::getThumbnail($item->image, 270, 250) : asset('img/noimage.gif') }}"
                                         alt="{{ $item->name }}" class="item__image img-responsive" />
                                </a>
                            </div>
                            <div class="col-md-6 item__body">
                                <div class="item__content mb-auto">
                                    <h4 class="item__title">
                                        <a href="{{ url('tour/'.$item->slug.'.html') }}" class="item__link">{{ $item->name }}</a>
                                    </h4>
{{--                                    <div class="item__excerpt">--}}
{{--                                        {!! Str::limit($item->intro,220) !!}--}}
{{--                                    </div>--}}
                                </div>
                                <p class="item__location">
                                    <i class="input-icon field-icon fa">
                                        <svg width="20px" height="20px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                            <g id="Ico_maps" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                                                <g id="Group" transform="translate(4.000000, 0.000000)" stroke="#666666">
                                                    <g id="pin-1" transform="translate(-0.000000, 0.000000)">
                                                        <path d="M15.75,8.25 C15.75,12.471 12.817,14.899 10.619,17.25 C9.303,18.658 8.25,23.25 8.25,23.25 C8.25,23.25 7.2,18.661 5.887,17.257 C3.687,14.907 0.75,12.475 0.75,8.25 C0.75,4.10786438 4.10786438,0.75 8.25,0.75 C12.3921356,0.75 15.75,4.10786438 15.75,8.25 Z" id="Shape"></path>
                                                        <circle id="Oval" cx="8.25" cy="8.25" r="3"></circle>
                                                    </g>
                                                </g>
                                            </g>
                                        </svg>
                                    </i>
                                    {{ !empty(optional($item->city)->name) ? optional($item->city)->name : 'Không xác định' }}
                                </p>
                            </div>
                            <div class="col-md-3 item__footer">
                                <div class="item__content mb-auto">
                                    <div class="service item__price">
                                        <i class="input-icon field-icon fa">
                                            <div hidden>
                                                <svg xmlns="http://www.w3.org/2000/svg" style="display: none;"><symbol id="price" viewBox="0 0 59 59"><title>price</title><path style="fill:#46B29D;" d="M30.22,5L1.241,34.218c-0.987,0.987-0.987,2.588,0,3.576l20.466,20.466 c0.987,0.987,2.588,0.987,3.576,0L53.5,29.28L53.5,5H30.22z M43.5,19c-2.209,0-4-1.791-4-4c0-2.209,1.791-4,4-4s4,1.791,4,4 C47.5,17.209,45.709,19,43.5,19z"/><path style="fill:#28685C;" d="M43.5,7c-4.418,0-8,3.582-8,8s3.582,8,8,8s8-3.582,8-8S47.918,7,43.5,7z M43.5,19 c-2.209,0-4-1.791-4-4c0-2.209,1.791-4,4-4s4,1.791,4,4C47.5,17.209,45.709,19,43.5,19z"/><path style="fill:#FDD7AD;" d="M43.5,16c-0.256,0-0.512-0.098-0.707-0.293c-0.391-0.391-0.391-1.023,0-1.414l14-14 c0.391-0.391,1.023-0.391,1.414,0s0.391,1.023,0,1.414l-14,14C44.012,15.902,43.756,16,43.5,16z"/><path style="fill:#FFFFFF;" d="M22.55,44.399c-1.971,0-3.862-0.777-5.268-2.183c-2.905-2.905-2.905-7.633,0-10.538 c0.391-0.391,1.023-0.391,1.414,0s0.391,1.023,0,1.414c-2.125,2.126-2.125,5.584,0,7.71c1.137,1.137,2.71,1.713,4.312,1.578 c0.806-0.067,1.539-0.461,2.063-1.107c0.553-0.683,0.808-1.574,0.699-2.447l-1.326-10.605c-0.176-1.411,0.235-2.853,1.13-3.956 c0.871-1.073,2.096-1.727,3.45-1.839c2.193-0.181,4.338,0.603,5.892,2.156c2.905,2.905,2.905,7.633,0,10.538 c-0.391,0.391-1.023,0.391-1.414,0s-0.391-1.023,0-1.414c2.125-2.126,2.125-5.584,0-7.71c-1.137-1.136-2.703-1.714-4.312-1.578 c-0.806,0.067-1.539,0.461-2.063,1.107c-0.553,0.682-0.808,1.574-0.699,2.447l1.326,10.605c0.176,1.41-0.236,2.853-1.13,3.955 c-0.871,1.074-2.096,1.728-3.45,1.84C22.966,44.391,22.758,44.399,22.55,44.399z"/><path style="fill:#FFFFFF;" d="M16.533,43.968c-0.256,0-0.512-0.098-0.707-0.293c-0.391-0.391-0.391-1.023,0-1.414l18.818-18.818 c0.391-0.391,1.023-0.391,1.414,0s0.391,1.023,0,1.414L17.24,43.675C17.044,43.87,16.789,43.968,16.533,43.968z"/></symbol></svg>
                                            </div>
                                            <svg class="icon" width="16" height="16">
                                                <use xlink:href="#price" />
                                            </svg>
                                        </i>
                                        {{ number_format($item->price) }} Đ
                                        @empty(!($item->price_discount))
                                            <span style="text-decoration: line-through; font-size: 16px; padding-left: 10px;">
                                                {{ number_format($item->price_discount) }} đ
                                            </span>
                                        @endempty
                                    </div>
                                    <div class="service item__day">
                                        <i class="fa fa-bus" aria-hidden="true"></i>{{ !empty($item->departure_date) ? Carbon\Carbon::parse($item->departure_date)->format(config('settings.format.date')) : 'Khởi hành hàng ngày' }}
                                    </div>
                                    <div class="service item__duration">
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>{{ $day_night }}
                                    </div>
                                </div>
                                <div class="item__view">
                                    <a href="{{ url('tour/'.$item->slug.'.html') }}" class="item__view--link">{{ __('Xem chi tiết') }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
        {!! $tours->appends(Request::except('page'))->render() !!}
    </div>
@endsection
@section('scripts')
    <script>
        $(function () {
            $('#drpOrderTour').on('change', function(){
                let tour_order = $(this).val();
                let actForm = $('#tour-search');
                $('#tour_order').attr('value', tour_order);
                $.ajax({
                    url: actForm.attr('action'),
                    type: 'GET',
                    data: actForm.serialize(),
                    success: function (res) {
                        actForm.submit();
                    },
                    error: function () {
                        alert("Đã có lỗi xả ra, vui lòng thử lại!");
                    }
                });
            });
        });
    </script>
@endsection

