<div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row st bg-holder vc_custom_1551754905020 vc_row-has-fill vc_video-bg-container">
    <div class="vc_video-bg vc_hidden-xs vc_image_slider">
        <div id="carousel-simple" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                @if($slides->count() > 0)
                    @for($i = 0; $i < $slides->count(); $i++)
                        <li data-target="#carousel-simple" data-slide-to="{{ $i }}" class="{{ $i == 0 ? 'active' : '' }}"></li>
                    @endfor
                @else
                    <li data-target="#carousel-simple" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-simple" data-slide-to="1" class=""></li>
                    <li data-target="#carousel-simple" data-slide-to="2" class=""></li>
                @endif
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                @if($slides->count() > 0)
                    @php($j = 0)
                    @foreach($slides as $slide)
                        <div class="item {{ $j == 0 ? 'active' : '' }}">
                            <img src="{{ empty($slide->image) ? '' : asset(Storage::url($slide->image)) }}" alt="{{ $slide->name }}">
                        </div>
                    @endforeach
                @else
                    <div class="item active">
                        <img src="{{ asset('img/tomap-halong.jpg') }}">
                    </div>
                    <div class="item">
                        <img src="{{ asset('img/hue-tomap.jpg') }}">
                    </div>
                    <div class="item">
                        <img src="{{ asset('img/saigon-tomap.jpg') }}">
                    </div>
                @endif
            </div>

            <!-- Controls -->
            {{--<a class="left carousel-control" href="#carousel-simple" role="button" data-slide="prev">
                <i class="fa fa-chevron-left" aria-hidden="true"></i>
            </a>
            <a class="right carousel-control" href="#carousel-simple" role="button" data-slide="next">
                <i class="fa fa-chevron-right" aria-hidden="true"></i>
            </a>--}}
        </div>
    </div>
    @include('theme::tomap.frontend.layouts.search-form-tour')
</div>