<div class="search-result-page st-tours service-slider-wrapper">
    <div class="st-hotel-result">
        <div class="owl-carousel st-service-slider">
            @foreach($tourNewHot as $item)
                @if($item->night_number !== 0)
                    @php
                        $day_night = $item->day_number . ' ngày ' . $item->night_number .' đêm';
                    @endphp
                @elseif($item->day_number === 0.5)
                    @php
                        $day_night = '1/2 ngày';
                    @endphp
                @elseif($item->night_number !== 0 && $item->day_number === 0.5)
                    @php
                        $day_night = '1/2 ngày ' . $item->night_number .' đêm';
                    @endphp
                @else
                    @php
                        $day_night = $item->day_number . ' ngày';
                    @endphp
                @endif
                <div class="item-service grid-item has-matchHeight">
                    <div class="service-border">
                        <div class="thumb">
                            {{--<span class="st_sale_class box_sale sale_small">11% </span>           --}}
                            {{--<a href="" class="login" data-toggle="modal" data-target="#st-login-form">
                                <div class="service-add-wishlist" title="Add to wishlist">
                                    <i class="fa fa-heart"></i>
                                    <div class="lds-dual-ring"></div>
                                </div>
                            </a>--}}
                            <div class="service-tag bestseller">
                                @if($item->hot == 1)
                                <div class="feature_class st_featured featured">{{ trans('theme::frontend.tours.featured') }}</div>
                                @endif
                            </div>
                            <a href="{{ url(config('theme.slug.slug_tour_new_hot'). '/' .$item->slug.'.html') }}">
                                <img width="680" height="500" src="{{ !empty($item->image) ? \App\Traits\ImageResize::getThumbnail($item->image, 275, 202) : asset('img/noimage.gif') }}" class="img-responsive wp-post-image" alt="{{ $item->name }}" />
                            </a>
                        </div>
                        <p class="service-location plr15"><i class="input-icon field-icon fa">
                                <svg width="15px" height="15px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <!-- Generator: Sketch 49 (51002) - http://www.bohemiancoding.com/sketch -->
                                    <desc>Created with Sketch.</desc>
                                    <defs></defs>
                                    <g id="Ico_maps" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                                        <g id="Group" transform="translate(4.000000, 0.000000)" stroke="#666666">
                                            <g id="pin-1" transform="translate(-0.000000, 0.000000)">
                                                <path d="M15.75,8.25 C15.75,12.471 12.817,14.899 10.619,17.25 C9.303,18.658 8.25,23.25 8.25,23.25 C8.25,23.25 7.2,18.661 5.887,17.257 C3.687,14.907 0.75,12.475 0.75,8.25 C0.75,4.10786438 4.10786438,0.75 8.25,0.75 C12.3921356,0.75 15.75,4.10786438 15.75,8.25 Z" id="Shape"></path>
                                                <circle id="Oval" cx="8.25" cy="8.25" r="3"></circle>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                            </i>{{ \Modules\Tour\Entities\TourProperty::where('id', $item->start_city)->first()->name ?? 'Không xác định' }}
                        </p>
                        <h4 class="service-title plr15"><a href="{{ url(config('theme.slug.slug_tour_new_hot'). '/' .$item->slug.'.html') }}">{{ $item->name }}</a></h4>
                        <div class="service-review plr15">
                            <div class="service-start-day review-child">
                                {{--<ul class="icon-group text-color booking-item-rating-stars">--}}
                                <i class="fa fa-bus" aria-hidden="true"></i>
                                {{--</ul>--}}
                                <span class="review">{{ !empty($item->departure_date) ? Carbon\Carbon::parse($item->departure_date)->format(config('settings.format.date')) : 'Khởi hành hàng ngày' }}</span>
                            </div>
                            <div class="service-duration review-child">
                                {{--<i class="input-icon field-icon fa">--}}
                                {{--<svg height="16px" width="16px" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"--}}
                                {{--viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">--}}
                                {{--<g fill="#5E6D77">--}}
                                {{--<path d="M12,23.25C5.797,23.25,0.75,18.203,0.75,12C0.75,5.797,5.797,0.75,12,0.75c6.203,0,11.25,5.047,11.25,11.25--}}
                                {{--C23.25,18.203,18.203,23.25,12,23.25z M12,2.25c-5.376,0-9.75,4.374-9.75,9.75s4.374,9.75,9.75,9.75s9.75-4.374,9.75-9.75--}}
                                {{--S17.376,2.25,12,2.25z"/>--}}
                                {{--<path d="M15.75,16.5c-0.2,0-0.389-0.078-0.53-0.22l-2.25-2.25c-0.302,0.145-0.632,0.22-0.969,0.22c-1.241,0-2.25-1.009-2.25-2.25--}}
                                {{--c0-0.96,0.615-1.808,1.5-2.121V5.25c0-0.414,0.336-0.75,0.75-0.75s0.75,0.336,0.75,0.75v4.629c0.885,0.314,1.5,1.162,1.5,2.121--}}
                                {{--c0,0.338-0.075,0.668-0.22,0.969l2.25,2.25c0.292,0.292,0.292,0.768,0,1.061C16.139,16.422,15.95,16.5,15.75,16.5z M12,11.25--}}
                                {{--c-0.414,0-0.75,0.336-0.75,0.75s0.336,0.75,0.75,0.75s0.75-0.336,0.75-0.75S12.414,11.25,12,11.25z"/>--}}
                                {{--</g>--}}
                                {{--</svg>--}}
                                {{--</i>--}}
                                <i class="fa fa-clock-o" aria-hidden="true"></i>
                                <span class="review">{{ $day_night }}</span>
                            </div>
                        </div>
                        <div class="section-footer">
                            <div class="footer-inner plr15">
                                <div class="service-price">
                                    <span>
                                        <i class="input-icon field-icon fa">
                                            <div hidden>
                                                <svg xmlns="http://www.w3.org/2000/svg" style="display: none;"><symbol id="price" viewBox="0 0 59 59"><title>price</title><path style="fill:#46B29D;" d="M30.22,5L1.241,34.218c-0.987,0.987-0.987,2.588,0,3.576l20.466,20.466 c0.987,0.987,2.588,0.987,3.576,0L53.5,29.28L53.5,5H30.22z M43.5,19c-2.209,0-4-1.791-4-4c0-2.209,1.791-4,4-4s4,1.791,4,4 C47.5,17.209,45.709,19,43.5,19z"/><path style="fill:#28685C;" d="M43.5,7c-4.418,0-8,3.582-8,8s3.582,8,8,8s8-3.582,8-8S47.918,7,43.5,7z M43.5,19 c-2.209,0-4-1.791-4-4c0-2.209,1.791-4,4-4s4,1.791,4,4C47.5,17.209,45.709,19,43.5,19z"/><path style="fill:#FDD7AD;" d="M43.5,16c-0.256,0-0.512-0.098-0.707-0.293c-0.391-0.391-0.391-1.023,0-1.414l14-14 c0.391-0.391,1.023-0.391,1.414,0s0.391,1.023,0,1.414l-14,14C44.012,15.902,43.756,16,43.5,16z"/><path style="fill:#FFFFFF;" d="M22.55,44.399c-1.971,0-3.862-0.777-5.268-2.183c-2.905-2.905-2.905-7.633,0-10.538 c0.391-0.391,1.023-0.391,1.414,0s0.391,1.023,0,1.414c-2.125,2.126-2.125,5.584,0,7.71c1.137,1.137,2.71,1.713,4.312,1.578 c0.806-0.067,1.539-0.461,2.063-1.107c0.553-0.683,0.808-1.574,0.699-2.447l-1.326-10.605c-0.176-1.411,0.235-2.853,1.13-3.956 c0.871-1.073,2.096-1.727,3.45-1.839c2.193-0.181,4.338,0.603,5.892,2.156c2.905,2.905,2.905,7.633,0,10.538 c-0.391,0.391-1.023,0.391-1.414,0s-0.391-1.023,0-1.414c2.125-2.126,2.125-5.584,0-7.71c-1.137-1.136-2.703-1.714-4.312-1.578 c-0.806,0.067-1.539,0.461-2.063,1.107c-0.553,0.682-0.808,1.574-0.699,2.447l1.326,10.605c0.176,1.41-0.236,2.853-1.13,3.955 c-0.871,1.074-2.096,1.728-3.45,1.84C22.966,44.391,22.758,44.399,22.55,44.399z"/><path style="fill:#FFFFFF;" d="M16.533,43.968c-0.256,0-0.512-0.098-0.707-0.293c-0.391-0.391-0.391-1.023,0-1.414l18.818-18.818 c0.391-0.391,1.023-0.391,1.414,0s0.391,1.023,0,1.414L17.24,43.675C17.044,43.87,16.789,43.968,16.533,43.968z"/></symbol></svg>
                                            </div>
                                            <svg class="icon">
                                                <use xlink:href="#price" />
                                            </svg>
                                        </i>
                                    </span>
                                    <span class="price">
                                        <span class="text-lg lh1em item "> {{ number_format($item->price) }} Đ</span>
                                    </span>
                                </div>
                            </div>
                            @empty(!($item->price_discount))
                                <div style="text-decoration: line-through; padding-right: 21px; text-align: right;">
                                    {{ number_format($item->price_discount) }} đ
                                </div>
                            @endempty
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>