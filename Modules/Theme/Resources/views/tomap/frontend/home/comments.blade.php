<div class="vc_row wpb_row st bg-holder">
    <div class='container '>
        <div class='row'>
            <div class="wpb_column column_container col-md-12">
                <div class="vc_column-inner wpb_wrapper">
                    <div class="st-testimonial-new">
                        <h3>Our happy clients</h3>
                        <div class="owl-carousel st-testimonial-slider style-1 ">
                            <div class="item has-matchHeight">
                                <div class="author">
                                    <img src="https://tomap.travelerwp.com/wp-content/uploads/2018/12/people_8-1-70x70.jpg" alt="User Avatar"/>
                                    <div class="author-meta">
                                        <h4>Eva Hicks</h4>
                                        <div class="star">
                                            <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>
                                        </div>
                                    </div>
                                </div>
                                <p>Faucibus tristique felis potenti ultrices ornare rhoncus semper hac facilisi Rutrum tellus lorem sem velit nisi non pharetra in dui.</p>
                            </div>
                            <div class="item has-matchHeight">
                                <div class="author">
                                    <img src="https://tomap.travelerwp.com/wp-content/uploads/2018/12/people_7-70x70.jpg" alt="User Avatar"/>
                                    <div class="author-meta">
                                        <h4>Donald Wolf</h4>
                                        <div class="star">
                                            <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>
                                        </div>
                                    </div>
                                </div>
                                <p>Nam dapibus nisl vitae elit fringilla rutrum. Aenean sollicitudin, erat a elementum rutrum.</p>
                            </div>
                            <div class="item has-matchHeight">
                                <div class="author">
                                    <img src="https://tomap.travelerwp.com/wp-content/uploads/2018/12/people_3-70x70.jpg" alt="User Avatar"/>
                                    <div class="author-meta">
                                        <h4>Charlie Harrington</h4>
                                        <div class="star">
                                            <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>
                                        </div>
                                    </div>
                                </div>
                                <p>Faucibus tristique felis potenti ultrices ornare rhoncus semper hac facilisi Rutrum tellus lorem sem velit nisi non pharetra in dui.</p>
                            </div>
                            <div class="item has-matchHeight">
                                <div class="author">
                                    <img src="https://tomap.travelerwp.com/wp-content/uploads/2018/12/people_4-70x70.jpg" alt="User Avatar"/>
                                    <div class="author-meta">
                                        <h4>Kody Byrd</h4>
                                        <div class="star">
                                            <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>                             </div>
                                    </div>
                                </div>
                                <p>
                                    Nam dapibus nisl vitae elit fringilla rutrum. Aenean sollicitudin, erat a elementum rutrum.                    </p>
                            </div>
                            <div class="item has-matchHeight">
                                <div class="author">
                                    <img src="https://tomap.travelerwp.com/wp-content/uploads/2018/12/people_5-70x70.jpg" alt="User Avatar"/>
                                    <div class="author-meta">
                                        <h4>Lee Cunningham</h4>
                                        <div class="star">
                                            <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>                             </div>
                                    </div>
                                </div>
                                <p>
                                    Faucibus tristique felis potenti ultrices ornare rhoncus semper hac facilisi Rutrum tellus lorem sem velit nisi non pharetra in dui                    </p>
                            </div>
                            <div class="item has-matchHeight">
                                <div class="author">
                                    <img src="https://tomap.travelerwp.com/wp-content/uploads/2018/12/people_4-70x70.jpg" alt="User Avatar"/>
                                    <div class="author-meta">
                                        <h4>Melody Marshall</h4>
                                        <div class="star">
                                            <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>                             </div>
                                    </div>
                                </div>
                                <p>
                                    Nam dapibus nisl vitae elit fringilla rutrum. Aenean sollicitudin, erat a elementum rutrum.                    </p>
                            </div>
                        </div></div>
                </div>
            </div>
        </div><!--End .row-->
    </div><!--End .container-->
</div>