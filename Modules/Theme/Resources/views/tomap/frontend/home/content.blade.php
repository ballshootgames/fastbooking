@extends("theme::$themeName.frontend.master")

@section('content')
    <div class="container fill">
        @include('theme::tomap.frontend.home.slides')
    </div>
    <div class="container">
        <div class="vc_row-full-width vc_clearfix"></div>
        <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row st bg-holder vc_row-has-fill">
            <div class='container'>
                <div class='row'>
                    <div class="wpb_column column_container col-md-12 vc_custom_1542167696382">
                        <div class="vc_column-inner wpb_wrapper">
                            <div class="wpb_text_column wpb_content_element  fs-28 fs-normal" >
                                <div class="wpb_wrapper">
                                    <h2>{{ trans('theme::eagles.tour_new_hot') }}</h2>
                                </div>
                            </div>
                            @include('theme::tomap.frontend.home.tour-new')
                        </div>
                    </div>
                </div><!--End .row-->
            </div><!--End .container-->
        </div>
        <div class="vc_row-full-width vc_clearfix"></div>
        <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row st bg-holder vc_row-has-fill">
            <div class='container'>
                <div class='row'>
                    <div class="wpb_column column_container col-md-12 vc_custom_1542167696382">
                        <div class="vc_column-inner wpb_wrapper">
                            <div class="wpb_text_column wpb_content_element  fs-28 fs-normal" >
                                <div class="wpb_wrapper">
                                    <h2>{{ trans('theme::eagles.tour_shock') }}</h2>
                                </div>
                            </div>
                            @include('theme::tomap.frontend.home.tour-shock')
                        </div>
                    </div>
                </div><!--End .row-->
            </div><!--End .container-->
        </div>
        <div class="vc_row-full-width vc_clearfix"></div>
        @include('theme::tomap.frontend.home.destinations')
        @include('theme::tomap.frontend.home.tour-inland')
        @include('theme::tomap.frontend.home.tour-abroad')
    </div>
@endsection