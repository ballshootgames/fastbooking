<div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row st bg-holder">
    <div class='container '>
        <div class='row'>
            <div class="wpb_column column_container col-md-12 vc_custom_1543548162184"><div class="vc_column-inner wpb_wrapper">
                    <div class="wpb_text_column wpb_content_element  fs-28 fs-normal" >
                        <div class="wpb_wrapper">
                            <h2>{{ trans('theme::frontend.tours.destination') }}</h2>
                        </div>
                    </div>
                    <div class="row list-destination masonry">
                        @if($tourPlaces->count() > 0)
                            @php
                                $i = 0
                            @endphp
                            @foreach($tourPlaces as $itemPlace)
                                @php
                                    $idProperties = $itemPlace->id;
                                    $tours = new \Modules\Tour\Entities\Tour();
                                    $tours = $tours->whereHas('properties', function ($query) use ($idProperties){
                                        $query->where('type', 'tour_place')->where('id',$idProperties);
                                    });
                                    $countTourPlaces = $tours->count();
                                @endphp
                                <div class="{{ $i == 0 ? 'col-xs-12 col-sm-12 col-md-8 first-item' : 'col-xs-6 col-sm-6 col-md-4 second-item' }} has-matchHeight">
                                    <div class="destination-item">
                                        <a class="st-link" href="{{ url('diem-den-tour/' . Str::slug($itemPlace->name)) }}">
                                            <div class="image">
                                                <img src="{{ !empty($itemPlace->image) ? $i == 0 ? \App\Traits\ImageResize::getThumbnail($itemPlace->image, 773, 377) : \App\Traits\ImageResize::getThumbnail($itemPlace->image, 377, 377) : asset('img/noimage.gif') }}"
                                                     class="img-responsive">
                                                <div class="content">
                                                    <h4 class="title">{{ $itemPlace->name }}</h4>
                                                    <div class="desc st_tours">{{ $countTourPlaces }} {{ trans('theme::frontend.tours.tour') }}</div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                @php($i++)
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div><!--End .row-->
    </div><!--End .container-->
</div>
<div class="vc_row-full-width vc_clearfix"></div>