<div class="hs-contact">
    <div class="container">
        <div class="col-lg-8 col-lg-offset-2">
            <h2 class="title-h2 text-white">{{ trans('theme::eagles.send_request') }}</h2>
            {!! Form::open(['method' => 'POST', 'url' => '', 'role' => 'contact', 'id' => 'contact'])  !!}
            <div class="hs-form-success text-center" style="display: none">
                {{ trans('theme::eagles.success_contact') }}
            </div>
            <div class="hs-contact-frm row">
                <div class="form-group col-sm-6">
{{--                    {!! Form::label('fullname', 'Họ và tên', ['class' => 'control-label label-required']) !!}--}}
                    {!! Form::text('fullname', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Họ và tên']) !!}
                    {!! $errors->first('fullname', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group col-sm-6">
{{--                    {!! Form::label('email', 'Email', ['class' => 'control-label label-required']) !!}--}}
                    {!! Form::email('email', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Email']) !!}
                    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group col-sm-6">
{{--                    {!! Form::label('address', 'Địa chỉ', ['class' => 'control-label']) !!}--}}
                    {!! Form::text('address', null, ['class' => 'form-control', 'placeholder' => 'Địa chỉ']) !!}
                </div>
                <div class="form-group col-sm-6">
{{--                    {!! Form::label('phone', 'Điện thoại', ['class' => 'control-label']) !!}--}}
                    {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Điện thoại']) !!}
                </div>
                <div class="form-group col-xs-12">
{{--                    {!! Form::label('message', 'Nội dung', ['class' => 'control-label label-required']) !!}--}}
                    {!! Form::textarea('message', null, ['class' => 'form-control', 'required' => 'required', 'rows' => 6, 'placeholder' => 'Nội dung']) !!}
                    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="col-xs-12 text-center text-uppercase">
                    <button type="submit" class="button-search">{{ __('theme::eagles.send') }}</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>