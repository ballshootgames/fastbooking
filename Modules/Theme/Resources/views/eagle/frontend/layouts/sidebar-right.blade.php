<div class="sidebar">
    <div class="related-post">
        <h3 class="related"><span>Tìm kiếm</span></h3>
        <div class="container-search">
            {!! Form::open(['method' => 'GET', 'url' => 'tim-kiem.html', 'role' => 'form']) !!}
                <input type="text" value="{{\Request::get('title')}}" class="form-control input-sm" name="title" autocomplete="off" placeholder="{{ __('message.search_keyword') }}">
                <select name="destination" id="destination" class="form-control">
                    <option value="">{{ trans('theme::eagles.destinations') }}</option>
                    @foreach($newsTypeDes['newsTypes'] as $itemType)
                        @php($selected = $itemType->id == Request::get('destination') ? 'selected' : '' )
                        <option value="{{ $itemType->id }}" {{ $selected }}>{{ $itemType->title }}</option>
                    @endforeach
                </select>
                <button type="submit" class="button-search">Tìm kiếm</button>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="hk-support">
        <h3>Hỗ trợ trực tuyến</h3>
        <ul>
            <li>
                <strong>Đặt tour</strong> <span>Call: {{$settings['phone_contact_tour']}}</span>
                <span>Email: {{$settings['email_contact_tour']}}</span>
                <p class="sp-social"><a href="{{$settings['facebook_link']}}" class="hk-ic hk-f">Facebook</a> <a href="#" class="hk-ic hk-s">Skype</a>
                </p>
            </li>
            <li><strong>tư vấn du lịch</strong> <span>Call: {{$settings['phone_contact_advisory']}}</span>
                <span>Email: {{$settings['email_contact_advisory']}}</span>
                <p class="sp-social"><a href="{{$settings['facebook_link']}}" class="hk-ic hk-f">Facebook</a> <a href="#" class="hk-ic hk-s">Skype</a>
                </p></li>
            <li class="text-center"><span class="cl-red">Mọi chi tiết xin vui lòng gửi về địa chỉ</span> {{$settings['email_receive_message']}}
                <span></span>
            </li>
        </ul>
    </div>
    <div class="banner hidden-sm hidden-xs">
        <a href="#">
            <img style="width: 100%;" src="https://dulichdaibang.com/wp-content/themes/vinavivu/images/banner.jpg"
                 alt="" class="lazyloading"
            >
        </a>
    </div>
    <div class="clear"></div>
    <br>
    <div class="hk-news-rg"><h3>Tin mới</h3>
        <div class="info-nws">
            <ul class="row">
                @foreach($newNews as $itemNew)
                    <li class="col-md-12 col-sm-6 col-xs-6">
                        <div class="detail">
                            <a href="{{ url( optional($itemNew->type)->slug . '/' . $itemNew->slug) }}.html" class="img">
                                <img data-sizes="auto" class="lazyload" data-expand="-10" data-src="{{ empty($itemNew->image) ? asset('img/noimage.gif') : asset(Storage::url($itemNew->image)) }}"
                                     alt="{{ $itemNew->title }}"/>
                            </a>
                            <div class="txt-news">
                                <h4>
                                    <a href="{{ url( optional($itemNew->type)->slug . '/' . $itemNew->slug) }}.html">{!! $itemNew->title !!}</a>
                                </h4>
                                <span>{{ \Carbon\Carbon::parse($itemNew->updated_at)->format(config('settings.format.date_')) }}</span>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
    <br>
    <div class="hk-like-box">    
        {!!$settings['fanpage_facebook_embed']!!}
    </div>
</div>