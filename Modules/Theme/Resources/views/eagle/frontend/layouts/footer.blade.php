@include('theme::eagle.frontend.layouts.form-contact')
<div id="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-sm-12 col-xs-12">
                    <div class="ft-ad">
                        <h3>{{ $settings['company_name'] }}</h3>
                        @foreach(__('theme::eagles.company_info') as $key => $msg)
                            <span>{{ $msg }}: {{ $settings[$key] }}</span>
                        @endforeach
                        <ul class="ft-social">
                            <li><a href="{{ $settings["facebook_link"] }}" class="hk-ic hk-f" target="_blank">facebook</a></li>
                            <li><a href="{{ $settings["twitter_link"] }}" class="hk-ic hk-t" target="_blank">twitter_link</a></li>
                            <li><a href="{{ $settings["linkedin_link"] }}" class="hk-ic hk-l" target="_blank">linkedin_link</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="ft-sv"><h3>{{ trans('theme::eagles.information_general') }}</h3>
                        <ul class="ft-lst">
                            @foreach(__('theme::eagles.company_general') as $key => $msg)
                                <li>
                                    <a href="#">
                                        {{ $msg  }}: {{ $settings[$key] }}
                                    </a>
                                </li>
                            @endforeach
                            <img src="{{ asset('img/thanh-toan-vinavivu-icon.jpg') }}"
                             alt="Chấp nhận thanh toán" title="Chấp nhận thanh toán" width="150px" height="100px"/>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <div class="ft-sp">
                        <h3>{{ trans('theme::eagles.customer_support') }}</h3>
                        <ul class="ft-lst">
                            @foreach($botMenus as $itemMenu)
                                <li>
                                    <a href="{{ url($itemMenu->slug) }}.html">{{ $itemMenu->title }}</a>
                                </li>
                            @endforeach
                            @empty(!($settings['ministry_industry_trade_link']))
                                <a href={{ $settings['ministry_industry_trade_link'] }}
                                   target="_blank"><img src="{{ asset('img/dathongbao.png') }}" alt="Đã thông báo với Bộ Công Thương" title="Đã thông báo với Bộ Công Thương" width="100px" height="70px"/>
                                </a>
                            @endempty
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="all-right text-center">
    {{ __('All Rights Reserved') }} &copy; <span>{{ $settings['company_name'] }}</span>
</div>
<nav id="menu-mobile">
    <div id="main-nav">
        @php
            $menu = new \Modules\Theme\Entities\Menu();
            $menu->showMainMenuMobile($mainMenus, Request::url());
        @endphp
    </div>
</nav>
<a class="back-to-top"><i class="fa fa-angle-up"></i></a>
<a href="{{"tel:".$settings['phone']}}" class="suntory-alo-phone suntory-alo-green" id="suntory-alo-phoneIcon" style="left: 0; bottom: 120px;">
    <div class="suntory-alo-ph-circle"></div>
    <div class="suntory-alo-ph-circle-fill"></div>
    <div class="suntory-alo-ph-img-circle"><i class="fa fa-phone"></i></div>
</a>