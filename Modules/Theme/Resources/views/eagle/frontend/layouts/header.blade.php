<div id="header">
    <div class="header-top">
        <div class="container">
            <div class="pull-left hd-top-lf hidden-sm hidden-xs">
                <p>{{ trans('theme::eagles.welcome') }}<span>{{ $settings['website_name'] }}</span></p>
                <ul class="hk-social">
                    @if($settings['facebook_link'])
                    <li><a href="{{ $settings["facebook_link"] }}" class="hk-ic hk-f" target="_blank">facebook</a></li>
                    @endif
                    @if($settings['twitter_link'])
                    <li><a href="{{ $settings["twitter_link"] }}" class="hk-ic hk-t" target="_blank">twitter_link</a></li>
                    @endif
                    @if($settings['linkedin_link'])
                    <li><a href="{{ $settings["linkedin_link"] }}" class="hk-ic hk-l" target="_blank">linkedin_link</a></li>
                    @endif
                </ul>
                <div class="clear"></div>
            </div>
            <div class="pull-right hd-top-rg">
                <div class="hd-contact">
                    <span><i class="hk-ic hk-a"></i>Việt Nam</span>
                    <span><i class="hk-ic hk-p"></i>{{ $settings['phone'] }}</span>
                    @if(Auth::guest())
                        <span> <a href="#" data-toggle="modal" data-target="#modalLogin">{{ trans('theme::eagles.login') }}</a>
                        {{--<div class="popup-dangnhap hidden-xs">
                            <div class="popup-dangnhap__tit">{{ trans('theme::eagles.popup_notification_point') }}</div>
                            <a class="btn popup-dangnhap__btn-dangnhap" href="{{ url('/login') }}">{{ trans('theme::eagles.login') }}</a>
                            <a class="btn popup-dangnhap__btn-dangky" href="{{ url('/register') }}">{{ trans('theme::eagles.register') }}</a>
                            <span class="popup-dangnhap__btn-tat">{{ trans('theme::eagles.close') }}</span>
                        </div>
                        <script>
                            document.querySelector('.popup-dangnhap__btn-tat').addEventListener('click', () => {
                                document.querySelector('.popup-dangnhap').style.display = 'none';
                            })
                        </script>--}}
                        </span>
{{--                        <span><a href="#" data-toggle="modal" data-target="#modalRegister">{{ trans('theme::eagles.register') }}</a></span>--}}
                    @else
                        <span><a href="{{ url('/home') }}" class="hs-web-login hs-char">{{ Auth::user()->name }}</a></span>
                        <span>
                            <a class="hs-web-login" href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ trans('theme::eagles.logout') }}</a>
                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                                <input type="submit" value="logout" style="display: none;">
                            </form>
                        </span>
                    @endif
                    {{--<span id="cart-header">
                        <a href="{{ url('gio-hang') }}">
                            {{ trans('theme::eagles.cart') }}: {{ $carts['totalQty'] }} Tour
                            <span class="woocommerce-Price-amount amount">- {{ number_format($carts['totalPrice']) }}&nbsp;
                                <span class="woocommerce-Price-currencySymbol"> VNĐ </span>
                            </span>
                        </a>
                    </span>--}}
                </div>
            </div>
        </div>
    </div>
</div>