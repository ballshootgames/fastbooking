<div class="tab-content tab-content--m">
    <div role="tabpanel" class="tab-pane active" id="home">
        <form action="{{ url('tour') }}" method="GET" role="form" id="tour-search">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-2 tour-select tour-select--cat mb-m-15">
                    <select name="tour_category" id="tour-category" class="form-control select2">
                        <option value="">{{ trans('theme::frontend.tours.tour_category') }}</option>
                        @foreach(\Modules\Tour\Entities\TourProperty::where('type', 'tour_category')->get() as $item)
                            <option {{ Request::get('tour_category') == $item->id || isset($slugChild) && $slugChild == $item->slug ? "selected" : "" }} value="{{ $item->id }}">{{ $item->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-2 tour-select tour-select--cat mb-m-15">
                    <select name="tour_place" id="tour-place" class="form-control select2">
                        <option value="">{{ trans('theme::frontend.tours.tour_place') }}</option>
                        @php
                            if (isset($slugChild) && in_array($slugChild,\Modules\Tour\Entities\TourProperty::where('type', 'tour_category')->pluck('slug')->toArray())){
                                $tourCategory = \Modules\Tour\Entities\TourProperty::where(['type' => 'tour_category','slug' => $slugChild])->first();
                            }
                            $tourPlaces = new \Modules\Tour\Entities\TourProperty();
                            if (!empty($tourCategory)){
                                $tourPlaces = $tourPlaces->where('type_id', $tourCategory->id);
                            }
                            $tourPlaces = $tourPlaces->where(['type' => 'tour_place', 'parent_id' => null])->orderBy('name','asc')->get();
                        @endphp
                        @foreach($tourPlaces as $item)
                            <option {{ Request::get('tour_place') == $item->id || isset($slugChild) && $slugChild == $item->slug ? "selected" : "" }} value="{{ $item->id }}">{{ $item->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-2 tour-select tour-select--cat mb-m-15">
                    <select name="tour_cities" class="form-control select2" id="tour_cities">
                        <option value="">{{ trans('tour::tour_schedules.schedule_destinations') }}</option>
                        @if(Request::get('tour_place'))
                            @foreach(\Modules\Tour\Entities\TourProperty::where(['type' => 'tour_place', 'parent_id' => Request::get('tour_place')])->pluck('name', 'id') as $id => $name)
                                <option {{ Request::get('tour_cities') == $id ? "selected" : "" }} value="{{ $id }}"> {{ $name }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-2 tour-select tour-select--cat mb-m-15">
                    <select name="tour_list" class="form-control select2">
                        <option value="">{{ trans('theme::frontend.tours.tour_list') }}</option>
                        @foreach(\Modules\Tour\Entities\TourProperty::where('type', 'tour_list')->get() as $item)
                            <option {{ Request::get('tour_list') == $item->id || isset($slugChild) && $slugChild == $item->slug  ? "selected" : "" }} value="{{ $item->id }}">{{ $item->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-2 tour-select tour-select--cat mb-m-15">
                    <select name="tour_price" class="form-control select2">
                        <option value="">Khoảng giá</option>
                        @foreach(config('theme.list_price') as $id => $name)
                            <option {{ Request::get('tour_price') == $id ? "selected" : "" }} value="{{ $id }}">{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
                <input type="hidden" name="tour_order" id="tour_order" value="{{ !empty(Request::get('tour_order')) ? Request::get('tour_order') : '' }}"/>
                <div class="col-xs-12 col-md-2">
                    <button type="submit" class="button-tour">Tìm kiếm</button>
                </div>
            </div>
        </form>
    </div>
</div>