<div class="top-footer">
    <div class="container">
        <div class="hk-block hk-tour">
            <div class="hk-title">
                <h2>Địa điểm du lịch</h2>
            </div>
            <ul class="row">
                @foreach($newsTypeDes['newsTypes'] as $itemType)
                    <li class="col-xs-6 col-sm-4 col-md-2">
                        <a href="{{ url($newsTypeDes['newsTypeParent']->slug . '/' . $itemType->slug) }}">{{ $itemType->title }}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>