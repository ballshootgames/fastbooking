@extends('theme::eagle.frontend.master')

@section('breadcrumb')
    <div class="bearch">
        <div class="container">
            <p id="breadcrumbs">
                <i class="fa fa-home"></i>
                <span>
                    <span><a href="{{ url('/') }}">{{ trans('theme::eagles.home') }}</a>
                        <i class="fa fa-angle-double-right"></i> <span class="breadcrumb_last">Giỏ hàng</span>
                    </span>
                </span>
            </p>
        </div>
    </div>
@endsection

@section('styles')
    <link rel="stylesheet" id="woocommerce-general-css" href="https://dulichdaibang.com/wp-content/plugins/woocommerce/assets/css/woocommerce.css?ver=3.2.6" type="text/css" media="all">
    <link rel="stylesheet" id="woocommerce-layout-css" href="https://dulichdaibang.com/wp-content/plugins/woocommerce/assets/css/woocommerce-layout.css?ver=3.2.6" type="text/css" media="all">
    <style>
        .fa-loading {
            position: absolute;
            left: 0;
            right: 0;
            top: 50%;
            z-index: 1000;
        }
        .post-content .woocommerce {
            position: relative;
        }
        #overlay {
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgba(255,255,255,0.6);
            z-index: 4;
            cursor: pointer;
        }
        .woocommerce-message::before{
            display: none;
        }
        .wc-terms-and-conditions{
            margin-left: 20px !important;
        }
        .wc-terms-and-conditions .checkbox{
            margin-top: 0;
            display: flex;
        }
        .wc-terms-and-conditions .woocommerce-form__checkbox{
            padding-right: 10px;
            padding-top: 5px;
        }
        .wc-terms-and-conditions .woocommerce-form__checkbox .terms{
            width: 20px;
            height: 20px;
        }
    </style>
@endsection

@section('content')
    @php($linkSocial = Request::fullUrl())
    <section class="single">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-9 col-md-push-3">
                    <div class="single-page">
                        <div class="hk-title">
                            <h1 class="title-single">Thanh toán</h1>
                        </div>
                        <article class="post-content">
                            <div class="mailmunch-forms-before-post" style="display: none !important;"></div>
                            @if(Session::has('paypal_error'))
                                <div class="alert alert-danger">
                                    <span><i class="fa fa-exclamation-circle" aria-hidden="true" style="padding-right: 10px"></i></span>{{ Session::get('paypal_error') }}
                                </div>
                                <?php Session::forget('paypal_error');?>
                            @elseif(Session::has('paypal_success'))
                                @if(Session::has('paypal_success_error'))
                                    <div class="alert alert-success">
                                        <span><i class="fa fa-check" aria-hidden="true" style="padding-right: 10px"></i></span>{{ Session::get('paypal_success_error') }}
                                    </div>
                                    <?php Session::forget('paypal_success_error');?>
                                @endif
                                <div class="woocommerce">
                                    <div class="woocommerce-order">
                                        <p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received">Đơn hàng của bạn đã được nhận, hãy ghi lại mã đơn hàng để tiện cho việc tìm kiếm sau này nhé</p>
                                        @foreach(Session::get('paypal_success') as $booking_id)
                                            @php($booking = \Modules\Booking\Entities\Booking::find($booking_id))
                                            <ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details">
                                                <li class="woocommerce-order-overview__order order">
                                                    Mã đơn hàng: <strong>{{ $booking->code }}</strong>
                                                </li>
                                                <li class="woocommerce-order-overview__date date">
                                                    Ngày: <strong>{{ Carbon\Carbon::parse($booking->created_at)->format(config('settings.format.date')) }}</strong>
                                                </li>
                                                <li class="woocommerce-order-overview__total total">
                                                    Tổng cộng: <strong><span class="woocommerce-Price-amount amount">{{ number_format($booking->total_price) }}&nbsp;<span class="woocommerce-Price-currencySymbol"> VND </span></span></strong>
                                                </li>
                                                <li class="woocommerce-order-overview__payment-method method">
                                                    Phương thức thanh toán: <strong>{{ optional($booking->payment)->name }}</strong>
                                                </li>
                                                <li class="woocommerce-order-overview__payment-method method">
                                                    Trạng thái: <strong>{{ optional($booking->status)->name }}</strong>
                                                </li>
                                            </ul>
                                        @endforeach
                                    </div>
                                </div>
                                <?php Session::forget('paypal_success');?>
                            @endif
                                <div class="woocommerce">
                                    <form class="checkout_coupon" method="post" style="display:none">
                                        <p class="form-row form-row-first">
                                            <input type="text" name="coupon_code" class="input-text" placeholder="Mã ưu đãi" id="coupon_code" value="">
                                        </p>
                                        <p class="form-row form-row-last">
                                            <input type="submit" class="button" name="apply_coupon" value="Áp dụng mã ưu đãi">
                                        </p>
                                        <div class="clear"></div>
                                    </form>
                                    <form name="checkout" method="post" class="checkout woocommerce-checkout" action="{{ url('cart/checkout/tour') }}" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="col2-set" id="customer_details">
                                            <div class="col-1">
                                                <div class="woocommerce-billing-fields">
                                                    <h3>Thông tin thanh toán</h3>
                                                    <div class="woocommerce-billing-fields__field-wrapper">
                                                        <?php $user = Auth::check() ? Auth::user() : null ?>
                                                        <p class="form-row form-row-first validate-required" id="billing_first_name_field" data-priority="30">
                                                            <label for="billing_first_name" class="">Họ và tên <abbr class="required" title="bắt buộc">*</abbr></label>
                                                            <input type="text" class="input-text" required name="customer[name]" placeholder="" value="{{ !empty($user) ? $user->name : '' }}" autocomplete="given-name" autofocus="autofocus">
                                                        </p>
                                                        <p class="form-row form-row-wide address-field" id="billing_address_1_field" data-priority="50">
                                                            <label for="billing_address_1" class="">Địa chỉ</label>
                                                            <input type="text" class="input-text" name="customer[address]" placeholder="Số nhà và tên đường" value="{{ !empty($user) ? optional($user->profile)->address : '' }}">
                                                        </p>
                                                        {{--<p class="form-row form-row-wide address-field validate-required" id="billing_city_field" data-priority="70" data-o_class="form-row form-row-wide address-field validate-required">--}}
                                                        {{--<label for="billing_city" class="">Tỉnh / Thành phố <abbr class="required" title="bắt buộc">*</abbr></label>--}}
                                                        {{--<input type="text" class="input-text " name="billing_city" id="billing_city" placeholder="" value="" autocomplete="address-level2">--}}
                                                        {{--</p>--}}
                                                        <p class="form-row form-row-first validate-required validate-phone" id="billing_phone_field" data-priority="100">
                                                            <label for="billing_phone" class="">Số điện thoại <abbr class="required" title="bắt buộc">*</abbr></label>
                                                            <input type="tel" class="input-text" required name="customer[phone]" placeholder="" value="{{ !empty($user) ? optional($user->profile)->phone : '' }}">
                                                        </p>
                                                        <p class="form-row form-row-last validate-email" id="billing_email_field" data-priority="110">
                                                            <label for="billing_email" class="">Địa chỉ email</label>
                                                            <input type="email" class="input-text" name="customer[email]" placeholder="" value="{{ !empty($user) ? $user->email : '' }}">
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <div class="woocommerce-shipping-fields">
                                                    <h3>Thông tin bổ sung</h3>
                                                    <div class="woocommerce-additional-fields__field-wrapper">
                                                        <p class="form-row notes" id="order_comments_field" data-priority="">
                                                            <label for="order_comments" class="">Ghi chú đơn hàng</label>
                                                            <textarea name="note" class="input-text " id="order_comments"
                                                                      placeholder="Ghi chú về sản phẩm, ví dụ: thời gian hay chỉ dẫn địa điểm giao hàng chi tiết hơn."
                                                                      rows="2"></textarea>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <h3 id="order_review_heading">Đơn hàng của bạn</h3>
                                        <div id="order_review" class="woocommerce-checkout-review-order">
                                            <table class="shop_table woocommerce-checkout-review-order-table">
                                                <thead>
                                                <tr>
                                                    <th class="product-name">Sản phẩm</th>
                                                    <th class="product-total">Tổng cộng</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($carts['products'] as $key => $tour)
                                                    <tr class="cart_item">
                                                        <td class="product-name">
                                                            {{ $tour['item']->name }}
                                                            <strong class="product-quantity">× {{ $tour['qty'] }}</strong>
                                                        </td>
                                                        <td class="product-total">
                                                        <span class="woocommerce-Price-amount amount">{{ number_format($tour['qty'] * $tour['item']->price) }}&nbsp;
                                                            <span class="woocommerce-Price-currencySymbol"> VND </span>
                                                        </span>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                                <tfoot>
                                                <tr class="order-total">
                                                    <th>Tổng cộng</th>
                                                    <td>
                                                        <strong>
                                                            <span class="woocommerce-Price-amount amount">{{ number_format($carts['totalPrice']) }}&nbsp;
                                                                <span class="woocommerce-Price-currencySymbol"> VND </span>
                                                            </span>
                                                        </strong>
                                                    </td>
                                                </tr>
                                                </tfoot>
                                            </table>
                                            <div id="payment" class="woocommerce-checkout-payment">
                                                <ul class="wc_payment_methods payment_methods methods">
                                                    @foreach(\App\PaymentMethod::orderBy('index')->get() as $key => $item)
                                                        <li class="wc_payment_method payment_method_cod">
                                                            <input id="payment_method_{{ $item->code }}" type="radio" class="input-radio" {{ $item->index === 1 ? 'checked' : '' }}  name="payment_method" value="{{ $item->code }}" data-order_button_text="">
                                                            <label for="payment_method_{{ $item->code }}">
                                                                {{ $item->name }}
                                                                @if($item->code === config('booking.payment_method.paypal'))
                                                                    <img src="https://dulichdaibang.com/wp-content/plugins/woocommerce/includes/gateways/paypal/assets/images/paypal.png"
                                                                         alt="Dấu chấp nhận thanh toán PayPal">
                                                                    <a href="https://www.paypal.com/vn/webapps/mpp/paypal-popup" class="about_paypal"
                                                                       onclick="javascript:window.open('https://www.paypal.com/vn/webapps/mpp/paypal-popup','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;">
                                                                        Paypal là gì?
                                                                    </a>
                                                                @endif
                                                            </label>
                                                            {{--<div class="payment_box payment_method_cod" style="display:none;">--}}
                                                            {{--<p>Chỉ tiến hành giữ chỗ – Thanh toán sau, Du Lịch Đại Bàng sẽ liên hệ lại và gửi đến bạn phiếu xác nhận dịch vụ – Nhân viên của Du Lịch Đại Bàng sẽ thu tiền sau khi bạn nhận được phiếu xác nhận dịch vụ này, đừng lo chúng tôi sẽ thu phí rất nhỏ thôi.</p>--}}
                                                            {{--</div>--}}
                                                        </li>
                                                    @endforeach
                                                    {{--<div class="payment_box payment_method_paypal">--}}
                                                    {{--<p>Trả thông qua Paypal; bạn có thể thanh toán với thẻ tín dụng nếu bạn không có tài khoản PayPal.</p>--}}
                                                    {{--</div>--}}
                                                </ul>
                                                <div class="form-row place-order">
                                                    <div class="woocommerce-terms-and-conditions" id="woocommerce-terms-and-conditions" style="display: none; max-height: 200px; overflow: auto;">
                                                        <ol style="text-align: justify;">
                                                            <li><strong>CHÍNH SÁCH THAY ĐỔI/ HỦY DỊCH VỤ</strong></li>
                                                        </ol>
                                                        <p style="text-align: justify;">– Mỗi Dịch vụ cũng như đơn vị tổ chức sẽ có quy định thay đổi/hủy, Khách hàng cần tuân thủ và thực hiện theo đúng chính sách thay đổi/hủy của từng sản phẩm. Chính sách này sẽ được quy định tại phần thông tin chi tiết của mỗi đơn vị tổ chức và trong đơn đặt dịch vụ online của Khách hàng. Khách hàng chỉ được đổi/hủy khi có đủ các điều kiện được quy định tại chính sách thay đổi/hủy của VINAVIVU.COM.</p>
                                                        <p style="text-align: justify;">– Trong trường hợp Khách hàng không thay đổi/hủy dịch vụ thành công và không sử dụng dịch đã đặt thì Khách hàng vẫn phải thanh toán toàn bộ tiền cho đơn đặt dịch vụ đó.</p>
                                                        <ol style="text-align: justify;" start="2">
                                                            <li><strong>CHÍNH SÁCH ĐẢM BẢO HOÀN TIỀN</strong></li>
                                                        </ol>
                                                        <p style="text-align: justify;"><strong>2.1 ĐIỀU KIỆN HOÀN TIỀN</strong></p>
                                                        <p style="text-align: justify;">–&nbsp;Chỉ áp dụng cho các dịch vụ có dòng chữ “Đảm bảo Hoàn tiền” trên website dulichdaibang.com;</p>
                                                        <p style="text-align: justify;">–&nbsp;Chỉ áp dung cho những đơn đặt dịch vụ thành công, đơn đặt dịch vụ thành công được tính kể từ thời điểm Khách hàng nhận được phiếu nhận phòng từ nhân viên chăm sóc khách hàng của dulichdaibang.com qua email;</p>
                                                        <p style="text-align: justify;">–&nbsp;Thông tin khiếu nại chỉ được dulichdaibang.com giải quyết và hoàn tiền khi được Khách hàng gọi điện hoặc gửi email thông báo trong vòng 24 giờ kể từ thời điểm bắt đầu dịch vụ;</p>
                                                        <p style="text-align: justify;">–&nbsp;Khách hàng cung cấp bằng chứng để chứng minh khiếu nại của Khách hàng là đúng, chính xác và có cơ sở;</p>
                                                        <p style="text-align: justify;"><strong>2.2 CÁC TRƯỜNG HỢP ĐƯỢC DULICHDAIBANG.COM HOÀN TIỀN</strong></p>
                                                        <p style="text-align: justify;">– Dịch vụ không cung cấp được đầy đủ theo đúng thông tin trong đơn đặt dịch vụ mà Khách hàng đã thanh toán thành công thì dulichdaibang.com cam kết sẽ hoàn lại số tiền tiền dịch vụ chi tiết tương ứng mà dulichdaibang.comkhông cung cấp được hoặc không cung cấp đủ mà Khách hàng đã thanh toán và tặng kèm một phiếu giảm giá có giá trị bằng với số tiền Khách hàng đã thanh toán.</p>
                                                        <p style="text-align: justify;"><strong>2.3 QUY TRÌNH GIẢI QUYẾT KHIẾU NẠI VÀ HOÀN TIỀN CHO KHÁCH HÀNG</strong></p>
                                                        <ol style="text-align: justify;">
                                                            <li><strong>Quy trình giải quyết khiếu nại</strong></li>
                                                        </ol>
                                                        <p style="text-align: justify;">–&nbsp;&nbsp;Khi phát sinh các trường hợp nêu tại mục 2.2 nêu trên, Khách hàng vui lòng gọi điện thoại tới số 0234.393.67.87 hoặc email tới địa chỉ booking@dulichdaibang.com&nbsp;và. Nội dung khiếu nại cần phải cung cấp mã đơn hàng. Thông tin khiếu nại chỉ được dulichdaibang.com giải quyết khi được Khách hàng gọi điện hoặc gửi email phản hồi trong vòng 24 giờ kể từ thời điểm Khách hàng nhận phòng. Hotline điện thoại chỉ hỗ trợ trong giờ hành chính.</p>
                                                        <p style="text-align: justify;">– Khi nhận được khiếu nại từ Khách hàng, dulichdaibang.com có quyền kiểm tra lại tính xác thực của các yêu cầu khiếu nại từ phía Khách hàng. Nếu Khách hàng không cung cấp được bằng chứng xác thực hoặc dulichdaibang.com đã xác nhận lại và thấy thông báo của Khách hàng không chính xác thì dulichdaibang.com có quyền từ chối giải quyết. Trong trường hợp có tranh chấp hoặc xung đột giữa các bên liên quan thì quyết định của dulichdaibang.com là quyết định cuối cùng để giải quyết khiếu nại.</p>
                                                        <p style="text-align: justify;">– Thời gian giải quyết khiếu nại của Khách hàng tối đa là 3 ngày làm việc kể từ ngày nhận được khiếu nại.</p>
                                                        <ol style="text-align: justify;">
                                                            <li><strong>Hoàn tiền và phiếu giảm giá hoặc mã giảm giá</strong></li>
                                                        </ol>
                                                        <p style="text-align: justify;">–&nbsp;Thời gian hoàn tiền cho Khách hàng trong vòng 3 ngày làm việc kể từ ngày dulichdaibang.com xác nhận về kết quả giải quyết khiếu nại cho Khách hàng.</p>
                                                        <p style="text-align: justify;">–&nbsp;Quy định về phiếu giảm giá và/hoặc mã giảm giá:</p>
                                                        <ul style="text-align: justify;">
                                                            <li>Mã giảm giá được gửi qua email cho Khách hàng;</li>
                                                            <li>Mã giảm giá chỉ được sử dụng 1 lần, không được quy đổi thành tiền mặt, không áp dụng trong các ngày lễ tết, không áp dụng đồng thời cùng các chương trình khuyến mại khác;</li>
                                                            <li>Mã giảm giá&nbsp;có hạn sử dụng được ghi kèm theo trong email dulichdaibang.com gửi;</li>
                                                            <li>Nếu đơn đặt hàng bị hủy, mã giảm giá sẽ không được hoàn lại.</li>
                                                        </ul>
                                                        <p style="text-align: justify;"><strong>2.4&nbsp;CÁC TRƯỜNG HỢP MIỄN TRỪ TRÁCH NHIỆM CỦA DULICHDAIBANG.COM</strong></p>
                                                        <p style="text-align: justify;">–&nbsp;dulichdaibang.com được miễn trừ trách nhiệm khi xảy ra một trong các trường hợp bất khả kháng theo quy định của pháp luật.</p>
                                                        <p style="text-align: justify;">–&nbsp;Dulichdaibang.com được miễn trừ trách nhiệm trong việc bồi thường thiệt hại gián tiếp, ngẫu nhiên, hoặc do hậu quả phát sinh.</p>
                                                        <p style="text-align: justify;">–&nbsp;Trong mọi trường hợp, trách nhiệm bồi thường của dulichdaibang.com không vượt quá tổng số tiền Khách hàng đã thanh toán cho đơn đặt dịch vụ thành công bị vi phạm.</p>
                                                        <p style="text-align: justify;">
                                                        </p>
                                                    </div>
                                                    <p class="form-row terms wc-terms-and-conditions">
                                                        <label class="checkbox">
                                                        <span class="woocommerce-form__checkbox">
                                                            <input type="checkbox" required class="terms" name="terms" id="terms">
                                                        </span>
                                                            <span>Tôi đã đọc và chấp nhận
                                                            <a href="#" target="_blank" id="terms-link">
                                                                các điều khoản &amp; điều kiện
                                                            </a>
                                                            <span class="required">*</span>
                                                        </span>
                                                        </label>
                                                    </p>
                                                    <input type="submit" class="button alt" id="place_order" value="Tiến hành thanh toán" data-value="Đặt hàng">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            <p></p>
                            <div class="mailmunch-forms-in-post-middle" style="display: none !important;"></div>
                            <div class="mailmunch-forms-after-post" style="display: none !important;"></div>
                        </article>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-3 col-md-pull-9">
                    @include('theme::eagle.frontend.layouts.sidebar-right')
                </div>

            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(function () {
            $("#terms-link").click( function(e){
                e.preventDefault();
                $('#woocommerce-terms-and-conditions').slideToggle('400');
            });
        })
    </script>
@endsection
