<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="geo.position" content="Huế"/>
    <meta name="geo.region" content="VN"/>
    @section('title')
        <title>{{ $settings['seo_title'] }}</title>
        <META NAME="KEYWORDS" content="{{ $settings['seo_keywords'] }}"/>
        <meta name="description" content="{{ $settings['seo_description'] }}"/>
    @show
    <link rel="shortcut icon" href="{{ !empty($settings['favicon']) ? asset(Storage::url($settings['favicon'])) : url('favicon.ico') }}" />
    <link rel="canonical" href="{{ Request::fullUrl() }}"/>
    <meta property="og:locale" content="{{ app()->getLocale() }}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="{{ Request::fullUrl() }}"/>
    <meta property="og:site_name" content="{{ $settings['seo_title'] }}"/>
    @section('facebook')
        <meta property="og:title" content="{{ $settings['seo_title'] }}"/>
        <meta property="og:description" content="{{ $settings['seo_description'] }}"/>
        <meta property="og:image" content="{{ asset(Storage::url($settings['url_logo'])) }}"/>
        <meta property="og:image:secure_url" content="{{ asset(Storage::url($settings['url_logo'])) }}"/>
    @show
    <meta property="og:image:width" content="458"/>
    <meta property="og:image:height" content="239"/>
    <meta property="fb:app_id" content="655619694879300"/>
    <meta property="fb:admins" content="100007518978857" />
    <meta name="twitter:card" content="summary"/>
    @section('twitter')
        <meta name="twitter:title" content="{{ $settings['seo_title'] }}"/>
        <meta name="twitter:description" content="{{ $settings['seo_description'] }}"/>
        <meta name="twitter:image" content="{{ asset(Storage::url($settings['url_logo'])) }}"/>
    @show
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700|Roboto+Condensed:400,700|Roboto:300,400,500,700&display=swap&subset=latin-ext,vietnamese" />
    <link rel="stylesheet" type="text/css" href="{{ url(mix('css/web-eagle.css')) }}"/>
    @yield('styles')
    <style>
        .is-invalid{
            border-color: #e3342f !important;
        }
        .hk-menu {
            display: flex;
        }
        .hk-menu ul.menu li ul{
            top: 150%;
        }
        .hk-menu ul.menu li ul li ul{
            right: 200px;
        }
        .hk-menu ul.menu li ul li:hover>ul {
            top: 0;
        }
        @media (max-width: 991px){
            .hk-menu ul.menu {
                display: none;
            }
        }
    </style>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-151745410-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-151745410-1');
    </script>
<body class="home blog">
@include('theme::layouts.facebook-script')
<div id="wrapper">
    @include('theme::eagle.frontend.layouts.header')
    <div class="header-nav">
        <div class="header-bot">
            <div class="container header-container">
                <div class="hk-logo">
                    <a href="{{ url('/') }}"><img src="{{asset(Storage::url($settings['url_logo']))}}" alt="{{ $settings['website_name']  }}"/></a>
                </div>
                <div class="hk-menu ml-auto">
                    <a href="#menu-mobile" class="menu-mobile">Menu</a>
                    @php
                        $menu = new \Modules\Theme\Entities\Menu();
                        $menu->showMainMenu($mainMenus, Request::url());
                    @endphp
{{--                    <ul id="main-nav" class="menu">--}}
{{--                        <li class="more-menu">--}}
{{--                            <a href="#"><i class="fa fa-bars"></i></a>--}}
{{--                            <ul class="sub-menu">--}}
{{--                                @foreach($typeTourLists as $itemTour)--}}
{{--                                    <li>--}}
{{--                                        <a href="{{ url('loai-tour/' . $itemTour->slug) }}">{{ $itemTour->name }}</a>--}}
{{--                                    </li>--}}
{{--                                @endforeach--}}
{{--                            </ul>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        @section('slider')
        @show
    </div>
    @section('breadcrumb')
    @show
    @yield('content')
    @yield('content_destination')
</div>
@include('theme::eagle.frontend.layouts.footer')
<div id="modalLogin" class="modal fade hs-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">{{ __('theme::eagles.login') }}</h4>
            </div>
            <div class="modal-body">
                <div class="modal-img text-center">
                    <img src="{{asset(Storage::url($settings['url_logo']))}}" alt="{{ $settings['website_name']  }}" width="200px"/>
                </div>
                <div class="modal-form">
                    <form action="" method="post" class="modal-form__login" id="hs-modal__login">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" placeholder="{{ trans('message.user.username_email') }}" name="username" >
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                            <p id="username" class="text-danger"></p>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="password" class="form-control" placeholder="Mật khẩu" name="password" >
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                            <p id="password" class="text-danger"></p>
                        </div>
                        <div id="form-error"></div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary modal-btn">Đăng nhập</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="modalRegister" class="modal fade hs-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">{{ __('theme::eagles.register') }}</h4>
            </div>
            <div class="modal-body">
                <div class="modal-img text-center">
                    <img src="{{asset(Storage::url($settings['url_logo']))}}" alt="{{ $settings['website_name']  }}" width="200px"/>
                </div>
                <div class="modal-form">
                    <form action="" method="post" id="hs-modal__register">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" placeholder="{{ trans('adminlte_lang::message.fullname') }}*" name="name" value="{{ old('name') }}" required autofocus/>
                            <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" placeholder="{{ trans('adminlte_lang::message.username') }}*" name="username" value="{{ old('username') }}" required/>
                            <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="email" class="form-control" placeholder="{{ trans('adminlte_lang::message.email') }}*" name="email" value="{{ old('email') }}" required/>
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="password" class="form-control" placeholder="{{ trans('adminlte_lang::message.password') }}*" name="password" required/>
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="password" class="form-control" placeholder="{{ trans('adminlte_lang::message.retypepassword') }}*" name="password_confirmation" required/>
                            <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <div class="checkbox-register text-center">
                                <label class="checkbox-register__label">
                                    <input type="checkbox" name="terms" class="checkbox-register__terms" required id="check_register"/>
                                    <span class="checkbox-register__checkmark"></span>
                                </label>
                                <button type="button" class="btn btn-link text-link" data-toggle="modal" data-target="#termsModal">{{ trans('adminlte_lang::message.terms') }}</button>
                            </div>
                        </div>
                        <div id="form-register-error"></div>
                        <div class="form-group text-center modal-btn__register">
                            <button type="submit" class="btn btn-primary modal-btn" disabled>{{ trans('adminlte_lang::message.register') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{ url(mix('js/web-eagle.js')) }}"></script>
@yield('scripts')
<script>
    $ = jQuery.noConflict();
    (function ($) {
        $('#tour-place').on('change', function(){
            let idPlace = $(this).val();
            $("#tour_cities").html('<option value="">{{ __('message.loading') }}</option>');
            $.ajax({
                url: '{{ url('ajax/searchComplete?idPlace=') }}'+idPlace,
                type: 'GET',
                success: function (response) {
                    $('#tour_cities').html('<option value="">{{ __('tour::tour_schedules.schedule_destinations') }}</option>');
                    $.each(response, function(key, item){
                        $('#tour_cities').append('<option value="'+ item.id +'">'+ item.name +'</option>');
                    });
                },
                error: function () {
                    alert("Đã có lỗi xả ra, vui lòng thử lại!");
                }
            });
        });
        $('#tour-category').change(function () {
            let typeId = $(this).val();
            $.ajax({
                url: '{{ url('ajax/getTourPlace?type_id=') }}' + typeId,
                type: 'GET',
                dataType: 'JSON',
                success: function(respon){
                    $("#tour-place").html('');
                    $("#tour-place").html('<option value="">{{ __('Chọn điểm đến') }}</option>');
                    $.each(respon, (key, value) => {
                        $("#tour-place").append('<option value="'+ key +'">'+ value +'</option>');
                    })
                },
                error: function () {
                    alert('Có lỗi xảy ra, vui lòng thử lại!');
                }
            })
        });
        $('.btn-dropdown').on('click', () => {
             $('.dropdown-menu').toggle();
        });
        $('#hs-modal__login').submit(function (e) {
            e.preventDefault();
            $.ajax({
                url: '{{ url('login-user/ajax') }}',
                type: 'POST',
                dataType: 'JSON',
                data: $(this).serialize(),
                success: function(res){
                    if (res.success === 'ok') {
                        window.location.href = '{{url('/home')}}';
                    }else{
                        let errorsHtml = '<div class="alert alert-danger">';
                            errorsHtml += res.error;
                            errorsHtml += '</div>';
                        $('#form-error').html(errorsHtml);
                    }
                },
                error: function (res) {
                    let errors = res.responseJSON.errors;
                    $.each(errors, function (key, value) {
                        $("input[name='"+key+"']").addClass('is-invalid');
                        $('#' + key).html(value);
                    })
                }
            });
        });
        $('#check_register').change(function () {
            if(this.checked) {
                $('.modal-btn__register button').prop('disabled', false);
            }else{
                $('.modal-btn__register button').prop('disabled', true);
            }
        });
        $('#hs-modal__register').submit(function (e) {
            e.preventDefault();
            $.ajax({
                url: '{{ url('register-user/ajax') }}',
                type: 'POST',
                dataType: 'JSON',
                data: $(this).serialize(),
                success: function (data) {
                    window.location.reload();
                },
                error: function(data){
                    let errors = data.responseJSON.errors;
                    let errorsHtml = '<div class="alert alert-danger"><ul>';
                    $.each(errors, function (key, value) {
                        errorsHtml += '<li>' + value[0] + '</li>';
                    });
                    errorsHtml += '</ul></div>';
                    $('#form-register-error').html(errorsHtml);
                }
            });
        });
        $('#contact').submit(function(e){
            e.preventDefault();
            $.ajax({
                url: '{{ url('/lien-he/ajax') }}',
                type: 'POST',
                dataType: 'JSON',
                data: $(this).serialize(),
                success: function (res) {
                    if (res.success === "ok"){
                        $('.hs-contact-frm').hide();
                        $('.hs-form-success').show();
                    }else{
                        console.log(data.errors);
                    }
                },
                error: function (error) {
                    alert("{{ __('theme::eagles.error') }}");
                }
            });
        });
    })(jQuery);
</script>
</body>
</html>