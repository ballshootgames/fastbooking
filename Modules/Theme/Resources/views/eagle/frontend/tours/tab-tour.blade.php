<div class="container">
    <br>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-9">
            <div role="tabpanel">
                <ul id="nav-tabs" class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#tab" aria-controls="tab" role="tab"
                                                              data-toggle="tab" aria-expanded="false">{{ trans('theme::frontend.tours.tour_program') }}</a></li>
{{--                    @if($tour->schedules->count() > 1)--}}
{{--                        <li role="presentation" class=""><a href="#khoihanh" aria-controls="khoihanh" role="tab"--}}
{{--                                                            data-toggle="tab" aria-expanded="false">{{ trans('theme::frontend.tours.tour_departure_schedule') }}</a></li>--}}
{{--                    @endif--}}
                    <li role="presentation" class=""><a href="#ghichu" aria-controls="ghichu" role="tab"
                                                        data-toggle="tab" aria-expanded="false">{{ trans('theme::frontend.tours.tour_note') }}</a></li>
{{--                    <li role="presentation" class=""><a href="#comment" aria-controls="comment" role="tab"--}}
{{--                                                        data-toggle="tab" aria-expanded="false">{{ trans('theme::frontend.tours.tour_idea_customer') }} ({{ $countComment ?? 0 }})</a></li>--}}
                    @if(count($tour->medias))
                        <li>
                            <a href="#" data-toggle="modal" data-target="#modal-download">
                                <i class="fa fa-file-pdf-o" aria-hidden="true" style="font-size: 16px;"></i> {{ trans('theme::frontend.tours.tour_download_schedule') }}
                            </a>
                        </li>
                    @endif
                </ul>
                <div class="tab-content tour__content">
                    <div role="tabpanel" class="tab-pane active" id="tab">
                        <article class="post-content">
                            <div class="shcedule__intro">
                                @empty(!$tour->intro)
                                    {!! $tour->intro !!}
                                @endempty
                            </div>
                            @if($tour->schedules->count() > 1)
                                @foreach($tour->schedules as $schedule)
                                    <div class="schedule" id="schedule">
                                        <div class="schedule__day">Ngày {{ $schedule->day }}</div>
                                        <div class="schedule__desc">
                                            <div class="schedule__desc_border"></div>
                                            <h3 class="schedule__desc_title">{{ $schedule->name }}</h3>
                                            <div class="schedule__desc_content">
                                                {!! $schedule->content !!}
                                            </div>
                                            <a href="#" class="schedule-btn"></a>
                                            <div class="schedule__desc_des">
                                                <span><b><u>{{ trans('tour::tour_schedules.schedule_destinations') }}:</u></b></span>
                                                @php($i = 0)
                                                @php($count = count($cityPlaces = $schedule->cities()->pluck('city_id')->all()))
                                                @foreach($cityPlaces as $destination)
                                                    @php($i++)
                                                    @php($s = $count == $i ? '' : ',')
                                                    @if($cityDestination = \Modules\Tour\Entities\TourProperty::where([ ['id', '=', $destination], ['parent_id', '<>', null]])->first())
{{--                                                        {!! '<a href="'.url('/diem-tham-quan/'.$cityDestination->slug).'.html">'.$cityDestination->name.'</a>' . $s !!}--}}
                                                        {!! '<a href="#">'.$cityDestination->name.'</a>' . $s !!}
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class="shcedule__intro">
                                    {!! empty(!optional($tour->schedules)->first()) ? optional($tour->schedules)->first()->content : '' !!}
                                    @if(!empty(optional($tour->schedules)->first()))
                                        @php($count = count($cityPlaces = optional($tour->schedules)->first()->cities()->pluck('city_id')->all()))
                                        @if($count > 0)
                                        <div class="schedule__desc_des">
                                            <span><b><u>{{ trans('tour::tour_schedules.schedule_destinations') }}:</u></b></span>
                                            @php($i = 0)

                                                @foreach($cityPlaces as $destination)
                                                    @php($i++)
                                                    @php($s = $count == $i ? '' : ',')
                                                    @if($cityDestination = \Modules\Tour\Entities\TourProperty::where([ ['id', '=', $destination], ['parent_id', '<>', null]])->first())
                                                        {{--                                                        {!! '<a href="'.url('/diem-tham-quan/'.$cityDestination->slug).'.html">'.$cityDestination->name.'</a>' . $s !!}--}}
                                                        {!! '<a href="#">'.$cityDestination->name.'</a>' . $s !!}
                                                    @endif
                                                @endforeach
                                            @endif
                                        </div>
                                    @endif
                                </div>
                            @endif
                        </article>
                    </div>
{{--                    <div role="tabpanel" class="tab-pane" id="khoihanh">--}}
{{--                        <article class="post-content">--}}
{{--                            {!! $tour->departure_schedule !!}--}}
{{--                        </article>--}}
{{--                    </div>--}}
                    <div role="tabpanel" class="tab-pane" id="ghichu">
                        <article class="post-content">
                            @empty(!$tour->note)
                                {!! $tour->note !!}
                            @endempty
                        </article>
                    </div>
{{--                    <div role="tabpanel" class="tab-pane" id="comment">--}}
{{--                        <article class="post-content">--}}
{{--                            <div id="overlay" style="display: none"></div>--}}
{{--                            <div class="fa-loading text-center" style="display: none">--}}
{{--                                <span class="fa fa-spinner fa-spin fa-3x text-primary" aria-hidden="true"></span>--}}
{{--                            </div>--}}
{{--                            <h4 class="comment__title text-uppercase"><i class="fa fa-commenting" aria-hidden="true"></i> Ý kiến</h4>--}}
{{--                            <div id="list-comment" class="comment__items"></div>--}}
{{--                            <h4 class="comment__title text-uppercase"><i class="fa fa-comments-o"></i> Gửi ý kiến</h4>--}}
{{--                            <form action="{{ url('admin/control/store-comment') }}" method="post" id="comment-form" class="comment__frm">--}}
{{--                                <input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
{{--                                <p class="comment-notes text-center">Cám ơn bạn đã gửi ý kiến cho chúng tôi! <br/> Chúc bạn một ngày tốt lành!</p>--}}
{{--                                <div class="row comment-hide">--}}
{{--                                    <div class="col-sm-6 form-group">--}}
{{--                                        <input type="text" class="comment__frm-control" name="user_name" required aria-required="true" placeholder="Họ tên(*)"/>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-sm-6 form-group">--}}
{{--                                        <input type="email" class="comment__frm-control" name="user_email" required aria-required="true" placeholder="Email(*)"/>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-xs-12 form-group">--}}
{{--                                        <textarea style="height: auto" name="comment" class="comment__frm-control" cols="20" rows="5" aria-required="true" required placeholder="Ý kiến của bạn(*)"></textarea>--}}
{{--                                    </div>--}}
{{--                                    <div class="form-submit text-center">--}}
{{--                                        <button name="submit" type="submit" id="submit" class="comment__frm-btn">Gửi đi <i class="fa fa-paper-plane"></i></button>--}}
{{--                                        <input type="hidden" name="tour_id" value="{{ $tour->id }}">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </form>--}}
{{--                        </article>--}}
{{--                    </div>--}}
                </div>
            </div>
            <script>
                prdTabs = document.querySelectorAll('ul.nav.nav-tabs > li');
                prdTabs.forEach(ele => ele.addEventListener('click', () => {
                    document.querySelector('div[role=tabpanel]').scrollIntoView();
                }))
            </script>
            <div class="lienhe-dang-tour">
                <div class="book-tour">
                    @if($people_limit === 0)
                        <div class="contact-tv"><p>Số lượng giới hạn đã hết, vui lòng liên hệ: <span>{{ $settings['phone_contact_advisory'] }}</span> để được tư vấn!</p></div>
                    @else
                        <div class="contact-tv"><p>Hotline: <span>{{ $settings['phone_contact_advisory'] }}</span> để được tư vấn!</p></div>
                        <div class="booking-ft">
                            {{--<form action="" method="post"><input type="hidden" name="add-to-cart" value="5141">--}}
                            {{--                                    <a href="{{ url('booking/add-item?id='.$tour->id) }}">--}}
                            {{--                                        <button class="btn btn-danger"><i class="fa fa-check-square" aria-hidden="true"></i>--}}
                            {{--                                            {{ trans('theme::frontend.tours.tour_book_free') }}--}}
                            {{--                                        </button>--}}
                            {{--                                    </a>--}}
                            <a href="{{ url('booking/add-item?id='.$tour->id) }}">
                                <button class="btn btn-primary">
                                    <i class="fa fa-cart-plus" aria-hidden="true"></i> {{ trans('theme::frontend.tours.tour_book_now') }}
                                </button>
                            </a>
                            @if(count($tour->medias))
                            <a href="#" data-toggle="modal" data-target="#modal-download">
                                <button class="btn btn-danger">
                                    <i class="fa fa-file-pdf-o" aria-hidden="true"></i> {{ trans('theme::frontend.tours.tour_download_schedule') }}
                                </button>
                            </a>
                            @endif
                            {{--</form>--}}
                        </div>
                    @endif
                    <div class="clear"></div>
                </div>
            </div>
            <div class="comment-facebook" style="margin-top: 20px;">
                @include('theme::layouts.facebook-comment', ['link' => $link])
            </div>
        </div>
        @if(!empty($tourLink->tour_right))
            @include('theme::eagle.frontend.tours.tour-link-right')
        @else
            @include('theme::eagle.frontend.tours.tour-place')
        @endif
    </div>
</div>