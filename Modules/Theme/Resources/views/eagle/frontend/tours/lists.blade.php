@extends('theme::eagle.frontend.master')
@section('title')
    <title>{{ !empty($tourProperties->name) ? $tourProperties->name : $settings['seo_title'] . ' - ' . $settings['website_name'] }}</title>
    <META NAME="KEYWORDS" content="{{ !empty($tourProperties->meta_keyword) ? $tourProperties->meta_keyword : $settings['seo_keywords'] }}"/>
    <meta name="description" content="{{ !empty($tourProperties->description) ? strip_tags(Str::limit($tourProperties->description, 200)) : $settings['seo_description'] }}"/>
@endsection
@section('breadcrumb')
    <div class="bearch">
        <div id="breadcrumbs" class="container">
            <i class="fa fa-home"></i>
            <span>
                <span>
                    <a href="{{ url('/') }}">{{ trans('theme::eagles.home') }}</a>
                    <i class="fa fa-angle-double-right"></i>
                    <span>{{ $slugParent }}</span>
                </span>
            </span>
        </div>
    </div>
@endsection
@section('content')
    <div class="hk-block hk-tour">
        <div class="container">
            <div class="hk-title hk-title-box">
                <h1>{{ !empty($tourProperties->name) ? $tourProperties->name : 'Tours' }}</h1>
                <select name="drpOrderTour" id="drpOrderTour" class="tour-order">
                    <option value="">{{ trans('theme::frontend.sort_tours.sort') }}</option>
                    <option value="asc" {{ Request::get('tour_order') === 'asc' ? 'selected' : '' }}>
                        {{ trans('theme::frontend.sort_tours.a_z') }}
                    </option>
                    <option value="desc" {{ Request::get('tour_order') === 'desc' ? 'selected' : '' }}>
                        {{ trans('theme::frontend.sort_tours.z_a') }}
                    </option>
                    <option value="ascPrice" {{ Request::get('tour_order') === 'ascPrice' ? 'selected' : '' }}>
                        {{ trans('theme::frontend.sort_tours.min_max') }}
                    </option>
                    <option value="descPrice" {{ Request::get('tour_order') === 'descPrice' ? 'selected' : '' }}>
                        {{ trans('theme::frontend.sort_tours.max_min') }}
                    </option>
                </select>
            </div>
            @include('theme::eagle.frontend.layouts.search-form-tour')
            <br/>
        </div>
        <div class="container lst-items">
            <ul class="row">
                @foreach($tours as $tour)
                    <li class="col-xs-12 col-sm-12 col-md-3">
                        <div class="detail">
                            <a href="{{ url( $slugParent . '/' . $tour->slug ) }}.html" class="img">
                                <img data-sizes="auto" class="lazyload" data-expand="-10"
                                     data-src="{{ !empty($tour->image) ? \App\Traits\ImageResize::getThumbnail($tour->image, 300, 200) : asset('img/noimage.gif') }}"
                                     alt="{{ $tour->name }}">
                                @empty(!($tour->price_discount))
                                    <span class="tour-discount">-{{ (round(((float)$tour->price_discount - (float)$tour->price) / (float)$tour->price_discount, 2) * 100) }}%</span>
                                @endempty
                                @if($tour->hot === 1) <span class="tour-hot">HOT</span> @endif
                                {{--<span class="tour-tra-gop">TRẢ GÓP</span>--}}
                                {{--<span class="tour-noi-dung-khung-phai" style="background-color: #006eb9"> Chỉ từ 750.000đ/tháng </span>--}}
                            </a>
                            <div class="txt-blk">
                                <div class="rate-price tour--price">
                                    <span style="text-decoration: line-through">
                                        @empty(!($tour->price_discount))
                                            {{ number_format($tour->price_discount) }} đ
                                        @endempty
                                    </span>
                                    <span>{{ number_format($tour->price) }} đ</span>
                                </div>
                                <h3>
                                    <a href="{{ url( $slugParent  . '/' . $tour->slug ) }}.html">{{ $tour->name }}</a></h3>
                                <span class="hk-lc">
                                    <i class="hk-ic"></i>{{ trans('theme::frontend.tours.tour_place_start') }}:
                                    <strong>{{ \Modules\Tour\Entities\TourProperty::where('id', $tour->start_city)->first()->name ?? 'Không xác định' }}</strong>
                                </span>
                                <span class="hk-lc">
                                    <i class="fa fa-calendar"></i>{{ trans('theme::frontend.tours.tour_time') }}:
                                    <strong>
                                        @if($tour->night_number !== 0)
                                            {{ $tour->day_number }} ngày - {{ $tour->night_number }} đêm
                                        @elseif($tour->day_number === 0.5)
                                            1/2 ngày
                                        @elseif($tour->night_number !== 0 && $tour->day_number === 0.5)
                                            1/2 ngày - {{ $tour->night_number }} đêm
                                        @else
                                            {{ $tour->day_number }} ngày
                                        @endif
                                    </strong>
                                </span>
                                <span class="hk-lc">
                                    <i class="fa fa-clock-o"></i>{{ trans('theme::frontend.tours.tour_day_start') }}:
                                    <strong>{{ !empty($tour->departure_date) ? Carbon\Carbon::parse($tour->departure_date)->format(config('settings.format.date')) : trans('theme::frontend.tours.tour_daily_start') }}</strong>
                                </span>
                                <a href="{{ url( $slugParent  . '/' . $tour->slug ) }}.html" class="view-detail">{{ trans('theme::frontend.tours.tour_detail') }}</a>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
        <div class="container">
            <div class="quatrang">
                {!! $tours->appends(\Request::except('page'))->render() !!}
            </div>
        </div>
    </div>
@endsection
@section('content_destination')
    @include('theme::eagle.frontend.layouts.destination')
@endsection
@section('scripts')
    <script>
        $(function () {
            $('#drpOrderTour').on('change', function(){
                let tour_order = $(this).val();
                let actForm = $('#tour-search');
                $('#tour_order').attr('value', tour_order);
                $.ajax({
                    url: actForm.attr('action'),
                    type: 'GET',
                    data: actForm.serialize(),
                    success: function (res) {
                        actForm.submit();
                    },
                    error: function () {
                        alert("Đã có lỗi xả ra, vui lòng thử lại!");
                    }
                });
            });
        });
    </script>
@endsection

