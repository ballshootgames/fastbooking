<div class="col-xs-12 col-sm-12 col-md-3">
    <div class="related-post"><h3 class="related"><span>{{ trans('theme::frontend.tours.tour_place_same') }}</span></h3>
        <div class="content-related">
            <ul class="row">
                @php
                    $tourRights = explode("$", (!empty($tourLink)) ? $tourLink->tour_right : '');
                @endphp
                @foreach($tourRights as $id)
                    @php($item = \Modules\Tour\Entities\Tour::where([['id','=', !empty($id) ? $id : ''],['active', '=', config('settings.active')]])->first())
                    <li class="col-xs-12 col-sm-12 col-md-12">
                        <a href="{{ url($slugParent . '/' .$item->slug.'.html') }}">
                            <img data-sizes="auto" class="lazyload" data-expand="-10"
                                 data-src="{{ !empty($item->image) ? \App\Traits\ImageResize::getThumbnail($item->image, 300, 200) : asset('img/noimage.gif') }}"
                                 alt="{{ $item->name }}">
                        </a>
                        <h4><a href="{{ url($slugParent . '/' .$item->slug.'.html') }}">
                                {{ $item->name }}</a></h4>
                        <div class="info-tour-rel">
                            <div class="pull-left">
                                <p>{{ trans('theme::frontend.tours.tour_time') }}:
                                    @if($item->night_number !== 0)
                                        {{ $item->day_number }} ngày - {{ $item->night_number }} đêm
                                    @elseif($item->day_number === 0.5)
                                        1/2 ngày
                                    @elseif($item->night_number !== 0 && $item->day_number === 0.5)
                                        1/2 ngày - {{ $item->night_number }} đêm
                                    @else
                                        {{ $item->day_number }} ngày
                                    @endif
                                </p>
                            </div>
                            <div class="pull-right"><p class="price">{{ number_format($item->price) }} VNĐ</p></div>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
    <div class="widget">
        <div class="textwidget">
            <p><br>
                <script type="text/javascript">var Tawk_API = Tawk_API || {},
                        Tawk_LoadStart = new Date();
                    (function () {
                        var s1 = document.createElement("script"),
                            s0 = document.getElementsByTagName("script")[0];
                        s1.async = true;
                        s1.src = 'https://embed.tawk.to/5657dc2b81505c8622dad7c0/default';
                        s1.charset = 'UTF-8';
                        s1.setAttribute('crossorigin', '*');
                        s0.parentNode.insertBefore(s1, s0);
                    })();</script>
                <br></p>
        </div>
    </div>
</div>