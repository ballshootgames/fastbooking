@extends('theme::eagle.frontend.master')

@section('breadcrumb')
    <div class="bearch">
        <div class="container">
            <p id="breadcrumbs">
                <i class="fa fa-home"></i>
                <span>
                    <span>
                        <a href="{{ url('/') }}">{{ trans('theme::eagles.home') }}</a>
                        <i class="fa fa-angle-double-right"></i>
                        <span class="breadcrumb_last">{{ $page->title }}</span>
                    </span>
                </span>
            </p>
        </div>
    </div>
@endsection
@section('content')
    <div id="content" class="child-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-9 pull-right">
                    <div class="single-page">
                        <div class="hk-title">
                            <h1 class="title-single">
                                {{ __('Kết quả tìm kiếm') }}
                            </h1>
                        </div>
                        @if(!empty($newsLists->count()))
                            @foreach($newsLists as $item)
                                <div class="list-post">
                                    <a href="{{ url( optional($item->type)->slug) . '/' . $item->slug }}.html" class="img link-img">
                                        <img data-sizes="auto" class="lazyload" data-expand="-10" data-src="{{ empty($item->image) ? asset('img/noimage.gif') : asset(Storage::url($item->image)) }}"
                                             alt="{{ $item->title }}"/>
                                    </a>
                                    <h4><a href="{{ url( optional($item->type)->slug) . '/' . $item->slug }}.html">{{ $item->title }}</a></h4>
                                    <div class="meta"><span>{{ \Carbon\Carbon::parse($item->updated_at)->format(config('settings.format.date')) }}</span></div>
                                    <article class="post-content">
                                        {!! Str::limit($item->description, 250) !!}
                                    </article>
                                    <div class="clear"></div>
                                </div>
                            @endforeach
                        @else
                            <p class="text-center" style="font-size: 16px">{{ __('Không có kết quả nào phù hợp. Vui lòng tìm kiếm từ khóa khác!') }}</p>
                        @endif
                        <div class="quatrang">
                            {!! $newsLists->appends(\Request::except('page'))->render() !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-3">
                    @include('theme::eagle.frontend.layouts.sidebar-right')
                </div>
            </div>
        </div>
    </div>
@endsection

