@extends('theme::eagle.frontend.master')
@section('title')
    <title>{{ $page->title }} - {{ $settings['website_name'] }}</title>
    <META NAME="KEYWORDS" content="{{ !empty($page->meta_keyword) ? $page->meta_keyword : $settings['seo_keywords'] }}"/>
    <meta name="description" content="{{ !empty($page->description) ? strip_tags(Str::limit($page->description, 200)) : strip_tags(Str::limit($page->content, 200))}}"/>
@endsection
@section('styles')
    <style>
        .hs-contact form{
            background-color: transparent;
        }
        .hs-contact form label{
            margin-bottom: 5px;
        }
    </style>
@endsection
@section('breadcrumb')
    <div class="bearch">
        <div class="container">
            <p id="breadcrumbs">
                <i class="fa fa-home"></i>
                <span>
                    <span>
                        <a href="{{ url('/') }}">{{ trans('theme::eagles.home') }}</a>
                        <i class="fa fa-angle-double-right"></i>
                        <span class="breadcrumb_last">{{ $page->title }}</span>
                    </span>
                </span>
            </p>
        </div>
    </div>
@endsection
@section('content')
    <div id="content" class="child-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-9 pull-right">
                    <div class="single-page">
                        <div class="hk-title">
                            <h1 class="title-single">
                                {{ $page->title }}
                            </h1>
                        </div>
                        <article class="post-content">
                            {!! $page->content !!}
                        </article>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-3">
                    @include('theme::eagle.frontend.layouts.sidebar-right')
                </div>
            </div>
        </div>
    </div>
@endsection
