@extends('theme::eagle.frontend.master')
@section('styles')
    <link rel="stylesheet" id="woocommerce-general-css" href="https://dulichdaibang.com/wp-content/plugins/woocommerce/assets/css/woocommerce.css?ver=3.2.6" type="text/css" media="all">
    <link rel="stylesheet" id="woocommerce-layout-css" href="https://dulichdaibang.com/wp-content/plugins/woocommerce/assets/css/woocommerce-layout.css?ver=3.2.6" type="text/css" media="all">
    <style>
        .fa-loading {
            position: absolute;
            left: 0;
            right: 0;
            top: 50%;
            z-index: 1000;
        }
        .post-content .woocommerce {
            position: relative;
        }
        #overlay {
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgba(255,255,255,0.6);
            z-index: 4;
            cursor: pointer;
        }
        .wc-terms-and-conditions .checkbox{
            margin-top: 0;
            display: flex;
        }
        .wc-terms-and-conditions .woocommerce-form__checkbox{
            padding-right: 10px;
            padding-top: 5px;
        }
        .wc-terms-and-conditions .woocommerce-form__checkbox .terms{
            width: 20px;
            height: 20px;
        }
        .card_active{
            background: white;
            box-shadow: 0 2px 3px rgba(0,0,0,0.5);
            border-radius: 3px;
        }
        .card_opacity {
            opacity: .5;
            background: white;
            border-radius: 3px;
        }
        .img_contain{
            color: #434a54;
            font-weight: 500;
            padding: 0;
            width:150px;
            height:67.5px;
            border: 1px solid #dcdcdf;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            line-height: 4.5em;
            display: block;
            position: relative;
            margin-bottom: 0;
            -webkit-transition: all .15s ease;
            -o-transition: all .15s ease;
            transition: all .15s ease;
            text-align: center;
            -webkit-box-shadow: 1px 2px 3px 0 rgba(0,0,0,.08);
            box-shadow: 1px 2px 3px 0 rgba(0,0,0,.08);
        }
        .card_opacity:hover{
            border: 1px solid #ea9629;
        }
        .frame-noidung {
            text-align: justify;
            word-wrap: break-word;
            overflow-y: scroll;
            line-height: 22px;
            height: 180px !important;
        }
        .woocommerce form .form-row .required
        {
            visibility: visible;
        }
    </style>
    <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datepicker/datepicker3.css') }}">
@endsection
@section('breadcrumb')
    <div class="bearch">
        <div class="container">
            <p id="breadcrumbs">
                <i class="fa fa-home"></i>
                <span>
                    <span><a href="{{ url('/') }}">{{ trans('theme::eagles.home') }}</a>
                        <i class="fa fa-angle-double-right"></i> <span class="breadcrumb_last">Đặt tour</span>
                    </span>
                </span>
            </p>
        </div>
    </div>
@endsection
@section('content')
    @php($linkSocial = Request::fullUrl())
    <section class="single">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="single-page">
                        <div class="hk-title">
                            <h1 class="title-single">Đặt tour</h1>
                        </div>
                        <article class="post-content">
                            <div class="woocommerce book-woo">
                                <div id="overlay" style="display: none"></div>
                                <div class="fa-loading text-center" style="display: none">
                                    <span class="fa fa-spinner fa-spin fa-3x text-primary" aria-hidden="true"></span>
                                </div>
                                @if(Session::has('book'))
                                    @foreach($books['tour'] as $key => $tour)
                                        <div class="row">
                                            <div class="col-md-4 col-md-4 col-lg-push-8 col-md-push-8 lst-items">
                                                <div class="booking-info detail">
                                                    <img src="{{ !empty($tour['item']->image) ? asset(Storage::url($tour['item']->image)) : asset('img/noimage.gif') }}"
                                                         alt="{{ $tour['item']->name }}">
                                                    <div class="txt-blk">
                                                        <h3><a href="{{ url('tour/'.$tour['item']->slug.'.html') }}">{{ $tour['item']->name }}</a></h3>
                                                        <p><i class="fa fa-barcode"></i> Mã tour: <b>{{ $tour['item']->prefix_code.$tour['item']->code }}</b></p>
                                                        <p><i class="fa fa-map-marker"></i> {{ trans('theme::frontend.tours.tour_place_start') }} <b>{{ \Modules\Tour\Entities\TourProperty::where('id',$tour['item']->start_city)->first()->name ?? 'Không xác định' }}</b></p>
                                                        <p><i class="fa fa-calendar" aria-hidden="true"></i> Thời gian:
                                                            <b>
                                                                @if($tour['item']->night_number !== 0)
                                                                    {{ $tour['item']->day_number }} ngày - {{ $tour['item']->night_number }} đêm
                                                                @elseif($tour['item']->day_number === 0.5)
                                                                    1/2 ngày
                                                                @elseif($tour['item']->night_number !== 0 && $tour['item']->day_number === 0.5)
                                                                    1/2 ngày - {{ $tour['item']->night_number }} đêm
                                                                @else
                                                                    {{ $tour['item']->day_number }} ngày
                                                                @endif
                                                            </b>
                                                        </p>
                                                        <div class="row" style="align-items:center; display:flex; padding-bottom: 5px;">
                                                            <div class="col-sm-6 col-lg-4" style="padding-right: 0; padding-left: 15px;"><p class="adult"><i class="fa fa-calendar" aria-hidden="true"></i> <span>Ngày khởi hành</span></p></div>
                                                            <div class="col-sm-6 col-lg-4"><input type="text" class="form-control book-qty input-sm datepicker" name="departure_date" id="departure_date" placeholder></div>
                                                        </div>
                                                        <div class="row" style="align-items:center; display:flex; padding-bottom: 5px;">
                                                            <div class="col-sm-6 col-lg-4" style="padding-right: 0; padding-left: 5px;"><p class="adult"><i class="fa fa-user-secret" aria-hidden="true" style="margin-left: 10px;"></i> Người lớn  </p></div>
                                                            <div class="col-md-3 col-lg-4"><input type="number" class="form-control book-qty" name="adult" id="nAdult" data-tour-id="{{ $tour['item']->id }}" value="{{ $tour['adult'] }}" pattern="[0-9]*" min="1"></div>
                                                            <div class="col-md-3 col-lg-4" style="padding-left: 0px;padding-right: 0px;"><b> <span id="adult">@if($tour['adult']!=0) {{ number_format($tour['item']->price * $tour['adult']) }} đ @endif</span></b></div>
                                                        </div>
                                                        <div class="row" style="align-items:center; display:flex; padding-bottom: 5px;">
                                                            <div class="col-md-6 col-lg-4" style="padding-right: 0; padding-left: 5px;"><p class="child"><i class="fa fa-child" aria-hidden="true" style="margin-left: 10px;"></i> Trẻ em  </p></div>
                                                            <div class="col-md-3 col-lg-4"><input type="number" class="form-control book-qty" name="child" id="nChild" data-tour-id="{{ $tour['item']->id }}" value="{{ $tour['child'] == 0 ? 0 : $tour['child'] }}" pattern="[0-9]*" min="0"></div>
                                                            <div class="col-md-3 col-lg-4" style="padding-left: 0px;padding-right: 0px;"><b><span id="child">@if($tour['child']!=0) {{ number_format($tour['item']->price_children * $tour['child']) }} đ @endif</span></b></div>
                                                        </div>
                                                        <div class="row" style="align-items:center; display:flex; padding-bottom: 5px;">
                                                            <div class="col-md-4" style="padding-right: 0; padding-left: 5px;"><p class="baby"><i class="fa fa-user-times" aria-hidden="true" style="margin-left: 10px;"></i> Em bé  </p></div>
                                                            <div class="col-md-3 col-lg-4"><input type="number" class="form-control book-qty" name="baby" id="nBaby" data-tour-id="{{ $tour['item']->id }}" value="{{ $tour['baby'] == 0 ? 0 : $tour['baby'] }}" pattern="[0-9]*" min="0"></div>
                                                            <div class="col-md-3 col-lg-4" style="padding-left: 0px;padding-right: 0px;"><b><span id="baby">@if($tour['baby']!=0) {{ number_format($tour['item']->price_baby * $tour['baby']) }} đ @endif</span></b></div>
                                                        </div>
                                                        <div class="row" style="align-items:center; display:flex; margin-top: 5px;">
                                                            <div class="col-md-6 col-lg-4"><b><span>Tổng </span></b></div>
                                                            <div class="col-md-3 col-lg-4"></div>
                                                            <div class="col-md-3 col-lg-4">
                                                                <b><span id="book-total">{{ isset($books['totalPrice']) && !empty($books['totalPrice']) ? number_format($books['totalPrice']) : 0 }} đ</span>
                                                                </b>
                                                            </div>
                                                        </div>
                                                        <input type="hidden" name="adult_price" id="adult_price" value={{ $tour['item']->price }} >
                                                        <input type="hidden" name="child_price" id="child_price" value={{ $tour['item']->price_children }} >
                                                        <input type="hidden" name="baby_price" id="baby_price" value={{ $tour['item']->price_baby }} >
                                                        <input type="hidden" name="hidlimit" id="hidlimit" value={{ $tour['item']->limit }} >
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-8 col-md-8 col-lg-pull-4 col-md-pull-4">
                                                <div class="woocommerce">
                                                    <form class="checkout_coupon" method="post" style="display:none;">
                                                        <p class="form-row form-row-first">
                                                            <input type="text" name="coupon_code" class="input-text" placeholder="Mã ưu đãi" id="coupon_code" value="">
                                                        </p>
                                                        <p class="form-row form-row-last">
                                                            <input type="submit" class="button" name="apply_coupon" value="Áp dụng mã ưu đãi">
                                                        </p>
                                                        <div class="clear"></div>
                                                    </form>
                                                    <form name="checkout" method="post" class="checkout woocommerce-checkout" action="{{ url('booking/checkout/tour') }}" enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        <div class="col-set" id="customer_details">
                                                            <div>
                                                                <div class="woocommerce-billing-fields">
                                                                    <h3>Thông tin liên hệ</h3>
                                                                    <div class="woocommerce-billing-fields__field-wrapper">
                                                                        <?php $user = Auth::check() ? Auth::user() : null ?>
                                                                        <p class="form-row form-row-first validate-required" id="billing_first_name_field" data-priority="30">
                                                                            <label for="billing_first_name" class="">Họ và tên <abbr class="required" title="bắt buộc">*</abbr></label>
                                                                            <input type="text" class="input-text" required name="customer[name]" placeholder="" value="{{ !empty($user) ? $user->name : '' }}" autocomplete="given-name" autofocus="autofocus">
                                                                        </p>
                                                                        <p class="form-row form-row-last validate-required validate-phone" id="billing_phone_field" data-priority="100">
                                                                            <label for="billing_phone" class="">Số điện thoại <abbr class="required" title="bắt buộc">*</abbr></label>
                                                                            <input type="tel" class="input-text" required name="customer[phone]" placeholder="" value="{{ !empty($user) ? optional($user->profile)->phone : '' }}">
                                                                        </p>
                                                                        <p class="form-row form-row-first validate-email" id="billing_email_field" data-priority="110">
                                                                            <label for="billing_email" class="">Địa chỉ email <abbr class="required" title="bắt buộc">*</abbr></label>
                                                                            <input type="email" class="input-text" name="customer[email]" placeholder="" value="{{ !empty($user) ? $user->email : '' }}">
                                                                        </p>
                                                                        <p class="form-row form-row-last address-field" id="billing_address_1_field" data-priority="50">
                                                                            <label for="billing_address_1" class="">Địa chỉ</label>
                                                                            <input type="text" class="input-text" name="customer[address]" placeholder="Số nhà và tên đường" value="{{ !empty($user) ? optional($user->profile)->address : '' }}">
                                                                        </p>
                                                                        <p class="form-row form-row-wide" id="order_comments_field" data-priority="">
                                                                            <label for="order_comments" class="">Ghi chú đơn hàng</label>
                                                                            <textarea name="note" class="input-text form-control" id="order_comments"
                                                                                      placeholder="Ghi chú về sản phẩm, ví dụ: thời gian hay chỉ dẫn địa điểm giao hàng chi tiết hơn."
                                                                                      rows="2"></textarea>
                                                                        </p>
                                                                        <input type="hidden" name="hidDepartureDate" id="hidDepartureDate" value="">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <h3>Hình thức thanh toán</h3>
                                                        <div id="order_review" class="woocommerce-checkout-review-order">
                                                            <div id="payment" class="woocommerce-checkout-payment">
                                                                <ul class="wc_payment_methods payment_methods methods">
                                                                    @php($firstCheck=1)
                                                                    @foreach(\App\PaymentMethod::where('active', 1)->orderBy('arrange')->get() as $key => $item)
                                                                        <li class="wc_payment_method payment_method_cod">
                                                                            <input id="payment_method_{{ $item->code }}" type="radio" class="chkPayment" {{ $firstCheck === 1 ? 'checked' : '' }}  name="payment_method" value="{{ $item->code }}" data-order_button_text="">
                                                                            <label for="payment_method_{{ $item->code }}">
                                                                                {{ $item->name }}
                                                                            </label>
                                                                            @if($item->code === config('booking.payment_method.credit'))
                                                                                <div class="row cards" id="divCardPaypal" style="display: none; padding-bottom: 20px;">
                                                                                    <div class="text" style="font-weight: bold; margin-left: 43px;">
                                                                                        Quý khách vui lòng chọn loại thẻ:
                                                                                    </div>
                                                                                    <div class="frame-cards" style="padding: 10px; margin-left: 25px;">
                                                                                        <input id="cardType" name="cardType" type="hidden" value="visa">
                                                                                        <div card="visa" onclick="SelectCard('visa')" class="visa card_type f-left card_active img_contain" style="cursor: pointer; float: left; margin-left: 10px; margin-top: 10px; margin-right: 10px;">
                                                                                            <img src={{asset('img/payments/visa_logo.png')}} alt="visa" style="margin: 0;">
                                                                                        </div>
                                                                                        <div card="mastercard" onclick="SelectCard('mastercard')" class="mastercard card_type f-left card_opacity img_contain" style="cursor: pointer; float: left; margin-left: 10px; margin-top: 10px; margin-right: 10px;">
                                                                                            <img src={{asset('img/payments/mastercard_logo.png')}} alt="master" style="margin: 0;">
                                                                                        </div>
                                                                                        <div card="jcb" onclick="SelectCard('jcb')" class="jcb card_type f-left card_opacity img_contain" style="cursor: pointer; float: left; margin-left: 10px; margin-top: 10px; margin-right: 10px;">
                                                                                            <img src={{asset('img/payments/jcb_logo.png')}} alt="jcb" style="margin: 0;">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            @elseif($item->code === config('booking.payment_method.vnpay'))
                                                                                <div class="row cards" id="divCardVNPay" style="display: none; padding-bottom: 20px;">
                                                                                    <div class="text" style="font-weight: bold; margin-left: 43px;">
                                                                                        Quý khách vui lòng chọn ngân hàng:
                                                                                    </div>
                                                                                    <div class="frame-cards" style="padding: 10px; margin-left: 25px;">
                                                                                        <input id="bankType" name="bankType" type="hidden" value="VIETCOMBANK">
                                                                                        <div bank="VIETCOMBANK" onclick="SelectBank('VIETCOMBANK')" class="VIETCOMBANK bank_type f-left card_active img_contain" style="cursor: pointer; float: left; margin-left: 10px; margin-top: 10px; margin-right: 10px;">
                                                                                            <img src={{asset('img/payments/vietcombank_logo.png')}} alt="VIETCOMBANK" style="margin: 0;">
                                                                                        </div>
                                                                                        <div bank="VIETINBANK" onclick="SelectBank('VIETINBANK')" class="VIETINBANK bank_type f-left card_opacity img_contain" style="cursor: pointer; float: left; margin-left: 10px; margin-top: 10px; margin-right: 10px;">
                                                                                            <img src={{asset('img/payments/vietinbank_logo.png')}} alt="VIETINBANK" style="margin: 0;">
                                                                                        </div>
                                                                                        <div bank="BIDV" onclick="SelectBank('BIDV')" class="BIDV bank_type f-left card_opacity img_contain" style="cursor: pointer; float: left; margin-left: 10px; margin-top: 10px; margin-right: 10px;">
                                                                                            <img src={{asset('img/payments/bidv_logo.png')}} alt="BIDV" style="margin: 0;">
                                                                                        </div>
                                                                                        <div bank="AGRIBANK" onclick="SelectBank('AGRIBANK')" class="AGRIBANK bank_type f-left card_opacity img_contain" style="cursor: pointer; float: left; margin-left: 10px; margin-top: 10px; margin-right: 10px;">
                                                                                            <img src={{asset('img/payments/agribank_logo.png')}} alt="AGRIBANK" style="margin: 0;">
                                                                                        </div>
                                                                                        <div bank="SACOMBANK" onclick="SelectBank('SACOMBANK')" class="SACOMBANK bank_type f-left card_opacity img_contain" style="cursor: pointer; float: left; margin-left: 10px; margin-top: 10px; margin-right: 10px;">
                                                                                            <img src={{asset('img/payments/sacombank_logo.png')}} alt="SACOMBANK" style="margin: 0;">
                                                                                        </div>
                                                                                        <div bank="TECHCOMBANK" onclick="SelectBank('TECHCOMBANK')" class="TECHCOMBANK bank_type f-left card_opacity img_contain" style="cursor: pointer; float: left; margin-left: 10px; margin-top: 10px; margin-right: 10px;">
                                                                                            <img src={{asset('img/payments/techcombank_logo.png')}} alt="TECHCOMBANK" style="margin: 0;">
                                                                                        </div>
                                                                                        <div bank="ACB" onclick="SelectBank('ACB')" class="ACB bank_type f-left card_opacity img_contain" style="cursor: pointer; float: left; margin-left: 10px; margin-top: 10px; margin-right: 10px;">
                                                                                            <img src={{asset('img/payments/acb_logo.png')}} alt="ACB" style="margin: 0;">
                                                                                        </div>
                                                                                        <div bank="VPBANK" onclick="SelectBank('VPBANK')" class="VPBANK bank_type f-left card_opacity img_contain" style="cursor: pointer; float: left; margin-left: 10px; margin-top: 10px; margin-right: 10px;">
                                                                                            <img src={{asset('img/payments/vpbank_logo.png')}} alt="VPBANK" style="margin: 0;">
                                                                                        </div>
                                                                                        <div bank="DONGABANK" onclick="SelectBank('DONGABANK')" class="DONGABANK bank_type f-left card_opacity img_contain" style="cursor: pointer; float: left; margin-left: 10px; margin-top: 10px; margin-right: 10px;">
                                                                                            <img src={{asset('img/payments/dongabank_logo.png')}} alt="DONGABANK" style="margin: 0;">
                                                                                        </div>
                                                                                        <div bank="EXIMBANK" onclick="SelectBank('EXIMBANK')" class="EXIMBANK bank_type f-left card_opacity img_contain" style="cursor: pointer; float: left; margin-left: 10px; margin-top: 10px; margin-right: 10px;">
                                                                                            <img src={{asset('img/payments/eximbank_logo.png')}} alt="EXIMBANK" style="margin: 0;">
                                                                                        </div>
                                                                                        <div bank="TPBANK" onclick="SelectBank('TPBANK')" class="TPBANK bank_type f-left card_opacity img_contain" style="cursor: pointer; float: left; margin-left: 10px; margin-top: 10px; margin-right: 10px;">
                                                                                            <img src={{asset('img/payments/tpbank_logo.png')}} alt="TPBANK" style="margin: 0;">
                                                                                        </div>
                                                                                        <div bank="NCB" onclick="SelectBank('NCB')" class="NCB bank_type f-left card_opacity img_contain" style="cursor: pointer; float: left; margin-left: 10px; margin-top: 10px; margin-right: 10px;">
                                                                                            <img src={{asset('img/payments/ncb_logo.png')}} alt="NCB" style="margin: 0;">
                                                                                        </div>
                                                                                        <div bank="OJB" onclick="SelectBank('OJB')" class="OJB bank_type f-left card_opacity img_contain" style="cursor: pointer; float: left; margin-left: 10px; margin-top: 10px; margin-right: 10px;">
                                                                                            <img src={{asset('img/payments/ojb_logo.png')}} alt="OJB" style="margin: 0;">
                                                                                        </div>
                                                                                        <div bank="MSBANK" onclick="SelectBank('MSBANK')" class="MSBANK bank_type f-left card_opacity img_contain" style="cursor: pointer; float: left; margin-left: 10px; margin-top: 10px; margin-right: 10px;">
                                                                                            <img src={{asset('img/payments/msbank_logo.png')}} alt="MSBANK" style="margin: 0;">
                                                                                        </div>
                                                                                        <div bank="HDBANK" onclick="SelectBank('HDBANK')" class="HDBANK bank_type f-left card_opacity img_contain" style="cursor: pointer; float: left; margin-left: 10px; margin-top: 10px; margin-right: 10px;">
                                                                                            <img src={{asset('img/payments/hdbank_logo.png')}} alt="HDBANK" style="margin: 0;">
                                                                                        </div>
                                                                                        <div bank="NAMABANK" onclick="SelectBank('NAMABANK')" class="NAMABANK bank_type f-left card_opacity img_contain" style="cursor: pointer; float: left; margin-left: 10px; margin-top: 10px; margin-right: 10px;">
                                                                                            <img src={{asset('img/payments/namabank_logo.png')}} alt="NAMABANK" style="margin: 0;">
                                                                                        </div>
                                                                                        <div bank="OCB" onclick="SelectBank('OCB')" class="OCB bank_type f-left card_opacity img_contain" style="cursor: pointer; float: left; margin-left: 10px; margin-top: 10px; margin-right: 10px;">
                                                                                            <img src={{asset('img/payments/ocb_logo.png')}} alt="OCB" style="margin: 0;">
                                                                                        </div>
                                                                                        <div bank="SCB" onclick="SelectBank('SCB')" class="SCB bank_type f-left card_opacity img_contain" style="cursor: pointer; float: left; margin-left: 10px; margin-top: 10px; margin-right: 10px;">
                                                                                            <img src={{asset('img/payments/scb_logo.png')}} alt="SCB" style="margin: 0;">
                                                                                        </div>
                                                                                        <div bank="IVB" onclick="SelectBank('IVB')" class="IVB bank_type f-left card_opacity img_contain" style="cursor: pointer; float: left; margin-left: 10px; margin-top: 10px; margin-right: 10px;">
                                                                                            <img src={{asset('img/payments/ivb_logo.png')}} alt="IVB" style="margin: 0;">
                                                                                        </div>
                                                                                        <div bank="ABBANK" onclick="SelectBank('ABBANK')" class="ABBANK bank_type f-left card_opacity img_contain" style="cursor: pointer; float: left; margin-left: 10px; margin-top: 10px; margin-right: 10px;">
                                                                                            <img src={{asset('img/payments/abbank_logo.png')}} alt="ABBANK" style="margin: 0;">
                                                                                        </div>
                                                                                        <div bank="SHB" onclick="SelectBank('SHB')" class="SHB bank_type f-left card_opacity img_contain" style="cursor: pointer; float: left; margin-left: 10px; margin-top: 10px; margin-right: 10px;">
                                                                                            <img src={{asset('img/payments/shb_logo.png')}} alt="SHB" style="margin: 0;">
                                                                                        </div>
                                                                                        <div bank="VIB" onclick="SelectBank('VIB')" class="VIB bank_type f-left card_opacity img_contain" style="cursor: pointer; float: left; margin-left: 10px; margin-top: 10px; margin-right: 10px;">
                                                                                            <img src={{asset('img/payments/vib_logo.png')}} alt="VIB" style="margin: 0;">
                                                                                        </div>
                                                                                        <div bank="VIETCAPITALBANK" onclick="SelectBank('VIETCAPITALBANK')" class="VIETCAPITALBANK bank_type f-left card_opacity img_contain" style="cursor: pointer; float: left; margin-left: 10px; margin-top: 10px; margin-right: 10px;">
                                                                                            <img src={{asset('img/payments/vccb_logo.png')}} alt="VIETCAPITALBANK" style="margin: 0;">
                                                                                        </div>
                                                                                        <div bank="PVCOMBANK" onclick="SelectBank('PVCOMBANK')" class="PVCOMBANK bank_type f-left card_opacity img_contain" style="cursor: pointer; float: left; margin-left: 10px; margin-top: 10px; margin-right: 10px;">
                                                                                            <img src={{asset('img/payments/PVComBank_logo.png')}} alt="PVCOMBANK" style="margin: 0;">
                                                                                        </div>
                                                                                        <div bank="SAIGONBANK" onclick="SelectBank('SAIGONBANK')" class="SAIGONBANK bank_type f-left card_opacity img_contain" style="cursor: pointer; float: left; margin-left: 10px; margin-top: 10px; margin-right: 10px;">
                                                                                            <img src={{asset('img/payments/saigonbank.png')}} alt="SAIGONBANK" style="margin: 0;">
                                                                                        </div>
                                                                                        <div bank="MBBANK" onclick="SelectBank('MBBANK')" class="MBBANK bank_type f-left card_opacity img_contain" style="cursor: pointer; float: left; margin-left: 10px; margin-top: 10px; margin-right: 10px;">
                                                                                            <img src={{asset('img/payments/mbbank_logo.png')}} alt="MBBANK" style="margin: 0;">
                                                                                        </div>
                                                                                        <div bank="BACABANK" onclick="SelectBank('BACABANK')" class="BACABANK bank_type f-left card_opacity img_contain" style="cursor: pointer; float: left; margin-left: 10px; margin-top: 10px; margin-right: 10px;">
                                                                                            <img src={{asset('img/payments/bacabank_logo.png')}} alt="BACABANK" style="margin: 0;">
                                                                                        </div>
                                                                                        <div bank="UPI" onclick="SelectBank('UPI')" class="UPI bank_type f-left card_opacity img_contain" style="cursor: pointer; float: left; margin-left: 10px; margin-top: 10px; margin-right: 10px;">
                                                                                            <img src={{asset('img/payments/upi_logo.png')}} alt="UPI" style="margin: 0;">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            @endif
                                                                        </li>
                                                                        @if($firstCheck==1)
                                                                            @php($firstCheck=0)
                                                                        @endif
                                                                    @endforeach
                                                                </ul>
                                                                <div>
                                                                    <div class="woocommerce-terms-and-conditions" style="max-height: 180px; word-wrap: break-word; overflow: auto;text-align: justify;">
                                                                        <div id="conditionPayment"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="woocommerce-checkout-review-order">
                                                            <div class="woocommerce-checkout-payment">
                                                                <div>
                                                                    <div class="woocommerce-terms-and-conditions" id="woocommerce-terms-and-conditions" style="display: none; max-height: 200px; overflow: auto;">
                                                                        <ol style="text-align: justify;">
                                                                            <li><strong>CHÍNH SÁCH THAY ĐỔI/ HỦY DỊCH VỤ</strong></li>
                                                                        </ol>
                                                                        <p style="text-align: justify;">– Mỗi Dịch vụ cũng như đơn vị tổ chức sẽ có quy định thay đổi/hủy, Khách hàng cần tuân thủ và thực hiện theo đúng chính sách thay đổi/hủy của từng sản phẩm. Chính sách này sẽ được quy định tại phần thông tin chi tiết của mỗi đơn vị tổ chức và trong đơn đặt dịch vụ online của Khách hàng. Khách hàng chỉ được đổi/hủy khi có đủ các điều kiện được quy định tại chính sách thay đổi/hủy của VINAVIVU.COM.</p>
                                                                        <p style="text-align: justify;">– Trong trường hợp Khách hàng không thay đổi/hủy dịch vụ thành công và không sử dụng dịch đã đặt thì Khách hàng vẫn phải thanh toán toàn bộ tiền cho đơn đặt dịch vụ đó.</p>
                                                                        <ol style="text-align: justify;" start="2">
                                                                            <li><strong>CHÍNH SÁCH ĐẢM BẢO HOÀN TIỀN</strong></li>
                                                                        </ol>
                                                                        <p style="text-align: justify;"><strong>2.1 ĐIỀU KIỆN HOÀN TIỀN</strong></p>
                                                                        <p style="text-align: justify;">–&nbsp;Chỉ áp dụng cho các dịch vụ có dòng chữ “Đảm bảo Hoàn tiền” trên website dulichdaibang.com;</p>
                                                                        <p style="text-align: justify;">–&nbsp;Chỉ áp dung cho những đơn đặt dịch vụ thành công, đơn đặt dịch vụ thành công được tính kể từ thời điểm Khách hàng nhận được phiếu nhận phòng từ nhân viên chăm sóc khách hàng của dulichdaibang.com qua email;</p>
                                                                        <p style="text-align: justify;">–&nbsp;Thông tin khiếu nại chỉ được dulichdaibang.com giải quyết và hoàn tiền khi được Khách hàng gọi điện hoặc gửi email thông báo trong vòng 24 giờ kể từ thời điểm bắt đầu dịch vụ;</p>
                                                                        <p style="text-align: justify;">–&nbsp;Khách hàng cung cấp bằng chứng để chứng minh khiếu nại của Khách hàng là đúng, chính xác và có cơ sở;</p>
                                                                        <p style="text-align: justify;"><strong>2.2 CÁC TRƯỜNG HỢP ĐƯỢC DULICHDAIBANG.COM HOÀN TIỀN</strong></p>
                                                                        <p style="text-align: justify;">– Dịch vụ không cung cấp được đầy đủ theo đúng thông tin trong đơn đặt dịch vụ mà Khách hàng đã thanh toán thành công thì dulichdaibang.com cam kết sẽ hoàn lại số tiền tiền dịch vụ chi tiết tương ứng mà dulichdaibang.comkhông cung cấp được hoặc không cung cấp đủ mà Khách hàng đã thanh toán và tặng kèm một phiếu giảm giá có giá trị bằng với số tiền Khách hàng đã thanh toán.</p>
                                                                        <p style="text-align: justify;"><strong>2.3 QUY TRÌNH GIẢI QUYẾT KHIẾU NẠI VÀ HOÀN TIỀN CHO KHÁCH HÀNG</strong></p>
                                                                        <ol style="text-align: justify;">
                                                                            <li><strong>Quy trình giải quyết khiếu nại</strong></li>
                                                                        </ol>
                                                                        <p style="text-align: justify;">–&nbsp;&nbsp;Khi phát sinh các trường hợp nêu tại mục 2.2 nêu trên, Khách hàng vui lòng gọi điện thoại tới số 0234.393.67.87 hoặc email tới địa chỉ booking@dulichdaibang.com&nbsp;và. Nội dung khiếu nại cần phải cung cấp mã đơn hàng. Thông tin khiếu nại chỉ được dulichdaibang.com giải quyết khi được Khách hàng gọi điện hoặc gửi email phản hồi trong vòng 24 giờ kể từ thời điểm Khách hàng nhận phòng. Hotline điện thoại chỉ hỗ trợ trong giờ hành chính.</p>
                                                                        <p style="text-align: justify;">– Khi nhận được khiếu nại từ Khách hàng, dulichdaibang.com có quyền kiểm tra lại tính xác thực của các yêu cầu khiếu nại từ phía Khách hàng. Nếu Khách hàng không cung cấp được bằng chứng xác thực hoặc dulichdaibang.com đã xác nhận lại và thấy thông báo của Khách hàng không chính xác thì dulichdaibang.com có quyền từ chối giải quyết. Trong trường hợp có tranh chấp hoặc xung đột giữa các bên liên quan thì quyết định của dulichdaibang.com là quyết định cuối cùng để giải quyết khiếu nại.</p>
                                                                        <p style="text-align: justify;">– Thời gian giải quyết khiếu nại của Khách hàng tối đa là 3 ngày làm việc kể từ ngày nhận được khiếu nại.</p>
                                                                        <ol style="text-align: justify;">
                                                                            <li><strong>Hoàn tiền và phiếu giảm giá hoặc mã giảm giá</strong></li>
                                                                        </ol>
                                                                        <p style="text-align: justify;">–&nbsp;Thời gian hoàn tiền cho Khách hàng trong vòng 3 ngày làm việc kể từ ngày dulichdaibang.com xác nhận về kết quả giải quyết khiếu nại cho Khách hàng.</p>
                                                                        <p style="text-align: justify;">–&nbsp;Quy định về phiếu giảm giá và/hoặc mã giảm giá:</p>
                                                                        <ul style="text-align: justify;">
                                                                            <li>Mã giảm giá được gửi qua email cho Khách hàng;</li>
                                                                            <li>Mã giảm giá chỉ được sử dụng 1 lần, không được quy đổi thành tiền mặt, không áp dụng trong các ngày lễ tết, không áp dụng đồng thời cùng các chương trình khuyến mại khác;</li>
                                                                            <li>Mã giảm giá&nbsp;có hạn sử dụng được ghi kèm theo trong email dulichdaibang.com gửi;</li>
                                                                            <li>Nếu đơn đặt hàng bị hủy, mã giảm giá sẽ không được hoàn lại.</li>
                                                                        </ul>
                                                                        <p style="text-align: justify;"><strong>2.4&nbsp;CÁC TRƯỜNG HỢP MIỄN TRỪ TRÁCH NHIỆM CỦA DULICHDAIBANG.COM</strong></p>
                                                                        <p style="text-align: justify;">–&nbsp;dulichdaibang.com được miễn trừ trách nhiệm khi xảy ra một trong các trường hợp bất khả kháng theo quy định của pháp luật.</p>
                                                                        <p style="text-align: justify;">–&nbsp;Dulichdaibang.com được miễn trừ trách nhiệm trong việc bồi thường thiệt hại gián tiếp, ngẫu nhiên, hoặc do hậu quả phát sinh.</p>
                                                                        <p style="text-align: justify;">–&nbsp;Trong mọi trường hợp, trách nhiệm bồi thường của dulichdaibang.com không vượt quá tổng số tiền Khách hàng đã thanh toán cho đơn đặt dịch vụ thành công bị vi phạm.</p>
                                                                        <p style="text-align: justify;">
                                                                        </p>
                                                                    </div>
                                                                    <p class="form-row terms wc-terms-and-conditions" style="margin-bottom: 30px;">
                                                                        <label class="checkbox">
                                                                            <span class="woocommerce-form__checkbox" style="padding-top:0px;">
                                                                                <input type="checkbox" required class="terms" name="terms" id="terms" style="margin-left: 0px;">
                                                                            </span>
                                                                            <span style="margin-left: 15px;">Tôi đã đọc và chấp nhận
                                                                                <a href="#" target="_blank" id="terms-link">
                                                                                    các điều khoản &amp; điều kiện
                                                                                </a>
                                                                                <span class="required">*</span>
                                                                            </span>
                                                                        </label>
                                                                        <input type="hidden" id="hidSession" value="{{Session::get('paypal_success')[0]}}">
                                                                        <input type="submit" class="button alt" id="place_order" value="Tiến hành thanh toán" data-value="Đặt hàng">
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <input type="hidden" name="hidadult" id="hidadult" value={{ $tour['adult'] }} >
                                                        <input type="hidden" name="hidchild" id="hidchild" value={{ $tour['child'] }} >
                                                        <input type="hidden" name="hidbaby" id="hidbaby" value={{ $tour['baby'] }} >
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ asset('plugins/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}" ></script>
    <script>
        function formatNumber(num) {
            return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
        }
        function setHtmlCart(response){
            let html_1 = '';
            if (response.totalAdult != '0'){
                html_1 = formatNumber(response.price * response.totalAdult)+' đ';
            }
            $('#adult').html(html_1);

            let html_2 = '';
            if (response.totalChild != '0') {
                html_2 = formatNumber(response.price_children * response.totalChild)+' đ';
            }
            $('#child').html(html_2);

            let html_3 = '';
            if (response.totalBaby != '0') {
                html_3 = formatNumber(response.price_baby * response.totalBaby)+' đ';
            }
            $('#baby').html(html_3);

            let html_4 = '';
            html_4 += formatNumber(response.totalPrice)+' đ';
            $('#book-total').html(html_4);
        }
        $(function () {
            $('.book-qty').bind('keyup change', function(){
                if ($(this).attr('name')=='departure_date')
                {
                    $('#hidDepartureDate').val($(this).val());
                } else {
                    if ($('#nAdult').val()=='' || parseInt($('#nAdult').val()) === 0){
                        alert('Số lượng người lớn nhập phải lớn hơn 0');
                    } else {
                        if ($(this).val()=='')
                        {
                            alert('Số lượng người nhập không được để trống');
                        }
                        else
                        {
                            let qty = parseInt($(this).val());
                            let name = $(this).attr('name');
                            $('#hid'+name).val(qty);
                            let totalQty = parseInt($('#nAdult').val()) + parseInt($('#nChild').val()) + parseInt($('#nBaby').val());
                            document.activeElement.blur();
                            if ($('#hidlimit').val() != null && $('#hidlimit').val()!=='')
                            {
                                $('#overlay').show();
                                $('.fa-loading').show();
                                $.ajax({
                                    url: "{{ url('booking/change-qty') }}",
                                    type: 'GET',
                                    dataType: "JSON",
                                    data: {
                                        id: $(this).attr('data-tour-id'),
                                        qty: qty,
                                        name: name,
                                        totalQty: totalQty
                                    },
                                    success: (response) => {
                                        if (response.error_limit !== ''){
                                            alert(response.error_limit);
                                            $('#overlay').hide();
                                            $('.fa-loading').hide();
                                        } else {
                                            setHtmlCart(response);
                                            $('#overlay').hide();
                                            $('.fa-loading').hide();
                                        }
                                    },
                                    error: function (error) {
                                        alert('Đã có một số sự cố khi hệ thống xử lý, vui lòng thử lại!');
                                        $('#overlay').hide();
                                        $('.fa-loading').hide();
                                    }
                                });
                            } else
                            {
                                let html_1 = formatNumber($('#nAdult').val()* $('#adult_price').val()) +' đ';
                                $('#adult').html(html_1);

                                if ($('#nChild').val() != 0)
                                {
                                    let html_2 = formatNumber($('#nChild').val() * $('#child_price').val())+' đ';
                                    $('#child').html(html_2);
                                }

                                if($('#nBaby').val() != 0)
                                {
                                    let html_3 = formatNumber($('#nBaby').val() * $('#baby_price').val())+' đ';
                                    $('#baby').html(html_3);
                                }

                                let html_4 = formatNumber($('#nAdult').val()* $('#adult_price').val() + $('#nChild').val() * $('#child_price').val() + $('#nBaby').val()* $('#baby_price').val()) +' đ';
                                $('#book-total').html(html_4);

                            }
                        }
                    }
                }
            });
            $('.datepicker').datepicker({
                autoclose: true,
                language: '{{ app()->getLocale() }}',
                format: '{{ config('settings.format.date_js') }}',
                startDate: '{{ date('d/m/Y') }}'
            });
        })
        $(function () {
            $("#terms-link").click( function(e){
                e.preventDefault();
                $('#woocommerce-terms-and-conditions').slideToggle('400');
            });
        })
        $('.chkPayment').click('change', function(){
            payID = $('.chkPayment:checked').val();
            LoadConditionPayment(payID);
            if(payID==='credit')
            {
                $('#divCardPaypal').show();
                $('#divCardVNPay').hide();
            } else if(payID==='vnpay')
            {
                $('#divCardVNPay').show();
                $('#divCardPaypal').hide();
            } else
            {
                $('#divCardVNPay').hide();
                $('#divCardPaypal').hide();
            }
        })
        function SelectCard(type)
        {
            $('.card_type').each(function() {
                if($(this).attr('card')==type)
                {
                    $('#cardType').val(type);
                    $(this).addClass('card_active');
                    $(this).removeClass('card_opacity');
                } else
                {
                    $(this).removeClass('card_active');
                    $(this).addClass('card_opacity');
                }
            })
        }
        function SelectBank(type)
        {
            $('.bank_type').each(function() {
                if($(this).attr('bank')==type)
                {
                    $('#bankType').val(type);
                    $(this).addClass('card_active');
                    $(this).removeClass('card_opacity');
                } else
                {
                    $(this).removeClass('card_active');
                    $(this).addClass('card_opacity');
                }
            })
        }
        function LoadConditionPayment(paymentID)
        {
            $.ajax({
                url: "{{ url('booking/load-condition') }}",
                type: 'GET',
                dataType: "JSON",
                data: {
                    code: paymentID
                },
                success: (response) => {
                    $('#conditionPayment').html(response.description);
                },
                error: function (error) {
                    alert('Đã có một số sự cố khi hệ thống xử lý, vui lòng thử lại!');
                    $('#conditionPayment').html('');
                }
            });
        }
        function LoadPage()
        {
            payID = $('.chkPayment:checked').val();
            LoadConditionPayment(payID);
            if(payID==='credit')
            {
                $('#divCardPaypal').show();
                $('#divCardVNPay').hide();
            } else if(payID==='vnpay')
            {
                $('#divCardVNPay').show();
                $('#divCardPaypal').hide();
            } else
            {
                $('#divCardVNPay').hide();
                $('#divCardPaypal').hide();
            }
        }
        $(document).ready(function() {
            $.ajax({
                url: "{{ url('booking/rollback-book') }}",
                type: 'GET',
                dataType: "JSON",
                data: {
                },
                success: (response) => {
                    LoadPage();
                },
                error: function (error) {
                    LoadPage();
                }
            });
        })
    </script>
@endsection
