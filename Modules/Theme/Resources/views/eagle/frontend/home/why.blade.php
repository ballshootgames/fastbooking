<div class="container">
    <h2 class="why-us--title">{{ trans('theme::eagles.eagle_why_us') }}</h2>
    <div class="row why-us--row">
        @if($why_us->count() > 0)
            @php($i = 0)
            @foreach($why_us as $item)
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="list-choose why-us--choose">
                        <span>{{ ++$i }}</span>
                        <h4 style="padding-left: 45px;">
                            <a href="{{ $item['link'] }}" target="_blank">{{ $item['name'] }}</a>
                        </h4>
                        <p style="padding-left: 45px;">{{ $item['description'] }}</p>
                        <div class="clear"></div>
                    </div>
                </div>
            @endforeach
        @else
            @include('theme::eagle.demo.why')
        @endif
    </div>
</div>