<div class="container">
    <h2 class="title-h2--awesome">
        {!! !empty($settings['awesome_tour']) ? $settings['awesome_tour'] : 'Pack and Go <br/> <span>Awesome Tours</span>' !!}
    </h2>
    <div class="tour-awesome--row awesome-items">
        @php
            $i = 0;
        @endphp
        @foreach($tourNewHot as $tour)
            @php
                $i++;
            @endphp
            <div class="awesome-items-colmd-3 awesome-item {{ $i === 2 ? 'awesome-active' : '' }}">
                <a href="{{ url( $slugHome[3].'/' . $tour->slug ) }}.html" class="img item-thumb awesome-item--thumb">
                    <img data-sizes="auto" class="lazyload item-thumb-image" data-expand="-10"
                         data-src="{{ !empty($tour->image) ? \App\Traits\ImageResize::getThumbnail($tour->image, 380, 285) : asset('img/noimage.gif') }}"
                         alt="{{ $tour->name }}">
                    @empty(!($tour->price_discount))
                        <span class="tour-discount">-{{ (round(((float)$tour->price_discount - (float)$tour->price) / (float)$tour->price_discount, 2) * 100) }}%</span>
                    @endempty
                </a>
                <div class="awesome-item--desc awesome-desc">
                    <p class="awesome-desc-p awesome-time">
                        @if($tour->night_number !== 0)
                            {{ $tour->day_number }} Ngày + {{ $tour->night_number }} Đêm
                        @elseif($tour->day_number === 0.5)
                            1/2 Ngày
                        @elseif($tour->night_number !== 0 && $tour->day_number === 0.5)
                            1/2 Ngày + {{ $tour->night_number }} Đêm
                        @else
                            {{ $tour->day_number }} Ngày
                        @endif
                    </p>
                    <h3 class="awesome-title"><a href="{{ url( $slugHome[3].'/' . $tour->slug ) }}.html">{{ $tour->name }}</a></h3>
                    <p class="awesome-desc-p awesome--city">
                        {{ \Modules\Tour\Entities\TourProperty::where('id', $tour->start_city)->first()->name ?? 'Không xác định' }}
                    </p>
                    <p class="awesome-desc-p awesome-departure">
                        {{ !empty($tour->departure_date) ? Carbon\Carbon::parse($tour->departure_date)->format(config('settings.format.date')) : 'Khởi hành hàng ngày' }}
                    </p>
                    <div class="awesome-price">
                        <p>{{ number_format($tour->price) }} đ</p>
                        <p style="text-decoration: line-through">
                            @empty(!($tour->price_discount))
                                {{ number_format($tour->price_discount) }} đ
                            @endempty
                        </p>
                    </div>
                </div>
                <div class="awesome-item--booking">
                    <a href="{{ url('booking/add-item?id='.$tour->id ) }}" class="awesome-btn">{{ trans('theme::frontend.tours.tour_book_now') }}</a>
                </div>
            </div>
        @endforeach
    </div>
</div>