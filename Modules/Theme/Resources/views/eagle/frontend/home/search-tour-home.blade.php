<div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">
        <form action="{{ url('tour') }}" method="GET" role="form">
            <div class="row">
{{--                <div class="col-xs-12 col-sm-5 col-md-4 col-lg-6 pr15 tour-select mb-m-15 form-group">--}}
{{--                    <select name="tour_category" class="form-control select2" id="tour-category">--}}
{{--                        <option value="">{{ trans('theme::frontend.tours.tour_category') }}</option>--}}
{{--                        @foreach(\Modules\Tour\Entities\TourProperty::where('type', 'tour_category')->pluck('name', 'id') as $id => $name)--}}
{{--                            <option {{ Request::get('tour_category') == $id ? "selected" : "" }} value="{{ $id }}">{{ $name }}</option>--}}
{{--                        @endforeach--}}
{{--                    </select>--}}
{{--                </div>--}}
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-6 pr15 tour-select form-group">
                    <select name="tour_place" class="form-control select2" id="tour-place">
                        <option value="">{{ trans('theme::frontend.tours.tour_place') }}</option>
                        @foreach(\Modules\Tour\Entities\TourProperty::where('type', 'tour_place')->orderBy('name','asc')->pluck('name', 'id') as $id => $name)
                            <option {{ Request::get('tour_place') == $id ? "selected" : "" }} value="{{ $id }}">{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-xs-12 col-sm-5 col-md-4 col-lg-6 tour-select form-group">
                    <select name="tour_cities" class="form-control select2" id="tour_cities">
                        <option value="">{{ trans('tour::tour_schedules.schedule_destinations') }}</option>
                        @if(Request::get('tour_place'))
                            @foreach(\Modules\Tour\Entities\TourProperty::where(['type' => 'tour_place', 'parent_id' => Request::get('tour_place')])->pluck('name', 'id') as $id => $name)
                                <option {{ Request::get('tour_cities') == $id ? "selected" : "" }} value="{{ $id }}"> {{ $name }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-2 col-lg-6 pr15 mb-m-15">
                    <button type="button" class="btn-dropdown form-control btn--fixed">Nâng cao</button>
                </div>
                <div class="col-xs-12 col-md-2 col-lg-6">
                    <button type="submit" class="button-tour">Tìm kiếm</button>
                </div>
            </div>
            <div class="dropdown-menu dropdown-menu--fixed">
                <div class="row">
                    <div class="col-sm-6 pr15 tour-select mb-m-15">
                        <select name="tour_list" class="form-control select2">
                            <option value="">{{ trans('theme::frontend.tours.tour_list') }}</option>
                            @foreach(\Modules\Tour\Entities\TourProperty::where('type', 'tour_list')->pluck('name', 'id') as $id => $name)
                                <option {{ Request::get('tour_list') == $id ? "selected" : "" }} value="{{ $id }}">{{ $name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-6 tour-select">
                        <select name="tour_price" class="form-control select2">
                            <option value="">Khoảng giá</option>
                            @foreach(config('theme.list_price') as $id => $name)
                                <option {{ Request::get('tour_price') == $id ? "selected" : "" }} value="{{ $id }}">{{ $name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>