<div class="container">
    <h2 class="title-h2">{{ trans('theme::eagles.eagle_partner') }}</h2>
    <div class="owl-carousel owl-carousel-partner owl-theme">
        @if($partners->count() > 0)
        @foreach($partners as $itemPartner)
            <div class="item">
                <a href="{{ !empty($itemPartner->link) ? $itemPartner->link : '#' }}" target="_blank" class="item-link item-icon">
                    <img data-sizes="auto" class="lazyload" data-expand="-10" data-src="{{ \Modules\Theme\Entities\Block\Partner::getThumbnail($itemPartner->image, 189, 126) }}" alt="{{ $itemPartner->name }}">
                    <i class="fa fa-search item-icon-search"></i>
                </a>
            </div>
        @endforeach
        @else
            @include('theme::eagle.demo.partner')
        @endif
    </div>
</div>