<div class="container tour-container">
    <div class="hk-block hk-tour">
        <h2 class="title-h2">{!! !empty($settings['top_tour']) ? $settings['top_tour'] : '<span class="title-gray">Top</span>&nbsp;<span class="title-orange">Tours</span>' !!}</h2>
        <div class="lst-items">
            @if($tourPriceShocks->count() > 4)
                <div class="owl-carousel owl-carousel-tour owl-theme">
                    @foreach($tourPriceShocks as $tour)
                        <div class="item">
                            <div class="detail">
                                <a href="{{ url( $slugHome[0].'/' . $tour->slug ) }}.html" class="img">
                                    <img data-sizes="auto" class="lazyload" data-expand="-10"
                                         data-src="{{ !empty($tour->image) ? \App\Traits\ImageResize::getThumbnail($tour->image) : asset('img/noimage.gif') }}"
                                         alt="{{ $tour->name }}">
                                    @empty(!($tour->price_discount))
                                        <span class="tour-discount">-{{ (round(((float)$tour->price_discount - (float)$tour->price) / (float)$tour->price_discount, 2) * 100) }}%</span>
                                    @endempty
                                    @if($tour->hot === 1) <span class="tour-hot">HOT</span> @endif
                                </a>
                                <div class="txt-blk">
                                    <div class="rate-price tour--price">
                                        <span style="text-decoration: line-through">
                                            @empty(!($tour->price_discount))
                                                {{ number_format($tour->price_discount) }} đ
                                            @endempty
                                        </span>
                                        {{--                                    <div class="rate">--}}
                                        {{--                                        <img src="{{ asset('img/rate-5.png') }}" alt="rate"/>--}}
                                        {{--                                    </div>--}}
                                        <span>{{ number_format($tour->price) }} đ</span>
                                    </div>
                                    <h3>
                                        <a href="{{ url( $slugHome[0].'/' . $tour->slug ) }}.html">{{ $tour->name }}</a>
                                    </h3>
                                    <span class="hk-lc">
                                    <i class="hk-ic"></i>{{ trans('theme::frontend.tours.tour_place_start') }}:
                                    <strong>{{ \Modules\Tour\Entities\TourProperty::where('id', $tour->start_city)->first()->name ?? 'Không xác định' }}</strong>
                                </span>
                                    <span class="hk-lc">
                                    <i class="fa fa-calendar"></i>{{ trans('theme::frontend.tours.tour_time') }}:
                                    <strong>
                                        @if($tour->night_number !== 0)
                                            {{ $tour->day_number }} ngày - {{ $tour->night_number }} đêm
                                        @elseif($tour->day_number === 0.5)
                                            1/2 ngày
                                        @elseif($tour->night_number !== 0 && $tour->day_number === 0.5)
                                            1/2 ngày - {{ $tour->night_number }} đêm
                                        @else
                                            {{ $tour->day_number }} ngày
                                        @endif
                                    </strong>
                                </span>
                                    <span class="hk-lc">
                                    <i class="fa fa-clock-o"></i>{{ trans('theme::frontend.tours.tour_day_start') }}:
                                    <strong>{{ !empty($tour->departure_date) ? Carbon\Carbon::parse($tour->departure_date)->format(config('settings.format.date')) : trans('theme::frontend.tours.tour_daily_start')  }}</strong>
                                </span>
                                    <a href="{{ url( $slugHome[0].'/' . $tour->slug ) }}.html" class="view-detail">{{ trans('theme::frontend.tours.tour_detail') }}</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @else
                <div class="row">
                    @foreach($tourPriceShocks as $tour)
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <div class="detail">
                                <a href="{{ url( $slugHome[0].'/' . $tour->slug ) }}.html" class="img">
                                    <img data-sizes="auto" class="lazyload" data-expand="-10"
                                         data-src="{{ !empty($tour->image) ? \App\Traits\ImageResize::getThumbnail($tour->image, 300, 200) : asset('img/noimage.gif') }}"
                                         alt="{{ $tour->name }}">
                                    @empty(!($tour->price_discount))
                                        <span class="tour-discount">-{{ (round(((float)$tour->price_discount - (float)$tour->price) / (float)$tour->price_discount, 2) * 100) }}%</span>
                                    @endempty
                                    @if($tour->hot === 1) <span class="tour-hot">HOT</span> @endif
                                    {{--<span class="tour-tra-gop">TRẢ GÓP</span>--}}
                                    {{--<span class="tour-noi-dung-khung-phai" style="background-color: #006eb9"> Chỉ từ 750.000đ/tháng </span>--}}
                                </a>
                                <div class="txt-blk">
                                    <div class="rate-price tour--price">
                                        <span style="text-decoration: line-through">
                                            @empty(!($tour->price_discount))
                                                {{ number_format($tour->price_discount) }} đ
                                            @endempty
                                        </span>
                                        {{--                                    <div class="rate">--}}
                                        {{--                                        <img src="{{ asset('img/rate-5.png') }}" alt="rate"/>--}}
                                        {{--                                    </div>--}}
                                        <span>{{ number_format($tour->price) }} đ</span>
                                    </div>
                                    <h3>
                                        <a href="{{ url( $slugHome[0].'/' . $tour->slug ) }}.html">{{ $tour->name }}</a></h3>
                                    <span class="hk-lc">
                                        <i class="hk-ic"></i>{{ trans('theme::frontend.tours.tour_place_start') }}:
                                        <strong>{{ \Modules\Tour\Entities\TourProperty::where('id', $tour->start_city)->first()->name ?? 'Không xác định' }}</strong>
                                    </span>
                                    <span class="hk-lc">
                                        <i class="fa fa-calendar"></i>{{ trans('theme::frontend.tours.tour_time') }}:
                                        <strong>
                                            @if($tour->night_number !== 0)
                                                {{ $tour->day_number }} ngày - {{ $tour->night_number }} đêm
                                            @elseif($tour->day_number === 0.5)
                                                1/2 ngày
                                            @elseif($tour->night_number !== 0 && $tour->day_number === 0.5)
                                                1/2 ngày - {{ $tour->night_number }} đêm
                                            @else
                                                {{ $tour->day_number }} ngày
                                            @endif
                                        </strong>
                                    </span>
                                    <span class="hk-lc">
                                        <i class="fa fa-clock-o"></i>{{ trans('theme::frontend.tours.tour_day_start') }}:
                                        <strong>{{ !empty($tour->departure_date) ? Carbon\Carbon::parse($tour->departure_date)->format(config('settings.format.date')) : trans('theme::frontend.tours.tour_daily_start') }}</strong>
                                    </span>
                                    <a href="{{ url( $slugHome[0].'/'. $tour->slug ) }}.html" class="view-detail">{{ trans('theme::frontend.tours.tour_detail') }}</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif
        </div>
    </div>
</div>