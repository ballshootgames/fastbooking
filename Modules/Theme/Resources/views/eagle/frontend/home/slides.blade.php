{{--<ul class="rslides">--}}
{{--    @if($slides->count() > 0)--}}
{{--        @foreach($slides as $itemSlide)--}}
{{--            <li><img src="{{ empty($itemSlide->image) ? '' : asset(Storage::url($itemSlide->image)) }}" alt="{{ $itemSlide->name }}"></li>--}}
{{--        @endforeach--}}
{{--    @else--}}
{{--        @include('theme::eagle.demo.slide')--}}
{{--    @endif--}}
{{--</ul>--}}
<div class="container header-map">
    <div class="col-lg-6">
        <div id="image-map"></div>
    </div>
    <div class="col-lg-6">
        @include('theme::eagle.frontend.home.search-tour')
        <div class="header-description hidden visible-lg-block text-justify">
            <p>Khi đến Việt Nam, du khách sẽ cảm nhận được điều đó khi có cơ hội thưởng thức nhiều món ăn đa dạng, phong phú từ ổ bánh mì kẹp thịt đơn sơ, bình dị nhưng đậm đà khó quên mà du khách có thể gặp ở bất cứ góc phố nào cho đến món bún chả Hà Nội nồng nàn, món nem nướng Nha Trang nức lòng thực khách nơi phố biển…</p>
            <p>“Tuy nhiên, bên cạnh những thành phố đông đúc, Việt Nam còn có những làng quê yên ả với cánh đồng bao la, bát ngát, xanh mát. Trái ngược hoàn toàn với cảnh đông đúc, nhộn nhịp nơi phố thị, đến những nơi này, bạn sẽ được tận mắt ngắm nhìn những thửa ruộng bậc thang nổi bật trên nền xanh hun hút của những cánh rừng bạt ngàn mà bất cứ ai ngắm nhìn đều không khỏi đắm say. Cảnh sắc nơi đây yên bình, nên thơ, không khí trong lành. Nếu bạn đang có kế hoạch đi du lịch đến Việt Nam, đừng quên ghé qua các vùng đất nông thôn yên ả, thanh bình,” tác giả giới thiệu.</p>
            <p>Nói về công trình kiến trúc, danh lam, thắng cảnh, tác giả viết: Một số người nghĩ rằng người Việt Nam chủ yếu theo đạo Phật nên đến đây, đa số là những công trình chùa chiềng. Tuy nhiên, thực tế thì nơi đây cũng có người theo Đạo Thiên Chúa và một số tín ngưỡng khác. Đến Việt Nam, ngoài những ngôi chùa, đền, tháp, thiền viện cổ kính, thanh tịnh mang nhiều ý nghĩa về mặt lịch sử và văn hóa tâm linh còn có nhiều nhà thờ đẹp quanh thành phố.</p>
            <p>Bên cạnh đó, bài viết cũng giúp du khách hiểu thêm về Việt Nam qua trang phục trong cuộc sống đời thường. “Khi chưa đến Việt Nam, chắc chắn du khách sẽ nghĩ rằng trang phục phổ biến của phụ Nữ Việt luôn gắn liền với nón lá vì ai cũng biết rằng chiếc nón lá là biểu tượng của người phụ nữ Việt Nam. Tuy nhiên, trong thời đại ngày ngay, trang phục ở Việt Nam cũng có nhiều thay đổi cho phù hợp với cuộc sống hiện đại. Những bộ trang phục cách tân, đa dạng về kiểu dáng, chất liệu, thuộc các thương hiệu khác nhau trong và ngoài nước ra đời góp phần tôn thêm nét đẹp của người phụ nữ Việt Nam...</p>
            <p>“Hình ảnh đất nước và con người Việt Nam hôm nay đã có nhiều đổi thay, khác xa với hình ảnh của nhiều thập niên trước, khi đất nước vừa mới thoát khỏi chiến tranh. Ngày nay, Việt Nam trở thành một trong những nước nổi bật có nhiều phong cảnh đẹp, năng động, kinh tế ổn định ở khu vực Đông Nam Á. Chính vì thế, Việt Nam là nơi đáng để du khách ghé thăm ít nhất một lần trong đời”- tác giả khẳng định.</p>
        </div>
    </div>
</div>
