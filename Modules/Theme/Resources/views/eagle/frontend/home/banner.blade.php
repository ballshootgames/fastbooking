<div class="banner">
    @if($banners->count() > 0)
        @foreach($banners as $itemBanner)
            <a href="{{ empty($itemBanner->link) ? '#' : $itemBanner->link }}">
                <img data-sizes="auto" class="lazyload" data-expand="-10" data-src="{{ \App\Traits\ImageResize::getThumbnail($itemBanner->image, 400, 164) }}" alt="{{ $itemBanner->name }}">
            </a>
        @endforeach
    @else
        @include('theme::eagle.demo.banner')
    @endif
    <div class="clear"></div>
</div>