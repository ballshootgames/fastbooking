<div class="testimonial hk-feedback">
    <div class="container">
        <div class="col-lg-8 col-lg-offset-2">
        <h2 class="title-h2">Cảm nhận</h2>
        <ul class="lst-fdb">
            @if($comments->count() > 0)
                @foreach($comments as $itemComment)
                    <li>
                        <div class="detail">
                            <p class="comment">{{ $itemComment->comment }}</p>
                            <div class="img-fdb">
                                <img style="width: 130px; height: 130px; object-fit: cover" src="{{ asset('images/avatar.png') }}"
                                     alt="{{ $itemComment->user_name }}"/></div>
                            <h3 class="comment-title">- {{ $itemComment->user_name }} -</h3>
                        </div>
                    </li>
                @endforeach
            @else
                @include('theme::eagle.demo.comment')
            @endif
        </ul>
        </div>
    </div>
</div>
<div class="our-blog container">
    <h2 class="title-h2">{{ !empty($settings['our_blog']) ? $settings['our_blog'] : 'Our Blog' }}</h2>
    <div class="row lst-news">
        @foreach($newsListHome as $item)
            <div class="col-md-4 col-sm-6 col-xs-6 mb-15">
                <div class="detail lf-news">
                    <a href="{{ url(optional($item->type)->slug . '/' . $item->slug) }}.html" class="img item-icon item-thumb">
                        <img data-sizes="auto" class="lazyload item-thumb-image" data-expand="-10" data-src="{{ empty($item->image) ? asset('img/noimage.gif') : asset(Storage::url($item->image)) }}" alt="{{ $item->title }}" />
                        <i class="fa fa-search item-icon-search"></i>
                    </a>
                    <h3 class="item-title">
                        <a class="item-title--link" href="{{ url(optional($item->type)->slug . '/' . $item->slug) }}.html">{{ $item->title }}</a>
                    </h3>
                    <p class="item-desc">
                        {{ Str::limit($item->description, 150) }}
                    </p>
                </div>
            </div>
        @endforeach
    </div>
</div>