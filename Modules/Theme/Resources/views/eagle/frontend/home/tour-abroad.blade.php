<h2 class="title-h2">{{ !empty($settings['favorite_tour']) ? $settings['favorite_tour'] : 'Favorite Tours' }}</h2>
<div class="flex-container tour-items">
    @foreach($tourAbroadTravels as $tour)
        <div class="tour-item item-col-lg-3 item-col-sm-2 item-col-1">
                <a href="{{ url( $slugHome[2].'/' . $tour->slug ) }}.html" class="img item-thumb item--link">
                    <img data-sizes="auto" class="lazyload item-thumb-image" data-expand="-10"
                         data-src="{{ !empty($tour->image) ? \App\Traits\ImageResize::getThumbnail($tour->image, 340, 255) : asset('img/noimage.gif') }}"
                         alt="{{ $tour->name }}">
                    @empty(!($tour->price_discount))
                        <span class="tour-discount">-{{ (round(((float)$tour->price_discount - (float)$tour->price) / (float)$tour->price_discount, 2) * 100) }}%</span>
                    @endempty
                    <div class="item--price">
                            @empty(!($tour->price_discount))
                            <span class="price" style="text-decoration: line-through">
                                {{ number_format($tour->price_discount) }} đ
                            </span>
                                @else
                                <span></span>
                            @endempty
                        <span class="price">{{ number_format($tour->price) }} đ</span>
                    </div>
                </a>
                <div class="item--info hk-tour">
                    <h3 class="item--info-title">
                        <a class="item--link" href="{{ url( $slugHome[2].'/' . $tour->slug ) }}.html" title="{{$tour->name}}">{{ $tour->name }}</a>
                    </h3>
                    <p class="item--info-place hk-lc">
                        <i class="hk-ic"></i>{{ trans('theme::frontend.tours.tour_place_start') }}:
                        <strong>{{ \Modules\Tour\Entities\TourProperty::where('id', $tour->start_city)->first()->name ?? 'Không xác định' }}</strong>
                    </p>
                    <p class="item--info-time hk-lc">
                        <i class="fa fa-calendar"></i>{{ trans('theme::frontend.tours.tour_time') }}:
                        <strong>
                            @if($tour->night_number !== 0)
                                {{ $tour->day_number }} ngày - {{ $tour->night_number }} đêm
                            @elseif($tour->day_number === 0.5)
                                1/2 ngày
                            @elseif($tour->night_number !== 0 && $tour->day_number === 0.5)
                                1/2 ngày - {{ $tour->night_number }} đêm
                            @else
                                {{ $tour->day_number }} ngày
                            @endif
                        </strong>
                    </p>
                </div>
            </div>
    @endforeach
</div>