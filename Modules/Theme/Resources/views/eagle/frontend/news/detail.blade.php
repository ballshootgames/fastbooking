@extends('theme::'.$themeName.'.frontend.master')
@section('title')
    <title>{!! $news->title !!} - {{ $settings['website_name']  }}</title>
    <META NAME="KEYWORDS" content="{{ !empty($news->meta_keyword) ? $news->meta_keyword : $settings['seo_keywords'] }}"/>
    <meta name="description" content="{{ !empty($news->description) ? strip_tags(Str::limit($news->description, 200)) : strip_tags(Str::limit($news->content, 200)) }}"/>
@endsection
@section('facebook')
    <meta property="og:title" content="{!! $news->title !!} - {{ $settings['website_name'] }}"/>
    <meta property="og:description" content="{{ !empty($news->description) ? strip_tags(Str::limit($news->description, 200)) : strip_tags(Str::limit($news->content, 200)) }}"/>
    <meta property="og:image" content="{{ !empty($news->image) ? asset(Storage::url($news->image)) : asset(Storage::url($settings['url_logo'])) }}"/>
    <meta property="og:image:secure_url" content="{{ !empty($news->image) ? asset(Storage::url($news->image)) : asset(Storage::url($settings['url_logo'])) }}"/>
@endsection
@section('twitter')
    <meta name="twitter:title" content="{!! $news->title !!} - {{ $settings['website_name'] }}"/>
    <meta name="twitter:description" content="{{ !empty($news->description) ? strip_tags(Str::limit($news->description, 200)) : strip_tags(Str::limit($news->content, 200)) }}"/>
    <meta name="twitter:image" content="{{ !empty($news->image) ? asset(Storage::url($news->image)) : asset(Storage::url($settings['url_logo'])) }}"/>
@endsection
@section('breadcrumb')
    <div class="bearch">
        <div class="container">
            <p id="breadcrumbs">
                <i class="fa fa-home"></i>
                <span>
                    <span>
                        <a href="{{ url('/') }}">{{ trans('theme::eagles.home') }}</a>
                        <i class="fa fa-angle-double-right"></i>
                        <span>
                            @if(!empty($newsType->parent_id))
                                <a href="{{ url( $newsTypeParent->slug . '/' . optional($news->type)->slug) }}">{{ optional($news->type)->title }}</a>
                                @else
                                <a href="{{ url( $newsTypeParent->slug ) }}">{{ $newsTypeParent->title }}</a>
                            @endif
                            <i class="fa fa-angle-double-right"></i>
                            <span class="breadcrumb_last">{!! $news->title !!}</span>
                        </span>
                    </span>
                </span>
            </p>
        </div>
    </div>
@endsection
@section('content')
    @php($linkSocial = Request::fullUrl())
    <section class="single">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-9 ">
                    <div class="content-single">
                        <h1 class="title">{!! $news->title !!}</h1>
                        <div class="meta">
                            <span><strong>Ngày đăng: </strong>{{ \Carbon\Carbon::parse($news->updated_at)->format(config('settings.format.date_')) }}</span>
                            @if($news->user)
                            <span><strong>Bởi: </strong>{{ optional($news->user)->name }}</span>
                            @endif
                            <span>
                                @if(!empty($newsType->parent_id))
                                    <a href="{{ url( $newsTypeParent->slug . '/' . optional($news->type)->slug) }}" rel="category tag">{{ optional($news->type)->title }}</a>
                                @else
                                    <a href="{{ url( $newsTypeParent->slug ) }}">{{ $newsTypeParent->title }}</a>
                                @endif
                            </span>
                        </div>
                        <span class="like">
                            @include('theme::layouts.facebook-like', ['link' => $linkSocial])
                        </span>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-9 col-md-push-3">
                                <article class="post-content">
{{--                                    <p style="text-align: justify;"><strong>{!!$news->description!!}</strong></p>--}}
                                    {!! $news->content !!}
                                </article>

                                <div class="meta-s">
                                    <span class="like">
                                        <p><img src="{{ asset('img/like_click.gif') }}" alt="Click like"></p>
                                        @include('theme::layouts.facebook-like', ['link' => $linkSocial])
                                    </span>
                                </div>
                                <div class="content-cmt">
                                    @include('theme::layouts.facebook-comment', ['link' => $linkSocial])
                                </div>
                                <div class="related">
                                    <h3 class="title-related">Bài viết liên quan</h3>
                                    <ul class="row">
                                        @foreach($newsRelates as $itemRelate)
                                            <li class="col-xs-12 col-sm-6 col-md-4 mb-10">
                                                <a class="link-img" href="{{ url( optional($itemRelate->type)->slug . '/' . $itemRelate->slug) }}.html">
                                                    <img data-sizes="auto" class="lazyload" data-expand="-10" data-src="{{ empty($itemRelate->image) ? asset('img/noimage.gif') : asset(Storage::url($itemRelate->image)) }}" alt="{{ $itemRelate->title }}">
                                                </a>
                                                <h4>
                                                    <a href="{{ url( optional($itemRelate->type)->slug . '/' . $itemRelate->slug) }}.html">{!! $itemRelate->title !!}</a>
                                                </h4>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-3 col-md-pull-9">
                                <div class="left-single">
                                    <h3 class="ran">Bài viết ngẫu nhiên</h3>
                                    <ul>
                                        @foreach($newsRandoms as $itemRandom)
                                            <li>
                                                <a class='img' href="{{ url( optional($itemRandom->type)->slug . '/' . $itemRandom->slug) }}.html">
                                                    <img data-sizes="auto" class="lazyload" data-expand="-10" data-src="{{ empty($itemRandom->image) ? asset('img/noimage.gif') : asset(Storage::url($itemRandom->image)) }}"
                                                         alt="{{ $itemRandom->title }}"/>
                                                </a>
                                                <h4><a href="{{ url( optional($itemRandom->type)->slug . '/' . $itemRandom->slug) }}.html">{!! $itemRandom->title !!}</a></h4>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-3">
                    @include('theme::'.$themeName.'.frontend.layouts.sidebar-right')
                </div>
            </div>
        </div>
    </section>
@endsection

