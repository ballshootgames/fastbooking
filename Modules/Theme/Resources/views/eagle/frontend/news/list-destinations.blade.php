@extends('theme::eagle.frontend.master')
@section('title')
    <title>{{ $newsType->title }} - {{ $settings['website_name'] }}</title>
    <META NAME="KEYWORDS" content="{{ !empty($newsType->meta_keyword) ? $newsType->meta_keyword : $settings['seo_keywords'] }}"/>
    <meta name="description" content="{{ !empty($newsType->description) ? strip_tags(Str::limit($newsType->description, 200)) : strip_tags(Str::limit($newsType->content, 200))}}"/>
@endsection
@section('breadcrumb')
    <div class="bearch">
        <div class="container">
            <p id="breadcrumbs">
                <i class="fa fa-home"></i>
                <span>
                    <span>
                        <a href="{{ url('/') }}">{{ trans('theme::eagles.home') }}</a>
                        <i class="fa fa-angle-double-right"></i>
                        <span class="breadcrumb_last">{{ $newsType->title }}</span>
                    </span>
                </span>
            </p>
        </div>
    </div>
@endsection
@section('content')
    <section class="single category">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8">
                    <div class="content-single content-category">
                        <h1 class="tit">
                            <span class="tm">{{ $newsType->title }}</span>
                        </h1>
                        @if(!empty($newsLists->count()))
                            @foreach($newsLists as $item)
                                <div class="list-post">
                                    <a href="{{ url( optional($item->type)->slug) . '/' . $item->slug }}.html" class="img link-img">
                                        <img data-sizes="auto" class="lazyload" data-expand="-10" data-src="{{ empty($item->image) ? asset('img/noimage.gif') : asset(Storage::url($item->image)) }}"
                                             alt="{{ $item->title }}"/>
                                    </a>
                                    <h4><a href="{{ url( optional($item->type)->slug) . '/' . $item->slug }}.html">{{ $item->title }}</a></h4>
                                    <div class="meta"><span>{{ \Carbon\Carbon::parse($item->updated_at)->format(config('settings.format.date')) }}</span></div>
                                    <article class="post-content">
                                        {!! Str::limit($item->description, 250) !!}
                                    </article>
                                    <div class="clear"></div>
                                </div>
                            @endforeach
                        @else
                            <p class="text-center" style="font-size: 16px;">{{ trans('theme::eagles.data_update') }}</p>
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4">
                    @include('theme::eagle.frontend.layouts.sidebar-right')
                </div>
            </div>
        </div>
    </section>
@endsection