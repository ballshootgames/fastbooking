@extends('theme::'.$themeName.'.frontend.master')
@section('title')
    <title>{{ $newsTypeParent->title }} - {{ $settings['website_name'] }}</title>
    <META NAME="KEYWORDS" content="{{ !empty($newsTypeParent->meta_keyword) ? $newsTypeParent->meta_keyword : $settings['seo_keywords'] }}"/>
    <meta name="description" content="{{ !empty($newsTypeParent->description) ? strip_tags(Str::limit($newsTypeParent->description, 200)) : strip_tags(Str::limit($newsTypeParent->content, 200)) }}"/>
@endsection
@section('breadcrumb')
    <div class="bearch">
        <div class="container">
            <p id="breadcrumbs">
                <i class="fa fa-home"></i>
                <span>
                    <span>
                        <a href="{{ url('/') }}">{{ trans('theme::eagles.home') }}</a>
                        <i class="fa fa-angle-double-right"></i>
                        <span class="breadcrumb_last">{{ $newsTypeParent->title }}</span>
                    </span>
                </span>
            </p>
        </div>
    </div>
@endsection
@section('content')
    <section class="single category">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-9">
                    <div class="content-single content-category">
                        <h1 class="tit">
                            <span class="tm">{{ $newsTypeParent->title }}</span>
                        </h1>
                        <div class="slider-cat">
                            <div id="carousel-id" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    @for($i = 0; $i < $newsTypes->count(); $i++)
                                        @php
                                            $newsLists = \Modules\News\Entities\News::with('type')->where(['news_type_id' => $newsTypes[$i]->id, 'active'=>config('settings.active')])->inRandomOrder()->first();
                                        @endphp
                                        <div class="item {{ $i == 1 ? 'active' : '' }}">
                                            @if(!empty($newsLists))
                                            <a href="{{ url(optional($newsLists->type)->slug . '/' . $newsLists['slug']) }}.html">
                                                <img src="{{ empty($newsLists['image']) ? asset('img/noimage.gif') : asset(Storage::url($newsLists['image'])) }}"
                                                     alt="{{ $newsLists['title'] }}">
                                            </a>
                                            @endif
                                        </div>
                                    @endfor
                                </div>
                                <a class="left carousel-control" href="#carousel-id" data-slide="prev"><span
                                            class="glyphicon glyphicon-chevron-left"></span></a>
                                <a class="right carousel-control" href="#carousel-id" data-slide="next"><span
                                            class="glyphicon glyphicon-chevron-right"></span></a></div>
                        </div>
                        <br>
                        @foreach($newsTypes as $item)
                            <div class="child-cat">
                                <h2 class="title-cat"><span><a href="{{ url($newsTypeParent->slug . '/' . $item->slug) }}">{{ $item->title }}</a></span></h2>
                                <div class="row">
                                    @php
                                        $newsLists = \Modules\News\Entities\News::with('type')->where(['news_type_id'=>$item->id, 'active'=>config('settings.active')])->take(5)->get();
                                        $strActive = $str = '';
                                    @endphp
                                    @if(!empty($newsLists) && $newsLists->count() > 0)
                                    @for($i = 0; $i < $newsLists->count(); $i++)
                                        @php
                                            $size = $i == 0 ? [517,213] : [100,80]; /*0: width; 1: height*/
                                            $pathImage = !empty($newsLists[$i]['image']) ? \Modules\News\Entities\News::getThumbnail($newsLists[$i]['image'], $size[0], $size[1]) : asset('img/noimage.gif');
                                        @endphp
                                        @if($i == 0)
                                            @php
                                                $strActive  .= '<a href="'.url( optional($newsLists[$i]->type)->slug . '/' . $newsLists[$i]['slug']).'.html">'
                                                            . '<img data-sizes="auto" class="lazyload" data-expand="-10" data-src="'. $pathImage .'" alt="'.$newsLists[$i]['title'].'" />'
                                                            . '</a>'
                                                            . '<h4 class="tit"><a href="'.url( optional($newsLists[$i]->type)->slug . '/' . $newsLists[$i]['slug']).'.html">'
                                                            . $newsLists[$i]['title']
                                                            . '</a></h4>'
                                                            . '<div class="meta"><span>'
                                                            . \Carbon\Carbon::parse($newsLists[$i]['updated_at'])->format(config('settings.format.date_'))
                                                            . '</span></div>'
                                                            . '<p>'
                                                            . Str::limit($newsLists[$i]['description'],300)
                                                            . '</p>';
                                            @endphp
                                        @else
                                            @php
                                                $str    .= '<li>'
                                                        . '<a href="'.url( optional($newsLists[$i]->type)->slug . '/' . $newsLists[$i]['slug']).'.html" class="link-img">'
                                                        . '<img data-sizes="auto" class="lazyload" data-expand="-10" data-src="'. $pathImage .'" alt="'.$newsLists[$i]['title'].'" />'
                                                        . '</a>'
                                                        . '<h4 class="tit-chuil"><a href="'.url( optional($newsLists[$i]->type)->slug . '/' . $newsLists[$i]['slug']).'.html">'
                                                        . $newsLists[$i]['title']
                                                        . '</a></h4>'
                                                        . '<div class="meta"><span>'
                                                        . \Carbon\Carbon::parse($newsLists[$i]['updated_at'])->format(config('settings.format.date_'))
                                                        . '</span></div>'
                                                        . '<div class="clear"></div>'
                                                        .'</li>';
                                            @endphp
                                        @endif
                                    @endfor
                                        <div class="col-xs-12 col-sm-12 col-md-7 mb-10">
                                            {!! $strActive !!}
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-5">
                                            <ul class="list-cat-chil">
                                                {!! $str !!}
                                            </ul>
                                        </div>
                                    @else
                                        <div class="col-xs-12 text-center">
                                        {{ __('Dữ liệu đang cập nhật!') }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-3">
                    @include('theme::'.$themeName.'.frontend.layouts.sidebar-right')
                </div>
            </div>
        </div>
    </section>
@endsection