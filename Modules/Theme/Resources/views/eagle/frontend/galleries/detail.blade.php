@extends('theme::eagle.frontend.master')
@section('title')
    <title>{{ $galleries->title }} - {{ $settings['website_name']  }}</title>
    <meta name="description" content="{{ !empty($galleries->description) ? strip_tags(Str::limit($galleries->description, 200)) : strip_tags(Str::limit($galleries->content, 200)) }}"/>
@endsection
@section('facebook')
    <meta property="og:title" content="{{ $galleries->title }} - {{ $settings['website_name'] }}"/>
    <meta property="og:description" content="{{ !empty($galleries->description) ? strip_tags(Str::limit($galleries->description, 200)) : strip_tags(Str::limit($galleries->content, 200)) }}"/>
    <meta property="og:image" content="{{ !empty($galleries->image) ? asset(Storage::url($galleries->image)) : asset(Storage::url($settings['url_logo'])) }}"/>
    <meta property="og:image:secure_url" content="{{ !empty($galleries->image) ? asset(Storage::url($galleries->image)) : asset(Storage::url($settings['url_logo'])) }}"/>
@endsection
@section('twitter')
    <meta name="twitter:title" content="{{ $galleries->title }} - {{ $settings['website_name'] }}"/>
    <meta name="twitter:description" content="{{ !empty($galleries->description) ? strip_tags(Str::limit($galleries->description, 200)) : strip_tags(Str::limit($galleries->content, 200)) }}"/>
    <meta name="twitter:image" content="{{ !empty($galleries->image) ? asset(Storage::url($galleries->image)) : asset(Storage::url($settings['url_logo'])) }}"/>
@endsection
@section('breadcrumb')
    <div class="bearch">
        <div class="container">
            <p id="breadcrumbs">
                <i class="fa fa-home"></i>
                <span>
                    <span>
                        <a href="{{ url('/') }}">{{ trans('theme::eagles.home') }}</a>
                        <i class="fa fa-angle-double-right"></i>
                        <span><a href="{{ url( '/' .Str::slug(trans('theme::eagles.eagle_customer')) ) }}">{{ trans('theme::eagles.eagle_customer') }}</a>
                            <i class="fa fa-angle-double-right"></i> <span
                                    class="breadcrumb_last">{{ $galleries->title }}</span></span>
                    </span>
                </span>
            </p>
        </div>
    </div>
@endsection
@section('content')
    @php($linkSocial = Request::fullUrl())
    <section class="single">
        <div class="container">
            <div class="content-single">
                <h1 class="title">{{ $galleries->title }}</h1>
                <div class="meta">
                    <span><strong>Ngày đăng: </strong>{{ \Carbon\Carbon::parse($galleries->updated_at)->format(config('settings.format.date_')) }}</span>
                </div>
                @empty(!$galleries->description)
                    <div class="mb-10">
                        {!! $galleries->description !!}
                    </div>
                @endempty
                @if(!empty($galleries->files))
                    <div class="gallery__grid">
                        @foreach(explode('$', $galleries->files) as $value)
                            @empty(!$value)
                                <a class="gallery__grid-fancybox fancybox animate" href="{{ asset(\Storage::url($value)) }}" data-fancybox="gallery">
                                    <img width="100%" src="{{ \App\Traits\ImageResize::getThumbnail($value, 300, 200) }}" alt="anh"/>
                                    <i class="fa fa-picture-o"></i>
                                </a>
                            @endempty
                        @endforeach
                    </div>
                @endif
            </div>
			<div class="related" style="margin-bottom: 30px;">
				@include('theme::eagle.frontend.galleries.other')
			</div>
        </div>
    </section>
@endsection
@section('scripts')
    <link rel="stylesheet" href="{{ asset('plugins/fancybox/jquery.fancybox.min.css') }}" />
    <script src="{{ asset('plugins/fancybox/jquery.fancybox.min.js') }}"></script>
    <script>
        $('.fancybox').fancybox({
            loop: true,
            protect: true,
            animationEffect: "zoom-in-out",
            transitionEffect: "zoom-in-out",
            slideShow: {
                autoStart: true,
                speed: 3000
            },
            arrows: true,
            buttons : [
                "zoom",
                //"share",
                "slideShow",
                "fullScreen",
                "download",
                "thumbs",
                "close"
            ],
        });
    </script>
@endsection