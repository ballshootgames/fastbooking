<ul class="row">
    @if($otherGalleries->count() > 0)
        <h3 class="col-xs-12 title-related">
            {{ __('Album khách hàng khác') }}
        </h3>
        @foreach($otherGalleries as $item)
            @php($itemAlbum = explode('$', $item->files))
            <li class="col-sm-4 col-md-3 mb-10">
                <a class="link-img" href="{{ url('/album-khach-hang/' . $item->slug) }}.html">
                    <img data-sizes="auto" class="lazyload" data-expand="-10"
                         data-src="{{ \App\Traits\ImageResize::getThumbnail($itemAlbum[0], 300, 200) }}" alt="{{ $item->title }}" />
                </a>
				<h4 class="text-center">
					<a href="{{ url('/album-khach-hang/' . $item->slug) }}.html">{{ $item->title }}</a>
				</h4>
            </li>
        @endforeach
    @endif
</ul>