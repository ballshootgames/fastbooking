@extends('theme::eagle.frontend.master')
@section('title')
    <title>{{ trans('theme::eagles.eagle_customer') }} - {{ $settings['website_name'] }}</title>
    <meta name="description" content="{{ trans('theme::eagles.meta_description_customer') }}"/>
@endsection
@section('breadcrumb')
    <div class="bearch">
        <div class="container">
            <p id="breadcrumbs">
                <i class="fa fa-home"></i>
                <span>
                    <span>
                        <a href="{{ url('/') }}">{{ trans('theme::eagles.home') }}</a>
                        <i class="fa fa-angle-double-right"></i>
                        <span class="breadcrumb_last">{{ trans('theme::eagles.eagle_customer') }}</span>
                    </span>
                </span>
            </p>
        </div>
    </div>
@endsection
@section('content')
    <section class="single category">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8">
                    <div class="content-single content-category">
                        <h1 class="tit">
                            <span class="tm">{{ __('Album khách hàng') }}</span>
                        </h1>
                        <div class="related">
                            <ul class="row">
                                @if($galleries->count() > 0)
                                    @foreach($galleries as $item)
                                        @php($itemAlbum = explode('$', $item->files))
                                        <li class="col-sm-4 col-md-4 mb-10">
                                            <a class="link-img" href="{{ url($slugParent . '/' . $item->slug) }}.html">
                                                <img data-sizes="auto" class="lazyload" data-expand="-10"
                                                     data-src="{{ \App\Traits\ImageResize::getThumbnail($itemAlbum[0], 300, 200) }}" alt="{{ $item->title }}" />
                                            </a>
                                            <h4 class="text-center">
                                                <a href="{{ url('/album-khach-hang/' . $item->slug) }}.html">{{ $item->title }}</a>
                                            </h4>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                            <div class="quatrang">
                                {!! $galleries->appends(\Request::except('page'))->render() !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4">
                    @include('theme::eagle.frontend.layouts.sidebar-right')
                </div>
            </div>
        </div>
    </section>
@endsection

