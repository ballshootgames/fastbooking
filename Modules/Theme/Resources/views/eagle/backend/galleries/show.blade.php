@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __('theme::galleries.gallery') }}
@endsection
@section('contentheader_title')
    {{ __('theme::galleries.gallery') }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li><a href="{{ url('/admin/themes/galleries') }}">{{ __('theme::galleries.gallery') }}</a></li>
        <li class="active">{{ __("message.detail") }}</li>
    </ol>
@endsection
@section('main-content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">{{ __("message.detail") }}</h3>
            <div class="box-tools">
                <a href="{{ url('/admin/themes/galleries') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
                {{--@can('PagesController@update')--}}
                    <a href="{{ url('/admin/themes/galleries/' . $gallery->id . '/edit') }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                {{--@endcan
                @can('PagesController@destroy')--}}
                    {!! Form::open([
                        'method'=>'DELETE',
                        'url' => ['galleries', $gallery->id],
                        'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-sm',
                            'title' => __('message.delete'),
                            'onclick'=>'return confirm("'.__('message.confirm_delete').'")'
                    ))!!}
                    {!! Form::close() !!}
                {{--@endcan--}}
            </div>
        </div>
        <div class="box-body table-responsive no-padding">
            <table class="table table-striped">
                <tbody>
                <tr>
                    <th class="col-sm-3"> {{ trans('theme::galleries.title') }} </th>
                    <td class="col-sm-9"> {{ $gallery->title }} </td>
                </tr>
                <tr>
                    <th class="col-sm-3"> {{ trans('theme::galleries.album') }} </th>
                    <td class="col-sm-9">
                        @if(!empty($gallery->files))
                            <div class="gallery__grid">
                                @foreach(explode('$', $gallery->files) as $value)
                                    @empty(!$value)
                                    <a class="gallery__grid-fancybox fancybox" href="{{ asset(\Storage::url($value)) }}" data-fancybox="gallery">
                                        <img width="100%" src="{{ asset(\Storage::url($value)) }}" alt="anh"/>
                                    </a>
                                    @endempty
                                @endforeach
                            </div>
                        @endif
                    </td>
                </tr>
                <tr>
                    <th class="col-sm-3"> {{ trans('theme::galleries.updated_at') }} </th>
                    <td class="col-sm-9"> {{ Carbon\Carbon::parse($gallery->updated_at)->format(config('settings.format.datetime')) }} </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

@endsection
@section('scripts-footer')
    <link rel="stylesheet" href="{{ asset('plugins/fancybox/jquery.fancybox.min.css') }}" />
    <script src="{{ asset('plugins/fancybox/jquery.fancybox.min.js') }}"></script>
    <script>
        $('.fancybox').fancybox({
            loop: true,
            protect: true,
            animationEffect: "zoom-in-out",
            transitionEffect: "zoom-in-out",
            slideShow: {
                autoStart: true,
                speed: 3000
            },
            arrows: true,
            buttons : [
                "zoom",
                //"share",
                "slideShow",
                "fullScreen",
                "download",
                "thumbs",
                "close"
            ],
        });
    </script>
@endsection
