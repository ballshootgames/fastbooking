@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __('theme::galleries.gallery') }}
@endsection
@section('contentheader_title')
    {{ __('theme::galleries.gallery') }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li class="active">{{ __('theme::galleries.gallery') }}</li>
    </ol>
@endsection
@section('main-content')
    <div id="alert"></div>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">{{ __('message.lists') }}</h3>
            <div class="box-tools">
                {!! Form::open(['method' => 'GET', 'url' => '/admin/themes/galleries', 'class' => 'pull-left', 'role' => 'search'])  !!}
                <div class="input-group" style="width: 200px;">
                    <input type="text" value="{{\Request::get('search')}}" class="form-control input-sm" name="search" placeholder="{{ __('message.search_keyword') }}">
                    <span class="input-group-btn">
                        <button class="btn btn-default btn-sm" type="submit">
                            <i class="fa fa-search"></i> {{ __('message.search') }}
                        </button>
                    </span>
                </div>
                <input type="hidden" name="hidRecord" value="{{ Request::has('record') ? Request::get('record') : config('settings.total_rec')[0] }}">
                {!! Form::close() !!}
                {{--@can('GalleryController@store')--}}
                    <a href="{{ url('/admin/themes/galleries/create') }}" class="btn btn-success btn-sm" title="{{ __('message.new_add') }}">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                    </a>
                {{--@endcan--}}
            </div>
        </div>
        @php($index = ($galleries->currentPage()-1)*$galleries->perPage())
        <div class="box-body table-responsive no-padding">
            <table id="table-copy" class="table--layout table table-condensed table-bordered">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 3.5%;">{{ trans('message.index') }}</th>
                        <th class="text-center" style="width: 2.5%;">
                            <input type="checkbox" name="chkAll" id="chkAll"/>
                        </th>
                        <th style="width: 50%">{{ trans('theme::galleries.title') }}</th>
                        <th class="text-center" style="width: 5%">{{ trans('theme::galleries.active') }}</th>
                        <th class="text-center" style="width: 8%">{{ trans('theme::galleries.arrange') }}</th>
                        <th class="text-center" style="width: 10%;">{{ trans('theme::galleries.updated_at') }}</th>
                        <th class="text-center" style="width: 9%"></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($galleries as $item)
                    <tr>
                        <td class="text-center">{{ ++$index }}</td>
                        <td class="text-center">
                            <input type="checkbox" name="chkId" id="chkId" value="{{ $item->id }}" data-id="{{ $item->id }}"/>
                        </td>
                        <td>{{ $item->title }}</td>
                        <td class="text-center">{!! $item->active === 1 ? '<i class="fa fa-check text-primary"></i>' : ''  !!}</td>
                        <td class="text-center">{{ $item->arrange }}</td>
                        <td class="text-center">{{ Carbon\Carbon::parse($item->updated_at)->format(config('settings.format.date')) }}</td>
                        <td class="text-center">
                            {{--@can('GalleryController@show')--}}
                                <a href="{{ url('/admin/themes/galleries/' . $item->id) }}" title="{{ __('message.view') }}"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                            {{--@endcan
                            @can('GalleryController@update')--}}
                                <a href="{{ url('/admin/themes/galleries/' . $item->id . '/edit') }}" title="{{ __('message.edit') }}"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                            {{--@endcan
                            @can('GalleryController@destroy')--}}
{{--                                {!! Form::open([--}}
{{--                                    'method'=>'DELETE',--}}
{{--                                    'url' => ['/admin/themes/galleries', $item->id],--}}
{{--                                    'style' => 'display:inline'--}}
{{--                                ]) !!}--}}
{{--                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> ', array(--}}
{{--                                        'type' => 'submit',--}}
{{--                                        'class' => 'btn btn-danger btn-xs',--}}
{{--                                        'title' => __('message.delete'),--}}
{{--                                        'onclick'=>'return confirm("'.__('message.confirm_delete').'")'--}}
{{--                                )) !!}--}}
{{--                                {!! Form::close() !!}--}}
                            {{--@endcan--}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="7" class="text-right">
                            Hiển thị
                            <select name="record" id="record">
                                @php($total_rec = config('settings.total_rec'))
                                @foreach($total_rec as $item)
                                    <option value="{{$item}}" {{ Request::get('hidRecord') == $item ? 'selected' : '' }}>{{$item}}</option>
                                @endforeach
                            </select>
                            dòng/1 trang
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div class="box-footer">
            <div class="row">
                <div id="btn-act" class="col-sm-6 text-left">
{{--                    @can('GalleryController@destroy')--}}
                        <a href="#" id="delItem" data-action="delItem" class="btn-act btn btn-danger btn-sm" title="{{ __('message.delete') }}">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </a>
{{--                    @endcan--}}
{{--                    @can('GalleryController@active')--}}
                        <a href="#" id="activeItem" data-action="activeItem" class="btn-act btn btn-primary btn-sm" title="{{ __('message.user.active') }}">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </a>
{{--                    @endcan--}}
                </div>
                <div class="col-sm-6 text-right">
                    {!! $galleries->appends(\Request::except('page'))->render() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts-footer')
    <script type="text/javascript">
        $('#chkAll').on('click', function () {
            $("input:checkbox").prop('checked', $(this).prop("checked"));
        });
        $('#record').on('change', function () {
            let form = $('#search');
            let record = $(this).val();
            form.find('input[name="record"]').val(record);
            axios.get(form.attr('action')).then(function (res) {
                form.submit();
            }).catch(function () {
                alert('Có lỗi xảy ra vui lòng thử lại!')
            })
            return false;
        });
        $('#btn-act').on('click', '.btn-act', function(e){
            e.preventDefault();
            let action = $(this).data('action');
            ajaxListTour(action);
        });
        function ajaxListTour(action){
            let chkId = $("input[name='chkId']:checked");
            let actTxt = '', successAlert = '', classAlert = '';
            switch (action) {
                case 'delItem':
                    actTxt = 'xóa';
                    successAlert = '{{trans('theme::galleries.deleted_success')}}';
                    classAlert = 'alert-danger';
                    break;
                case 'activeItem':
                    actTxt = 'duyệt';
                    successAlert = '{{trans('theme::galleries.updated_success')}}';
                    classAlert = 'alert-success';
                    break;
            }
            if (chkId.length != 0){
                let notificationConfirm = 'Bạn có muốn '+actTxt+' hồ sơ này không?';
                let notification = confirm(notificationConfirm);
                if (notification){
                    var arrId = '';
                    $("input[name='chkId']:checked").map((val,key) => {
                        arrId += key.value + ',';
                    });
                    axios.get('{{url('/admin/themes/ajax/')}}/'+action, {
                        params: {
                            ids: arrId
                        }
                    })
                        .then((response) => {
                            // console.log(response);
                            if (response.data.success === 'ok'){
                                $('#alert').html('<div class="alert '+classAlert+'">' +
                                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                                    successAlert +
                                    ' </div>');
                                window.location = '{{ url('/admin/themes/galleries') }}'
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                        })
                }
            }else{
                let notificationAlert = 'Vui lòng chọn hồ sơ để '+actTxt+'!';
                alert(notificationAlert);
            }
        }
    </script>
@endsection
