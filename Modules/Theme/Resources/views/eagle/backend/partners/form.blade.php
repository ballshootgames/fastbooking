<div class="box-body">
    @if ($errors->any())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <p><i class="fa fa-fw fa-check"></i> {{ $error }}</p>
            @endforeach
        </div>
    @endif
    {!! Form::hidden('ptype_id', $typeId) !!}
    <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
        {!! Form::label('name', trans('theme::partners.'.config('theme.text')[$typeId].'.name'), ['class' => 'col-md-3 control-label label-required']) !!}
        <div class="col-md-6">
            {!! Form::text('name', null, ['class' => 'form-control input-sm', 'required'=>true]) !!}
            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    @if($typeId === 1)
        <div class="form-group{{ $errors->has('image') ? ' has-error' : ''}}">
            {!! Form::label('image', trans('theme::partners.'.config('theme.text')[$typeId].'.image'), ['class' => 'col-md-3 control-label']) !!}
            <div class="col-md-6">
                <div class="input-group inputfile-wrap ">
                    <input type="text" class="form-control input-sm" readonly>
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-danger btn-sm">
                            <i class=" fa fa-upload"></i>
                            {{ __('message.upload') }}
                        </button>
                        {!! Form::file('image', array_merge(['class' => 'form-control input-sm', "accept" => "image/*"])) !!}
                    </div>
                </div>
                {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
                <div class="clearfix"></div>
                <div class="imgprev-wrap" style="display:{{ !empty($partner->image)?'block':'none' }}">
                    <img class="img-preview" style="width: 50%" src="{{ !empty($partner) ? asset(\Storage::url($partner->image)) : '' }}" alt="{{ !empty($partner) ? $partner->name : '' }}"/>
                    <i class="fa fa-trash text-danger"></i>
                </div>
            </div>
        </div>
    @endif
    <div class="form-group {{ $errors->has('link') ? 'has-error' : ''}}">
        {!! Form::label('link', trans('theme::partners.'.config('theme.text')[$typeId].'.link'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('link', null, ['class' => 'form-control input-sm']) !!}
            {!! $errors->first('link', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    @if($typeId === 2)
        <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
            {!! Form::label('description', trans('theme::partners.'.config('theme.text')[$typeId].'.description'), ['class' => 'col-md-3 control-label']) !!}
            <div class="col-md-6">
                {!! Form::textarea('description', null, ['class' => 'form-control input-sm','rows' => 5]) !!}
                {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    @endif
    <div class="form-group {{ $errors->has('arrange') ? 'has-error' : ''}}">
        {!! Form::label('arrange', trans('theme::partners.'.config('theme.text')[$typeId].'.arrange'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-6">
            {!! Form::number('arrange', isset($partner) ? $partner->arrange : $arrange+=1, ['class' => 'form-control input-sm','min' => 1]) !!}
            {!! $errors->first('arrange', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('active') ? 'has-error' : ''}}">
        {!! Form::label('active', trans('theme::partners.'.config('theme.text')[$typeId].'.active'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-6">
            {!! Form::checkbox('active', config('settings.active'),isset($partner) && $partner->active === config('settings.active') ? true : false, ['class' => 'flat-blue', 'id' => 'active']) !!}
            {!! $errors->first('active', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>
<div class="box-footer">
    {!! Form::submit(isset($submitButtonText) ? $submitButtonText : __('message.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ url('admin/themes/partners?type=' . $typeId) }}" class="btn btn-default">{{ __('message.cancel') }}</a>
</div>
@section('scripts-footer')
    <script type="text/javascript">
        $(function(){
            $('#image').change(function () {
                let preview = document.querySelector('img.img-preview');
                let file    = document.querySelector('#image').files[0];
                let reader  = new FileReader();

                if ( /\.(jpe?g|png|gif)$/i.test(file.name) ) {
                    reader.addEventListener("load", function () {
                        preview.src = reader.result;
                        $('.imgprev-wrap').css('display','block');
                        $('.inputfile-wrap').find('input[type=text]').val(file.name);
                    }, false);

                    if (file) {
                        reader.readAsDataURL(file);
                    }
                }else{
                    document.querySelector('#image').value = '';
                }
            });

            $('.imgprev-wrap .fa-trash').click(function () {
                let preview = document.querySelector('img.img-preview');

                if(confirm('{{ __('message.confirm_delete') }}')){
                    preview.src = '';
                    $('.imgprev-wrap').css('display','none');
                    $('.inputfile-wrap').find('input[type=file]').attr('required','required');
                    $('.inputfile-wrap').find('input[type=text]').val('');
                }
            })
        });
    </script>
@endsection
