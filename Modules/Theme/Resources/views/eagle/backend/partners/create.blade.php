@extends('adminlte::layouts.app')
@php($typeId = (int)\Request::get('type'))
@section('htmlheader_title')
    {{ trans('theme::partners.'.config('theme.text')[$typeId].'.title') }}
@endsection
@section('contentheader_title')
    {{ trans('theme::partners.'.config('theme.text')[$typeId].'.title') }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li><a href="{{ url('/admin/themes/partners?type=' . $typeId ) }}">{{ trans('theme::partners.'.config('theme.text')[$typeId].'.title') }}</a></li>
        <li class="active">{{ __('message.new_add') }}</li>
    </ol>
@endsection

@section('main-content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __('message.new_add') }}</h3>
            <div class="box-tools">
                <a href="{{ url('/admin/themes/partners?type=' . $typeId) }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.lists') }}</span></a>
            </div>
        </div>

        {!! Form::open(['url' => '/admin/themes/partners', 'class' => 'form-horizontal', 'files' => true]) !!}

        @include('theme::eagle.backend.partners.form')

        {!! Form::close() !!}
    </div>
@endsection
