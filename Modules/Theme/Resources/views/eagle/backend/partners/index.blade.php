@extends('adminlte::layouts.app')
@php($typeId = (int)\Request::get('type'))
@section('htmlheader_title')
    {{ trans('theme::partners.'.config('theme.text')[$typeId].'.title') }}
@endsection
@section('contentheader_title')
    {{ trans('theme::partners.'.config('theme.text')[$typeId].'.title') }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li class="active">{{ trans('theme::partners.'.config('theme.text')[$typeId].'.title') }}</li>
    </ol>
@endsection
@section('main-content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">{{ __('message.lists') }}</h3>
            <div class="box-tools">
                {!! Form::open(['method' => 'GET', 'url' => '/admin/themes/partners?type=' . $typeId, 'class' => 'pull-left', 'role' => 'search'])  !!}
                <div class="input-group" style="width: 200px;">
                    <input type="text" value="{{\Request::get('search')}}" class="form-control input-sm" name="search" placeholder="{{ __('message.search_keyword') }}">
                    <span class="input-group-btn">
                        <button class="btn btn-default btn-sm" type="submit">
                            <i class="fa fa-search"></i> {{ __('message.search') }}
                        </button>
                    </span>
                </div>
                {!! Form::close() !!}
                {{--@can('PartnerController@store')--}}
                    <a href="{{ url('/admin/themes/partners/create?type=' . $typeId) }}" class="btn btn-success btn-sm" title="{{ __('message.new_add') }}">
                        <i class="fa fa-plus" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.new_add') }}</span>
                    </a>
                {{--@endcan--}}
            </div>
        </div>
        @php($index = ($partners->currentPage()-1)*$partners->perPage())
        <div class="box-body table-responsive no-padding">
            <table class="table table-striped">
                <tbody>
                <tr>
                    <th class="text-center">{{ trans('message.index') }}</th>
                    <th>{{ trans('theme::partners.'.config('theme.text')[$typeId].'.name') }}</th>
                    <th class="col-md-3">{{ $typeId === 1 ? trans('theme::partners.partner.image') : trans('theme::partners.why_us.description') }}</th>
                    <th class="text-center">{{ trans('theme::partners.'.config('theme.text')[$typeId].'.active') }}</th>
                    <th class="text-center">{{ trans('theme::partners.'.config('theme.text')[$typeId].'.arrange') }}</th>
                    <th class="text-center">{{ trans('theme::partners.'.config('theme.text')[$typeId].'.updated_at') }}</th>
                    <th class="text-center"></th>
                </tr>
                @foreach($partners as $item)
                    <tr>
                        <td class="text-center">{{ ++$index }}</td>
                        <td>
                            <a href="{{ empty($item->link) ? '#' : $item->link }}">{{ $item->name }}</a>
                        </td>
                        <td>
                            {!! $item->ptype_id === 1 ? $item->image ? '<img style="object-fit: cover" width="80" src="'.asset(Storage::url($item->image)).'">' : '' : $item->description !!}
                        </td>
                        <td class="text-center">{!! $item->active == config('settings.active') ? '<i class="fa fa-check text-primary"></i>' : '' !!}</td>
                        <td class="text-center">{{ $item->arrange }}</td>
                        <td class="text-center">{{ Carbon\Carbon::parse($item->updated_at)->format(config('settings.format.date')) }}</td>
                        <td class="text-center">
                            {{--@can('PartnerController@show')--}}
                                <a href="{{ url('/admin/themes/partners/' . $item->id . '?type=' . $typeId) }}" title="{{ __('message.view') }}"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> {{ __('message.view') }}</button></a>
                            {{--@endcan
                            @can('PartnerController@update')--}}
                                <a href="{{ url('/admin/themes/partners/' . $item->id . '/edit?type=' . $typeId) }}" title="{{ __('message.edit') }}"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ __('message.edit') }}</button></a>
                            {{--@endcan
                            @can('PartnerController@destroy')--}}
                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['/admin/themes/partners', $item->id],
                                    'style' => 'display:inline'
                                ]) !!}
                                {!! Form::hidden('type', $typeId) !!}
                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> '.__('message.delete'), array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => __('message.delete'),
                                        'onclick'=>'return confirm("'.__('message.confirm_delete').'")'
                                )) !!}
                                {!! Form::close() !!}
                            {{--@endcan--}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="box-footer clearfix">
                {!! $partners->appends(\Request::except('page'))->render() !!}
            </div>
        </div>
    </div>
@endsection
