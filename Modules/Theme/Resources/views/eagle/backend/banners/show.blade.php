@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __('theme::banners.banner') }}
@endsection
@section('contentheader_title')
    {{ __('theme::banners.banner') }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li><a href="{{ url('/admin/themes/banners') }}">{{ __('theme::banners.banner') }}</a></li>
        <li class="active">{{ __("message.detail") }}</li>
    </ol>
@endsection
@section('main-content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">{{ __("message.detail") }}</h3>
            <div class="box-tools">
                <a href="{{ url('/admin/themes/banners') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ trans('message.lists') }}</span></a>
                {{--@can('PagesController@update')--}}
                    <a href="{{ url('/admin/themes/banners/' . $banner->id . '/edit') }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.edit') }}</span></a>
                {{--@endcan
                @can('PagesController@destroy')--}}
                    {!! Form::open([
                        'method'=>'DELETE',
                        'url' => ['admin/themes/banners', $banner->id],
                        'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> <span class="hidden-xs">'.__('message.delete').'</span>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-sm',
                            'title' => __('message.delete'),
                            'onclick'=>'return confirm("'.__('message.confirm_delete').'")'
                    ))!!}
                    {!! Form::close() !!}
                {{--@endcan--}}
            </div>
        </div>
        <div class="box-body table-responsive no-padding">
            <table class="table table-striped">
                <tbody>
                <tr>
                    <th> {{ trans('theme::banners.image') }} </th>
                    <td>
                        @if(!empty($banner->image))
                            <img width="400" src="{{ asset(\Storage::url($banner->image)) }}" alt="anh"/>
                        @endif
                    </td>
                </tr>
                <tr>
                    <th> {{ trans('theme::banners.name') }} </th>
                    <td> {{ $banner->name }} </td>
                </tr>
                <tr>
                    <th> {{ trans('theme::banners.link') }} </th>
                    <td> {{ $banner->link }} </td>
                </tr>
                <tr>
                    <th> {{ trans('pages.updated_at') }} </th>
                    <td> {{ Carbon\Carbon::parse($banner->updated_at)->format(config('settings.format.datetime')) }} </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

@endsection
