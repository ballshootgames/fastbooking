@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __('theme::slides.slide') }}
@endsection
@section('contentheader_title')
    {{ __('theme::slides.slide') }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li><a href="{{ url('/admin/themes/slides') }}">{{ __('theme::slides.slide') }}</a></li>
        <li class="active">{{ __("message.detail") }}</li>
    </ol>
@endsection
@section('main-content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">{{ __("message.detail") }}</h3>
            <div class="box-tools">
                <a href="{{ url('/admin/themes/slides') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ trans('message.lists') }}</span></a>
                {{--@can('PagesController@update')--}}
                    <a href="{{ url('/admin/themes/slides/' . $slide->id . '/edit') }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.edit') }}</span></a>
                {{--@endcan
                @can('PagesController@destroy')--}}
                    {!! Form::open([
                        'method'=>'DELETE',
                        'url' => ['slides', $slide->id],
                        'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> <span class="hidden-xs">'.__('message.delete').'</span>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-sm',
                            'title' => __('message.delete'),
                            'onclick'=>'return confirm("'.__('message.confirm_delete').'")'
                    ))!!}
                    {!! Form::close() !!}
                {{--@endcan--}}
            </div>
        </div>
        <div class="box-body table-responsive no-padding">
            <table class="table table-striped">
                <tbody>
                <tr>
                    <th> {{ trans('theme::slides.image') }} </th>
                    <td>
                        @if(!empty($slide->image))
                            <img width="400" src="{{ asset(\Storage::url($slide->image)) }}" alt="anh"/>
                        @endif
                    </td>
                </tr>
                <tr>
                    <th> {{ trans('theme::slides.name') }} </th>
                    <td> {{ $slide->name }} </td>
                </tr>
                <tr>
                    <th> {{ trans('theme::slides.updated_at') }} </th>
                    <td> {{ Carbon\Carbon::parse($slide->updated_at)->format(config('settings.format.datetime')) }} </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

@endsection
