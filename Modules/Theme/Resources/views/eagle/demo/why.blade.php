<div class="col-xs-12 col-sm-6 col-md-4">
    <div class="list-choose why-us--choose">
        <span>1</span>
        <h4><a href="#" target="_blank">Mạng bán tour số 1 việt nam</a>
        </h4>
        <p>Hơn 10.000+ chương trình đặc sắc khởi hành 24/7</p>
        <div class="clear"></div>
    </div>
</div>
<div class="col-xs-12 col-sm-6 col-md-4">
    <div class="list-choose why-us--choose">
        <span>2</span>
        <h4>
            <a href="#" target="_blank">Đặt tour dễ dàng và có thể trả góp</a>
        </h4>
        <p>Đặt tour trong 2 phút và "Đi tour trước, trả tiền sau"</p>
        <div class="clear"></div>
    </div>
</div>
<div class="col-xs-12 col-sm-6 col-md-4">
    <div class="list-choose why-us--choose">
        <span>3</span>
        <h4><a href="#" target="_blank">Công ty du lịch quốc tế được cấp phép</a></h4>
        <p>Giấy phép lữ hành quốc tế được bộ VH- TT&DL cấp !</p>
        <div class="clear"></div>
    </div>
</div>
<div class="col-xs-12 col-sm-6 col-md-4">
    <div class="list-choose why-us--choose">
        <span>4</span>
        <h4><a href="#" target="_blank">25.000 khách hàng tin tưởng mỗi
                năm</a></h4>
        <p>Hơn 15.000 ý kiến đánh giá của quý khách hàng đã sử dụng dịch vụ</p>
        <div class="clear"></div>
    </div>
</div>
<div class="col-xs-12 col-sm-6 col-md-4">
    <div class="list-choose why-us--choose">
        <span>5</span>
        <h4><a href="#" target="_blank">Hơn 20+ báo lớn đã nói về chúng tôi</a></h4>
        <p>Khám Phá Di Sản, Dân Trí, Lao Động đã nói về những dịch vụ tuyệt vời của Eagle Tourist</p>
        <div class="clear"></div>
    </div>
</div>
<div class="col-xs-12 col-sm-6 col-md-4">
    <div class="list-choose why-us--choose">
        <span>6</span>
        <h4><a href="#" target="_blank">Nhận được nhiều giải thường danh giá</a></h4>
        <p>Với sự phát triển không ngừng, Du Lịch Đại Bàng nhận được nhiều giải thường danh giá</p>
        <div class="clear"></div>
    </div>
</div>