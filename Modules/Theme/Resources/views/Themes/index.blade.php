@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __('theme::themes.theme') }}
@endsection
@section('contentheader_title')
    {{ __('theme::themes.theme') }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li class="active">{{ __('theme::themes.theme') }}</li>
    </ol>
@endsection
@section('main-content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">{{ __('message.lists') }}</h3>
            <div class="box-tools">
                <a href="{{ url('admin/themes/theme/create') }}" class="btn btn-success btn-sm" title="{{ __('message.new_add') }}">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                </a>
            </div>
        </div>
        <div class="box-body table-responsive no-padding">
            <table class="table table-striped">
                <tbody>
                <tr>
                    <th>{{ trans('theme::themes.name') }}</th>
                    <th class="text-center"></th>
                </tr>
                @foreach($themes as $item)
                    <tr>
                        <td>{{ $item->name }}</td>
                        <td class="text-center">
                            <a href="{{ url('/admin/themes/theme/' . $item->id) }}" target="_blank" title="{{ __('message.view') }}"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
