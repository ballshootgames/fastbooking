<div class="box-body" xmlns="">
    @if ($errors->any())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <p><i class="fa fa-fw fa-check"></i> {{ $error }}</p>
            @endforeach
        </div>
    @endif
    <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
        {!! Form::label('title', trans('theme::menus.title'), ['class' => 'col-md-3 control-label label-required']) !!}
        <div class="col-md-6">
            {!! Form::text('title', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    @if(isset($menu))
        <div class="form-group {{ $errors->has('slug') ? 'has-error' : ''}}" style="display: none;">
            {!! Form::label('slug', trans('theme::menus.slug'), ['class' => 'col-md-3 control-label']) !!}
            <div class="col-md-6">
                {!! Form::text('slug', null, ['class' => 'form-control']) !!}
                {!! $errors->first('slug', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    @endif
    <div class="form-group {{ $errors->has('parent_id') ? 'has-error' : ''}}">
        {!! Form::label('parent_id', trans('theme::menus.parent_id'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-6">
            {!! Form::select('parent_id',$listMenu, null, ['class' => 'form-control select2']) !!}
            {!! $errors->first('parent_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('position') ? ' has-error' : ''}}">
        {!! Form::label('role', __('theme::menus.position'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-6">
            {!! Form::select('position[]', $position, isset($position_edit) ? $position_edit : null, ['class' => 'form-control select2', 'multiple' => true]) !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('arrange') ? 'has-error' : ''}}">
        {!! Form::label('arrange', trans('theme::menus.arrange'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-6">
            {!! Form::number('arrange', isset($menu->arrange) ? $menu->arrange : $menu_arrange+1, ['class' => 'form-control', 'min' => 1]) !!}
            {!! $errors->first('arrange', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('type_id') ? 'has-error' : ''}}">
        {!! Form::label('type_id', trans('theme::menus.type_id'), ['class' => 'col-md-3 control-label label-required']) !!}
        <div class="col-md-6">
            {!! Form::select('type_id', $typeMenu, null, ['class' => 'form-control select2','required' => 'required']) !!}
            {!! $errors->first('type_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('keywords') ? 'has-error' : ''}}">
        {!! Form::label('keywords', trans('theme::menus.keywords'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-6">
            {!! Form::select('keywords', $keywords, null, ['class' => 'form-control input-sm select2']) !!}
            {!! $errors->first('keywords', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>

<div class="box-footer">
    {!! Form::submit(isset($submitButtonText) ? $submitButtonText : __('message.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ url('admin/themes/menus') }}" class="btn btn-default">{{ __('message.close') }}</a>
</div>

@section('scripts-footer')

@endsection