<div class="box-body">
    @if ($errors->any())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <p><i class="fa fa-fw fa-check"></i> {{ $error }}</p>
            @endforeach
        </div>
    @endif
    <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
        {!! Form::label('title', trans('theme::pages.title'), ['class' => 'col-md-3 control-label label-required']) !!}
        <div class="col-md-6">
            {!! Form::text('title', null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
            {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    @if(isset($page))
        <div class="form-group {{ $errors->has('slug') ? 'has-error' : ''}}">
            {!! Form::label('slug', trans('theme::pages.slug'), ['class' => 'col-md-3 control-label']) !!}
            <div class="col-md-6">
                {!! Form::text('slug', null, ['class' => 'form-control input-sm']) !!}
                {!! $errors->first('slug', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    @endif
    <div class="form-group {{ $errors->has('parent_id') ? 'has-error' : ''}}">
        {!! Form::label('parent_id', trans('news::news_type.parent_id'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-6">
            {!! Form::select('parent_id', $parents, null, ['class' => 'form-control select2']) !!}
            {!! $errors->first('parent_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('position') ? ' has-error' : ''}}">
        {!! Form::label('role', __('theme::menus.position'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-6">
            {!! Form::select('position[]', $position, isset($position_edit) ? $position_edit : null, ['class' => 'form-control input-sm select2', 'multiple' => true]) !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
        {!! Form::label('description', trans('theme::pages.description'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-6">
            {!! Form::textarea('description', null, ['class' => 'form-control input-sm ','rows' => 5]) !!}
            {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('content') ? 'has-error' : ''}}">
        {!! Form::label('content', trans('theme::pages.content'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-7">
            {!! Form::textarea('content', null, ['class' => 'form-control input-sm ckeditor']) !!}
            {!! $errors->first('content', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('meta_keyword') ? 'has-error' : ''}}">
        {!! Form::label('meta_keyword', trans('message.meta_keyword'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-7">
            {!! Form::textarea('meta_keyword', null, ['class' => 'form-control', 'rows' => 3]) !!}
            {!! $errors->first('meta_keyword', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('active') ? 'has-error' : ''}}">
        {!! Form::label('active', trans('news::news.active'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-7">
            {!! Form::checkbox('active', 1, (isset($page) && $page->active===1)?true:false, ['class' => 'flat-blue', 'id' => 'active']) !!}
        </div>
    </div>
</div>
<div class="box-footer">
    {!! Form::submit(isset($submitButtonText) ? $submitButtonText : __('message.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ url('admin/themes/pages') }}" class="btn btn-default">{{ __('message.close') }}</a>
</div>
@section('scripts-footer')
    <script type="text/javascript" src="{{ asset('plugins/ckeditor/ckeditor.js') }}" ></script>
    <script>
        CKEDITOR.replace( 'content', ckeditor_options);
    </script>
@endsection