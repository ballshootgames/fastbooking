@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __('theme::pages.page') }}
@endsection
@section('contentheader_title')
    {{ __('theme::pages.page') }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li><a href="{{ url('admin/themes/pages') }}">{{ __('theme::pages.page') }}</a></li>
        <li class="active">{{ __("message.detail") }}</li>
    </ol>
@endsection
@section('main-content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">{{ __("message.detail") }}</h3>
            <div class="box-tools">
                <a href="{{ url('admin/themes/pages') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ trans('message.lists') }}</span></a>
{{--                @can('PagesController@update')--}}
                    <a href="{{ url('admin/themes/pages/' . $page->id . '/edit') }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.edit') }}</span></a>
                {{--@endcan--}}
                {{--@can('PagesController@destroy')--}}
                    {!! Form::open([
                        'method'=>'DELETE',
                        'url' => ['admin/themes/pages', $page->id],
                        'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> <span class="hidden-xs">'.__('message.delete').'</span>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-sm',
                            'title' => __('message.delete'),
                            'onclick'=>'return confirm("'.__('message.confirm_delete').'")'
                    ))!!}
                    {!! Form::close() !!}
                {{--@endcan--}}
            </div>
        </div>
        <div class="box-body table-responsive no-padding">
            <table class="table table-striped">
                <tbody>
                <tr>
                    <th> {{ trans('theme::pages.title') }} </th>
                    <td> {{ $page->title }} </td>
                </tr>
                <tr>
                    <th> {{ trans('theme::pages.slug') }} </th>
                    <td> {{ $page->slug }} </td>
                </tr>
                <tr>
                    <th> {{ trans('theme::pages.description') }} </th>
                    <td>{{ $page->description }}</td>
                </tr>
                <tr>
                    <th> {{ trans('theme::pages.content') }} </th>
                    <td>{!! $page->content !!}</td>
                </tr>
                <tr>
                    <th> {{ trans('theme::pages.updated_at') }} </th>
                    <td> {{ Carbon\Carbon::parse($page->updated_at)->format(config('settings.format.datetime')) }} </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

@endsection