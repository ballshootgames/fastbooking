@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __('theme::pages.page') }}
@endsection
@section('contentheader_title')
    {{ __('theme::pages.page') }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li class="active">{{ __('theme::pages.page') }}</li>
    </ol>
@endsection
@section('main-content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">{{ __('message.lists') }}</h3>
            <div class="box-tools">
                {!! Form::open(['method' => 'GET', 'url' => '/pages', 'class' => 'pull-left', 'role' => 'search'])  !!}
                <div class="input-group" style="width: 200px;">
                    <input type="text" value="{{\Request::get('search')}}" class="form-control input-sm" name="search" placeholder="{{ __('message.search_keyword') }}">
                    <span class="input-group-btn">
                        <button class="btn btn-default btn-sm" type="submit">
                            <i class="fa fa-search"></i> {{ __('message.search') }}
                        </button>
                    </span>
                </div>
                {!! Form::close() !!}
                {{--@can('PagesController@store')--}}
                    <a href="{{ url('admin/themes/pages/create') }}" class="btn btn-success btn-sm" title="{{ __('message.new_add') }}">
                        <i class="fa fa-plus" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.new_add') }}</span>
                    </a>
                {{--@endcan--}}
            </div>
        </div>
        <div class="box-body table-responsive no-padding">
            <table class="table table-striped">
                <tbody>
                <tr>
                    <th>@sortablelink('title',trans('theme::pages.title'))</th>
                    <th>{{ trans('theme::pages.slug') }}</th>
                    <th>{{ trans('theme::pages.description') }}</th>
                    <th>{{ trans('theme::pages.updated_at') }}</th>
                    <th></th>
                </tr>
                @php
                    $listPage = new \Modules\Theme\Entities\Page();
                    $listPage->showListPage($pages);
                @endphp
                </tbody>
            </table>
        </div>
    </div>
@endsection
