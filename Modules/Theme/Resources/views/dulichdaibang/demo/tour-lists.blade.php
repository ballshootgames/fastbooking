<div class="item">
    <img src="{{ asset('img/vay-tra-gop-theo-ngay.png') }}" alt="Chùm tour 30/04 - 01/05">
    <h4><a href="#loai-tour/chum-tour-le/chum-tour-30-04-chum-tour-le">Chùm
            tour 30/04 - 01/05</a></h4>
</div>
<div class="item">
    <img src="{{ asset('img/vay-tra-gop-theo-ngay.png') }}"
         alt="Chùm tour HOT tháng 12">
    <h4><a href="#loai-tour/chum-tour-hot-thang-12">Chùm tour HOT tháng
            12</a></h4>
</div>
<div class="item">
    <img src="{{ asset('img/vay-tra-gop-theo-ngay.png') }}"
         alt="Chùm tour trăng mật">
    <h4><a href="#loai-tour/chum-tour-trang-mat">Chùm tour trăng mật</a>
    </h4>
</div>
<div class="item">
    <img src="{{ asset('img/vay-tra-gop-theo-ngay.png') }}"
         alt="Du lịch hành hương">
    <h4><a href="#loai-tour/du-lich-hanh-huong">Du lịch hành hương</a></h4>
</div>
<div class="item">
    <img src="{{ asset('img/vay-tra-gop-theo-ngay.png') }}" alt="Du lịch Mỹ">
    <h4><a href="#loai-tour/du-lich-my">Du lịch Mỹ</a></h4>
</div>
<div class="item">
    <img src="{{ asset('img/vay-tra-gop-theo-ngay.png') }}"
         alt="Du lịch trả góp">
    <h4><a href="#loai-tour/du-lich-tra-gop">Du lịch trả góp</a></h4>
</div>
<div class="item">
    <img src="{{ asset('img/vay-tra-gop-theo-ngay.png') }}" alt="Tết Nguyên Đán">
    <h4><a href="#loai-tour/chum-tour-le/tet-nguyen-dan">Tết Nguyên Đán</a>
    </h4>
</div>
<div class="item">
    <img src="{{ asset('img/vay-tra-gop-theo-ngay.png') }}"
         alt="Tour giảm giá sốc">
    <h4><a href="#loai-tour/tour-giam-gia-soc">Tour giảm giá sốc</a></h4>
</div>
<div class="item">
    <img src="{{ asset('img/vay-tra-gop-theo-ngay.png') }}"
         alt="Tour giờ chót">
    <h4><a href="#loai-tour/tour-gio-chot">Tour giờ chót</a></h4>
</div>
<div class="item">
    <img src="{{ asset('img/vay-tra-gop-theo-ngay.png') }}"
         alt="Tour tiêu chuẩn">
    <h4><a href="#loai-tour/tour-tieu-chuan">Tour tiêu chuẩn</a></h4>
</div>