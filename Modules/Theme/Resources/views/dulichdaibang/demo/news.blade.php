<div class="col-md-6 col-sm-6 col-xs-6">
    <div class="detail lf-news">
        <a href="#" class="img">
            <img data-sizes="auto" class="lazyautosizes lazyloaded" data-expand="-10" data-src="{{ asset('img/ky-nghi-le-2-9-1.jpeg') }}" alt="Top địa điểm du lịch tâm linh cho kỳ nghỉ lễ 2/9" sizes="264px" src="{{ asset('img/ky-nghi-le-2-9-1.jpeg') }}">
        </a>
        <h3 class="text-uppercase">
            <a href="#">Top địa điểm du lịch tâm linh cho kỳ nghỉ lễ 2/9</a>
        </h3>
        <p>Kỳ nghỉ lễ 2/9 bạn dự định sẽ đi đâu? Đi nghỉ dưỡng ở các địa điểm du lịch nổi tiếng? Hay tham gia các tour du lịch 2/9? Vậy bạn đã từng nghĩ đến sẽ đến các địa điểm du lịch tâm linh cho kỳ nghỉ của mình hay chưa? Hãy cùng vinavivu điểm qua một số điểm đến tâm linh đáng chú ý cho dịp lễ này nhé.</p>
    </div>
</div>
<div class="col-md-6 col-sm-6 col-xs-6">
    <ul>
        <li>
            <div class="detail">
                <a href="#" class="img link-img">
                    <img data-sizes="auto" class="lazyautosizes lazyloaded" data-expand="-10" data-src="{{ asset('img/nhung-quy-tac-bo-tui-khi-di-du-lich-thai-lan.jpg') }}" alt="Những quy tắc bỏ túi khi đi du lịch Thái Lan" sizes="145px" src="{{ asset('img/nhung-quy-tac-bo-tui-khi-di-du-lich-thai-lan.jpg') }}">
                </a>
                <div class="txt-news">
                    <h3>
                        <a href="#">Những quy tắc bỏ túi khi đi du lịch Thái Lan</a>
                    </h3>
                    <span>18-12-2019</span>
                </div>
            </div>
        </li>
        <li>
            <div class="detail">
                <a href="#" class="img link-img">
                    <img data-sizes="auto" class="lazyautosizes lazyloaded" data-expand="-10" data-src="{{ asset('img/dien-bien-top-nhung-toa-do-check-in-nhat-dinh-khong-duoc-bo-qua-tai-dien-bien.jpg') }}" alt="[Điện Biên] Top những tọa độ check-in nhất định không được bỏ qua tại Điện Biên" sizes="145px" src="{{ asset('img/dien-bien-top-nhung-toa-do-check-in-nhat-dinh-khong-duoc-bo-qua-tai-dien-bien.jpg') }}">
                </a>
                <div class="txt-news">
                    <h3>
                        <a href="#">[Điện Biên] Top những tọa độ check-in nhất định không được bỏ qua tại Điện Biên</a>
                    </h3>
                    <span>18-12-2019</span>
                </div>
            </div>
        </li>
    </ul>
</div>