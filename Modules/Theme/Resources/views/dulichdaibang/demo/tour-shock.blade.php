<li>
    <div class="detail">
        <a href="#tour/kham-pha-jeju-hon-dao-thien-duong-4n3d"
           class="img">
            <img data-sizes="auto" class="lazyload" data-expand="-10" data-src="#"
                 alt="Khám phá Jeju-Hòn đảo thiên đường (4N3Đ)">
            <span class="tour-tra-gop">TRẢ GÓP</span>
            <span class="tour-noi-dung-khung-phai" style="background-color: #006eb9"> Chỉ từ 750.000đ/tháng </span>
        </a>
        <div class="txt-blk">
            <div class="rate-price">
                <div class="rate">
                    <img src="{{ asset('img/rate-5.png') }}" alt=""/>
                </div>
                <span>8.490.000đ</span>
            </div>
            <h3>
                <a href="#tour/kham-pha-jeju-hon-dao-thien-duong-4n3d">Khám
                    phá Jeju-Hòn đảo thiên đường (4N3Đ)</a></h3>
            <span class="hk-lc"><i
                        class="hk-ic"></i>Địa điểm: <strong>Đà Nẵng</strong></span>
            <span class="hk-lc"><i class="fa fa-calendar"></i>Thời gian: <strong>4 Ngày 3 Đêm</strong></span>
            <span class="hk-lc"><i
                        class="fa fa-clock-o"></i>Khời hành: <strong>05/06/2019</strong></span>
            <a href="#tour/kham-pha-jeju-hon-dao-thien-duong-4n3d"
               class="view-detail">Chi tiết</a>
        </div>
    </div>
</li>
<li>
    <div class="detail">
        <a href="#tour/kham-pha-vuong-quoc-su-tu-bien-21-03-4n3d"
           class="img">
            <img data-sizes="auto" class="lazyload" data-expand="-10" data-src="{{ asset('img/tourhantet-2-300x200.jpg') }}"
                 alt="Khám phá vương quốc Sư Tử Biển (13/06-4N3Đ)">
            <span class="tour-tra-gop">TRẢ GÓP</span>
            <span class="tour-noi-dung-khung-phai" style="background-color: #3fb846"> Chỉ từ 1,325,000đ/ Tháng </span>
        </a>
        <div class="txt-blk">
            <div class="rate-price">
                <div class="rate">
                    <img src="{{ asset('img/rate-5.png') }}"
                         alt=""/>
                </div>
                <span>14,990,000 VNĐ</span>
            </div>
            <h3>
                <a href="#tour/kham-pha-vuong-quoc-su-tu-bien-21-03-4n3d">Khám
                    phá vương quốc Sư Tử Biển (13/06-4N3Đ)</a></h3>
            <span class="hk-lc"><i class="hk-ic"></i>Địa điểm: <strong>Đà Nẵng</strong></span>
            <span class="hk-lc"><i class="fa fa-calendar"></i>Thời gian: <strong>4 Ngày 3 Đêm</strong></span>
            <span class="hk-lc"><i
                        class="fa fa-clock-o"></i>Khời hành: <strong>13/06/2019</strong></span>
            <a href="#tour/kham-pha-vuong-quoc-su-tu-bien-21-03-4n3d"
               class="view-detail">Chi tiết</a>
        </div>
    </div>
</li>
<li>
    <div class="detail">
        <a href="#tour/tour-le-08-03-da-lat-tu-hue-3n2d-tron-goi-ve-may-bay-khu-hoi"
           class="img"> <img
                    data-sizes="auto" class="lazyload" data-expand="-10" data-src="{{ asset('img/tourhantet-2-300x200.jpg') }}"
                    alt="Tour lễ 12/04 Đà Lạt từ Huế (3N2Đ) trọn gói vé máy bay khứ hồi">
            <span class="tour-hot">HOT</span> <span class="tour-tra-gop">TRẢ GÓP</span>
            <span class="tour-noi-dung-khung-phai" style="background-color: #e91e63"> Chỉ từ 324,000 tháng </span>
        </a>
        <div class="txt-blk">
            <div class="rate-price">
                <div class="rate"><img
                            src="{{ asset('img/rate-5.png') }}"
                            alt=""/>
                </div>
                <span>3.690.000 VNĐ</span></div>
            <h3>
                <a href="#tour/tour-le-08-03-da-lat-tu-hue-3n2d-tron-goi-ve-may-bay-khu-hoi">Tour
                    lễ 12/04 Đà Lạt từ Huế (3N2Đ) trọn gói vé máy bay khứ hồi</a></h3> <span
                    class="hk-lc"><i class="hk-ic"></i>Địa điểm: <strong>Huế</strong></span>
            <span class="hk-lc"><i class="fa fa-calendar"></i>Thời gian: <strong>3 Ngày 2 Đêm</strong></span>
            <span class="hk-lc"><i
                        class="fa fa-clock-o"></i>Khời hành: <strong>Hàng Tháng</strong></span>
            <a href="#tour/tour-le-08-03-da-lat-tu-hue-3n2d-tron-goi-ve-may-bay-khu-hoi"
               class="view-detail">Chi tiết</a></div>
    </div>

</li>
<li>

    <div class="detail">
        <a href="#tour/hue-da-lat-hue-4n3d" class="img"> <img
                    data-sizes="auto" class="lazyload" data-expand="-10" data-src="{{ asset('img/tourhantet-2-300x200.jpg') }}"
                    alt="Du lịch Đà Lạt từ Huế(4N3Đ) siêu HOT &#8211; trọn gói vé máy bay">
            <span class="tour-hot">HOT</span> <span class="tour-tra-gop">TRẢ GÓP</span>
            <span class="tour-noi-dung-khung-phai" style="background-color: #3fb846"> Chỉ từ 376,000 tháng </span>
        </a>
        <div class="txt-blk">
            <div class="rate-price">
                <div class="rate"><img
                            src="{{ asset('img/rate-5.png') }}"
                            alt=""/>
                </div>
                <span>4.250.000 VNĐ</span></div>
            <h3><a href="#tour/hue-da-lat-hue-4n3d">Du lịch Đà Lạt
                    từ Huế(4N3Đ) siêu HOT &#8211; trọn gói vé máy bay</a></h3> <span
                    class="hk-lc"><i class="hk-ic"></i>Địa điểm: <strong>Huế</strong></span>
            <span class="hk-lc"><i class="fa fa-calendar"></i>Thời gian: <strong>4 Ngày 3 Đêm</strong></span>
            <span class="hk-lc"><i class="fa fa-clock-o"></i>Khời hành: <strong>28/4; 2/6; 9/6; 16/6</strong></span>
            <a href="#tour/hue-da-lat-hue-4n3d"
               class="view-detail">Chi tiết</a></div>
    </div>
</li>
<li>

    <div class="detail">
        <a href="#tour/hue-da-lat-hue-3n2d" class="img"> <img
                    data-sizes="auto" class="lazyload" data-expand="-10" data-src="{{ asset('img/tourhantet-2-300x200.jpg') }}"
                    alt="Du lịch Đà Lạt từ Huế (3N2Đ) trọn gói vé máy bay khứ hồi"> <span
                    class="tour-hot">HOT</span> <span class="tour-tra-gop">TRẢ GÓP</span>
            <span class="tour-noi-dung-khung-phai" style="background-color: #e91e63"> Chỉ từ 318,000 tháng </span>
        </a>
        <div class="txt-blk">
            <div class="rate-price">
                <div class="rate"><img
                            src="{{ asset('img/rate-5.png') }}"
                            alt=""/>
                </div>
                <span>3.690.000 VNĐ</span></div>
            <h3><a href="#tour/hue-da-lat-hue-3n2d">Du lịch Đà Lạt
                    từ Huế (3N2Đ) trọn gói vé máy bay khứ hồi</a></h3> <span
                    class="hk-lc"><i class="hk-ic"></i>Địa điểm: <strong>Huế</strong></span>
            <span class="hk-lc"><i class="fa fa-calendar"></i>Thời gian: <strong>3 Ngày 2 Đêm</strong></span>
            <span class="hk-lc"><i
                        class="fa fa-clock-o"></i>Khời hành: <strong>Hàng Tháng</strong></span>
            <a href="#tour/hue-da-lat-hue-3n2d"
               class="view-detail">Chi tiết</a></div>
    </div>
</li>