@extends('theme::'.$themeName.'.frontend.master')
@section('title')
    <title>{{ $page->title }} - {{ $settings['website_name'] }}</title>
    <META NAME="KEYWORDS" content="{{ !empty($page->meta_keyword) ? $page->meta_keyword : $settings['seo_keywords'] }}"/>
    <meta name="description" content="{{ !empty($page->description) ? strip_tags(Str::limit($page->description, 200)) : strip_tags(Str::limit($page->content, 200))}}"/>
@endsection
@section('styles')
    <style>
        .hs-contact form{
            background-color: transparent;
        }
        .hs-contact form label{
            margin-bottom: 5px;
        }
    </style>
@endsection
@section('breadcrumb')
    <div class="bearch">
        <div class="container">
            <p id="breadcrumbs">
                <i class="fa fa-home"></i>
                <span>
                    <span>
                        <a href="{{ url('/') }}">{{ trans('theme::eagles.home') }}</a>
                        <i class="fa fa-angle-double-right"></i>
                        <span class="breadcrumb_last">{{ $page->title }}</span>
                    </span>
                </span>
            </p>
        </div>
    </div>
@endsection
@section('content')
    <div id="content" class="child-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-9">
                    <div class="single-page">
                        <div class="hk-title">
                            <h1 class="title-single">
                                {{ $page->title }}
                            </h1>
                        </div>
                        <article class="post-content">
                            {!! $page->content !!}
                        </article>
                        @if($slugPage === 'lien-he')
                            <div class="hs-contact container-search">
                                <h2 class="text-uppercase text-center" style="font-size: 24px; margin-bottom: 25px; margin-top: 25px;">{{ trans('theme::eagles.send_request') }}</h2>
                                {!! Form::open(['method' => 'POST', 'url' => '', 'role' => 'contact', 'id' => 'contact'])  !!}
                                <div class="hs-form-success text-center" style="display: none">
                                    {{ trans('theme::eagles.success_contact') }}
                                </div>
                                <div class="hs-form-contact row">
                                    <div class="form-group col-md-6">
                                        {!! Form::label('fullname', 'Họ và tên', ['class' => 'control-label label-required']) !!}
                                        {!! Form::text('fullname', null, ['class' => 'form-control', 'required' => 'required']) !!}
                                        {!! $errors->first('fullname', '<p class="help-block">:message</p>') !!}
                                    </div>
                                    <div class="form-group col-md-6">
                                        {!! Form::label('email', 'Email', ['class' => 'control-label label-required']) !!}
                                        {!! Form::email('email', null, ['class' => 'form-control', 'required' => 'required', 'id' => '']) !!}
                                        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                                    </div>
                                    <div class="form-group col-md-6">
                                        {!! Form::label('address', 'Địa chỉ', ['class' => 'control-label']) !!}
                                        {!! Form::text('address', null, ['class' => 'form-control']) !!}
                                    </div>
                                    <div class="form-group col-md-6">
                                        {!! Form::label('phone', 'Điện thoại', ['class' => 'control-label']) !!}
                                        {!! Form::text('phone', null, ['class' => 'form-control']) !!}
                                    </div>
                                    <div class="form-group col-xs-12">
                                        {!! Form::label('message', 'Nội dung', ['class' => 'control-label label-required']) !!}
                                        {!! Form::textarea('message', null, ['class' => 'form-control', 'required' => 'required', 'rows' => 5]) !!}
                                        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                                    </div>
                                    <div class="col-xs-12 text-center text-uppercase">
                                        <button type="submit" class="button-search">{{ __('theme::eagles.send') }}</button>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-3">
                    @include('theme::'.$themeName.'.frontend.layouts.sidebar-right')
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function(){
            $('#contact').submit(function(e){
                e.preventDefault();
                $.ajax({
                    url: '{{ url('/lien-he/ajax') }}',
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize(),
                    success: function (res) {
                        if (res.success === "ok"){
                            $('.hs-form-contact').hide();
                            $('.hs-form-success').show();
                        }else{
                            console.log(res.errors);
                        }
                    },
                    error: function (error) {
                        alert("{{ __('theme::eagles.error') }}");
                    }
                });
            });
        })

    </script>
@endsection
