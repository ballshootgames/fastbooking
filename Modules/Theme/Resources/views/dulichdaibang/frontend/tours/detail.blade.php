@extends('theme::'.$themeName.'.frontend.master')
@section('title')
    <title>{{ $tour->name . ' - ' . $settings['website_name']}}</title>
    <META NAME="KEYWORDS" content="{{ !empty($tour->meta_keyword) ? $tour->meta_keyword : $settings['seo_keywords'] }}"/>
    <meta name="description" content="{{ !empty($tour->name) ? $tour->name : $settings['seo_description'] }}"/>
@endsection
@section('facebook')
    <meta property="og:title" content="{{ $tour->name }} - {{ $settings['website_name'] }}"/>
    <meta name="description" content="{!! !empty($tour->name) ? $tour->name : $settings['seo_description'] !!}"/>
    <meta property="og:image" content="{{ !empty($tour->image) ? asset(Storage::url($tour->image)) : asset(Storage::url($settings['url_logo'])) }}"/>
    <meta property="og:image:secure_url" content="{{ !empty($tour->image) ? asset(Storage::url($tour->image)) : asset(Storage::url($settings['url_logo'])) }}"/>
@endsection
@section('twitter')
    <meta name="twitter:title" content="{{ $tour->name }} - {{ $settings['website_name'] }}"/>
    <meta name="description" content="{!! !empty($tour->name) ? $tour->name : $settings['seo_description'] !!}"/>
    <meta name="twitter:image" content="{{ !empty($tour->image) ? asset(Storage::url($tour->image)) : asset(Storage::url($settings['url_logo'])) }}"/>
@endsection
@section('breadcrumb')
    <div class="bearch">
        <div class="container">
            <p id="breadcrumbs">
                <i class="fa fa-home"></i>
                <span>
                    <span>
                        <a href="{{ url('/') }}">{{ trans('theme::eagles.home') }}</a>
                        <i class="fa fa-angle-double-right"></i>
                        <span>
                            <a href="{{ url($breadcrumb['slug']) }}">{{ $breadcrumb['name'] }}</a>
                            <i class="fa fa-angle-double-right"></i>
                            <span class="breadcrumb_last">{{ $tour->name }}</span>
                        </span>
                    </span>
                </span>
            </p>
        </div>
    </div>
@endsection
@section('content')
    <div id="content" class="single-page">
        <div class="container tour tour-detail">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="tour__title">{!! $tour->name !!}</h1>
                    <div class="tour__code">
                        <span><i class="fa fa-barcode"></i> {{ $tour->prefix_code . $tour->code }}</span>
                        <span class="social-box">
                            @php($people_limit = null)
                            @php($link = Request::fullUrl())
                            @include('theme::layouts.facebook-like', ['link' => $link])
                        </span>
                    </div>
                </div>
            </div>
            <div class="tour__grid-container">
                <div class="grid__col-1">
                    <div class="slider-tour">
                        @if(!empty((optional($tour->galleries)->first)->count()))
                            <ul class="imageGallery">
                                @foreach(\Modules\Tour\Entities\TourGallery::where('tour_id', $tour->id)->orderBy('image_order', 'ASC')->get() as $item)
                                    <li data-thumb="{{ asset(Storage::url($item->image)) }}">
                                        <img src="{{ asset(Storage::url($item->image)) }}" loading="lazy"/>
                                    </li>
                                @endforeach
                            </ul>
                        @else
                            <img src="{{ asset(Storage::url($tour->image)) }}" class="slider-tour--img"/>
                        @endif
                    </div>
                </div>
                <div class="grid__col-2 tour__description">
                    <div class="tour__description-star tour__box">
                        <span><i class="fa fa-star"></i></span>
                        <span><i class="fa fa-star"></i></span>
                        <span><i class="fa fa-star"></i></span>
                        <span><i class="fa fa-star"></i></span>
                        <span><i class="fa fa-star"></i></span>
                    </div>
                    <div class="tour__description-info tour__box">
                        <div class="row">
                            <div class="col-xs-6 fs16 mb10">
                                {{ trans('theme::frontend.tours.tour_day_start') }}
                            </div>
                            <div class="col-xs-6 fs16 mb10">
                                {{ !empty($tour->departure_date) ? Carbon\Carbon::parse($tour->departure_date)->format(config('settings.format.date')) : trans('theme::frontend.tours.tour_daily_start') }}
                            </div>
                            <div class="col-xs-6 fs16 mb10">
                                {{ trans('theme::frontend.tours.tour_time') }}
                            </div>
                            <div class="col-xs-6 fs16 mb10">
                                @if($tour->night_number !== 0)
                                    {{ $tour->day_number }} ngày - {{ $tour->night_number }} đêm
                                @elseif($tour->day_number === 0.5)
                                    1/2 ngày
                                @elseif($tour->night_number !== 0 && $tour->day_number === 0.5)
                                    1/2 ngày - {{ $tour->night_number }} đêm
                                @else
                                    {{ $tour->day_number }} ngày
                                @endif
                            </div>
                            <div class="col-xs-6 fs16">
                                {{ trans('theme::frontend.tours.tour_place_start') }}
                            </div>
                            <div class="col-xs-6 fs16">
                                {{ \Modules\Tour\Entities\TourProperty::where('id',$tour->start_city)->first()->name ?? 'Không xác định' }}
                            </div>
                        </div>
                    </div>
                    <div class="tour__description-price tour__box">
                        <div class="row">
                            <div class="col-xs-6 fs16 mb10">
                                {{ trans('theme::frontend.tours.tour_price') }}
                            </div>
                            <div class="col-xs-6 fs16 mb10 price">
                                {{ number_format($tour->price) }} đ <br/>
                                @empty(!($tour->price_discount))
                                    <small style="text-decoration: line-through">{{ number_format($tour->price_discount) }} đ</small>
                                @endempty
                            </div>
                            @if($tour->limit)
                                <div class="col-xs-6 fs16">
                                    {{ trans('theme::frontend.tours.tour_limit') }}:
                                </div>
                                <div class="col-xs-6 fs16">
                                    @php($people_limit = \Modules\Theme\Entities\Theme::getExistPeopleLimitTour($tour))
                                    <span style="margin-left: 2px" class="label {{ $people_limit > 0 ? 'label-success' : 'label-warning' }}">{{ $people_limit }}</span>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="tour__description-book">
                        <div class="row">
                            @if($people_limit === 0)
                                <div class="contact-tv col-xs-12"><p>Số chỗ còn nhận đã hết, vui lòng liên hệ: <span>{{ $settings['phone_contact_advisory'] }}</span> để được tư vấn!</p></div>
                            @else
                                <div class="contact-tv col-xs-12 fs16 mb10 text-center"><p>Hotline: <span>{{ $settings['phone_contact_advisory'] }}</span> để được tư vấn!</p></div>
                                <div class="col-sm-6 col-md-12 col-lg-6 mb-10">
                                    <a href="{{ url('booking/add-item?id='.$tour->id) }}">
                                        <button class="btn btn-primary form-control">
                                            <i class="fa fa-cart-plus" aria-hidden="true"></i> {{ trans('theme::frontend.tours.tour_book_now') }}
                                        </button>
                                    </a>
                                </div>
                                <div class="col-sm-6 col-md-12 col-lg-6 mb-10">
                                    <button class="btn btn-warning form-control" data-toggle="modal" data-target="#modal-installment">
                                        <i class="fa fa-money" aria-hidden="true"></i> Mua {{ trans('installment::installments.installment') }}
                                    </button>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('theme::'.$themeName.'.frontend.tours.tab-tour')
    </div>
    <div class="container">
        <div class="vua-xem">
            <div class="clear"></div>
        </div>
        @if(!empty($tourLink->tour_related))
            @include('theme::'.$themeName.'.frontend.tours.tour-link-related')
        @else
            @include('theme::'.$themeName.'.frontend.tours.tour-related')
        @endif
    </div>
    <div id="modal-download" class="modal fade hs-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title modal-title-success" style="display: none">
                        <i class="fa fa-envelope" aria-hidden="true"></i> {{ __('Bạn đã gửi email thành công!') }}
                    </h4>
                    <h4 class="modal-title modal-title-frm">
                        <i class="fa fa-file-pdf-o" aria-hidden="true"></i> {{ __('Tải chương trình tour của bạn') }}
                    </h4>
                </div>
                <div class="modal-body modal-download">
                    <div class="modal-img text-center">
                        <img src="{{asset(Storage::url($settings['url_logo']))}}" alt="{{ $settings['website_name']  }}" width="200px"/>
                    </div>
                    <div class="modal-download--form ">
                        {!! Form::open(['method' => 'POST', 'url' => '', 'id' => 'download-file'])  !!}
                            <input type="hidden" name="tour_id" value="{{ $tour->id }}">
                            <div class="hs-send-success text-center modal-download--info" style="display: none">
                                Chúng tôi đã gửi file chương trình tour {{ $tour->name }} đến email của bạn. Vui lòng kiểm tra hộp thư của bạn.
                            </div>
                            <div class="hs-send-frm">
                                <div class="modal-download--info text-center">
                                    Vui lòng nhập email của bạn để nhận được {{ $tour->name }} có chứa tất cả thông tin quan trọng ngay trong hộp thư đến của bạn!
                                </div>
                                <div class="input-group">
                                    {!! Form::email('email', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Email']) !!}
                                    <span class="input-group-right">
                                        <button type="submit" class="btn btn-download">{{ trans('theme::frontend.tours.tour_download_schedule') }}</button>
                                    </span>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modal-installment" class="modal fade hs-modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
{{--                <div class="modal-header">--}}
{{--                    --}}
{{--                    <h4 class="modal-title">--}}
{{--                        <i class="fa fa-money" aria-hidden="true"></i> Mua {{ trans('installment::installments.installment') }}--}}
{{--                    </h4>--}}
{{--                </div>--}}
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="modal-img text-center">
                        <img src="{{asset(Storage::url($settings['url_logo']))}}" alt="{{ $settings['website_name']  }}" width="200px"/>
                    </div>
                    <div class="hs-send-success text-center modal-download--info" style="display: none">
                        Cám ơn bạn đã tin tưởng và chọn dịch vụ của {{ $settings['website_name'] }}!<br/>
                        Quý khách đã gửi thông tin trả góp Tour thành công!<br/>
                        Nhân viên {{ $settings['website_name'] }} sẽ liên hệ với quý khách từ 2 - 4 giờ để tư vấn tour và đăng kí tour trả góp.<br/>
                        Quý khách vui lòng để ý điện thoại của mình, {{ $settings['website_name'] }} xin cám ơn!
                    </div>
                    <div class="modal-download--form " id="hs-frm-info">
                        <div class="row booking-installment">
                            <div class="col-sm-6 col-md-4">
                                <img src="{{ !empty($tour->image) ? asset(Storage::url($tour->image)) : asset('img/noimage.gif') }}"
                                     alt="{{ $tour->name }}">
                            </div>
                            <div class="booking-info col-sm-6 col-md-8">
                                <div class="txt-blk">
                                    <h3><a href="{{ url('tour/'.$tour->slug.'.html') }}"><b>Mua trả góp</b> {{ $tour->name }}</a></h3>
                                    <div class="row">
                                        <p class="col-xs-6 col-sm-6 col-md-4">
                                            <i class="fa fa-barcode"></i> Mã tour:
                                        </p>
                                        <div class="col-xs-6 col-sm-6 col-md-4">
                                            <b>{{ $tour->prefix_code.$tour->code }}</b>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <p class="col-xs-6 col-sm-6 col-md-4">
                                            <i class="fa fa-usd" aria-hidden="true"></i> Giá tour:
                                        </p>
                                        <div class="col-xs-6 col-sm-6 col-md-4">
                                            <b>{{ number_format($tour->price) }} Đ</b>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <p class="col-xs-6 col-sm-6 col-md-4">
                                            <i class="fa fa-map-marker"></i> {{ trans('theme::frontend.tours.tour_place_start') }}
                                        </p>
                                        <div class="col-xs-6 col-sm-6 col-md-4">
                                            <b>{{ \Modules\Tour\Entities\TourProperty::where('id',$tour->start_city)->first()->name ?? 'Không xác định' }}</b>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <p class="col-xs-6 col-sm-6 col-md-4">
                                            <i class="fa fa-calendar" aria-hidden="true"></i> Thời gian:
                                        </p>
                                        <div class="col-xs-6 col-sm-6 col-md-4">
                                            <b>
                                                @if($tour->night_number !== 0)
                                                    {{ $tour->day_number }} ngày - {{ $tour->night_number }} đêm
                                                @elseif($tour->day_number === 0.5)
                                                    1/2 ngày
                                                @elseif($tour->night_number !== 0 && $tour->day_number === 0.5)
                                                    1/2 ngày - {{ $tour->night_number }} đêm
                                                @else
                                                    {{ $tour->day_number }} ngày
                                                @endif
                                            </b>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h3 class="info-installment">Thông tin cá nhân:</h3>
                        {!! Form::open(['method' => 'POST', 'url' => '', 'id' => 'frm-installment', 'class' => 'frm-installment'])  !!}
                            <input type="hidden" name="tour_id" value="{{ $tour->id }}">
                            <div class="form-group">
                                <input type="text" data-validate="customer.name" name="customer[name]" class="form-control" placeholder="Họ và tên*"/>
                            </div>
                            <div class="row">
                                <div class="form-group col-sm-6">
                                    <input type="text" data-validate="customer.phone" name="customer[phone]" class="form-control" placeholder="Số điện thoại*" />
                                </div>
                                <div class="form-group col-sm-6">
                                    <input type="text" name="customer[email]" class="form-control" placeholder="Email"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-sm-6">
                                    <select name="object_installment" id="object_installment" class="form-control">
                                        <option value="0">-- Vui lòng chọn đối tượng trả góp --</option>
                                        @foreach(trans('installment::installments.object_installment_options') as $key => $val)
                                            <option value="{{ $key }}">{{ $val }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-sm-6">
                                    <select name="time_installment" id="time_installment" class="form-control">
                                        @foreach(trans('installment::installments.time_installment_options') as $val)
                                            <option value="{{ $val }}">Thời gian trả góp {{ $val }} tháng</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <textarea name="note" class="form-control" id="order_comments" rows="2" placeholder="Ghi chú"></textarea>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary modal-btn">Gửi</button>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ asset('plugins/moment.min.js') }}" ></script>
    <script>
        const getListComments = (tourId = {{ $tour->id }}, page = 1) => {
            $('#overlay').show();
            $('.fa-loading').show();
            var text = '{{ $tour->name }}';
            $.ajax({
                url: "{{ url('/ajax/getComments') }}",
                type: 'GET',
                dataType: "JSON",
                data: {
                    tourId: tourId,
                    page: page
                },success: function(response){
                    // console.log(response);
                    let html = '';
                    if (response.total >= 1) {
                        let data = response.data;
                        $.each(data, function (key, value) {
                            html += '<div class="row">';
                                html += '<div class="col-xs-12">';
                                    html += '<h4>'+value.user_name+' <small>'+moment(value.updated_at).format('DD/MM/YYYY')+'</small></h4>';
                                    html += '<p>'+value.comment+'</p>';
                                html += '</div>';
                            html += '</div>';
                        });
                        html += `<div class="hs-page clearfix text-right"></div>`;
                    }
                    $('#overlay').hide();
                    $('.fa-loading').hide();
                    $('#list-comment').html(html);
                }
            })
        };
        getListComments({{$tour->id}});
        $(document).ready(function () {
            $('#comment-form').submit(function (e) {
                e.preventDefault();
                $('#overlay').show();
                $('.fa-loading').show();
                $.ajax({
                    // url: $('#comment-form').attr('action'),
                    url: "{{ url('admin/control/store-comment') }}",
                    type: 'POST',
                    dataType: "JSON",
                    data: $(this).serialize(),
                    success: function(response){
                        // console.log(response);
                        // $('.comment-hide').hide();
                        // $('.comment-notes').show();
                        $('#overlay').hide();
                        $('.fa-loading').hide();
                        $('#comment-form')[0].reset();
                        getListComments({{$tour->id}});
                    },
                    error: function (error) {
                        console.log(error);
                        alert('Đã có lỗi xảy ra, vui lòng thử lại!');
                        $('#overlay').hide();
                        $('.fa-loading').hide();
                    }
                })
            })
            $('.schedule-btn').click(function (e) {
                e.preventDefault();
                let hTabs = $('#nav-tabs').height() + 10;
                let h = $(this).siblings('.schedule__desc_content').height();
                if (h > 75) {
                    $(this).html('Xem thêm <i class="fa fa-angle-double-down" aria-hidden="true"></i>').siblings('.schedule__desc_content').css('max-height', '75px');
                    $('html,body').animate({scrollTop: $(this).siblings('.schedule__desc_title').offset().top - hTabs}, 1000);
                }else{
                    $(this).html('Ẩn đi <i class="fa fa-angle-double-up" aria-hidden="true"></i>').siblings('.schedule__desc_content').css('max-height', '100%');
                }
            });
            let schedule = document.querySelectorAll('.schedule .schedule__desc');
            $.each(schedule, (index, item) => {
                // let h = item.offsetHeight;
                // console.log(h);
                // if (h >= 117) {
                    item.children[3].innerHTML = 'Xem thêm <i class="fa fa-angle-double-down" aria-hidden="true"></i>';
                // }
            })
        });
    </script>
    <script>
        $('#download-file').submit(function(e){
            e.preventDefault();
            $.ajax({
                url: '{{ url('/send-file-tour-schedule/ajax') }}',
                type: 'POST',
                dataType: 'JSON',
                data: $(this).serialize(),
                success: function (res) {
                    if (res.success === "ok"){
                        $('.modal-title-frm').hide();
                        $('.hs-send-frm').hide();
                        $('.modal-title-success').show();
                        $('.hs-send-success').show();
                    }else{
                        console.log(res.errors);
                    }
                },
                error: function (res) {
                    let errors = res.responseJSON.errors;
                    $.each(errors, function (key, value) {
                        $("input[name='"+key+"']").addClass('is-invalid');
                        $('#' + key).html(value);
                    })
                }
            });
        })
        $('#frm-installment').submit(function(e){
            e.preventDefault();
            $.ajax({
                url: '{{ url('/send-info-installment/ajax') }}',
                type: 'POST',
                dataType: 'JSON',
                data: $(this).serialize(),
                success: function (res) {
                    if (res.success === "ok"){
                        $('#hs-frm-info').hide();
                        $('.hs-send-success').show();
                    }else{
                        let errors = res.errors;
                        $.each(errors, function (key, value) {
                            $("input[data-validate='"+key+"']").addClass('is-invalid');
                        })
                    }
                },
                error: function (res) {
                    alert('Có vấn đề xảy ra. Vui lòng thử lại!')
                }
            });
        })
    </script>
@endsection