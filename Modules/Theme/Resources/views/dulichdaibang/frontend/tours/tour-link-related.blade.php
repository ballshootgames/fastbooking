<div class="hk-block hk-tour">
    <div class="hk-title">
        <h2>Tour liên quan</h2>
    </div>
    <div class="lst-items">
        <ul class="row">
            @php
                $tourLinkRelated = explode("$", !empty($tourLink) ? $tourLink->tour_related : '');
            @endphp
            @foreach($tourLinkRelated as $id)
                @php($item = \Modules\Tour\Entities\Tour::where([['id','=', !empty($id) ? $id : ''],['active', '=', config('settings.active')]])->first())
                <li class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                    <div class="detail">
                        <a href="{{ url($slugParent . '/' .$item['slug'].'.html') }}" class="img">
                            <img data-sizes="auto" class="lazyload" data-expand="-10"
                                 data-src="{{ !empty($item['image']) ? \App\Traits\ImageResize::getThumbnail($item['image'], 300, 200) : asset('img/noimage.gif') }}" alt="{{ $item['name'] }}" class="lazyloading" data-was-processed="true">
                            @empty(!($item['price_discount']))
                                <span class="tour-discount">-{{ (round(((float)$item['price_discount'] - (float)$item['price']) / (float)$item['price_discount'], 2) * 100) }}%</span>
                            @endempty
                            @if($item['hot'] === 1) <span class="tour-hot">HOT</span> @endif
                        </a>
                        <div class="txt-blk">
                            <div class="rate-price tour--price">
                                <span style="text-decoration: line-through">
                                    @empty(!($item['price_discount']))
                                        {{ number_format($item['price_discount']) }} đ
                                    @endempty
                                </span>
                                <span>{{ number_format($item['price']) }} đ</span>
                            </div>
                            <h3><a href="{{ url($slugParent . '/' .$item['slug'].'.html') }}">{{ $item['name'] }}</a></h3>
                            <span class="hk-lc">
                                        <i class="hk-ic"></i>{{ trans('theme::frontend.tours.tour_place_start') }}:
                                        <strong>{{ \Modules\Tour\Entities\TourProperty::where('id', $item['start_city'])->first()->name ?? 'Không xác định' }}</strong></span>
                            <span class="hk-lc"><i class="fa fa-calendar"></i>{{ trans('theme::frontend.tours.tour_time') }}:
                                        <strong>
                                            @if($item['night_number'] !== 0)
                                                {{ $item['day_number'] }} ngày - {{ $item['night_number'] }} đêm
                                            @elseif($item['day_number'] === 0.5)
                                                1/2 ngày
                                            @elseif($item['night_number'] !== 0 && $item['day_number'] === 0.5)
                                                1/2 ngày - {{ $item['night_number'] }} đêm
                                            @else
                                                {{ $item['day_number'] }} ngày
                                            @endif
                                        </strong>
                                    </span>
                            <span class="hk-lc"><i class="fa fa-clock-o"></i>{{ trans('theme::frontend.tours.tour_day_start') }}:
                                        <strong>{{ !empty($item['departure_date']) ? Carbon\Carbon::parse($item['departure_date'])->format(config('settings.format.date')) : trans('tour::tours.departure_type')[$tour->departure_type] }}</strong>
                                    </span>
                            <a href="{{ url($slugParent . '/' .$item['slug'].'.html') }}" class="view-detail">{{ trans('theme::frontend.tours.tour_detail') }}</a>
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
</div>