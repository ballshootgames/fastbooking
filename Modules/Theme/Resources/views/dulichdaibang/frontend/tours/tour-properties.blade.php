@extends('theme::'.$themeName.'.frontend.master')
@section('title')
    <title>{{ $tourProperties->name }} - {{ $settings['website_name'] }}</title>
    <META NAME="KEYWORDS" content="{{ !empty($tourProperties->meta_keyword) ? $tourProperties->meta_keyword : $settings['seo_keywords'] }}"/>
    <meta name="description" content="{{ !empty($tourProperties->description) ? strip_tags(Str::limit($tourProperties->description, 200)) : $settings['seo_description'] }}"/>
@endsection
@section('breadcrumb')
    <div class="bearch">
        <div class="container">
            <p id="breadcrumbs">
                <i class="fa fa-home"></i>
                <span>
                    <span>
                        <a href="{{ url('/') }}">{{ trans('theme::eagles.home') }}</a>
                        <i class="fa fa-angle-double-right"></i>
                        <span>{{ $tourProperties->name }}</span>
                    </span>
                </span>
            </p>
        </div>
    </div>
@endsection
@section('content')
    <div class="hk-block hk-tour">
        <div class="container">
            <div class="hk-title">
                <h1>{{ $tourProperties->name }}</h1>
            </div>
            <div class="search-tour-js mb-30 m-box-search">
                @include('theme::'.$themeName.'.frontend.layouts.search-form-tour')
            </div>
        </div>
        <div class="container lst-items">
            <ul class="row">
                @if(!empty($tours->count()))
                    @foreach($tours as $tour)
                    <li class="col-xs-12 col-sm-12 col-md-3">
                        <div class="detail">
                            <a href="{{ url( $slugChild  . '/' . $tour->slug ) }}.html" class="img">
                                <img data-sizes="auto" class="lazyload" data-expand="-10"
                                     data-src="{{ !empty($tour->image) ? \App\Traits\ImageResize::getThumbnail($tour->image, 300, 200) : asset('img/noimage.gif') }}"
                                     alt="{{ $tour->name }}">
                                @empty(!($tour->price_discount))
                                    <span class="tour-discount">-{{ (round(((float)$tour->price_discount - (float)$tour->price) / (float)$tour->price_discount, 2) * 100) }}%</span>
                                @endempty
                                @if($tour->hot === 1) <span class="tour-hot">HOT</span> @endif
                                @if($tour->installment === 1) <span class="tour-installment">Trả góp</span> @endif
                            </a>
                            <div class="txt-blk">
                                <div class="rate-price tour--price">
                                <span style="text-decoration: line-through">
                                    @empty(!($tour->price_discount))
                                        {{ number_format($tour->price_discount) }} đ
                                    @endempty
                                </span>
                                    <span>{{ number_format($tour->price) }} đ</span>
                                </div>
                                <h3>
                                    <a href="{{ url( $slugChild  . '/' . $tour->slug ) }}.html">{{ $tour->name }}</a></h3>
                                <span class="hk-lc">
                                    <i class="hk-ic"></i>{{ trans('theme::frontend.tours.tour_place_start') }}:
                                    <strong>{{ \Modules\Tour\Entities\TourProperty::where('id', $tour->start_city)->first()->name ?? 'Không xác định' }}</strong>
                                </span>
                                <span class="hk-lc">
                                    <i class="fa fa-calendar"></i>{{ trans('theme::frontend.tours.tour_time') }}:
                                    <strong>
                                        @if($tour->night_number !== 0)
                                            {{ $tour->day_number }} ngày - {{ $tour->night_number }} đêm
                                        @elseif($tour->day_number === 0.5)
                                            1/2 ngày
                                        @elseif($tour->night_number !== 0 && $tour->day_number === 0.5)
                                            1/2 ngày - {{ $tour->night_number }} đêm
                                        @else
                                            {{ $tour->day_number }} ngày
                                        @endif
                                    </strong>
                                </span>
                                <span class="hk-lc">
                                    <i class="fa fa-clock-o"></i>{{ trans('theme::frontend.tours.tour_day_start') }}:
                                    <strong>{{ !empty($tour->departure_date) ? Carbon\Carbon::parse($tour->departure_date)->format(config('settings.format.date')) : trans('tour::tours.departure_type')[$tour->departure_type] }}</strong>
                                </span>
                                <a href="{{ url('booking/add-item?id='.$tour->id) }}" class="view-detail">{{ trans('theme::frontend.tours.tour_book_now') }}</a>
                            </div>
                        </div>
                    </li>
                @endforeach
                @else
                    <p class="text-center" style="font-size: 16px;">{{ trans('theme::eagles.data_update') }}</p>
                @endif
            </ul>
        </div>
        <div class="container">
            <div class="quatrang">
                {!! $tours->appends(\Request::except('page'))->render() !!}
            </div>
        </div>
    </div>
@endsection
@section('content_destination')
    @include('theme::eagle.frontend.layouts.destination')
@endsection