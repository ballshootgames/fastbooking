<div class="col-xs-12 col-sm-12 col-md-3">
    <div class="related-post"><h3 class="related"><span>{{ trans('theme::frontend.tours.tour_place_same') }}</span></h3>
        <div class="content-related">
            <ul class="row">
                @foreach($tourRelatedSide as $item)
                    <li class="col-xs-12 col-sm-12 col-md-12">
                        <a href="{{ url($slugParent . '/' .$item->slug.'.html') }}">
                            <img data-sizes="auto" class="lazyload" data-expand="-10"
                                 data-src="{{ !empty($item->image) ? \App\Traits\ImageResize::getThumbnail($item->image, 300, 200) : asset('img/noimage.gif') }}"
                                 alt="{{ $item->name }}">
                        </a>
                        <h4><a href="{{ url($slugParent . '/' .$item->slug.'.html') }}">
                                {{ $item->name }}</a></h4>
                        <div class="info-tour-rel">
                            <div class="pull-left">
                                <p>{{ trans('theme::frontend.tours.tour_time') }}:
                                    @if($item->night_number !== 0)
                                        {{ $item->day_number }} ngày - {{ $item->night_number }} đêm
                                    @elseif($item->day_number === 0.5)
                                        1/2 ngày
                                    @elseif($item->night_number !== 0 && $item->day_number === 0.5)
                                        1/2 ngày - {{ $item->night_number }} đêm
                                    @else
                                        {{ $item->day_number }} ngày
                                    @endif
                                </p>
                            </div>
                            <div class="pull-right"><p class="price">{{ number_format($item->price) }} VNĐ</p></div>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>