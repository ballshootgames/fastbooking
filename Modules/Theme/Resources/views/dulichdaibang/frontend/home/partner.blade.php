<div class="hk-title"><h2>{{ trans('theme::eagles.eagle_partner') }}</h2></div>
<div class="owl-carousel owl-carousel-partner owl-theme">
    @if($partners->count() > 0)
    @foreach($partners as $itemPartner)
        <div class="item">
            <img data-sizes="auto" class="lazyload" data-expand="-10" data-src="{{ \Modules\Theme\Entities\Block\Partner::getThumbnail($itemPartner->image, 189, 126) }}" alt="{{ $itemPartner->name }}">
        </div>
    @endforeach
    @else
        @include('theme::'.$themeName.'.demo.partner')
    @endif
</div>
