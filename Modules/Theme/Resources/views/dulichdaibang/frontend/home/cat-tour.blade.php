<div class="container">
    <div class="owl-carousel owl-carousel-partner owl-theme">
        @if($typeTourLists->count() > 0)
            @foreach($typeTourLists as $itemTourList)
                <div class="item">
                    <img src="{{ !empty($itemTourList->image) ? asset(Storage::url($itemTourList->image)) : asset('img/vay-tra-gop-theo-ngay.png') }}" alt="{{ $itemTourList->name }}">
                    <h4>
                        <a href="{{ url(config('theme.slug.slug_tour_list')  . '/' .$itemTourList->slug) }}">{{ $itemTourList->name }}</a>
                    </h4>
                </div>
            @endforeach
        @else
            @include('theme::'.$themeName.'.demo.tour-lists')
        @endif
    </div>
</div>