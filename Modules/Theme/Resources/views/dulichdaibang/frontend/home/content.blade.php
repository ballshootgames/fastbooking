@extends("theme::$themeName.frontend.master")

@section('content')
    <div id="content">
        <div class="top-content">
            @include('theme::'.$themeName.'.frontend.home.slides')
        </div>
{{--        <div class="loai-tour cat-tour">--}}
{{--            @include('theme::'.$themeName.'.frontend.home.cat-tour')--}}
{{--        </div>--}}
        @include('theme::'.$themeName.'.frontend.home.search-tour')
{{--        <div class="container">--}}
{{--            @include('theme::'.$themeName.'.frontend.home.banner')--}}
{{--        </div>--}}
        <div class="container tour-container">
            @include('theme::'.$themeName.'.frontend.home.tour-promotion')
        </div>
        <div class="container tour-container">
            @include('theme::'.$themeName.'.frontend.home.tour-new')
        </div>
        <div class="container tour-container">
            @include('theme::'.$themeName.'.frontend.home.tour-shock')
        </div>
        <div class="container tour-container">
            @include('theme::'.$themeName.'.frontend.home.tour-inland')
        </div>
        <div class="container tour-container">
            @include('theme::'.$themeName.'.frontend.home.tour-abroad')
        </div>

        <div class="container">
            <div class="hk-location">
                @include('theme::'.$themeName.'.frontend.home.destination')
            </div>
        </div>
        <div class="slide_khachhang" style="padding-top: 30px; padding-bottom: 30px">
            @include('theme::'.$themeName.'.frontend.home.customer')
        </div>
        <div class="container">
            <div class="why-us">
                @include('theme::'.$themeName.'.frontend.home.why')
            </div>
            <div class="loai-tour doitact">
                @include('theme::'.$themeName.'.frontend.home.partner')
            </div>
        </div>
        <div class="container">
            @include('theme::'.$themeName.'.frontend.home.news')
        </div>
    </div>
@endsection
@section('content_destination')
    @include('theme::'.$themeName.'.frontend.layouts.destination')
@endsection