<div class="row hk-content-bot">
    <div class="col-md-8 col-sm-12 col-xs-12">
        <div class="hk-news">
            <div class="hk-title">
                <a href="{{ url('/' . Str::slug(trans('theme::frontend.news.title'))) }}"><h2>{{ trans('theme::frontend.news.travel_guide') }}</h2></a>
            </div>
            <div class="row lst-news">
                @if($newsListHome->count() > 0)
                    @php
                        $strActive = $str = '';
                    @endphp
                    @for($i = 0; $i < $newsListHome->count(); $i++)
                        @php
                            $pathImage = empty($newsListHome[$i]['image']) ? asset('img/noimage.gif') : asset(Storage::url($newsListHome[$i]['image']))
                        @endphp
                        @if($i == 0)
                            @php
                                $strActive  .= '<a href="'.url(optional($newsListHome[$i]->type)->slug . '/' . $newsListHome[$i]['slug']).'.html" class="img image-thumb image-rand">'
                                            . '<img data-sizes="auto" class="lazyload" data-expand="-10" data-src="'. $pathImage .'" alt="'.$newsListHome[$i]['title'].'" />'
                                            . '</a>'
                                            . '<h3 class="text-uppercase"><a href="'.url( optional($newsListHome[$i]->type)->slug . '/' . $newsListHome[$i]['slug']).'.html">'
                                            . $newsListHome[$i]['title']
                                            . '</a></h3>'
                                            . '<p>'
                                            . Str::limit($newsListHome[$i]['description'],180)
                                            . '</p>';
                            @endphp
                        @else
                            @php
                                $str    .= '<li>'
                                        . '<div class="detail">'
                                        . '<a href="'.url( optional($newsListHome[$i]->type)->slug . '/' . $newsListHome[$i]['slug']).'.html" class="img link-img">'
                                        . '<img data-sizes="auto" class="lazyload" data-expand="-10" data-src="'. $pathImage .'" alt="'.$newsListHome[$i]['title'].'" />'
                                        . '</a>'
                                        . '<div class="txt-news"><h3><a href="'.url( optional($newsListHome[$i]->type)->slug . '/' . $newsListHome[$i]['slug']).'.html">'
                                        . $newsListHome[$i]['title']
                                        . '</a></h3><span>'.\Carbon\Carbon::parse($newsListHome[$i]['updated_at'])->format(config('settings.format.date_')).'</span></div>'
                                        .'</div></li>';
                            @endphp
                        @endif
                    @endfor
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="detail lf-news">
                            {!! $strActive !!}
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <ul>
                            {!! $str !!}
                        </ul>
                    </div>
                @else
                    @include('theme::' . $themeName . '.demo.news')
                @endif
            </div>
        </div>
    </div>

    <div class="col-md-4 col-sm-12 col-xs-12">
        <div class="hk-feedback">
            <div class="hk-title text-left">
                <h2>Cảm nhận</h2>
            </div>
            <ul class="lst-fdb">
                @if($comments->count() > 0)
                    @foreach($comments as $itemComment)
                        <li>
                            <div class="detail">
                                <p>{{ $itemComment->comment }}</p>
                                <div class="img-fdb">
                                    <img style="width: 130px; height: 130px; object-fit: cover" src="{{ !empty($itemComment->avatar) ? asset(Storage::url($itemComment->avatar)) : asset('images/avatar.png') }}"
                                                          alt="{{ $itemComment->user_name }}"/></div>
                                <h3>- {{ $itemComment->user_name }} -</h3>
                            </div>
                        </li>
                    @endforeach
                @else
                    @include('theme::' . $themeName . '.demo.comment')
                @endif
            </ul>
        </div>
    </div>
</div>