<h2>{{ trans('theme::eagles.highlight_destination') }}</h2>
<p>{{ trans('theme::eagles.highlight_destination_description') }}</p>
<ul class="lst-lct">
    @if($tourPlaces->count() > 0)
        @foreach($tourPlaces as $itemPlace)
            <li>
                <div class="detail">
                    <a href="{{ url('diem-den-tour/' . Str::slug($itemPlace->name)) }}" class="img">
                        <img data-sizes="auto" class="lazyload" data-expand="-10"
                             data-src="{{ !empty($itemPlace->image) ? \App\Traits\ImageResize::getThumbnail($itemPlace->image, 300, 200) : asset('img/noimage.gif') }}" alt="{{ $itemPlace->name }}" />
                    </a>
                    <h3><a href="{{ url('diem-den-tour/' . Str::slug($itemPlace->name)) }}"> {{ $itemPlace->name }}</a></h3>
                </div>
            </li>
        @endforeach
    @else
        @include('theme::'.$themeName.'.demo.destination')
    @endif
</ul>