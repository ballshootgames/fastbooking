<ul class="rslides">
    @if($slides->count() > 0)
        @foreach($slides as $itemSlide)
            <li>
                <a href="{{ !empty($itemSlide->link) ? $itemSlide->link : '#' }}">
                    <img src="{{ empty($itemSlide->image) ? '' : asset(Storage::url($itemSlide->image)) }}" alt="{{ $itemSlide->name }}">
                </a>
            </li>
        @endforeach
    @else
        @include('theme::'. $themeName .'.demo.slide')
    @endif
</ul>