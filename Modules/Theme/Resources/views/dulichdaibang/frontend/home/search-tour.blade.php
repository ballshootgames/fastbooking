<div class="box-search">
    <div class="container">
        <div class="col-lg-10 col-lg-offset-1">
            <div role="tabpanel" class="tabs-box m-box-search">
                @include('theme::'.$themeName.'.frontend.home.search-tour-home')
            </div>
        </div>
    </div>
</div>