@if(!empty($tourPromotion))
<div class="hk-block hk-tour">
    <div class="hk-title">
        <h2>{{ __('Tour khuyến mãi') }}</h2>
    </div>
    <div class="lst-items">
            <div class="owl-carousel owl-carousel-tour owl-theme">
            @foreach($tourPromotion as $tour)
                @php($slugTourCategory = optional($tour->properties)->where('type','tour_category')->first())
                @php($slugTourCategory = !empty($slugTourCategory['slug']) ? $slugTourCategory['slug'] : 'tour')
                <div class="item">
                    <div class="detail">
                        <a href="{{ url(  $slugTourCategory  . '/' . $tour->slug ) }}.html" class="img">
                            <img data-sizes="auto" class="lazyload" data-expand="-10"
                                 data-src="{{ !empty($tour->image) ? \App\Traits\ImageResize::getThumbnail($tour->image, 300, 200) : asset('img/noimage.gif') }}"
                                 alt="{{ $tour->name }}">
                            @empty(!($tour->price_discount))
                                <span class="tour-discount">-{{ (round(((float)$tour->price_discount - (float)$tour->price) / (float)$tour->price_discount, 2) * 100) }}%</span>
                            @endempty
                            @if($tour->hot === 1) <span class="tour-hot">HOT</span> @endif
                            @if($tour->installment === 1) <span class="tour-installment">Trả góp</span> @endif
                        </a>
                        <div class="txt-blk">
                            <div class="rate-price tour--price">
                                <span style="text-decoration: line-through">
                                    @empty(!($tour->price_discount))
                                        {{ number_format($tour->price_discount) }} đ
                                    @endempty
                                </span>
                                <span>{{ number_format($tour->price) }} đ</span>
                            </div>
                            <h3>
                                <a href="{{ url( $slugTourCategory  . '/' . $tour->slug ) }}.html">{{ $tour->name }}</a>
                            </h3>
                            <span class="hk-lc">
                            <i class="hk-ic"></i>{{ trans('theme::frontend.tours.tour_place_start') }}:
                            <strong>{{ \Modules\Tour\Entities\TourProperty::where('id', $tour->start_city)->first()->name ?? 'Không xác định' }}</strong>
                        </span>
                            <span class="hk-lc">
                            <i class="fa fa-calendar"></i>{{ trans('theme::frontend.tours.tour_time') }}:
                            <strong>
                                @if($tour->night_number !== 0)
                                    {{ $tour->day_number }} ngày - {{ $tour->night_number }} đêm
                                @elseif($tour->day_number === 0.5)
                                    1/2 ngày
                                @elseif($tour->night_number !== 0 && $tour->day_number === 0.5)
                                    1/2 ngày - {{ $tour->night_number }} đêm
                                @else
                                    {{ $tour->day_number }} ngày
                                @endif
                            </strong>
                        </span>
                            <span class="hk-lc">
                            <i class="fa fa-clock-o"></i>{{ trans('theme::frontend.tours.tour_day_start') }}:
                            <strong>{{ !empty($tour->departure_date) ? Carbon\Carbon::parse($tour->departure_date)->format(config('settings.format.date')) : trans('tour::tours.departure_type')[$tour->departure_type] }}</strong>
                        </span>
                            <a href="{{ url('booking/add-item?id='.$tour->id) }}" class="view-detail">{{ trans('theme::frontend.tours.tour_book_now') }}</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endif