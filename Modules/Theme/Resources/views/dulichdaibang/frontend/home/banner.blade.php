<div class="banner">
    @if($banners->count() > 0)
        <div class="owl-carousel owl-carousel-banner owl-theme">
        @foreach($banners as $itemBanner)
            <div class="item">
                <a href="{{ empty($itemBanner->link) ? '#' : $itemBanner->link }}">
                    <img src="{{ \App\Traits\ImageResize::getThumbnail($itemBanner->image, 400, 164) }}" alt="{{ $itemBanner->name }}">
                </a>
            </div>
        @endforeach
        </div>
    @else
        @include('theme::eagle.demo.banner')
    @endif
    <div class="clear"></div>
</div>