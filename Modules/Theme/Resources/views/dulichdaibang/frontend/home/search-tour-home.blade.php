<div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">
        <form action="{{ url('tour') }}" method="GET" role="form">
            <div class="row">
                <div class="col-xs-12 form-group input-name">
                    <input type="text" name="name" class="form-control txt-name" placeholder="{{ trans('message.search_keyword')  }}"/>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 pr15 tour-select form-group">
                    <select name="tour_place" class="form-control select2" id="tour-place" onchange="onChangeCity()">
                        <option value="">{{ trans('theme::frontend.tours.tour_place') }}</option>
                        @foreach(\Modules\Tour\Entities\TourProperty::where(['type' => 'tour_place', 'parent_id' => null])->orderBy('name','asc')->pluck('name', 'id') as $id => $name)
                            <option {{ Request::get('tour_place') == $id ? "selected" : "" }} value="{{ $id }}">{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 pr15 tour-select form-group">
                    <select name="tour_cities" class="form-control select2" id="tour_cities">
                        <option value="">{{ trans('tour::tour_schedules.schedule_destinations') }}</option>
                        @if(Request::get('tour_place'))
                            @foreach(\Modules\Tour\Entities\TourProperty::where(['type' => 'tour_place', 'parent_id' => Request::get('tour_place')])->pluck('name', 'id') as $id => $name)
                                <option {{ Request::get('tour_cities') == $id ? "selected" : "" }} value="{{ $id }}"> {{ $name }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 pr15 form-group">
                    <button type="button" class="btn-dropdown form-control btn--fixed" onclick="showAdvanced()">Nâng cao</button>
                </div>
                <div class="dropdown-menu dropdown-menu--fixed dropdown-search">
                    <div class="row">
                        <div class="col-sm-6 pr15 tour-select mb-m-15">
                            <select name="tour_list" class="form-control select2">
                                <option value="">{{ trans('theme::frontend.tours.tour_list') }}</option>
                                @foreach(\Modules\Tour\Entities\TourProperty::where('type', 'tour_list')->pluck('name', 'id') as $id => $name)
                                    <option {{ Request::get('tour_list') == $id ? "selected" : "" }} value="{{ $id }}">{{ $name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-6 tour-select">
                            <select name="tour_price" class="form-control select2">
                                <option value="">Khoảng giá</option>
                                @foreach(config('theme.list_price') as $id => $name)
                                    <option {{ Request::get('tour_price') == $id ? "selected" : "" }} value="{{ $id }}">{{ $name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-2 col-lg-2">
                    <button type="submit" class="button-tour">
                        {{ trans('theme::frontend.search.title') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>