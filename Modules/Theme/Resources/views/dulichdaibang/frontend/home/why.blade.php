<h2 class="why-us--title">{{ trans('theme::eagles.eagle_why_us') }}</h2>
<div class="row why-us--row">
    @if($why_us->count() > 0)
        @php($i = 0)
        @foreach($why_us as $itemWhy)
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="list-choose why-us--choose">
                    <span>{{ ++$i }}</span>
                    <h4>
                        <a href="{{ $itemWhy['link'] }}" target="_blank">{{ $itemWhy['name'] }}</a>
                    </h4>
                    <p>{{ $itemWhy['description'] }}</p>
                    <div class="clear"></div>
                </div>
            </div>
        @endforeach
    @else
        @include('theme::'.$themeName.'.demo.why')
    @endif
</div>