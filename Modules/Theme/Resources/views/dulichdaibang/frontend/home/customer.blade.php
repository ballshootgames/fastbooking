<div class="container">
    <div class="hk-title">
        <a href="{{ url('/' . Str::slug(trans('theme::eagles.eagle_customer'))) }}">
            <h2>{{ trans('theme::eagles.eagle_customer') }}</h2>
        </a>
    </div>
    <ul class="lst-kh " id="lst-kh-slider">
        @if($customers->count() > 0)
            @foreach($customers as $itemCustomer)
                @php($itemAlbum = explode('$', $itemCustomer->files))
                <li>
                    <a href="{{ url('/' . Str::slug(trans('theme::eagles.eagle_customer')) . '/' . $itemCustomer->slug) }}.html">
                        <img data-sizes="auto" class="lazyload" data-expand="-10"
                             data-src="{{ \App\Traits\ImageResize::getThumbnail($itemAlbum[0], 300, 200) }}" alt="{{ $itemCustomer->title }}" />
                        <div class="kh-title">{{ $itemCustomer->title }}</div>
                    </a>
                </li>
            @endforeach
        @else
            @include('theme::'.$themeName.'.demo.customer')
        @endif
    </ul>
</div>