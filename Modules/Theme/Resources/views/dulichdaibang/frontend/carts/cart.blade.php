@extends('theme::'.$themeName.'.frontend.master')

@section('breadcrumb')
    <div class="bearch">
        <div class="container">
            <p id="breadcrumbs">
                <i class="fa fa-home"></i>
                <span>
                    <span><a href="{{ url('/') }}">{{ trans('theme::eagles.home') }}</a>
                        <i class="fa fa-angle-double-right"></i> <span class="breadcrumb_last">Giỏ hàng</span>
                    </span>
                </span>
            </p>
        </div>
    </div>
@endsection
@section('styles')
    <link rel="stylesheet" id="woocommerce-general-css" href="https://dulichdaibang.com/wp-content/plugins/woocommerce/assets/css/woocommerce.css?ver=3.2.6" type="text/css" media="all">
    <link rel="stylesheet" id="woocommerce-layout-css" href="https://dulichdaibang.com/wp-content/plugins/woocommerce/assets/css/woocommerce-layout.css?ver=3.2.6" type="text/css" media="all">
    <style>
        .fa-loading {
            position: absolute;
            left: 0;
            right: 0;
            top: 50%;
            z-index: 1000;
        }
        .post-content .woocommerce {
            position: relative;
        }
        #overlay {
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgba(255,255,255,0.6);
            z-index: 4;
            cursor: pointer;
        }
    </style>
@endsection

@section('content')
    {{--@dump(Session::has('cart'))--}}
    {{--@dd($carts)--}}
    @php($linkSocial = Request::fullUrl())
    <section class="single">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-9 col-md-push-3">
                    <div class="single-page">
                        <div class="hk-title">
                            <h1 class="title-single">Giỏ hàng</h1>
                        </div>
                        <article class="post-content">
                            <div class="woocommerce">
                                <div id="overlay" style="display: none"></div>
                                <div class="fa-loading text-center" style="display: none">
                                    <span class="fa fa-spinner fa-spin fa-3x text-primary" aria-hidden="true"></span>
                                </div>
                                @if(Session::has('cart'))
                                    {{--<form class="woocommerce-cart-form" id="woocommerce-cart-form">--}}
                                    <div class="table-responsive">
                                        <table class="shop_table shop_table_responsive cart woocommerce-cart-form__contents" cellspacing="0">
                                            <thead>
                                            <tr>
                                                <th class="product-remove">&nbsp;</th>
                                                <th class="product-thumbnail">&nbsp;</th>
                                                <th class="product-name" style="width: 30%">Sản phẩm</th>
                                                <th class="product-price">Giá</th>
                                                <th class="product-quantity">Số lượng</th>
                                                <th class="product-subtotal">Tổng cộng</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($carts['products'] as $key => $tour)
                                                    <tr class="woocommerce-cart-form__cart-item cart_item">
                                                        <td class="product-remove">
                                                            <a class="remove remove-cart" aria-label="Xóa sản phẩm này" data-tour-id="{{ $tour['item']->id }}" data-product_sku="DOM0174">×</a>
                                                        </td>
                                                        <td class="product-thumbnail">
                                                            <a href="{{ url('/tour/'.$tour['item']->slug.'.html') }}">
                                                                <img src="{{ !empty($tour['item']->image) ? asset(Storage::url($tour['item']->image)) : asset('img/noimage.gif')}}" style="width: 100px"  class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt=""></a>
                                                        </td>
                                                        <td class="product-name" data-title="Sản phẩm">
                                                            <a href="{{ url('tour/'.$tour['item']->slug.'.html') }}">{{ $tour['item']->name }}</a>
                                                        </td>
                                                        <td class="product-price" data-title="Giá">
                                                            <span class="woocommerce-Price-amount amount">{{ number_format($tour['item']->price) }}<span class="woocommerce-Price-currencySymbol"> VND </span></span>
                                                        </td>
                                                        <td class="product-quantity" data-title="Số lượng">
                                                            <div class="quantity">
                                                                <label class="screen-reader-text">Số lượng</label>
                                                                <input type="number" id="quantity" class="input-text cart-qty qty text" data-tour-id="{{ $tour['item']->id }}" step="1" min="1" max="" value="{{ $tour['qty'] }}" title="SL" size="4" pattern="[0-9]*" inputmode="numeric">
                                                            </div>
                                                        </td>
                                                        <td class="product-subtotal" data-title="Tổng cộng">
                                                            <span class="woocommerce-Price-amount amount">{{ number_format($tour['item']->price * $tour['qty']) }}<span class="woocommerce-Price-currencySymbol"> VND </span></span>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    {{--</form>--}}
                                    </div>
                                    <div class="col-sm-offset-6">
                                            <h2>Tổng số lượng</h2>
                                            <table class="table">
                                                <tbody class="tbody-cart-total">
                                                    <tr class="cart-subtotal">
                                                        <th>Tổng cộng</th>
                                                        <td data-title="Tổng cộng"><span class="woocommerce-Price-amount amount">{{ isset($carts['totalPrice']) && !empty($carts['totalPrice']) ? number_format($carts['totalPrice']) : 0 }}<span class="woocommerce-Price-currencySymbol"> VND </span></span></td>
                                                    </tr>
                                                    <tr class="order-total">
                                                        <th>Tổng cộng</th>
                                                        <td data-title="Tổng cộng"><strong><span class="woocommerce-Price-amount amount">{{ isset($carts['totalPrice']) && !empty($carts['totalPrice']) ? number_format($carts['totalPrice']) : 0 }}<span class="woocommerce-Price-currencySymbol"> VND </span></span></strong> </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="wc-proceed-to-checkout">
                                                <a href="{{ url('thanh-toan') }}" class="checkout-button button alt wc-forward">Tiến hành đặt chỗ</a>
                                            </div>
                                        </div>
                                @else
                                    <p class="cart-empty">Chưa có sản phẩm nào trong giỏ hàng.</p>	<p class="return-to-shop">
                                        <a class="button wc-backward" href="{{ url('/tour') }}">
                                            Quay trở lại cửa hàng
                                        </a>
                                    </p>
                                @endif
                            </div>
                        </article>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-3 col-md-pull-9">
                    @include('theme::eagle.frontend.layouts.sidebar-right')
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        function formatNumber(num) {
            return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
        }
        function setHtmlCart(response){
            let html = '';
            html += '<a href="'+'{{ url('gio-hang') }}'+'">';
                html += '{{ trans('theme::eagles.cart') }}: '+response.totalQty+' Tour';
                html += '<span class="woocommerce-Price-amount amount">';
                    html += '- '+formatNumber(response.totalPrice)+'';
                    html += '<span class="woocommerce-Price-currencySymbol"> VNĐ </span>';
                html += '</span>';
            html += '</a>';
            $('#cart-header').html(html);

            let html_2 = '';
            html_2 += '<tr class="cart-subtotal">';
                html_2 += '<th>Tổng cộng</th>';
                html_2 += '<td data-title="Tổng cộng"><span class="woocommerce-Price-amount amount">'+formatNumber(response.totalPrice)+'<span class="woocommerce-Price-currencySymbol"> VND </span></span></td>';
            html_2 += '</tr>';
            html_2 += '<tr class="order-total">';
                html_2 += '<th>Tổng cộng</th>';
                html_2 += '<td data-title="Tổng cộng"><strong><span class="woocommerce-Price-amount amount">'+formatNumber(response.totalPrice)+'<span class="woocommerce-Price-currencySymbol"> VND </span></span></strong></td>';
            html_2 += '</tr>';
            $('.tbody-cart-total').html(html_2);
        }
        $(function () {
            $('.remove-cart').click(function () {
                let c = confirm('Bạn có muốn xóa sản phẩm này không!');
                $('#overlay').show();
                $('.fa-loading').show();
                if (c === true){
                    $.ajax({
                        url: "{{ url('cart/remove-item?id=') }}"+$(this).attr('data-tour-id'),
                        type: 'GET',
                        dataType: "JSON",
                        success: (response) => {
                            $(this).parents('tr').remove('.cart_item');
                            setHtmlCart(response);
                            $('#overlay').hide();
                            $('.fa-loading').hide();
                        },
                        error: function (error) {
                            console.log(error);
                            $('#overlay').hide();
                            $('.fa-loading').hide();
                            alert('Đã có một số sự cố khi hệ thống xử lý, vui lòng thử lại!');
                        }
                    })
                }
            });

            $('.cart-qty').bind('keyup change', function(){
            // $('#woocommerce-cart-form').submit(function(e){
            //     e.preventDefault();
                if (parseInt($(this).val()) === 0){
                    alert('Số sản phẩm nhập phải lớn hơn 0');
                } else{
                    $('#overlay').show();
                    $('.fa-loading').show();
                    $.ajax({
                        url: "{{ url('cart/change-qty-item?id=') }}"+$(this).attr('data-tour-id')+'&qty='+$(this).val(),
                        type: 'GET',
                        dataType: "JSON",
                        success: (response) => {
                            if (response.error_limit !== ''){
                                alert(response.error_limit);
                                $('#overlay').hide();
                                $('.fa-loading').hide();
                            } else {
                                let itemPrice = '<span class="woocommerce-Price-amount amount">'+ formatNumber(response.itemPrice) +'<span class="woocommerce-Price-currencySymbol"> VND </span></span>';
                                $(this).parents('.product-quantity').next('td').html(itemPrice);
                                setHtmlCart(response);
                                $('#overlay').hide();
                                $('.fa-loading').hide();
                            }
                        },
                        error: function (error) {
                            console.log(error);
                            alert('Đã có một số sự cố khi hệ thống xử lý, vui lòng thử lại!');
                            $('#overlay').hide();
                            $('.fa-loading').hide();
                        }
                    });
                }
            });
        })
    </script>
@endsection
