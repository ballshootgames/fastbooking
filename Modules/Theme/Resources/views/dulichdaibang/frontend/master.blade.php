<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="geo.position" content="Huế"/>
    <meta name="geo.region" content="VN"/>
    @section('title')
        <title>{{ $settings['seo_title'] }}</title>
        <META NAME="KEYWORDS" content="{{ $settings['seo_keywords'] }}"/>
        <meta name="description" content="{{ $settings['seo_description'] }}"/>
    @show
    <link rel="shortcut icon" href="{{ !empty($settings['favicon']) ? asset(Storage::url($settings['favicon'])) : url('favicon.ico') }}" />
    <link rel="canonical" href="{{ Request::fullUrl() }}"/>
    <meta property="og:locale" content="{{ app()->getLocale() }}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="{{ Request::fullUrl() }}"/>
    <meta property="og:site_name" content="{{ $settings['seo_title'] }}"/>
    @section('facebook')
        <meta property="og:title" content="{{ $settings['seo_title'] }}"/>
        <meta property="og:description" content="{{ $settings['seo_description'] }}"/>
        <meta property="og:image" content="{{ asset(Storage::url($settings['url_logo'])) }}"/>
        <meta property="og:image:secure_url" content="{{ asset(Storage::url($settings['url_logo'])) }}"/>
    @show
    <meta property="og:image:width" content="458"/>
    <meta property="og:image:height" content="239"/>
    <meta property="fb:app_id" content="655619694879300"/>
    <meta property="fb:admins" content="100007518978857" />
    <meta name="twitter:card" content="summary"/>
    @section('twitter')
        <meta name="twitter:title" content="{{ $settings['seo_title'] }}"/>
        <meta name="twitter:description" content="{{ $settings['seo_description'] }}"/>
        <meta name="twitter:image" content="{{ asset(Storage::url($settings['url_logo'])) }}"/>
    @show
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700|Roboto+Condensed:400,700|Roboto:300,400,500,700&display=swap&subset=latin-ext,vietnamese" />
    <link rel="stylesheet" type="text/css" href="{{ url(mix('css/web-dulichdaibang.css')) }}"/>
    @yield('styles')
    {!! str_replace('<br />','',$settings['google_analytics']) !!}
<body class="home blog">
@include('theme::layouts.facebook-script')
<script type="text/javascript">
    var Tawk_API = Tawk_API || {},
        Tawk_LoadStart = new Date();
    (function () {
        var s1 = document.createElement("script"),
            s0 = document.getElementsByTagName("script")[0];
        s1.async = true;
        s1.src = 'https://embed.tawk.to/5657dc2b81505c8622dad7c0/default';
        s1.charset = 'UTF-8';
        s1.setAttribute('crossorigin', '*');
        s0.parentNode.insertBefore(s1, s0);
    })();
</script>
<div id="wrapper">
    @include('theme::'.$themeName.'.frontend.layouts.header')
    @section('breadcrumb')
    @show
    @yield('content')
    @yield('content_destination')
</div>
@include('theme::'.$themeName.'.frontend.layouts.footer')
{{--<div id="modalLogin" class="modal fade hs-modal">--}}
{{--    <div class="modal-dialog">--}}
{{--        <div class="modal-content">--}}
{{--            <div class="modal-header">--}}
{{--                <button type="button" class="close" data-dismiss="modal">&times;</button>--}}
{{--                <h4 class="modal-title">{{ __('theme::eagles.login') }}</h4>--}}
{{--            </div>--}}
{{--            <div class="modal-body">--}}
{{--                <div class="modal-img text-center">--}}
{{--                    <img src="{{asset(Storage::url($settings['url_logo']))}}" alt="{{ $settings['website_name']  }}" width="200px"/>--}}
{{--                </div>--}}
{{--                <div class="modal-form">--}}
{{--                    <form action="" method="post" class="modal-form__login" id="hs-modal__login">--}}
{{--                        <input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
{{--                        <div class="form-group has-feedback">--}}
{{--                            <input type="text" class="form-control" placeholder="{{ trans('message.user.username_email') }}" name="username" >--}}
{{--                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>--}}
{{--                            <p id="username" class="text-danger"></p>--}}
{{--                        </div>--}}
{{--                        <div class="form-group has-feedback">--}}
{{--                            <input type="password" class="form-control" placeholder="Mật khẩu" name="password" >--}}
{{--                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>--}}
{{--                            <p id="password" class="text-danger"></p>--}}
{{--                        </div>--}}
{{--                        <div id="form-error"></div>--}}
{{--                        <div class="text-center">--}}
{{--                            <button type="submit" class="btn btn-primary modal-btn">Đăng nhập</button>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
{{--<div id="modalRegister" class="modal fade hs-modal">--}}
{{--    <div class="modal-dialog">--}}
{{--        <div class="modal-content">--}}
{{--            <div class="modal-header">--}}
{{--                <button type="button" class="close" data-dismiss="modal">&times;</button>--}}
{{--                <h4 class="modal-title">{{ __('theme::eagles.register') }}</h4>--}}
{{--            </div>--}}
{{--            <div class="modal-body">--}}
{{--                <div class="modal-img text-center">--}}
{{--                    <img src="{{asset(Storage::url($settings['url_logo']))}}" alt="{{ $settings['website_name']  }}" width="200px"/>--}}
{{--                </div>--}}
{{--                <div class="modal-form">--}}
{{--                    <form action="" method="post" id="hs-modal__register">--}}
{{--                        <input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
{{--                        <div class="form-group has-feedback">--}}
{{--                            <input type="text" class="form-control" placeholder="{{ trans('adminlte_lang::message.fullname') }}*" name="name" value="{{ old('name') }}" required autofocus/>--}}
{{--                            <span class="glyphicon glyphicon-user form-control-feedback"></span>--}}
{{--                        </div>--}}
{{--                        <div class="form-group has-feedback">--}}
{{--                            <input type="text" class="form-control" placeholder="{{ trans('adminlte_lang::message.username') }}*" name="username" value="{{ old('username') }}" required/>--}}
{{--                            <span class="glyphicon glyphicon-user form-control-feedback"></span>--}}
{{--                        </div>--}}
{{--                        <div class="form-group has-feedback">--}}
{{--                            <input type="email" class="form-control" placeholder="{{ trans('adminlte_lang::message.email') }}*" name="email" value="{{ old('email') }}" required/>--}}
{{--                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>--}}
{{--                        </div>--}}
{{--                        <div class="form-group has-feedback">--}}
{{--                            <input type="password" class="form-control" placeholder="{{ trans('adminlte_lang::message.password') }}*" name="password" required/>--}}
{{--                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>--}}
{{--                        </div>--}}
{{--                        <div class="form-group has-feedback">--}}
{{--                            <input type="password" class="form-control" placeholder="{{ trans('adminlte_lang::message.retypepassword') }}*" name="password_confirmation" required/>--}}
{{--                            <span class="glyphicon glyphicon-log-in form-control-feedback"></span>--}}
{{--                        </div>--}}
{{--                        <div class="form-group has-feedback">--}}
{{--                            <div class="checkbox-register text-center">--}}
{{--                                <label class="checkbox-register__label">--}}
{{--                                    <input type="checkbox" name="terms" class="checkbox-register__terms" required id="check_register"/>--}}
{{--                                    <span class="checkbox-register__checkmark"></span>--}}
{{--                                </label>--}}
{{--                                <button type="button" class="btn btn-link text-link" data-toggle="modal" data-target="#termsModal">{{ trans('adminlte_lang::message.terms') }}</button>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div id="form-register-error"></div>--}}
{{--                        <div class="form-group text-center modal-btn__register">--}}
{{--                            <button type="submit" class="btn btn-primary modal-btn" disabled>{{ trans('adminlte_lang::message.register') }}</button>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
<script type="text/javascript" src="{{ url(mix('js/web-dulichdaibang.js')) }}"></script>
@yield('scripts')
<script>
    $ = jQuery.noConflict();
    $(function () {
        if (window.innerWidth < 768) {
            $('#content .box-search .tab-content').remove();
            $('#wrapper .search-tour-js .tab-content').remove();
        }else{
            $('#content .box-search .tabs-box').load('{{ url('ajax/getModalSearch?keySearch=home') }}');
            $('#wrapper .search-tour-js').load('{{ url('ajax/getModalSearch') }}');
        }
    });
    $(window).resize(function () {
        var w = $(this).width();
        if (w < 768) {
            $('#content .box-search .tab-content').remove();
            $('#wrapper .search-tour-js .tab-content').remove();
        }else{
            $('#content .box-search .tabs-box').load('{{ url('ajax/getModalSearch?keySearch=home') }}');
            $('#wrapper .search-tour-js').load('{{ url('ajax/getModalSearch') }}');
        }
    });
    function showAdvanced() {
        $('.dropdown-search').toggle();
    }
    function onChangeCategories() {
        var typeId = $('#tour-category').val();
        $("#tour-place").html('<option value="">{{ __('message.loading') }}</option>');
        if (typeId === '') {
            $('#tour-place').html('<option value="">{{ __('Chọn điểm đến') }}</option>');
            $('#tour_cities').html('<option value="">{{ __('tour::tour_schedules.schedule_destinations') }}</option>');
            return true;
        }
        $.ajax({
            url: '{{ url('ajax/getTourPlace?type_id=') }}' + typeId,
            type: 'GET',
            dataType: 'JSON',
            success: function(res){
                $("#tour-place").html('');
                $("#tour-place").html('<option value="">{{ __('Chọn điểm đến') }}</option>');
                $.each(res, (key, value) => {
                    $("#tour-place").append('<option value="'+ key +'">'+ value +'</option>');
                })
            },
            error: function () {
                alert('Có lỗi xảy ra, vui lòng thử lại!');
            }
        })
    }
    function onChangeCity(){
        var idPlace = $('#tour-place').val();
        $("#tour_cities").html('<option value="">{{ __('message.loading') }}</option>');
        if (idPlace === '') return $('#tour_cities').html('<option value="">{{ __('tour::tour_schedules.schedule_destinations') }}</option>');
        $.ajax({
            url: '{{ url('ajax/searchComplete?idPlace=') }}'+idPlace,
            type: 'GET',
            success: function (response) {
                $('#tour_cities').html('<option value="">{{ __('tour::tour_schedules.schedule_destinations') }}</option>');
                $.each(response, function(key, item){
                    $('#tour_cities').append('<option value="'+ item.id +'">'+ item.name +'</option>');
                });
            },
            error: function () {
                alert("Đã có lỗi xả ra, vui lòng thử lại!");
            }
        });
    }
    (function ($) {
        $('#hs-modal__login').submit(function (e) {
            e.preventDefault();
            $.ajax({
                url: '{{ url('login-user/ajax') }}',
                type: 'POST',
                dataType: 'JSON',
                data: $(this).serialize(),
                success: function(res){
                    if (res.success === 'ok') {
                        window.location.href = '{{url('/home')}}';
                    }else{
                        let errorsHtml = '<div class="alert alert-danger">';
                        errorsHtml += res.error;
                        errorsHtml += '</div>';
                        $('#form-error').html(errorsHtml);
                    }
                },
                error: function (res) {
                    let errors = res.responseJSON.errors;
                    $.each(errors, function (key, value) {
                        $("input[name='"+key+"']").addClass('is-invalid');
                        $('#' + key).html(value);
                    })
                }
            });
        });
        $('#check_register').change(function () {
            if(this.checked) {
                $('.modal-btn__register button').prop('disabled', false);
            }else{
                $('.modal-btn__register button').prop('disabled', true);
            }
        });
        $('#hs-modal__register').submit(function (e) {
            e.preventDefault();
            $.ajax({
                url: '{{ url('register-user/ajax') }}',
                type: 'POST',
                dataType: 'JSON',
                data: $(this).serialize(),
                success: function (data) {
                    window.location.reload();
                },
                error: function(data){
                    let errors = data.responseJSON.errors;
                    let errorsHtml = '<div class="alert alert-danger"><ul>';
                    $.each(errors, function (key, value) {
                        errorsHtml += '<li>' + value[0] + '</li>';
                    });
                    errorsHtml += '</ul></div>';
                    $('#form-register-error').html(errorsHtml);
                }
            });
        });
        $('.btn-js').on('click', function () {
            var keySearch = $(this).data('search');
            $.get('{{ url('ajax/getModalSearch?keySearch=') }} ' + keySearch, function (data) {
                $('body').append('<div class="modal fade modal-js" id="modal-search-js" tabindex="-1" role="dialog" aria-hidden="true">\n' +
                                    '<div class="modal-dialog" role="document">\n' +
                                        '<div class="box-search">' +
                                            '<div class="container">' +
                                                '<div class="col-lg-10 col-lg-offset-1">' +
                                                    '<div role="tabpanel" class="tabs-box">' +
                                                        data +
                                                    '</div>\n' +
                                                '</div>\n' +
                                            '</div>\n' +
                                        '</div>\n' +
                                    '</div>\n' +
                                '</div>');

                $('#modal-search-js').modal('show').on('hide.bs.modal', function (e) {
                    $('#modal-search-js').remove();
                });
            });
        });
    })(jQuery);
</script>
</body>
</html>