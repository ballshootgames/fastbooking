@extends('theme::'.$themeName.'.frontend.master')
@section('title')
    <title>{{ $newsTypeParent->title }} - {{ $settings['website_name'] }}</title>
    <META NAME="KEYWORDS" content="{{ !empty($newsTypeParent->meta_keyword) ? $newsTypeParent->meta_keyword : $settings['seo_keywords'] }}"/>
    <meta name="description" content="{{ !empty($newsTypeParent->description) ? strip_tags(Str::limit($newsTypeParent->description, 200)) : strip_tags(Str::limit($newsTypeParent->content, 200)) }}"/>
@endsection
@section('breadcrumb')
    <div class="bearch">
        <div class="container">
            <p id="breadcrumbs">
                <i class="fa fa-home"></i>
                <span>
                    <span>
                        <a href="{{ url('/') }}">{{ trans('theme::eagles.home') }}</a>
                        <i class="fa fa-angle-double-right"></i>
                        <span class="breadcrumb_last">{{ $newsTypeParent->title }}</span>
                    </span>
                </span>
            </p>
        </div>
    </div>
@endsection
@section('content')
    <section class="single category">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8">
                    <div class="content-single content-category">
                        <h1 class="tit">
                            <span class="tm">{{ $newsTypeParent->title }}</span>
                        </h1>
                        @if(($newsLists->count() > 0))
                        <div class="slider-cat">
                            <div id="carousel-id" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    @for($i = 1; $i <= 3;$i++)
                                        <div class="item {{ $i == 1 ? 'active' : '' }}">
                                            <a href="{{ url(optional($newsLists[$i]['type'])->slug . '/' .$newsLists[$i]['slug']) }}.html">
                                                <img src="{{ empty($newsLists[$i]['image']) ? asset('img/noimage.gif') : asset(Storage::url($newsLists[$i]['image'])) }}"
                                                     alt="{{ $newsLists[$i]['title'] }}">
                                            </a>
                                        </div>
                                    @endfor
                                </div>
                                <a class="left carousel-control" href="#carousel-id" data-slide="prev"><span
                                            class="glyphicon glyphicon-chevron-left"></span></a>
                                <a class="right carousel-control" href="#carousel-id" data-slide="next"><span
                                            class="glyphicon glyphicon-chevron-right"></span></a></div>
                        </div>
                        <br>
                            @foreach($newsLists as $item)
                                <div class="list-post">
                                    <a href="{{ url( $slugParent . '/' . $item->slug ) }}.html" class="img link-img">
                                        <img data-sizes="auto" class="lazyload" data-expand="-10" data-src="{{ empty($item->image) ? asset('img/noimage.gif') : \App\Traits\ImageResize::getThumbnail($item->image,200,150) }}"
                                             alt="{{ $item->title }}"/>
                                    </a>
                                    <h4><a href="{{ url( $slugParent . '/' . $item->slug ) }}.html">{!! $item->title !!}</a></h4>
                                    <div class="meta"><span>{{ \Carbon\Carbon::parse($item->updated_at)->format(config('settings.format.date')) }}</span></div>
                                    <article class="post-content">
                                        {!! Str::limit($item->description, 250) !!}
                                    </article>
                                    <div class="clear"></div>
                                </div>
                            @endforeach
                            <div class="quatrang">
                                {!! $newsLists->appends(\Request::except('page'))->render() !!}
                            </div>
                        @else
                            <div class="col-xs-12 text-center">
                                {{ __('Dữ liệu đang cập nhật!') }}
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4">
                    @include('theme::'.$themeName.'.frontend.layouts.sidebar-right')
                </div>
            </div>
        </div>
    </section>
@endsection

