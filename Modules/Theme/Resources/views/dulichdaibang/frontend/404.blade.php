@extends('theme::'.$themeName.'.frontend.master')
@section('title')
    <title>{{ '404: Page not found - ' . $settings['website_name'] }}</title>
    <META NAME="KEYWORDS" content="{{ $settings['seo_keywords'] }}"/>
    <meta name="description" content="{{ $settings['seo_description'] }}"/>
@endsection
@section('breadcrumb')
    <div class="bearch">
        <div class="container">
            <p id="breadcrumbs">
                <i class="fa fa-home"></i>
                <span>
                    <span>
                        <a href="{{ url('/') }}">{{ trans('theme::frontend.home') }}</a>
                        <i class="fa fa-angle-double-right"></i>
                        <span class="breadcrumb_last">{{ trans('theme::frontend.error_page.not_found') }}</span>
                    </span>
                </span>
            </p>
        </div>
    </div>
@endsection
@section('content')
<div id="content" class="child-page">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-9 pull-right">
                <div class="single-page">
                    <div class="hk-title">
                        <h1 class="title-single">
                            {{ trans('theme::frontend.error_page.not_found') }}
                        </h1>
                    </div>
                    <article class="post-content">
                        <div class="text-center">
                            {{ trans('theme::frontend.error_page.sorry_page') }} <a href="{{ url('/') }}">{{ trans('theme::frontend.home') }}</a>
                        </div>
                    </article>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-3">
                @include('theme::eagle.frontend.layouts.sidebar-right')
            </div>
        </div>
    </div>
</div>
@endsection

