@extends('theme::'.$themeName.'.frontend.master')
@section('title')
    <title>{{ trans('theme::eagles.eagle_customer') }} - {{ $settings['website_name'] }}</title>
    <meta name="description" content="{{ trans('theme::eagles.meta_description_customer') }}"/>
@endsection
@section('breadcrumb')
    <div class="bearch">
        <div class="container">
            <p id="breadcrumbs">
                <i class="fa fa-home"></i>
                <span>
                    <span>
                        <a href="{{ url('/') }}">{{ trans('theme::eagles.home') }}</a>
                        <i class="fa fa-angle-double-right"></i>
                        <span class="breadcrumb_last">{{ trans('theme::eagles.eagle_customer') }}</span>
                    </span>
                </span>
            </p>
        </div>
    </div>
@endsection
@section('content')
    <section class="single category">
        <div class="container">
            <div class="content-single content-category">
                <h1 class="tit">
                    <span class="tm">{{ __('Album khách hàng') }}</span>
                </h1>
                <div class="related">
                    <ul class="row">
                        @if($galleries->count() > 0)
                            @foreach($galleries as $item)
                                @php($itemAlbum = explode('$', $item->files))
                                <li class="col-sm-4 col-md-3 mb-30 d-flex">
                                    <a href="{{ url($slugParent . '/' . $item->slug) }}.html" class="gallery_item">
                                        <div class="link-img">
                                            <img data-sizes="auto" class="lazyload" data-expand="-10"
                                                 data-src="{{ asset(Storage::url($itemAlbum[0])) }}"
                                                 alt="{{ $item->title }}" />
                                        </div>
                                        <h4 class="text-center">
                                            {{ $item->title }}
                                        </h4>
                                    </a>
                                </li>
                            @endforeach
                        @endif
                    </ul>
                    <div class="quatrang">
                        {!! $galleries->appends(\Request::except('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

