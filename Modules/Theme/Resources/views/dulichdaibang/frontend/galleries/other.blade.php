<ul class="row">
    @if($otherGalleries->count() > 0)
        <h3 class="col-xs-12 title-related">
            {{ trans('theme::frontend.galleries.txt_other') }}
        </h3>
        @foreach($otherGalleries as $item)
            @php($itemAlbum = explode('$', $item->files))
            <li class="col-sm-4 col-md-3 mb-10 d-flex">
                <a href="{{ url('/'.$slugParent.'/' . $item->slug) }}.html" class="gallery_item">
                    <div class="link-img">
                        <img data-sizes="auto" class="lazyload" data-expand="-10"
                             data-src="{{ asset(Storage::url($itemAlbum[0])) }}" alt="{{ $item->title }}" />
                    </div>
                    <h4 class="text-center">
                        {{ $item->title }}
                    </h4>
                </a>
            </li>
        @endforeach
    @endif
</ul>