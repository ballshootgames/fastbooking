<div class="top-footer">
    <div class="container">
        <div class="hk-block hk-tour">
            <div class="hk-title">
                <h2>{{ trans('theme::eagles.tour_destination') }}</h2>
            </div>
            <ul class="row">
                @foreach($tourDestinations as $itemType)
                    <li class="col-xs-6 col-sm-4 col-md-2">
                        <a href="{{ url( Str::slug(trans('theme::eagles.tour_destination')) . '/' . $itemType->slug) }}">{{ $itemType->name }}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>