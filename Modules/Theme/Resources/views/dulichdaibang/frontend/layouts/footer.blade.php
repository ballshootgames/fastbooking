<div id="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-sm-12 col-xs-12">
                    <h3>{{ $settings['company_name'] }}</h3>
                    @foreach(__('theme::eagles.company_info') as $key => $msg)
                        <span>{{ $msg }}: {{ $settings[$key] }}</span>
                    @endforeach

                    <ul class="ft-social">
                        <li><a href="{{ $settings["facebook_link"] }}" class="hk-ic hk-f" target="_blank">facebook</a></li>
                        <li><a href="{{ $settings["twitter_link"] }}" class="hk-ic hk-t" target="_blank">twitter_link</a></li>
                        <li><a href="{{ $settings["linkedin_link"] }}" class="hk-ic hk-l" target="_blank">linkedin_link</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="ft-sv"><h3>{{ trans('theme::eagles.information_general') }}</h3>
                        <ul class="ft-lst">
                            @foreach(__('theme::eagles.company_general') as $key => $msg)
                                <li>
                                    <a href="#">
                                        {{ $msg  }}: {{ $settings[$key] }}
                                    </a>
                                </li>
                            @endforeach
                            <img src="{{ asset('img/thanh-toan-vinavivu-icon.jpg') }}"
                             alt="Chấp nhận thanh toán" title="Chấp nhận thanh toán" width="150px" height="100px"/>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <div class="ft-sp">
                        <h3>{{ trans('theme::eagles.customer_support') }}</h3>
                        <ul class="ft-lst">
                            @foreach($botMenus as $itemMenu)
                                <li>
                                    <a href="{{ url($itemMenu->slug) }}.html">{{ $itemMenu->title }}</a>
                                </li>
                            @endforeach
                            <a href={{ $settings['ministry_industry_trade_link'] }}
                               target="_blank"><img src="{{ asset('img/dathongbao.png') }}" alt="Đã thông báo với Bộ Công Thương" title="Đã thông báo với Bộ Công Thương" width="100px" height="70px"/>
                            </a>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="copyright">
    <div class="container">
        Bản quyền thuộc về dulichdaibang.com, Tất cả các quyền được bảo hộ và vận hành bởi Eagle Tourist JSC
    </div>
</div>
<nav id="menu-mobile">
    <div id="main-nav">
        @php
            $menu = new \Modules\Theme\Entities\Menu();
            $menu->showMainMenuMobile($mainMenus, Request::url());
        @endphp
        <ul class="mm-listview">
            <li class="more-menu">
                <a href="#"><i class="fa fa-bars"></i></a>
                <ul class="sub-menu">
                    @foreach($typeTourLists as $itemTour)
                        <li>
                            <a href="{{ url('loai-tour/' . $itemTour->slug) }}">{{ $itemTour->name }}</a>
                        </li>
                    @endforeach
                </ul>
            </li>
        </ul>
    </div>
</nav>
<a class="back-to-top"><i class="fa fa-angle-up"></i></a>
{{--<a href="{{"tel:".$settings['phone']}}" class="suntory-alo-phone suntory-alo-green" id="suntory-alo-phoneIcon" style="left: 0; bottom: 120px;">--}}
{{--    <div class="suntory-alo-ph-circle"></div>--}}
{{--    <div class="suntory-alo-ph-circle-fill"></div>--}}
{{--    <div class="suntory-alo-ph-img-circle"><i class="fa fa-phone"></i></div>--}}
{{--</a>--}}
<div class="hotline-phone-ring-wrap">
    <div class="hotline-phone-ring">
        <div class="hotline-phone-ring-circle"></div>
        <div class="hotline-phone-ring-circle-fill"></div>
        <div class="hotline-phone-ring-img-circle">
            <a href="{{"tel:".$settings['phone']}}" class="pps-btn-img">
                <img src="{{ asset('images/icon-phone.png') }}" alt="Telephone" width="50">
            </a>
        </div>
    </div>
    <div class="hotline-bar">
        <a href="{{"tel:".$settings['phone']}}">
            <span class="text-hotline">{{$settings['phone']}}</span>
        </a>
    </div>
</div>
{{--<div class="hidden-xs footer-call">--}}
{{--    <div class="text-hotline"><i class="fa fa-phone"></i> {{ trans('theme::eagles.hotline') }}</div>--}}
{{--    <div class="row row-hotline">--}}
{{--        <div class="col-sm-4 col-md-3 text-center">--}}
{{--            <span class="sdt-tit">{{ trans('theme::eagles.name_tour_inland') }}</span> --}}
{{--            <span class="sdt-num">{{ $settings['phone_contact_tour_inland'] }}</span>--}}
{{--        </div>--}}
{{--        <div class="col-sm-4 col-md-3 text-center">--}}
{{--            <span class="sdt-tit">{{ trans('theme::eagles.name_tour_foreign') }}</span> --}}
{{--            <span class="sdt-num">{{ $settings['phone_contact_tour_foreign'] }}</span>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}