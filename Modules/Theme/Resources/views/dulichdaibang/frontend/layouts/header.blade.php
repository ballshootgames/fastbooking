<div id="header">
    <div class="header-top hidden-xs">
        <div class="container">
            <div class="col-sm-6 col-md-7 col-lg-8 hd-top-lf">
                <p>{{ trans('theme::eagles.welcome') }}<span>{{ $settings['website_name'] }}</span></p>
                <ul class="hk-social">
                    @if($settings['facebook_link'])
                    <li><a href="{{ $settings["facebook_link"] }}" class="hk-ic hk-f" target="_blank">facebook</a></li>
                    @endif
                    @if($settings['twitter_link'])
                    <li><a href="{{ $settings["twitter_link"] }}" class="hk-ic hk-t" target="_blank">twitter_link</a></li>
                    @endif
                    @if($settings['linkedin_link'])
                    <li><a href="{{ $settings["linkedin_link"] }}" class="hk-ic hk-l" target="_blank">linkedin_link</a></li>
                    @endif
                </ul>
            </div>
            <div class="col-sm-6 col-md-5 col-lg-4 hd-top-rg">
                <div class="hd-contact">
                    <span>
                        <a href="tel:{{ $settings['phone'] }}">
                            <i class="fas fa-phone-alt"></i>
                            <label style="font-weight: normal">{{ $settings['phone'] }}</label>
                        </a>
                    </span>
                    <span>
                        <a href="mail:{{ $settings['email_receive_message'] }}">
                            <i class="fa fa-envelope"></i>
                            <label style="font-weight: normal">{{ $settings['email_receive_message'] }}</label>
                        </a>
                    </span>
{{--                    @if(Auth::guest())--}}
{{--                        <span>--}}
{{--                            <a href="#" data-toggle="modal" data-target="#modalLogin">{{ trans('theme::eagles.login') }}</a>--}}
{{--                        </span>--}}
{{--                        <span><a href="#" data-toggle="modal" data-target="#modalRegister">{{ trans('theme::eagles.register') }}</a></span>--}}
{{--                    @else--}}
{{--                        <span><a href="{{ url('/home') }}" class="hs-web-login hs-char">{{ Auth::user()->name }}</a></span>--}}
{{--                        <span>--}}
{{--                            <a class="hs-web-login" href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ trans('theme::eagles.logout') }}</a>--}}
{{--                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">--}}
{{--                                {{ csrf_field() }}--}}
{{--                                <input type="submit" value="logout" style="display: none;">--}}
{{--                            </form>--}}
{{--                        </span>--}}
{{--                    @endif--}}
                </div>
            </div>
        </div>
    </div>
    <div class="header-bot {{ (Request::is('/') || Request::is('trang-chu')) ? 'header-bot-home' : '' }}">
        <div class="container header-container">
            <div class="hk-logo">
                <a href="{{ url('/') }}"><img src="{{asset(Storage::url($settings['url_logo']))}}" alt="{{ $settings['website_name']  }}"/></a>
            </div>
            <div class="hk-menu ml-auto">
                <button type="button" class="btn-none btn-js visible-xs hidden-sm" data-search="{{ (Request::is('/') || Request::is('trang-chu')) ? 'home' : 'tour' }}">
                    <i class="fa fa-search fa-18"></i>
                </button>
                <a href="#menu-mobile" class="menu-mobile">Menu</a>
                @php
                    $menu = new \Modules\Theme\Entities\Menu();
                    $menu->showMainMenu($mainMenus, Request::url());
                @endphp
                <ul id="main-nav" class="menu">
                    <li class="more-menu">
                        <a href="#"><i class="fa fa-bars"></i></a>
                        <ul class="sub-menu">
                            @foreach($typeTourLists as $itemTour)
                                <li>
                                    <a href="{{ url('loai-tour/' . $itemTour->slug) }}">{{ $itemTour->name }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                </ul>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</div>
<style>
    .hk-menu {
        display: flex;
    }
    .hk-menu ul.menu li ul{
        top: 150%;
    }
    .hk-menu ul.menu li ul li ul{
        right: 200px;
    }
    .hk-menu ul.menu li ul li:hover>ul {
        top: 0;
    }
    @media (max-width: 991px){
        .hk-menu ul.menu {
            display: none;
        }
    }
</style>