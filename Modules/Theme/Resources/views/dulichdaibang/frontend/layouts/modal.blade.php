<form action="{{ url('/login') }}" method="POST" id="form-modal-login">
    @csrf
    <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="{{ trans('adminlte_lang::message.username') }}*" required name="username"/>
        <span class="form-control-feedback"><i class="fa fa-user" aria-hidden="true"></i></span>
    </div>
    <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="{{ trans('adminlte_lang::message.password') }}*" required name="password"/>
        <span class="form-control-feedback"><i class="fa fa-lock" aria-hidden="true"></i></span>
    </div>
    <div class="form-login-error"></div>
    <div class="form-login-bottom text-center">
        <button class="btn btn-register hs-btn-red">{{ __('frontend.login') }}</button>
    </div>
    {{--<div class="login-other text-center">
        <p>Hoặc đăng nhập với</p>
        <p><a href="{{ url('redirect/facebook') }}" class="btn btn-social btn-facebook btn-flat" id="social_facebook"><i class="fa fa-facebook"></i> Sign in using Facebook</a></p>
        <p><a href="{{ url('redirect/google') }}" class="btn btn-social btn-google btn-flat" id="social_google"><i class="fa fa-google-plus"></i> Sign in using Google+</a></p>
    </div>--}}
</form>