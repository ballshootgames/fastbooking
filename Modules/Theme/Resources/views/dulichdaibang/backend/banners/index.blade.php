@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __('theme::banners.banner') }}
@endsection
@section('contentheader_title')
    {{ __('theme::banners.banner') }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li class="active">{{ __('theme::banners.banner') }}</li>
    </ol>
@endsection
@section('main-content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">{{ __('message.lists') }}</h3>
            <div class="box-tools">
                {!! Form::open(['method' => 'GET', 'url' => '/admin/themes/banners', 'class' => 'pull-left', 'role' => 'search'])  !!}
                <div class="input-group" style="width: 200px;">
                    <input type="text" value="{{\Request::get('search')}}" class="form-control input-sm" name="search" placeholder="{{ __('message.search_keyword') }}">
                    <span class="input-group-btn">
                        <button class="btn btn-default btn-sm" type="submit">
                            <i class="fa fa-search"></i> {{ __('message.search') }}
                        </button>
                    </span>
                </div>
                {!! Form::close() !!}
                {{--@can('BannerController@store')--}}
                    <a href="{{ url('/admin/themes/banners/create') }}" class="btn btn-success btn-sm" title="{{ __('message.new_add') }}">
                        <i class="fa fa-plus" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.new_add') }}</span>
                    </a>
                {{--@endcan--}}
            </div>
        </div>
        @php($index = ($banners->currentPage()-1)*$banners->perPage())
        <div class="box-body table-responsive no-padding">
            <table class="table table-striped">
                <tbody>
                <tr>
                    <th class="text-center">{{ trans('message.index') }}</th>
                    <th>{{ trans('theme::banners.name') }}</th>
                    <th class="text-center col-md-5">{{ trans('theme::banners.image') }}</th>
                    <th class="text-center">{{ trans('theme::banners.updated_at') }}</th>
                    <th class="text-center"></th>
                </tr>
                @foreach($banners as $item)
                    <tr>
                        <td class="text-center">{{ ++$index }}</td>
                        <td><a href="{{ empty($item->link) ? '#' : $item->link }}" target="_blank">{{ $item->name }}</a></td>
                        <th class="text-center">{!! $item->image ? '<img style="object-fit: cover" width="100%" src="'.asset(Storage::url($item->image)).'">' : '' !!}</th>
                        <td class="text-center">{{ Carbon\Carbon::parse($item->updated_at)->format(config('settings.format.date')) }}</td>
                        <td class="text-center">
                            {{--@can('BannerController@show')--}}
                                <a href="{{ url('/admin/themes/banners/' . $item->id) }}" title="{{ __('message.view') }}"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> {{ __('message.view') }}</button></a>
                            {{--@endcan
                            @can('BannerController@update')--}}
                                <a href="{{ url('/admin/themes/banners/' . $item->id . '/edit') }}" title="{{ __('message.edit') }}"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ __('message.edit') }}</button></a>
                            {{--@endcan
                            @can('BannerController@destroy')--}}
                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['/admin/themes/banners', $item->id],
                                    'style' => 'display:inline'
                                ]) !!}
                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> '.__('message.delete'), array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => __('message.delete'),
                                        'onclick'=>'return confirm("'.__('message.confirm_delete').'")'
                                )) !!}
                                {!! Form::close() !!}
                            {{--@endcan--}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="box-footer clearfix">
                {!! $banners->appends(\Request::except('page'))->render() !!}
            </div>
        </div>
    </div>
@endsection
