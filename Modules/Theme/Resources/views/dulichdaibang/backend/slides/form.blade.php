<div class="box-body">
    @if ($errors->any())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <p><i class="fa fa-fw fa-check"></i> {{ $error }}</p>
            @endforeach
        </div>
    @endif
    <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
        {!! Form::label('name', trans('theme::slides.name'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('name', null, ['class' => 'form-control input-sm']) !!}
            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('image') ? ' has-error' : ''}}">
        {!! Form::label('image', trans('theme::slides.image'), ['class' => 'col-md-3 control-label label-required']) !!}
        <div class="col-md-6">
            <div class="input-group inputfile-wrap ">
                <input type="text" class="form-control input-sm" readonly>
                <div class="input-group-btn">
                    <button type="button" class="btn btn-danger btn-sm">
                        <i class=" fa fa-upload"></i>
                        {{ __('message.upload') }}
                    </button>
                    {!! Form::file('image', array_merge(['class' => 'form-control input-sm', "accept" => "image/*", !empty($slide->image) ? '' : 'required'])) !!}
                </div>
            </div>
            {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
            <div class="clearfix"></div>
            <div class="imgprev-wrap" style="display:{{ !empty($slide->image)?'block':'none' }}">
                <img class="img-preview" style="width: 80%" src="{{ !empty($slide) ? asset(\Storage::url($slide->image)) : '' }}" alt="{{ !empty($slide) ? $slide->name : '' }}"/>
                <i class="fa fa-trash text-danger"></i>
            </div>
        </div>
    </div>
</div>
<div class="box-footer">
    {!! Form::submit(isset($submitButtonText) ? $submitButtonText : __('message.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ url('admin/themes/slides') }}" class="btn btn-default">{{ __('message.close') }}</a>
</div>
@section('scripts-footer')
    <script type="text/javascript">
        $(function(){
            $('#image').change(function () {
                let preview = document.querySelector('img.img-preview');
                let file    = document.querySelector('#image').files[0];
                let reader  = new FileReader();

                if ( /\.(jpe?g|png|gif)$/i.test(file.name) ) {

                    reader.addEventListener("load", function () {
                        preview.src = reader.result;
                        $('.imgprev-wrap').css('display','block');
                        $('.inputfile-wrap').find('input[type=text]').val(file.name);
                    }, false);

                    if (file) {
                        reader.readAsDataURL(file);
                    }
                }else{
                    document.querySelector('#image').value = '';
                }
            });

            $('.imgprev-wrap .fa-trash').click(function () {
                let preview = document.querySelector('img.img-preview');

                if(confirm('{{ __('message.confirm_delete') }}')){
                    preview.src = '';
                    $('.imgprev-wrap').css('display','none');
                    $('.inputfile-wrap').find('input[type=text]').val('');
                    $('.inputfile-wrap').find('input[type=file]').attr('required','required');
                }
            })
        });
    </script>
@endsection
