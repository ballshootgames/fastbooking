<div class="box-body">
    @if ($errors->any())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <p><i class="fa fa-fw fa-check"></i> {{ $error }}</p>
            @endforeach
        </div>
    @endif
    <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
        {!! Form::label('name', trans('theme::galleries.name'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('name', null, ['class' => 'form-control input-sm']) !!}
            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('album') ? ' has-error' : ''}}">
        {!! Form::label('album', trans('theme::galleries.album'), ['class' => 'col-md-3 control-label label-required']) !!}
        <div class="col-md-6">
            <div class="dropzone">
                {{--<input name="file" type="file" multiple>--}}
                {{--<div id="previews">--}}
                {{--<div id="template">--}}
                {{--<div class="dz-preview dz-file-preview">--}}
                {{--<div class="dz-details">--}}
                {{--<div class="dz-filename"><span data-dz-name></span></div>--}}
                {{--<div class="dz-size" data-dz-size></div>--}}
                {{--<img data-dz-thumbnail />--}}
                {{--</div>--}}
                {{--<div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>--}}
                {{--<div class="dz-success-mark"><span>✔</span></div>--}}
                {{--<div class="dz-error-mark"><span>✘</span></div>--}}
                {{--<div class="dz-error-message"><span data-dz-errormessage></span></div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                <div id="actions" class="">
                    <div class="input-group">
                        <input type="text" class="form-control input-sm" readonly>
                        <input type="text" id="files" style="opacity: 0;position: absolute;width: 77px;">
                        <div class="input-group-btn">
                                        <span class="btn btn-danger btn-sm fileinput-button">
                                            <i class="fa fa-upload"></i>
                                            <span>{{ __('message.upload') }}</span>
                                        </span>
                        </div>
                    </div>
                </div>
{{--                @isset($tour->galleries)--}}
{{--                    <div id="files-list" class="galleries">--}}
{{--                        @if(!empty($tour->galleries))--}}
{{--                            @foreach($tour->galleries as $file)--}}
{{--                                <div class="gallery imgprev-wrap imgprev-wrap-gallery" style="display:block">--}}
{{--                                    <img class="img-preview" src="{{ asset(\Storage::url($file->image)) }}" alt="">--}}
{{--                                    <i class="fa fa-trash text-danger" onclick="return deleteFile(this,{{ $file->id }})"></i>--}}
{{--                                </div>--}}
{{--                            @endforeach--}}
{{--                        @endif--}}
{{--                    </div>--}}
{{--                @endisset--}}
                <div class="files galleries" id="previews">
                    <div id="template" class="gallery imgprev-wrap imgprev-wrap-gallery">
                        <input type="hidden" name="files[]" required>
                        <img data-dz-thumbnail/>
                        <div class="progress progress-striped progress-xs no-margin active" role="progressbar" aria-valuemin="0" aria-valuemax="100"
                             aria-valuenow="0">
                            <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                        </div>
                        <div>
                            <strong class="error text-danger" data-dz-errormessage></strong>
                        </div>
                        <i class="fa fa-trash text-danger" ></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
        {!! Form::label('description', trans('theme::galleries.description'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-6">
            {!! Form::textarea('description', null, ['class' => 'form-control input-sm ckeditor']) !!}
            {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>
<div class="box-footer">
    {!! Form::submit(isset($submitButtonText) ? $submitButtonText : __('message.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ url('admin/themes/galleries') }}" class="btn btn-default">{{ __('message.close') }}</a>
</div>
@section('scripts-footer')
    <script type="text/javascript" src="{{ asset('plugins/ckeditor/ckeditor.js') }}" ></script>
    <script>
        CKEDITOR.replace( 'description', ckeditor_options);
    </script>
    <script type="text/javascript">
        function deleteFile(ob, id, folder = null) {
            if(confirm('{{ __('Bạn có muốn xóa file này không?') }}')) {
                $(ob).closest('.imgprev-wrap').addClass('deleting');
                if(id>0) {
                    axios({
                        method: 'delete',
                        url: '{{ url('admin/control/tour-galleries/') }}'+'/'+id,
                    }).then(function (response) {
                        $(ob).closest('.imgprev-wrap').remove();
                    }).catch(function (error) {
                        console.log(error);
                        $(ob).closest('.imgprev-wrap').removeClass('deleting');
                        alert('Error!');
                    });
                }else{
                    axios({
                        method: 'post',
                        url: '{{ url('admin/control/tour-gallery-delete') }}',
                        data: {
                            folder: folder
                        }
                    }).then(function (respon) {
                        $(ob).closest('.imgprev-wrap').remove();
                        //checkRequiredFile();
                    })
                }
            }
            return false;
        }

        //dropzone
        Dropzone.autoDiscover = false;
        var previewNode = document.querySelector("#template");
        previewNode.id = "";
        var previewTemplate = previewNode.parentNode.innerHTML;
        previewNode.parentNode.removeChild(previewNode);

        var myDropzone = new Dropzone(document.body, { // Make the whole body a dropzone
            url: "{{ url('admin/themes/upload') }}", // Set the url
            params: {
                _token: '{{ csrf_token() }}',
                tour_id: '{{ !empty($gallery->id)?$gallery->id:0 }}',
            },
            acceptedFiles: 'image/*',
            timeout: 20000, /*milliseconds*/
            maxThumbnailFilesize: 100,
            maxFilesize: 200, // MB
            thumbnailWidth: 80,
            thumbnailHeight: 80,
            thumbnailMethod: 'contain',
            parallelUploads: 20,
            previewTemplate: previewTemplate,
            autoProcessQueue: true,
            autoQueue: true, // Make sure the files aren't queued until manually added
            previewsContainer: "#previews", // Define the container to display the previews
            clickable: ".fileinput-button" // Define the element that should be used as click trigger to select files.
        });

        myDropzone.on("complete", function(file) {
            var obj = JSON.parse(file.xhr.response);
            file.previewElement.querySelector("img").src = obj.src;
            file.previewElement.querySelector('[name="files[]"]').value = obj.name;
            file.previewElement.querySelector(".progress").style.display = 'none';
            file.previewElement.querySelector(".fa-trash").setAttribute('onclick','return deleteFile(this,'+obj.file_id+',"'+obj.name+'")');
            $('#files').removeAttr('required');
        });
    </script>
@endsection
