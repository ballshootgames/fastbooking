window.$ = window.jQuery = require('jquery');

require('bootstrap-less');

require('../js/jquery.mmenu.all.min');

require('../js/responsiveslides');

require('../js/slick.min');

require('../js/lightslider.min');

//require('../js/lightgallery-all.min');

require('../js/owl.carousel.min');

require('../js/lazysizes.min');

require('../js/select2.min');

require('../js/common');