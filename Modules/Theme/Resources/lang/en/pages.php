<?php

return [
    'page' => 'Trang',
    'slug' => 'Slug',
    'url' => 'Đường dẫn URL',
    'name' => 'Tên',
    'description' => 'Mô tả',
    'content' => 'Nội dung',
    'updated_at' => 'Ngày cập nhật',
    'created_success' => 'Trang đã được thêm!',
    'updated_success' => 'Trang đã được cập nhật!',
    'deleted_success' => 'Trang đã được xóa!',
];