<?php

return [
    'home' => 'Trang chủ',
    'users' => [
        'login' => 'Đăng nhập',
        'logout' => 'Đăng xuất',
        'hello' => 'Xin chào',
        'register' => 'Đăng ký',
        'not_account' => 'Bạn chưa có tài khoản?',
        'accounted' => 'Bạn đã có tài khoản?',
        'forget_password' => 'Quên mật khẩu?',
        'remember' => 'Ghi nhớ',
        'account_name' => 'Tài khoản',
        'full_name' => 'Họ tên',
        'password' => 'Mật khẩu',
        'repeat_password' => 'Nhập lại mật khẩu',
        'email_or_account' => 'Email hoặc tài khoản',
        'back_login' => 'Trở lại đăng nhập',
        'send' => 'Gửi',
        'get_password' => 'Lấy lại mật khẩu'
    ],
    'tours' => [
        'featured' => 'Nổi bật',
        'destination' => 'Điểm đến nổi bật',
        'tour' => 'tours',
        'tour_category' => 'Loại tour',
        'tour_place' => 'Điểm đến',
        'tour_list' => 'Dòng tour',
        'tour_program' => 'Chương trình tour',
        'tour_departure_schedule' => 'Lịch khởi hành',
        'tour_note' => 'Lưu ý',
        'tour_idea_customer' => 'Ý kiến khách hàng',
        'tour_place_start' => 'Nơi khởi hành',
        'tour_time' => 'Thời gian',
        'tour_day_start' => 'Khởi hành',
        'tour_detail' => 'Chi tiết',
        'tour_price' => 'Giá',
        'tour_limit' => 'Số chỗ còn nhận',
        'tour_book_free' => 'Giữ chỗ miễn phí',
        'tour_book_now' => 'Đặt tour ngay',
        'tour_place_same' => 'Cùng địa điểm',
        'tour_daily_start' => 'Hàng ngày',
        'tour_download_schedule' => 'Tải chương trình tour'
    ],
    'newsletter' => [
        'notification' => 'Nhận các thông báo',
        'register_box' => 'Hãy đăng ký hộp thư của bạn',
        'success' => 'Cám ơn bạn đã đăng ký nhận tin tại website của chúng tôi!',
        'error' => 'Đã có lỗi xảy ra!'
    ],
    'footer' => [
        'advisory' => 'Bạn cần tư vấn?',
        'call_us' => 'Gọi cho chúng tôi',
        'send_mail' => 'Gửi email cho chúng tôi',
        'follow_us' => 'Theo dõi chúng tôi tại',
        'company_info' => 'Thông tin công ty',
        'company_general' => 'Thông tin chung',
        'support_customer' => 'Hỗ trợ khách hàng'
    ],
    'error_page' => [
        'not_found' => '404: Không tìm thấy trang',
        'not_data' => 'Không tìm thấy dữ liệu của link này!',
        'sorry_page' => 'Xin lỗi, chúng tôi không thể tìm thấy trang bạn đang tìm kiếm. Hãy thử quay lại'
    ],
    'contact' => [
        'request' => 'Chúng tôi lắng nghe ý kiến từ bạn',
        'description' => 'Gửi yêu cầu cho chúng tôi và chúng tôi sẽ trả lời sớm nhất có thể.'
    ],
    'news' => [
        'related' => 'Bài viết liên quan',
        'random' => 'Bài viết ngẫu nhiên',
        'news_new' => 'Tin mới',
        'travel_guide' => 'Cẩm nang du lịch',
        'title' => 'Tin tức du lịch'
    ],
    'booking' => [
        'adult' => 'Người lớn',
        'child' => 'Trẻ em',
        'baby' => 'Em bé',
        'age' => 'Tuổi',
        'book_now' => 'Đặt ngay',
        'book_tour' => 'Đặt tour',
        'passenger_quanlity' => 'Số lượng hành khách',
        'payment_methods' => 'Phương thức thanh toán',
        'payments' => 'Hình thức thanh toán',
        'paypal' => 'Thanh toán',
        'your_booking' => 'Thông tin tour',
        'book_process' => 'Tiến hành đặt chỗ',
        'confirm' => 'Xác nhận',
    ],
    'price' => [
        'adult' => 'Giá người lớn',
        'child' => 'Giá trẻ em',
        'baby' => 'Giá em bé'
    ],
    'sort_tours' => [
        'sort' => 'Sắp xếp tour',
        'a_z' => 'Tour A đến Z',
        'z_a' => 'Tour Z đến A',
        'min_max' => 'Giá tour thấp đến cao',
        'max_min' => 'Giá tour cao đến thấp'
    ],
    'galleries' => [
        'txt_other' => 'Album ảnh các đơn vị khác'
    ],
    'support' => [
        'title' => 'Hỗ trợ trực tuyến',
        'book_tour' => 'Đặt tour',
        'call' => 'Call',
        'email' => 'Email',
        'advisory_tour' => 'Tư vấn du lịch',
        'advisory_tour_visa' => 'Tư vấn tour nước ngoài, visa',
        'please_contact_address' => 'Mọi chi tiết xin vui lòng gửi về địa chỉ'
    ],
    'search' => [
        'title' => 'Tìm kiếm',
        'result' => 'Kết quả tìm kiếm',
        'not_found' => 'Không có kết quả nào phù hợp. Vui lòng tìm kiếm từ khóa khác!'
    ],
    'please_update' => 'Dữ liệu đang cập nhật!',
];