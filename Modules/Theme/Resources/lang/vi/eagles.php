<?php

return [
    /*Define SEO*/
    'title_seo' => 'Du Lịch Đại Bàng - Nhà tổ chức du lịch quốc tế uy tín, chất lượng hàng đầu !',
    'description_seo' => 'Nhà tổ chức du lịch quốc tế uy tín, chất lượng hàng đầu !',
    'site_name' => 'Du Lịch Đại Bàng',
    'home' => 'Trang chủ',
    /*Define website*/
    'welcome' => 'Chào mừng đến với ',
    'name' => 'Eagle Tourist',
    'full_name' => '#Công ty cổ phần Du Lịch Đại Bàng (Eagle Tourist)',
    'phone' => '0234.393.67.87',
    'country' => 'Việt nam',
    'login' => 'Đăng nhập',
    'register' => 'Đăng kí',
    'logout' => 'Đăng xuất',
    'popup_notification_point' => 'Đăng nhập và tích lũy điểm thưởng mỗi đơn hàng',
    'close' => 'Đóng',
    'cart' => 'Giỏ hàng',
    'find_tour' => 'Tìm tour',
    'search' => 'Tìm kiếm',
    'tour_shock' => 'Tour giá sốc',
    'tour_inland' => 'Tour trong nước được mua nhiều',
    'tour_foreign' => 'Tour nước ngoài được mua nhiều',
    'tour_new_hot' => 'Tour mới cực HOT',
    'hot' => 'HOT',
    'destination' => 'Địa điểm',
    'time' => 'Thời gian',
    'start' => 'Khởi hành',
    'detail' => 'Chi tiết',
    'book_tour' => 'Đặt tour',
    'highlight_destination' => 'Điểm đến nổi bật',
    'highlight_destination_description' => 'TOP điểm đến hấp dẫn nên khám phá',
    'eagle_customer' => 'Khách hàng Eagle Tourist',
    'meta_description_customer' => 'Khách hàng Eagle Tourist thân quen',
    'eagle_why_us' => 'Vì sao chọn Eagle Tourist',
    'eagle_partner' => 'Đối tác',
    'news_tour' => 'Cẩm nang du lịch',
    'comment' => 'Cảm nhận',
    'tour_destination' => 'Địa điểm du lịch',
    'information_general' => 'Thông tin chung',
    'customer_support' => 'HỖ TRỢ KHÁCH HÀNG',
    'name_tour_inland' => 'Tour trong nước',
    'name_tour_foreign' => 'Tour nước ngoài',
    'phone_inland' => '0914.510.246 - 0911.345.808',
    'phone_foreign' => '0911.345.848 - 0911.344.883',
    'installment' => 'TRẢ GÓP',
    'hotline' => 'Hotline',
    'follow_header' => [
        'facebook' => 'https://www.facebook.com/dulichdaibang.vn/',
        'linkedin' => '#',
        'pinterest' => '#',
        'twitter' => '#'
    ],
    'follow_footer' => [
        'facebook' => 'https://www.facebook.com/dulichdaibang.vn/',
        'twitter' => '#',
        'google' => '#',
        'linkedin' => '#',
    ],
    'data_update' => 'Dữ liệu đang cập nhật!',
    'destinations' => 'Địa điểm',
    'email_booking' => 'booking@dulichdaibang.com',
    'email_support' => 'hotro@dulichdaibang.com',
    'seo' => [
        'title_tour' => 'Tour du lịch',
        'des_tour' => 'Du Lịch Đại Bàng xin cung cấp bảng giá vé nghe ca Huế trên sông Hương, giá thuê dịch vụ thuyền rồng trên sông Hương để quý du khách tiện tham khảo. Thưởng thức, nghe ca Huế trên sông Hương bạn còn được ngắm cầu Trường Tiền lung linh, huyền ảo về đêm &#8211; Thưởng thức những làn điệu ca Huế độc đáo.'
    ],
    'send_request' => 'Gửi yêu cầu cho chúng tôi!',
    'send' => 'Gửi',
    'error' => 'Xảy ra lỗi, vui lòng kiểm tra lại thông tin!',
    'success_contact' => 'Xin cảm ơn! Thông tin yêu cầu của bạn đã được gửi đi!',
    'company_info'=>[
        'company_address' => 'Địa chỉ',
        'phone' => 'Điện thoại',
        'email_receive_message' => 'Email',
        'website_link' => 'Website',
    ],
	'company_general'=>[
		'managing' => 'Chủ quản',
		'representative' => 'Người đại diện',
		'tax_code' =>'MST',
		'issued_by_tax_code'=>'Nơi cấp',
		'field'=>'Lĩnh vực',
		'travel_business_license'=>'Giấy phép KDLQ quốc tế'
	]

];
