<?php

return [
    'partner' => [
        'title' => 'Đối tác',
        'name' => 'Tên',
        'link' => 'Link',
        'image' => 'Ảnh',
        'active' => 'Duyệt',
        'arrange' => 'Sắp xếp',
        'updated_at' => 'Ngày cập nhật',
        'created_success' => 'Đối tác đã được thêm!',
        'updated_success' => 'Đối tác đã được cập nhật!',
        'deleted_success' => 'Đối tác đã được xóa!',
    ],
    'why_us' => [
        'title' => 'Vì sao chọn',
        'name' => 'Tên',
        'link' => 'Link',
        'description' => 'Mô tả',
        'active' => 'Duyệt',
        'arrange' => 'Sắp xếp',
        'updated_at' => 'Ngày cập nhật',
        'created_success' => 'Vì sao chọn đã được thêm!',
        'updated_success' => 'Vì sao chọn đã được cập nhật!',
        'deleted_success' => 'Vì sao chọn đã được xóa!',
    ],
];
