<?php

return [
    'gallery' => 'Album khách hàng',
    'title' => 'Tên',
    'slug' => 'Slug',
    'album' => 'Albums',
    'description' => 'Mô tả',
    'active' => 'Duyệt',
    'arrange' => 'Sắp xếp',
    'updated_at' => 'Ngày cập nhật',
    'created_success' => 'Album khách hàng đã được thêm!',
    'updated_success' => 'Album khách hàng đã được cập nhật!',
    'deleted_success' => 'Album khách hàng đã được xóa!',
];
