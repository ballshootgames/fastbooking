<?php

return [
    'banner' => 'Banner',
    'name' => 'Tên',
    'link' => 'Link',
    'image' => 'Ảnh',
    'updated_at' => 'Ngày cập nhật',
    'created_success' => 'Banner đã được thêm!',
    'updated_success' => 'Banner đã được cập nhật!',
    'deleted_success' => 'Banner đã được xóa!',
];
