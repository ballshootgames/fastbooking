<?php

return [
    'theme' => 'Theme',
    'name' => 'Tên theme',
    'image' => 'Template',
    'view' => 'Xem',
    'name_theme' => 'Eagle Tourist',
    'created_success' => 'Thêm mới theme thành công!',
    'updated_success' => 'Cập nhật theme thành công!',
    'deleted_success' => 'Xóa theme thành công!'
];
