<?php

return [
    'contact' => 'Liên hệ',
    'name' => 'Họ tên',
    'email' => 'Email',
    'address' => 'Địa chỉ',
    'phone' => 'Số điện thoại',
    'content' => 'Nội dung',
    'updated_at' => 'Ngày cập nhật',
    'created_success' => 'Liên hệ đã được thêm!',
    'updated_success' => 'Liên hệ đã được cập nhật!',
    'deleted_success' => 'Liên hệ đã được xóa!',
];