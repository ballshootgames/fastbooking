<?php

return [
    'menu' => 'Hệ thống menu',
    'type_id' => 'Template',
    'slug' => 'Slug',
    'title' => 'Tiêu đề',
    'position' => 'Vị trí hiển thị',
    'arrange' => 'Sắp xếp',
    'url' => 'Đường dẫn trang',
    'updated_at' => 'Ngày cập nhật',
    'note' => 'Ghi chú',
    'parent_id' => 'Thể loại',
    'keywords' => 'Trình diễn',
    'note_content' => 'Trường Slug bạn có thể để trống, hệ thống sẽ tự động lấy theo tiêu đề của trang, nhưng bạn cũng có thể tùy chỉnh theo nhu cầu. Ví dụ: ("Giới thiệu" = "gioi-thieu")',
    'created_success' => 'Hệ thống menu đã được thêm!',
    'updated_success' => 'Hệ thống menu đã được cập nhật!',
    'deleted_success' => 'Hệ thống menu đã được xóa!',
];