<?php

return [
    'newsletter' => 'Đăng ký nhận tin',
    'email' => 'Email',
    'created_at' => 'Ngày đăng ký',
    'created_success' => 'Đăng ký nhận tin đã được thêm!',
    'updated_success' => 'Đăng ký nhận tin đã được cập nhật!',
    'deleted_success' => 'Đăng ký nhận tin đã được xóa!',
];