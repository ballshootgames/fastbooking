<?php

return [
    'slide' => 'Slide',
    'name' => 'Tên',
    'link' => 'Liên kết',
    'image' => 'Ảnh',
    'updated_at' => 'Ngày cập nhật',
    'created_success' => 'Ảnh đã được thêm!',
    'updated_success' => 'Ảnh đã được cập nhật!',
    'deleted_success' => 'Ảnh đã được xóa!',
];
