<?php

namespace Modules\Theme\Entities\Block;

use App\Traits\ImageResize;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
	use ImageResize;
	public static $imageFolder = "banners";
	public static $imageMaxWidth = 400;
	public static $imageMaxHeight = 164;

    protected $table = 'banners';

    protected $primaryKey = 'id';

    protected $fillable = ['name', 'image', 'link'];

}
