<?php

namespace Modules\Theme\Entities\Block;

use App\Setting;
use App\Traits\ImageResize;
use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
	use ImageResize;
	public static $imageFolder = "slides";
	public static $imageMaxWidth = 1920;
	public static $imageMaxHeight = 420;
	public static $imageAspectRatio = false;//giữ nguyên tỉ lệ ảnh

    protected $table = 'slides';

    protected $primaryKey = 'id';

    protected $fillable = ['name', 'image', 'link'];

    public function getThemes(){
        return Setting::getSettingThemeName();
    }
}
