<?php

namespace Modules\Theme\Entities\Block;

use App\Traits\ImageResize;
use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
	use ImageResize;
	public static $imageFolder = "partners";
	public static $imageMaxWidth = 300;
	public static $imageMaxHeight = 200;

    protected $table = 'partners';

    protected $primaryKey = 'id';

    protected $fillable = ['name', 'link', 'image', 'description', 'ptype_id', 'active', 'arrange'];

}
