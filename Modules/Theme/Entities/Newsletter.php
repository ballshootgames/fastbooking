<?php

namespace Modules\Theme\Entities;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Newsletter extends Model
{
    use Sortable;

    protected $table = 'newsletters';

    protected $primaryKey = 'id';

    public $sortable = ['email','created_at'];

    protected $fillable = ['email'];
}
