<?php

namespace Modules\Theme\Entities;

use App\Traits\ImageResize;
use Illuminate\Database\Eloquent\Model;

class Theme extends Model
{
    use ImageResize;
    public static $imageFolder = "theme";
    public static $imageMaxWidth = 460;
    public static $imageMaxHeight = 680;
    public static $imageAspectRatio = true;//giữ nguyên tỉ lệ ảnh

    protected $table = 'themes';

    protected $primaryKey = 'id';

    protected $fillable = ['name', 'image'];

    public function blocks(){
        return $this->hasMany('Modules\Theme\Entities\ThemeBlock', 'theme_id');
    }

    public static function getExistPeopleLimitTour($service = null){
        if (empty($service) || empty($service->limit)) return;

        $limit = $service->limit;
        $allTourPeople = null;
        foreach ($service->bookings as $key){
            $allTourPeople += ($key->adult_number + $key->child_number + $key->baby_number);
        }
        $existTourPeople = $limit - $allTourPeople;

        return $existTourPeople;
    }
}
