<?php

namespace Modules\Theme\Entities;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Contact extends Model
{
    use Sortable;
    protected $table = 'contacts';

    protected $primaryKey = 'id';

    protected $sortable = [
        'name',
        'email',
        'phone',
        'updated_at'
    ];

    protected $fillable = ['name', 'email', 'address', 'phone', 'content'];
}
