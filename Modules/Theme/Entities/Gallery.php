<?php

namespace Modules\Theme\Entities;

use App\Traits\ImageResize;
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    use ImageResize;
    public static $imageFolder = "galleries";
    public static $imageMaxWidth = 750;
    public static $imageMaxHeight = 750;

    protected $table = 'galleries';

    protected $primaryKey = 'id';

    protected $fillable = ['title', 'slug' , 'name', 'files', 'active', 'arrange', 'description'];
}
