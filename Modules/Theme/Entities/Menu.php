<?php

namespace Modules\Theme\Entities;

use App\Setting;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Request;
use Kalnoy\Nestedset\NodeTrait;
use Modules\News\Entities\News;
use Modules\News\Entities\NewsType;
use Modules\Tour\Entities\Tour;
use Modules\Tour\Entities\TourProperty;
use Illuminate\Support\Arr;

class Menu extends Model
{
    use NodeTrait;

    protected $table = 'menus';

    protected $primaryKey = 'id';

    protected $fillable = ['type_id', 'title', 'slug', 'position', 'arrange', 'parent_id','keywords','active'];

    public static $KEYWORDS = [
        'PAGES' => 'Trang',
        'NEWS_TYPE' => 'Loại bài viết',
        'TOURS_TYPE' => 'Loại tours'
    ];

    public static function getKeywords(){
        $arrKeywords = [
            'pages' => 'Trang',
            'news_type' => 'Danh mục bài viết',
            'tour_type' => 'Danh mục tours'
        ];
        return $arrKeywords;
    }
    public static function getTemplate(){
        $arr = ['Template danh mục', 'Template chi tiết', 'Template trang'];
        return $arr;
    }

    public static function setTrueMenuSlug($slug){
        $menus = Menu::pluck('slug')->toArray();
        if (in_array($slug, $menus)){
            $newSlug = '';
            for ($i = 1; $i < 10; $i++){
                $newSlug = $slug . '-' . $i;
                if (!in_array($newSlug,$menus)) break;
            }
            return $newSlug;
        }
        return $slug;
    }

    public static function setSlug($slug){
        $newsType = NewsType::pluck('slug')->toArray();
        $news = News::pluck('slug')->toArray();
        $tourProperty = TourProperty::pluck('slug')->toArray();
        $page = Page::pluck('slug')->toArray();
        $tours = Tour::pluck('slug')->toArray();
        $gallery = Gallery::pluck('slug')->toArray();
        $arrSlug = Arr::collapse([$newsType,$news,$tourProperty,$tours,$page,$gallery]);

        if (in_array($slug,$arrSlug)){
            $newSlug = '';
            for ($i = 1; $i < 100; $i++){
                $newSlug = $slug . '-' . $i;
                if (!in_array($newSlug,$arrSlug)) break;
            }
            return $newSlug;
        }
        return $slug;
    }

    public function getListMenuToArray($listMenu, $menuId = null, $index = '', $result = [])
    {
        global $result;
        foreach ($listMenu as $item){
            $result[$item->id] = $index.$item->title;
            if (!empty($item->children)){
                $this->getListMenuToArray($item->children, $menuId, $index . '-- ', $result);
            }
        }
        if (!empty($menuId)){
            if (array_key_exists($menuId, $result)){
                unset($result[$menuId]);
            }
        }

        return $result;
    }

    public function showListMenu($menus, $level = ''){
        $typeMenu = Menu::getTemplate();
        foreach ($menus as $menu){
            echo '<tr>';
            echo '<td>'.$level.$menu->title.'</td>';
            echo '<td>'.$menu->slug.'</td>';
            echo '<td>'.$menu->arrange.'</td>';
            echo '<td>'.$typeMenu[$menu->type_id].'</td>';
            echo '<td>'.Carbon::parse($menu->updated_at)->format(config('settings.format.datetime')).'</td>';
            echo '<td>';
            echo '<a style="padding-right: 4px" href="'.url('/admin/themes/menus/' . $menu->id).'" title="'.__('message.show').'"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> '.__('message.view').'</button></a>';
            echo '<a href="'.url('/admin/themes/menus/' . $menu->id . '/edit').'" title="'.__('message.edit').'"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> '.__('message.edit').'</button></a>';
            echo '<form method="POST" action="'.url('/admin/themes/menus/'.$menu->id).'" style="display: inline">
                            <input type="hidden" name="_method" value="delete" />
                            <input class="_token" name="_token" type="hidden" value="'.csrf_token().'">
                            <button type="submit" class="com-delete btn btn-danger btn-xs" onclick="return confirm(&quot;Bạn chắc chắn muốn xoá phần từ này?&quot;)" title="Xóa"><i class="fa fa-trash-o" aria-hidden="true"></i> Xóa</button>
                          </form>';
            echo '</td>';
            echo '</tr>';
            if (!empty($menu->children->first())){
                $this->showListMenu($menu->children, $level.'-- ');
            }
        }
    }
    public function showMainMenu($menus, $url = null, $level = 1, $slug = ''){
        $element = config('theme.class.'.Setting::getSettingThemeName());
//        dd($element);
        if ($level == 1){
            $ul = $element['ul_lv_1'];
        }else{
            $ul = $element['ul_sub'];
        }
        echo '<ul '.$ul.'>';
        foreach ($menus as $menu){
            if (!empty($menu->children->first())){
                $a = $element['a'];
                $i = $element['i'];
                //$span = '<span class="hs-caret" data-toggle="dropdown"><i class="caret"></i></span>';
                if (!empty(strpos($url, $menu->slug))){
                    $li = $element['li_children_active'];
                }else{
                    $li = $element['li_children'];
                }
            }else{
                $a = $element['a'];
                $i = '';
                //$span = '';
                if (!empty(strpos($url, $menu->slug))){
                    $li = $element['li_active'];
                }else{
                    $li = $element['li'];
                }
            }
            if (($menu->type_id == config('theme.type_menu.detail')) || ($menu->type_id == config('theme.type_menu.page'))) {
                $html = '.html';
            }else{
                $html = '';
            }
            echo '<li '.$li.'>';
            echo '<a href="'.url($slug.'/'.$menu->slug).$html.'"'.$a.'>'.$menu->title.'</a>';
            echo $i;
            if (!empty($menu->children->first())){
                    if ($level > 1){
                        $true_slug = $menu->slug;
                    }else{
                        $true_slug = $slug. '/' . $menu->slug;
                    }
                    $this->showMainMenu($menu->children, $url,$level+1, $true_slug);
            }
            echo '</li>';
        }
        echo '</ul>';
    }
    public function showMainMenuMobile($menus, $url = null, $level = 1, $slug = ''){
        if ($level == 1){
            $ul = 'class="nav-menu"';
        }else{
            $ul = 'class = "sub-menu"';
        }
        echo '<ul '.$ul.'>';
        foreach ($menus as $menu){
            if (!empty($menu->children->first())){
                $a = '';
                if (!empty(strpos($url, $menu->slug))){
                    $li = 'class="dropdown active"';
                }else{
                    $li = 'class="dropdown"';
                }
            }else{
                $a = '';
                if (!empty(strpos($url, $menu->slug))){
                    $li = 'class="current-menu-item"';
                }else{
                    $li = '';
                }
            }
            if (($menu->type_id == config('theme.type_menu.detail')) || ($menu->type_id == config('theme.type_menu.page'))) {
                $html = '.html';
            }else{
                $html = '';
            }
            echo '<li '.$li.'>';
            echo '<a href="'.url($slug.'/'.$menu->slug). $html . '"'.$a.'>'.$menu->title.'</a>';
            if (!empty($menu->children->first())){
                if ($level > 1){
                    $true_slug = $menu->slug;
                }else{
                    $true_slug = $slug.'/'.$menu->slug;
                }
                $this->showMainMenuMobile($menu->children, $url,$level+1, $true_slug);
            }
            echo '</li>';
        }
        echo '</ul>';
    }
    /*
     * Hàm save - update menu
     * */
    static public function saveUpdateMenu($nameModel,$model,$menu,$position,$func = true){
        if (empty($menu)){
            $menu = new Menu();
            if (!empty($model->parent_id)){
                $slug = $model->where('id', $model->parent_id)->first()->slug;
                $menu->parent_id = Menu::where('slug', $slug)->first()->id;
            }
            $func = true;
        }else if (!empty($model->parent_id)){
            $slug = $model->where('id', $model->parent_id)->first()->slug;
            $menu->parent_id = Menu::where('slug', $slug)->first()->id;
        }
        $menu->title = $model->title;
        $menu->slug = $model->slug;
        $menu->position = !empty($position) ? implode(',', $position) : null;
        $menu->type_id = config('settings.template.list');
        $menu->keywords = $nameModel;
        $menu->arrange = $model->id;
        if ($func){
            $menu->save();
        }else{
            $menu->update();
        }
    }
    /*Hàm get path Image*/
//    static public function getImageWordpress($id_featured_media){
//        $jsonImage = file_get_contents('https://dulichdaibang.com/wp-json/wp/v2/media/' . $id_featured_media);
//        $objImage = json_decode($jsonImage,true);
//        $path = explode('/', $objImage['media_details']['file'])[2];
//        return 'public/images/sites/' .\URL::getDefaultParameters()['subdomain'] . '/news/' . $path;
//    }
}
