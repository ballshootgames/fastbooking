<?php

namespace Modules\Theme\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Page extends Model
{
    use Sortable;

    protected $table = 'pages';

    protected $primaryKey = 'id';

    protected $fillable = ['title', 'slug', 'description', 'content', 'parent_id','meta_keyword'];

    protected $sortable = ['name'];

    public function showListPage($pages, $parent_id = '', $level = ''){
        foreach ($pages as $key => $item){
            if ($item['parent_id'] == $parent_id){
                echo '<tr>';
                echo '<td>'.$level.$item['title'].'</td>';
                echo '<td>'.$item['slug'].'</td>';
                echo '<td>'.$item['description'].'</td>';
                echo '<td>'.Carbon::parse($item->updated_at)->format(config('settings.format.datetime')).'</td>';
                echo '<td>';
                echo '<a style="padding-right: 4px" href="'.url('/admin/themes/pages/' . $item['id']).'" title="'.__('message.show').'"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> '.__('message.view').'</button></a>';
                echo '<a href="'.url('/admin/themes/pages/' . $item['id'] . '/edit').'" title="'.__('message.edit').'"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> '.__('message.edit').'</button></a>';
                echo '<form method="POST" action="'.url('/admin/themes/pages/'.$item['id']).'" style="display: inline">
                            <input type="hidden" name="_method" value="delete" />
                            <input class="_token" name="_token" type="hidden" value="'.csrf_token().'">
                            <button type="submit" class="com-delete btn btn-danger btn-xs" onclick="return confirm(&quot;Bạn chắc chắn muốn xoá phần từ này?&quot;)" title="Xóa"><i class="fa fa-trash-o" aria-hidden="true"></i> Xóa</button>
                          </form>';
                echo '</td>';
                echo '</tr>';
                unset($pages[$key]);
                $this->showListPage($pages, $item['id'], $level.'-- ');
            }
        }
    }
}
