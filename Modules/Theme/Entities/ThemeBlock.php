<?php

namespace Modules\Theme\Entities;

use Illuminate\Database\Eloquent\Model;

class ThemeBlock extends Model
{
    protected $table = 'theme_block';

    protected $primaryKey = 'id';

    protected $fillable = ['name', 'key', 'value', 'theme_id'];

    public function theme(){
        return $this->belongsTo('Modules\Theme\Entities\Theme', 'theme_id');
    }
}
