<?php

namespace Modules\Theme\Http\Controllers;

use App\Traits\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Kalnoy\Nestedset\NodeTrait;
use Modules\News\Entities\News;
use Modules\News\Entities\NewsType;
use Modules\Theme\Entities\Menu;
use Illuminate\Support\Str;
use Modules\Theme\Entities\Page;
use Session;

class MenuController extends Controller
{
    use Authorizable;
    use NodeTrait;
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');

        $menus = new Menu();
        if (!empty($keyword)){
            $menus = Menu::where('title','LIKE',"%$keyword%");
        }

        $typeMenu = Menu::getTemplate();
        $menus = $menus->get()->toTree();

        return view('theme::manage.menus.index', compact('menus', 'typeMenu'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $menuTree = Menu::get()->toTree();
        $listMenu = new Menu();
        $listMenu = $listMenu->getListMenuToArray($menuTree);
        $listMenu = \Arr::prepend(!empty($listMenu) ? $listMenu : [], __('message.please_select'), '');

        $menu_arrange = Menu::count('arrange');
        $position = config('theme.menu.position');
        $typeMenu = Menu::getTemplate();
        $keywords = Menu::getKeywords();
        $keywords = \Arr::prepend($keywords, __('message.please_select'), '');

        return view('theme::manage.menus.create', compact('menu_arrange', 'position', 'typeMenu', 'listMenu','keywords'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'slug' => 'nullable',
            'type_id' => 'required'
        ]);

        $requestData = $request->all();
        if (empty($request->get('slug'))){
            $requestData['slug'] = Str::slug($requestData['title']);
        }
        $requestData['slug'] = Menu::setTrueMenuSlug($requestData['slug']);
        $requestData['position'] = !empty($request->position) ? implode(',', $requestData['position']) : '';
        \DB::transaction(function () use ($request, $requestData){
            $menu = Menu::create($requestData);
            $keywords = $request->keywords;
            switch ($keywords){
                case 'pages':
                    $page = new Page();
                    $this->modelEditUpdate($page, $menu, true);
                break;
                case 'news_type':
                    $newsType = new NewsType();
                    $this->modelEditUpdate($newsType, $menu, true);
            }
        });

        Session::flash('flash_message', __('theme::menus.created_success'));
        return redirect('admin/themes/menus');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $menu = Menu::findOrFail($id);
        $typeMenu = Menu::getTemplate();
        return view('theme::manage.menus.show', compact('menu', 'typeMenu'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $menuTree = Menu::whereNotDescendantOf($id)->get()->toTree();
        $listMenu = new Menu();
        $listMenu = $listMenu->getListMenuToArray($menuTree, $id);
        $listMenu = \Arr::prepend($listMenu, __('message.please_select'), '');

        $menu = Menu::findOrFail($id);
        $position = config('theme.menu.position');
        $position_edit = explode(',',$menu->position);
        $typeMenu = Menu::getTemplate();
        $keywords = Menu::getKeywords();
        $keywords = \Arr::prepend($keywords, __('message.please_select'), '');

        return view('theme::manage.menus.edit', compact('menu', 'position', 'position_edit', 'typeMenu', 'listMenu','keywords'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'slug' => 'nullable'
        ]);
        $menu = Menu::findOrFail($id);

        $requestData = $request->all();
        if (empty($request->get('slug'))){
            $requestData['slug'] = \Str::slug($requestData['title']);
            $requestData['slug'] = Menu::setTrueMenuSlug($requestData['slug']);
        }

        \DB::transaction(function () use ($request, $requestData, $menu){
            $requestData['position'] = !empty($request->position) ? implode(',', $requestData['position']) : '';
            $menu->update($requestData);
            $keywords = $request->keywords;
            switch ($keywords){
                case 'pages':
                    $page = Page::where('slug', $menu->slug)->first();
                    $this->modelEditUpdate($page,$menu,false);
                    break;
                case 'news_type':
                    $newsType = NewsType::where('slug', $menu->slug)->first();
                    $this->modelEditUpdate($newsType, $menu, false);
            }
        });

        Session::flash('flash_message', __('theme::menus.updated_success'));
        return redirect('admin/themes/menus');
    }
//    public function updateModel($model,$menu){
//        $model->title = $menu->title;
//        $model->slug = $menu->slug;
//        $model->parent_id = null;
//        if (!empty($menu->parent_id)){
//            $slug = Menu::where('id',$menu->parent_id)->first()->slug;
//            $model->parent_id = NewsType::where('slug',$slug)->first()->id;
//        }
//        $model->update();
//    }

    public function modelEditUpdate($model,$menu,$func = true){
        $model->title = $menu->title;
        $model->slug = $menu->slug;
        $model->active = config('settings.active');
        $model->parent_id = null;
        if (!empty($menu->parent_id)){
            $slug = Menu::where('id',$menu->parent_id)->first()->slug;
            $model->parent_id = $model->where('slug',$slug)->first()->id;
        }
        if ($func){
            $model->save();
        }else{
            $model->update();
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        \DB::transaction(function () use ($id){
            $menu = Menu::findOrFail($id);
            switch ($menu->keywords){
                case 'pages':
                    Page::where('slug', $menu->slug)->delete();
                    break;
                case 'news_type':
                    NewsType::where('slug', $menu->slug)->delete();
                    break;
            }
            $menu->destroy($id);
        });

        Session::flash('flash_message', __('theme::menus.deleted_success'));
        return redirect('admin/themes/menus');
    }
}
