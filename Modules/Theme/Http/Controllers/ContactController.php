<?php

namespace Modules\Theme\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Theme\Entities\Contact;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = config('settings.perpage');

        $contacts = new Contact();

        if (!empty($keyword)){
            $contacts = $contacts->where('name','LIKE',"%$keyword%")
                ->orWhere('email','LIKE',"%$keyword%")
                ->orWhere('phone','LIKE',"%$keyword%");
        }

        $contacts = $contacts->paginate($perPage);
        return view('theme::manage.contacts.index', compact('contacts'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('theme::manage.contact.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $contact = Contact::findOrFail($id);
        return view('theme::manage.contacts.show', compact('contact'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('theme::manage.contact.edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        Contact::destroy($id);
        \Session::flash('flash_message', __('theme::contacts.deleted_success'));
        return redirect('contacts');
    }
}
