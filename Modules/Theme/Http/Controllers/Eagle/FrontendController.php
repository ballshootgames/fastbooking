<?php

namespace Modules\Theme\Http\Controllers\Eagle;

use App\Cart;
use App\Events\FileTourSchedule;
use App\Events\InstallmentEvent;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Booking\Entities\Customer;
use Modules\Booking\Entities\CustomerType;
use Modules\Installment\Entities\Installment;
use Modules\News\Entities\News;
use Modules\News\Entities\NewsType;
use Modules\Theme\Entities\Block\Banner;
use Modules\Theme\Entities\Block\Partner;
use Modules\Theme\Entities\Block\Slide;
use Modules\Theme\Entities\Contact;
use Modules\Theme\Entities\Gallery;
use Modules\Theme\Entities\Menu;
use Modules\Theme\Entities\Newsletter;
use Modules\Theme\Entities\Page;
use Modules\Theme\Entities\Theme;
use Modules\Tour\Entities\Comment;
use Modules\Tour\Entities\Tour;
use Modules\Tour\Entities\TourLinked;
use Modules\Tour\Entities\TourProperty;
use App\Setting;
use Modules\Tour\Entities\TourSchedule;
use function Sodium\compare;
use Str;

class FrontendController extends Controller
{
    public $themeName = null;
    public function __construct()
    {
        $this->middleware(function ($request, $next){
            //$carts = new Cart();
            $book = new Cart();

            $newNews = $this->getNewsNew();
            $mainMenus = $this->getMenus(config('theme.menus.main_menu'));
            $botMenus = $this->getMenus(config('theme.menus.bottom_menu'));
            $newsTypeDes = $this->getNewsDes();
            $typeTourLists = TourProperty::where(['type'=>'tour_list', 'active' => config('settings.active')])->orderBy('prop_order','asc')->get();

            //Lấy tên theme
            $this->themeName = Setting::getSettingThemeName();

	        //Thông tin footer
	        $logo = Setting::where('key','url_logo')->first();
	        $urlLogo = $logo != null ? $logo->value : "";


	        $settings = Setting::get();

	        $companyList = $this->findItemsByGroupData($settings,'group_data','company_info');
	        $companyInfo = $companyList->mapWithKeys(function ($item) {
		        return [$item['key'] => $item['value']];
	        });
	        unset($companyInfo['url_logo']);
	        unset($companyInfo['ministry_industry_trade_link']);
	        unset($companyInfo['website_name']);

	        $generalList = $this->findItemsByGroupData($settings,'group_data','general_info');
	        $generalInfo = $generalList->mapWithKeys(function ($item) {
		        return [$item['key'] => $item['value']];
	        });
            unset($generalInfo['keyword']);
//            dd($generalInfo);

	        $socialList = $this->findItemsByGroupData($settings,'group_data','social_info');
	        $socialInfo = $socialList->mapWithKeys(function ($item) {
		        return [$item['key'] => $item['value']];
	        });

	        $contactList = $this->findItemsByGroupData($settings,'group_data','contact_info');
	        $contactInfo = $contactList->mapWithKeys(function ($item) {
		        return [$item['key'] => $item['value']];
	        });

            $slides = Slide::orderBy('updated_at','desc')->take(5)->get();

            $tourDestinations = TourProperty::where(['type'=>'tour_place', 'active' => config('settings.active'), 'parent_id' => null])->orderBy('prop_order','asc')->get();

	        $settings = Setting::allConfigsKeyValue();
            \View::share([
                'books' => [
                    'tour' => $book->getBook(),
                    'totalAdult' => $book->getTotalAdults(),
                    'totalChild' => $book->getTotalChild(),
                    'totalBaby' => $book->getTotalBaby(),
                    'totalPrice' => $book->getBookingTotalPrice(),
                    'totalPayment' => $book->getTotalPayment()
                ],
                'mainMenus' => $mainMenus,
                'botMenus' => $botMenus,
                'newNews' => $newNews,
                'newsTypeDes' => $newsTypeDes,
                'typeTourLists' => $typeTourLists,
                'settings' => $settings,
                'urlLogo'=>$urlLogo,
                'companyInfo'=>$companyInfo,
                'generalInfo'=>$generalInfo,
                'socialInfo'=>$socialInfo,
                'contactInfo'=>$contactInfo,
                'themeName' => $this->themeName,
                'slides' => $slides,
                'tourDestinations' => $tourDestinations
            ]);

            return $next($request);
        });
    }

    function findItemsByGroupData($array,$key, $value) { 
        $temp_array = [];
        $settingConfig = config('theme.settings_site');
        foreach($array as $item) {
           foreach ($settingConfig as $config){
               if ($config[$key] === $value){
                   if ($item->key === $config['key']){
                       $temp_array[] = $item;
                   }
               }
           }
        }
        return collect($temp_array);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        switch ($this->themeName){
            case "tomap":
            case "dulichdaibang":
                $slugHome = [];
                $showHomes = TourProperty::where('show_home', '<>', null)->orderBy('show_home', 'ASC')->get();
                $tours = [];
                foreach ($showHomes as $tourProperty){
                    $tours[] = [$this->getTourProperties($tourProperty->type,$tourProperty->id),$tourProperty->name,$tourProperty->slug];
                }
                $tourPriceShocks = $tours[0][0]; // Danh sách tour giá sốc
                $tourInlandTravels = $tours[1][0]; // Danh sách tour nội bộ
                $tourAbroadTravels = $tours[2][0]; // Danh sách tour nước ngoài
                $tourNewHot = isset($tours[3][0]) ? $tours[3][0] : null; // Danh sách tour hot
                $tourNew = Tour::with('properties')->whereHas('properties', function ($query){
                    $query->where('type','tour_category')
                        ->where('id','<>', 138);
                })->where([['active','=',config('settings.active')], ['is_promotion', '<>', config('settings.active')]])->orderBy('updated_at','desc')->take(8)->get();
                $tourPromotion = Tour::with('properties')->whereHas('properties', function ($query){
                    $query->where('type','tour_category')
                        ->where('id','<>', 138);
                })->where([['is_promotion','=', config('settings.active')],['active', '=', config('settings.active')]])->orderBy('updated_at','desc')->take(8)->get();
                //Hiển thị tiêu đề của các danh mục đã chọn
                $titleTourProperty = [$tours[0][1],$tours[1][1],$tours[2][1],isset($tours[3][1]) ? $tours[3][1] : null];
                $slugTourProperty = [$tours[0][2],$tours[1][2],$tours[2][2],isset($tours[3][2]) ? $tours[3][2] : null];
                $perPageNews = 4;
                $perPageBanner = 6;
                break;
            case "eagle":
                $tourNew = $tourPromotion =  '';
                $slugHome = ['top-tours', 'offer-tours', 'favorite-tours', 'awesome-tours'];
                $tourPriceShocks = $this->getTourPropertiesBySlug('tour_list',$slugHome[0],1);
                $tourInlandTravels = $this->getTourPropertiesBySlug('tour_list',$slugHome[1], 1);
                $tourAbroadTravels = $this->getTourPropertiesBySlug('tour_list',$slugHome[2],null,4);
                $tourNewHot = $this->getTourPropertiesBySlug('tour_list',$slugHome[3],null,3);
                $titleTourProperty = $slugTourProperty = null;
                $perPageNews = $perPageBanner = 3;
                break;
        }
        $banners = Banner::orderBy('updated_at','desc')->take($perPageBanner)->get();
        $tourPlaces = TourProperty::where(['type'=>'tour_place', 'active' => config('settings.active'), 'prop_top' => config('settings.active')])->orderBy('prop_order','asc')->take($this->paginate($this->themeName)['pageDestination'])->get();
        $idNewsType = NewsType::where('parent_id',config('theme.id_news_type_parent.id_news_tour'))->pluck('id')->toArray();
        $newsListHome = News::with('type')->whereIn('news_type_id',$idNewsType)->where('active', config('settings.active'))->orderBy('updated_at','desc')->take($perPageNews)->get();
        $comments = Comment::orderBy('updated_at','desc')->take(5)->get();
        $customers = Gallery::where('active', config('settings.active'))->orderBy('arrange', 'asc')->take(12)->get();
        $partners = $this->getListPartners(config('theme.ptype_id_web.partner'));
        $why_us = $this->getListPartners(config('theme.ptype_id_web.why_us'),6);

        return view("theme::$this->themeName.frontend.home.content",
            compact('newsListHome','banners','customers','partners','why_us',
                'tourPriceShocks','tourInlandTravels','tourAbroadTravels','tourNewHot','tourPlaces','comments','slugHome',
                'titleTourProperty', 'slugTourProperty', 'tourNew', 'tourPromotion'));
    }

    public function getTourProperties($type,$idTourProperties = null,$hot = null){
        $tourProperties = new Tour();
        $tourProperties = $tourProperties->whereHas('properties', function ($query) use ($type,$idTourProperties){
            $query->where('type',$type)
                ->where('id', $idTourProperties);
        });
        if ($hot == 1){
            $tourProperties = $tourProperties->where('hot',$hot);
        }

        $tourProperties = $tourProperties->where('active',config('settings.active'))->orderBy('updated_at','desc')->take(8)->get();
        return $tourProperties;
    }

    public function getTourPropertiesBySlug($type,$slugTourProperties = null,$hot = null, $page = 8){
        $tourProperties = new Tour();
        $tourProperties = $tourProperties->whereHas('properties', function ($query) use ($type,$slugTourProperties){
            $query->where('type',$type)
                ->where('slug', $slugTourProperties);
        });
        if ($hot == 1){
            $tourProperties = $tourProperties->where('hot',$hot);
        }

        $tourProperties = $tourProperties->where('active',config('settings.active'))->orderBy('updated_at','desc')->take($page)->get();
        return $tourProperties;
    }

    public function getListPartners($ptypeId,$take = null){
        $partners = new Partner();
        $partners = $partners->where('ptype_id',$ptypeId);
        if (!empty($take)){
            $partners = $partners->take($take);
        }
        $partners = $partners->where('active', config('settings.active'))->orderBy('arrange','asc')->get();
        return $partners;
    }

    public function getNewsDes(){
        $newsTypeParent = NewsType::where('id',config('theme.id_news_type_parent.id_news_destination'))->first();
        $newsTypes = [];
        if ($newsTypeParent){
            $newsTypes = NewsType::where('parent_id', $newsTypeParent->id)->orderBy('updated_at')->get();
        }
        return compact('newsTypes','newsTypeParent');
    }

    public function getMenus($position){
        $menus = Menu::where('position','LIKE',"%$position%")->orderBy('arrange','ASC')->get()->toTree();
        return $menus;
    }

    public function getNewsRandom($news,$idNewsTypeParent,$key){
        $idNewsType = NewsType::where('parent_id',$idNewsTypeParent)->pluck('id')->toArray();
        $idNewsType = count($idNewsType) > 0 ? $idNewsType : [$idNewsTypeParent];
        $newsRandoms = News::with('type')->where([['slug','<>',$news->slug],['active', '=', config('settings.active')]])->whereIn('news_type_id',$idNewsType);
        if ($key === config('theme.show_sidebar_right')){
            $newsRandoms = $newsRandoms->inRandomOrder()->take($this->paginate($this->themeName)['pageRandom']);
        }else if ($key === config('theme.show_related')){
            $newsRandoms = $newsRandoms->orderBy('updated_at','desc')->take($this->paginate($this->themeName)['pageRelate']);
        }
        $newsRandoms = $newsRandoms->get();
        return $newsRandoms;
    }

    public function getNewsNew(){
        $newNews = News::with('type')->where('active', config('settings.active'))->orderBy('updated_at', 'DESC')->take(7)->get();
        return $newNews;
    }

    public function getNewsLists($slug,$perPage = 10){
        $idNewsType = NewsType::where('slug',$slug)->first();
        $news = News::with('type')->where(['news_type_id' => $idNewsType->id, 'active'=>config('settings.active')])->paginate($perPage);
        return $news;
    }

    public function getNewsDetail($slug){
        $news = News::with('type')->where(['slug'=>$slug,'active'=>config('settings.active')])->first();
        return $news;
    }

    public function getTourLists($requestData){
        $tours = new Tour();
        if (!empty($requestData['name'])){
            $name = $requestData['name'];
            $tours = $tours->where('name', 'LIKE', "%$name%");
        }
        if (!empty($requestData['tour_category'])){
            $tours = $tours->with('properties')->whereHas('properties', function ($query) use ($requestData){
                $query->where('type', 'tour_category')
                        ->where('id', $requestData['tour_category']);
            });
        }
        if (!empty($requestData['tour_list'])){
            $tours = $tours->with('properties')->whereHas('properties', function ($query) use ($requestData){
                $query->where('type', 'tour_list')
                    ->where('id', $requestData['tour_list']);
            });
        }
        if (!empty($requestData['tour_cities'])){
            $tours = $tours->with('schedules')->whereHas('schedules', function ($query) use ($requestData){
                $idCity = $requestData['tour_cities'];
                $query->with('cities')->whereHas('cities', function ($query) use ($idCity){
                    $query->where('city_id',$idCity);
                });
            });
        }elseif (!empty($requestData['tour_place'])){
            $tours = $tours->with('properties')->whereHas('properties', function ($query) use ($requestData){
                $query->where('type', 'tour_place')
                    ->where('id', $requestData['tour_place']);
            });
        }
        if (!empty($requestData['tour_price'])){
            $tours = $tours->where(function ($query) use ($requestData){
                switch ($requestData['tour_price']) {
                    case 1:
                        $query->where('price', '<=', 500000);
                        break;
                    case 2:
                        $query->where('price', '<=', 1000000);
                        break;
                    case 3:
                        $query->where('price', '>=', 1000000)->where('price', '<=', 2000000);
                        break;
                    case 4:
                        $query->where('price', '>=', 2000000)->where('price', '<=', 5000000);
                        break;
                    case 5:
                        $query->where('price', '>=', 5000000)->where('price', '<=', 10000000);
                        break;
                    case 6:
                        $query->where('price', '>=', 10000000)->where('price', '<=', 20000000);
                        break;
                    case 7:
                        $query->where('price', '>=', 20000000)->where('price', '<=', 50000000);
                        break;
                    case 8:
                        $query->where('price', '>=', 50000000);
                        break;
                }
            });
        }
        $tours = $tours->where('active', config('settings.active'));
        if (!empty($requestData['tour_order'])){
            if (($requestData['tour_order'] !== 'ascPrice') && ($requestData['tour_order'] !== 'descPrice')){
                $tours = $tours->orderBy('name',$requestData['tour_order']);
            }else{
                $order = $requestData['tour_order'] === 'descPrice' ? 'desc' : 'asc';
                $tours = $tours->orderBy('price',$order);
            }
        }else{
            $tours = $tours->orderBy('updated_at', 'desc');
        }
        $tours = $tours->paginate($this->paginate($this->themeName)['pageListTour']);
        return $tours;
    }

    public function getTourDetail($slug){
        $tour = Tour::with(['properties', 'schedules'])->where([['slug', '=', $slug],['active', '=', config('settings.active')]])->first();
        return $tour;
    }

    public function getTourRelated($tour, $tourType, $type = 1, $take = 4){
        $tourRelated = new Tour();
        if ($type === 2){
            $places = $tour->properties()->where('type', 'tour_place')->get();
            $arrTourPlace = [];
            foreach ($places as $tourProperty){
                $arrTourPlace[] = $tourProperty->id;
            }
            $tourRelated = $tourRelated->whereHas('properties', function ($query) use ($arrTourPlace, $tour, $tourType){
                if ($tour->properties()->where('type', $tourType)->get()->count() > 0){
                    $query->where('type', $tourType)
                        ->whereIn('id', $arrTourPlace);
                }else{
                    $query->where('type', $tourType);
                }
            });
        }else {
            //Cùng loại tour
            $tourRelated = $tourRelated->whereHas('properties', function ($query) use ($tour, $tourType){
                if ($tour->properties()->where('type', $tourType)->get()->count() > 0){
                    $query->where('type', $tourType)
                            ->where('id', $tour->properties()->where('type', $tourType)->first()->id);
                }else{
                    $query->where('type', $tourType);
                }
            });
        }

        /*$tourRelated = $tourRelated->whereNull('limit_end_date')->orWhere('limit_end_date', '>=', date('Y-m-d'));
        $tourRelated = $tourRelated->whereNull('departure_date')->orWhere('departure_date', '>=', date('Y-m-d'));*/
        $tourRelated = $tourRelated->where([['slug', '<>', $tour->slug],['active', '=', config('settings.active')]])->orderBy('updated_at', 'desc')->take($take)->get();
        return $tourRelated;
    }

    public function error404(){
        return view("theme::$this->themeName.frontend.404");
    }
    public function getListsParents($slugParent, Request $request){
        $newsTypeParent = NewsType::where('slug',$slugParent)->first();
        $slugNewsTypeParent = !empty($newsTypeParent) ? $newsTypeParent->slug : '';
        $tourProperties = TourProperty::where(['slug' => $slugParent, 'active' => config('settings.active')])->first();
        $slugTourProperties = $tourProperties ? $tourProperties->slug : '';
        switch ($slugParent){
            case $slugNewsTypeParent:
                $newsTypesChildren = NewsType::where('parent_id', $newsTypeParent->id)->first();
                if(!empty($newsTypesChildren)){
                    $newsTypes = NewsType::where('parent_id',$newsTypeParent->id)->take(3)->get();
                    return view("theme::$this->themeName.frontend.news.parents", compact('slugParent', 'newsTypeParent','newsTypes'));
                }else{
                    $newsLists = $this->getNewsLists($slugNewsTypeParent,$this->paginate($this->themeName)['pageListNews']);
                    return view("theme::$this->themeName.frontend.news.lists", compact('slugParent', 'newsTypeParent','newsLists'));
                }
            case 'tour':
                $tours = $this->getTourLists($request->all());
                return view("theme::$this->themeName.frontend.tours.lists", compact('slugParent', 'tours'));
            case $slugTourProperties:
                $slugChild = $tourProperties->slug;
                $tours = Tour::with('properties');
                if ($tourProperties->count()){
                    $tours = $tours->whereHas('properties', function ($query) use ($tourProperties){
                        $query->where('id', $tourProperties->id);
                    });
                }
                $tours = $tours->where('active', config('settings.active'))->orderBy('updated_at','DESC')->paginate($this->paginate($this->themeName)['pageListTour']);
                return view("theme::$this->themeName.frontend.tours.lists", compact('slugParent', 'tours', 'slugChild', 'tourProperties'));
            /*case 'gio-hang':
                return view('theme::$this->themeName.frontend.carts.cart');*/
            case 'booking-tour':
                return view("theme::$this->themeName.frontend.bookings.booking-tour");
            /*case 'thanh-toan':
                return view('theme::$this->themeName.frontend.carts.checkout');*/
            case 'thanh-toan':
                return view("theme::$this->themeName.frontend.bookings.booking-checkout");
            case Str::slug(trans('theme::eagles.eagle_customer')):
                $galleries = Gallery::where('active', config('settings.active'))->orderBy('arrange','ASC')->paginate(12);
                return view("theme::$this->themeName.frontend.galleries.list", compact('slugParent','galleries'));
            default:
                return view("theme::$this->themeName.frontend.404", compact('slugParent'));
        }
    }

    public function getLists($slugParent,$slugChild,Request $request){
        $idNewsType = NewsType::where('slug',$slugParent)->first();
        $slugNewsType = [];
        if ($idNewsType){
            $slugNewsType = NewsType::where('parent_id',$idNewsType->id)->pluck('slug')->toArray();
        }
        $tourListProperties = TourProperty::pluck('slug')->toArray();
        switch ($slugChild){
            case in_array($slugChild,$slugNewsType):
                $parents = NewsType::where('id', $idNewsType->parent_id)->first();
                $slugParents = !empty($parents) ? $parents->slug : '';
                $newsType = NewsType::where('slug',$slugChild)->first();
                $newsLists = $this->getNewsLists($slugChild,$this->paginate($this->themeName)['pageListNews']);
                if ($slugParent === config('theme.slug.slug_dia_diem')){
                    $view = "theme::$this->themeName.frontend.news.list-destinations";
                }else{
                    $view = "theme::$this->themeName.frontend.news.list-children";
                }
                return view($view, compact('slugParents','slugParent','slugChild','newsLists','idNewsType','newsType'));
            case in_array($slugChild,$tourListProperties);
                $tourProperties = TourProperty::where('slug',$slugChild)->first();
                $tours = Tour::with('properties');
                if ($tourProperties->count()){
                    $tours = $tours->whereHas('properties', function ($query) use ($tourProperties){
                        $query->where('id', $tourProperties->id);
                    });
                }
//                $tours = $tours->whereNull('limit_end_date')->orWhere('limit_end_date', '>=', date('Y-m-d'));
//                $tours = $tours->whereNull('departure_date')->orWhere('departure_date', '>=', date('Y-m-d'));
//                $perPage = $this->paginate($this->themeName);
                $tours = $tours->where('active', config('settings.active'))->orderBy('updated_at','DESC')->paginate($this->paginate($this->themeName)['pageListTour']);
                return view("theme::$this->themeName.frontend.tours.tour-properties", compact('slugChild', 'slugParent','tours','tourProperties'));
            default:
                return view("theme::$this->themeName.frontend.404", compact('slugChild'));
        }
    }

    public function getDetail($slugParent,$slugDetail){
        $slugNewsType = NewsType::where('slug',$slugParent)->pluck('slug')->toArray();
        $slugTourProperty = TourProperty::pluck('slug')->toArray();
        switch ($slugParent){
            case in_array($slugParent,$slugNewsType):
                $news = $this->getNewsDetail($slugDetail);
                $newsType = NewsType::where('slug', $slugParent)->first();
                $idNewsTypeParent = !empty($newsType->parent_id) ? optional($news->type)->parent_id : $newsType->id;
                $newsTypeParent = NewsType::where('id',$idNewsTypeParent)->first();
                $newsRandoms = $this->getNewsRandom($news,$idNewsTypeParent,config('theme.show_sidebar_right'));
                $newsRelates = $this->getNewsRandom($news,$idNewsTypeParent,config('theme.show_related'));
                return view("theme::$this->themeName.frontend.news.detail", compact('slugParent','slugDetail','news','newsTypeParent','newsRandoms','newsRelates','newsType'));
            case in_array($slugParent,$slugTourProperty):
            case 'tour':
                $tour = $this->getTourDetail($slugDetail);
                if ($tour){
                    $tourProperty = TourProperty::where('slug', $slugParent)->first();
                    $tourLink = TourLinked::where('tour_id', $tour->id)->first();
                    if ($tourProperty){
                        switch ($tourProperty->type){
                            case 'tour_list':
                            case 'tour_place':
                            case 'tour_category':
                                $slug = $tourProperty->type !== 'tour_place' ? $tourProperty->type === 'tour_category' ? 'tour' :'dong-tour' : Str::slug(trans('theme::eagles.tour_destination'));
                                $breadcrumb = [
                                    'slug' => $slug . '/' . $slugParent,
                                    'name' => $tourProperty->name
                                ];
                                break;
                        }
                        $tourRelatedBot = $this->getTourRelated($tour,$tourProperty->type, 1, 5);
                    }else{
                        $breadcrumb = [
                            'slug' => 'tour',
                            'name' => 'tour'
                        ];
                        $tourRelatedBot = $this->getTourRelated($tour,'tour_category', 1, 5);
                    }
                    $countComment = Comment::where('tour_id',$tour->id)->count();
                    $tourRelatedSide = $this->getTourRelated($tour,'tour_place',2, 3);
                    return view("theme::$this->themeName.frontend.tours.detail", compact('tourLink','breadcrumb','countComment','slugParent','slugDetail', 'tour', 'tourRelatedSide', 'tourRelatedBot'));
                }
                return view("theme::$this->themeName.frontend.404", compact('slugParent','slugDetail'));
            case 'gioi-thieu':
                $page = Page::where('slug',$slugDetail)->first();
                $slugPage = $slugDetail;
                return view("theme::$this->themeName.frontend.pages.page", compact('slugPage','page'));
            case Str::slug(trans('theme::eagles.eagle_customer')):
                $galleries = Gallery::where(['slug' => $slugDetail, 'active' => config('settings.active')])->first();
                $otherGalleries = Gallery::where([['slug','<>',$slugDetail], ['active','=',config('settings.active')]])->orderBy('arrange','desc')->take(4)->get();
                return view("theme::$this->themeName.frontend.galleries.detail", compact('slugParent','slugDetail','galleries','otherGalleries'));
            default:
                return view("theme::$this->themeName.frontend.404", compact('slugParent','slugDetail'));
        }
    }

    public function getPage($slugPage, Request $request){
        $page = Page::where('slug',$slugPage)->first();
        if ($slugPage === 'tim-kiem'){
            $keyword = $request->get('title');
            $des = $request->get('destination');
            $newsLists = new News();
            if (!empty($keyword)){
                $newsLists = $newsLists->where('title','LIKE',"%$keyword%");
            }
            if (!empty($des)){
                $newsLists = $newsLists->where('news_type_id',$des);
            }
            $newsLists = $newsLists->paginate(10);
            return view("theme::$this->themeName.frontend.pages.search",  compact('newsLists','page'));
        }else{
            return view("theme::$this->themeName.frontend.pages.page", compact('slugPage','page'));
        }
    }

    public function postContact(Request $request){
        $validator = \Validator::make($request->all(), [
            'fullname' => 'required',
            'email' => 'required|email',
            'message' => 'required'
        ]);
        if ($validator->passes()){
            $contact = new Contact();
            $contact->name = $request->fullname;
            $contact->email = $request->email;
            $contact->address = !empty($request->address) ? $request->address : '';
            $contact->phone = !empty($request->phone) ? $request->phone : '';
            $contact->content = $request->message;
            $contact->save();
            return response()->json([
                'success' => 'ok'
            ]);
        }
        return response()->json(['errors'=>$validator->errors()->all()]);
    }

    public function postNewsletter(Request $request){
        $validator = \Validator::make($request->all(), [
            'newsletter_email' => 'unique:newsletters,email'
        ]);
        if ($validator->passes()){
            $newsletter = new Newsletter();
            $newsletter->email = $request->newsletter_email;
            $newsletter->save();
            return response()->json([
                'success' => 'ok'
            ]);
        }
        return response()->json(['errors'=>$validator->errors()->all()]);
    }

    public function paginate($themeName){
        switch ($themeName){
            case "tomap":
                $pageDestination = 5;
                $pageListTour = 8;
                $pageListNews = 12;
                $pageRandom = 6;
                $pageRelate = 8;
                return compact('pageDestination','pageListTour', 'pageListNews', 'pageRandom', 'pageRelate');
            case "eagle":
            case "dulichdaibang";
                $pageDestination = 12;
                $pageListTour = 12;
                $pageListNews = 10;
                $pageRandom = 8;
                $pageRelate = 6;
                return compact('pageDestination','pageListTour','pageListNews', 'pageRandom', 'pageRelate');
                break;
        }
    }
    /*
     * Hàm gửi email để nhận file chương trình tour
     * @param: @request
     * */
    public function postSendFileTourSchedule(Request $request){
        $validator = \Validator::make($request->all(), [
            'email' => 'required|email',
        ]);
        if ($validator->passes()){
            $settings = \App\Setting::allConfigsKeyValue();
            $tour = Tour::findOrFail($request->tour_id);
            $contact = new Contact();
            $contact->name = explode("@",$request->email)[0];
            $contact->email = $request->email;
            $contact->content = 'Nhận file tải chương trình tour ' . $tour->name;
            $contact->save();
            event(new FileTourSchedule($tour,$settings,$contact));
            return response()->json([
                'success' => 'ok'
            ]);
        }
        return response()->json(['errors'=>$validator->errors()->all()]);
    }
    /*
     * Hàm gửi thông tin mua trả góp
     * @param: @request
     * */
    public function postSendInfoInstallment(Request $request){
        $requestData = $request->all();
        $validator = \Validator::make($requestData, [
            'customer.name' => 'required',
            'customer.email' => 'nullable|email',
            'customer.phone' => 'required|numeric|digits_between:7,13',
        ]);
        if ($validator->passes()){
            $customer = new Customer();
            $customer = $customer->where('phone', trim($requestData['customer']['phone']))->orWhere('email', trim($requestData['customer']['email']))->first();
            $settings = \App\Setting::allConfigsKeyValue();
            if ($customer){
                if (empty($requestData['customer']['email'])) $requestData['customer']['email'] = $customer->email;
                if (empty($requestData['customer']['address'])) $requestData['customer']['address'] = $customer->address;
                $customer->update($requestData['customer']);
            }else{
                $customer = Customer::create($requestData['customer']);
                $customerType = CustomerType::find(3); // tao moi la khach trả góp
                $customer->customerType()->associate($customerType);
                $customer->save();
            }
            $installment = [
                'customer_id' => $customer->id,
                'tour_id' => $requestData['tour_id'],
                'object_installment' => $requestData['object_installment'],
                'time_installment' => $requestData['time_installment'],
                'note' => $requestData['note'],
            ];
            $installment = Installment::create($installment);
            event(new InstallmentEvent($installment,$settings));
            if ($installment){
                return response()->json([
                    'success' => 'ok'
                ]);
            }else{
                return response()->json(['errors'=>'Có vấn đề xảy ra. Vui lòng kiểm tra lại']);
            }
        }
        return response()->json(['errors'=>$validator->errors()]);
    }
}
