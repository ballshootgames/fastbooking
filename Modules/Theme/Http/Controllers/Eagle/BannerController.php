<?php

namespace Modules\Theme\Http\Controllers\Eagle;

use App\Traits\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Modules\Theme\Entities\Block\Banner;

class BannerController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = config('settings.perpage');
        $banners = new Banner();

        if (!empty($keyword)){
            $banners = $banners->where('name','LIKE',"%$keyword%");
        }

        $banners = $banners->orderBy('updated_at', 'desc')->paginate($perPage);
        return view('theme::eagle.backend.banners.index', compact('banners'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('theme::eagle.backend.banners.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:'.(5*1024),
        ]);

        \DB::transaction(function () use ($request){
            $requestData = $request->all();
            if ($request->hasFile('image')){
                $requestData['image'] = Banner::saveImageResize($request->file('image'));
            }
            Banner::create($requestData);
        });

        \Session::flash('flash_message', __('theme::banners.created_success'));
        return redirect('admin/themes/banners');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $banner = Banner::findOrFail($id);
        return view('theme::eagle.backend.banners.show', compact('banner'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $banner = Banner::findOrFail($id);
        return view('theme::eagle.backend.banners.edit', compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:'.(5*1024),
        ]);
        $banner = Banner::findOrFail($id);
        \DB::transaction(function () use ($request,$banner){
            $requestData = $request->all();
            if ($request->hasFile('image')){
                Banner::deleteFile($banner->image);
                $requestData['image'] = Banner::saveImageResize($request->file('image'));
            }
            $banner->update($requestData);
        });

        \Session::flash('flash_message', __('theme::banners.updated_success'));
        return redirect('admin/themes/banners');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $banner = Banner::findOrFail($id);
	    Banner::deleteFile($banner->image);
	    $banner->delete();

        \Session::flash('flash_message', __('theme::banners.deleted_success'));
        return redirect('admin/themes/banners');
    }
}
