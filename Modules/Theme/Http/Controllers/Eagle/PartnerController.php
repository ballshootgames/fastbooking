<?php

namespace Modules\Theme\Http\Controllers\Eagle;

use App\Traits\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Modules\Theme\Entities\Block\Partner;

class PartnerController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = config('settings.perpage');
        $partners = new Partner();

        if (!empty($keyword)){
            $partners = $partners->where('name','LIKE',"%$keyword%");
        }

        $partners = $partners->where('ptype_id', $request->get('type'))->orderBy('arrange', 'asc')->paginate($perPage);
        return view('theme::eagle.backend.partners.index', compact('partners'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Request $request)
    {
        $arrange = Partner::where('ptype_id', $request->get('type'))->max('arrange');
        return view('theme::eagle.backend.partners.create', compact('arrange'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:'.(5*1024),
        ]);
        $typeId = $request->get('ptype_id');
        \DB::transaction(function () use ($request, $typeId){
            $requestData = $request->all();
            if ($request->hasFile('image')){
                $requestData['image'] = Partner::saveImageResize($request->file('image'));
            }
            Partner::create($requestData);
        });
        \Session::flash('flash_message', __('theme::partners.'.config('theme.text')[$typeId].'.created_success'));
        return redirect('admin/themes/partners?type=' . $typeId);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $partner = Partner::findOrFail($id);
        return view('theme::eagle.backend.partners.show', compact('partner'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $partner = Partner::findOrFail($id);
        return view('theme::eagle.backend.partners.edit', compact('partner'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'name' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:'.(5*1024),
        ]);
        $typeId = $request->get('ptype_id');
        $partner = Partner::findOrFail($id);
        \DB::transaction(function () use ($request, $partner){
            $requestData = $request->all();
            if (empty($request->get('active'))){
                $requestData['active'] = 0;
            }
            if ($request->hasFile('image')){
            	Partner::deleteFile($partner->image);
                $requestData['image'] = Partner::saveImageResize($request->file('image'));
            }
            $partner->update($requestData);
        });
        \Session::flash('flash_message', __('theme::partners.'.config('theme.text')[$typeId].'.updated_success'));
        return redirect('admin/themes/partners?type=' . $typeId);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Request $request, $id)
    {
        $partner = Partner::findOrFail($id);
        $typeId = $request->get('type');
	    Partner::deleteFile($partner->image);
	    $partner->delete();
        \Session::flash('flash_message', __('theme::partners.'.config('theme.text')[$typeId].'.deleted_success'));
        return redirect('admin/themes/partners?type=' . $typeId);
    }
}
