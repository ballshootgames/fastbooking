<?php

namespace Modules\Theme\Http\Controllers\Eagle;

use App\City;
use App\Country;
use App\District;
use App\PaymentMethod;
use App\Role;
use App\Setting;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Modules\Theme\Entities\Contact;
use Modules\Tour\Entities\Comment;
use Modules\Tour\Entities\TourProperty;

class AjaxFrontendController extends Controller
{
    /**
     * Gọi ajax: sẽ gọi đến hàm = tên $action
     * @param Request $action
     * @param Request $request
     * @return mixed
     */
    public function index($action, Request $request)
    {
        return $this->{$action}($request);
    }

    public function getComments(Request $request)
    {
        $comments = Comment::with('tour')->where('tour_id', $request->tourId)->latest()->paginate(5);
        return \Response::json($comments);
    }

    /*List Countries*/
    public function getListCountries(Request $request){
        $country_id = $request->country_id;
        $cities = City::where('country_id', $country_id)->pluck('name', 'id');
        return \response()->json($cities);
    }
    /*AutoComplete City*/
    public function getAutoCompleteCity(Request $request){
        return \response()->json(City::where('name','LIKE',"%$request->city%")->orderBy('updated_at','desc')->get());
    }
    /*public function getModal(Request $request){
        $frm = $request->frm;
        return view('theme::eagle.frontend.layouts.modal', compact('frm'));
    }*/

    public function loginUser(Request $request){
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required'
        ]);
        $requestData = $request->all();
        $username = \Str::contains($request->get('username'), '@') ? 'email' : 'username';
        if (\Auth::attempt([$username => $requestData['username'], 'password' => $requestData['password'], 'active' => config('settings.active')])){
            return response()->json(['success' => 'ok']);
        }
        return response()->json(['error' => 'Tên '.$request->get('username').' hoặc mật khẩu của bạn không đúng']);
    }

    public function registerUser(Request $request){
        $this->validate($request, [
            'name' => 'required|max:255',
            'username' => 'sometimes|required|max:255|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed'
        ]);
        $requestData = $request->all();
        //$role_user = \Role::where('name','user')->first();

        $fields = [
            'name' => $requestData['name'],
            'email' => $requestData['email'],
            'password' => bcrypt($requestData['password'])
        ];

        if (config('auth.providers.users.field','email') === 'username' && isset($requestData['username'])){
            $fields['username'] = $requestData['username'];
        }

        $user = User::create($fields);
        event(new Registered($user));

        /*if ($user){
            $user->roles()->attach($role_user);
        }*/

        \Auth::guard()->login($user);
        return response()->json('ok');
    }

    //Delete Property Items
    public function delPaymentMethodItem(Request $request){
        $ids = $request->ids;
        $arrId = explode(',', $ids,-1);
        foreach ($arrId as $item){
            $paymentMethod = PaymentMethod::findOrFail($item);

            $paymentMethod::destroy($item);
        }
        return \response()->json(['success' => 'ok']);
    }
    //Active Property Items
    public function activePaymentMethodItem(Request $request){
        $ids = $request->ids;
        $arrId = explode(',', $ids,-1);
        foreach ($arrId as $item){
            $paymentMethod = PaymentMethod::findOrFail($item);
            $active = $paymentMethod->active == config('settings.active') ? config('settings.inactive') : config('settings.active');
            \DB::table('payment_methods')->where('id', $paymentMethod->id)->update(['active' => $active, 'updator_id' => \Auth::user()->id]);
        }
        return \response()->json(['success' => 'ok']);
    }
    //Delete Property Items
    public function delCountryItem(Request $request){
        $ids = $request->ids;
        $arrId = explode(',', $ids,-1);
        foreach ($arrId as $item){
            $country = Country::findOrFail($item);

            $country::destroy($item);
        }
        return \response()->json(['success' => 'ok']);
    }
    //Active Property Items
    public function activeCountryItem(Request $request){
        $ids = $request->ids;
        $arrId = explode(',', $ids,-1);
        foreach ($arrId as $item){
            $country = Country::findOrFail($item);
            $active = $country->active == config('settings.active') ? config('settings.inactive') : config('settings.active');
            \DB::table('countries')->where('id', $country->id)->update(['active' => $active, 'updator_id' => \Auth::user()->id]);
        }
        return \response()->json(['success' => 'ok']);
    }
    //Delete Property Items
    public function delCityItem(Request $request){
        $ids = $request->ids;
        $arrId = explode(',', $ids,-1);
        foreach ($arrId as $item){
            $city = City::findOrFail($item);

            $city::destroy($item);
        }
        return \response()->json(['success' => 'ok']);
    }
    //Active Property Items
    public function activeCityItem(Request $request){
        $ids = $request->ids;
        $arrId = explode(',', $ids,-1);
        foreach ($arrId as $item){
            $city = City::findOrFail($item);
            $active = $city->active == config('settings.active') ? config('settings.inactive') : config('settings.active');
            \DB::table('cities')->where('id', $city->id)->update(['active' => $active, 'updator_id' => \Auth::user()->id]);
        }
        return \response()->json(['success' => 'ok']);
    }

    //Delete Property Items
    public function delDistrictItem(Request $request){
        $ids = $request->ids;
        $arrId = explode(',', $ids,-1);
        foreach ($arrId as $item){
            $district = District::findOrFail($item);

            $district::destroy($item);
        }
        return \response()->json(['success' => 'ok']);
    }
    //Active Property Items
    public function activeDistrictItem(Request $request){
        $ids = $request->ids;
        $arrId = explode(',', $ids,-1);
        foreach ($arrId as $item){
            $district = District::findOrFail($item);
            $active = $district->active == config('settings.active') ? config('settings.inactive') : config('settings.active');
            \DB::table('districts')->where('id', $district->id)->update(['active' => $active, 'updator_id' => \Auth::user()->id]);
        }
        return \response()->json(['success' => 'ok']);
    }
    /*
     * Hàm lấy danh sách điểm đến
     * @param: $type
     * */
    public function getTourPlace(Request $request){
        $tourPlace = new TourProperty();
        if (!empty($typeId = $request->get('type_id'))){
            $tourPlace = $tourPlace->where('type_id',$typeId);
        }
        $tourPlace = $tourPlace->where(['type' => 'tour_place','active' => config('settings.active'),'parent_id' => null])->orderBy('name','asc')->pluck('name','id');
        return \response()->json($tourPlace);
    }
    /*
     * Hàm lấy danh sách địa điểm du lịch
     * @param: $id
     * */
    public function searchComplete(Request $request){
        $idPlace = $request->get('idPlace');
        $cities = TourProperty::where(['type' => 'tour_place', 'parent_id' => $idPlace])->get();
        $result = [];
        if (!empty($cities)){
            foreach ($cities as $key){
                $result[] = ['id' => $key->id, 'name' => $key->name];
            }
        }
        return \response()->json($result);
    }
    /*
     * Hàm lấy danh sách địa điểm du lịch
     * @param: $id
     * */
    public function getModalSearch(Request $request){
        $keySearch = $request->get('keySearch');
        $themeName = Setting::getSettingThemeName();
        if ($keySearch === 'home') return view('theme::'.$themeName.'.frontend.home.search-tour-home');
        return view('theme::'.$themeName.'.frontend.layouts.search-form-tour');
    }
}
