<?php

namespace Modules\Theme\Http\Controllers\Eagle;

use App\Setting;
use App\Traits\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Modules\Theme\Entities\Gallery;
use Modules\Theme\Entities\Menu;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = config('settings.perpage10');
        $galleries = new Gallery();

        if (!empty($keyword)){
            $galleries = $galleries->where('title','LIKE',"%$keyword%");
        }

        $galleries = $galleries->orderBy('arrange','asc')->paginate($perPage);
        return view('theme::eagle.backend.galleries.index', compact('galleries'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $arrange = Gallery::max('arrange');
        return view('theme::eagle.backend.galleries.create', compact('arrange'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required'
        ]);

        \DB::transaction(function() use ($request){
            $requestData = $request->all();
            if (empty($request->slug)){
                $requestData['slug'] = \Str::slug($request->title);
            }
            $requestData['slug'] = Menu::setSlug($requestData['slug']);
            $requestData['files'] = implode("$", $requestData['files']);
            Gallery::create($requestData);
        });

        \Session::flash('flash_message', __('theme::galleries.created_success'));

        return redirect('admin/themes/galleries');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $gallery = Gallery::findOrFail($id);
        return view('theme::eagle.backend.galleries.show', compact('gallery'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $gallery = Gallery::findOrFail($id);
        return view('theme::eagle.backend.galleries.edit', compact('gallery'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required'
        ]);
        $gallery = Gallery::findOrFail($id);
        \DB::transaction(function() use ($request, $gallery){
            $requestData = $request->all();
            if (empty($request->get('slug'))){
                $requestData['slug'] = \Str::slug($requestData['title']);
                $requestData['slug'] = Menu::setSlug($requestData['slug']);
            }
            if (empty($requestData['active'])){
                $requestData['active'] = 0;
            }
            $requestData['files'] = implode("$", $requestData['files']);
            $gallery->update($requestData);
        });

        \Session::flash('flash_message', __('theme::galleries.updated_success'));

        return redirect('admin/themes/galleries');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $gallery = Gallery::findOrFail($id);
        foreach (explode('$', $gallery->files) as $file){
            Gallery::deleteFile($file);
        }
        $gallery->destroy($id);

        \Session::flash('flash_message', __('theme::galleries.deleted_success'));
        return redirect('admin/themes/galleries');
    }

    /*Upload image*/
    public function uploadAlbums(Request $request){
        $file = $request->file('file');
        if ($path_image = Gallery::saveImageResize($file)){
            $info['name'] = $path_image;
            $info['src'] = asset(\Storage::url($path_image));
            return \response()->json($info);
        }
        return \response()->json('error');
    }
    public function deleteAlbums(Request $request){
        $folder = $request->folder;
        Gallery::deleteFile($folder);

        return \Response::json('success', 200);
    }
}
