<?php

namespace Modules\Theme\Http\Controllers\Eagle;

use App\Setting;
use App\Traits\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Modules\Theme\Entities\Block\Slide;

class SlideController extends Controller
{
    use Authorizable;
    private function getThemes(){
        return Setting::getSettingThemeName();
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = config('settings.perpage');
        $slides = new Slide();

        if (!empty($keyword)){
            $slides = $slides->where('name','LIKE',"%$keyword%");
        }
        $slides = $slides->paginate($perPage);

        return view('theme::eagle.backend.slides.index', compact('slides'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('theme::eagle.backend.slides.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:'.(5*1024),
        ]);

        \DB::transaction(function () use ($request){
            $requestData = $request->all();
            if ($request->hasFile('image')){
                if ($this->getThemes() === "dulichdaibang"){
                    $requestData['image'] = Slide::saveImageResize($request->file('image'), 2000, 732);
                }else{
                    $requestData['image'] = Slide::saveImageResize($request->file('image'));
                }
            }
            Slide::create($requestData);
        });
        \Session::flash('flash_message', __('theme::slides.created_success'));

        return redirect('admin/themes/slides');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $slide = Slide::findOrFail($id);
        return view('theme::eagle.backend.slides.show', compact('slide'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $slide = Slide::findOrFail($id);
        return view('theme::eagle.backend.slides.edit', compact('slide'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:'.(5*1024),
        ]);
        $slide = Slide::findOrFail($id);
        \DB::transaction(function () use ($request, $slide){
            $requestData = $request->all();
            if ($request->hasFile('image')){
                Slide::deleteFile($slide->image);
                if ($this->getThemes() === "dulichdaibang"){
                    $requestData['image'] = Slide::saveImageResize($request->file('image'), 2000, 732);
                }else{
                    $requestData['image'] = Slide::saveImageResize($request->file('image'));
                }
            }
            $slide->update($requestData);
        });
        \Session::flash('flash_message', __('theme::slides.updated_success'));
        return redirect('admin/themes/slides');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $slide = Slide::findOrFail($id);
	    Slide::deleteFile($slide->image);

        Slide::destroy($id);

        \Session::flash('flash_message', __('theme::slides.deleted_success'));

        return redirect('admin/themes/slides');
    }
}
