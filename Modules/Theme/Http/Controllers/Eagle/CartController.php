<?php

namespace Modules\Theme\Http\Controllers\Eagle;

use App\Cart;
use App\Events\BookingEvent;
use App\PaymentMethod;
use App\PaymentPaypal;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Modules\Booking\Entities\Booking;
use Modules\Booking\Entities\BookingItem;
use Modules\Booking\Entities\BookingProperty;
use Modules\Booking\Entities\Customer;
use Modules\Booking\Entities\CustomerType;
use Modules\Booking\Entities\Status;
use Modules\Theme\Entities\Theme;
use Modules\Tour\Entities\Tour;
use Illuminate\Support\Facades\Session;
use Modules\Tour\Entities\TourDeposit;
use mysql_xdevapi\Exception;
use PayPal\Api\Amount;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Exception\PayPalConnectionException;
use PayPal\Rest\ApiContext;
use Illuminate\Support\Arr;

class CartController extends Controller
{
    private $_api_context;

    private function initPaypal()
    {
        /** PayPal api context **/
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
                $paypal_conf['client_id'],
                $paypal_conf['secret'])
        );
        $this->_api_context->setConfig($paypal_conf['settings']);
    }

    public function resetSession(Request $request){
        $request->session()->flush();
    }

    /**
     * Khởi tạo dữ liệu booking cho tour (tạo đối tượng cart với dữ liệu tour truyền vào)
     * Sau này nếu làm module khác như bus hay car thì thêm hàm addBus hay addCar
     * @return Response
     */
    public function addBooking(Request $request){
        $request->session()->forget('book');
        $tour = Tour::find($request->id);
        $adult = !empty($request->adult) ? $request->adult : 1;
        $child = !empty($request->child) ? $request->child : 0;
        $baby = !empty($request->baby) ? $request->baby : 0;
        $carts = new Cart();
        $carts->addBookingItem($tour,$adult,$child,$baby);
        return redirect('/booking-tour');
    }

    /***
     * Hàm thực hiện cập nhật dữ liệu khi thay đổi số lượng người ở tour được chọn
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeQty(Request $request){
        $id = $request->id;
        $qty = $request->qty;
        $name = $request->name;
        $totalQty = $request->totalQty;

        $tour = Tour::find($id);
        if ($tour->limit){
            $people_exist = Theme::getExistPeopleLimitTour($tour);
            if ((int)$totalQty > (int)$people_exist) return \response()->json(['error_limit' => 'Số lượng giới hạn còn lại của sản phẩm '.$tour->name.' là: '.$people_exist.', vui lòng nhập số lượng bằng, nhỏ hơn số lượng giới hạn còn lại của sản phẩm hoặc liên hệ với nhân viên để được tư vấn thêm, xin cám ơn!']);
        }
        $books = new Cart();
        $books->setQty($id, $qty, $name);
        $item = $books->getBook();
        $price = $item[$id]['item']->price;
        $priceChildren = $item[$id]['item']->price_children;
        $priceBaby = $item[$id]['item']->price_baby;

        // thiết lập hình thức thanh toán

        return \response()->json([
            'price' => $price,
            'price_children' => $priceChildren,
            'price_baby' => $priceBaby,
            'totalAdult' => $books->getTotalAdults(),
            'totalChild' => $books->getTotalChild(),
            'totalBaby' => $books->getTotalBaby(),
            'totalPrice' => $books->getBookingTotalPrice(),
            'totalPayment' => $books->getTotalPayment(),
            'error_limit' => '',
        ]);
    }

    /***
     * Lưu booking
     * @param $module
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
     * @throws \Illuminate\Validation\ValidationException
     * Session notice:
     * paypal_error: lỗi không kết nối thanh toán được (dữ liệu nội dung lỗi hiển thị)
     * paypal_success: kết nối thanh toán thành công (lưu danh sách booking ids)
     * paypal_success_error: lỗi ở phần thanh toán
     */
    public function checkoutBooking($module, Request $request)
    {
        if (!\Session::has('book')) return;
        $payWithPaypal = false;
        $payWithVNPay = false;

        // validate thông tin người liên hệ
        $this->validate($request, [
            'customer.name' => 'required',
            'customer.address' => 'nullable',
            'customer.email' => 'nullable|email',
            'customer.phone' => 'required|numeric|digits_between:7,13',
            'payment_method' => 'required'
        ]);

        //get Cart

        $book = new Cart();
        $books = $book->getBook();
        $requestData = $request->all();

        //create or update customer
        $customer = new Customer();
        $customer = $customer->where('phone', trim($requestData['customer']['phone']))->orWhere('email', trim($requestData['customer']['email']))->first();

        if ($customer){
            if (empty($requestData['customer']['email'])) $requestData['customer']['email'] = $customer->email;
            if (empty($requestData['customer']['address'])) $requestData['customer']['address'] = $customer->address;
            $customer->update($requestData['customer']);
        }else{
            $customer = Customer::create($requestData['customer']);
            $customerType = CustomerType::find(2); // tao moi la khach thuong
            $customer->customerType()->associate($customerType);
            $customer->save();
        }

        $bankCode = "";
        if ($requestData['payment_method'] === config('booking.payment_method.paypal')){
            $payWithPaypal = true;
            $listItem = [];
        } else if ($requestData['payment_method'] === config('booking.payment_method.vnpay'))
        {
            $payWithVNPay = true;
            $bankCode = $requestData['bankType'];
            $listItem = [];
        } else if ($requestData['payment_method'] === config('booking.payment_method.credit'))
        {
            $payWithVNPay = true;
            $bankCode = $requestData['cardType'];
            $listItem = [];
        } else if ($requestData['payment_method'] === config('booking.payment_method.vnpayqr'))
        {
            $payWithVNPay = true;
            $bankCode = 'VNPAYQR';
            $listItem = [];
        }
        //create booking
        $bookings_id = [];
        // lay giá chuyển đổi 23000 (sau này thay đổi phương thức lấy tiền tệ chuyển đổi tự động)
        $unit_price = Booking::$defaultParams['unit_price'];
        foreach ($books as $key => $tour) {
            $booking = Booking::create([
                'total_price' => $tour['item']->price,
                'customer_id' => $customer->id,
                'is_vat' => 1,
                'note' => $requestData['note']===null?"":$requestData['note'],
                'payment_method_id' => PaymentMethod::where('code', $requestData['payment_method'])->value('id'),
                'status_id' => $requestData['hidpay']
            ]);
            // kiem tra loai module de xu ly tinh gia ca theo tung loai (totalPrice)
            $bookings_id[] = $booking->id;
            $total = 0;

            if ($module=="tour")
            {
                //tao 3 dong booking_detail
                $bookingItems = [];
                $bookingItems[sizeof($bookingItems)] = BookingItem::create([
                    'booking_id' => $booking->id,
                    'name' => 'adult_number',
                    'item_number' => $requestData['hidadult'],
                    'price' => $tour['item'] -> price
                ]);
                if ($requestData['hidchild'] > 0)
                {
                    $bookingItems[sizeof($bookingItems)] = BookingItem::create([
                        'booking_id' => $booking->id,
                        'name' => 'child_number',
                        'item_number' => $requestData['hidchild'],
                        'price' => $tour['item'] -> price_children?$tour['item'] -> price_children:0
                    ]);
                }
                if ($requestData['hidbaby'] > 0){
                    $bookingItems[sizeof($bookingItems)] = BookingItem::create([
                        'booking_id' => $booking->id,
                        'name' => 'baby_number',
                        'item_number' => $requestData['hidbaby'],
                        'price' => $tour['item'] -> price_baby?$tour['item'] -> price_baby:0
                    ]);
                }
                if ($requestData['hidpay'] == 3){
                    $deposit = TourDeposit::where('tour_id', $tour['item']->id)->orderBy('value', 'ASC')->first();
                }
                foreach($bookingItems as $bi => $item )
                {
                    $total += $item->price * $item->item_number;
                }
                $booking -> bookingItems() -> saveMany($bookingItems);

                //Nếu có ngày xuất phát
                if (!empty($requestData['properties'])){
                    $booking_properties = BookingProperty::where('module', 'tour')->pluck('type', 'key');
                    foreach ($booking_properties as $keyProperty => $type){
                        $bookingPropertyId = BookingProperty::where(['module' => 'tour', 'key' => $keyProperty])->value('id');
                        $booking->properties()->attach($bookingPropertyId, ['value' => $requestData['properties'][$keyProperty]]);
                    }
                }
            }
            // cap nhat booking sau khi lay giá
            $booking->total_price = $total;
            $booking->services()->attach([$key => ['price' => $total]]);
            $booking->save();
            // lấy số tiền cần thanh toán
            $totalPrice = $requestData['hidpay'] == 3 ? ($total*$deposit->value)/100 : $total;
            //Lấy id booking truyền vào item tương ứng paypal
            if ($payWithPaypal){
                $listItem[$key] = new Item();
                $listItem[$key]->setName($tour['item']->name)
                    ->setCurrency('USD')
                    ->setSku($booking->id)
                    ->setTax($requestData['hidpay'])
                    ->setQuantity(1)
                    ->setPrice(round($totalPrice/$unit_price, 2, PHP_ROUND_HALF_UP));
            } else if($payWithVNPay)
            {
                $listItem = array(
                    "vnp_Version" => "2.0.0",
                    "vnp_TmnCode" => config('vnpay.vnp_TmnCode'),
                    "vnp_Amount" => $totalPrice * 100,
                    "vnp_Command" => "pay",
                    "vnp_CreateDate" => date('YmdHis'),
                    "vnp_CurrCode" => "VND",
                    "vnp_IpAddr" => '10.238.210.104',
                    "vnp_Locale" => 'vn',
                    "vnp_OrderInfo" => "Thanh toán dịch vụ",
                    "vnp_OrderType" => '170000',
                    "vnp_ReturnUrl" => \URL::route('check-vnpay'),
                    "vnp_BankCode" => $bankCode,
                    "vnp_TxnRef" => $booking->id
                );
            }
        }

        \Session::put('paypal_success', $bookings_id);
        //Pay with paypal
        if ($payWithPaypal){
            \Session::put('moduleBook',$module);

            $this->initPaypal();
            //A resource representing a Payer that funds a payment For PayPal account payments
            $payer = new Payer();
            $payer->setPaymentMethod('paypal');

            //(Optional) Lets you specify item wise information
            $values = Arr::flatten($listItem);
            $item_list = new ItemList();
            $item_list->setItems($values);

            $total_price = null;
            foreach ($item_list->getItems() as $item){
                $total_price += (float)$item->getPrice();
            }
            //Lets you specify a payment amount. You can also specify additional details such as shipping, tax.
            $amount = new Amount();
            $amount->setCurrency('USD')
                ->setTotal($total_price);

            //A transaction defines the contract of a payment – what is the payment for and who is fulfilling it.
            $transaction = new Transaction();
            $transaction->setAmount($amount)
                ->setItemList($item_list)
                ->setDescription('Your transaction description');

            //Set the URLs that the buyer must be redirected to after payment approval/ cancellation
            $redirect_urls = new RedirectUrls();
            $redirect_urls->setReturnUrl(\URL::route('check-paypal')) /** Specify return URL **/
            ->setCancelUrl(\URL::route('check-paypal'));

            //A Payment Resource; create one using the above types and intent set to ‘sale’
            $payment = new Payment();
            $payment->setIntent('Sale')
                ->setPayer($payer)
                ->setRedirectUrls($redirect_urls)
                ->setTransactions(array($transaction));

            try {
                $payment->create($this->_api_context);
            } catch (PayPalConnectionException $ex) {
                if (\Config::get('app.debug')) {
                    dd($ex);
                } else {
                    \Session::put('paypal_success_error', 'Có thể một số sự cố đã xảy ra dẫn đến việc không thể liên kết đến trang thanh toán Paypal, xin lỗi vì sự bất tiện này!');
                    return \redirect('thanh-toan');
                }
            }

            foreach ($payment->getLinks() as $link) {
                if ($link->getRel() == 'approval_url') {
                    $redirect_url = $link->getHref();
                    break;
                }
            }
            /** add payment ID to session **/
            \Session::put('paypal_payment_id', $payment->getId());
            if (isset($redirect_url)) {
                /** redirect to paypal **/
                return Redirect::away($redirect_url);
            }

            //Xóa giỏ hàng khi đã Tạo Booking, trong trường hợp thanh toán không kết nối được paypal
            \Session::forget('book');

            \Session::put('paypal_success_error', 'Một số sự cố đã xảy ra dẫn đến việc không thể liên kết đến trang thanh toán Paypal, xin lỗi vì sự bất tiện này!');
            return \redirect('thanh-toan');
        }
        else if($payWithVNPay)
        {
            \Session::put('moduleBook',$module);
            $vnp_Url = config('vnpay.vnp_Url');
            $vnp_HashSecret = config('vnpay.vnp_HashSecret');
            ksort($listItem);
            $query = "";
            $i = 0;
            $hashdata = "";
            foreach ($listItem as $key => $value) {
                if ($i == 1) {
                    $hashdata .= '&' . $key . "=" . $value;
                } else {
                    $hashdata .= $key . "=" . $value;
                    $i = 1;
                }
                $query .= urlencode($key) . "=" . urlencode($value) . '&';
            }

            $vnp_Url = $vnp_Url . "?" . $query;
            if (isset($vnp_HashSecret)) {
                $vnpSecureHash = hash('sha256',$vnp_HashSecret . $hashdata);
                $vnp_Url .= 'vnp_SecureHashType=SHA256&vnp_SecureHash=' . $vnpSecureHash;
                return redirect($vnp_Url);
            }

            //Xóa giỏ hàng khi đã Tạo Booking, trường hợp không kết nối được thanh toán VNPay
            \Session::forget('book');

            \Session::put('paypal_success_error', 'Một số sự cố đã xảy ra dẫn đến việc không thể liên kết đến trang thanh toán VNPay, xin lỗi vì sự bất tiện này!');
            return \redirect('thanh-toan');
        }

        event(new BookingEvent($bookings_id));
        //Xóa giỏ hàng khi đã Tạo Booking, thanh toán thủ công
//        \Session::forget('book');
        //Thanh toán thành công
//        \Session::put('paypal_success', $bookings_id);
        return redirect('thanh-toan');
    }

    public function checkPaypal(){
        $this->initPaypal();
        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');

        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
            // chữ kí không hợp lệ
            $booking_ids = Session::get('paypal_success');
            \Session::forget('paypal_success');
            //Xoá booking đã lưu
            foreach($booking_ids as $bid)
            {
                $book = Booking::find($bid);
                $book->forceDelete();
            }
            return redirect('/booking-tour');
        }

        $payment = Payment::get($payment_id, $this->_api_context);
//        dump($payment['transactions']);
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));

        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);
        //Lấy các booking id từ paypal
        //Cập nhật trạng thái booking - đã thanh toán (Lấy status mặc định)
        $bookings_id = [];
        foreach ($result->getTransactions()[0]->getItemList()->getItems() as $tour){
            $booking = Booking::find($tour->getSku());
            if ($tour->getTax() == Booking::$defaultParams['pay_success_id_status_50']){
                $booking->status_id = Booking::$defaultParams['pay_success_id_status_50'];
            }else{
                $booking->status_id = Booking::$defaultParams['pay_success_id_status'];
            }
            $booking->save();

            $bookings_id[] = $tour->getSku();
        }

        event(new BookingEvent($bookings_id));

        \Session::forget('moduleBook');

        if ($result->getState() == 'approved') {
            return \redirect('thanh-toan');
        }

        return \redirect('thanh-toan');
    }

    public function checkVNPay(Request $request){
        // lay ma booking da duoc luu
        $booking_ids = \Session::get('paypal_success');
        // kiem tra ket qua tra ve de hien thi thong bao ket qua
        // success chuyen ve trang ket qua thanh toan
        // fail thong bao va chuyen ve trang truoc do
//        $result_msg = "";

        // nếu tồn tại lỗi thì lưu biến lỗi của bên thanh toán vào session hoặc log, không làm gì.

        try {
            $inputData = array();
            $data = $_REQUEST;
            foreach ($data as $key => $value) {
                if (substr($key, 0, 4) == "vnp_") {
                    $inputData[$key] = $value;
                }
            }

            $vnp_SecureHash = $inputData['vnp_SecureHash'];
            unset($inputData['vnp_SecureHashType']);
            unset($inputData['vnp_SecureHash']);
            ksort($inputData);
            $i = 0;
            $hashData = "";
            foreach ($inputData as $key => $value) {
                if ($i == 1) {
                    $hashData = $hashData . '&' . $key . "=" . $value;
                } else {
                    $hashData = $hashData . $key . "=" . $value;
                    $i = 1;
                }
            }
            $secureHash = hash('sha256',config('vnpay.vnp_HashSecret') . $hashData);
            //Check Orderid
            //Kiểm tra checksum của dữ liệu
            if ($secureHash == $vnp_SecureHash) {
                switch ($inputData['vnp_ResponseCode'])
                {
                    case "00":
                        //Thanh toán thành công
                        //Xóa giỏ hàng
                        \Session::forget('book');
                        //Gửi email tới người dùng.
                        event(new BookingEvent($booking_ids));
                        \Session::forget('moduleBook');
                        //cập nhật trạng thái đã thanh toán cho đơn hàng
                        foreach($booking_ids as $bid)
                        {
                            $book = Booking::find($bid);
                            $book->status_id = Booking::$defaultParams['pay_success_id_status'];
                            $book->save();
                        }
                        // chuyển về trang thanh toán thành công
                        return \redirect('thanh-toan');
                        break;
                    case "01":
//                        $result_msg = "Giao dịch đã tồn tại";
                        break;
                    case "02":
//                        $result_msg = "Merchant không hợp lệ";
                        break;
                    case "03":
//                        $result_msg = "Dữ liệu gửi sang không đúng định dạng";
                        break;
                    case "04":
//                        $result_msg = "Khởi tạo GD không thành công do Website đang bị tạm khóa";
                        break;
                    case "05":
//                        $result_msg = "Giao dịch không thành công do: Quý khách nhập sai mật khẩu quá số lần quy định. Xin quý khách vui lòng thực hiện lại giao dịch";
                        break;
                    case "13":
//                        $result_msg = "Giao dịch không thành công do Quý khách nhập sai mật khẩu xác thực giao dịch (OTP). Xin quý khách vui lòng thực hiện lại giao dịch.";
                        break;
                    case "07":
//                        $result_msg = "Giao dịch bị nghi ngờ là giao dịch gian lận";
                        break;
                    case "09":
//                        $result_msg = "Giao dịch không thành công do: Thẻ/Tài khoản của khách hàng chưa đăng ký dịch vụ InternetBanking tại ngân hàng.";
                        break;
                    case "10":
//                        $result_msg = "Giao dịch không thành công do: Khách hàng xác thực thông tin thẻ/tài khoản không đúng quá 3 lần";
                        break;
                    case "11":
//                        $result_msg = "Giao dịch không thành công do: Đã hết hạn chờ thanh toán. Xin quý khách vui lòng thực hiện lại giao dịch.";
                        break;
                    case "12":
//                        $result_msg = "Giao dịch không thành công do: Thẻ/Tài khoản của khách hàng bị khóa.";
                        break;
                    case "51":
//                        $result_msg = "Giao dịch không thành công do: Tài khoản của quý khách không đủ số dư để thực hiện giao dịch.";
                        break;
                    case "65":
//                        $result_msg = "Giao dịch không thành công do: Tài khoản của Quý khách đã vượt quá hạn mức giao dịch trong ngày.";
                        break;
                    case "08":
//                        $result_msg = "Giao dịch không thành công do: Hệ thống Ngân hàng đang bảo trì. Xin quý khách tạm thời không thực hiện giao dịch bằng thẻ/tài khoản của Ngân hàng này.";
                        break;
                    case "99":
//                        $result_msg = "Giao dịch không thành công";
                        break;
                    default:
                        \Session::forget('paypal_success');
                        //Xoá booking đã lưu
                        foreach($booking_ids as $bid)
                        {
                            $book = Booking::find($bid);
                            $book->forceDelete();
                        }
                        return redirect('/booking-tour');
                        break;
                }
            } else {
                // chữ kí không hợp lệ
                \Session::forget('paypal_success');
                //Xoá booking đã lưu
                foreach($booking_ids as $bid)
                {
                    $book = Booking::find($bid);
                    $book->forceDelete();
                }
                return redirect('/booking-tour');
            }
        } catch (Exception $e) {
            \Session::forget('paypal_success');
            //Xoá booking đã lưu
            foreach($booking_ids as $bid)
            {
                $book = Booking::find($bid);
                $book->forceDelete();
            }
            return redirect('/booking-tour');
        }
    }

    public function loadCondition(Request $request)
    {
        $code = $request->code;
        $payment = \App\PaymentMethod::where('code', $code)->first();
        return \response()->json(['description' => $payment->description ]);
    }

    public function rollbackBook(Request $request)
    {
        try
        {
            $book_id = Session::get('paypal_success');
            \Session::forget('paypal_success');
            // neu ton tai session thi xoa book
            if(isset($book_id) && !isEmpty($book_id[0]))
            {
                $book = Booking::find($book_id[0]);
                $book->forceDelete();
            }
            return \response()->json([
                'success' => true
            ]);
        }catch (Exception $ex)
        {
            return \response()->json([
                'success' => false
            ]);
        }
    }
}
