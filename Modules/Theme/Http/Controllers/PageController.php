<?php

namespace Modules\Theme\Http\Controllers;

use App\Traits\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Modules\News\Entities\NewsType;
use Modules\Theme\Entities\Menu;
use Modules\Theme\Entities\Page;
use Illuminate\Support\Str;

class PageController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
//        $perPage = config('settings.perpage');
//
        $pages = Page::sortable();

        if (!empty($keyword)){
            $pages = Page::where('title','LIKE',"%$keyword%");
        }

        $pages = $pages->get();
        return view('theme::manage.pages.index', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $treePage = Page::all();
        $parents = NewsType::getListNewsTypeToArray($treePage);
        $parents = \Arr::prepend(!empty($parents) ? $parents : [], __('message.please_select'), '');
        $position = config('theme.menu.position');
        return view('theme::manage.pages.create', compact('position', 'parents'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required'
        ]);

        $requestData = $request->all();
        if (empty($request->get('slug'))){
            $requestData['slug'] = Str::slug($requestData['title']);
        }

        $requestData['slug'] = Menu::setSlug($requestData['slug']);

        \DB::transaction(function () use ($request, $requestData){
            $page = Page::create($requestData);
            Menu::saveUpdateMenu(config('settings.model_pages'),$page,null,$request->position,true);
        });

        Session::flash('flash_message', __('theme::pages.created_success'));
        return redirect('admin/themes/pages');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $page = Page::findOrFail($id);
        return view('theme::manage.pages.show', compact('page'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $page = Page::findOrFail($id);
        $treePage = Page::all();
        $parents = NewsType::getListNewsTypeToArray($treePage);
        $parents = \Arr::prepend($parents, __('message.please_select'), '');
        $position = config('theme.menu.position');
        $menu = Menu::where('slug',$page->slug)->first();
        $position_edit = null;
        if (!empty($menu)){
            $position_edit = explode(',',$menu->position);
        }
        return view('theme::manage.pages.edit', compact('page','parents', 'position', 'position_edit'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required'
        ]);

        $page = Page::findOrFail($id);

        $requestData = $request->all();
        if (empty($request->get('slug'))){
            $requestData['slug'] = Str::slug($requestData['title']);
            $requestData['slug'] = Menu::setSlug($requestData['slug']);
        }

        \DB::transaction(function () use ($request, $requestData, $page){
            $page->update($requestData);
            $menu = Menu::where('slug',$page->slug)->first();
            Menu::saveUpdateMenu(config('settings.model_pages'),$page,$menu,$request->position,false);
        });

        Session::flash('flash_message', __('theme::pages.updated_success'));
        return redirect('admin/themes/pages');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        \DB::transaction(function () use ($id){
            $page = Page::findOrFail($id);
            Menu::where('slug',$page->slug)->delete();
            Page::destroy($id);
        });

        \Session::flash('flash_message', __('theme::pages.deleted_success'));

        return redirect('admin/themes/pages');
    }
}
