<?php

namespace Modules\Theme\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Modules\Theme\Entities\Theme;

class ThemeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $themes = Theme::all();
        return view('theme::themes.index', compact('themes'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('theme::themes.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:'.(5*1024),
        ]);

        \DB::transaction(function () use ($request){
            $requestData = $request->all();
            if ($request->hasFile('image')){
                $requestData['image'] = Theme::saveImageResize($request->file('image'));
            }
            Theme::create($requestData);
        });
        \Session::flash('flash_message', __('theme::themes.created_success'));

        return redirect('admin/themes/theme');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('theme::themes.show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $theme = Theme::findOrFail($id);
        return view('theme::themes.edit', compact('theme'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:'.(5*1024),
        ]);
        $theme = Theme::findOrFail($id);
        \DB::transaction(function () use ($request, $theme){
            $requestData = $request->all();
            if ($request->hasFile('image')){
                Theme::deleteFile($theme->image);
                $requestData['image'] = Theme::saveImageResize($request->file('image'));
            }
            $theme->update($requestData);
        });
        \Session::flash('flash_message', __('theme::themes.updated_success'));
        return redirect('admin/themes/theme');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        return redirect('theme');
    }
}
