<?php

namespace Modules\Theme\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Theme\Entities\Gallery;

class AjaxController extends Controller
{
    /**
     * Gọi ajax: sẽ gọi đến hàm = tên $action
     * @param Request $action
     * @param Request $request
     * @return mixed
     */
    public function index($action, Request $request)
    {
        return $this->{$action}($request);
    }
    //Delete Item
    public function delItem(Request $request){
        $ids = $request->ids;
        $arrId = explode(',', $ids,-1);
        foreach ($arrId as $item){
            $gallery = Gallery::findOrFail($item);
            foreach (explode('$', $gallery->files) as $file){
                Gallery::deleteFile($file);
            }
            $gallery->destroy($item);
        }
        return \response()->json(['success' => 'ok']);
    }
    //Active Item
    public function activeItem(Request $request){
        $ids = $request->ids;
        $arrId = explode(',', $ids,-1);
        foreach ($arrId as $item){
            $gallery = Gallery::findOrFail($item);
            $active = $gallery->active == config('settings.active') ? config('settings.inactive') : config('settings.active');
            \DB::table('galleries')->where('id',$gallery->id)->update(['active' => $active]);
        }
        return \response()->json(['success' => 'ok']);
    }

}
