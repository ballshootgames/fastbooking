const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');

mix.setPublicPath('../../public').mergeManifest();

mix.js(__dirname + '/Resources/assets/eagle/js/app.js', 'js/web-eagle.js')
    .js(__dirname + '/Resources/assets/tomap/js/app.js', 'js/web-tomap.js')
    .js(__dirname + '/Resources/assets/dulichdaibang/js/app.js', 'js/web-dulichdaibang.js')
    .sass( __dirname + '/Resources/assets/eagle/sass/app.scss', 'css/theme-eagle.css')
    .sass( __dirname + '/Resources/assets/tomap/sass/app.scss', 'css/theme-tomap.css')
    .sass( __dirname + '/Resources/assets/dulichdaibang/sass/app.scss', 'css/theme-dulichdaibang.css')
    .combine([
        __dirname + '/Resources/assets/eagle/css/bootstrap.min.css',
        __dirname + '/Resources/assets/eagle/css/font-awesome.min.css',
        __dirname + '/Resources/assets/eagle/css/jquery.mmenu.all.css',
        __dirname + '/Resources/assets/eagle/css/slick.css',
        __dirname + '/Resources/assets/eagle/css/lightslider.min.css',
        __dirname + '/Resources/assets/eagle/css/lightgallery.min.css',
        __dirname + '/Resources/assets/eagle/css/owl.carousel.min.css',
        __dirname + '/Resources/assets/eagle/css/owl.theme.default.min.css',
        __dirname + '/Resources/assets/eagle/css/common.css',
        __dirname + '/Resources/assets/eagle/css/select2.min.css',
        __dirname + '/Resources/assets/eagle/css/format.css',
        '../../public/css/theme-eagle.css'
    ], '../../public/css/web-eagle.css')
    .combine([
        __dirname + '/Resources/assets/tomap/css/blockLibrary.css',
        __dirname + '/Resources/assets/tomap/css/blocks.css',
        __dirname + '/Resources/assets/tomap/css/style-form.css',
        __dirname + '/Resources/assets/tomap/css/woocommerce-layout.css',
        __dirname + '/Resources/assets/tomap/css/woocommerce-smallscreen.css',
        __dirname + '/Resources/assets/tomap/css/woocommerce.css',
        __dirname + '/Resources/assets/tomap/css/bootstrap.min.css',
        __dirname + '/Resources/assets/tomap/css/helpers.css',
        __dirname + '/Resources/assets/tomap/css/font-awesome.min.css',
        __dirname + '/Resources/assets/tomap/css/fotorama.css',
        __dirname + '/Resources/assets/tomap/css/ion.rangeSlider.css',
        __dirname + '/Resources/assets/tomap/css/ion.rangeSlider.skinHTML5.css',
        __dirname + '/Resources/assets/tomap/css/daterangepicker.css',
        __dirname + '/Resources/assets/tomap/css/sweetalert2.css',
        __dirname + '/Resources/assets/tomap/css/select2.min.css',
        __dirname + '/Resources/assets/tomap/css/flickity.css',
        __dirname + '/Resources/assets/tomap/css/magnific-popup.css',
        __dirname + '/Resources/assets/tomap/css/owl.carousel.min.css',
        __dirname + '/Resources/assets/tomap/css/style.css',
        __dirname + '/Resources/assets/tomap/css/search_result.css',
        __dirname + '/Resources/assets/tomap/css/js_composer.min.css',
        '../../public/css/theme-tomap.css'
    ], '../../public/css/web-tomap.css')
    .combine([
        __dirname + '/Resources/assets/dulichdaibang/css/bootstrap.min.css',
        __dirname + '/Resources/assets/dulichdaibang/css/font-awesome.min.css',
        __dirname + '/Resources/assets/dulichdaibang/css/jquery.mmenu.all.css',
        __dirname + '/Resources/assets/dulichdaibang/css/slick.css',
        __dirname + '/Resources/assets/dulichdaibang/css/lightslider.min.css',
        __dirname + '/Resources/assets/dulichdaibang/css/lightgallery.min.css',
        __dirname + '/Resources/assets/dulichdaibang/css/owl.carousel.min.css',
        __dirname + '/Resources/assets/dulichdaibang/css/owl.theme.default.min.css',
        __dirname + '/Resources/assets/dulichdaibang/css/common.css',
        __dirname + '/Resources/assets/dulichdaibang/css/select2.min.css',
        __dirname + '/Resources/assets/dulichdaibang/css/format.css',
        __dirname + '/Resources/assets/dulichdaibang/css/woocommerce.css',
        __dirname + '/Resources/assets/dulichdaibang/css/woocommerce-layout.css',
        '../../public/css/theme-dulichdaibang.css'
    ], '../../public/css/web-dulichdaibang.css')
    .copy(__dirname + '/Resources/assets/eagle/fonts/','../../public/fonts')
    .copy(__dirname + '/Resources/assets/eagle/img/','../../public/img')
    .copy(__dirname + '/Resources/assets/eagle/images/','../../public/images')
    .options({ processCssUrls: false });

if (mix.config.inProduction) {
    mix.version();
}