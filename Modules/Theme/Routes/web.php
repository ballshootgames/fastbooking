<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
$subdomain = \URL::getDefaultParameters()?\URL::getDefaultParameters()['subdomain']:"";
$appRoot = config('app.root').".";
if (\URL::getRequest()->getHost()!=$appRoot.config('app.domain') || !empty($subdomain))
    $appRoot = "";

Route::domain('{subdomain}.'.$appRoot.config('app.domain'))->group(function () {
    Route::group(['middleware' => 'subdomain'], function (){
        Route::group(['middleware' => 'auth', 'prefix' => 'admin'], function (){
            Route::group(['prefix' => 'themes'], function (){
                Route::resource('/theme', 'ThemeController');
                Route::resource('/menus', 'MenuController');
                Route::resource('/pages', 'PageController');
                Route::resource('/contacts', 'ContactController');
                Route::resource('/slides', 'Eagle\SlideController');
                Route::resource('/banners', 'Eagle\BannerController');
                Route::resource('/partners', 'Eagle\PartnerController');
                Route::resource('/galleries', 'Eagle\GalleryController');
                Route::resource('/newsletters', 'Tomap\NewsletterController')->only('index');
                Route::post('/upload', 'Eagle\GalleryController@uploadAlbums');
                Route::post('/delete', 'Eagle\GalleryController@deleteAlbums');
                Route::get('/ajax/{action}', 'AjaxController@index');
            });
        });

        Route::post('login-user/ajax', 'Eagle\AjaxFrontendController@loginUser');
        Route::post('register-user/ajax', 'Eagle\AjaxFrontendController@registerUser');
        Route::get('/ajax/{action}', 'Eagle\AjaxFrontendController@index');
		Route::get('/', 'Eagle\FrontendController@index');
		Route::get('/trang-chu', 'Eagle\FrontendController@index');
		Route::get('404', 'Eagle\FrontendController@error404');
		Route::get('/{slugPage}.html', 'Eagle\FrontendController@getPage');

        Route::group(['prefix' => 'booking'], function (){
            Route::get('/add-item','Eagle\CartController@addBooking');
            Route::get('/change-qty', 'Eagle\CartController@changeQty');
            Route::get('/reset-cart', 'Eagle\CartController@resetSession');
            Route::post('/checkout/{module}', 'Eagle\CartController@checkoutBooking');
            Route::get('/checkout/check-paypal', 'Eagle\CartController@checkPaypal')->name('check-paypal');
            Route::get('/checkout/check-vnpay', 'Eagle\CartController@checkVNPay')->name('check-vnpay');
            Route::get('/load-condition', 'Eagle\CartController@loadCondition');
            Route::get('/rollback-book', 'Eagle\CartController@rollbackBook');
        });

        Route::group(['prefix' => '{slugParent}'], function (){
            Route::get('/', 'Eagle\FrontendController@getListsParents');
            Route::get('/{slugDetail}.html', 'Eagle\FrontendController@getDetail');
            Route::get('/{slugChild}', 'Eagle\FrontendController@getLists');
        });
        Route::post('/lien-he/ajax', 'Eagle\FrontendController@postContact');
        Route::post('/newsletters/ajax', 'Eagle\FrontendController@postNewsletter');
        Route::post('/send-file-tour-schedule/ajax', 'Eagle\FrontendController@postSendFileTourSchedule');
        Route::post('/send-info-installment/ajax', 'Eagle\FrontendController@postSendInfoInstallment');
    });
});

Route::get('/', function () {
    return view('welcome');
});
