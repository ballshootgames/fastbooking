<?php
return [
    'name' => 'Theme',
    'book' => null,
    'root_url' => '',
    'menu' => [
        'position' => [
            1 => 'Main menu',
            2 => 'Menu bottom',
            3 => 'Home content',
        ]
    ],
    'list_price' => [
        1 => 'Dưới 500,000 đ',
        2 => 'Dưới 1 triệu',
        3 => 'Từ 1tr - 2tr',
        4 => 'Từ 2tr - 5tr',
        5 => 'Từ 5tr - 10tr',
        6 => 'Từ 10tr - 20tr',
        7 => 'Từ 20tr - 50tr',
        8 => 'Trên 50tr',
    ],
    'menus' => [
        'main_menu' => 1,
        'bottom_menu' => 2
    ],
    'url_menu' => [
        'category' => 0,
        'page' => 1
    ],
    'show_sidebar_right' => 1,
    'show_related' => 2,
    'id_news_type_parent' => [
        'id_news_tour' => 4,
        'id_news_destination' => 14
    ],
    /*Define Type ID Partners for Backend*/
    'type_id' => [
        1 => 'Đối tác',
        2 => 'Vì sao chọn'
    ],
    'text' => [
        1 => 'partner',
        2 => 'why_us'
    ],
    /*Define Type ID Partners for Frontend*/
    'ptype_id_web' => [
        'customer' => 0,
        'partner' => 1,
        'why_us' => 2
    ],
    'slug' => [
        'slug_tour' => 'tour',
        'slug_tour_category' => 'loai-tour',
        'slug_tin_tuc' => 'tin-tuc-du-lich',
        'slug_dia_diem' => 'dia-diem-du-lich',
        'slug_tour_foreign' => 'tour-du-lich-nuoc-ngoai',
        'slug_tour_inland' => 'tour-du-lich-trong-nuoc',
        'slug_tour_list' => 'dong-tour',
        'slug_tour_place' => 'diem-den-tour',
        'slug_tour_new_hot' => 'tour-moi-cuc-hot',
        'slug_tour_price_shock_discounts' => 'tour-giam-gia-soc'
    ],
    'settings_site' => [
        [
            "key" => "url_logo",
            "value" => "",
            "description" =>"Đường dẫn lưu logo",            
            "type"=>"image",
            "group_data"=>"company_info", // Thông tin công ty
        ],
        [
            "key" => "favicon",
            "value" => "",
            "description" =>"Favicon",
            "type"=>"image",
            "group_data"=>"company_info", // Thông tin công ty
        ],
        [
            "key" => "company_name",
            "value" => "",
            "description" =>"Tên công ty",           
            "type"=>"text",
            "group_data"=>"company_info",// Thông tin công ty
        ],
        [
            "key" => "website_name",
            "value" => "",
            "description" =>"Tên website",           
            "type"=>"text",
            "group_data"=>"company_info",// Thông tin công ty
        ],
        [
            "key" => "company_address",
            "value" => "",
            "description" =>"Địa chỉ",
            "group_data"=>"company_info",// Thông tin công ty
            "type"=>"text"
        ],
        [
            "key" => "phone",
            "value" => "",
            "description" =>"Số điện thoại",
            "group_data"=>"company_info",// Thông tin công ty
            "type"=>"text"
        ],
        [
            "key" => "website_link",
            "value" => Request::fullUrl(),
            "description" =>"Website",
            "group_data"=>"company_info",// Thông tin công ty
            "type"=>"text"
        ],
        [
            "key" => "email_receive_message",
            "value" => "",
            "description" =>"Email",
            "group_data"=>"company_info",// Thông tin công ty
            "type"=>"text"
        ],
        [
            "key" => "ministry_industry_trade_link",
            "value" => '',
            "description" =>"Địa chỉ 'Đã thông báo bộ công thương'",
            "group_data"=>"company_info",// Thông tin công ty
            "type"=>"text"
        ],
        [
            "key" => "facebook_link",
            "value" => "https://www.facebook.com/",
            "description" =>"Địa chỉ facebook",
            "group_data"=>"social_info",// Mạng xã hội
            "type"=>"text"
        ],
        [
            "key" => "twitter_link",
            "value" => "https://twitter.com/",
            "description" =>"Địa chỉ twitter",
            "group_data"=>"social_info",// Mạng xã hội
            "type"=>"text"
        ],
        [
            "key" => "linkedin_link",
            "value" => "https://www.linkedin.com/",
            "description" =>"Địa chỉ linkedin",
            "group_data"=>"social_info",// Mạng xã hội
            "type"=>"text"
        ],   
        [
            "key" => "fanpage_facebook_embed",
            "value" => "",
            "description" =>"Mã nhúng fanpage facebook",
            "group_data"=>"social_info",// Mạng xã hội
            "type"=>"textarea"
        ],
        [
            "key" => "google_analytics",
            "value" => "",
            "description" =>"Mã nhúng Analytics",
            "group_data"=>"social_info",// Mạng xã hội
            "type"=>"textarea"
        ],
        [
            "key" => "managing",
            "value" => "",
            "description" =>"Chủ quản",
            "group_data"=>"general_info",// Thông tin chung 
            "type"=>"text"
        ],
        [
            "key" => "representative",
            "value" => "",
            "description" =>"Người đại diện",
            "group_data"=>"general_info",// Thông tin chung 
            "type"=>"text"
        ],     
        [
            "key" => "tax_code",
            "value" => "",
            "description" =>"Mã số thuế",
            "group_data"=>"general_info",// Thông tin chung 
            "type"=>"text"
        ],
        [
            "key" => "issued_by_tax_code",
            "value" => "",
            "description" =>"Nơi cấp mã số thuế",
            "group_data"=>"general_info",// Thông tin chung 
            "type"=>"text"
        ],
        [
            "key" => "field",
            "value" => "",
            "description" =>"Lĩnh vực",
            "group_data"=>"general_info",// Thông tin chung 
            "type"=>"text"
        ],
        [
            "key" => "travel_business_license",
            "value" => "",
            "description" =>"Giấy phép KDLQ quốc tế",
            "group_data"=>"general_info",// Thông tin chung 
            "type"=>"text"
        ],
        [
            "key" => "seo_title",
            "value" => "",
            "description" =>"SEO title web",
            "group_data"=>"system_info",// Cấu hình hệ thống
            "type"=>"text"
        ],
        [
            "key" => "seo_keywords",
            "value" => "",
            "description" =>"SEO từ khóa web",
            "group_data"=>"system_info",// Cấu hình hệ thống
            "type"=>"textarea"
        ],
        [
            "key" => "seo_description",
            "value" => "",
            "description" =>"SEO mô tả web",
            "group_data"=>"system_info",// Cấu hình hệ thống
            "type"=>"textarea"
        ],
        [
            "key" => "chk_code_prefix",
            "value" => true,
            "description" =>"Hiển thị mã code prefix",
            "group_data"=>"system_info",// Cấu hình hệ thống
            "type"=>"checkbox"
        ],
        [
            "key" => "code_prefix",
            "value" => "OUT,INB",
            "description" =>"Mã code tour prefix(ví dụ: OUT,INB)",
            "group_data"=>"system_info",// Cấu hình hệ thống
            "type"=>"text"
        ],
        [
            "key" => "code_place",
            "value" => true,
            "description" =>"Hiển thị mã code điểm khởi hành",
            "group_data"=>"system_info",// Cấu hình hệ thống
            "type"=>"checkbox"
        ],
        [
            "key" => "code_num",
            "value" => "6",
            "description" =>"Dãy số code tour(ví dụ: 6 = 000000)",
            "group_data"=>"system_info",// Cấu hình hệ thống
            "type"=>"text"
        ],
        [
            "key" => "drpAscNumber",
            "value" => "code_number",
            "description" =>"Chọn tăng dần theo số thứ tự hoặc tiếp đầu ngữ",
            "group_data"=>"system_info",// Cấu hình hệ thống
            "type"=>"select"
        ],
        [
            "key" => "keyword",
            "value" => "",
            "description" =>"Từ khóa",
            "group_data"=>"system_info",// Cấu hình hệ thống
            "type"=>"textarea"
        ],
        [
            "key" => "deposit",
            "value" => "",
            "description" =>"Thông tin đặt cọc theo tour",
            "group_data"=>"system_info",// Cấu hình hệ thống
            "type"=>"number"
        ],
        [
            "key" => "VNP_TMNCODE",
            "value" => env('VNP_TMNCODE',''),
            "description" =>"Website merchant code VNPay",
            "group_data"=>"system_info",// Cấu hình hệ thống
            "type"=>"text"
        ],
        [
            "key" => "VNP_HASHSECRET",
            "value" => env('VNP_HASHSECRET',''),
            "description" =>"Mã checksum VNPay",
            "group_data"=>"system_info",// Cấu hình hệ thống
            "type"=>"text"
        ],
        [
            "key" => "PAYPAL_CLIENT_ID",
            "value" => env('PAYPAL_CLIENT_ID',''),
            "description" =>"ID Client Paypal",
            "group_data"=>"system_info",// Cấu hình hệ thống
            "type"=>"text"
        ],
        [
            "key" => "PAYPAL_SECRET",
            "value" => env('PAYPAL_SECRET',''),
            "description" =>"Mã bảo mật Paypal",
            "group_data"=>"system_info",// Cấu hình hệ thống
            "type"=>"text"
        ],
        [
            "key" => "phone_contact_tour",
            "value" => "",
            "description" =>"Số điện thoại đặt tour",
            "group_data"=>"contact_info",// Thông tin bài viết
            "type"=>"text"
        ],
        [
            "key" => "email_contact_tour",
            "value" => "",
            "description" =>"Email đặt tour",
            "group_data"=>"contact_info",// Thông tin bài viết
            "type"=>"text"
        ],
        [
            "key" => "phone_contact_advisory",
            "value" => "",
            "description" =>"Số điện thoại tư vấn du lịch",
            "group_data"=>"contact_info",// Thông tin bài viết
            "type"=>"text"
        ],
        [
            "key" => "email_contact_advisory",
            "value" => "",
            "description" =>"Email tư vấn du lịch",
            "group_data"=>"contact_info",// Thông tin bài viết
            "type"=>"text"
        ],
        [
            "key" => "phone_contact_visa",
            "value" => "",
            "description" =>"Số điện thoại tư vấn tour nước ngoài, visa",
            "group_data"=>"contact_info",// Thông tin bài viết
            "type"=>"text"
        ],
        [
            "key" => "email_contact_visa",
            "value" => "",
            "description" =>"Email tư vấn tour nước ngoài, visa",
            "group_data"=>"contact_info",// Thông tin bài viết
            "type"=>"text"
        ],
        [
            "key" => "phone_contact_tour_inland",
            "value" => "",
            "description" =>"Số điện thoại tour trong nước",
            "group_data"=>"contact_info",// Thông tin bài viết
            "type"=>"text"
        ],
        [
            "key" => "phone_contact_tour_foreign",
            "value" => "",
            "description" =>"Số điện thoại tour nước ngoài",
            "group_data"=>"contact_info",// Thông tin bài viết
            "type"=>"text"
        ],
        [
            "key" => "theme_name",
            "value" => "",
            "description" =>"Thiết lập Theme",
            "group_data"=>"theme_setting",// Thông tin bài viết
            "type"=>"theme"
        ],
        [
            "key" => "top_tour",
            "value" => "",
            "description" =>"Từ khóa Top tour",
            "group_data"=>"keyword_home",// Từ khóa bài viết
            "type"=>"text"
        ],
        [
            "key" => "offer_tour",
            "value" => "",
            "description" =>"Từ khóa Offer tour",
            "group_data"=>"keyword_home",// Từ khóa bài viết
            "type"=>"text"
        ],
        [
            "key" => "favorite_tour",
            "value" => "",
            "description" =>"Từ khóa Favorite tour",
            "group_data"=>"keyword_home",// Từ khóa bài viết
            "type"=>"text"
        ],
        [
            "key" => "awesome_tour",
            "value" => "",
            "description" =>"Từ khóa Awesome tour",
            "group_data"=>"keyword_home",// Từ khóa bài viết
            "type"=>"text"
        ],
        [
            "key" => "our_blog",
            "value" => "",
            "description" =>"Từ khóa Our blog",
            "group_data"=>"keyword_home",// Từ khóa bài viết
            "type"=>"text"
        ],
    ],
    'class' => [
        'eagle' => [
            'ul_lv_1' => 'id="main-nav" class="menu"',
            'ul_sub' => 'class = "sub-menu"',
            'li_children_active' => 'class="dropdown active"',
            'li_children' => 'class="dropdown"',
            'li_active' => 'class="current-menu-item"',
            'li' => '',
            'a' => '',
            'i' => ''
        ],
        'tomap' => [
            'ul_lv_1' => 'class="menu main-menu"',
            'ul_sub' => 'class = "menu-dropdown"',
            'li_children_active' => 'class="menu-item menu-item-has-children current-menu-ancestor"',
            'li_children' => 'class="menu-item menu-item-has-children"',
            'li_active' => 'class="menu-item current-menu-ancestor"',
            'li' => 'menu-item',
            'a' => '',
            'i' => '<i class="fa fa-angle-down"></i>'
        ],
        'dulichdaibang' => [
            'ul_lv_1' => 'id="main-nav" class="menu"',
            'ul_sub' => 'class = "sub-menu"',
            'li_children_active' => 'class="dropdown active"',
            'li_children' => 'class="dropdown"',
            'li_active' => 'class="current-menu-item"',
            'li' => '',
            'a' => '',
            'i' => ''
        ],
    ],
    'per_page' => [
        'per_page15' => 15,
        'per_page16' => 16,
    ],
    'type_menu' => [
        'list' => 0,
        'detail' => 1,
        'page' => 2
    ],
    'option_code' => [
        'code_number' => 'Tăng dần theo số thứ tự',
        'code_prefix' => 'Tăng dần theo tiếp đầu ngữ'
    ]
];
