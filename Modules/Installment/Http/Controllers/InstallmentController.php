<?php

namespace Modules\Installment\Http\Controllers;

use App\Traits\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Modules\Installment\Entities\Installment;

class InstallmentController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = !empty($request->get('hidRecord')) ? $request->get('hidRecord') : Config("settings.perpage10");

        $installments = Installment::sortable();
        $active = Installment::getListActive();

        if (!empty($keyword)){
            $installments = $installments->where('title','LIKE',"%$keyword%");
        }

        $installments = $installments->with('tour','customer');

        $installments = $installments->sortable(['created_at' => 'desc'])->paginate($perPage);

        $totalInActive = Installment::where('active', config('settings.inactive'))->count();
        $totalActive = Installment::where('active', config('settings.active'))->count();

        return view('installment::installments.index', compact('installments', 'active', 'totalInActive', 'totalActive'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('installment::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $installment = Installment::with('tour', 'customer')->findOrFail($id);
        return view('installment::installments.show', compact('installment'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('installment::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
