<?php

return [
    'installment' => 'Trả góp',
    'customer' => 'Khách hàng',
    'tour' => 'Tên tour',
    'object_installment' => 'Đối tượng trả góp',
    'time_installment' => 'Thời gian trả góp',
    'full_name' => 'Họ và tên',
    'phone' => 'Số điện thoại',
    'email' => 'Email',
    'note' => 'Ghi chú',
    'active' => 'Xử lý',
    'creator' => 'Người xử lý',
    'updated_at' => 'Ngày cập nhật',
    'created_at' => 'Ngày tạo',
    'created_success' => 'Trả góp đã được thêm!',
    'updated_success' => 'Trả góp đã được cập nhật!',
    'deleted_success' => 'Trả góp đã được xóa!',
    'object_installment_options' => [
        1 => 'Đi làm hưởng lương',
        2 => 'Công tác trong các cơ quan nhà nước',
        3 => 'Sở hữu xe ô tô',
        4 => 'Kinh doanh'
    ],
    'time_installment_options' => [6,9,12]
];