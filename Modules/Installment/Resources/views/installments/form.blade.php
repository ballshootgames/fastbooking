<div class="box-body">
    @if ($errors->any())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <p><i class="fa fa-fw fa-check"></i> {{ $error }}</p>
            @endforeach
        </div>
    @endif

    <table class="table-layout table table-striped table-bordered">
        <tbody>
        <tr>
            <td>{{ trans('news::news.title') }} <span class="label-required"></span></td>
            <td>
                <div>
                    {!! Form::text('title', null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
                    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
                </div>
            </td>
        </tr>
        <tr>
            <td>{{ trans('news::news.index') }}</td>
            <td>
                <div>
                    {!! Form::number('arrange', null, ['class' => 'form-control input-sm']) !!}
                    {!! $errors->first('arrange', '<p class="help-block">:message</p>') !!}
                </div>
            </td>
        </tr>
        @if(isset($news))
            <tr>
                <td>{{ trans('news::news.slug') }}</td>
                <td>
                    <div>
                        {!! Form::text('slug', null, ['class' => 'form-control input-sm']) !!}
                        {!! $errors->first('slug', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
        @endif
        <tr>
            <td>{{ trans('news::news.news_type_id') }}</td>
            <td>
                <div>
                    {!! Form::select('news_type_id', $news_type, null, ['class' => 'form-control input-sm select2']) !!}
                    {!! $errors->first('news_type_id', '<p class="help-block">:message</p>') !!}
                </div>
            </td>
        </tr>
        <tr>
            <td>{{ trans('news::news.image') }}</td>
            <td>
                <div>
                    <div class="input-group inputfile-wrap ">
                        <input type="text" class="form-control input-sm" readonly>
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-danger btn-sm">
                                <i class=" fa fa-upload"></i>
                                {{ __('message.upload') }}
                            </button>
                            {!! Form::file('image', array_merge(['id'=>'image', 'class' => 'form-control input-sm', "accept" => "image/*"])) !!}
                        </div>
                        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="clearfix"></div>
                    <div class="imgprev-wrap" style="display:{{ !empty($news->image)?'block':'none' }}">
                        <input type="hidden" value="" name="img-hidden"/>
                        <img class="img-preview" src="{{ !empty($news->image)?asset(\Storage::url($news->image)):'' }}" alt="{{ trans('news::news.image') }}"/>
                        <i class="fa fa-trash text-danger"></i>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>{{ trans('news::news.description') }}</td>
            <td>
                <div>
                    {!! Form::textarea('description', null, ['class' => 'form-control input-sm']) !!}
                    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                </div>
            </td>
        </tr>
        <tr>
            <td>{{ trans('news::news.content') }}</td>
            <td>
                <div>
                    {!! Form::textarea('content', null, ['class' => 'form-control input-sm']) !!}
                    {!! $errors->first('content', '<p class="help-block">:message</p>') !!}
                </div>
            </td>
        </tr>
        <tr>
            <td>{{ trans('news::news.active') }}</td>
            <td>
                <div>
                    {!! Form::checkbox('active', 1, (isset($news) && $news->active===1)?true:false, ['class' => 'flat-blue', 'id' => 'active']) !!}
                </div>
            </td>
        </tr>
        </tbody>
    </table>
    <div style="margin-top: 10px;">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : __('message.save'), ['class' => 'btn btn-primary']) !!}
        <a href="{{ $prevUrl }}" class="btn btn-default">{{ __('message.cancel') }}</a>
        <input type="hidden" id="hidUrl" name="hidUrl" value={{ $prevUrl }}>
    </div>
</div>
@section('scripts-footer')
    <script type="text/javascript" src="{{ asset('plugins/ckeditor/ckeditor.js') }}" ></script>
    <script>
        CKEDITOR.replace( 'content', ckeditor_options);
    </script>
    <script type="text/javascript">
        $(function(){
            $('#image').change(function () {
                var preview = document.querySelector('img.img-preview');
                var file    = document.querySelector('#image').files[0];
                var reader  = new FileReader();
                console.log(preview + '-' + file + '-' + reader);

                if ( /\.(jpe?g|png|gif)$/i.test(file.name) ) {

                    reader.addEventListener("load", function () {
                        preview.src = reader.result;
                        $('.imgprev-wrap').css('display','block');
                        $('.inputfile-wrap').find('input[type=text]').val(file.name);
                    }, false);

                    if (file) {
                        reader.readAsDataURL(file);
                    }
                }else{
                    document.querySelector('#image').value = '';
                    $('.imgprev-wrap').find('input[type=hidden]').val('');
                }
            });

            $('.imgprev-wrap .fa-trash').click(function () {
                var preview = document.querySelector('img.img-preview');

                if(confirm('{{ __('message.confirm_delete') }}')){
                    //$('#avatar').val('').attr('required', 'required');
                    $('.imgprev-wrap').find('input[type=hidden]').val(1);
                    preview.src = '';
                    $('.imgprev-wrap').css('display','none');
                    $('.inputfile-wrap').find('input[type=text]').val('');
                }
            })
        });
    </script>
@endsection