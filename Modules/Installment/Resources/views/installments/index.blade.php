@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __('installment::installments.installment') }}
@endsection
@section('contentheader_title')
    {{ __('installment::installments.installment') }}
@endsection
@section('contentheader_description')

@endsection
@section('css')
    <style>
        .box-function{
            display: flex;
            justify-content: space-evenly;
        }
    </style>
@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li class="active">{{ __('installment::installments.installment') }}</li>
    </ol>
@endsection

@section('main-content')
    <div id="alert"></div>
    <div class="statis">
        <ul>
            <li>Có {{ !empty($totalInActive) ? $totalInActive : '0' }} {{ trans('installment::installments.installment') }} chưa được xử lý</li>
            <li>Có {{ !empty($totalActive) ? $totalActive : '0' }} {{ trans('installment::installments.installment') }} đã được xử lý</li>
        </ul>
    </div>
    <div class="box box-primary">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-offset-2 col-sm-10">
                    <div class="pull-right">
                        {!! Form::open(['method' => 'GET', 'url' => '/installments', 'class' => 'pull-left form-inline', 'role' => 'search', 'id' => 'search'])  !!}
                        <div class="input-group" style="width: 200px;">
                            <input type="text" value="{{\Request::get('search')}}" class="form-control input-sm" name="search" placeholder="{{ __('message.search_keyword') }}">
                            <span class="input-group-btn">
                                <button class="btn btn-default btn-sm" type="submit">
                                    <i class="fa fa-search"></i> {{ __('message.search') }}
                                </button>
                            </span>
                        </div>
                        <input type="hidden" name="hidRecord" value="{{ Request::has('record') ? Request::get('record') : config('settings.total_rec')[0] }}">
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        @php($index = ($installments->currentPage()-1)*$installments->perPage())
        <div class="box-body table-responsive no-padding">
            <table id="table-copy" class="table--layout table table-condensed table-bordered">
                <thead>
                <tr class="active">
                    <th class="text-center" style="width: 3.5%;">{{ trans('message.index') }}</th>
                    <th class="text-center" style="width: 2.5%;">
                        <input type="checkbox" name="chkAll" id="chkAll"/>
                    </th>
                    <th style="width: 10%;">@sortablelink('customer_id', trans('installment::installments.customer'))</th>
                    <th style="width: 43%;">@sortablelink('tour_id', trans('installment::installments.tour'))</th>
                    <th class="text-center" style="width: 10%;">{{ trans('installment::installments.time_installment') }}</th>
                    <th class="text-center" style="width: 8%;">@sortablelink('active', trans('installment::installments.active'))</th>
                    <th class="text-center" style="width: 8%;">@sortablelink('created_at', trans('message.created_at'))</th>
                    <th class="text-center" style="width: 8%;">{{ trans('installment::installments.creator') }}</th>
                    <th style="width: 8%;"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($installments as $item)
                    <tr>
                        <td class="text-center">{{ ++$index }}</td>
                        <td class="text-center">
                            <input type="checkbox" name="chkId" id="chkId" value="{{ $item->id }}" data-id="{{ $item->id }}"/>
                        </td>
                        <td>{{ optional($item->customer)->name }}</td>
                        <td class="text-title">{{ optional($item->tour)->name }}</td>
                        <td class="text-center">{{ $item->time_installment }} tháng</td>
                        <td class="text-center">{!! $item->active == config('settings.active') ? '<i class="fa fa-check text-primary"></i>' : '' !!}</td>
                        <td class="text-center">{{ \Carbon\Carbon::parse($item->created_at)->format(config('settings.format.date')) }}</td>
                        <td>{{ optional($item->user)->name }}</td>
                        <td class="text-center">
{{--                            @can('InstallmentController@show')--}}
                                <a href="{{ url('/installments/' . $item->id) }}" title="{{ __('message.view') }}"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
{{--                            @endcan--}}
{{--                            @can('NewsController@update')--}}
{{--                                <a href="{{ url('/news/' . $item->id . '/edit') }}" title="{{ __('message.edit') }}"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>--}}
{{--                            @endcan--}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="12" class="text-right">
                        Hiển thị
                        <select name="record" id="record">
                            @php($total_rec = config('settings.total_rec'))
                            @foreach($total_rec as $item)
                                <option value="{{$item}}" {{ Request::get('hidRecord') == $item ? 'selected' : '' }}>{{$item}}</option>
                            @endforeach
                        </select>
                        dòng/1 trang
                    </td>
                </tr>
                </tfoot>
            </table>
        </div>
        <div class="box-footer clearfix">
            <div class="row">
                <div id="btn-act" class="col-sm-6 text-left">
{{--                    @can('InstallmentController@destroy')--}}
                        <a href="#" id="delInstallmentItem" data-action="delInstallmentItem" class="btn-act btn btn-danger btn-sm" title="{{ __('message.delete') }}">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </a>
{{--                    @endcan--}}
{{--                    @can('InstallmentController@active')--}}
                        <a href="#" id="activeInstallmentItem" data-action="activeInstallmentItem" class="btn-act btn btn-primary btn-sm" title="{{ __('message.active') }}">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </a>
{{--                    @endcan--}}
                </div>
                <div class="col-sm-6 text-right">
                    {!! $installments->appends(\Request::except('page'))->render() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts-footer')
    <script type="text/javascript">
        $(function() {
            $('#chkAll').on('click', function () {
                $("input:checkbox").prop('checked', $(this).prop("checked"));
            });
        });
        $('#record').on('change', function () {
            let form = $('#search');
            let record = $(this).val();
            form.find('input[name="hidRecord"]').val(record);
            axios.get(form.attr('action')).then(function (res) {
                form.submit();
            }).catch(function () {
                alert('Có lỗi xảy ra vui lòng thử lại!')
            })
            return false;
        });

        $('#btn-act').on('click', '.btn-act', function(e){
            e.preventDefault();
            let action = $(this).data('action');
            ajaxListNews(action);
        });

        function ajaxListNews(action){
            let chkId = $("input[name='chkId']:checked");
            let actTxt = '', successAlert = '', classAlert = '';
            switch (action) {
                case 'activeInstallmentItem':
                    actTxt = 'duyệt';
                    successAlert = '{{ trans('installment::installments.updated_success') }}';
                    classAlert = 'alert-success';
                    break;
                case 'delInstallmentItem':
                    actTxt = 'xóa';
                    successAlert = '{{ trans('installment::installments.deleted_success') }}';
                    classAlert = 'alert-danger';
                    break;
            }
            if (chkId.length != 0){
                let notificationConfirm = 'Bạn có muốn '+actTxt+' hồ sơ này không?';
                let notification = confirm(notificationConfirm);
                if (notification){
                    var arrId = '';
                    $("input[name='chkId']:checked").map((val,key) => {
                        arrId += key.value + ',';
                    });
                    axios.get('{{url('/bookings/ajax')}}/'+action, {
                        params: {
                            ids: arrId
                        }
                    })
                        .then((response) => {
                            // console.log(response);
                            if (response.data.success === 'ok'){
                                $('#alert').html('<div class="alert '+classAlert+'">' +
                                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                                    successAlert +
                                    ' </div>');
                                {{--                                window.location = '{{ url('/bookings/customers') }}'--}}
                                location.reload(true);
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                        })
                }
            }else{
                let notificationAlert = 'Vui lòng chọn hồ sơ để '+actTxt+'!';
                alert(notificationAlert);
            }
        }
    </script>
@endsection

