@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __('installment::installments.installment') }}
@endsection
@section('contentheader_title')
    {{ __('installment::installments.installment') }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li><a href="{{ url('/installments') }}">{{ __('installment::installments.installment') }}</a></li>
        <li class="active">{{ __("message.detail") }}</li>
    </ol>
@endsection

@section('main-content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __("message.detail") }}</h3>
            <div class="box-tools">
                    <a href="{{ url('/installments') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.lists') }}</span></a>
{{--                @can('InstallmentController@destroy')--}}
                    {!! Form::open([
                        'method'=>'DELETE',
                        'url' => ['/installments', $installment->id],
                        'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> <span class="hidden-xs">'.__('message.delete').'</span>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-sm',
                            'title' => __('message.delete'),
                            'onclick'=>'return confirm("'.__('message.confirm_delete').'")'
                    ))!!}
                    {!! Form::close() !!}
{{--                @endcan--}}
            </div>
        </div>
        <div class="box-body table-responsive">
            <table class="table-layout table table-striped table-bordered">
                <tbody>
                    <tr>
                        <td> {{ trans('installment::installments.tour') }} </td>
                        <td> {{ optional($installment->tour)->name }} </td>
                    </tr>
                    <tr>
                        <td> {{ trans('installment::installments.customer') }} </td>
                        <td> {{ optional($installment->customer)->name }} </td>
                    </tr>
                    <tr>
                        <td> {{ trans('installment::installments.phone') }} </td>
                        <td> {{ optional($installment->customer)->phone }} </td>
                    </tr>
                    <tr>
                        <td> {{ trans('installment::installments.email') }} </td>
                        <td> {{ optional($installment->customer)->email }} </td>
                    </tr>
                    <tr>
                        <td> {{ trans('installment::installments.object_installment') }} </td>
                        <td> {{ trans('installment::installments.object_installment_options')[$installment->object_installment] }} </td>
                    </tr>
                    <tr>
                        <td> {{ trans('installment::installments.time_installment') }} </td>
                        <td> {{ $installment->time_installment }} tháng </td>
                    </tr>
                    <tr>
                        <td> {{ trans('installment::installments.note') }} </td>
                        <td> {{ nl2br($installment->note) }} </td>
                    </tr>
                    <tr>
                        <td> {{ trans('installment::installments.created_at') }} </td>
                        <td> {{ $installment->created_at }} </td>
                    </tr>
                    <tr>
                        <td> {{ trans('installment::installments.creator') }} </td>
                        <td> {{ optional($installment->user)->name }}</td>
                    </tr>
                    <tr>
                        <td> {{ trans('installment::installments.active') }} </td>
                        <td> {!! $installment->active===1?'<i class="fas fa-check text-primary"></i>' : '' !!} </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
@section('css')
    <style>
        .table-layout tr td:first-child{
            width: 22%;
        }
        .table-layout tr td:last-child{
            width: 78%;
        }
    </style>
@endsection