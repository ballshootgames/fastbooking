<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstallmentsTable extends Migration
{
    /**
     * Run the migrations.
     * Trả góp
     * @return void
     */
    public function up()
    {
        \Schema::create('installments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->unsigned()->nullable();
            $table->foreign('customer_id')
                ->references('id')
                ->on('customers')
                ->onDelete('set null');
            $table->integer('tour_id')->unsigned()->nullable();
            $table->foreign('tour_id')
                ->references('id')
                ->on('tours')
                ->onDelete('set null');
            $table->integer('creator_id')->unsigned()->nullable();
            $table->foreign('creator_id')
                ->references('id')
                ->on('users')
                ->onDelete('set null');
            $table->tinyInteger('object_installment')->default(1);//Đối tượng trả góp
            //1: Đi làm hưởng lương; 2: Công tác trong các cơ quan nhà nước; 3: Sở hữu xe ô tô; 4: Kinh doanh
            $table->tinyInteger('time_installment')->default(6);//Thời gian trả góp
            //6: 6 tháng; 9: 9 tháng; 12: 12 tháng
            $table->text('note');
            $table->tinyInteger('active')->default(0); //1: Đã xử lý; 0: Chưa xử lý

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::dropIfExists('installments');
    }
}
