<?php

namespace Modules\Installment\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Installment extends Model
{
    use Sortable, SoftDeletes;
    protected $dates = ['deleted_at'];
    public $sortable = [
        'id',
        'tour_id',
        'customer_id',
        'active',
        'created_at',
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'installments';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    protected $fillable = ['customer_id', 'tour_id', 'creator_id', 'object_installment', 'time_installment', 'note', 'active'];

    public function tour(){
        return $this->belongsTo('Modules\Tour\Entities\Tour');
    }
    public function user(){
        return $this->belongsTo('App\User', 'creator_id');
    }
    public function customer(){
        return $this->belongsTo('Modules\Booking\Entities\Customer');
    }

    public static function getListActive(){
        $arr = [1 => 'Hiển thị', 2 => 'Không hiển thị'];
        return $arr;
    }
}
