<?php

namespace Modules\Booking\Http\Controllers;

use App\Events\LogEvent;
use App\ModuleInfo;
use App\PaymentMethod;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Excel;
use Modules\Booking\Entities\Booking;
use Modules\Booking\Entities\BookingItem;
use Modules\Booking\Entities\BookingPeople;
use Modules\Booking\Entities\BookingProperty;
use Modules\Booking\Entities\BookingSource;
use Modules\Booking\Entities\Customer;
use Modules\Booking\Entities\Status;
use Modules\Booking\Events\BookingEvent;
use Modules\Booking\Exports\BookingsExport;
use Modules\Booking\Transformers\BookingResource;
use Modules\Booking\Transformers\CustomerResource;
use Modules\Tour\Entities\Tour;
use Illuminate\Support\Arr;

class BookingController extends Controller
{
    private $module;
    private $service;
    private $moduleName;
    /**
     * BookingController constructor.
     */
    public function __construct() {
        //***Check module***
        //1.get module
        $moduleInfo = new ModuleInfo(\Route::input('module'));
        //4.Initial params
        $this->module = $moduleInfo->getModule();
        $this->service = $moduleInfo->getBookingServiceInfo();
        $this->moduleName = $moduleInfo->getModuleName();
        \View::share('serviceInfo', $this->service);
    }

    /**
     * Check and save customer info
     * @param $custom_id
     * @param $customerInfo
     *
     * @return Customer
     */
    private function saveCustomer($custom_id, $customerInfo){
        //If exits customer_id: Đã có id khách hàng
        if($custom_id && $customer = Customer::find($custom_id)){
            $customer->update($customerInfo);
        }else{
            //Check if customer is empty (kiểm tra xem dữ liệu KH đã có hay ko có nhập)
            $isCustomerEmpty = true;
            foreach ($customerInfo as $key=>$value){
                if(trim($value) !== ''){
                    $isCustomerEmpty = false;
                    break;
                }
            }
            //If customer is not empty: save data
            if(!$isCustomerEmpty){
                //Find customer by phone or email: kiểm tra xem có KH trùng với số phone hoặc email không
                if ( !empty( trim( $customerInfo['phone'] ) ) || !empty( trim( $customerInfo['email'] ) ) ) {
                    $customer = new Customer();
                    if (!empty(trim($customerInfo['phone']))) $customer = $customer->where('phone', trim($customerInfo['phone']));
                    if (!empty(trim( $customerInfo['email']))) $customer = $customer->orWhere('email', trim($customerInfo['email']));
                    if($customer = $customer->first()){
                        //Nếu có KH trung email hoặc phone thì cập nhật lại thông tin KH - xóa đi các trường rỗng trc khi cập nhật
                        //remove empty data and updated customer
                        $customer->update(Arr::where($customerInfo, function($value, $key){
                            return trim($value) !== '';
                        }));
                    }else{
                        $customer = Customer::create($customerInfo);
                    }
                }else{
                    $customer = Customer::create($customerInfo);
                }
            }
        }

        return $customer;
    }

    /**
     * Cancel booking
     * @param $module
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function cancel($module, $id, Request $request ) {
        $this->validate($request, [
            'cancel_note' => 'nullable',
        ]);
        $booking = Booking::byRole()->findOrFail($id);//byRole($type)->
        $requestData = $request->only(['cancel_note']);
        $requestData['approved'] = config('settings.approved.cancel');
        $booking->update($requestData);

        event(new BookingEvent(config('settings.notification_type.booking_canceled'), $booking));
        event(new LogEvent('cancelled', $booking, $request));

        if($request->wantsJson()){
            return response()->json([
                'message' => __('booking::bookings.canceled'),
                'id' => $id
            ]);
        }

        \Session::flash('flash_message', __('booking::bookings.canceled'));

        return back();
    }

    public function autocompleteCustomer(Request $request){
        $this->validate($request, [
            'name' => 'required_without_all:phone,email',
            'phone' => 'required_without_all:name,email|max:20',
            'email' => 'required_without_all:phone,name',
        ]);
        $where = [];
        if(!empty($request->get('name'))){
            $where[] = ['name', 'LIKE', '%'.$request->get('name').'%'];
        }
        if(!empty($request->get('phone'))){
            $where[] = ['phone', 'LIKE', '%'.$request->get('phone').'%'];
        }
        if(!empty($request->get('email'))){
            $where[] = ['email', 'LIKE', '%'.$request->get('email').'%'];
        }
        if(count($where) > 0){
            $customers = Customer::where($where)->paginate(Config("settings.perpage"));
            return CustomerResource::collection($customers);
        }
        return response()->json([]);
    }

    /**
     * Display a listing of the resource.
     * @param $module
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index($module, Request $request)
    {
        $dateValidate = $request->wantsJson() ? 'date|date_format:Y-m-d' : 'date_format:"'.config('settings.format.date').'"';
        $this->validate($request, [
            'departure_date' => 'nullable|'.$dateValidate,
            'search' => 'nullable',
            'service_id' => 'nullable|integer',//tour or journey id
        ]);

        $keyword = $request->get('search');
        $perPage = !empty($request->get('hidRecord')) ? $request->get('hidRecord') : Config("settings.perpage10");

        $bookings = Booking::sortable();
//        $bookings = Booking::byRole();
        if (!empty($keyword)) {
            $bookings = $bookings->where('code', 'like', "%$keyword%")
            ->orWhereHas('customer', function ($query) use ($keyword) {
                $query->where('name', 'like', "%$keyword%")
                    ->orWhere('phone', 'like', "%$keyword%")
                    ->orWhere('email', 'like', "%$keyword%");
            });
        }
        //departure_date
        if (!empty($request->get('departure_date'))){
            $bookingPropertyId = BookingProperty::where([['key', '=', 'departure_date'], ['module', '=', $this->moduleName]])->value('id');
            $bookings = $bookings->whereHas('properties', function ($query) use ($request, $bookingPropertyId){
                $query->where('booking_property_id', $bookingPropertyId)
                    ->where('value', $request->get('departure_date'));
            });
        }

        if(!empty($request->get('service_id'))){
		    $module_table = $this->service['table'];
            $service_id = $request->get('service_id');
            $bookings = $bookings->whereHas('services', function ($query) use ($module_table, $service_id) {
                $query->withTrashed()->where($module_table.'.id', '=', $service_id);
            });
        }

        if (!empty($request->get('status_id'))){
            $bookings = $bookings->where('status_id', $request->get('status_id'));
        }

        if (!empty($request->get('booking_status'))){
            $bookings = $bookings->where('booking_status', $request->get('booking_status'));
        }

        if (!empty($request->get('manager_id'))){
            $bookings = $bookings->where('manager_id', $request->get('manager_id'));
        }

        if ($request->ajax()){
            $bookings = $bookings->paginate($perPage)->appends($request->except('page'));
            return $bookings->toHtml();
        }

        if($request->wantsJson()){
            $bookings = $bookings->paginate($perPage)->appends($request->except('page'));
            return BookingResource::collection($bookings)->hide(['creator','cancel_note','detail']);
        }
        $bookings = $bookings->sortable(['created_at' => 'desc'])->paginate($perPage);
        $bookings->load(['creator'=> function($query){
            $query->select('id','username','name')->withTrashed();
        }, 'customer', 'services', 'properties']);

        $list_services = $this->service['namespaceModel']::pluck('name', 'id');

        $managers = User::pluck('name','id');
        $managers->prepend("--".__('booking::bookings.manager')."--", '')->all();

	    // dat cho, da thanh toan, thanh toan 50%, da huy
        $totalBooked = Booking::where('status_id', config('booking.payment_status.booked'))->count();
	    $totalPaid = Booking::where('status_id', config('booking.payment_status.paid'))->count();
        $totalUnpaid = Booking::where('status_id', config('booking.payment_status.unpaid'))->count();
	    $totalCancel = Booking::where('status_id', config('booking.payment_status.canceled'))->count();
        return view('booking::bookings.index', compact('bookings', 'list_services', 'totalBooked', 'totalPaid', 'totalUnpaid', 'totalCancel', 'managers'));
    }

    /**
     * Show the form for creating a new resource.
     * @param $module
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create($module)
    {
        $booking = new Booking();
        $adult_number = 1;
        $child_number = 0;
        $baby_number = 0;
        $adult_price = 0;

        $list_services = $this->service['namespaceModel']::get();

        $services = [];
        foreach ($list_services as $item){
            //Kiểm tra limit
            if ($item->limit){
                $existLimitPeople = Booking::getExistPeopleLimitTour($item);
                if ($existLimitPeople === 0) continue;
            }
            if ($item->limit_end_date){
                if (strtotime($item->limit_end_date) < strtotime(date('Y-m-d'))) continue;
            }
            if ($item->departure_date){
                if (strtotime($item->departure_date) < strtotime(date('Y-m-d'))) continue;
            }
            $services[$item->id] = $item->prefix_code.$item->code .' -- '. $item->{$this->service['column_name']};
        }
        if(!$this->service['multiple']) $services = Arr::prepend($services, "--".$this->service['label']."--", '');

        $module_properties = BookingProperty::where('module', $this->moduleName)->get();

        $booking_properties = [];
        foreach ($module_properties as $key => $value){
            $booking_properties[] = [
                'key' => $value['key'],
                'name' => $value['name'],
                'type' => $value['type'],
                'data' => json_decode($value['data'], true),
                'module' => $this->moduleName
            ];
        }

        $bookingSources = BookingSource::pluck('name', 'id');
        $bookingSources->prepend(__('message.please_select'), '')->all();
        $statuses = Status::orderBy('arrange')->pluck('name', 'id');
        $payments = PaymentMethod::orderBy('arrange')->pluck('name', 'id');
        $payments->prepend(__('message.please_select'), '')->all();
        // luu ket qua search tu link search truoc day
        $prevUrl = \URL::previous();

        return view('booking::bookings.create', compact('booking', 'services', 'booking_properties', 'bookingSources', 'statuses', 'payments', 'adult_number', 'child_number', 'baby_number','adult_price', 'prevUrl'));
    }

    /**
     * Store a newly created resource in storage.
     * @param $module
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Throwable
     */
    public function store($module, Request $request)
    {
        $arr_vale =  BookingProperty::where('module', $this->moduleName)->pluck('data', 'key');
        $arr_key = $arr_data = $arr_val = [];
        foreach ($arr_vale as $key => $value){
            $tmp = json_decode($value, true);
            if (array_key_exists('validate', $tmp))
            {
                $arr_key[] = 'properties.'.$key;
                $arr_data[] = $tmp;
            }
        }
        foreach ($arr_data as $value){
            if (array_key_exists('validate', $value))
                $arr_val[] = $value['validate'];
        }
        $properties_val = array_combine($arr_key, $arr_val);
        $validateServices = $request->wantsJson() ? 'nullable' : 'required';
        $validate = [
            'customer.id' => 'nullable|integer',
            'customer.phone' => 'required|numeric|digits_between:7,13',
            'customer.name' => 'required',
            'customer.email' => 'nullable|email',
            'customer.gender' => 'nullable|in:0,1',
            'adult_number' => [
                'required_if:child_number,0',
                'integer',
                'min:0',
                'max:255',
                function($attribute, $value, $fail) use ($request) {
                    $people = intval($value) + intval($request->get('child_number'));
                    if ($people <= 0) {
                        return $fail(__('booking::bookings.validates.number_people_must_more_0'));
                    }
                }
            ],
            'child_number' => 'required_if:adult_number,0|integer|min:0|max:255',
            'baby_number' => 'nullable|integer|min:0|max:255',
            'status_id' => 'required|integer',
            'services.*' => $validateServices
        ];
        $validate = array_merge($validate, $properties_val);
        $this->validate($request, $validate);
        $requestData = $request->except($arr_key);
        $requestKey = $request->only($arr_key);
        $requestKey = $requestKey['properties'];

        $booking = new Booking();
        $config_services = $this->service;

        //Lấy danh sách các dịch vụ được chọn khi thêm mới Booking
        $list_services = [];
        if (empty(array_filter($requestData['services'])) && isset($requestData['service'])){
            if ($request->wantsJson()){
                $tour = Tour::create($requestData['service']);
                $list_services = [$tour];
            }
        }else{
            //Lấy các dịch vụ tương ứng trong model
            $list_services = $config_services['namespaceModel']::whereIn('id', $requestData['services'])->get();
        }

        //Lấy tổng người
        $adult_number = intval($requestData['adult_number'] ?? 1);
        $child_number = intval($requestData['child_number'] ?? 0);
        $baby_number = intval($requestData['baby_number'] ?? 0);
        $total_people = $adult_number + $child_number + $baby_number;
        $total_price = 0;
        $adult_price = 0;
        $child_price = 0;
        $baby_price = 0;

        //check services có multi hay ko
        if ($config_services['multiple'] == false){
            $true_service = $request->wantsJson() ? reset($list_services) : $list_services->first();
            //check limit
            if (!empty($true_service->limit)){
                $existLimitPeople = Booking::getExistPeopleLimitTour($true_service);
                if ($total_people > $existLimitPeople){
                    if($request->wantsJson()){
                        return response()->json([
                            'message' => __('tour::tours.error_limit')
                        ]);
                    }

                    \Session::flash('flash_message', __('tour::tours.error_limit'));
                    return redirect()->back();
                }
            }

            $adult_price = $true_service->price;
            $child_price = $true_service->price_children?$true_service->price_children:0;
            $baby_price = $true_service->price_baby?$true_service->price_baby:0;
            $total_price = ($adult_number * $adult_price) + ($child_number * $child_price) + ($baby_number * $baby_price);
        }

        \DB::transaction(function () use ($requestData, &$booking, $requestKey, $request, $config_services, $total_price, $list_services, $adult_number, $adult_price, $child_number, $child_price, $baby_number, $baby_price){
            //prepare data
            $service_price = null;
            if ($config_services['price'] != null){
                //Lấy giá các dịch vụ theo từng module
                $service_price = $config_services['price'];
            }

            $requestData['total_price'] = $total_price;

            //Customer
            $customerInfo = $requestData['customer'];
            $customer = $this->saveCustomer(!empty($customerInfo['id']) ? $customerInfo['id'] : null, $customerInfo);
            if(!empty($customer)) $requestData['customer_id'] = $customer->id;

            //Check VAT
            if (!isset($requestData['is_vat'])){
                $requestData['is_vat'] = 0;
            }

            //Save booking
            $booking = Booking::create($requestData);

            $bookingItems = [];
            $bookingItems[sizeof($bookingItems)] = BookingItem::create([
                'booking_id' => $booking->id,
                'name' => 'adult_number',
                'item_number' => $adult_number,
                'price' => $adult_price
            ]);
            if ($child_number > 0)
                $bookingItems[sizeof($bookingItems)] = BookingItem::create([
                    'booking_id' => $booking->id,
                    'name' => 'child_number',
                    'item_number' => $child_number,
                    'price' => $child_price
                ]);
            if ($baby_number > 0)
                $bookingItems[sizeof($bookingItems)] = BookingItem::create([
                    'booking_id' => $booking->id,
                    'name' => 'baby_number',
                    'item_number' => $baby_number,
                    'price' => $baby_price
                ]);
            $booking->bookingItems()->saveMany($bookingItems);

            //Kiểm tra
            $services = [];
            if (empty(array_filter($requestData['services'])) && isset($requestData['service'])){
                if ($request->wantsJson()){
                    $service_id = reset($list_services);
                    $services[$service_id->id] =  ['price' => 0];
                }
                //gọi dịch vụ
                if($services) $booking->services()->attach( $services );
            }else{
                if (!empty($service_price)){
                    foreach ( $requestData['services'] as $service_id ) {
                        $services[ $service_id ] = [ 'price' => !empty($config_services['namespaceModel']::find($service_id)->$service_price()) ? $config_services['namespaceModel']::find($service_id)->$service_price() : 0 ];
                    }
                }
                //gọi dịch vụ
                if($services) $booking->services()->attach( $services );
            }

            //save booking property values
            if ($booking) {
                if (!empty($requestKey)) {
                    //lặp lấy mảng key có giá trị
//                    foreach ($requestKey as $key => $value){
//                        $resKey[] = $key;
//                    }
                    $booking_properties = BookingProperty::where('module', $this->moduleName)->pluck('type', 'key');
                    foreach ($booking_properties as $key => $type) {
                        $bookingPropertyId = BookingProperty::where([['key', '=', $key], ['module', '=', $this->moduleName]])->value('id');
                        if (isset($requestKey[$key])){
                            if ($type === 'file' || $type === 'image' && $request->hasFile('properties.'.$key)) {
                                $file = $request->file('properties.'.$key);
                                $requestKey[$key] = BookingProperty::saveFile($file, $type);
                            }
                            $booking->properties()->attach([$bookingPropertyId => ['value' => $requestKey[$key]]]);
                        } else {
                            // kiem tra neu co thi xoa
                            $booking->properties()->attach([$bookingPropertyId => ['value' => '']]);
                        }
                    }
                }
            }
            event(new BookingEvent(config('settings.notification_type.booking_created'), $booking));
        });
        event(new LogEvent('created', $booking, $request));

        if($request->wantsJson()){
            return response()->json([
                'message' => __('booking::bookings.created'),
                'id' => $booking->id
            ]);
        }

        \Session::flash('flash_message', __('booking::bookings.created'));

        return redirect("bookings/$module");
    }

    /**
     * Show the specified resource.
     * @param $module
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|BookingResource
     */
    public function show($module, $id, Request $request)
    {
//        $booking = Booking::byRole()->with(['services', 'properties', 'status', 'source', 'peoples'])->findOrFail($id);
        $booking = Booking::with(['services', 'properties', 'status', 'source', 'peoples'])->findOrFail($id);
        $module_properties = BookingProperty::where('module', $this->moduleName)->get();
//        \Auth::user()->notifications()->where('data->booking->id', (int)$id )->get()->markAsRead();
        if($request->wantsJson()){
            return new BookingResource($booking);
        }
        // luu ket qua search tu link search truoc day
        $prevUrl = \URL::previous();
        // lay thong tin peoples
        $keyword = $request->get('search');
        $peoples = $booking->peoples();
        if (!empty($keyword)) {
            $peoples = $peoples->where('name', 'LIKE', "%$keyword%");
        }
        $peoples = $peoples->get();
        $compareDate = date_create_from_format("Y-m-d", (date('Y')-18).'-'.date('m').'-'.date('d'));
        $totalAdult = $booking->peoples()->where('birthday', '<=', $compareDate)->count();

        return view('booking::bookings.show', compact('booking', 'module', 'module_properties', 'prevUrl', 'peoples', 'totalAdult'));
    }
    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($module, $id, Request $request)
    {
//        $booking = Booking::byRole()->with(['services'])->findOrFail($id);
        $booking = Booking::with(['services'])->findOrFail($id);

        //Dùng id của dịch vụ lấy ra mảng các dịch vụ để thể hiện dữ liệu select khi EDIT
        $list_services = $booking->services->first();
        // gia tien
        $adult_price = $list_services->price;
        $child_price = $list_services->price_children;
        $baby_price = $list_services->price_baby;
        $select_arr = BookingProperty::getDataSelect($list_services->id, $this->moduleName);

        $services = $this->service['namespaceModel']::get();
        $services = $services->mapWithKeys(function ($item) {
            return [$item->id => $item->{$this->service['column_name']}];
        });
        $services->all();

        if(!$this->service['multiple']) $services->prepend("--".$this->service['label']."--", '')->all();

        $module_properties = BookingProperty::where('module', $this->moduleName)->get();
        $booking_properties = [];
        foreach ($module_properties as $key => $value){
            $booking_properties[] = [
                'key' => $value['key'],
                'name' => $value['name'],
                'type' => $value['type'],
                'data' => json_decode($value['data'], true),
                'module' => $this->moduleName
            ];
        }
        //xử lý mảng lại theo key => value
        $properties_value = $booking->properties()->pluck('value', 'key');
        //gán phía dưới với biến đã đc xử lý ở trên
        $booking->properties = $properties_value;

        $bookingSources = BookingSource::pluck('name', 'id');
        $bookingSources->prepend(__('message.please_select'), '')->all();
        $statuses = Status::orderBy('arrange', 'desc')->pluck('name', 'id');
        $payments = PaymentMethod::orderBy('arrange', 'desc')->pluck('name', 'id');
        $payments->prepend(__('message.please_select'), '')->all();
        $adult_number = $booking->getTotalAdult();
        $child_number = $booking->getTotalChildren();
        $baby_number = $booking->getTotalBaby();
        // luu ket qua search tu link search truoc day
        $prevUrl = '';
        if ($request['hidUrl'] != null && $request['hidUrl']!='')
            $prevUrl = $request['hidUrl'];
        else
            $prevUrl = \URL::previous();

        return view('booking::bookings.edit', compact('booking', 'services', 'select_arr', 'booking_properties', 'properties_value', 'bookingSources', 'statuses', 'payments', 'adult_number', 'child_number', 'baby_number', 'adult_price', 'child_price', 'baby_price', 'prevUrl'));
    }

    public function StoreBookingItems($newValue, $oldValue, $type, $bookID, $price)
    {
        if ($newValue !== 0)
        {
            if ($oldValue===0)
            {
                // add
                BookingItem::create([
                    'booking_id' => $bookID,
                    'name' => $type,
                    'item_number' => $newValue,
                    'price' => ($price ? $price : 0)
                ]);
            } else {
                //update
                $bookingItem = BookingItem::where('booking_id', $bookID)
                    ->where('name', $type)->get()->first();
                if($bookingItem)$bookingItem->update(['item_number'=>$newValue, 'price'=>($price ? $price : 0)]);
            }
        } else {
            if ($oldValue !== 0)
            {
                //delete
                BookingItem::where('booking_id', $bookID)->where('name', $type)->forceDelete();
            }
        }
    }

    /**
     * Update the specified resource in storage.
     * @param $module
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Throwable
     */
    public function update($module, $id, Request $request)
    {
        $arr_vale =  BookingProperty::where('module', $this->moduleName)->pluck('data', 'key');
        $arr_key = $arr_key_validate = $arr_data = $arr_val = [];

        foreach ($arr_vale as $key => $value){
            $tmp = json_decode($value, true);
            if (array_key_exists('validate', $tmp))
            {
                $arr_key_validate[] = 'properties.'.$key;
            }
            $arr_key[] = 'properties.'.$key;
            $arr_data[] = $tmp;
        }
        foreach ($arr_data as $value){
            if (array_key_exists('validate', $value))
                $arr_val[] = $value['validate'];
        }
        $properties_val = array_combine($arr_key_validate, $arr_val);
        $validate = [
            'customer.id' => 'nullable|integer',
            'customer.phone' => 'required|numeric|digits_between:7,13',
            'customer.name' => 'required',
            'customer.email' => 'nullable|email',
            'customer.gender' => 'nullable|in:0,1',
            'adult_number' => [
                'required_if:child_number,0',
                'integer',
                'min:0',
                'max:255',
                function($attribute, $value, $fail) use ($request) {
                    $people = intval($value) + intval($request->get('child_number'));
                    if ($people <= 0) {
                        return $fail(__('booking::bookings.validates.number_people_must_more_0'));
                    }
                }
            ],
            'child_number' => 'required_if:adult_number,0|integer|min:0|max:255',
            'baby_number' => 'nullable|integer|min:0|max:255',
            'status_id' => 'required|integer',
            'services.*' => 'required'
        ];
        $validate = array_merge($validate, $properties_val);
        $this->validate($request, $validate);
        $requestData = $request->except($arr_key);
        $requestKey = $request->only($arr_key);
        $requestKey = $requestKey['properties'];


//        $booking = Booking::byRole()->with(['services'])->findOrFail($id);
        $booking = Booking::with(['services'])->findOrFail($id);
        $config_services = $this->service;

        //Lấy các dịch vụ tương ứng trong model
        $list_services = $booking->services;

        //Lây tổng người cũ, nếu trạng thái là huỷ thì không tính vào số người cũ
        $old_total_people = $booking->status->id===4 ? 0 : $booking->getTotalAdult() + $booking->getTotalChildren() + $booking->getTotalBaby();

        //Lấy tổng người
        $adult_number = intval($requestData['adult_number'] ?? 1);
        $child_number = intval($requestData['child_number'] ?? 0);
        $baby_number = intval($requestData['baby_number'] ?? 0);
        $total_people = $adult_number + $child_number + $baby_number;
        $total_price = 0;
        $adult_price = 0;
        $child_price = 0;
        $baby_price = 0;

        //check service có multi hay ko
        if ($config_services['multiple'] == false){
            $true_service = $list_services->first();
            //check limit
            if (!empty($true_service->limit)){
                $existLimitPeople = Booking::getExistPeopleLimitTour($true_service);
                $people = $existLimitPeople + $old_total_people;
                if ($total_people > $people){
                    if($request->wantsJson()){
                        return response()->json([
                            'message' => __('tour::tours.error_limit')
                        ]);
                    }

                    \Session::flash('flash_message', __('tour::tours.error_limit'));
                    return redirect()->back();
                }
            }
            $adult_price = $true_service->price;
            $child_price = $true_service->price_children;
            $baby_price = $true_service->price_baby;
            $total_price = ($adult_number * $adult_price) + ($child_number * $child_price) + ($baby_number * $baby_price);
        }

        // cap nhat booking items neu co
        // lay list items, kiem tra neu nhu co them moi
        if ($adult_number !== $booking->getTotalAdult())
            $this->StoreBookingItems($adult_number, $booking->getTotalAdult(), "adult_number", $booking->id, $adult_price);

        if ($child_number !== $booking->getTotalChildren())
            $this->StoreBookingItems($child_number, $booking->getTotalChildren(), "child_number", $booking->id, $child_price);

        if ($baby_number !== $booking->getTotalBaby())
            $this->StoreBookingItems($baby_number, $booking->getTotalBaby(), "baby_number", $booking->id, $baby_price);

        \DB::transaction(function () use ($requestData, &$booking, $request, $requestKey, $config_services, $total_price, $list_services){
            //prepare data
            $service_price = null;
            if ($config_services['price'] != null){
                //Lấy giá các dịch vụ theo từng module
                $service_price = $config_services['price'];
            }
            $requestData['total_price'] = $total_price;

            //Customer
            $customerInfo = $requestData['customer'];
            $customer = $this->saveCustomer($booking->customer_id, $customerInfo);
            if(!empty($customer)) $requestData['customer_id'] = $customer->id;

            //Check VAT
            if (!isset($requestData['is_vat'])){
                $requestData['is_vat'] = 0;
            }
            //Save booking
            $booking->update($requestData);

            //Cập nhật BookingDetail
            if (!empty($booking->services) && !empty($service_price)){
                $services = [];
                foreach ( $booking->services->all() as $service ) {
                    $services[ $service->id ] = [ 'price' => $config_services['namespaceModel']::find($service->id)->$service_price() ];
                }
                //gọi dịch vụ
                if($services) $booking->services()->sync( $services);
            }

            if ($booking) {
                if (!empty($requestKey)) {
                    $booking_properties = BookingProperty::where('module', $this->moduleName)->pluck('type', 'key');
                    foreach ($booking_properties as $key => $type) {
                        $bookingPropertyId = BookingProperty::where([['key', '=', $key], ['module', '=', $this->moduleName]])->value('id');
                        if (isset($requestKey[$key])){
                            if ($type === 'file' || $type === 'image' && $request->hasFile('properties.'.$key)) {
                                $file = $request->file('properties.'.$key);
                                $requestKey[$key] = BookingProperty::saveFile($file, $type);
                            }
                            $booking->properties()->sync([$bookingPropertyId => ['value' => $requestKey[$key]]], false);
                        } else {
                            // kiem tra neu co thi xoa
                            $booking->properties()->sync([$bookingPropertyId => ['value' => '']], false);
                        }

                    }
                }
            }
            event(new BookingEvent(config('settings.notification_type.booking_updated'), $booking));
        });
        event(new LogEvent('updated', $booking, $request));

        if($request->wantsJson()){
            return response()->json([
                'message' => __('booking::bookings.updated'),
                'id' => $booking->id
            ]);
        }

        \Session::flash('flash_message', __('booking::bookings.updated'));
        return \Redirect::to($requestData['hidUrl']);
//        return redirect("bookings/$module");
    }

    /**
     * Remove the specified resource from storage.
     * @param $module
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($module, $id, Request $request)
    {
        $booking = Booking::byRole()->findOrFail($id);
        $booking->delete();

        event(new LogEvent('deleted', $booking, $request));

        if($request->wantsJson()){
            return response()->json([
                'message' => __('booking::bookings.deleted'),
                'id' => $booking->id
            ]);
        }

        \Session::flash('flash_message', __('booking::bookings.deleted'));

        return redirect('bookings/'.$module);
    }

    public function export(Request $request){
        $dateValidate = $request->wantsJson() ? 'date|date_format:Y-m-d' : 'date_format:"'.config('settings.format.date').'"';
        $this->validate($request, [
            'departure_date' => 'nullable|'.$dateValidate,
            'from' => 'nullable|date|date_format:Y-m-d',
            'to' => 'nullable|date|date_format:Y-m-d',
            'search' => 'nullable',
            'service_id' => 'nullable|integer',//tour or journey id
        ]);

        $bookings = new Booking();
        $bookings = $bookings->byRole();
        $keyword = $request->get('search');

        if (!empty($keyword)) {
            $bookings = $bookings->whereHas('customer', function ($query) use ($keyword) {
                $query->where('name', 'like', "%$keyword%")
                    ->orWhere('phone', 'like', "%$keyword%")
                    ->orWhere('email', 'like', "%$keyword%");
            });
        }
        //booking created by me
        if($request->has('me')){
            $bookings = $bookings->where('creator_id', \Auth::id());
        }
        //departure_date
        if (!empty($request->get('departure_date'))){
            $bookingPropertyId = BookingProperty::where([['key', '=', 'departure_date'], ['module', '=', $this->moduleName]])->value('id');
            $bookings = $bookings->whereHas('properties', function ($query) use ($request, $bookingPropertyId){
                $query->where('booking_property_id', $bookingPropertyId)
                    ->where('value', $request->get('departure_date'));// Carbon::createFromFormat(config('settings.format.date'), $request->get('departure_date'))->format('Y-m-d'));
            });
        }
        //date created
        if(!empty($request->get('from'))){
            $bookings = $bookings->where('created_at', '>=', $from = $request->get('from')." 00:00:00");
        }
        if(!empty($request->get('to'))){
            $bookings = $bookings->where('created_at', '<=', $request->get('to')." 23:59:59");
        }
        if(!empty($request->get('service_id'))){
            $module_table = $this->service['table'];
            $service_id = $request->get('service_id');
            $bookings = $bookings->whereHas('services', function ($query) use ($module_table, $service_id) {
                $query->withTrashed()->where($module_table.'.id', '=', $service_id);
            });
        }

        $bookings = $bookings->sortable(['updated_at' => 'desc'])->with(['creator:id,username,name', 'customer', 'detail', 'detail.bookingable'=> function($query){
            $query->withTrashed();
        }, 'properties']);

        return \Maatwebsite\Excel\Facades\Excel::download(new BookingsExport(\Route::input('module'), $bookings), \Route::input('module').'.xlsx');
    }
}
