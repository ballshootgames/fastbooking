<?php

namespace Modules\Booking\Http\Controllers;

use App\Company;
use App\ModuleInfo;
use Illuminate\Support\Facades\Auth;
use Modules\Booking\Entities\Booking;
use App\ChartJs;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Traits\Authorizable;
use App\Utils;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ReportsController extends Controller
{
    use Authorizable;

    private $list_services = null;
    private $service = null;
    private $route_module = null;
    private $list_company = null;
    private $report_company = null;
    /**
     * ReportsController constructor
     */
    public function __construct(Request $request)
    {
        $this->middleware(function ($request, $next) {
            if (!empty($request->route('module'))){
                $this->route_module = \Route::input('module');

                $moduleInfo = new ModuleInfo(\Route::input('module'));
                $this->service = $moduleInfo->getBookingServiceInfo();
                $this->list_services = $this->service['namespaceModel']::pluck('name', 'id');
            }
            if (!empty(Auth::user()->company_id)){
                $companyTree = Company::descendantsAndSelf(Auth::user()->company_id)->toTree();
                $listCompany = new Company();
                $this->list_company = $listCompany->getListCompanyInfoToArray($companyTree);

                \View::share(['listCompany' => $this->list_company, 'route_module' => $this->route_module]);
            }else{
                \View::share(['listCompany' => $this->list_company, 'route_module' => $this->route_module]);
            }
            if (!empty($request->get('report_company'))){
                $this->report_company = $request->get('report_company');
            }

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function numberBookingByDate(Request $request)
    {
        $list_services = $this->list_services;
        $service = $this->service;
        $dataBookingByDate = 'bookingByDate';

        $dataBooking = $this->bookingByDate(Booking::REPORT_TYPE['NUMBER'], $request, $service);
        $title = __('reports.number_booking_by_date');

        return view('booking::reports.booking_line', compact('dataBooking', 'dataBookingByDate','list_services', 'title'));
    }
    public function numberBookingByMonth(Request $request)
    {
        $list_services = $this->list_services;
        $service = $this->service;
        $dataBookingByMonth = 'bookingByMonth';

        $dataBooking = $this->bookingByMonth(Booking::REPORT_TYPE['NUMBER'], $request, $service);
        $title = __('reports.number_booking_by_date');
        if (isset($request->report_year)){
            $title .= ' ('.__('reports.year').' '.$request->report_year.')';
        }else{
            $title .= ' ('.__('reports.year').' '.date('Y').')';
        }

        return view('booking::reports.booking_line', compact('dataBooking', 'dataBookingByMonth','list_services', 'title'));
    }
    public function numberBookingByYear(Request $request)
    {
        $list_services = $this->list_services;
        $service = $this->service;
        $dataBookingByMonth = 'bookingByMonth';

        $dataBooking = $this->bookingByYear(Booking::REPORT_TYPE['NUMBER'], $request, $service);
        $title = __('reports.number_booking_by_date');
        if (isset($request->report_year)){
            $title .= ' ('.__('reports.year').' '.$request->report_year.')';
        }else{
            $title .= ' ('.__('reports.year').' '.date('Y').')';
        }

        return view('booking::reports.booking_line', compact('dataBooking', 'dataBookingByMonth','list_services', 'title'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function peopleBookingByDate(Request $request)
    {
        $list_services = $this->list_services;
        $service = $this->service;
        $dataBookingByDate = 'bookingByDate';

        $dataBooking = $this->bookingByDate(Booking::REPORT_TYPE['PEOPLE'], $request, $service);
        $title = __('reports.people_booking_by_date');

        return view('booking::reports.booking_line', compact('dataBooking', 'dataBookingByDate','list_services', 'title'));
    }
    public function peopleBookingByMonth(Request $request)
    {
        $list_services = $this->list_services;
        $service = $this->service;
        $dataBookingByMonth = 'bookingByMonth';

        $dataBooking = $this->bookingByMonth(Booking::REPORT_TYPE['PEOPLE'], $request, $service);
        $title = __('reports.people_booking_by_date');

        return view('booking::reports.booking_line', compact('dataBooking', 'dataBookingByMonth','list_services', 'title'));
    }
    public function peopleBookingByYear(Request $request)
    {
        $list_services = $this->list_services;
        $service = $this->service;
        $dataBookingByMonth = 'bookingByMonth';

        $dataBooking = $this->bookingByYear(Booking::REPORT_TYPE['PEOPLE'], $request, $service);
        $title = __('reports.people_booking_by_date');

        return view('booking::reports.booking_line', compact('dataBooking', 'dataBookingByMonth','list_services', 'title'));
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function financeBookingByDate(Request $request)
    {
        $list_services = $this->list_services;
        $service = $this->service;
        $dataBookingByDate = 'bookingByDate';

        $dataBooking = $this->bookingByDate(Booking::REPORT_TYPE['FINANCE'], $request, $service);
        $dataFinance = $this->financeByDate(Booking::REPORT_TYPE['FINANCE'], $request, $service);
        $title = __('reports.finance_booking_by_date');

        return view('booking::reports.booking_line', compact('dataBooking', 'dataFinance','dataBookingByDate','list_services' , 'title'));
    }
    public function financeBookingByMonth(Request $request)
    {
        $list_services = $this->list_services;
        $service = $this->service;
        $dataBookingByMonth = 'bookingByMonth';

        $dataBooking = $this->bookingByMonth(Booking::REPORT_TYPE['FINANCE'], $request, $service);
        $title = __('reports.finance_booking_by_date');

        return view('booking::reports.booking_line', compact('dataBooking', 'dataBookingByMonth','list_services' , 'title'));
    }
    public function financeBookingByYear(Request $request)
    {
        $list_services = $this->list_services;
        $service = $this->service;
        $dataBookingByMonth = 'bookingByMonth';

        $dataBooking = $this->bookingByYear(Booking::REPORT_TYPE['FINANCE'], $request, $service);
        $title = __('reports.finance_booking_by_date');

        return view('booking::reports.booking_line', compact('dataBooking', 'dataBookingByMonth','list_services' , 'title'));
    }

    /**
     * @param $reportType = number|people
     * @param Request $request
     *
     * @return array
     */
    private function bookingByDate($reportType , Request $request, $service){
        $this->validate($request,[
            'from' => 'nullable|date|date_format:Y-m-d',
            'to' => 'nullable|date|date_format:Y-m-d',
        ]);
        $from = $request->get('from');
        if(empty($request->get('from'))){
            $date30Ago = date('Y-m-d', strtotime('-29 days'));
            $from = $date30Ago;
        }
        $to = $request->get('to');
        if(empty($request->get('to'))) {
            $today = date( 'Y-m-d' );
            $to = $today ;
        }

        $type_date = config('booking.type_report.type_date');

        //chart data
        $dates = Utils::generateDateRange(Carbon::parse($from), Carbon::parse($to));
        $dataBooking = [
            'labels' => $dates,
            'datasets' => []
        ];
        //get data
        $dataBooking['datasets'][0] = [
            'label' => __('booking::bookings.services'),
            'borderColor' => ChartJs::COLOR['green'],
            'backgroundColor' => ChartJs::COLOR['green'],
            'fill' => false,
            'data' => []
        ];
        $bookByDate = Booking::reportBooking( $reportType, $type_date, $service['table'], $from, $to, null, $request->get('services_id' ), $request->get('company_id'), $this->route_module, $this->report_company);
        //prepare data chart
        $datesFormat = [];
        foreach ($dates as $date) {
            $datesFormat[] = Carbon::parse($date)->format(config('settings.format.date'));
            if(isset($dataBooking['datasets'])){
                $dataBooking['datasets'][0]['data'][] = ! empty( $bookByDate[ $date ] ) ? $bookByDate[ $date ] : 0;
            }
        }

        $dataBooking['labels'] = $datesFormat;
        return $dataBooking;
    }
    private function bookingByMonth($reportType , Request $request, $service){
        $this->validate($request,[
            'report_year' => 'nullable',
        ]);

        $fromYear = $request->get('report_year');
        if (empty($fromYear)){
            $fromYear = date('Y');
        }

        $type_date = config('booking.type_report.type_month');

        //chart data
        $dates = Utils::getMonth();

        $dataBooking = [
            'labels' => $dates,
            'datasets' => []
        ];
        //get data
        $dataBooking['datasets'][0] = [
            'label' => __('booking::bookings.services'),
            'borderColor' => ChartJs::COLOR['green'],
            'backgroundColor' => ChartJs::COLOR['green'],
            'fill' => false,
            'data' => []
        ];
        $bookByDate = Booking::reportBooking( $reportType, $type_date, $service['table'], null, null, $fromYear, $request->get('services_id' ), $request->get('company_id'), $this->route_module, $this->report_company);

        //prepare data chart
        $datesFormat = [];
        foreach ($dates as $date) {
            $datesFormat[] = 'Tháng '.$date;
            if(isset($dataBooking['datasets'])){
                $dataBooking['datasets'][0]['data'][] = ! empty( $bookByDate[ $date ] ) ? $bookByDate[ $date ] : 0;
            }
        }

        $dataBooking['labels'] = $datesFormat;
        return $dataBooking;
    }
    private function bookingByYear($reportType , Request $request, $service){
        $this->validate($request,[
            'report_year' => 'nullable',
        ]);

        $fromYear = $request->get('report_year');
        if (empty($fromYear)){
            $fromYear = date('Y');
        }

        $type_date = config('booking.type_report.type_year');

        //chart data
        $dates = Utils::getYear($fromYear);
        $dataBooking = [
            'labels' => $dates,
            'datasets' => []
        ];
        //get data
        $dataBooking['datasets'][0] = [
            'label' => __('booking::bookings.services'),
            'borderColor' => ChartJs::COLOR['green'],
            'backgroundColor' => ChartJs::COLOR['green'],
            'fill' => false,
            'data' => []
        ];
        $bookByDate = Booking::reportBooking( $reportType, $type_date, $service['table'], null, null, $fromYear, $request->get('services_id' ), $request->get('company_id'), $this->route_module, $this->report_company);

        //prepare data chart
        $datesFormat = [];
        foreach ($dates as $date) {
            $datesFormat[] = 'Năm '.$date;
            if(isset($dataBooking['datasets'])){
                $dataBooking['datasets'][0]['data'][] = ! empty( $bookByDate[ $date ] ) ? $bookByDate[ $date ] : 0;
            }
        }

        $dataBooking['labels'] = $datesFormat;
        return $dataBooking;
    }
    /*
     * Hàm lấy reports finance
     * */
    private function financeByDate($reportType , Request $request, $service){
        $this->validate($request,[
            'from' => 'nullable|date|date_format:Y-m-d',
            'to' => 'nullable|date|date_format:Y-m-d',
        ]);
        $from = $request->get('from');
        if(empty($request->get('from'))){
            $date30Ago = date('Y-m-d', strtotime('-29 days'));
            $from = $date30Ago;
        }
        $to = $request->get('to');
        if(empty($request->get('to'))) {
            $today = date( 'Y-m-d' );
            $to = $today ;
        }

        $type_date = config('booking.type_report.type_date');

        //chart data
        $dates = Utils::generateDateRange(Carbon::parse($from), Carbon::parse($to));
        $bookByDate = Booking::reportBooking( $reportType, $type_date, $service['table'], $from, $to, null, $request->get('services_id' ), $request->get('company_id'), $this->route_module, $this->report_company);
        $dataFinances = [];
        //prepare data chart
        $datesFormat = [];
        $data = [];
        foreach ($dates as $date) {
            $datesFormat[] = Carbon::parse($date)->format(config('settings.format.date'));
            $data[] = ! empty( $bookByDate[ $date ] ) ? $bookByDate[ $date ] : 0;
        }
        $dataFinances = array_combine($datesFormat,$data);
        return $dataFinances;
    }
}
