<?php
namespace Modules\Booking\Http\Controllers;

use App\ChartJs;
use App\City;
use App\Country;
use App\District;
use App\Http\Controllers\Controller;
use App\ModuleInfo;
use App\Ward;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Modules\Booking\Entities\Booking;
use Modules\Booking\Entities\BookingProperty;
use Modules\Booking\Entities\Customer;
use Modules\Booking\Entities\CustomerType;
use Modules\Tour\Entities\Tour;

class AjaxController extends Controller
{
	/**
	 * Gọi ajax: sẽ gọi đến hàm = tên $action
	 * @param Request $action
	 * @param Request $request
	 * @return mixed
	 */
	public function index($action, Request $request)
	{
		return $this->{$action}($request);
	}
	/*
	 * Find customer by phone number
	 */
	public function getCustomerByPhone(Request $request){
		$this->validate($request, [
			'phone' => 'required|numeric'
		]);
		return response()->json(\Modules\Booking\Entities\Customer::where('phone', 'like', "%$request->phone%")->orderBy('updated_at','desc')->limit(10)->get());
	}

	/**
	 * Find customer by email
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 * @throws \Illuminate\Validation\ValidationException
	 */
	public function getCustomerByEmail(Request $request){
		$this->validate($request, [
			'email' => 'required'
		]);
		return response()->json(\Modules\Booking\Entities\Customer::where('email', 'like', "%$request->email%")->orderBy('updated_at','desc')->limit(10)->get());
	}

	public function getDataService(Request $request){
        $data_arr = BookingProperty::getDataSelect($request->idServices, $request->moduleName);
	    return response()->json($data_arr);
    }

    public function setConfigProperties(Request $request){
	    $moduleName = $request->moduleName;
        $moduleProperties = new ModuleInfo($moduleName);
        $moduleProperties->configBookingProperties();
        return response()->json($moduleName);
    }

    //Show list cities, districts, wards
    public function getListCities(Request $request){
	    $country_id = $request->nationality_id;
	    $cities = City::where('country_id', $country_id)->pluck('name', 'id');
	    return response()->json($cities);
    }

    public function getListDistricts(Request $request){
        $city_id = $request->city_id;
        $districts = District::where('city_id',$city_id)->pluck('name','id');
        return response()->json($districts);
    }

    public function getListWards(Request $request){
	    $district_id = $request->district_id;
	    $awards = Ward::where('district_id',$district_id)->pluck('name','id');
	    return response()->json($awards);
    }

    public function checkServiceLimit(Request $request){
        $id_service = $request->service_id;
        $module = $request->module;
        $service = $module::find($id_service);
        if (empty($service->limit)){
            return response()->json(['limit' => null, 'departure_date' => $service->departure_date]);
        }else{
            $limit = Booking::getExistPeopleLimitTour($service);
            return response()->json(['limit' => $limit, 'departure_date' => $service->departure_date]);
        }
    }
    /*
     * Hiển thị doanh thu theo tháng
     * */
    public function getFinanceBookingByDate(){
        $html = ChartJs::dashboardLineCharBooking30DaysAgo();
        return $html;
    }

    public function getFilterFinanceBookingByDate(Request $request){
        $from = $request->get('from');
        $to = $request->get('to');
        $html = ChartJs::dashboardLineCharBooking30DaysAgo($from,$to);
        return $html;
    }

    //Delete Item
    public function delItem(Request $request){
        $ids = $request->ids;
        $arrId = explode(',', $ids,-1);
        $str = '';
        $success = 0;
        foreach ($arrId as $item){
            $customer = Customer::findOrFail($item);
            if (isset($customer->bookings) && count($customer->bookings) > 0)
            {
                $str .= ($str==='')?$customer->name:', '.$customer->name;
            } else
            {
                $success +=1;
                Customer::destroy($item);
            }
        }
        if ($success>0) return \response()->json(['success' => 'ok', 'fails' => $str]);
        else return \response()->json(['success' => 'false', 'fails' => $str]);
    }
}