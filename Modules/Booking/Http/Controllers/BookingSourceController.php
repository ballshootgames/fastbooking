<?php

namespace Modules\Booking\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Modules\Booking\Entities\BookingSource;

class BookingSourceController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = !empty($request->get('hidRecord')) ? $request->get('hidRecord') : Config("settings.perpage10");

        $sources = BookingSource::sortable(['arrange' => 'asc']);
        if (!empty($keyword)) {
            $sources = $sources->where('name', 'LIKE', "%$keyword%");
        }
        $sources = $sources->paginate($perPage);
        $totalInActive = BookingSource::where('active', config('settings.inactive'))->count();
        $totalActive = BookingSource::where('active', config('settings.active'))->count();

        return view('booking::booking-sources.index', compact('sources', 'totalInActive', 'totalActive'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $prevUrl = \URL::previous();
        return view('booking::booking-sources.create', compact('prevUrl'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'arrange' => 'nullable|numeric',
        ]);
        $requestData = $request->all();
        BookingSource::create($requestData);

        \Session::flash('flash_message', __('booking::booking_sources.created_success'));
        return redirect('bookings/booking-sources');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $source = BookingSource::findOrFail($id);
        $prevUrl = \URL::previous();

        return view('booking::booking-sources.show', compact('source', 'prevUrl'));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id, Request $request)
    {
        $source = BookingSource::findOrFail($id);
        $prevUrl = '';
        if ($request['hidUrl'] != null && $request['hidUrl']!='')
            $prevUrl = $request['hidUrl'];
        else
            $prevUrl = \URL::previous();

        return view('booking::booking-sources.edit', compact('source', 'prevUrl'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'arrange' => 'nullable|numeric'
        ]);

        $requestData = $request->all();
        $source = BookingSource::findOrFail($id);
        if (empty($requestData['active'])){
            $requestData['active'] = 0;
        }
        $source->update($requestData);

        \Session::flash('flash_message', __('booking::booking_sources.updated_success'));

        return \Redirect::to($requestData['hidUrl']);
//        return redirect('bookings/booking-sources');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        BookingSource::destroy($id);

        \Session::flash('flash_message', __('booking::booking_sources.deleted_success'));

        return redirect('bookings/booking-sources');
    }
}
