<?php

namespace Modules\Booking\Http\Controllers;

use App\Agent;
use App\City;
use App\Imports\BookingPeopleImport;
use App\ModuleInfo;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Booking\Entities\Booking;
use Modules\Booking\Entities\BookingPeople;
use Modules\Booking\Entities\BookingSource;
use Modules\Booking\Entities\CustomerType;
use Modules\Booking\Entities\Office;
use Modules\Booking\Entities\Status;
use Modules\Installment\Entities\Installment;
use Modules\News\Entities\News;
use Modules\News\Entities\NewsType;
use Modules\Theme\Entities\Menu;
use Modules\Tour\Entities\Supplier;
use mysql_xdevapi\Exception;

class AjaxBookingController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($action, Request $request)
    {
        return $this->{$action}($request);
    }
    /**
     * Get list agent by company id
     * action: getAgentsByCompanyID
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse|null
     */
    private function getAgents(Request $request){

        $agents = Agent::select('name','id')->get();

        return response()->json($agents);
    }

    /**
     * Get list service in view report
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse|null
     */
    public function getServiceReport(Request $request){
        if ($request->module === ''){
            return \response()->json();
        }
        $moduleInfo = new ModuleInfo($request->module);
        $service = $moduleInfo->getBookingServiceInfo();
        $list_services = $service['namespaceModel']::pluck('name','id');
        return \response()->json($list_services);
    }
    /*List Countries*/
    public function getListCountries(Request $request){
        $country_id = $request->country_id;
        $cities = City::where('country_id', $country_id)->pluck('name', 'id');
        return \response()->json($cities);
    }
    /*AutoComplete City*/
    public function getAutoCompleteCity(Request $request){
        return \response()->json(City::where('name','LIKE',"%$request->city%")->orderBy('updated_at','desc')->get());
    }
    public function checkServiceLimit(Request $request){
        $id_service = $request->service_id;
        $module = $request->module;
        $service = $module::find($id_service);
        if (empty($service->limit)){
            return response()->json(['limit' => null, 'departure_date' => $service->departure_date, 'adult_price' => $service->price, 'child_price' => $service->price_children, 'baby_price' => $service->price_baby]);
        }else{
            $limit = Booking::getExistPeopleLimitTour($service);
            return response()->json(['limit' => $limit, 'departure_date' => $service->departure_date, 'adult_price' => $service->price, 'child_price' => $service->price_children, 'baby_price' => $service->price_baby]);
        }
    }

    //Delete Item
    public function delItem(Request $request){
        $ids = $request->ids;
        $arrId = explode(',', $ids,-1);
        foreach ($arrId as $item){
            Booking::destroy($item);
        }
        return \response()->json(['success' => 'ok']);
    }

    //Delete Property Items
    public function delStatusItem(Request $request){
        $ids = $request->ids;
        $arrId = explode(',', $ids,-1);
        foreach ($arrId as $item){
            $status = Status::findOrFail($item);

            $status::destroy($item);
        }
        return \response()->json(['success' => 'ok']);
    }
    //Active Property Items
    public function activeStatusItem(Request $request){
        $ids = $request->ids;
        $arrId = explode(',', $ids,-1);
        foreach ($arrId as $item){
            $status = Status::findOrFail($item);
            $active = $status->active == config('settings.active') ? config('settings.inactive') : config('settings.active');
            \DB::table('statuses')->where('id',$status->id)->update(['active' => $active, 'updator_id' => \Auth::user()->id]);
        }
        return \response()->json(['success' => 'ok']);
    }

    //Delete Property Items
    public function delBookingSourceItem(Request $request){
        $ids = $request->ids;
        $arrId = explode(',', $ids,-1);
        foreach ($arrId as $item){
            $bookingSource = BookingSource::findOrFail($item);

            $bookingSource::destroy($item);
        }
        return \response()->json(['success' => 'ok']);
    }
    //Active Property Items
    public function activeBookingSourceItem(Request $request){
        $ids = $request->ids;
        $arrId = explode(',', $ids,-1);
        foreach ($arrId as $item){
            $bookingSource = BookingSource::findOrFail($item);
            $active = $bookingSource->active == config('settings.active') ? config('settings.inactive') : config('settings.active');
            \DB::table('booking_source')->where('id', $bookingSource->id)->update(['active' => $active, 'updator_id' => \Auth::user()->id]);
        }
        return \response()->json(['success' => 'ok']);
    }

    //Delete Property Items
    public function delCustomerTypeItem(Request $request){
        $ids = $request->ids;
        $arrId = explode(',', $ids,-1);
        foreach ($arrId as $item){
            $customerType = CustomerType::findOrFail($item);

            $customerType::destroy($item);
        }
        return \response()->json(['success' => 'ok']);
    }
    //Active Property Items
    public function activeCustomerTypeItem(Request $request){
        $ids = $request->ids;
        $arrId = explode(',', $ids,-1);
        foreach ($arrId as $item){
            $customerType = CustomerType::findOrFail($item);
            $active = $customerType->active == config('settings.active') ? config('settings.inactive') : config('settings.active');
            \DB::table('customer_types')->where('id', $customerType->id)->update(['active' => $active, 'updator_id' => \Auth::user()->id]);
        }
        return \response()->json(['success' => 'ok']);
    }

    //Delete Property Items
    public function delNewsTypeItem(Request $request){
        $ids = $request->ids;
        $arrId = explode(',', $ids,-1);
        foreach ($arrId as $item){
            $newsType = NewsType::findOrFail($item);
            Menu::where('slug',$newsType->slug)->delete();
            $newsType::destroy($item);
        }
        return \response()->json(['success' => 'ok']);
    }
    //Active Property Items
    public function activeNewsTypeItem(Request $request){
        $ids = $request->ids;
        $arrId = explode(',', $ids,-1);
        foreach ($arrId as $item){
            $newsType = NewsType::findOrFail($item);
            $active = $newsType->active == config('settings.active') ? config('settings.inactive') : config('settings.active');
            \DB::table('news_type')->where('id', $newsType->id)->update(['active' => $active, 'updator_id' => \Auth::user()->id]);
        }
        return \response()->json(['success' => 'ok']);
    }

    //Delete Property Items
    public function delNewsItem(Request $request){
        $ids = $request->ids;
        $arrId = explode(',', $ids,-1);
        foreach ($arrId as $item){
            News::destroy($item);
        }
        return \response()->json(['success' => 'ok']);
    }
    //Active Property Items
    public function activeNewsItem(Request $request){
        $ids = $request->ids;
        $arrId = explode(',', $ids,-1);
        foreach ($arrId as $item){
            $news = News::findOrFail($item);
            $active = $news->active == config('settings.active') ? config('settings.inactive') : config('settings.active');
            \DB::table('news')->where('id', $news->id)->update(['active' => $active, 'updator_id' => \Auth::user()->id]);
        }
        return \response()->json(['success' => 'ok']);
    }

    //Delete Property Items Installment
    public function delInstallmentItem(Request $request){
        $ids = $request->ids;
        $arrId = explode(',', $ids,-1);
        foreach ($arrId as $item){
            $news = Installment::findOrFail($item);
            $news::destroy($item);
        }
        return \response()->json(['success' => 'ok']);
    }
    //Active Property Items Installment
    public function activeInstallmentItem(Request $request){
        $ids = $request->ids;
        $arrId = explode(',', $ids,-1);
        foreach ($arrId as $item){
            $news = Installment::findOrFail($item);
            $active = $news->active == config('settings.active') ? config('settings.inactive') : config('settings.active');
            \DB::table('installments')->where('id', $news->id)->update(['active' => $active, 'creator_id' => \Auth::user()->id]);
        }
        return \response()->json(['success' => 'ok']);
    }

    //Delete Property Items
    public function delPeoplesItem(Request $request){
        $ids = $request->ids;
        $arrId = explode(',', $ids,-1);
        foreach ($arrId as $item){
            $people = BookingPeople::findOrFail($item);

            $people->forceDelete();
        }
        \Session::put('bookings_passenger', "Select");
        return \response()->json(['success' => 'ok']);
    }

    //Delete Property Items
    public function editPeople(Request $request){
        $id = $request->id;
        $people = BookingPeople::findOrFail($id);
        return \response()->json(['success' => 'ok', 'data' => $people]);
    }

    //Delete Property Items
    public function addPeople(Request $request){
        $validator = \Validator::make($request->all(), [
            'name' => 'required',
            'birthday' => 'required',
            'id_number' => 'nullable|numeric',
            'gender' => 'nullable|in:0,1',
            'phone' => 'nullable|numeric|digits_between:7,13'
        ], [
            'name.required' => 'Tên khách hàng không được bỏ trống.',
            'birthday.required' => 'Ngày sinh không được bỏ trống.',
            'id_number.numeric' => 'CMND phải là kiểu số.',
            'phone.digits_between' => 'Độ dài trường số điện thoại phải nằm trong khoảng 7 đến 13 chữ số',
            'phone.numeric' => 'Điện thoại phải nhập đúng định dạng.'
        ]);
        if ($validator->passes()){
            $people = new BookingPeople();
            if (!empty($request->birthday)){
                $request->birthday = Carbon::createFromFormat(config('settings.format.date'),$request->birthday)->format('Y-m-d');
            }
            $people->name = $request->name;
            $people->birthday = $request->birthday;
            $people->id_number = $request->id_number;
            $people->picking_place = $request->picking_place;
            $people->gender = $request->gender;
            $people->phone = $request->phone;
            $people->booking_id = $request->bookid;
            $people->save();

            \Session::put('bookings_passenger', "Select");

            return response()->json([
                'success' => 'ok'
            ]);
        }
        return response()->json(['errors'=>$validator->errors()->all()]);
    }

    public function updatePeople(Request $request){
        $validator = \Validator::make($request->all(), [
            'name' => 'required',
            'birthday' => 'required',
            'id_number' => 'nullable|numeric',
            'gender' => 'nullable|in:0,1',
            'phone' => 'nullable|numeric|digits_between:7,13'
        ], [
            'name.required' => 'Tên khách hàng không được bỏ trống.',
            'birthday.required' => 'Ngày sinh không được bỏ trống.',
            'id_number.numeric' => 'CMND phải là kiểu số.',
            'phone.digits_between' => 'Độ dài trường số điện thoại phải nằm trong khoảng 7 đến 13 chữ số',
            'phone.numeric' => 'Điện thoại phải nhập đúng định dạng.'
        ]);
        if ($validator->passes()){
            $id = $request->id;
            $people = BookingPeople::findOrFail($id);
            if (!empty($request->birthday)){
                $request->birthday = Carbon::createFromFormat(config('settings.format.date'),$request->birthday)->format('Y-m-d');
            }
            $people->name = $request->name;
            $people->birthday = $request->birthday;
            $people->id_number = $request->id_number;
            $people->picking_place = $request->picking_place;
            $people->gender = $request->gender;
            $people->phone = $request->phone;
            $people->save();

            \Session::put('bookings_passenger', "Select");

            return response()->json([
                'success' => 'ok'
            ]);
        }
        return response()->json(['errors'=>$validator->errors()->all()]);
    }

    public function importItem(Request $request)
    {
        //luu session cho tab doan
        $hidUrl = $request->hidUrl;
        $bookid = $request->bookid;
        $booking = Booking::findOrFail($bookid);
        $peoples = $booking->peoples()->get();
        \Session::put('bookings_passenger', "Select");
        return view('booking::peoples.import-peoples', compact('hidUrl', 'peoples', 'bookid'));
    }

    public function importExcel(Request $request)
    {
        $action = $request->hidSubmit;
        return $this->{$action}($request);
    }

    public function loadExcel(Request $request)
    {
        //luu session cho tab doan
        $hidUrl = $request->hidUrl;
        $bookid = $request->bookid;
        $peoples = [];
        if (\Session::has('bookings_passenger')===false)
            \Session::put('bookings_passenger', "Select");

        $validator = \Validator::make($request->all(), [
            'attachment' => 'required'
        ], [
            'attachment.required' => 'Xin vui lòng chọn file excel để nhập.'
        ]);
        if ($validator->passes()){
            $file = $request->file('attachment');
            $destinationPath = 'uploads/imports';
            $file->move($destinationPath,$file->getClientOriginalName());
            $path = base_path().'/public/'.$destinationPath.'/'.$file->getClientOriginalName();
            $collection = (new BookingPeopleImport)->toCollection($path);
            if($collection != null && count($collection) > 0){
                $rows = $collection[0];
                if($rows != null && count($rows) > 0)
                {
                    for($i = 2; $i < count($rows);$i++){
                        $rowDataExcel = $rows[$i];
                        if(!$this->IsNullOrEmptyString($rowDataExcel[1])){
                            $rowData = $this->fetchRowData($rowDataExcel[1],$rowDataExcel[2],
                                $rowDataExcel[3],$rowDataExcel[4],$rowDataExcel[5],$rowDataExcel[6]);
                            $people = new BookingPeople();
                            $people->name = $rowData['name'];
                            $people->birthday = $rowData['birthday'];
                            $people->id_number = $rowData['id_number'];
                            $people->picking_place = $rowData['picking_place'];
                            $people->gender = $rowData['gender'];
                            $people->phone = $rowData['phone'];
                            $peoples[] = $people;
                        }
                    }
                }
            }

            \Session::put('peoples', $peoples);

            return view('booking::peoples.import-peoples', compact('hidUrl', 'peoples', 'bookid'));
        }
        return back()->with('error','Lỗi import excel, xin vui lòng xem file mẫu.');
    }

    public function saveExcel(Request $request)
    {
        $hidUrl = $request->hidUrl;
        $bookid = $request->bookid;
        $peoples = \Session::get('peoples');
        $fail_items = 0;
        if (\Session::has('bookings_passenger')===false)
            \Session::put('bookings_passenger', "Select");
        if (isset($peoples)&&count($peoples)>0)
        {
            // luu danh sach people cu, de sau khi them thanh cong thi xoa.
            $tmpPeoples = Booking::findOrFail($bookid)->peoples;
            try
            {
                foreach ($peoples as $item)
                {
                    $item->gender= ($item->gender==='Nam') ? 1 : 0;
                    $validator = \Validator::make($item->toArray(), [
                        'name' => 'required',
                        'birthday' => 'required',
                        'id_number' => 'nullable|numeric',
                        'gender' => 'nullable|in:0,1',
                        'phone' => 'nullable|numeric|digits_between:7,13'
                    ], [
                        'name.required' => 'Tên khách hàng không được bỏ trống.',
                        'birthday.required' => 'Ngày sinh không được bỏ trống.',
                        'id_number.numeric' => 'CMND phải là kiểu số.',
                        'phone.digits_between' => 'Độ dài trường số điện thoại phải nằm trong khoảng 7 đến 13 chữ số',
                        'phone.numeric' => 'Điện thoại phải nhập đúng định dạng.'
                    ]);
                    if ($validator->passes()){
                        if (!empty($item->birthday)){
//                        $item->birthday = Carbon::createFromFormat(config('settings.format.date'),$request->birthday)->format('Y-m-d');
                            $item->birthday = Carbon::createFromDate($item->birthday, 1, 1)->format('Y-m-d');
                        }
                        $item->booking_id = $bookid;
                        $item->save();
                    }
                    if (count($validator->errors()->all())>0) $fail_items++;
                }
                foreach ($tmpPeoples as $people)
                {
                    $p = BookingPeople::findOrFail($people->id);

                    $p->forceDelete();
                }
            }catch (\Exception $ex)
            {

            }
        }
        return redirect()->to($hidUrl)->with('fail_items', $fail_items);
    }

    function fetchRowData($full_name,$birthday,$id_number,$picking_place,$gender,$phone){
        return [
            'name' => $full_name,
            'birthday' => trim($birthday),
            'id_number' => trim($id_number),
            'picking_place' => trim($picking_place),
            'gender'=>trim($gender),
            'phone'=>trim($phone)
        ];
    }

    private function IsNullOrEmptyString($str){
        return (!isset($str) || trim($str) === '');
    }

    public function downloadTemplateExcel()
    {
        $file = public_path().'/uploads/imports/filemau.xlsx';
        $headers = array(
            'Content-Type: application/xlsx',
        );

        return response()->download($file, 'filemau.xlsx', $headers);
    }

    public function loadManagerModal(Request $request)
    {
        $bookId = $request->id;
        $managerId = $request->manager_id;
        $paymentMethodId = $request->payment_method_id;
        $managers = User::pluck('name','id');
        $managers->prepend(__('message.please_select'), '')->all();
        return view('booking::bookings.manager', compact('bookId', 'managers', 'managerId', 'paymentMethodId'));
    }

    public function saveManager(Request $request)
    {
        $id = $request->bookId;
        $book = Booking::findOrFail($id);
        if ($book->manager_id === null){
            $book->manager_id = $request->managerId;
            if ($book->status_id != config('booking.payment_status.pay'))
                $book->status_id = $request->paymentMethodId == 1 ? config('booking.payment_status.unpaid') : config('booking.payment_status.paid');
        }else if ($request->managerId!==$book->manager_id){
            $book->manager_id = $request->managerId;
            if ($book->status_id != config('booking.payment_status.pay'))
                $book->status_id = $request->paymentMethodId == 1 ? config('booking.payment_status.unpaid') : config('booking.payment_status.paid');
        }

        $book->update();

        return response()->json([
            'success' => 'ok'
        ]);
    }

    //delOfficeItem
    public function delOfficeItem(Request $request){
        $ids = $request->ids;
        $arrId = explode(',', $ids,-1);
        foreach ($arrId as $item){
            Office::destroy($item);
        }
        return \response()->json(['success' => 'ok']);
    }
    //activeOfficeItem
    public function activeOfficeItem(Request $request){
        $ids = $request->ids;
        $arrId = explode(',', $ids,-1);
        foreach ($arrId as $item){
            $office = Office::findOrFail($item);
            $active = $office->active == config('settings.active') ? config('settings.inactive') : config('settings.active');
            \DB::table('offices')->where('id', $office->id)->update(['active' => $active]);
        }
        return \response()->json(['success' => 'ok']);
    }

    //delSupplierItem
    public function delSupplierItem(Request $request){
        $ids = $request->ids;
        $arrId = explode(',', $ids,-1);
        foreach ($arrId as $item){
            Supplier::destroy($item);
        }
        return \response()->json(['success' => 'ok']);
    }

}
