<?php

namespace Modules\Booking\Http\Controllers;

use App\City;
use App\Country;
use App\District;
use App\Exports\CustomersExport;
use App\Http\Controllers\Controller;

use App\Imports\CustomersImport;
use App\Ward;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Booking\Entities\Customer;
use App\Traits\Authorizable;
use Illuminate\Http\Request;
use Modules\Booking\Entities\CustomerType;
use Modules\Booking\Entities\Office;
use Session;
use Illuminate\Support\Arr;

class CustomersController extends Controller
{
    use Sortable, SoftDeletes;
//	use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
//        $perPage = Config("settings.perpage");
        $perPage = !empty($request->get('hidRecord')) ? $request->get('hidRecord') : Config("settings.perpage10");

	    $customers = Customer::sortable(['created_at' => 'desc']);
        if (!empty($keyword)) {
            $customers = $customers->where('name', 'LIKE', "%$keyword%")
                ->orWhere('code','LIKE',"%$keyword%")
				->orWhere('email', 'LIKE', "%$keyword%")
				->orWhere('phone', 'LIKE', "%$keyword%")
				->orWhere('address', 'LIKE', "%$keyword%");
        }
	    $customers = $customers->paginate($perPage);

        return view('booking::customers.index', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $code = Customer::autoCode();
        $customerTypes = CustomerType::pluck('name','id');
        $customerTypes->prepend(__('message.please_select'), '')->all();
        $nationality = Country::pluck('name','id');
        $nationality->prepend(__('message.please_select'), '')->all();
        $cities = ['' => __('message.please_select')];
        $districts = ['' => __('message.please_select')];
        $wards = ['' => __('message.please_select')];
        $prevUrl = \URL::previous();
        $offices = Office::pluck('name','id');
        $offices->prepend(__('message.please_select'), '')->all();
        return view('booking::customers.create', compact('customerTypes',
            'cities','districts', 'wards','nationality', 'code', 'prevUrl', 'offices'));
    }

	/**
	 * Store a newly created resource in storage.
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 * @throws \Illuminate\Validation\ValidationException
	 */
    public function store(Request $request)
    {
        $this->validate($request, [
            'code' => 'nullable|unique:customers',
			'name' => 'required',
            'gender' => 'nullable|in:0,1',
	        'email' => 'required|email|unique:customers',
            'phone' => 'required|numeric|digits_between:7,13|unique:customers',
            'cmnd' => 'nullable|numeric',
            'file' => 'nullable|mimes:doc,pdf,xls,xlsx,docx'
		]);
        $requestData = $request->all();

        \DB::transaction(function () use ($request, $requestData) {
            if (!empty($request->cmnd_date))
                $requestData['cmnd_date'] = \DateTime::createFromFormat(config('settings.format.date'), $request->cmnd_date)->format('Y-m-d');
            if ($request->hasFile('file')){
                $requestData['file'] = Customer::uploadFile($request->file('file'));
            }
            Customer::create($requestData);
        });

        Session::flash('flash_message', __('booking::customers.customer_added'));

        return redirect('bookings/customers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id, Request $request)
    {
        $customer = Customer::with(['bookings'])->findOrFail($id);

        $customerType = $customer->customerType ? $customer->customerType->name : '';

        $nationality = $customer->nationality ? $customer->nationality->name : '';

        // lazy eagle loading bookings list
        $perPage = !empty($request->get('hidRecord')) ? $request->get('hidRecord') : Config("settings.perpage10");
        $bookings = $customer->bookings()->sortable(['create_at' => 'desc'])->paginate($perPage);
        $prevUrl = \URL::previous();

        return view('booking::customers.show', compact('customer', 'customerType', 'nationality', 'bookings', 'prevUrl'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id, Request $request)
    {
        $customer = Customer::findOrFail($id);
        $cmnd_date ="";
        if ($customer->cmnd_date && !empty($customer->cmnd_date)){
            $cmnd_date = Carbon::parse($customer->cmnd_date)->format(config('settings.format.date'));
        }
        if (empty($customer->code)){
            $code = $customer->id < 10 ? 'KH0' . $customer->id : 'KH' . $customer->id;
        } else
        {
            $code = $customer->code;
        }
        $customerTypes = CustomerType::pluck('name','id');
        $customerTypes->prepend(__('message.please_select'), '')->all();

        $nationality = Country::pluck('name','id');
        $nationality->prepend(__('message.please_select'), '')->all();

        $nationality_id = optional(optional(optional(optional($customer->ward)->district)->city)->country)->id;
        $cities = City::where('country_id', $nationality_id)->pluck('name','id');
        $cities->prepend(__('message.please_select'), '')->all();

        $city_id = optional(optional(optional($customer->ward)->district)->city)->id;
        $districts = District::where('city_id', $city_id)->pluck('name','id');
        $districts->prepend(__('message.please_select'), '')->all();

        $district_id = optional(optional($customer->ward)->district)->id;
        $wards = Ward::where('district_id', $district_id)->pluck('name', 'id');
        $wards->prepend(__('message.please_select'), '')->all();
        $offices = Office::pluck('name','id');
        $offices->prepend(__('message.please_select'), '')->all();
        $prevUrl = '';
        if ($request['hidUrl'] != null && $request['hidUrl']!='')
            $prevUrl = $request['hidUrl'];
        else
            $prevUrl = \URL::previous();

        return view('booking::customers.edit', compact('customer','code', 'districts', 'offices',
            'district_id', 'wards','customerTypes', 'cities', 'city_id','nationality', 'cmnd_date', 'prevUrl'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
	        'name' => 'required',
	        'phone' => 'required|numeric|digits_between:7,13|unique:customers,phone,'.$id,
	        'email' => 'nullable|email|unique:customers,email,'.$id,
            'code' => 'nullable|unique:customers,code,' . $id,
            'gender' => 'nullable|in:0,1',
            'cmnd' => 'nullable|numeric',
            'file' => 'nullable|mimes:doc,pdf,xls,xlsx,docx'
		]);
        $requestData = $request->all();
        
        $customer = Customer::findOrFail($id);

        \DB::transaction(function () use ($request, $requestData, $customer) {
            if (!empty($request->cmnd_date))
                $requestData['cmnd_date'] = \DateTime::createFromFormat(config('settings.format.date'),$request->cmnd_date)->format('Y-m-d');
            if ($request->hasFile('file')){
                \Storage::delete($customer->file);
                $requestData['file'] = Customer::uploadFile($request->file('file'));
            }
            if ( $request->filehidden == 1){
                \Storage::delete($customer->file);
                $requestData = Arr::prepend($requestData,NULL,'file');
            }
            $customer->update($requestData);
        });

        Session::flash('flash_message', __('booking::customers.customer_updated'));

        return \Redirect::to($requestData['hidUrl']);
//        return redirect('bookings/customers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $customer = Customer::findOrFail($id);
        if (isset($customer->bookings) && count($customer->bookings) > 0)
        {
            Session::flash('flash_message', __('booking::customers.customer_deleted_fail'));
            return redirect('bookings/customers');
        }

        Customer::destroy($id);

        Session::flash('flash_message', __('booking::customers.customer_deleted'));

        return redirect('bookings/customers');
    }

    /*
     * Import & Export Customer
     */
    public function exportCustomer()
    {
        $customers = Customer::all();
        return Excel::download(new CustomersExport($customers), 'customers-'.Carbon::now().'.xlsx');
    }
    public function downloadFileMau(){
        $file = public_path().'/uploads/imports/filemau.xlsx';
        $headers = array(
            'Content-Type: application/xlsx',
        );

        return response()->download($file, 'filemau.xlsx', $headers);
    }
    public function importView(){
        return view('booking::customers.importCustomerExcel');
    }

    public function showListImportExcel(Request $request){
        $this->validate($request, [
            'attachment' => 'required'
        ]);
        $file = $request->file('attachment');
        $destinationPath = 'uploads/imports';
        $file->move($destinationPath,$file->getClientOriginalName());
        $path = base_path().'/public/'.$destinationPath.'/'.$file->getClientOriginalName();
        $collection = (new CustomersImport())->toCollection($path);
        $importCustomers = [];
        if($collection != null && count($collection) > 0){
            $rows = $collection[0];
            if($rows != null && count($rows) > 0)
            {
                for($i = 1; $i < count($rows);$i++){
                    $rowDataExcel = $rows[$i];
                    if(!$this->IsNullOrEmptyString($rowDataExcel[1]))
                    {
                        $rowData = $this->fetchRowData($rowDataExcel[1],$rowDataExcel[2],
                            $rowDataExcel[3],$rowDataExcel[4],$rowDataExcel[5],$rowDataExcel[6],
                            $rowDataExcel[7],$rowDataExcel[8],$rowDataExcel[9],$rowDataExcel[10],$rowDataExcel[11]);
                        $importCustomers[] = $rowData;
                    }
                }
            }
        }
        session(['importCustomers' => $importCustomers]);
        return view('booking::customers.listCustomerExcel',compact('importCustomers'));
    }

    function fetchRowData($organ, $full_name, $position, $phone, $email, $facebook, $birthday, $city, $district, $ward, $address){
        $message = "";
        $isError = 0;
        if($this->IsNullOrEmptyString($full_name)){
            $message = trans('message.customer.name_empty');
            $isError = 1;
        }else if($this->IsNullOrEmptyString($phone)){
            $message = trans('message.customer.phone_empty');
            $isError = 1;
        }else if($this->IsNullOrEmptyString($email)){
            $message = trans('message.customer.email_empty');
            $isError = 1;
        }

        return [
            'organ' => $organ,
            'full_name' => $full_name,
            'position' => $position,
            'phone' => $phone,
            'email' => $email,
            'facebook' => $facebook,
            'birthday' => $birthday,
            'city' => $city,
            'district' => $district,
            'ward' => $ward,
            'address' => $address,
            'message'=>$message,
            'is_error'=>$isError
        ];
    }

    function saveDataImport(){
        $isSuccess = false;
        $importCustomers = session('importCustomers');
        foreach($importCustomers as $item){
            if($item['is_error'] == 0) //0: nghia la khong loi, 1: la bi loi
            {
                $this->checkAndSaveData($item['organ'],$item['full_name'],$item['position'],$item['phone'],
                    $item['email'],$item['facebook'],$item['birthday'],$item['city'],$item['district'],$item['ward'],$item['address']);
                $isSuccess = true;
            }
        }
        if($isSuccess)
            return redirect('bookings/customers')->with('flash_message', trans('message.customer.import_customer_success'));
        else
            return redirect('bookings/customers')->with('flash_info', trans('message.customer.import_customer_fail'));
    }

    private function IsNullOrEmptyString($str){
        return (!isset($str) || trim($str) === '');
    }

    public function checkAndSaveData($organ, $full_name, $position, $phone, $email, $facebook, $birthday, $city, $district, $ward, $address)
    {
        if($this->IsNullOrEmptyString($full_name)|| $this->IsNullOrEmptyString($phone) ||  $this->IsNullOrEmptyString($email)){
            return;
        }
        $city = City::where('name','LIKE',"%$city%")->first();
        if($city != null){
            $nationality = Country::where('id', $city->country_id)->first()->id;
            $district = District::where('city_id',$city->id)->where('name', 'LIKE', "%$district%")->first()->id;
            $ward = Ward::where('district_id', $district)->where('name', 'LIKE', "%$ward%")->first()->id;
        }
        $organ = Office::where('name','LIKE',"%$organ%")->first();
        if ($organ != null){
            $organ = $organ->id;
        }else{
            $organ = null;
        }
        $this->saveData($organ, $full_name, $position, $phone, $email, $facebook, $birthday, $nationality, $ward, $address);
    }

    public function saveData($organ, $full_name, $position, $phone, $email, $facebook, $birthday, $nationality, $ward, $address){
        $requestData = [
            'office_id' => $organ,
            'name' => $full_name,
            'position' => $position,
            'phone' => $phone,
            'email' => $email,
            'facebook' => $facebook,
            'birthday' => Carbon::createFromFormat(config('settings.format.date'), $birthday)->format('Y-m-d'),
            'nationality_id' => $nationality,
            'ward_id' => $ward,
            'address' => $address,
        ];
        $customer = Customer::where(function($query) use ($phone){
            $query->where('phone','=',$phone);
        })->first();

        \DB::transaction(function () use ($requestData,$customer) {
            if($customer) {
                $customer->update($requestData);
            }else{
                Customer::create($requestData);
            }
        });
    }
}
