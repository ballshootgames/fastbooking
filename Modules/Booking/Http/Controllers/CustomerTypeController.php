<?php

namespace Modules\Booking\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Traits\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Booking\Entities\CustomerType;
use Session;

class CustomerTypeController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = !empty($request->get('hidRecord')) ? $request->get('hidRecord') : Config("settings.perpage10");
        $customerTypes = CustomerType::sortable(['arrange' => 'asc']);

        if (!empty($keyword)){
            $customerTypes = $customerTypes->where('name','LIKE', "%$keyword%");
        }

        $customerTypes = $customerTypes->paginate($perPage);
        $totalInActive = CustomerType::where('active', config('settings.inactive'))->count();
        $totalActive = CustomerType::where('active', config('settings.active'))->count();

        return view('booking::customer-types.index', compact('customerTypes', 'totalInActive', 'totalActive'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $prevUrl = \URL::previous();
        return view('booking::customer-types.create', compact('prevUrl'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'arrange' => 'nullable|numeric',
        ]);
        $requestData = $request->all();

        CustomerType::create($requestData);

        Session::flash('flash_message', __('booking::customer_types.created_success'));

        return redirect('bookings/customer-types');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $customerType = CustomerType::findOrFail($id);
        $prevUrl = \URL::previous();

        return view('booking::customer-types.show', compact('customerType', 'prevUrl'));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id, Request $request)
    {
        $customerType = CustomerType::findOrFail($id);
        $prevUrl = '';
        if ($request['hidUrl'] != null && $request['hidUrl']!='')
            $prevUrl = $request['hidUrl'];
        else
            $prevUrl = \URL::previous();

        return view('booking::customer-types.edit', compact('customerType', 'prevUrl'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'name'=> 'required',
            'arrange' => 'nullable|numeric'
        ]);

        $requestData = $request->all();
        $customerType = CustomerType::findOrFail($id);
        if (empty($requestData['active'])){
            $requestData['active'] = 0;
        }
        $customerType->update($requestData);

        Session::flash('flash_message', __('booking::customer_types.updated_success'));

        return \Redirect::to($requestData['hidUrl']);
//        return redirect('bookings/customer-types');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        CustomerType::destroy($id);

        Session::flash('flash_message', __('booking::customer_types.deleted_success'));

        return redirect('bookings/customer-types');
    }
}
