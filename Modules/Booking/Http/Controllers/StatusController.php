<?php

namespace Modules\Booking\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Setting;
use Modules\Booking\Entities\Status;
use Illuminate\Http\Request;

class StatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = !empty($request->get('hidRecord')) ? $request->get('hidRecord') : Config("settings.perpage10");

        $statuses = Status::sortable(['arrange' => 'asc']);
        if (!empty($keyword)) {
            $statuses = $statuses->where('name', 'LIKE', "%$keyword%");
        }
        $statuses = $statuses->paginate($perPage);
        $totalInActive = Status::where('active', config('settings.inactive'))->count();
        $totalActive = Status::where('active', config('settings.active'))->count();

        $settings = Setting::allConfigsKeyValue();

        return view('booking::statuses.index', compact('statuses', 'totalInActive', 'totalActive', 'settings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $prevUrl = \URL::previous();
        return view('booking::statuses.create', compact('prevUrl'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'arrange' => 'required|numeric',
            'color' => 'nullable',
        ]);
        $requestData = $request->all();
        Status::create($requestData);

        \Session::flash('flash_message', __('booking::statuses.created_success'));
        return redirect('bookings/statuses');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Status  $status
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $status = Status::findOrFail($id);
        $prevUrl = \URL::previous();
        return view('booking::statuses.show', compact('status', 'prevUrl'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Status  $status
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $status = Status::findOrFail($id);
        $prevUrl = '';
        if ($request['hidUrl'] != null && $request['hidUrl']!='')
            $prevUrl = $request['hidUrl'];
        else
            $prevUrl = \URL::previous();

        return view('booking::statuses.edit', compact('status', 'prevUrl'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Status  $status
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'arrange' => 'required|numeric',
            'color' => 'nullable',
        ]);
        $requestData = $request->all();
        $status = Status::findOrFail($id);
        if (empty($requestData['active'])){
            $requestData['active'] = 0;
        }
        $status->update($requestData);

        \Session::flash('flash_message', __('booking::statuses.updated_success'));

        return \Redirect::to($requestData['hidUrl']);
//        return redirect('bookings/statuses');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Status  $status
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Status::destroy($id);

        \Session::flash('flash_message', __('booking::statuses.deleted_success'));

        return redirect('bookings/statuses');
    }
}
