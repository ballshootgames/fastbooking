<?php

namespace Modules\Booking\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Modules\Booking\Entities\Booking;
use Modules\Booking\Entities\BookingPeople;

class BookingPeopleController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = Config("settings.perpage");

        $peoples = new BookingPeople();
        if (!empty($keyword)) {
            $peoples = $peoples->where('name', 'LIKE', "%$keyword%");
        }
        $peoples = $peoples->with('booking');
        $peoples = $peoples->orderBy('booking_id','desc')->paginate($perPage);
        return view('booking::peoples.index', compact('peoples'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $bookings = Booking::pluck('code', 'id');
        $bookings->prepend(__('message.please_select'), '')->all();
        return view('booking::peoples.create', compact('bookings'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'birthday' => 'nullable|date_format:"'.config('settings.format.date').'"',
            'booking_id' => 'required'
        ]);

        $requestData = $request->all();
        if (!empty($requestData['birthday'])){
            $requestData['birthday'] = Carbon::createFromFormat(config('settings.format.date'),$requestData['birthday'])->format('Y-m-d');
        }
        BookingPeople::create($requestData);

        \Session::flash('flash_message', __('booking::booking_peoples.created_success'));
        return redirect('bookings/booking-peoples');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $people = BookingPeople::findOrFail($id);
        return view('booking::peoples.show', compact('people'));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $people = BookingPeople::findOrFail($id);
        $bookings = Booking::pluck('code', 'id');
        $bookings->prepend(__('message.please_select'), '')->all();
        return view('booking::peoples.edit', compact('people', 'bookings'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'birthday' => 'nullable|date_format:"'.config('settings.format.date').'"',
            'booking_id' => 'required'
        ]);

        $requestData = $request->all();
        if (!empty($requestData['birthday'])){
            $requestData['birthday'] = Carbon::createFromFormat(config('settings.format.date'),$requestData['birthday'])->format('Y-m-d');
        }
        $people = BookingPeople::findOrFail($id);
        $people->update($requestData);

        \Session::flash('flash_message', __('booking::booking_peoples.updated_success'));

        return redirect('bookings/booking-peoples');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        BookingPeople::destroy($id);

        \Session::flash('flash_message', __('booking::booking_peoples.deleted_success'));

        return redirect('bookings/booking-peoples');
    }
}
