<?php

namespace Modules\Booking\Http\Controllers;

use Session;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Modules\Booking\Entities\Office;

class OfficeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $keywordSearch = $request->get('search');
        $perPage = config('settings.perpage10');
        $offices = new Office();
        if (!empty($keywordSearch)){
            $offices = $offices->where('name','LIKE',"%$keywordSearch%");
        }
        $offices = $offices->paginate($perPage);
        return view('booking::offices.index', compact('offices'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $prevUrl = \URL::previous();
        return view('booking::offices.create', compact('prevUrl'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);
        $requestData = $request->all();
        if (!empty($requestData['date_establish'])) {
            $requestData['date_establish'] = Carbon::createFromFormat(config('settings.format.date'), $requestData['date_establish'])->format('Y-m-d');
        }
        Office::create($requestData);
        Session::flash('flash_message', __('booking::offices.created_success'));
        return redirect('bookings/customers/offices');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $office = Office::findOrFail($id);
        $prevUrl = \URL::previous();
        return view('booking::offices.show', compact('office','prevUrl'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id, Request $request)
    {
        $office = Office::findOrFail($id);
        $prevUrl = '';
        if ($request['hidUrl'] != null && $request['hidUrl']!='')
            $prevUrl = $request['hidUrl'];
        else
            $prevUrl = \URL::previous();
        return view('booking::offices.edit', compact('office','prevUrl'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);
        $requestData = $request->all();
        $office = Office::findOrFail($id);
        if (!empty($requestData['date_establish'])) {
            $requestData['date_establish'] = Carbon::createFromFormat(config('settings.format.date'), $requestData['date_establish'])->format('Y-m-d');
        }
        $office->update($requestData);
        Session::flash('flash_message', __('booking::offices.updated_success'));
        return redirect('bookings/customers/offices');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        Office::destroy($id);
        Session::flash('flash_message', __('booking::offices.deleted_success'));
        return redirect('bookings/customers/offices');
    }
}
