<?php

return [
    'office' => 'Đơn vị',
    'name' => 'Tên đơn vị',
    'slug' => 'Slug',
    'url' => 'Đường dẫn URL',
    'phone' => 'Điện thoại',
    'address' => 'Địa chỉ',
    'email' => 'Email',
    'website' => 'Đường dẫn website',
    'date_establish' => 'Ngày thành lập',
    'index' => 'Số thứ tự',
    'active' => 'Duyệt',
    'updated_at' => 'Ngày cập nhật',
    'created_success' => 'Đơn vị đã được thêm!',
    'updated_success' => 'Đơn vị đã được cập nhật!',
    'deleted_success' => 'Đơn vị đã được xóa!',
];