<?php

return [
    'booking_peoples' => 'Thông tin khách đi',
    'name' => 'Tên khách hàng',
    'birthday' => 'Sinh nhật',
    'booking_code' => 'Mã booking',
    'created_at' => 'Ngày tạo',
    'updated_at' => 'Ngày cập nhật',
    'deleted_success' => 'Khách đi đã được xóa!',
    'updated_success' => 'Khách đi đã được cập nhật!',
    'created_success' => 'Khách đi đã được thêm mới!',
];
