<?php

return [
    'status' => 'Trạng thái',
    'name' => 'Tên trạng thái',
    'index' => 'Số thứ tự',
    'color' => 'Màu sắc',
    'note' => 'Ghi chú',
    'updated_at' => 'Cập nhật',
    'created_at' => 'Ngày tạo',
    'deleted_success' => 'Trạng thái đã được xóa!',
    'updated_success' => 'Trạng thái đã được cập nhật!',
    'created_success' => 'Trạng thái đã được thêm mới!',
];
