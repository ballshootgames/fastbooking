<?php

return [
    'booking_sources' => 'Nguồn đặt vé',
    'name' => 'Tên nguồn',
    'index' => 'Số thứ tự',
    'active' => 'Duyệt',
    'created_at' => 'Ngày tạo',
    'updated_at' => 'Ngày cập nhật',
    'deleted_success' => 'Nguồn đặt vé đã được xóa!',
    'updated_success' => 'Nguồn đặt vé đã được cập nhật!',
    'created_success' => 'Nguồn đặt vé đã được thêm mới!',
];
