<?php

return [
    'status' => 'Trạng thái',
    'name' => 'Tên trạng thái',
    'index' => 'Số thứ tự',
    'color' => 'Màu sắc',
    'note' => 'Ghi chú',
    'note_detail' => ' Trạng thái với số thứ tự nhỏ nhất sẽ được dùng là trạng thái mặc định ban đầu của các booking.',
    'updated_at' => 'Ngày cập nhật',
    'created_at' => 'Ngày tạo',
    'deleted_success' => 'Trạng thái đã được xóa!',
    'updated_success' => 'Trạng thái đã được cập nhật!',
    'created_success' => 'Trạng thái đã được thêm mới!',
    'active' => 'Duyệt'
];
