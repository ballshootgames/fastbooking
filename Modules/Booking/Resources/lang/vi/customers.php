<?php

return [
	'customer' => 'Khách hàng',
    'name' => 'Tên Khách hàng',
    'customer_type' => 'Loại khách hàng',
    'code' => 'Mã khách hàng',
	'email' => 'Email',
	'phone' => 'Số điện thoại',
	'gender' => 'Giới tính',
	'picking_up_place' => 'Nơi đón',
    'cmnd' => 'Số CMND',
    'cmnd_date' => 'Ngày cấp',
    'cmnd_location' => 'Nơi cấp',
    'bithplace' => 'Nơi sinh',
    'note' => 'Ghi chú',
    'file' => 'File',
    'fax' => 'Fax',
    'twitter' => 'Twitter',
    'linkedin' => 'Linkedin',
    'website' => 'Website',
    'city' => 'Tỉnh, thành phố',
    'district' => 'Quận, huyện, thị xã',
    'ward' => 'Phường, xã',
    'nationality' => 'Quốc tịch',
	'customer_deleted' => 'Khách hàng đã được xóa!',
	'customer_updated' => 'Khách hàng đã được cập nhật!',
	'customer_added' => 'Khách hàng đã được thêm mới!',
	'address' => 'Địa chỉ',
	'permanent_address' => 'Thường trú',
	'facebook' => 'Facebook',
	'zalo' => 'Zalo',
	'creator_id' => 'Người tạo',
    'customer_deleted_fail' => 'Khách hàng có đơn hàng không được xoá!',
    'organization' => ['Cơ quan', 'Nhóm', 'Cá nhân'],
    'import_excel' => 'Nhập dữ liệu khách hàng',
    'export_excel' => 'Xuất dữ liệu khách hàng'
];
