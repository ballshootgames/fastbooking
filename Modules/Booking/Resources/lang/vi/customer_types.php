<?php

return [
    'customer_type' => 'Loại khách hàng',
    'name' => 'Tên loại khách hàng',
    'index' => 'Số thứ tự',
    'active' => 'Duyệt',
    'created_at' => 'Ngày tạo',
    'updated_at' => 'Ngày cập nhật',
	'deleted_success' => 'Loại khách hàng đã được xóa!',
	'updated_success' => 'Loại khách hàng đã được cập nhật!',
	'created_success' => 'Loại khách hàng đã được thêm mới!',
];
