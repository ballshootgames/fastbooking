<?php

return [
    'import' => 'Nhập dữ liệu',
    'export' => 'Xuất dữ liệu',
    'import_excel' => 'Nhập từ excel',
    'choose_file' => 'Chọn tập tin',
    'no_file' => 'Không có tập tin nào được chọn',
    'download_template' => 'Tải tệp mẫu',
    'load' => 'Hiển thị',
    'messages' => [
        'name' => 'Họ tên không được rỗng',
        'birthday' => 'Năm sinh rỗng hoặc không đúng định dạng',
        'id_number' => 'CMND không đúng định dạng',
        'gender' => 'Giới tính chỉ chọn nam hoặc nữ hoặc để rỗng',
        'phone' => 'Điện thoại không đúng định dạng'
    ],
];
