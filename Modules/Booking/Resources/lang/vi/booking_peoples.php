<?php

return [
    'booking_peoples' => 'Thông tin khách đi',
    'name' => 'Tên khách hàng',
    'birthday' => 'Sinh nhật',
    'booking_code' => 'Mã booking',
    'created_at' => 'Ngày tạo',
    'updated_at' => 'Ngày cập nhật',
    'deleted_success' => 'Khách đi đã được xóa!',
    'updated_success' => 'Khách đi đã được cập nhật!',
    'created_success' => 'Khách đi đã được thêm mới!',
    'id_number' => 'CMND',
    'picking_place' => 'Điểm đón',
    'gender' => 'Giới tính',
    'phone' => 'Điện thoại',
    'year' => 'Năm sinh',
    'import' => 'Nhập dữ liệu',
    'export' => 'Xuất dữ liệu',
    'import_excel' => 'Nhập từ excel',
    'choose_file' => 'Chọn tập tin',
    'no_file' => 'Không có tập tin nào được chọn',
    'download_template' => 'Tải tệp mẫu',
    'load' => 'Hiển thị'
];
