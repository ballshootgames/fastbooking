@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __('booking::statuses.status') }}
@endsection
@section('contentheader_title')
    {{ __('booking::statuses.status') }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li><a href="{{ url('/bookings/statuses') }}">{{ __('booking::statuses.status') }}</a></li>
        <li class="active">{{ __("message.detail") }}</li>
    </ol>
@endsection
@section('main-content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __("message.detail") }}</h3>
            <div class="box-tools">
                @if (strpos($prevUrl, url('/bookings/statuses') )!==false)
                    <a href="{{ $prevUrl }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.lists') }}</span></a>
                @else
                    <a href="{{ url('/bookings/statuses') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.lists') }}</span></a>
                @endif
                @can('StatusController@update')
                    {!! Form::open([
                        'method'=>'GET',
                        'url' => '/bookings/statuses/' . $status->id . '/edit',
                        'style' => 'display:inline'
                    ]) !!}
                    <input type="hidden" id="hidUrl" name="hidUrl" value={{ $prevUrl }}>
                    {!! Form::button('<i class="fa fa-pencil-square-o" aria-hidden="true"></i> <span class="hidden-xs">'.__('message.edit').'</span>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-primary btn-sm',
                            'title' => __('message.edit'),
                            'onclick'=>'return true'
                    ))!!}
                    {!! Form::close() !!}
                @endcan
                @can('StatusController@destroy')
                    {!! Form::open([
                        'method'=>'DELETE',
                        'url' => ['/bookings/statuses', $status->id],
                        'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> <span class="hidden-xs">'.__('message.delete').'</span>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-sm',
                            'title' => __('message.delete'),
                            'onclick'=>'return confirm("'.__('message.confirm_delete').'")'
                    ))!!}
                    {!! Form::close() !!}
                @endcan
            </div>
        </div>
        <div class="box-body table-responsive">
            <table class="table-layout table table-striped table-bordered">
                <tbody>
                <tr>
                    <td> {{ trans('booking::statuses.index') }} </td>
                    <td> {{ $status->arrange }} </td>
                </tr>
                <tr>
                    <td> {{ trans('booking::statuses.name') }} </td>
                    <td> {{ $status->name }} </td>
                </tr>
                <tr>
                    <td> {{ trans('booking::statuses.color') }} </td>
                    <td> <span class="label" style="background-color: {{ $status->color }}">{{ $status->name }}</span> </td>
                </tr>
                <tr>
                    <td> {{ trans('booking::statuses.created_at') }} </td>
                    <td> {{ $status->created_at }} </td>
                </tr>
                <tr>
                    <td> {{ trans('message.creator') }} </td>
                    <td> {{ optional($status->user)->name }}</td>
                </tr>
                <tr>
                    <td> {{ trans('booking::statuses.active') }} </td>
                    <td> {!! $status->active===1?'<i class="fas fa-check text-primary"></i>' : '<i class="fas fa-check"></i>' !!} </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
@section('css')
    <style>
        .table-layout tr td:first-child{
            width: 18%;
        }
        .table-layout tr td:last-child{
            width: 82%;
        }
    </style>
@endsection