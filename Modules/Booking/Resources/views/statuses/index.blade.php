@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __('booking::statuses.status') }}
@endsection
@section('contentheader_title')
    {{ __('booking::statuses.status') }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li class="active">{{ __('booking::statuses.status') }}</li>
    </ol>
@endsection
@section('main-content')
    <div id="alert"></div>
    <div class="statis">
        <ul>
            <li>Có {{ !empty($totalInActive) ? $totalInActive : '0' }} {{ trans('booking::statuses.status') }} chưa được phê duyệt</li>
            <li>Có {{ !empty($totalActive) ? $totalActive : '0' }} {{ trans('booking::statuses.status') }} đã được phê duyệt</li>
        </ul>
    </div>
    <div class="box box-primary">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-2">
                    @can('StatusController@store')
                        <a href="{{ url('/bookings/statuses/create') }}" class="btn btn-success btn-sm" title="{{ __('message.add') }}">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                        </a>
                    @endcan
                </div>
                <div class="col-sm-10">
                    <div class="pull-right">
                        {!! Form::open(['method' => 'GET', 'url' => '/bookings/statuses', 'class' => 'pull-left form-inline', 'role' => 'search', 'id' => 'search'])  !!}
                        <div class="input-group" style="width: 200px;">
                            <input type="text" value="{{\Request::get('search')}}" class="form-control input-sm" name="search" placeholder="{{ __('message.search_keyword') }}">
                            <span class="input-group-btn">
                                <button class="btn btn-default btn-sm" type="submit">
                                    <i class="fa fa-search"></i> {{ __('message.search') }}
                                </button>
                            </span>
                        </div>
                        <input type="hidden" name="hidRecord" value="{{ Request::has('record') ? Request::get('record') : config('settings.total_rec')[0] }}">
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        @php($index = ($statuses->currentPage()-1)*$statuses->perPage())
        <div class="box-body table-responsive no-padding">
            <table id="table-copy" class="table--layout table table-condensed table-bordered">
                <thead>
                <tr class="active">
                    <th class="text-center" style="width: 3.5%;">{{ trans('message.index') }}</th>
                    <th class="text-center" style="width: 2.5%;">
                        <input type="checkbox" name="chkAll" id="chkAll"/>
                    </th>
                    <th style="width: 40%;">@sortablelink('name', trans('booking::statuses.name'))</th>
                    <th class="text-center" style="width: 10%;">@sortablelink('arrange', trans('booking::statuses.index'))</th>
                    <th class="text-center" style="width: 10%;">{{ trans('booking::statuses.color') }}</th>
                    <th class="text-center" style="width: 6%;">@sortablelink('active', trans('booking::statuses.active'))</th>
                    <th class="text-center" style="width: 10%;">@sortablelink('created_at', trans('message.created_at'))</th>
                    <th class="text-center" style="width: 10%;">{{ trans('message.creator') }}</th>
                    <th style="width: 9%;"></th>
                </tr>
                </thead>
                <tbody>
                    @foreach($statuses as $item)
                        <tr>
                            <td class="text-center">{{ ++$index }}</td>
                            <td class="text-center">
                                <input type="checkbox" name="chkId" id="chkId" value="{{ $item->id }}" data-id="{{ $item->id }}"/>
                            </td>
                            <td>{{ $item->name }} {{ $item->name == 'Thanh toán' ? $settings['deposit'] . '%' : '' }}</td>
                            <td class="text-center">{{ $item->arrange }}</td>
                            <td class="text-center"><span class="text-center label" style="background-color: {{ $item->color }}">{{ $item->name }} {{ $item->name == 'Thanh toán' ? $settings['deposit'] . '%' : '' }}</span></td>
                            <td class="text-center">{!! $item->active == config('settings.active') ? '<i class="fa fa-check text-primary"></i>' : '' !!}</td>
                            <td class="text-center">{{ \Carbon\Carbon::parse($item->created_at)->format(config('settings.format.date')) }}</td>
                            <td class="text-title">{{ optional($item->user)->name }}</td>
                            <td class="text-center">
                                @can('StatusController@show')
                                    <a href="{{ url('/bookings/statuses/' . $item->id) }}" title="{{ __('message.view') }}"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                @endcan
                                @can('StatusController@update')
                                    <a href="{{ url('/bookings/statuses/' . $item->id . '/edit') }}" title="{{ __('message.edit') }}"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="12" class="text-right">
                            Hiển thị
                            <select name="record" id="record">
                                @php($total_rec = config('settings.total_rec'))
                                @foreach($total_rec as $item)
                                    <option value="{{$item}}" {{ Request::get('hidRecord') == $item ? 'selected' : '' }}>{{$item}}</option>
                                @endforeach
                            </select>
                            dòng/1 trang
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div class="box-comment">
            <p style="padding: 8px 20px; margin-bottom: 0; border-top: 1px solid #f9f9f9"><span style="color: #f44336;">{{ trans('booking::statuses.note') }}(*):</span>{{ trans('booking::statuses.note_detail') }}</p>
        </div>
        <div class="box-footer clearfix">
            <div class="row">
                <div id="btn-act" class="col-sm-6 text-left">
                    @can('StatusController@destroy')
                        <a href="#" id="delStatusItem" data-action="delStatusItem" class="btn-act btn btn-danger btn-sm" title="{{ __('message.delete') }}">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </a>
                    @endcan
                    @can('StatusController@active')
                        <a href="#" id="activeStatusItem" data-action="activeStatusItem" class="btn-act btn btn-primary btn-sm" title="{{ __('message.active') }}">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </a>
                    @endcan
                </div>
                <div class="col-sm-6 text-right">
                    {!! $statuses->appends(\Request::except('page'))->render() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts-footer')
    <script type="text/javascript">
        $(function() {
            $('#chkAll').on('click', function () {
                $("input:checkbox").prop('checked', $(this).prop("checked"));
            });
        });
        $('#record').on('change', function () {
            let form = $('#search');
            let record = $(this).val();
            form.find('input[name="hidRecord"]').val(record);
            axios.get(form.attr('action')).then(function (res) {
                form.submit();
            }).catch(function () {
                alert('Có lỗi xảy ra vui lòng thử lại!')
            })
            return false;
        });

        $('#btn-act').on('click', '.btn-act', function(e){
            e.preventDefault();
            let action = $(this).data('action');
            ajaxListStatuses(action);
        });

        function ajaxListStatuses(action){
            let chkId = $("input[name='chkId']:checked");
            let actTxt = '', successAlert = '', classAlert = '';
            switch (action) {
                case 'activeStatusItem':
                    actTxt = 'duyệt';
                    successAlert = '{{ trans('booking::statuses.updated_success') }}';
                    classAlert = 'alert-danger';
                    break;
                case 'delStatusItem':
                    actTxt = 'xóa';
                    successAlert = '{{ trans('booking::statuses.deleted_success') }}';
                    classAlert = 'alert-danger';
                    break;
            }
            if (chkId.length != 0){
                let notificationConfirm = 'Bạn có muốn '+actTxt+' hồ sơ này không?';
                let notification = confirm(notificationConfirm);
                if (notification){
                    var arrId = '';
                    $("input[name='chkId']:checked").map((val,key) => {
                        arrId += key.value + ',';
                    });
                    axios.get('{{url('/bookings/ajax')}}/'+action, {
                        params: {
                            ids: arrId
                        }
                    })
                        .then((response) => {
                            // console.log(response);
                            if (response.data.success === 'ok'){
                                $('#alert').html('<div class="alert '+classAlert+'">' +
                                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                                    successAlert +
                                    ' </div>');
                                {{--                                window.location = '{{ url('/bookings/customers') }}'--}}
                                location.reload(true);
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                        })
                }
            }else{
                let notificationAlert = 'Vui lòng chọn hồ sơ để '+actTxt+'!';
                alert(notificationAlert);
            }
        }
    </script>
@endsection
