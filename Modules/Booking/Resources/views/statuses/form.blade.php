@section('css')
    <style>
        .table-layout tr td:first-child{
            width: 18%;
        }
        .table-layout tr td:last-child{
            width: 82%;
        }
    </style>
@endsection
<div class="box-body">
    @if ($errors->any())
	    <div class="alert alert-danger">
	        @foreach ($errors->all() as $error)
	            <p><i class="fa fa-fw fa-check"></i> {{ $error }}</p>
	        @endforeach
	    </div>
	@endif

    <table class="table-layout table table-striped table-bordered">
        <tbody>
        <tr>
            <td>{{ trans('booking::statuses.name') }} <span class="label-required"></span></td>
            <td>
                <div>
                    {!! Form::text('name', null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
            </td>
        </tr>
        <tr>
            <td>{{ trans('booking::statuses.index') }} <span class="label-required"></span></td>
            <td>
                <div>
                    {!! Form::number('arrange', null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
                    {!! $errors->first('arrange', '<p class="help-block">:message</p>') !!}
                </div>
            </td>
        </tr>
        <tr>
            <td>{{ trans('booking::statuses.color') }}</td>
            <td>
                <div>
                    {!! Form::color('color', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('color', '<p class="help-block">:message</p>') !!}
                </div>
            </td>
        </tr>
        <tr>
            <td>{{ trans('booking::statuses.active') }}</td>
            <td>
                <div>
                    {!! Form::checkbox('active', 1, (isset($status) && $status->active===1)?true:false, ['class' => 'flat-blue', 'id' => 'active']) !!}
                </div>
            </td>
        </tr>
        </tbody>
    </table>
    <div style="margin-top: 10px;">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : __('message.save'), ['class' => 'btn btn-primary']) !!}
        <a href="{{ $prevUrl }}" class="btn btn-default">{{ __('message.cancel') }}</a>
        <input type="hidden" id="hidUrl" name="hidUrl" value={{ $prevUrl }}>
        {{--        <a href="{{ url('/admin/control/tour-properties?type='.$type) }}" class="btn btn-default">{{ __('message.close') }}</a>--}}
    </div>
{{--    <div class="form-group {{ $errors->has('color') ? 'has-error' : ''}}">--}}
{{--        {!! Form::label('color', trans('booking::statuses.color'), ['class' => 'col-md-3 control-label']) !!}--}}
{{--        <div class="col-md-6">--}}
{{--            --}}{{--{!! Form::color('color', null, ['class' => 'form-control']) !!}--}}
{{--            <input type="color" name="color" id="color" value="{{ isset($status) ? $status->color : '#ff0000' }}" class="form-control">--}}
{{--            {!! $errors->first('color', '<p class="help-block">:message</p>') !!}--}}
{{--        </div>--}}
{{--    </div>--}}
</div>
{{--<div class="box-footer">--}}
{{--    {!! Form::submit(isset($submitButtonText) ? $submitButtonText : __('message.save'), ['class' => 'btn btn-primary']) !!}--}}
{{--    <a href="{{ url('/bookings/statuses') }}" class="btn btn-default">{{ __('message.close') }}</a>--}}
{{--     --}}{{--{!! Form::submit(isset($submitButtonText) ? $submitButtonText." ".__('message.and_add') : __('message.save')." ".__('message.and_add'), ['class' => 'btn btn-success', 'name' => "btn-add"]) !!}--}}
{{--</div>--}}