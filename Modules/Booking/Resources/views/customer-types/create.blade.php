@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __('booking::customer_types.customer_type') }}
@endsection
@section('contentheader_title')
    {{ __('booking::customer_types.customer_type') }}
@endsection
@section('contentheader_description')
    
@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li><a href="{{ url('/bookings/customer-types') }}">{{ __('booking::customer_types.customer_type') }}</a></li>
        <li class="active">{{ __('message.new_add') }}</li>
    </ol>
@endsection

@section('main-content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __('message.new_add') }}</h3>
            <div class="box-tools">
                @if (strpos($prevUrl, url('/bookings/customer-types'))!==false)
                    <a href="{{ $prevUrl }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.lists') }}</span></a>
                @else
                    <a href="{{ url('/bookings/customer-types') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.lists') }}</span></a>
                @endif
            </div>
        </div>

        {!! Form::open(['url' => '/bookings/customer-types', 'class' => 'form-horizontal', 'files' => true]) !!}

        @include ('booking::customer-types.form')

        {!! Form::close() !!}
    </div>
@endsection