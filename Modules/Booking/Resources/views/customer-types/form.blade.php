@section('css')
	<style>
		.table-layout tr td:first-child{
			width: 18%;
		}
		.table-layout tr td:last-child{
			width: 82%;
		}
	</style>
@endsection
<div class="box-body">
	@if ($errors->any())
		<div class="alert alert-danger">
			@foreach ($errors->all() as $error)
				<p><i class="fa fa-fw fa-check"></i> {{ $error }}</p>
			@endforeach
		</div>
	@endif

	<table class="table-layout table table-striped table-bordered">
		<tbody>
		<tr>
			<td>{{ trans('booking::customer_types.name') }} <span class="label-required"></span></td>
			<td>
				<div>
					{!! Form::text('name', null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
					{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
				</div>
			</td>
		</tr>
		<tr>
			<td>{{ trans('booking::customer_types.index') }} <span class="label-required"></span></td>
			<td>
				<div>
					{!! Form::number('arrange', null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
					{!! $errors->first('arrange', '<p class="help-block">:message</p>') !!}
				</div>
			</td>
		</tr>
		<tr>
			<td>{{ trans('booking::customer_types.active') }}</td>
			<td>
				<div>
					{!! Form::checkbox('active', 1, (isset($customerType) && $customerType->active===1)?true:false, ['class' => 'flat-blue', 'id' => 'active']) !!}
				</div>
			</td>
		</tr>
		</tbody>
	</table>
	<div style="margin-top: 10px;">
		{!! Form::submit(isset($submitButtonText) ? $submitButtonText : __('message.save'), ['class' => 'btn btn-primary']) !!}
		<a href="{{ $prevUrl }}" class="btn btn-default">{{ __('message.cancel') }}</a>
		<input type="hidden" id="hidUrl" name="hidUrl" value={{ $prevUrl }}>
	</div>
</div>