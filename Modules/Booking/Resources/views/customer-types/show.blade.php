@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __('booking::customer_types.customer_type') }}
@endsection
@section('contentheader_title')
    {{ __('booking::customer_types.customer_type') }}
@endsection
@section('contentheader_description')
    
@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li><a href="{{ url('/bookings/customer-types') }}">{{ __('booking::customer_types.customer_type') }}</a></li>
        <li class="active">{{ __("message.detail") }}</li>
    </ol>
@endsection

@section('main-content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __("message.detail") }}</h3>
            <div class="box-tools">
                @if (strpos($prevUrl, url('/bookings/customer-types') )!==false)
                    <a href="{{ $prevUrl }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.lists') }}</span></a>
                @else
                    <a href="{{ url('/bookings/customer-types') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.lists') }}</span></a>
                @endif
                @can('CustomerTypeController@update')
                    {!! Form::open([
                        'method'=>'GET',
                        'url' => '/bookings/customer-types/' . $customerType->id . '/edit',
                        'style' => 'display:inline'
                    ]) !!}
                    <input type="hidden" id="hidUrl" name="hidUrl" value={{ $prevUrl }}>
                    {!! Form::button('<i class="fa fa-pencil-square-o" aria-hidden="true"></i> <span class="hidden-xs">'.__('message.edit').'</span>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-primary btn-sm',
                            'title' => __('message.edit'),
                            'onclick'=>'return true'
                    ))!!}
                    {!! Form::close() !!}
                @endcan
                @can('CustomerTypeController@destroy')
                    {!! Form::open([
                        'method'=>'DELETE',
                        'url' => ['/bookings/customer-types', $customerType->id],
                        'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> <span class="hidden-xs">'.__('message.delete').'</span>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-sm',
                            'title' => __('message.delete'),
                            'onclick'=>'return confirm("'.__('message.confirm_delete').'")'
                    ))!!}
                    {!! Form::close() !!}
                @endcan
            </div>
        </div>
        <div class="box-body table-responsive">
            <table class="table-layout table table-striped table-bordered">
                <tbody>
                <tr>
                    <td> {{ trans('booking::customer_types.index') }} </td>
                    <td> {{ $customerType->arrange }} </td>
                </tr>
                <tr>
                    <td> {{ trans('booking::customer_types.name') }} </td>
                    <td> {{ $customerType->name }} </td>
                </tr>
                <tr>
                    <td> {{ trans('message.created_at') }} </td>
                    <td> {{ $customerType->created_at }} </td>
                </tr>
                <tr>
                    <td> {{ trans('message.creator') }} </td>
                    <td> {{ optional($customerType->user)->name }}</td>
                </tr>
                <tr>
                    <td> {{ trans('booking::customer_types.active') }} </td>
                    <td> {!! $customerType->active===1?'<i class="fas fa-check text-primary"></i>' : '<i class="fas fa-check"></i>' !!} </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
@section('css')
    <style>
        .table-layout tr td:first-child{
            width: 18%;
        }
        .table-layout tr td:last-child{
            width: 82%;
        }
    </style>
@endsection