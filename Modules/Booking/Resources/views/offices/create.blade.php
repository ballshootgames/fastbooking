@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __('booking::offices.office') }}
@endsection
@section('contentheader_title')
    {{ __('booking::offices.office') }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li><a href="{{ url('/bookings/customers/offices') }}">{{ __('booking::offices.office') }}</a></li>
        <li class="active">{{ __('message.new_add') }}</li>
    </ol>
@endsection

@section('main-content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __('message.new_add') }}</h3>
            <div class="box-tools">
                @if (strpos($prevUrl, url('/bookings/customers/offices'))!==false)
                    <a href="{{ $prevUrl }}" class="btn btn-warning btn-sm" title="{{ __('message.lists') }}">
                        <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    </a>
                @else
                    <a href="{{ url('/bookings/customers/offices') }}" class="btn btn-warning btn-sm" title="{{ __('message.lists') }}">
                        <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    </a>
                @endif
            </div>
        </div>

        {!! Form::open(['url' => '/bookings/customers/offices', 'class' => 'form-horizontal', 'files' => true]) !!}

        @include ('booking::offices.form')

        {!! Form::close() !!}
    </div>
@endsection