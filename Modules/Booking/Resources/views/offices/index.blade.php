@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __('booking::offices.office') }}
@endsection
@section('contentheader_title')
    {{ __('booking::offices.office') }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li class="active">{{ __('booking::offices.office') }}</li>
    </ol>
@endsection

@section('main-content')
    <div id="alert"></div>
    <div class="box box-primary">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-2">
                    @can('OfficeController@store')
                        <a href="{{ url('/bookings/customers/offices/create') }}" class="btn btn-success btn-sm" title="{{ __('message.add') }}">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                        </a>
                    @endcan
                </div>
                <div class="col-sm-10">
                    <div class="pull-right">
                        {!! Form::open(['method' => 'GET', 'url' => '/bookings/customers/offices', 'class' => 'pull-left form-inline', 'role' => 'search', 'id' => 'search'])  !!}
                        <div class="input-group" style="width: 200px;">
                            <input type="text" value="{{\Request::get('search')}}" class="form-control input-sm" name="search" placeholder="{{ __('message.search_keyword') }}">
                            <span class="input-group-btn">
                                <button class="btn btn-default btn-sm" type="submit">
                                    <i class="fa fa-search"></i> {{ __('message.search') }}
                                </button>
                            </span>
                        </div>
{{--                        <input type="hidden" name="hidRecord" value="{{ Request::has('record') ? Request::get('record') : config('settings.total_rec')[0] }}">--}}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        @php($index = ($offices->currentPage()-1)*$offices->perPage())
        <div class="box-body table-responsive no-padding">
            <table id="table-copy" class="table--layout table table-condensed table-bordered">
                <thead>
                    <tr class="active">
                        <th class="text-center" style="width: 3.8%;">{{ trans('message.index') }}</th>
                        <th class="text-center" style="width: 3.1%;">
                            <input type="checkbox" name="chkAll" id="chkAll"/>
                        </th>
                        <th style="width: 20%;">@sortablelink('name',trans('booking::offices.name'))</th>
                        <th style="width: 22%;">{{ trans('booking::offices.phone') }}</th>
                        <th style="width: 22%;">{{ trans('booking::offices.email') }}</th>
                        <th class="text-center" style="width: 7%;">@sortablelink('active', trans('booking::offices.active'))</th>
                        <th class="text-center" style="width: 11%;">@sortablelink('updated_at', trans('message.updated_at'))</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($offices as $item)
                    <tr>
                        <td class="text-center">{{ ++$index }}</td>
                        <td class="text-center">
                            <input type="checkbox" name="chkId" id="chkId" value="{{ $item->id }}" data-id="{{ $item->id }}"/>
                        </td>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->phone }}</td>
                        <td>{{ $item->email }}</td>
                        <td class="text-center">
                            {!! $item->active == config('settings.active') ? '<i class="fa fa-check text-primary"></i>' : '' !!}
                        </td>
                        <td class="text-center">{{ \Carbon\Carbon::parse($item->updated_at)->format(config('settings.format.date')) }}</td>
                        <td class="text-center">
                            @can('OfficeController@show')
                                <a href="{{ url('/bookings/customers/offices/' . $item->id) }}" title="{{ __('message.view') }}"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                            @endcan
                            @can('OfficeController@update')
                                <a href="{{ url('/bookings/customers/offices/' . $item->id . '/edit') }}" title="{{ __('message.edit') }}"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                            @endcan
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="box-footer clearfix">
            <div class="row">
                <div id="btn-act" class="col-sm-6 text-left">
                    @can('OfficeController@destroy')
                        <a href="#" id="delOfficeItem" data-action="delOfficeItem" class="btn-act btn btn-danger btn-sm" title="{{ __('message.delete') }}">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </a>
                    @endcan
                    @can('OfficeController@active')
                        <a href="#" id="activeOfficeItem" data-action="activeOfficeItem" class="btn-act btn btn-primary btn-sm" title="{{ __('message.active') }}">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </a>
                    @endcan
                </div>
                <div class="col-sm-6 text-right">
                    {!! $offices->appends(\Request::except('page'))->render() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts-footer')
    <script type="text/javascript">
        $(function() {
            $('#chkAll').on('click', function () {
                $("input:checkbox").prop('checked', $(this).prop("checked"));
            });
        });
        $('#btn-act').on('click', '.btn-act', function(e){
            e.preventDefault();
            let action = $(this).data('action');
            ajaxListOffices(action);
        });

        function ajaxListOffices(action){
            let chkId = $("input[name='chkId']:checked");
            let actTxt = '', successAlert = '', classAlert = '';
            switch (action) {
                case 'activeOfficeItem':
                    actTxt = 'duyệt';
                    successAlert = '{{ trans('booking::offices.updated_success') }}';
                    classAlert = 'alert-danger';
                    break;
                case 'delOfficeItem':
                    actTxt = 'xóa';
                    successAlert = '{{ trans('booking::offices.deleted_success') }}';
                    classAlert = 'alert-danger';
                    break;
            }
            if (chkId.length != 0){
                let notificationConfirm = 'Bạn có muốn '+actTxt+' hồ sơ này không?';
                let notification = confirm(notificationConfirm);
                if (notification){
                    var arrId = '';
                    $("input[name='chkId']:checked").map((val,key) => {
                        arrId += key.value + ',';
                    });
                    axios.get('{{url('/bookings/ajax')}}/'+action, {
                        params: {
                            ids: arrId
                        }
                    })
                        .then((response) => {
                            // console.log(response);
                            if (response.data.success === 'ok'){
                                $('#alert').html('<div class="alert '+classAlert+'">' +
                                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                                    successAlert +
                                    ' </div>');
                                location.reload(true);
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                        })
                }
            }else{
                let notificationAlert = 'Vui lòng chọn hồ sơ để '+actTxt+'!';
                alert(notificationAlert);
            }
        }
    </script>
@endsection