@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __('booking::offices.office') }}
@endsection
@section('contentheader_title')
    {{ __('booking::offices.office') }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li><a href="{{ url('/bookings/customers/offices') }}">{{ __('booking::offices.office') }}</a></li>
        <li class="active">{{ __("message.detail") }}</li>
    </ol>
@endsection

@section('main-content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __("message.detail") }}</h3>
            <div class="box-tools">
                @if (strpos($prevUrl, url('/bookings/customers/offices') )!==false)
                    <a href="{{ $prevUrl }}" class="btn btn-warning btn-sm" title="{{ __('message.lists') }}">
                        <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    </a>
                @else
                    <a href="{{ url('/bookings/customers/offices') }}" class="btn btn-warning btn-sm" title="{{ __('message.lists') }}">
                        <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    </a>
                @endif
                @can('OfficeController@update')
                    {!! Form::open([
                        'method'=>'GET',
                        'url' => '/bookings/customers/offices/' . $office->id . '/edit',
                        'style' => 'display:inline'
                    ]) !!}
                    <input type="hidden" id="hidUrl" name="hidUrl" value={{ $prevUrl }}>
                    {!! Form::button('<i class="fa fa-pencil-square-o" aria-hidden="true"></i> <span class="hidden-xs">', array(
                            'type' => 'submit',
                            'class' => 'btn btn-primary btn-sm',
                            'title' => __('message.edit'),
                            'onclick'=>'return true'
                    ))!!}
                    {!! Form::close() !!}
                @endcan
                @can('OfficeController@destroy')
                    {!! Form::open([
                        'method'=>'DELETE',
                        'url' => ['/bookings/customers/offices', $office->id],
                        'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-sm',
                            'title' => __('message.delete'),
                            'onclick'=>'return confirm("'.__('message.confirm_delete').'")'
                    ))!!}
                    {!! Form::close() !!}
                @endcan
            </div>
        </div>
        <div class="box-body table-responsive">
            <table class="table-layout table table-striped table-bordered">
                <tbody>
                <tr>
                    <td> {{ trans('booking::offices.name') }} </td>
                    <td> {{ $office->name }} </td>
                </tr>
                <tr>
                    <td> {{ trans('booking::offices.phone') }} </td>
                    <td>{{ $office->phone }}</td>
                </tr>
                <tr>
                    <td> {{ trans('booking::offices.email') }} </td>
                    <td>{{ $office->email }}</td>
                </tr>
                <tr>
                    <td> {{ trans('booking::offices.address') }} </td>
                    <td>{{ $office->address }}</td>
                </tr>
                <tr>
                    <td> {{ trans('booking::offices.website') }} </td>
                    <td>{{ $office->website }}</td>
                </tr>
                <tr>
                    <td> {{ trans('booking::offices.date_establish') }} </td>
                    <td>{{ \Carbon\Carbon::parse($office->date_establish)->format(config('settings.format.date')) }}</td>
                </tr>
                <tr>
                    <td> {{ trans('booking::offices.active') }} </td>
                    <td> {!! $office->active===1?'<i class="fas fa-check text-primary"></i>' : '<i class="fas fa-check"></i>' !!} </td>
                </tr>
                <tr>
                    <td> {{ trans('message.created_at') }} </td>
                    <td> {{ \Carbon\Carbon::parse($office->created_at)->format(config('settings.format.datetime')) }} </td>
                </tr>
                <tr>
                    <td> {{ trans('message.updated_at') }} </td>
                    <td> {{ \Carbon\Carbon::parse($office->updated_at)->format(config('settings.format.datetime')) }} </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
@section('css')
    <style>
        .table-layout tr td:first-child{
            width: 18%;
        }
        .table-layout tr td:last-child{
            width: 82%;
        }
    </style>
@endsection