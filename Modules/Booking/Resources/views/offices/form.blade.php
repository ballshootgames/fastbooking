@section('css')
    <style>
        .table-layout tr td:first-child{
            width: 18%;
        }
        .table-layout tr td:last-child{
            width: 82%;
        }
    </style>
@endsection
<div class="box-body">
    @if ($errors->any())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <p><i class="fa fa-fw fa-check"></i> {{ $error }}</p>
            @endforeach
        </div>
    @endif
    <table class="table-layout table table-striped table-bordered">
        <tbody>
            <tr>
                <td>{{ trans('booking::offices.name') }} <span class="label-required"></span></td>
                <td>
                    <div>
                        {!! Form::text('name', null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
                        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>{{ trans('booking::offices.phone') }}</td>
                <td>
                    <div>
                        {!! Form::text('phone', null, ['class' => 'form-control input-sm']) !!}
                        {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>{{ trans('booking::offices.address') }}</td>
                <td>
                    <div>
                        {!! Form::text('address', null, ['class' => 'form-control input-sm']) !!}
                        {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>{{ trans('booking::offices.email') }}</td>
                <td>
                    <div>
                        {!! Form::email('email', null, ['class' => 'form-control input-sm']) !!}
                        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>{{ trans('booking::offices.website') }}</td>
                <td>
                    <div>
                        {!! Form::text('website', null, ['class' => 'form-control input-sm']) !!}
                        {!! $errors->first('website', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>{{ trans('booking::offices.date_establish') }}</td>
                <td>
                    <div>
                        {!! Form::text('date_establish', !empty($office->date_establish) ? Carbon\Carbon::parse($office->date_establish)->format(config('settings.format.date')) : null, ['class' => 'form-control input-sm datepicker']) !!}
                        {!! $errors->first('date_establish', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>{{ trans('booking::offices.active') }}</td>
                <td>
                    <div>
                        {!! Form::checkbox('active', 1, (isset($office) && $office->active===1)?true:false, ['class' => 'flat-blue', 'id' => 'active']) !!}
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <div style="margin-top: 10px;">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : __('message.save'), ['class' => 'btn btn-primary']) !!}
        <a href="{{ $prevUrl }}" class="btn btn-default">{{ __('message.cancel') }}</a>
        <input type="hidden" id="hidUrl" name="hidUrl" value={{ $prevUrl }}>
    </div>
</div>
@section('scripts-footer')
    <link rel="stylesheet" href="{{ asset('plugins/datepicker/datepicker3.css') }}">
    <script type="text/javascript" src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}" ></script>
    <script>
        $(function () {
            $('.datepicker').datepicker({
                autoclose: true,
                language: '{{ app()->getLocale() }}',
                format: '{{ config('settings.format.date_js') }}',
            });
        })(jQuery);
    </script>
@endsection
