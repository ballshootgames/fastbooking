<div class="box-body">
    @if ($errors->any())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <p><i class="fa fa-fw fa-check"></i> {{ $error }}</p>
            @endforeach
        </div>
        @endif
    <!--CUSTOMER INFO-->
    <div id="overlay" style="display: none"></div>
    <div class="fa-loading text-center" style="display: none;">
        <span class="fa fa-spinner fa-spin fa-3x text-primary" aria-hidden="true" ></span>
    </div>
    <div class="box-header with-border">
        <h3 class="box-title">{{ __('booking::bookings.customer_info') }}</h3>
    </div>
        {!! Form::hidden('customer[id]', null, ['class' => 'form-control input-sm customer_id']) !!}
    <table class="table-layout table table-striped table-bordered">
        <tbody>
            <tr>
                <td>{{ trans('booking::customers.phone') }} <span class="label-required"></span></td>
                <td>
                    <div class="{{ $errors->has('phone') ? 'has-error' : '' }}" style="position: relative">
                        {!! Form::text('customer[phone]', null, ['class' => 'form-control input-sm', 'required' => 'required', 'autocomplete' => 'off', 'id' => 'customer_phone']) !!}
                        <span id="phone_auto" style="position: absolute;right: 20px;top: 5px;"></span>
                        {!! $errors->first('customer.phone', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
                <td>{{ trans('booking::customers.name') }} <span class="label-required"></span></td>
                <td>
                    <div class="{{ $errors->has('name') ? 'has-error' : '' }}">
                        {!! Form::text('customer[name]', null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
                        {!! $errors->first('customer.name', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>

            <tr>
                <td>{{ trans('booking::customers.email') }} <span class="label-required"></span></td>
                <td>
                    <div class="{{ $errors->has('email') ? 'has-error' : '' }}" style="position: relative">
                        {!! Form::email('customer[email]', null, ['class' => 'form-control input-sm ', 'id' => 'customer_email']) !!}
                        <span id="email_auto" style="position: absolute;right: 20px;top: 5px;"></span>
                        {!! $errors->first('customer.email', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
                <td>{{ trans('booking::customers.gender') }}</td>
                <td>
                    <div class="col-md-12" style="display: inline-block;">
                        <div class="day">
                            {!! Form::radio('customer[gender]', 1, isset($booking->customer) && $booking->customer->gender===1?true:false, ['class' => '', 'id' => 'boy']) !!}
                            <span style="padding-right: 5px">{{ __('message.user.gender_male') }}</span>

                            {!! Form::radio('customer[gender]', 0, isset($booking->customer) && $booking->customer->gender===0?true:false, ['class' => '', 'id' => 'girl']) !!}
                            <span>{{ __('message.user.gender_female') }}</span>
                        </div>
                        {!! $errors->first('customer.gender', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>

            <tr>
                <td>{{ trans('booking::customers.address') }}</td>
                <td colspan="3">
                    <div class="{{ $errors->has('address') ? 'has-error' : '' }}">
                        {!! Form::text('customer[address]', null, ['class' => 'form-control input-sm']) !!}
                        {!! $errors->first('customer.address', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>

            <tr>
                <td>{{ trans('booking::customers.facebook') }}</td>
                <td>
                    <div class="{{ $errors->has('facebook') ? 'has-error' : '' }}">
                        {!! Form::text('customer[facebook]', null, ['class' => 'form-control input-sm']) !!}
                        {!! $errors->first('customer.address', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
                <td>{{ trans('booking::customers.zalo') }}</td>
                <td>
                    <div class="{{ $errors->has('zalo') ? 'has-error' : '' }}">
                        {!! Form::text('customer[zalo]', null, ['class' => 'form-control input-sm']) !!}
                        {!! $errors->first('customer.zalo', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
        <br>
    <!--BOOKING INFO-->
    <div class="box-header with-border">
        <h3 class="box-title">{{ __('booking::bookings.booking_info') }}</h3>
    </div>
    <table class="table-layout table table-striped table-bordered">
        <tbody>
            <tr>
                <td>{!! Form::label('services', $serviceInfo['label'], ['class' => 'control-label label-required']) !!}</td>
                <td colspan="3">
                    <div class="{{ $errors->has('services') ? 'has-error' : '' }}">
                        {!! Form::select('services[]', $services, null, ['class' => 'form-control input-sm select2', 'required' => 'required', 'id' => 'services', 'disabled' => $booking->id?true:false, 'min'=>0, 'multiple' => $serviceInfo['multiple'] ]) !!}
                        {!! $errors->first('services', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>{!! Form::label('total_people', trans('booking::bookings.total_people'), ['class' => 'control-label label-required']) !!}</td>
                <td>
                    <div class="col-md-12" style="display: inline-block;">
                        <span><i class=" fa fa-male text-purple" style="font-size: 1.6em"></i></span>
                        <div class="inline {{ $errors->has('adult_number') ? 'has-error' : '' }}">
                            {!! Form::number('adult_number', $adult_number, ['id' => 'adult_number', 'class' => 'form-control book-qty input-sm', 'required' => 'required', 'min'=>0, 'style' => 'width: 60px; display:inline-block']) !!}
                        </div>
                        <div class="inline {{ $errors->has('child_number') ? 'has-error' : '' }}">
                        <span style="padding-left: 5px"><i class=" fa fa-child text-maroon" style="font-size: 1.2em"></i></span>
                            {!! Form::number('child_number', $child_number, ['id' => 'child_number', 'class' => 'form-control book-qty input-sm', 'min'=>0, 'style' => 'width: 60px; display:inline-block']) !!}
                        </div>
                            <span style="padding-left: 5px"><img src="{{ asset('img/resources/assets/img/baby.png') }}" style="width: 12px;"></span>
                        <div class="inline {{ $errors->has('baby_number') ? 'has-error' : '' }}">
                            {!! Form::number('baby_number', $baby_number, ['id' => 'baby_number', 'class' => 'form-control book-qty input-sm', 'min'=>0, 'style' => 'width: 60px; display:inline-block']) !!}
                        </div>
                        {!! $errors->first('adult_number', '<p class="help-block">:message</p>') !!}
                        {!! $errors->first('child_number', '<p class="help-block">:message</p>') !!}
                        {!! $errors->first('baby_number', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
                <td>{!! Form::label('source_id', trans('booking::bookings.source_id'), ['class' => 'control-label']) !!}</td>
                <td>
                    <div class="{{ $errors->has('source_id') ? 'has-error' : '' }}">
                        {!! Form::select('source_id', $bookingSources, null, ['class' => 'form-control input-sm']) !!}
                        {!! $errors->first('source_id', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>

            @if(!empty($booking_properties))
                @foreach(array_chunk($booking_properties, 2) as $data_properties)
                    <tr>
                    @foreach($data_properties as $key => $value)
                        <td>{!! Form::label('properties['.$value['key'].']', trans($value['module'].'::bookings.'.$value['key']), $value['data']['label_attr']) !!} {!! ($value['key']==="picking_up_place")? ' <span class="label-required"></span>' : ''  !!}</td>
                        <td>
                            <div class="{{ $errors->has('properties.'.$value['key']) ? 'has-error' : ''}}">
                                @if($value['type'] == 'file' || $value['type'] == 'image')
                                    {!! Form::file('properties['.$value['key'].']', null, $value['data']['input_attr']) !!}
                                    @if($value['type'] == 'file')
                                        @if(!empty($properties_value[$value['key']]))
                                            <a class="btn btn-success" style="padding: 5px 8px; margin-top: 4px;" href="{{ asset(Storage::url($properties_value[$value['key']])) }}" download><i class="fa fa-download" aria-hidden="true" style="padding-right: 4px"></i>{{ trans('settings.download') }}</a>
                                        @endif
                                    @else
                                        <p style="margin-bottom: 0"><img src="{{ !empty($properties_value[$value['key']]) ? asset(Storage::url($properties_value[$value['key']])) : null }}" {{ !empty($properties_value[$value['key']]) ? "style=width:100px" : ''}} id="review-{{$value['key']}}"></p>
                                    @endif
                                    @php($nameFile[] = $value['key'])
                                @elseif($value['type'] == 'number')
                                    {!! Form::number('properties['.$value['key'].']', null, $value['data']['input_attr']) !!}
                                @elseif($value['type'] == 'date')
                                    {!! Form::text('properties['.$value['key'].']', null, ['class' => 'form-control input-sm datepicker', 'placeholder' => '']) !!}
                                @elseif($value['type'] == 'select')
                                    @if(isset($select_arr))
                                        @foreach($select_arr as $key => $item)
                                            @if($key == 'properties['.$value['key'].']')
                                                {!! Form::select('properties['.$value['key'].']', $item, null, $value['data']['input_attr']) !!}
                                            @endif
                                        @endforeach
                                    @else
                                        {!! Form::select('properties['.$value['key'].']', [], null, $value['data']['input_attr']) !!}
                                    @endif
                                    @php($nameSelect[] = 'properties['.$value['key'].']')
                                @elseif($value['type'] == 'time')
                                    {!! Form::time('properties['.$value['key'].']', null, $value['data']['input_attr']) !!}
                                @else
                                    {!! Form::text('properties['.$value['key'].']', null, $value['data']['input_attr']) !!}
                                @endif
                                {!! $errors->first('properties.'.$value['key'], '<p class="help-block">:message</p>') !!}
                            </div>
                        </td>
                    @endforeach
                    </tr>
                @endforeach
            @endif

            <tr>
                <td>{!! Form::label('status_id', trans('booking::bookings.status_id'), ['class' => 'control-label label-required']) !!}</td>
                <td>
                    <div class="{{ $errors->has('status_id') ? 'has-error' : '' }}">
                        {!! Form::select('status_id', $statuses, null, ['class' => 'form-control input-sm']) !!}
                        {!! $errors->first('status_id', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
                <td>{!! Form::label('payment_method_id', trans('booking::bookings.payment_method_id'), ['class' => 'control-label']) !!}</td>
                <td>
                    <div class="{{ $errors->has('payment_method_id') ? 'has-error' : '' }}">
                        {!! Form::select('payment_method_id', $payments, null, ['class' => 'form-control input-sm']) !!}
                        {!! $errors->first('payment_method_id', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>{!! Form::label('note', trans('booking::bookings.note'), ['class' => 'control-label']) !!}</td>
                <td colspan="3">
                    <div class="{{ $errors->has('note') ? 'has-error' : '' }}">
                        {!! Form::textarea('note', null, ['class' => 'form-control input-sm', 'rows' => 3]) !!}
                        {!! $errors->first('status_id', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
            <tr id="trTotal">
                <td>{!! Form::label('is_vat', trans('booking::bookings.is_vat'), ['class' => 'control-label']) !!}</td>
                <td {{$adult_price==0? 'colspan=3':''}}>
                    <div class="{{ $errors->has('is_vat') ? 'has-error' : '' }}">
                        {!! Form::checkbox('is_vat', 1, isset($booking->is_vat) && !empty($booking->is_vat) ? true : false, ['class' => 'flat-blue', 'id' => 'is_vat']) !!}
                    </div>
                </td>
                <td {{$adult_price==0?'style=display:none':''}}>{!! Form::label('total_price', trans('booking::bookings.total_price'), ['class' => 'control-label']) !!}</td>
                <td class="text-bold text-maroon" {{$adult_price==0?'style=display:none':'style=padding-top:12px;text-align:end;'}}> <span id="label_price">{{ number_format($booking->total_price) }} đ</span> </td>
            </tr>
{{--            <tr>--}}
{{--                <td>{!! Form::label('note', trans('booking::bookings.booking_status'), ['class' => 'control-label']) !!}</td>--}}
{{--                <td colspan="3">--}}
{{--                    <div class="{{ $errors->has('booking_status') ? 'has-error' : '' }}">--}}
{{--                        <select id="booking_status" name="booking_status" class="form-control input-sm">--}}
{{--                            @foreach(config('booking.booking_status') as $key=>$value)--}}
{{--                                @if(!empty($booking) && $booking->booking_status==$value)--}}
{{--                                    <option value='{{ $value }}' selected="selected">{{ $value }}</option>--}}
{{--                                @else--}}
{{--                                    <option value='{{ $value }}'>{{ $value }}</option>--}}
{{--                                @endif--}}
{{--                            @endforeach--}}
{{--                        </select>--}}
{{--                        {!! $errors->first('booking_status', '<p class="help-block">:message</p>') !!}--}}
{{--                    </div>--}}
{{--                </td>--}}
{{--            </tr>--}}
        </tbody>
    </table>
</div>
<div class="box-footer">
    {!! Form::submit(isset($booking->services) && !empty($booking->services) ? __('message.save') : __('message.continue'), ['class' => 'btn btn-primary']) !!}
{{--    <button type="submit" class='btn btn-primary'>{{ __('message.save')  }}</button>--}}
    <a href="{{ $prevUrl }}" class="btn btn-default">{{ __('message.cancel') }}</a>
    {{--{!! Form::submit(isset($submitButtonText) ? $submitButtonText." ".__('message.and_add') : __('message.save')." ".__('message.and_add'), ['class' => 'btn btn-success', 'name' => "btn-add"]) !!}--}}
    <input type="hidden" id="hidUrl" name="hidUrl" value={{ $prevUrl }}>
</div>

@section('scripts-footer')
    <link rel="stylesheet" href="{{ asset('plugins/datepicker/datepicker3.css') }}">
    <script type="text/javascript" src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}" ></script>

    <link rel="stylesheet" href="{{ asset('plugins/autocomplete/jquery.autocomplete.min.css') }}">
    <script type="text/javascript" src="{{ asset('plugins/autocomplete/jquery.autocomplete.min.js') }}" ></script>
    <style>
        table.table-layout td label {
            font-weight: 500;
        }
        .label-required:before {
            content: none;
        }
        .label-required:after {
            content: " *";
            color: red;
        }
        .fa-loading{
            z-index: 5;
            position: absolute;
            left: 0px;
            top: 0px;
            bottom: 0px;
            right: 0px;
            width: 100%;
            height: 100%;
        }
        .fa-loading span{
            position: absolute;
            left: 50%;
            top: 50%;
        }
        .fa-3x{
            font-size: 5em;
        }
        #overlay {
            position: fixed;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgba(0,0,0,0.5);
            z-index: 4;
            cursor: pointer;
        }
    </style>
    <script type="text/javascript">
        const updateCustomerInfo = (customer = null) => {
            if(customer == null) {
                customer = {
                    id: '',
                    name: '',
                    phone: '',
                    email: '',
                    address: '',
                    permanent_address: '',
                    facebook: '',
                    zalo: '',
                    gender: ''
                }
                $('#phone_auto').html('');
                $('#email_auto').html('');
            }
            $("[name='customer[id]']").val(customer.id);
            $("[name='customer[name]']").val(customer.name);
            $("[name='customer[email]']").val(customer.email);
            $("[name='customer[phone]']").val(customer.phone);
            $("[name='customer[address]']").val(customer.address);
            $("[name='customer[permanent_address]']").val(customer.permanent_address);
            $("[name='customer[facebook]']").val(customer.facebook);
            $("[name='customer[zalo]']").val(customer.zalo);
            if (customer.gender === 1) {
                $("#boy").prop("checked", true);
            } else if (customer.gender === 0) {
                $("#girl").prop("checked", true);
            }else{
                $("#boy").prop("checked", false);
                $("#girl").prop("checked", false);
            }
        };

        $(function(){
            @if($adult_price!=0)
                $('.book-qty').bind('keyup change', function(){
                    let adult_price = {{ $adult_price ? $adult_price : 0 }};
                    if (adult_price !== 0)
                    {
                        let child_price = {{ $child_price ? $child_price : 0 }};
                        let baby_price = {{ $baby_price ? $baby_price : 0 }};
                        if ($('#adult_number').val()=='' || parseInt($('#adult_number').val()) === 0){
                            alert('Số lượng người lớn nhập phải lớn hơn 0');
                        } else{
                            if ($(this).val()=='')
                            {
                                alert('Số lượng người nhập không được để trống');
                            }
                            else
                            {
                                let total = parseInt($('#adult_number').val()) * adult_price + parseInt($('#child_number').val()) * child_price + parseInt($('#baby_number').val()) * baby_price;
                                $('#label_price').html(total.toLocaleString() + " đ");
                            }
                        }
                    }
                });
            @endif
            //demo ảnh
            var nameFile = {!! isset($nameFile) ? json_encode($nameFile) : "''" !!};
            $.each(nameFile, function (key, name) {
                $("[name='properties["+name+"]']").change(function (e) {
                    var fileInput = this;
                    if (fileInput.files[0]){
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $('#review-'+name+'').attr('src',e.target.result);
                            $('#review-'+name+'').css({"width":"100px","height":"100px","border":"1px solid #333"});
                        }
                        reader.readAsDataURL(fileInput.files[0]);
                    }
                });
            });

            $("[name='services[]']").change(function () {
                var idServices = $(this).val();
                var moduleName = '{{ $serviceInfo['moduleName'] }}';
                var nameElement = {!! isset($nameSelect) ? json_encode($nameSelect) : "''" !!};
                if (nameElement){
                    $.each(nameElement, function (key, item) {
                        $("[name='"+ item +"']").html('<option value="">{{ __('message.loading') }}</option>');
                    });
                    $.ajax({
                        url: '{{ url('bookings/ajax/getDataService') }}',
                        type: "get",
                        dataType: "json",
                        data: {
                            idServices: idServices,
                            moduleName: moduleName
                        },
                        success: function (data) {
                            $.each(nameElement, function (key, item) {
                                $("[name='"+ item +"']").html('<option value="">{{ __('message.please_select') }}</option>');
                            });

                            $.each(data, function (key, item) {
                                $.each(nameElement, function (keyName, valueName) {
                                    if (key == valueName){
                                        $.each(item, function (itemKey, itemValue) {
                                            $("[name='"+ valueName +"']").append('<option value="'+ itemKey +'">'+ itemValue +'</option>');
                                        });
                                    }
                                });
                            });
                        }
                    })
                }
            });
            $('#customer_phone').autocomplete({
                paramName: 'phone',
                minChars: 3,
                dataType: 'json',
                serviceUrl: '{{ url('bookings/ajax/getCustomerByPhone') }}',
                transformResult: function(response) {
                    return {
                        suggestions: $.map(response, function(item) {
                            return { value: item.phone , data: item.id, item: item };
                        })
                    };
                },
                onSelect: function (suggestion) {
                    $('#phone_auto').html(deleteCustomer());
                    $('#email_auto').html('');
                    updateCustomerInfo(suggestion.item);
                }
            });
            $('#customer_email').autocomplete({
                paramName: 'email',
                minChars: 3,
                dataType: 'json',
                serviceUrl: '{{ url('bookings/ajax/getCustomerByEmail') }}',
                transformResult: function(response) {
                    return {
                        suggestions: $.map(response, function(item) {
                            return { value: item.email , data: item.id , item: item };
                        })
                    };
                },
                onSelect: function (suggestion) {
                    $('#phone_auto').html('');
                    $('#email_auto').html(deleteCustomer());
                    updateCustomerInfo(suggestion.item);
                }
            });

            function deleteCustomer() {
                return '<a href="javascript:void(0)" onclick="updateCustomerInfo()" class="magic-customer"><i class="fa fa-trash text-danger"></i></a>';
            }

            //Date range picker with time picker
            $('.datepicker').datepicker({
                autoclose: true,
                language: '{{ app()->getLocale() }}',
                format: '{{ config('settings.format.date_js') }}',
                startDate: '{{ date('d/m/Y') }}'
            });

            {{--@dd($booking->services->first())--}}
            var total_people = null;
            var limit = null;
            @if(empty($booking->services))
                $('#services').change(function () {
                    let service_id = $(this).val();
                    let module =  "{{ str_replace('\\', '\\\\', $serviceInfo['namespaceModel']) }}";
                    if (service_id!='')
                    {
                        $.ajax({
                            url: '{{ url('bookings/ajax/checkServiceLimit') }}',
                            type: "get",
                            dataType: "json",
                            data: {
                                service_id: service_id,
                                module: module
                            },
                            beforeSend: function(){
                                $('.fa-loading').show();
                                $('#overlay').show();
                            },
                            success: function (data) {
                                $('.fa-loading').hide();
                                $('#overlay').hide();
                                limit = data.limit;
                                let departure_date = data.departure_date;
                                let adult_price = data.adult_price;
                                let child_price = data.child_price;
                                let baby_price = data.baby_price;
                                if (limit === 0){
                                    alert('Dịch vụ bạn chọn là dịch vụ có giới hạn và số lượng giới hạn đã hết, hãy chọn dịch vụ khác hoặc tăng số giới hạn của dịch vụ trong quản lý dịch vụ Tour');
                                }
                                if (departure_date !== null){
                                    $('[name="properties[departure_date]"]').val(departure_date);
                                    $('[name="properties[departure_date]"]').attr('readonly', true);
                                }else{
                                    $('[name="properties[departure_date]"]').val('');
                                    $('[name="properties[departure_date]"]').attr('readonly', false);
                                }
                                // hien total
                                let total = parseInt($('#adult_number').val()) * adult_price + parseInt($('#child_number').val()) * child_price + parseInt($('#baby_number').val()) * baby_price;
                                $('#label_price').html(total + " đ");
                                let tmp = '<td>{!! Form::label("is_vat", trans("booking::bookings.is_vat"), ["class" => "control-label"]) !!}</td>' +
                                    '                    <td>' +
                                    '                        <div class="{{ $errors->has("is_vat") ? "has-error" : "" }}">' +
                                    '                            {!! Form::checkbox("is_vat", 1, isset($booking->is_vat) && !empty($booking->is_vat) ? true : false, ["class" => "flat-blue", "id" => "is_vat"]) !!}' +
                                    '                        </div>' +
                                    '                    </td>' +
                                    '                    <td>{!! Form::label("total_price", trans("booking::bookings.total_price"), ["class" => "control-label"]) !!}</td>' +
                                    '                    <td class="text-bold text-maroon" style="padding-top:12px;text-align:end;"> <span id="label_price">' + total.toLocaleString() + ' đ</span> </td>';
                                $('#trTotal').html(tmp);
                                $('.book-qty').bind('keyup change', function(){
                                    if ($('#adult_number').val()=='' || parseInt($('#adult_number').val()) === 0){
                                        alert('Số lượng người lớn nhập phải lớn hơn 0');
                                    } else{
                                        if ($(this).val()=='')
                                        {
                                            alert('Số lượng người nhập không được để trống');
                                        }
                                        else
                                        {
                                            let totalPrice = parseInt($('#adult_number').val()) * adult_price + parseInt($('#child_number').val()) * child_price + parseInt($('#baby_number').val()) * baby_price;
                                            $('#label_price').html(totalPrice.toLocaleString() + " đ");
                                        }
                                    }
                                });
                            },
                            error: function (error) {
                                alert('Đã có một số sự cố xảy ra, hãy load lại trang để thử lại');
                            }
                        });
                    } else {
                        $('.book-qty').unbind('keyup change');
                        let tmp = '<td>{!! Form::label("is_vat", trans("booking::bookings.is_vat"), ["class" => "control-label"]) !!}</td>' +
                            '                    <td colspan="3">' +
                            '                        <div class="{{ $errors->has("is_vat") ? "has-error" : "" }}">' +
                            '                            {!! Form::checkbox("is_vat", 1, isset($booking->is_vat) && !empty($booking->is_vat) ? true : false, ["class" => "flat-blue", "id" => "is_vat"]) !!}' +
                            '                        </div>' +
                            '                    </td>';
                        $('#trTotal').html(tmp);
                    }
                });
                $('form').submit(function (e) {
                    if (limit === null){
                        return true;
                    }else{
                        total_people = parseInt($('#adult_number').val());
                        if ($('#child_number').val() !== ""){
                            total_people = total_people + parseInt($('#child_number').val());
                        }
                        if ($('#baby_number').val() !== ""){
                            total_people = total_people + parseInt($('#baby_number').val());
                        }
                        if (total_people <= limit){
                            return true
                        }else{
                            alert('Dịch vụ bạn đang chọn là sản phẩm có giới hạn và số lượng giới hạn còn lại là: '+limit+', tổng số người đặt dịch vụ không được lớn hơn số lượng giới hạn còn lại của dịch vụ!');
                            e.preventDefault();
                        }
                    }
                });
            @else
                limit = '{!! \Modules\Booking\Entities\Booking::getExistPeopleLimitTour(optional($booking->services)->first()) !!}';
                var old_total_people = {!! $booking->getTotalAdult() + $booking->getTotalChildren() + $booking->getTotalBaby() !!} + parseInt(limit);
                @if(!empty(optional(optional($booking->services)->first())->departure_date))
                    $('[name="properties[departure_date]"]').attr('readonly', true);
                @endif
                $('form').submit(function (e) {
                    if (limit === ''){
                        return true;
                    }else{
                        total_people = parseInt($('#adult_number').val());
                        if ($('#child_number').val() !== ""){
                            total_people = total_people + parseInt($('#child_number').val());
                        }
                        if ($('#baby_number').val() !== ""){
                            total_people = total_people + parseInt($('#baby_number').val());
                        }
                        if (total_people <= old_total_people){
                            return true
                        }else{
                            alert('Dịch vụ bạn đang chọn là sản phẩm có giới hạn và số lượng giới hạn còn lại là: '+limit+', bạn chỉ có thể cập nhật với tổng số người không được lớn hơn: '+old_total_people+' người');
                            e.preventDefault();
                        }
                    }
                });
            @endif
        });
    </script>
@endsection
