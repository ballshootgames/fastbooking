<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title" id="form_title">{{ __('booking::bookings.add_manager') }}</h3>
    </div>
    <div class="box-body">
        <div id="submitError" class="alert alert-danger" style="display: none;">
        </div>
        <input type="hidden" id="hidBooking" value="{{ $bookId }}">
        <input type="hidden" id="hidPayment" value="{{ $paymentMethodId }}">
        <table class="table-layout table table-striped table-bordered">
            <tbody>
            <tr>
                <td class="col-md-3" style="width: auto; vertical-align: inherit;">
                    {{ __('booking::bookings.manager') }}
                </td>
                <td class="col-md-9">
                    <div id="divManager" class="{{ $errors->has('manager_id') ? 'has-error' : ''}}" style="width: auto; ">
                        {!! Form::select('manager', $managers, isset($managerId) && !empty($managerId)?$managerId:null, ['class' => 'form-control', 'id' => 'manager']) !!}
                        {!! $errors->first('manager_id', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="box-footer">
        <a onclick="saveManager()" id="btnSaveManager" data-action="saveManager" class="btn btn-save-manager btn-primary" title="{{ __('message.save') }}">{{ __('message.save') }}</a>
        <a href="{{ Url::current() }}" class="btn btn-default" data-dismiss="modal">{{ __('message.close') }}</a>
    </div>
</div>