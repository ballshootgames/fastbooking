<button type="button" class="btn-dropdown form-control input-sm">Lọc <i class="fa fa-plus" aria-hidden="true"></i></button>
<div class="dropdown-menu dropdown-filter" style="display: none">
    <input type="text" value="{{\Request::get('departure_date')}}" class="form-control input-sm datepicker" name="departure_date" placeholder="{{ __('booking::bookings.departure_date') }}" autocomplete="off">
    @isset($list_services)
        <select name="service_id" class="form-control input-sm select2" style="width: 150px;">
            <option value="">--{{ __('booking::bookings.'.Route::input('module')) }}--</option>
            @foreach($list_services as $id => $name)
                <option {{ Request::get('service_id') == $id ? "selected" : "" }} value="{{ $id }}"> {{ $name }}</option>
            @endforeach
        </select>
    @endisset
</div>
<!-- Nhan vien dieu hanh -->
@isset($managers)
    <select name="manager_id" class="form-control input-sm select2">
{{--        <option value="">--{{  __('booking::bookings.manager') }}--</option>--}}
        @foreach($managers as $id => $name)
            <option {{ Request::get('manager_id') == $id ? "selected" : "" }} value="{{ $id }}"> {{ $name }}</option>
        @endforeach
    </select>
@endisset
<!-- Tinh trang -->
<select name="status_id" class="form-control input-sm select2">
    <option value="">--{{ __('booking::bookings.status_id') }}--</option>
    @foreach(\Modules\Booking\Entities\Status::pluck('name', 'id') as $key => $value)
        <option {{ Request::get('status_id') == $key ? "selected" : "" }} value="{{ $key }}"> {{ $value }}</option>
    @endforeach
</select>

<div class="input-group">
    <input style="width: 200px;" type="text" value="{{\Request::get('search')}}" class="form-control input-sm" name="search" placeholder="{{ __('message.search_keyword') }}">
    <span class="input-group-btn">
        <button class="btn btn-default btn-sm" type="submit">
            <i class="fa fa-search"></i> {{ __('message.search') }}
        </button>
    </span>
</div>
