@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __('booking::bookings.booking') }}
@endsection
@section('contentheader_title')
    {{ __('booking::bookings.booking') }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li><a href="{{ url('/bookings/'.Route::input('module')) }}">{{ __('booking::bookings.booking') }}</a></li>
        <li class="active">{{ __("message.detail") }}</li>
    </ol>
@endsection
@section('main-content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">{{ __("message.detail") }}</h3>
            <div class="box-tools">
                @if (strpos($prevUrl, url('/bookings/'.Route::input('module').'?') )!==false)
                    <a href="{{ $prevUrl }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.lists') }}</span></a>
                @else
                    <a href="{{ url('/bookings/'.Route::input('module')) }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.lists') }}</span></a>
                @endif
{{--                <a href="{{ url('/bookings/'.Route::input('module').'/' . $booking->id . '/edit') }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.edit') }}</span></a>--}}
                {!! Form::open([
                    'method'=>'GET',
                    'url' => '/bookings/'.Route::input('module').'/'.$booking->id.'/edit',
                    'style' => 'display:inline'
                ]) !!}
                <input type="hidden" id="hidUrl" name="hidUrl" value={{ $prevUrl }}>
                {!! Form::button('<i class="fa fa-pencil-square-o" aria-hidden="true"></i> <span class="hidden-xs">'.__('message.edit').'</span>', array(
                        'type' => 'submit',
                        'class' => 'btn btn-primary btn-sm',
                        'title' => __('message.edit'),
                        'onclick'=>'return true'
                ))!!}
                {!! Form::close() !!}

                {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['/bookings/'.Route::input('module'), $booking->id],
                    'style' => 'display:inline'
                ]) !!}
                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> <span class="hidden-xs">'.__('message.delete').'</span>', array(
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-sm',
                        'title' => __('message.delete'),
                        'onclick'=>'return confirm("'.__('message.confirm_delete').'")'
                ))!!}
                {!! Form::close() !!}
            </div>
        </div>

        <div class="box-body table-responsive">
            @php
                $tmp = strpos(\Request::fullUrl(), '?') !== false ? 1 : 0;
                $passenger = Session::has('bookings_passenger')? 1: 0;
                Session::forget('bookings_passenger')
            @endphp
            <ul class="nav nav-tabs">
                <li {!! ($tmp===0 && $passenger===0) ? 'class="active"':'' !!}><a data-toggle="tab" href="#bookings_info">{{ trans('booking::bookings.info') }}</a></li>
                <li {!! ($tmp===1 || $passenger===1) ? 'class="active"':'' !!}><a data-toggle="tab" href="#bookings_passenger">{{ trans('booking::bookings.passenger') }}</a></li>
                <li><a data-toggle="tab" href="#bookings_history">{{ trans('booking::bookings.history') }}</a></li>
            </ul>
            <div class="tab-content tab-amenities">
                <div id="bookings_info" {!! ($tmp===0 && $passenger===0) ? 'class="tab-pane fade in active"':'class="tab-pane fade"' !!}>
                    <table class="table-layout table table-striped table-bordered" style="margin-bottom: 18px">
                        <tbody>
                        <tr>
                            <td colspan="4" class="text-danger">
                                {{ __('booking::bookings.customer_info') }}
                            </td>
                        </tr>
                        <tr>
                            <td> {{ trans('booking::bookings.customer') }} </td><td> {{ optional($booking->customer)->name }} </td>
                            <td> {{ trans('booking::customers.phone') }} </td><td> {{ optional($booking->customer)->phone }} </td>
                        </tr>
                        <tr>
                            <td> {{ trans('booking::customers.email') }} </td><td> {{ optional($booking->customer)->email }} </td>
                            <td> {{ trans('booking::customers.gender') }} </td><td> {{ optional($booking->customer)->textGender }} </td>
                        </tr>
                        <tr>
                            <td> {{ trans('booking::customers.address') }} </td><td colspan="3"> {{ optional($booking->customer)->address }} </td>
                        </tr>
                        <tr>
                            <td> {{ trans('booking::customers.facebook') }} </td><td> {{ optional($booking->customer)->facebook }} </td>
                            <td> {{ trans('booking::customers.zalo') }} </td><td> {{ optional($booking->customer)->zalo }} </td>
                        </tr>
                        </tbody>
                    </table>
                    <table class="table-layout table table-striped table-bordered" style="margin-bottom: 18px">
                        <tbody>
                        <tr>
                            <td colspan="4" class="text-danger">
                                {{ __('booking::bookings.booking_info') }}
                            </td>
                        </tr>
                        <tr>
                            <td> {{ $serviceInfo['label'] }} </td>
                            <td colspan="3">{{ $booking->services->pluck($serviceInfo['column_name'])->implode(',') }}</td>
                        </tr>
                        <tr>
                            <td> {{ trans('booking::bookings.total_number') }} </td>
                            <td>
                                @if($booking->getTotalAdult()>0)
                                    <span class="text-bold text-purple">{{ $booking->getTotalAdult() }}</span>
                                    <i class=" fa fa-male text-purple" style="font-size: 1.6em"></i>
                                @endif
                                @if($booking->getTotalChildren() >0)
                                    <span class="text-bold text-maroon">{{ $booking->getTotalChildren() }}</span>
                                    <i class=" fa fa-child text-maroon" style="font-size: 1.2em"></i>
                                @endif
                                @if($booking->getTotalBaby()>0)
                                    <span class="text-bold">{{ $booking->getTotalBaby() }}</span>
                                    <img src="{{ asset('img/resources/assets/img/baby.png') }}" style="width: 10px;">
                                @endif
                            </td>
                            <td> {{ trans('booking::bookings.total_price') }} </td><td class="text-bold text-maroon"> {{ number_format($booking->total_price) }} đ</td>
                        </tr>
                        <tr>
                            @php($bookDeposit = \Modules\Tour\Entities\TourDeposit::where('tour_id', $booking->services->pluck('id'))->orderBy('value', 'ASC')->first())
                            <td> {{ trans('booking::bookings.approved') }} </td><td>{{ optional($booking->status)->name }} {{ optional($booking->status)->id == 3 && !is_null($bookDeposit) ? $bookDeposit->value . '%' : ''}}</td>
                            <td> {{ trans('booking::bookings.creator_id') }} </td><td>{{ optional($booking->creator)->name }}</td>
                        </tr>
                        <tr>
                            <td> {{ trans('booking::bookings.payment_method_id') }} </td><td>{{ optional($booking->payment)->name }}</td>
                            <td> {{ trans('booking::bookings.is_vat') }} </td><td>{{ !empty($booking->is_vat) ? 'Đã bao gồm thuế VAT' : 'Không' }}</td>
                        </tr>
                        <tr>
                            <td> {{ trans('message.updated_at') }} </td><td> {{ Carbon\Carbon::parse($booking->updated_at)->format(config('settings.format.date')) }} </td>
                            <td>{{ trans('booking::bookings.source_id') }}</td><td>{{ optional($booking->source)->name }}</td>
                        </tr>
                        </tbody>
                    </table>
                    <table class="table-layout table table-striped table-bordered">
                        <tbody>
                        <tr>
                            <td colspan="4" class="text-danger">
                                {{ __('booking::bookings.booking_properties') }}
                            </td>
                        </tr>
                        <tr>
                            @foreach($booking->properties as $item)
                                <td> {{ trans($module.'::bookings.'.$item->key) }} </td>
                                <td>
                                    {{ $item->pivot->value }}
                                </td>
                            @endforeach
                        </tr>
                        <tr>
                            <td> {{ trans('booking::bookings.manager') }} </td>
                            <td colspan="3">
                                {{ optional($booking->manager)->name }}
                            </td>
{{--                            <td> {{ trans('booking::bookings.booking_status') }} </td>--}}
{{--                            <td class="text-bold text-maroon">--}}
{{--                                {{ $booking->booking_status }}--}}
{{--                            </td>--}}
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div id="bookings_passenger" {!! ($tmp===1 || $passenger===1) ? 'class="tab-pane fade in active"':'class="tab-pane fade"' !!}>
                    @include ('booking::peoples.index')
                </div>
                <div id="bookings_history" class="tab-pane fade">
                    <table class="table--layout table table-condensed table-bordered">
                        <thead>
                        <tr class="active">
                            <td><span class="text-bold"> {{ trans('booking::bookings.action') }} </span></td>
                            <td><span class="text-bold"> {{ trans('booking::bookings.user') }} </span></td>
                            <td style="text-align: center;"><span class="text-bold"> {{ trans('booking::bookings.time') }} </span></td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($booking->history()->with('user')->get() as $h)
                            <tr>
                                <td>{{ $h->description }}</td>
                                <td>{{ optional($h->user)->name }}</td>
                                <td style="text-align: center;">{{ $h->created_at->format(config('settings.format.datetime')) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection