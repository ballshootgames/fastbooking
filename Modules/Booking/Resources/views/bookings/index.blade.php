@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __('booking::bookings.booking') }}
@endsection
@section('contentheader_title')
    {{ __('booking::bookings.booking') }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li class="active">{{ __('booking::bookings.booking') }}</li>
    </ol>
@endsection
@section('main-content')
    <div id="alert"></div>
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" id="divManagerContain">
                {{--                @include ('booking::bookings.manager')--}}
            </div>
        </div>
    </div>
    <div class="statis">
        <div class="row">
            <div class="col-md-6">
                <ul>
                    <li>Có {{ !empty($totalBooked) ? $totalBooked : '0' }} đơn hàng đang xử lý</li>
                    <li>Có {{ !empty($totalCancel) ? $totalCancel : '0' }} đơn hàng đã huỷ</li>
                </ul>
            </div>
            <div class="col-md-6">
                <ul>
                    <li>Có {{ !empty($totalPaid) ? $totalPaid : '0' }} đơn hàng đã thanh toán</li>
                    <li>Có {{ !empty($totalUnpaid) ? $totalUnpaid : '0' }} đơn hàng chưa thanh toán</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="box box-primary">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-2">
                    @can('BookingController@store')
                        <a href="{{ url('/bookings/'.Route::input('module').'/create') }}" class="btn btn-success btn-sm" title="{{ __('message.new_add') }}">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                        </a>
                    @endcan
                </div>
                <div class="col-sm-10">
                    <div class="pull-right">
                        {!! Form::open(['method' => 'GET', 'url' => '/bookings/'.Route::input('module'), 'class' => 'pull-left form-inline', 'role' => 'search', 'id' => 'search', 'style' => 'position:relative'])  !!}
                        @include('booking::bookings.search-input')
                        <input type="hidden" name="hidRecord" value="{{ Request::has('record') ? Request::get('record') : config('settings.total_rec')[0] }}">
                        {!! Form::close() !!}
                        <a href="{{ url('/bookings/'.Route::input('module').'/export?'.http_build_query(\Request::except('page'))) }}" class="btn bg-purple btn-sm" title="{{ __('message.export') }}" style="margin-left: 5px;">
                            <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        @php($index = ($bookings->currentPage()-1)*$bookings->perPage())
        <div class="box-body table-responsive no-padding">
            <table id="table-copy" class="table--layout table table-condensed table-bordered">
                <thead>
                    <tr class="active">
                        <th class="text-center" style="width: 3.5%;">{{ trans('message.index') }}</th>
                        <th class="text-center" style="width: 2.5%;">
                            <input type="checkbox" name="chkAll" id="chkAll"/>
                        </th>
                        <th style="width: 6%;">@sortablelink('code', trans('booking::bookings.code'))</th>
                        <th style="width: 22%;">{{ $serviceInfo['label'] }}</th>
                        <th style="width: 10%;">@sortablelink('customer_id', __('booking::bookings.customer'))</th>
                        <th class="text-center" style="width: 9%;">{{ trans('booking::bookings.total_number') }}</th>
                        <th class="text-right" style="width: 8%;">@sortablelink('total_price', __('booking::bookings.total_price'))</th>
                        <th class="text-center" style="width: 9%;">@sortablelink('status_id', __('booking::bookings.status_id'))</th>
                        <th class="text-center" style="width: 11%;">{{ trans('booking::bookings.departure_date') }}</th>
                        <th style="width: 11%;">{{ __('NV điều hành') }}</th>
                        <th style="width: 8%;">{{ __('Điểm đón') }}</th>
                        <th style="width: 9%;"></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($bookings as $item)
                    <tr>
                        <td class="text-center">{{ ++$index }}</td>
                        <td class="text-center">
                            <input type="checkbox" name="chkId" id="chkId" value="{{ $item->id }}" data-id="{{ $item->id }}"/>
                        </td>
                        <td style="color: {{ optional($item->status)->color }}">{{ $item->code }}</td>
                        <td class="text-title">
                            {{ $item->services->pluck($serviceInfo['column_name'])->implode(',') }}
                            @if($item->approved === config('settings.approved.tmp'))
                                <small class="label label-warning">
                                    {{ __('booking::bookings.approved_tmp') }}
                                </small>
                            @elseif($item->approved === config('settings.approved.cancel'))
                                <small class="label label-danger" data-toggle="tooltip" data-original-title="{{ $item->cancel_note }}">
                                    {{ __('booking::bookings.approved_cancel') }}
                                </small>
                            @endif
                        </td>
                        <td class="text-title"><p style="margin: 0px;">
                            {{ optional($item->customer)->name }}
                            @if(!empty($item->note))
                                <small class="label label-warning"><i class="fa fa-comment-o" data-toggle="tooltip" title="{{ __('booking::bookings.note') }}"></i></small>
                            @endif
                            </p>
                        </td>
                        <td class="text-center">
                            @if($item->getTotalAdult()>0)
                                <span class="text-bold text-purple">{{ $item->getTotalAdult() }}</span>
                                <i class=" fa fa-male text-purple" style="font-size: 1.6em"></i>
                            @endif
                            @if($item->getTotalChildren() >0)
                                <span class="text-bold text-maroon">{{ $item->getTotalChildren() }}</span>
                                <i class=" fa fa-child text-maroon" style="font-size: 1.2em"></i>
                            @endif
                            @if($item->getTotalBaby()>0)
                                <span class="text-bold">{{ $item->getTotalBaby() }}</span>
                                <img src="{{ asset('img/resources/assets/img/baby.png') }}" style="width: 10px;">
                            @endif
                        </td>
                        <td class="text-bold text-danger text-right">{{ number_format($item->total_price) }}</td>
                        <td class="text-center">
                            @php($bookDeposit = \Modules\Tour\Entities\TourDeposit::whereIn('tour_id', $item->services->pluck('id'))->orderBy('value', 'ASC')->first())
                            <span class="label" style="background-color: {{ optional($item->status)->color }}">{{ optional($item->status)->name }} {{ optional($item->status)->id == 3 && !empty($bookDeposit) ? $bookDeposit->value . '%' : ''}}</span>
                        </td>
                        <td class="text-center">
                            @foreach($item->properties as $value)
                                @if($value->key == 'departure_date')
                                    {{ ($value->pivot)->value }}
                                @endif
                            @endforeach
                        </td>
                        <td class="text-title">
                            {{ optional($item->manager)->name }}
                        </td>
                        <td class="text-title">
                            @foreach($item->properties as $value)
                                @if($value->key == 'picking_up_place')
                                    {{ ($value->pivot)->value }}
                                @endif
                            @endforeach
                        </td>
{{--                        <td class="text-center">{{ \Carbon\Carbon::parse($item->created_at)->format(config('settings.format.date')) }}</td>--}}
                        <td class="text-center">
                            @can('BookingController@update')
                            <a href="#" title="{{ __('booking::bookings.add_manager') }}"><button id="{{ $item->id }},{{ $item->manager_id }},{{ $item->payment_method_id }}" data-action="addManager" class="btn btn-manager bg-purple btn-xs"><i class="fa fa-user-plus" aria-hidden="true"></i></button></a>
                            @endcan
                            @can('BookingController@show')
                            <a href="{{ url('/bookings/'.Route::input('module').'/' . $item->id) }}" title="{{ __('message.view') }}"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                            @endcan
                            @can('BookingController@update')
                            <a href="{{ url('/bookings/'.Route::input('module').'/' . $item->id . '/edit') }}" title="{{ __('message.edit') }}"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                            @endcan
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                    <td colspan="12" class="text-right">
                        Hiển thị
                        <select name="record" id="record">
                            @php($total_rec = config('settings.total_rec'))
                            @foreach($total_rec as $item)
                                <option value="{{$item}}" {{ Request::get('hidRecord') == $item ? 'selected' : '' }}>{{$item}}</option>
                            @endforeach
                        </select>
                        dòng/1 trang
                    </td>
                </tfoot>
            </table>
        </div>
        <div class="box-footer clearfix">
            <div class="row">
                <div id="btn-act" class="col-sm-6 text-left">
                    @can('BookingController@destroy')
                        <a href="#" id="delItem" data-action="delItem" class="btn-act btn btn-danger btn-sm" title="{{ __('message.delete') }}">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </a>
                    @endcan
                </div>
                <div class="col-sm-6 text-right">
                    {!! $bookings->appends(\Request::except('page'))->render() !!}
                </div>
            </div>
        </div>
    </div>
    @include('booking::bookings.cancel-modal')
@endsection
@include('booking::bookings.index-script')