@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datepicker/datepicker3.css') }}">
@endsection
@section('scripts-footer')
    <script type="text/javascript" src="{{ asset('plugins/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}" ></script>
    <script type="text/javascript">
        $(function() {
            $('.btn-dropdown').on('click', () => {
                $('.dropdown-filter').toggle();
            });
            $('#table-copy').resizableColumns();
            //Date range picker with time picker
            $('.datepicker').datepicker({
                autoclose: true,
                language: '{{ app()->getLocale() }}',
                format: '{{ config('settings.format.date_js') }}'
            });
            $('.cancel-button').click(function () {
                const modal = $('.cancel-modal');
                modal.find('form').attr('action', $(this).data('href'));
                modal.modal('show');
            });

            let from = '';//moment().subtract(29, 'days');
            @if(!empty(Request::get('from')))
                from = moment('{{ Request::get('from') }}');
                    @endif
            let to =  '';//moment();
            @if(!empty(Request::get('to')))
                to = moment('{{ Request::get('to') }}');
            @endif
            $('#daterange-btn').daterangepicker(
                {
                    ranges   : {
                        '{{ __('reports.today') }}'   : [moment(), moment()],
                        '{{ __('reports.yesterday') }}'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        '{{ __('reports.last_7_days') }}' : [moment().subtract(6, 'days'), moment()],
                        '{{ __('reports.last_30_days') }}': [moment().subtract(29, 'days'), moment()],
                        '{{ __('reports.this_month') }}'  : [moment().startOf('month'), moment().endOf('month')],
                        '{{ __('reports.last_month') }}'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: from,
                    endDate  : to,
                    "locale": {
                        "format": "{{ strtoupper(config('settings.format.date_js')) }}",
                        "separator": " - ",
                        "applyLabel": "{{ __('reports.apply') }}",
                        "cancelLabel": "{{ __('reports.close') }}",
                        "fromLabel": "{{ __('reports.from') }}",
                        "toLabel": "{{ __('reports.to') }}",
                        "customRangeLabel": "{{ __('reports.custom') }}",
                        "daysOfWeek": [
                            "{{ __('reports.Su') }}",
                            "{{ __('reports.Mo') }}",
                            "{{ __('reports.Tu') }}",
                            "{{ __('reports.We') }}",
                            "{{ __('reports.Th') }}",
                            "{{ __('reports.Fr') }}",
                            "{{ __('reports.Sa') }}"
                        ],
                        "monthNames": [
                            "{{ __('reports.january') }}",
                            "{{ __('reports.february') }}",
                            "{{ __('reports.march') }}",
                            "{{ __('reports.april') }}",
                            "{{ __('reports.may') }}",
                            "{{ __('reports.june') }}",
                            "{{ __('reports.july') }}",
                            "{{ __('reports.august') }}",
                            "{{ __('reports.september') }}",
                            "{{ __('reports.october') }}",
                            "{{ __('reports.november') }}",
                            "{{ __('reports.december') }}"
                        ],
                        "firstDay": 1
                    }
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('{{ strtoupper(config('settings.format.date_js')) }}') + ' - ' + end.format('{{ strtoupper(config('settings.format.date_js')) }}'));
                    $('[name=from]').val(start.format('YYYY-MM-DD'));
                    $('[name=to]').val(end.format('YYYY-MM-DD'));
                }
            )
            $('#chkAll').on('click', function () {
                $("input:checkbox").prop('checked', $(this).prop("checked"));
            });
            $('#record').on('change', function () {
                let form = $('#search');
                let record = $(this).val();
                form.find('input[name="hidRecord"]').val(record);
                axios.get(form.attr('action')).then(function (res) {
                    form.submit();
                }).catch(function () {
                    alert('Có lỗi xảy ra vui lòng thử lại!')
                })
                return false;
            });
            $('#btn-act').on('click', '.btn-act', function(e){
                e.preventDefault();
                let action = $(this).data('action');
                ajaxListBooking(action);
            });
            function ajaxListBooking(action){
                let chkId = $("input[name='chkId']:checked");
                let actTxt = '', successAlert = '', classAlert = '';
                switch (action) {
                    case 'delItem':
                        actTxt = 'xóa';
                        successAlert = '{{ trans('booking::bookings.deleted') }}';
                        classAlert = 'alert-danger';
                        break;
                }
                if (chkId.length != 0){
                    let notificationConfirm = 'Bạn có muốn '+actTxt+' hồ sơ này không?';
                    let notification = confirm(notificationConfirm);
                    if (notification){
                        var arrId = '';
                        $("input[name='chkId']:checked").map((val,key) => {
                            arrId += key.value + ',';
                        });
                        axios.get('{{url('/bookings/ajax/')}}/'+action, {
                            params: {
                                ids: arrId
                            }
                        })
                            .then((response) => {
                                // console.log(response);
                                if (response.data.success === 'ok'){
                                    $('#alert').html('<div class="alert '+classAlert+'">' +
                                        '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                                        successAlert +
                                        ' </div>');
                                    {{--window.location = '{{ url('/bookings/tour') }}'--}}
                                    location.reload(true);
                                }
                            })
                            .catch((error) => {
                                console.log(error);
                            })
                    }
                }else{
                    let notificationAlert = 'Vui lòng chọn hồ sơ để '+actTxt+'!';
                    alert(notificationAlert);
                }
            }
            $('.btn-manager').on('click', function(e){
                e.preventDefault();
                let bookid = $(this).attr('id');
                showModal(bookid);
            });
            function showModal(bookid) {
                var arrIds = bookid.split(',');
                axios.get('{{url('/bookings/ajax/loadManagerModal')}}', {
                    params: {
                        id: arrIds[0],
                        manager_id: arrIds[1],
                        payment_method_id: arrIds[2]
                    }
                })
                .then((response) => {
                    $('#divManagerContain').html(response.data);
                    $('#myModal').modal('show');
                })
                .catch((error) => {
                    console.log(error);
                });
            }
        });
    </script>
    <script type="text/javascript">
        function saveManager(e) {
            let action = $('#btnSaveManager').data('action');
            axios.get('{{url('/bookings/ajax')}}/'+action, {
                params: {
                    managerId:$("#manager").val(),
                    bookId:$("#hidBooking").val(),
                    paymentMethodId: $("#hidPayment").val()
                }
            })
            .then((response) => {
                if (response.data.success === 'ok'){
                    $('#myModal').modal("hide");
                    var success = `<div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ trans('booking::bookings.updated') }}
                    </div>`;
                    $('#alert').html(success);
                    location.reload();
                }
            })
            .catch((error) => {
                console.log(error);
            })
        }
    </script>
    <style>
        #myModal .modal-lg{
            width: 700px;
            max-width: 100%;
        }
    </style>
@endsection