@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ $title }}
@endsection
@section('contentheader_title')
    {{ $title }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li class="active">{{ $title }}</li>
    </ol>
@endsection

@section('main-content')
    <div class="box">
        <div class="box-body">
            <ul class="nav nav-tabs">
                <li id="day-active"><a href="">Thống kê theo ngày</a></li>
                <li id="month-active"><a href="">Thống kê theo tháng</a></li>
                <li id="year-active"><a href="">Thống kê theo năm</a></li>
            </ul>
            <div class="tab-content">
                <div id="report-form-day" class="chart tab-pane active">
                    <div class="box-header with-border">
                        <h3 class="box-title"></h3>
                        <div class="box-tools">
                            <form action="" method="get" class="pull-left form-inline" id="report-form">
                                <select name="module" class="form-control input-sm select2" style="width: 130px;">
                                    <option value="">--{{ trans('booking::bookings.services') }}--</option>
                                    @foreach(\Nwidart\Modules\Facades\Module::all() as $key => $item)
                                        @if($item->active)
                                            @continue($key == 'Booking' || $key == 'Theme' || $key == 'News')
                                            <option {{ Request::get('module') == strtolower($key) ? "selected" : "" }} value="{{ strtolower($key) }}"> {{ $item->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <select name="services_id" class="form-control input-sm select2" style="width: 230px;">
                                    <option value="">{{ __('message.please_select') }}</option>
                                    @if(!empty($list_services))
                                        @foreach($list_services as $key => $value)
                                            <option {{ Request::get('services_id') == $key ? "selected" : "" }} value="{{ $key }}"> {{ $value }}</option>
                                        @endforeach
                                    @endif
                                </select>

                                {{--@if(Auth::user()->isAdminCompany() || Auth::user()->isCompanyManagerBooking())--}}
                                {{--<select name="employee_id" class="form-control input-sm">--}}
                                {{--<option value="">--{{ trans('booking::bookings.creator_id') }}--</option>--}}
                                {{--@foreach(\App\User::getUsersByOnlyCompany() as $id => $name)--}}
                                {{--<option {{ Request::get('employee_id') == $id ? "selected" : "" }} value="{{ $id }}"> {{ $name }}</option>--}}
                                {{--@endforeach--}}
                                {{--</select>--}}
                                {{--@endif--}}
                                <select name="company_id" class="form-control input-sm select2">
                                    <option value="">--{{ __('companies.company') }}--</option>
                                    @if(!empty($listCompany))
                                        @foreach($listCompany as $key => $value)
                                            <option {{ Request::get('company_id') == $key ? "selected" : "" }} value="{{ $key }}">{{ $value }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @if(isset($dataBookingByDate))
                                    <div class="input-group">
                                        <button type="button" class="btn btn-default btn-sm pull-right" id="daterange-btn">
                                <span>
                                  <i class="fa fa-calendar"></i> {{ __('reports.date_range') }}
                                </span>
                                            <i class="fa fa-caret-down"></i>
                                        </button>
                                        <input type="hidden" name="from" value="{{ Request::get('from') }}"/>
                                        <input type="hidden" name="to" value="{{ Request::get('to') }}"/>
                                    </div>
                                @elseif(isset($dataBookingByMonth))
                                    <select name="report_year" class="form-control input-sm select2" style="width: 140px;">
                                        <option value="">--{{ __('reports.year') }}--</option>
                                        @foreach(\App\Utils::getYear() as $value)
                                            <option {{ Request::get('report_year') == $value ? "selected" : "" }} value="{{ $value }}"> {{ $value }}</option>
                                        @endforeach
                                    </select>
                                @endif
                                <button type="submit" name="report_company" value="{{ config('booking.report_btn_total') }}" class="btn btn-success btn-sm">{{ __('reports.report_btn_total') }}</button>
                                <button type="submit" name="report_company" value="{{ config('booking.report_btn_one') }}" class="btn btn-warning btn-sm">{{ __('reports.report_btn_one') }}</button>
                            </form>
                        </div>
                    </div>
                    <div class="box-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <p><i class="fa fa-fw fa-check"></i> {{ $error }}</p>
                                @endforeach
                            </div>
                        @endif
                        {{ \App\ChartJs::init() }}
                        {{ \App\ChartJs::lineChart('report-date', $dataBooking, '70vh') }}
                    </div>
                    @isset($dataFinance)
                        <div class="box-body table-responsive no-padding">
                        <table id="table-copy" class="table--layout table table-condensed table-bordered">
                            <thead>
                            <tr class="active">
                                <th>{{ __('Ngày') }}</th>
                                <th class="text-right">{{ __('booking::bookings.total_price') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($dataFinance as $key => $val)
                                <tr>
                                    <td>{{ $key }}</td>
                                    <td class="text-bold text-danger text-right">{{ number_format($val) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    @endisset
                </div>
            </div>
        </div>
    </div>
    {{--@dump($listCompany)--}}
    {{--@dd($route_module)--}}
@endsection
@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}">
@endsection
@section('scripts-footer')
    <script type="text/javascript" src="{{ asset('plugins/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
    <script type="text/javascript">
        var url = window.location.href;
        var true_url = '';
        if (url.search('booking-number') !== -1){
            true_url = '{{ url('bookings/reports/booking-number') }}';
        }else if(url.search('booking-people') !== -1){
            true_url = '{{ url('bookings/reports/booking-people') }}';
        }else{
            true_url = '{{ url('bookings/reports/finance') }}';
        }

        $('#day-active a').attr('href', true_url);
        $('#month-active a').attr('href', true_url+'/month');
        $('#year-active a').attr('href', true_url+'/year');

        if (url.search('/month') !== -1) {
            true_url = true_url+'/month';
            $('#month-active').addClass('active');
            $('#month-active a').attr('href', true_url);
        }else if(url.search('/year') !== -1){
            true_url = true_url+'/year';
            $('#year-active').addClass('active');
            $('#year-active a').attr('href', true_url);
        }else{
            $('#day-active').addClass('active');
            $('#day-active a').attr('href', true_url);
        }

        @if(!empty($route_module))
            let route_module = '/{{ $route_module }}';
            $('#report-form').attr('action', true_url+route_module);
        @else
            $('#report-form').attr('action', true_url);
        @endif

        $("[name='module']").change(function () {
            var option = $(this).val();
            $('#report-form').attr('action', true_url+'/'+option);
            $("[name='services_id']").html('<option value="">{{ __('message.loading') }}</option>');
            $.ajax({
                url: '{{ url('bookings/ajax/getServiceReport') }}',
                type: 'get',
                dataType: 'json',
                data: {module: option},
                success: function (data) {
                    $("[name='services_id']").html('<option value="">{{ __('message.please_select') }}</option>');
                    $.each(data, (key, item) =>  {
                        $("[name='services_id']").append('<option value="'+key+'">'+item+'</option>');
                    })
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function(){
            let from = moment().subtract(29, 'days');
            @if(!empty(Request::get('from')))
            from = moment('{{ Request::get('from') }}');
            @endif
            let to =  moment();
            @if(!empty(Request::get('to')))
                to = moment('{{ Request::get('to') }}');
            @endif
            $('#daterange-btn').daterangepicker(
                {
                    ranges   : {
//                        'Hôm nay'   : [moment(), moment()],
//                        'Hôm qua'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        '{{ __('reports.last_7_days') }}' : [moment().subtract(6, 'days'), moment()],
                        '{{ __('reports.last_30_days') }}': [moment().subtract(29, 'days'), moment()],
                        '{{ __('reports.this_month') }}'  : [moment().startOf('month'), moment().endOf('month')],
                        '{{ __('reports.last_month') }}'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: from,
                    endDate  : to,
                    "locale": {
                        "format": "{{ strtoupper(config('settings.format.date_js')) }}",
                        "separator": " - ",
                        "applyLabel": "{{ __('reports.apply') }}",
                        "cancelLabel": "{{ __('reports.close') }}",
                        "fromLabel": "{{ __('reports.from') }}",
                        "toLabel": "{{ __('reports.to') }}",
                        "customRangeLabel": "{{ __('reports.custom') }}",
                        "daysOfWeek": [
                            "{{ __('reports.Su') }}",
                            "{{ __('reports.Mo') }}",
                            "{{ __('reports.Tu') }}",
                            "{{ __('reports.We') }}",
                            "{{ __('reports.Th') }}",
                            "{{ __('reports.Fr') }}",
                            "{{ __('reports.Sa') }}"
                        ],
                        "monthNames": [
                            "{{ __('reports.january') }}",
                            "{{ __('reports.february') }}",
                            "{{ __('reports.march') }}",
                            "{{ __('reports.april') }}",
                            "{{ __('reports.may') }}",
                            "{{ __('reports.june') }}",
                            "{{ __('reports.july') }}",
                            "{{ __('reports.august') }}",
                            "{{ __('reports.september') }}",
                            "{{ __('reports.october') }}",
                            "{{ __('reports.november') }}",
                            "{{ __('reports.december') }}"
                        ],
                        "firstDay": 1
                    }
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('{{ strtoupper(config('settings.format.date_js')) }}') + ' - ' + end.format('{{ strtoupper(config('settings.format.date_js')) }}'));
                    $('[name=from]').val(start.format('YYYY-MM-DD'));
                    $('[name=to]').val(end.format('YYYY-MM-DD'));
                }
            )
        });
    </script>
@endsection