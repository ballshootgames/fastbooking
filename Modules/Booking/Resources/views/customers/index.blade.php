@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __('booking::customers.customer') }}
@endsection
@section('contentheader_title')
    {{ __('booking::customers.customer') }}
@endsection
@section('contentheader_description')
    
@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li class="active">{{ __('booking::customers.customer') }}</li>
    </ol>
@endsection
@section('main-content')
    <div id="alert"></div>
    <div class="statis">
        <div class="row">
        </div>
    </div>
    <div class="box box-primary">
        <div class="box-header">
            @can('CustomersController@store')
                <a href="{{ url('/bookings/customers/create') }}" class="btn btn-success btn-sm" title="{{ __('message.new_add') }}">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                </a>
            @endcan
            <div class="pull-right">
                {!! Form::open(['method' => 'GET', 'url' => '/bookings/customers', 'class' => 'pull-left form-inline', 'role' => 'search', 'id' => 'search'])  !!}
                @include('booking::customers.search')
                {!! Form::close() !!}
                <a href="{{ url('/bookings/customers/import-view') }}" class="btn btn-primary btn-sm" title="{{ trans('booking::customers.import_excel') }}">
                    <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                </a>
                <a href="{{ url('/bookings/customers/export-customer') }}" class="btn btn-warning btn-sm" title="{{ trans('booking::customers.export_excel') }}">
                    <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                </a>
            </div>
        </div>
        @php($index = ($customers->currentPage()-1)*$customers->perPage())
        <div class="box-body table-responsive no-padding">
            <table id="table-copy" class="table--layout table table-condensed table-bordered">
                <thead>
                    <tr class="active">
                        <th class="text-center" style="width: 3.5%;">{{ trans('message.index') }}</th>
                        <th class="text-center" style="width: 2.5%;">
                            <input type="checkbox" name="chkAll" id="chkAll"/>
                        </th>
                        <th style="width: 25%;">@sortablelink('name', trans('booking::customers.name'))</th>
                        <th style="width: 10%;">{{ trans('booking::customers.customer_type') }}</th>
                        <th style="width: 12%;">@sortablelink('phone', trans('booking::customers.phone'))</th>
                        <th style="width: 23%;">@sortablelink('email', trans('booking::customers.email'))</th>
                        <th class="text-center" style="width: 8%;">@sortablelink('gender', 'Giới tính')</th>
                        <th class="text-center" style="width: 8%;">@sortablelink('created_at', __('message.created_at'))</th>
                        <th style="width: 9%"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($customers as $item)
                        <tr>
                            <td class="text-center">{{ ++$index }}</td>
                            <td class="text-center">
                                <input type="checkbox" name="chkId" id="chkId" value="{{ $item->id }}" data-id="{{ $item->id }}"/>
                            </td>
                            <td>{{ $item->name }}</td>
                            <td>{{ optional($item->customerType)->name }}</td>
                            <td>{{ $item->phone }}</td>
                            <td>{{ $item->email }}</td>
                            <td class="text-center">{{ ($item->gender && $item->gender==1) ? "Nam":"Nu" }}</td>
                            <td class="text-center">{{ \Carbon\Carbon::parse($item->created_at)->format(config('settings.format.date')) }}</td>
                            <td class="text-center">
                                @can('CustomersController@show')
                                <a href="{{ url('/bookings/customers/' . $item->id) }}" title="{{ __('message.view') }}"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                @endcan
                                @can('CustomersController@update')
                                <a href="{{ url('/bookings/customers/' . $item->id . '/edit') }}" title="{{ __('message.edit') }}"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <td colspan="12" class="text-right">
                        Hiển thị
                        <select name="record" id="record">
                            @php($total_rec = config('settings.total_rec'))
                            @foreach($total_rec as $item)
                                <option value="{{$item}}" {{ Request::get('hidRecord') == $item ? 'selected' : '' }}>{{$item}}</option>
                            @endforeach
                        </select>
                        dòng/1 trang
                    </td>
                </tfoot>
            </table>
        </div>
        <div class="box-footer">
            <div class="row">
                <div id="btn-act" class="col-sm-6 text-left">
                    @can('CustomersController@destroy')
                        <a href="#" id="delItem" data-action="delItem" class="btn-act btn btn-danger btn-sm" title="{{ __('message.delete') }}">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </a>
                    @endcan
                </div>
                <div class="col-sm-6 text-right">
                    {!! $customers->appends(\Request::except('page'))->render() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts-footer')
    <script type="text/javascript">
        $(function() {
            $('#chkAll').on('click', function () {
                $("input:checkbox").prop('checked', $(this).prop("checked"));
            });
        });
        $('#record').on('change', function () {
            let form = $('#search');
            let record = $(this).val();
            form.find('input[name="hidRecord"]').val(record);
            axios.get(form.attr('action')).then(function (res) {
                form.submit();
            }).catch(function () {
                alert('Có lỗi xảy ra vui lòng thử lại!')
            })
            return false;
        });

        $('#btn-act').on('click', '.btn-act', function(e){
            e.preventDefault();
            let action = $(this).data('action');
            ajaxListCustomer(action);
        });

        function ajaxListCustomer(action){
            let chkId = $("input[name='chkId']:checked");
            let actTxt = '', successAlert = '', classAlert = '';
            switch (action) {
                case 'delItem':
                    actTxt = 'xóa';
                    successAlert = '{{ trans('booking::bookings.deleted') }}';
                    classAlert = 'alert-danger';
                    break;
            }
            if (chkId.length != 0){
                let notificationConfirm = 'Bạn có muốn '+actTxt+' hồ sơ này không?';
                let notification = confirm(notificationConfirm);
                if (notification){
                    var arrId = '';
                    $("input[name='chkId']:checked").map((val,key) => {
                        arrId += key.value + ',';
                    });
                    axios.get('{{url('/bookings/customer/ajax/')}}/'+action, {
                        params: {
                            ids: arrId
                        }
                    })
                        .then((response) => {
                            if (response.data.success === 'ok'){
                                if (response.data.fails !== '')
                                {
                                    $('#alert').html('<div class="alert '+classAlert+'">' +
                                        '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                                        successAlert + '<br> Khách hàng: ' + response.data.fails + ' có đơn hàng không xoá được.' +
                                        ' </div>');

                                } else
                                {
                                    $('#alert').html('<div class="alert '+classAlert+'">' +
                                        '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                                        successAlert +
                                        ' </div>');
                                }
{{--                                window.location = '{{ url('/bookings/customers') }}'--}}
                                location.reload(true);
                            } else
                            {
                                if (response.data.fails !== '')
                                {
                                    $('#alert').html('<div class="alert '+classAlert+'">' +
                                        '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                                        '{{ trans('booking::bookings.deleted_fail') }}' + '<br> Khách hàng: ' + response.data.fails + ' có đơn hàng không xoá được.' +
                                        ' </div>');

                                } else
                                {
                                    $('#alert').html('<div class="alert '+classAlert+'">' +
                                        '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                                        '{{ trans('booking::bookings.deleted_fail') }}' +
                                        ' </div>');
                                }
                                {{--                                window.location = '{{ url('/bookings/customers') }}'--}}
                                location.reload(true);
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                        })
                }
            }else{
                let notificationAlert = 'Vui lòng chọn hồ sơ để '+actTxt+'!';
                alert(notificationAlert);
            }
        }
    </script>
@endsection