@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __('message.customer.import') }}
@endsection
@section('contentheader_title')
    {{ __('message.customer.import') }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li><a href="{{ url('/bookings/customers') }}">{{ __('message.customer.import') }}</a></li>
        <li class="active">{{ __('message.import_excel') }}</li>
    </ol>
@endsection

@section('main-content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __('message.customer.import') }}</h3>
            <div class="box-tools">
                <a href="{{url('/bookings/customers/download-file-mau')}}" class="btn btn-success btn-sm">
                    <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                    <span class="hidden-xs"> {{ __('message.customer.download_example') }}</span>
                </a>
            </div>   
        </div>

        {!! Form::open(['url' => '/bookings/customers/list-customer-import', 'class' => 'form-horizontal', 'files' => true]) !!}

        <div class="box-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <p><i class="fa fa-fw fa-check"></i> {{ $error }}</p>
                    @endforeach
                </div>
            @endif
            <div class="col-sm-offset-3 col-sm-6">
                <table class="table-layout table table-striped table-bordered">
                    <tr>
                        <td>{{ trans('message.customer.import_excel') }}</td>
                        <td>
                            <div class="">
                                <div class="input-group inputfile-wrap ">
                                    <input type="text" class="form-control input-sm" readonly="">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-danger btn-sm">
                                            <i class=" fa fa-upload"></i>
                                            Upload
                                        </button>
                                        <input class="form-control input-sm" accept=".xlsx" id="file" name="attachment" type="file">
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="imgprev-wrap fileprev-wrap" style="display:none">
                                    <input type="hidden" name="filehidden">
                                    <span class="file-preview"></span>
                                    <i class="fa fa-trash text-danger" style="margin-top: 7px;"></i>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="box-footer">
            {!! Form::submit(__('message.import.name'), ['class' => 'btn btn-primary']) !!}
            <a href="{{ url('/customers') }}" class="btn btn-default">{{ __('message.close') }}</a>
        </div>
        {!! Form::close() !!}
    </div>
@endsection