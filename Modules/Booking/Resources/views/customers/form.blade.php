<div class="box-body">
    @if ($errors->any())
	    <div class="alert alert-danger">
	        @foreach ($errors->all() as $error)
	            <p><i class="fa fa-fw fa-check"></i> {{ $error }}</p>
	        @endforeach
	    </div>
	@endif
    <table class="table-layout table table-striped table-bordered">
        <tbody>
            <tr>
                <td>{{ trans('booking::customers.code') }}</td>
                <td style="border-right-width: 0">
                    <div class="{{ $errors->has('code') ? 'has-error' : ''}}">
                        {!! Form::text('code', !empty($customer->code) ? $customer->code : $code, ['class' => 'form-control input-sm']) !!}
                        {!! $errors->first('code', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
                <td style="border-right-width: 0; border-left-width: 0"></td>
                <td style="border-left-width: 0"></td>
            </tr>
            <tr>
                <td>{{ trans('booking::customers.name') }} <span class="label-required"></span></td>
                <td>
                    <div class="{{ $errors->has('name') ? 'has-error' : ''}}">
                        {!! Form::text('name', null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
                        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
                <td>{{ trans('booking::customers.gender') }}</td>
                <td>
                    <div class="{{ $errors->has('gender') ? ' has-error' : ''}}">
                        <label for="boy">
                            {!! Form::radio('gender', 1, isset($customer) && $customer->gender===1?true:false, ['class' => '', 'id' => 'boy']) !!}
                            {{ __('message.user.gender_male') }}
                        </label>&nbsp;
                        <label for="girl">
                            {!! Form::radio('gender', 0, isset($customer) && $customer->gender===0?true:false, ['class' => '', 'id' => 'girl']) !!}
                            {{ __('message.user.gender_female') }}
                        </label>
                        {!! $errors->first('gender', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>{{ trans('message.customer.organization') }}</td>
                <td>
                    <div class="{{ $errors->has('organization') ? 'has-error' : ''}}">
{{--                        <select class="form-control input-sm">--}}
{{--                            @foreach($organization as $val)--}}
{{--                                <option value="{{ $val }}" {{ isset($customer) && $customer->organization == $val ? 'selected' : '' }}>{{ $val }}</option>--}}
{{--                            @endforeach--}}
{{--                        </select>--}}
                        {!! Form::select('office_id', $offices, null, ['class' => 'form-control input-sm select2']) !!}
                        {!! $errors->first('office_id', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
                <td>{{ trans('message.customer.position') }}</td>
                <td>
                    <div class="{{ $errors->has('position') ? 'has-error' : ''}}">
                        {!! Form::text('position', null, ['class' => 'form-control input-sm']) !!}
                        {!! $errors->first('position', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>{{ trans('booking::customers.customer_type') }}</td>
                <td>
                    <div class="{{ $errors->has('customer_type_id') ? 'has-error' : ''}}">
                    {!! Form::select('customer_type_id', $customerTypes,null, ['class' => 'form-control input-sm select2']) !!}
                    {!! $errors->first('customer_type_id', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
                <td>{{ trans('booking::customers.email') }}  <span class="label-required"></span></td>
                <td>
                    <div class="{{ $errors->has('email') ? 'has-error' : ''}}">
                        {!! Form::email('email', null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
                        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>{{ trans('booking::customers.phone') }} <span class="label-required"></span></td>
                <td>
                    <div class="{{ $errors->has('phone') ? 'has-error' : ''}}">
                        {!! Form::text('phone', null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
                        {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
                <td>{{ trans('booking::customers.fax') }}</td>
                <td>
                    <div class="{{ $errors->has('fax') ? 'has-error' : ''}}">
                        {!! Form::text('fax', null, ['class' => 'form-control input-sm']) !!}
                        {!! $errors->first('fax', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>{{ trans('booking::customers.nationality') }}</td>
                <td>
                    <div class="{{ $errors->has('nationality_id') ? 'has-error' : ''}}">
                        {!! Form::select('nationality_id', $nationality,null, ['class' => 'form-control input-sm select2', 'id' => 'nationality_id']) !!}
                        {!! $errors->first('nationality_id', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
                <td>{{ trans('booking::customers.city') }}</td>
                <td>
                    <div class="{{ $errors->has('city_id') ? 'has-error' : ''}}">
                        {!! Form::select('city_id', $cities, isset($city_id) ? $city_id : null, ['class' => 'form-control input-sm select2', 'id' => 'city_id']) !!}
                        {!! $errors->first('city_id', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>{{ trans('booking::customers.district') }}</td>
                <td>
                    <div class="{{ $errors->has('district_id') ? 'has-error' : ''}}">
                        {!! Form::select('district_id', $districts, isset($district_id) ? $district_id : null, ['class' => 'form-control input-sm select2', 'id' => 'district_id']) !!}
                        {!! $errors->first('district_id', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
                <td> {{ trans('booking::customers.ward') }} </td>
                <td>
                    <div class="{{ $errors->has('ward_id') ? 'has-error' : ''}}">
                        {!! Form::select('ward_id', $wards, null, ['class' => 'form-control input-sm select2', 'id' => 'ward_id']) !!}
                        {!! $errors->first('ward_id', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>{{ trans('booking::customers.address') }}</td>
                <td colspan="3">
                    <div class="{{ $errors->has('address') ? 'has-error' : ''}}" style="display: flex;">
                        {!! Form::text('address', null, ['class' => 'form-control input-sm']) !!}
                        {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>{{ trans('booking::customers.permanent_address') }} </td>
                <td colspan="3">
                    <div class="{{ $errors->has('permanent_address') ? 'has-error' : ''}}">
                        {!! Form::text('permanent_address', null, ['class' => 'form-control input-sm']) !!}
                        {!! $errors->first('permanent_address', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>{{ trans('booking::customers.cmnd') }} </td>
                <td>
                    <div class="{{ $errors->has('cmnd') ? ' has-error' : ''}}">
                        {!! Form::number('cmnd', null, ['class' => 'form-control input-sm']) !!}
                        {!! $errors->first('cmnd', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
                <td>{{ trans('booking::customers.cmnd_date') }} </td>
                <td>
                    <div class="{{ $errors->has('cmnd_date') ? ' has-error' : ''}}">
                        {!! Form::text('cmnd_date', isset($cmnd_date) ? $cmnd_date : null, ['class' => 'form-control input-sm datepicker']) !!}
                        {!! $errors->first('cmnd_date', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>{{ trans('booking::customers.cmnd_location') }}</td>
                <td>
                    <div class="{{ $errors->has('cmnd_location') ? ' has-error' : ''}}">
                        {!! Form::text('cmnd_location', null, ['class' => 'form-control input-sm']) !!}
                        {!! $errors->first('cmnd_location', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
                <td>{{ trans('booking::customers.bithplace') }}</td>
                <td>
                    <div class="{{ $errors->has('bithplace') ? ' has-error' : ''}}">
                        {!! Form::text('bithplace', null, ['class' => 'form-control input-sm']) !!}
                        {!! $errors->first('bithplace', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>{{ trans('booking::customers.website') }}</td>
                <td>
                    <div class="{{ $errors->has('website') ? 'has-error' : ''}}">
                        {!! Form::text('website', null, ['class' => 'form-control input-sm']) !!}
                        {!! $errors->first('website', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
                <td>{{ trans('booking::customers.facebook') }}</td>
                <td>
                    <div class="{{ $errors->has('facebook') ? 'has-error' : ''}}">
                        {!! Form::text('facebook', null, ['class' => 'form-control input-sm']) !!}
                        {!! $errors->first('facebook', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>{{ trans('booking::customers.twitter') }}</td>
                <td>
                    <div class="{{ $errors->has('twitter') ? 'has-error' : ''}}">
                        {!! Form::text('twitter', null, ['class' => 'form-control input-sm']) !!}
                        {!! $errors->first('twitter', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
                <td>{{ trans('booking::customers.linkedin') }}</td>
                <td>
                    <div class="{{ $errors->has('linkedin') ? 'has-error' : ''}}">
                        {!! Form::text('linkedin', null, ['class' => 'form-control input-sm']) !!}
                        {!! $errors->first('linkedin', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>{{ trans('booking::customers.zalo') }}</td>
                <td>
                    <div class="{{ $errors->has('zalo') ? 'has-error' : ''}}">
                        {!! Form::text('zalo', null, ['class' => 'form-control input-sm']) !!}
                        {!! $errors->first('zalo', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
                <td>{{ trans('booking::customers.file') }}</td>
                <td>
                    <div class="{{ $errors->has('file') ? 'has-error' : ''}}">
                        <div class="input-group inputfile-wrap ">
                            <input type="text" class="form-control input-sm" readonly>
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-danger btn-sm">
                                    <i class=" fa fa-upload"></i>
                                    {{ __('message.upload') }}
                                </button>
                                {!! Form::file('file', array_merge(['class' => 'form-control input-sm', "accept" => "file/*", 'id' => 'file'])) !!}
                            </div>
                        </div>
                        {!! $errors->first('file', '<p class="help-block">:message</p>') !!}
                        <div class="clearfix"></div>
                        <div class="imgprev-wrap fileprev-wrap" style="display:{{ !empty($customer->file)?'block':'none' }}">
                            <input type="hidden" name="filehidden"/>
                            <span class="file-preview">{{ !empty($customer->file)? str_replace('public/file/------','',preg_replace_array('/[0-9_]+/', [''], $customer->file)) :'' }}</span>
                            <i class="fa fa-trash text-danger" style="margin-top: 7px;"></i>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>{{ trans('booking::customers.note') }}</td>
                <td colspan="3">
                    <div class="{{ $errors->has('note') ? 'has-error' : ''}}">
                        {!! Form::textarea('note', null, ['class' => 'form-control input-sm', 'rows' => 5]) !!}
                        {!! $errors->first('note', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <div style="margin-top: 10px;">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : __('message.save'), ['class' => 'btn btn-primary']) !!}
        <a href="{{ $prevUrl }}" class="btn btn-default">{{ __('message.cancel') }}</a>
        <input type="hidden" id="hidUrl" name="hidUrl" value={{ $prevUrl }}>
{{--        <a href="{{ url('/bookings/customers') }}" class="btn btn-default">{{ __('message.close') }}</a>--}}
    </div>
</div>
@section('scripts-footer')
    <link rel="stylesheet" href="{{ asset('plugins/datepicker/datepicker3.css') }}">
    <script type="text/javascript" src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}" ></script>
    <script>
        $(function () {
            $(".select2").select2({ width: '100%' });
            $('.datepicker').datepicker({
                autoclose: true,
                language: '{{ app()->getLocale() }}',
                format: '{{ config('settings.format.date_js') }}'
            });
            $('#nationality_id').change(function () {
                $('#city_id').html('<option>{{ __('message.loading') }}</option>');
                $('#district_id').html('<option>{{ __('message.please_select') }}</option>');
                $('#ward_id').html('<option value="0">{{ __('message.please_select') }}</option>');
                let nationality_id = $(this).val();
                axios.get('{{ url('bookings/customer/ajax/getListCities?nationality_id=') }}' + nationality_id)
                    .then(function (response) {
                        const data = response.data;
                        $('#city_id').html('<option value="0">{{ __('message.please_select') }}</option>');
                        $.each(data, function (key, value) {
                            $('#city_id').append('<option value="'+key+'">'+value+'</option>');
                        })
                    })
                    .catch(function (error) {
                        console.log(error)
                    })
            });

            $('#city_id').change(function () {
                $('#district_id').html('<option>{{ __('message.loading') }}</option>');
                $('#ward_id').html('<option value="0">{{ __('message.please_select') }}</option>');
                let city_id = $(this).val();
                axios.get('{{ url('bookings/customer/ajax/getListDistricts?city_id=') }}' + city_id)
                    .then(function (response) {
                        const data = response.data;
                        $('#district_id').html('<option>{{ __('message.please_select') }}</option>');
                        $.each(data, function (key, value) {
                            $('#district_id').append('<option value="'+key+'">'+value+'</option>');
                        })
                    })
                    .catch(function (error) {
                        console.log(error);
                    })
            });

            $('#district_id').change(function () {
                $('#ward_id').html('<option value="0">{{ __('message.loading') }}</option>');
                let district_id = $(this).val();
                axios.get('{{ url('bookings/customer/ajax/getListWards?district_id=') }}' + district_id)
                    .then(function (response) {
                        const data = response.data;
                        $('#ward_id').html('<option value="0">{{ __('message.please_select') }}</option>');
                        $.each(data, function (key, value) {
                            $('#ward_id').append('<option value="'+key+'">'+value+'</option>');
                        })
                    })
                    .catch(function (error) {
                        console.log(error)
                    })
            });

            $('#file').change(function () {
                let file = document.querySelector('#file').files[0];
                if (/\.(doc|docx|xlsx|xls|pdf)$/i.test(file.name)){
                    $('.fileprev-wrap').css('display','block');
                    $('.fileprev-wrap').find('input[type=hidden]').val('');
                    $('.inputfile-wrap').find('input[type=text]').val(file.name);
                    $('.file-preview').text(file.name);
                } else {
                    alert('Vui lòng upload file có đuôi DOC,DOCX,XLS,XLSX,PDF');
                    document.querySelector('#file').value = '';
                    $('.fileprev-wrap').find('input[type=hidden]').val('');
                }
            });
            $('.fileprev-wrap .fa-trash').click(function () {
                var preview = document.querySelector('span.file-preview');

                if(confirm('{{ __('message.confirm_delete') }}')){
                    $('.fileprev-wrap').find('input[type=hidden]').val(1);
                    preview = '';
                    $('.fileprev-wrap').css('display','none');
                    $('.inputfile-wrap').find('input[type=text]').val('');
                }
            })
        });
    </script>
@endsection