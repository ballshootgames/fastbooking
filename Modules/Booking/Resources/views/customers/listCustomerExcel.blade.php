@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{trans('message.import.list_customer_import')}}
@endsection
@section('contentheader_title')
    {{trans('message.import.list_customer_import')}}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li><a href="{{ url("/bookings/customers") }}">{{ __('booking::customers.customer') }}</a></li>
        <li class="active">{{ __('message.import.list_customer_import') }}</li>
    </ol>
@endsection
@section('main-content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">{{ __('message.lists') }}</h3>
            <div class="box-tools">
                <a href="{{url('/bookings/customers') }}" style="margin-right: 5px"
                   class="btn btn-warning btn-sm pull-left">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs">{{ trans('message.lists') }}</span>
                </a>
            </div>
        </div>

    {{-- @php($index = ($importCustomers->currentPage()-1)*$importCustomers->perPage()) --}}
    @php($index = 0)
    <div class="box-body table-responsive no-padding">
        <table class="table table-striped">
            <tbody>
                <tr>
                    <th class="text-center">{{ trans('message.index') }}</th>
                    <th>{{trans('message.customer.organization')}}</th>
                    <th>{{trans('message.customer.name')}}</th>
                    <th>{{trans('message.customer.position')}}</th>
                    <th>{{trans('message.customer.phone')}}</th>
                    <th>{{trans('message.customer.email')}}</th>

                </tr>
            @foreach($importCustomers as $item)
                <tr>
                    <td class="text-center">{{ ++$index }}</td>
                    <td>{{ $item["organ"] }} </td>
                    <td>{{ $item["full_name"] }} </td>
                    <td>{{ $item["position"] }} </td>
                    <td>{{ $item["phone"]}}</td>
                    <td>{{ $item["email"]}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="box-footer">
            <a href="{{ url('/bookings/customers/save-data-import') }}" class="btn btn-primary">{{ __('message.save') }}</a>
            <a href="{{ url('/bookings/customers') }}" class="btn btn-default">{{ __('message.close') }}</a>
        </div>

    </div>
</div>

@endsection
