<input type="text" style="width: 250px; margin-bottom: 5px;" value="{{\Request::get('search')}}" class="form-control input-sm" name="search" placeholder="{{ __('message.search_keyword') }}">
<div class="input-group" style="width: 50px;">
    <span class="input-group-btn">
        <button class="btn btn-default btn-sm" type="submit" style="margin-bottom: 5px;">
            <i class="fa fa-search"></i>  {{ __('message.search') }}
        </button>
    </span>
</div>
<input type="hidden" name="hidRecord" value="{{ Request::has('record') ? Request::get('record') : config('settings.total_rec')[0] }}">