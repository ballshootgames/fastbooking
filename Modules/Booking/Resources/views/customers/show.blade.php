@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __('booking::customers.customer') }}
@endsection
@section('contentheader_title')
    {{ __('booking::customers.customer') }}
@endsection
@section('contentheader_description')
    
@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li><a href="{{ url('/bookings/customers') }}">{{ __('booking::customers.customer') }}</a></li>
        <li class="active">{{ __("message.detail") }}</li>
    </ol>
@endsection
@section('main-content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __("message.detail") }}</h3>
            <div class="box-tools">
                @if (strpos($prevUrl, url('/bookings/customers?') )!==false)
                    <a href="{{ $prevUrl }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.lists') }}</span></a>
                @else
                    <a href="{{ url('/bookings/customers') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.lists') }}</span></a>
                @endif

                @can('CustomersController@update')
                {!! Form::open([
                    'method'=>'GET',
                    'url' => '/bookings/customers/'.$customer->id.'/edit',
                    'style' => 'display:inline'
                ]) !!}
                <input type="hidden" id="hidUrl" name="hidUrl" value={{ $prevUrl }}>
                {!! Form::button('<i class="fa fa-pencil-square-o" aria-hidden="true"></i> <span class="hidden-xs">'.__('message.edit').'</span>', array(
                        'type' => 'submit',
                        'class' => 'btn btn-primary btn-sm',
                        'title' => __('message.edit'),
                        'onclick'=>'return true'
                ))!!}
                {!! Form::close() !!}
{{--                <a href="{{ url('/bookings/customers/' . $customer->id . '/edit') }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.edit') }}</span></a>--}}
                @endcan
                @can('CustomersController@destroy')
                {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['/bookings/customers', $customer->id],
                    'style' => 'display:inline'
                ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> <span class="hidden-xs">'.__('message.delete').'</span>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-sm',
                            'title' => __('message.delete'),
                            'onclick'=>'return confirm("'.__('message.confirm_delete').'")'
                    ))!!}
                {!! Form::close() !!}
                @endcan
            </div>
        </div>
        <div class="box-body table-responsive">
            <table class="table-layout table table-striped table-bordered">
                <tbody>
                    <tr>
                        <td> {{ trans('message.customer.organization') }} </td>
                        <td> {{ $customer->organization }} </td>
                        <td> {{ trans('message.customer.position') }} </td>
                        <td> {{ $customer->position }} </td>
                    </tr>
                    <tr>
                        <td> {{ trans('booking::customers.name') }} </td>
                        <td> {{ $customer->name }} </td>
                        <td> {{ trans('booking::customers.gender') }} </td>
                        <td> {{ $customer->textGender }} </td>
                    </tr>
                    <tr>
                        <td> {{ trans('booking::customers.email') }} </td>
                        <td> {{ $customer->email }} </td>
                        <td> {{ trans('booking::customers.phone') }} </td>
                        <td> {{ $customer->phone }} </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="bg-warning"><a href="#" data-toggle="collapse" data-target="#collapseHistory" aria-expanded="false" aria-controls="collapseHistory">Chi tiết</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="box-body table-responsive">
            <div class="collapse" id="collapseHistory" style="border: 0px;">
                <div class="well" style="border: 0px; border-radius: 0px; padding: 0px;">
                    <table class="table-layout table table-striped table-bordered">
                        <tr>
                            <td> {{ trans('booking::customers.code') }} </td>
                            <td colspan="3"> {{ $customer->code }} </td>
                        </tr>
                        <tr>
                            <td> {{ trans('booking::customers.customer_type') }} </td>
                            <td> {{ $customerType }} </td>
                            <td> {{ trans('booking::customers.nationality') }} </td>
                            <td> {{ $nationality }} </td>
                        </tr>
                        <tr>
                            <td> {{ trans('booking::customers.address') }} </td>
                            <td> {{ $customer->address }} </td>
                            <td> {{ trans('booking::customers.fax') }} </td>
                            <td> {{ $customer->fax }} </td>
                        </tr>
                        <tr>
                            <td> {{ trans('booking::customers.bithplace') }} </td>
                            <td> {{ $customer->bithplace }} </td>
                            <td> {{ trans('booking::customers.permanent_address') }} </td>
                            <td> {{ $customer->permanent_address }} </td>
                        </tr>
                        <tr>
                            <td> {{ __('CMND') }} </td>
                            <td colspan="3">
                                @if(!empty($customer->cmnd))
                                    {{ __('Số') }}: {{ $customer->cmnd }},
                                    {{ trans('booking::customers.cmnd_date') }}:
                                    {{ Carbon\Carbon::parse($customer->cmnd_date)->format(config('settings.format.date')) }},
                                    {{ trans('booking::customers.cmnd_location') }}: {{ $customer->cmnd_location }}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td> {{ trans('booking::customers.website') }} </td>
                            <td> {{ $customer->website }} </td>
                            <td> {{ trans('booking::customers.facebook') }} </td>
                            <td> {{ $customer->facebook }} </td>
                        </tr>
                        <tr>
                            <td> {{ trans('booking::customers.twitter') }} </td>
                            <td> {{ $customer->twitter }} </td>
                            <td> {{ trans('booking::customers.linkedin') }} </td>
                            <td> {{ $customer->linkedin }} </td>
                        </tr>
                        <tr>
                            <td> {{ trans('booking::customers.zalo') }} </td>
                            <td> {{ $customer->zalo }} </td>
                            <td> {{ trans('booking::customers.file') }} </td>
                            <td>
                                @if(!empty($customer->file))
                                    <a class="btn btn-primary btn-md" href="{{ asset(Storage::url($customer->file)) }}" download><i class="fa fa-download" aria-hidden="true"></i> {{ str_replace('public/file/------','',preg_replace_array('/[0-9_]+/', [''], $customer->file)) }}</a>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td> {{ trans('booking::customers.note') }} </td>
                            <td colspan="3"> {{ $customer->note }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        @if($bookings!=null && $bookings!=='' && $bookings->count()>0)
            <div class="box-body table-responsive" style="padding-bottom: 0px;">
                <table class="table--layout table table-condensed table-bordered">
                    <tr>
                        <td colspan="4" class="text-danger" style="font-size: 15px; font-weight: bold;">Dịch vụ đã đặt</td>
                    </tr>
                </table>
            </div>
            @php($index = ($bookings->currentPage()-1)*$bookings->perPage())
            <div class="box-body table-responsive" style="padding-top: 0px;">
                <table id="table-copy"  class="table--layout table table-condensed table-bordered">
                    <thead>
                    <tr>
                        <th class="text-center" style="width: 3.5%;">{{ trans('message.index') }}</th>
                        <th style="width: 7%;">@sortablelink('code', trans('booking::bookings.code'))</th>
                        <th style="width: 43%;">Dịch vụ</th>
                        <th class="text-center" style="width: 10%;">{{ trans('booking::bookings.total_number') }}</th>
                        <th class="text-center" style="width: 9%;">@sortablelink('status_id', __('booking::bookings.status_id'))</th>
                        <th class="text-right" style="width: 8%;">@sortablelink('total_price', __('booking::bookings.total_price'))</th>
                        <th class="text-center" style="width: 12%;">{{ trans('booking::bookings.departure_date') }}</th>
                        <th class="text-center" style="width: 8%;">@sortablelink('created_at', __('message.created_at'))</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($bookings as $item)
                        @php($detail = $item->detail->bookingable())
                        @php($detail = $detail->loadMorph()->first())
                        @if($detail!=null && $detail->deleted_at==null)
                        <tr>
                            <td class="text-center">{{ ++$index }}</td>
                            <td>{{ $item->code }}</td>
                            <td>{{ $detail->name }}</td>
                            <td class="text-center">
                                @if($item->getTotalAdult()>0)
                                    <span class="text-bold text-purple">{{ $item->getTotalAdult() }}</span>
                                    <i class=" fa fa-male text-purple" style="font-size: 1.6em"></i>
                                @endif
                                @if($item->getTotalChildren() >0)
                                    <span class="text-bold text-maroon">{{ $item->getTotalChildren() }}</span>
                                    <i class=" fa fa-child text-maroon" style="font-size: 1.2em"></i>
                                @endif
                                @if($item->getTotalBaby()>0)
                                    <span class="text-bold">{{ $item->getTotalBaby() }}</span>
                                    <img src="{{ asset('img/resources/assets/img/baby.png') }}" style="width: 10px;">
                                @endif
                            </td>
                            <td class="text-center">
                                <span class="label" style="background-color: {{ optional($item->status)->color }}">{{ optional($item->status)->name }}</span>
                            </td>
                            <td class="text-bold text-danger text-right">{{ number_format($item->total_price) }}</td>
                            <td class="text-center">
                                @foreach($item->properties as $value)
                                    @if($value->key == 'departure_date')
                                        {{ ($value->pivot)->value }}
                                    @endif
                                @endforeach
                            </td>
                            <td class="text-center">{{ \Carbon\Carbon::parse($item->created_at)->format(config('settings.format.date')) }}</td>
                        </tr>
                        @endif
                    @endforeach
                    </tbody>
                    <tfoot>
                        <td colspan="12" class="text-right">
                            Hiển thị
                            <select name="record" id="record">
                                @php($total_rec = config('settings.total_rec'))
                                @foreach($total_rec as $item)
                                    <option value="{{$item}}" {{ Request::get('hidRecord') == $item ? 'selected' : '' }}>{{$item}}</option>
                                @endforeach
                            </select>
                            dòng/1 trang
                        </td>
                    </tfoot>
                </table>
            </div>
            <div class="box-footer clearfix" style="margin-right: 15px;">
                <div class="row">
                    <div class="text-right">
                        {!! $bookings->appends(\Request::except('page'))->render() !!}
                    </div>
                </div>
            </div>
            {!! Form::open(['method' => 'GET', 'url' => '/bookings/customers/'. $customer->id, 'class' => 'pull-left form-inline', 'role' => 'search', 'id' => 'search'])  !!}
            <input type="hidden" name="hidRecord" value="{{ Request::has('record') ? Request::get('record') : config('settings.total_rec')[0] }}">
            {!! Form::close() !!}
        @endif
    </div>
@endsection
@section('scripts-footer')
    <script type="text/javascript">
        $(function() {
            $('#record').on('change', function () {
                let form = $('#search');
                let record = $(this).val();
                form.find('input[name="hidRecord"]').val(record);
                axios.get(form.attr('action')).then(function (res) {
                    form.submit();
                }).catch(function () {
                    alert('Có lỗi xảy ra vui lòng thử lại!')
                })
                return false;
            });
        });
    </script>
@endsection