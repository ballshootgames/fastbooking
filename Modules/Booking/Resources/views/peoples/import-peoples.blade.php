@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __('booking::bookings.booking') }}
@endsection
@section('contentheader_title')
    {{ __('booking::bookings.passenger') }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li><a href="{{ url('/bookings/'.Route::input('module')) }}">{{ __('booking::bookings.booking') }}</a></li>
        <li class="active">{{ __("message.detail") }}</li>
    </ol>
@endsection
@section('main-content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __('booking::booking_peoples.import') }}</h3>
            <div class="box-tools">
                <a href="{{url('/bookings/ajax/downloadTemplateExcel')}}" class="btn btn-success btn-sm">
                    <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                    <span class="hidden-xs"> {{ __('booking::booking_peoples.download_template') }} </span>
                </a>
            </div>
        </div>

        {!! Form::open(['url' => '/bookings/ajax/importExcel', 'class' => 'form-horizontal', 'files' => true]) !!}
        @csrf
        <div class="box-header with-border">
            @if ($errors->any())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <p><i class="fa fa-fw fa-check"></i> {{ $error }}</p>
                    @endforeach
                </div>
            @endif
            <input type="hidden" id="hidSubmit" name="hidSubmit">
            <input type="hidden" id="hidUrl" name="hidUrl" value="{{ $hidUrl }}">
            <input type="hidden" id="bookid" name="bookid" value="{{ $bookid }}">

            <div class="form-group {{ $errors->has('attachment') ? 'has-error' : ''}}">
                {!! Form::label('attachment', trans('booking::booking_peoples.import_excel'), ['class' => 'col-md-3 control-label '.('%required%' == 'required' ? 'label-required' : '')]) !!}
                <div class="col-md-6">
                    {!! Form::file('attachment',['accept'=>'.xlsx', 'style' => 'display:inline-block;padding-top:9px;font:-webkit-small-control;']) !!}
                    {!! Form::submit(trans('booking::booking_peoples.load'), ['class' => 'btn btn-primary', 'id'=>'loadExcel']) !!}
{{--                    <a id="loadExcel" href="#" data-action="loadExcel" class="btn btn-primary">{{ trans('booking::booking_peoples.load') }}</a>--}}
                    {!! $errors->first('attachment', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
        <div class="table-responsive" style="padding: 10px;">
            <table id="table-copy" class="table--layout table table-condensed table-bordered dataTable" role="grid">
                <thead>
                <tr class="active">
                    <td class="text-center" style="width: 3.8%;">{{ trans('message.index') }}</td>
                    <td> {{ trans('booking::booking_peoples.name') }} </td>
                    <td class="text-center"> {{ trans('booking::booking_peoples.year') }} </td>
                    <td> {{ trans('booking::booking_peoples.id_number') }} </td>
                    <td> {{ trans('booking::booking_peoples.picking_place') }} </td>
                    <td class="text-center"> {{ trans('booking::booking_peoples.gender') }} </td>
                    <td> {{ trans('booking::booking_peoples.phone') }} </td>
                </tr>
                </thead>
                <tbody>
                @php($index=0)
                @foreach($peoples as $item)
                    <tr>
                        <td class="text-center">{{ ++$index }}</td>
                        <td>{{ $item->name }}</td>
                        <td class="text-center">{{ date('Y', strtotime($item->birthday)) }}</td>
                        <td>{{ $item->id_number }}</td>
                        <td>{{ $item->picking_place }}</td>
                        <td class="text-center">{{ $item->gender==1?'Nam':'Nữ' }}</td>
                        <td>{{ $item->phone }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="box-footer">
            {!! Form::submit(trans('message.save'), ['class' => 'btn btn-primary', 'id' => 'saveExcel']) !!}
            <a href="{{ $hidUrl }}" class="btn btn-default">{{ __('message.close') }}</a>
        </div>
        {!! Form::close() !!}
    </div>
@endsection
@section('scripts-footer')
    <script type="text/javascript">
        $('#loadExcel').on('click', function(e){
            // e.preventDefault();
            $('#hidSubmit').val('loadExcel');
        });
        $('#saveExcel').on('click', function(e){
            // e.preventDefault();
            $('#hidSubmit').val('saveExcel');
        });
    </script>
@endsection