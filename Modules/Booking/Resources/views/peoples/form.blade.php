<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title" id="form_title"></h3>
    </div>
    <div class="box-body">
        <div id="submitError" class="alert alert-danger" style="display: none;">
        </div>
        <input type="hidden" id="hidBooking" value={{ $booking->id }}>
        <table class="table-layout table table-striped table-bordered">
            <tbody>
                <tr>
                    <td class="col-md-3">
                        {{ trans('booking::booking_peoples.name') }} (<span class="label-required"></span>)
                    </td>
                    <td class="col-md-9">
                        <div>
                            {!! Form::text('name', null, ['id' => 'name', 'class' => 'form-control input-sm', 'required' => 'required']) !!}
                            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-3">
                        {{ trans('booking::booking_peoples.birthday') }} (<span class="label-required"></span>)
                    </td>
                    <td class="col-md-9">
                        <div>
                            {!! Form::text('birthday', null, ['id' => 'birthday', 'class' => 'form-control input-sm datepicker', 'required' => 'required']) !!}
                            {!! $errors->first('birthday', '<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-3">
                        {{ trans('booking::booking_peoples.id_number') }}
                    </td>
                    <td class="col-md-9">
                        <div>
                            {!! Form::text('id_number', null, ['id' => 'id_number', 'class' => 'form-control input-sm']) !!}
                            {!! $errors->first('id_number', '<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-3">
                        {{ trans('booking::booking_peoples.picking_place') }}
                    </td>
                    <td class="col-md-9">
                        <div>
                            {!! Form::text('picking_place', null, ['id' => 'picking_place', 'class' => 'form-control input-sm']) !!}
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-3">
                        {{ trans('booking::booking_peoples.gender') }}
                    </td>
                    <td class="col-md-9">
                        <div id="gender">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-3">
                        {{ trans('booking::booking_peoples.phone') }}
                    </td>
                    <td class="col-md-9">
                        <div>
                            {!! Form::text('phone', null, ['id' => 'phone', 'class' => 'form-control input-sm']) !!}
                            {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="box-footer">
        <input type="hidden" id="hidPeople">
        <a href="#" id="btn-save" data-action="updatePeople" class="btn btn-primary" title="{{ __('message.save') }}">{{ __('message.save') }}</a>
        <a href="{{ Url::current() }}" class="btn btn-default" data-dismiss="modal">{{ __('message.close') }}</a>
    </div>
</div>