<div id="alert"></div>
<div class="statis">
    <ul>
        <li style="margin:15px;">Có {{ !empty($totalAdult) ? $totalAdult : '0' }} người lớn</li>
    </ul>
</div>
<div class="box box-primary">
    <div class="box-header">
        <div class="row">
            <div class="col-sm-2">
                @can('BookingPeopleController@store')
                    <a href="#" data-action="addPeople" class="btn btn-add btn-success btn-sm" title="{{ __('message.add') }}">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                    </a>
                @endcan
            </div>
            <div class="col-sm-10">
                <div class="pull-right">
                    <div class="input-group" style="width: 200px;">
                        <input id="search" type="text" value="{{\Request::get('search')}}" class="form-control input-sm" name="search" placeholder="{{ __('message.search_keyword') }}">
                        <span id="btn-search" class="input-group-btn">
                            <a href="#" id="searchItem" data-action="searchItem" class="btn btn-search btn-default btn-sm" title="{{ __('message.submit') }}">
                                <i class="fa fa-search" aria-hidden="true"></i>
                            </a>
                            @can('BookingPeopleController@store')
{{--                                <a href="#" id="importItem" data-action="importItem" class="btn btn-search btn-success btn-sm" title="{{ __('message.import') }}" style="margin-left: 5px;">--}}
{{--                                    <i class="fa fa-file-excel-o" aria-hidden="true"></i>--}}
{{--                                </a>--}}
                                <a href="{{ url('/bookings/ajax/importItem?bookid='.$booking->id.'&hidUrl='.url::current()) }}" class="btn btn-success btn-sm" title="{{ trans('message.import.name') }}" style="margin-left: 5px;">
                                    <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                </a>
                            @endcan
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @php($index = 0)
    <div class="box-body table-responsive no-padding">
        <table id="table-copy" class="table--layout table table-condensed table-bordered dataTable" role="grid">
            <thead>
            <tr class="active">
                <td class="text-center" style="width: 3.8%;">{{ trans('message.index') }}</td>
                <td class="text-center" style="width: 3.1%;">
                    <input type="checkbox" name="chkAll" id="chkAll"/>
                </td>
                <td> {{ trans('booking::booking_peoples.name') }} </td>
                <td class="text-center"> {{ trans('booking::booking_peoples.year') }} </td>
                <td> {{ trans('booking::booking_peoples.id_number') }} </td>
                <td> {{ trans('booking::booking_peoples.picking_place') }} </td>
                <td class="text-center"> {{ trans('booking::booking_peoples.gender') }} </td>
                <td> {{ trans('booking::booking_peoples.phone') }} </td>
                <td style="width: 5.8%;"></td>
            </tr>
            </thead>
            <tbody>
                @foreach($peoples as $item)
                    <tr>
                        <td class="text-center">{{ ++$index }}</td>
                        <td class="text-center">
                            <input type="checkbox" name="chkId" id="chkId" value="{{ $item->id }}" data-id="{{ $item->id }}"/>
                        </td>
                        <td>{{ $item->name }}</td>
                        <td class="text-center">{{ date('Y', strtotime($item->birthday)) }}</td>
                        <td>{{ $item->id_number }}</td>
                        <td>{{ $item->picking_place }}</td>
                        <td class="text-center">{{ $item->gender==1?'Nam':'Nữ' }}</td>
                        <td>{{ $item->phone }}</td>
                        <td class="text-center">
                            @can('StatusController@update')
                                <a href="#" id={{ $item->id }} data-action="editPeople" class="btn btn-edit btn-primary btn-xs" title="{{ __('message.edit') }}" style="margin-left: 5px;">
                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </a>
                            @endcan
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="box-footer clearfix">
        <div class="row">
            <div id="btn-act" class="col-sm-6 text-left">
                @can('BookingPeopleController@destroy')
                    <a href="#" id="delPeoplesItem" data-action="delPeoplesItem" class="btn-act btn btn-danger btn-sm" title="{{ __('message.delete') }}">
                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                    </a>
                @endcan
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                @include ('booking::peoples.form')
            </div>
        </div>
    </div>
</div>

@section('scripts-footer')
    <link rel="stylesheet" href="{{ asset('plugins/datepicker/datepicker3.css') }}">
    <script type="text/javascript" src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}" ></script>
    <style>
        #myModal .modal-lg{
            width: 800px;
            max-width: 100%;
        }
    </style>
    <script type="text/javascript">
        $('#table-copy').resizableColumns();
        $(function() {
            $('#chkAll').on('click', function () {
                $("input:checkbox").prop('checked', $(this).prop("checked"));
            });
            $('.datepicker').datepicker({
                autoclose: true,
                language: '{{ app()->getLocale() }}',
                format: '{{ config('settings.format.date_js') }}'
            });
        });

        $('#btn-search').on('click', '.btn-search', function(e){
            e.preventDefault();
            let key = $("#search").val();
            let action = $(this).data('action');
            if (action==='searchItem')
            {
                ajaxSearch(key);
            }
            else
            {
                ajaxImportExcel(action);
            }
        });

        $('.btn-edit').on('click', function(e){
            e.preventDefault();
            let action = $(this).data('action');
            let id = $(this)[0].id;
            ajaxShowModel(action, id);
        });

        $('.btn-add').on('click', function(e){
            e.preventDefault();
            let action = $(this).data('action');
            showAddModel(action);
        });

        var html ="";

        function showAddModel(action) {
            $('#myModal').on('show.bs.modal', function(e) {
                target = e.target;
                if (target && target.className==="modal fade")
                {
                    modal = $(this);
                    html = "";
                    $('#submitError').css("display", "none");
                    modal.find('#form_title').html(`{{ trans('message.new_add') }}`);
                    modal.find('#hidPeople').val(0);
                    modal.find('#name').val("");
                    var str = formattedDate(new Date());
                    modal.find('#birthday').val(str);
                    modal.find('#id_number').val("");
                    modal.find('#picking_place').val("");
                    var gender = `<label for="boy">
                                    <input id="boy" name="customer_gender" type="radio" value="1" checked>
                                    {{ __('message.user.gender_male') }}
                                </label>
                                <label for="girl">
                                    <input id="girl" name="customer_gender" type="radio" value="0">
                                    {{ __('message.user.gender_female') }}
                                </label>{!! $errors->first('gender', '<p class="help-block">:message</p>') !!}`;
                    modal.find('#gender').html(gender);
                    modal.find('#phone').val("");
                    modal.find('#btn-save').attr('data-action', action);
                }
            });
            $('#myModal').modal('show');
        }

        function ajaxShowModel(action, id){
            axios.get('{{url('/bookings/ajax')}}/'+action, {
                params: {
                    id: id
                }
            })
            .then((response) => {
                if (response.data.success === 'ok'){
                    var people = response.data.data;
                    $('#myModal').on('show.bs.modal', function(e) {
                        target = e.target;
                        if (target && target.className==="modal fade")
                        {
                            modal = $(this);
                            html = "";
                            $('#submitError').css("display", "none");
                            modal.find('#form_title').html(`{{ trans('message.edit_title') }}`);
                            modal.find("#hidPeople").val(id);
                            modal.find("#name").val(people.name);
                            var str = formattedDate(new Date(people.birthday));
                            modal.find("#birthday").val(str);
                            modal.find("#id_number").val(people.id_number);
                            modal.find("#picking_place").val(people.picking_place);
                            var gender = people.gender == 1 ?
                                `<label for="boy">
                                    <input id="boy" name="customer_gender" type="radio" value="1" checked>
                                    {{ __('message.user.gender_male') }}
                                </label>
                                <label for="girl">
                                    <input id="girl" name="customer_gender" type="radio" value="0">
                                    {{ __('message.user.gender_female') }}
                                </label>`:
                                `<label for="boy">
                                    <input class="" id="boy" name="customer_gender" type="radio" value="1">
                                    {{ __('message.user.gender_male') }}
                                    </label>
                                    <label for="girl">
                                        <input class="" id="girl" name="customer_gender" type="radio" value="0" checked>
                                        {{ __('message.user.gender_female') }}
                                    </label>`;
                            modal.find("#gender").html(gender);
                            modal.find("#phone").val(people.phone);
                        }
                    });
                    $('#myModal').modal('show');
                }
            })
            .catch((error) => {
                console.log(error);
            })
        }

        function ajaxImportExcel(action)
        {
            axios.get('{{url('/bookings/ajax')}}/'+action, {
            })
            .then((response) => {
                if (response.data.success === 'ok'){
                }
            })
            .catch((error) => {
                console.log(error);
            })
        }

        function formattedDate(d = new Date) {
            let month = String(d.getMonth() + 1);
            let day = String(d.getDate());
            const year = String(d.getFullYear());

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return `${day}/${month}/${year}`;
        }

        function ajaxSearch(key){
            var path = window.location.href;
            path = path.substr(0, path.indexOf("?"));
            path = path + '?search=' + key;
            window.location = path;
        }

        $('#btn-act').on('click', '.btn-act', function(e){
            e.preventDefault();
            let action = $(this).data('action');
            ajaxListPeoples(action);
        });

        function ajaxListPeoples(action){
            let chkId = $("input[name='chkId']:checked");
            let actTxt = '', successAlert = '', classAlert = '';
            switch (action) {
                case 'delPeoplesItem':
                    actTxt = 'xóa';
                    successAlert = '{{ trans('booking::booking_peoples.deleted_success') }}';
                    classAlert = 'alert-danger';
                    break;
            }
            if (chkId.length != 0){
                let notificationConfirm = 'Bạn có muốn '+actTxt+' người này không?';
                let notification = confirm(notificationConfirm);
                if (notification){
                    var arrId = '';
                    $("input[name='chkId']:checked").map((val,key) => {
                        arrId += key.value + ',';
                    });
                    axios.get('{{url('/bookings/ajax')}}/'+action, {
                        params: {
                            ids: arrId
                        }
                    })
                    .then((response) => {
                        if (response.data.success === 'ok'){
                            $('#alert').html('<div class="alert '+classAlert+'">' +
                                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                                successAlert +
                                ' </div>');
                            location.reload();
                        }
                    })
                    .catch((error) => {
                        console.log(error);
                    })
                }
            }else{
                let notificationAlert = 'Vui lòng chọn hồ sơ để '+actTxt+'!';
                alert(notificationAlert);
            }
        }

        $('#btn-save').on('click', function(e){
            html = "";
            let action = $(this).data('action');
            let bookid = $("#hidBooking").val();
            let id = $(this).parent().find("#hidPeople").val();
            let name = $("#name").val();
            let birthday = $("#birthday").val();
            let id_number = $("#id_number").val();
            let picking_place = $("#picking_place").val();
            let gender = $('#boy').is(":checked") ? 1 : 0;
            let phone = $('#phone').val();
            axios.get('{{url('/bookings/ajax')}}/'+action, {
                params: {
                    id: id,
                    name:name,
                    birthday:birthday,
                    id_number:id_number,
                    picking_place:picking_place,
                    gender:gender,
                    phone:phone,
                    bookid:bookid
                }
            })
            .then((response) => {
                if (response.data.success === 'ok'){
                    $('#myModal').modal("hide");
                    var success = (action==='updatePeople') ?
                        `<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ trans('booking::booking_peoples.updated_success') }}
                        </div>`:
                        `<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ trans('booking::booking_peoples.created_success') }}
                        </div>`;
                    $('#alert').html(success);
                    location.reload();
                } else {
                    var data = response.data.errors;
                    data.forEach(myFunction);
                    // $('#submitError').html('<i class="fa fa-fw fa-check"></i>' + response.data.success);
                    $('#submitError').css("display", "block");
                }
            })
            .catch((error) => {
                console.log(error);
            })
        });

        function myFunction(item) {
            html += '<p><i class="fa fa-fw fa-check"></i>' + item + '</p>';
            $('#submitError').html(html) ;
        }
    </script>
@endsection
