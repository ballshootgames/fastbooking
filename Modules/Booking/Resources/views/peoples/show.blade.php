@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __('booking::booking_peoples.booking_peoples') }}
@endsection
@section('contentheader_title')
    {{ __('booking::booking_peoples.booking_peoples') }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li><a href="{{ url('/bookings/booking-peoples') }}">{{ __('booking::booking_peoples.booking_peoples') }}</a></li>
        <li class="active">{{ __("message.detail") }}</li>
    </ol>
@endsection
@section('main-content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">{{ __("message.detail") }}</h3>
            <div class="box-tools">
                <a href="{{ url('/bookings/booking-peoples') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ trans('message.lists') }}</span></a>
                @can('BookingPeopleController@update')
                <a href="{{ url('/bookings/booking-peoples/' . $people->id . '/edit') }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.edit') }}</span></a>
                @endcan
                @can('BookingPeopleController@destroy')
                {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['/bookings/booking-peoples', $people->id],
                    'style' => 'display:inline'
                ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> <span class="hidden-xs">'.__('message.delete').'</span>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-sm',
                            'title' => __('message.delete'),
                            'onclick'=>'return confirm("'.__('message.confirm_delete').'")'
                    ))!!}
                {!! Form::close() !!}
                @endcan
            </div>
        </div>
        <div class="box-body table-responsive no-padding">
            <table class="table table-striped">
                <tbody>
                <tr>
                    <th> {{ trans('booking::booking_peoples.name') }} </th>
                    <td> {{ $people->name }} </td>
                </tr>
                <tr>
                    <th> {{ trans('booking::booking_peoples.birthday') }} </th>
                    <td> {{ !empty($people->birthday) ? \Carbon\Carbon::parse($people->birthday)->format(config('settings.format.date')) : null }} </td>
                </tr>
                <tr>
                    <th> {{ trans('booking::booking_peoples.booking_code') }} </th>
                    <td> {{ optional($people->booking)->code }} </td>
                </tr>
                <tr>
                    <th> {{ trans('booking::booking_peoples.created_at') }} </th>
                    <td> {{ $people->created_at }} </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

@endsection