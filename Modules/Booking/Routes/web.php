<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
$subdomain = \URL::getDefaultParameters()?\URL::getDefaultParameters()['subdomain']:"";
$appRoot = config('app.root').".";
if (\URL::getRequest()->getHost()!=$appRoot.config('app.domain') || !empty($subdomain))
    $appRoot = "";

Route::domain('{subdomain}.'.$appRoot.config('app.domain'))->group(function () {
    Route::group(['prefix' => 'bookings', 'middleware' => ['subdomain', 'auth']], function () {
        //Route::get('ajax/{action}', 'AjaxController@index');
        Route::get('ajax/{action}', 'AjaxBookingController@index');
        Route::post('ajax/importExcel', 'AjaxBookingController@importExcel');
        Route::group(['prefix' => 'customers'], function () {
            Route::get('import-view', 'CustomersController@importView');
            Route::get('export-customer', 'CustomersController@exportCustomer');
            Route::post('import-customer', 'CustomersController@importCustomer');
            Route::post('list-customer-import', 'CustomersController@showListImportExcel');
            Route::get('save-data-import', 'CustomersController@saveDataImport');
            Route::get('download-file-mau', 'CustomersController@downloadFileMau')->name('download-file-mau');
            Route::resource('offices', 'OfficeController');
        });
        Route::resource('customers', 'CustomersController');
        Route::resource('customer-types', 'CustomerTypeController');
        Route::resource('statuses', 'StatusController');
        Route::resource('booking-peoples', 'BookingPeopleController');
        Route::resource('booking-sources', 'BookingSourceController');

        Route::get('reports/booking-number/month/{module?}', 'ReportsController@numberBookingByMonth');
        Route::get('reports/booking-number/year/{module?}', 'ReportsController@numberBookingByYear');
        Route::get('reports/booking-people/month/{module?}', 'ReportsController@peopleBookingByMonth');
        Route::get('reports/booking-people/year/{module?}', 'ReportsController@peopleBookingByYear');
        Route::get('reports/finance/month/{module?}', 'ReportsController@financeBookingByMonth');
        Route::get('reports/finance/year/{module?}', 'ReportsController@financeBookingByYear');

        Route::get('reports/booking-number/{module?}', 'ReportsController@numberBookingByDate');
        Route::get('reports/booking-people/{module?}', 'ReportsController@peopleBookingByDate');
        Route::get('reports/finance/{module?}', 'ReportsController@financeBookingByDate');

        Route::get('ajax/{module?}/get-finance-booking', 'AjaxController@getFinanceBookingByDate');
        Route::get('ajax/{module?}/get-filter', 'AjaxController@getFilterFinanceBookingByDate');
        Route::get('customer/ajax/{action}', 'AjaxController@index');

        Route::get('{module}/export', 'BookingController@export');

        Route::resource('{module}', 'BookingController')->parameters([
            '{module}' => 'id'
        ]);
        Route::put('{module}/{id}/cancel', 'BookingController@cancel');

    });
});
