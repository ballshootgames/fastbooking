<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UddateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_peoples', function (Blueprint $table){
            $table->increments('id');
            $table->string('name')->nullable();
            $table->date('birthday')->nullable();
            $table->string('id_number')->nullable();
            $table->string('picking_place')->nullable();
            $table->tinyInteger('gender')->nullable();//0|1
            $table->string('phone')->nullable();
            $table->string('company')->nullable();
            $table->integer('booking_id')->unsigned()->nullable();

            $table->foreign('booking_id')
                ->references('id')
                ->on('bookings')
                ->onDelete('cascade');

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('booking_source', function (Blueprint $table){
            $table->increments('id');
            $table->string('name')->nullable();
            $table->tinyInteger('arrange')->nullable()->unsigned();
            $table->tinyInteger('active')->default(0);
            $table->softDeletes();
            $table->timestamps();
            $table->integer('creator_id')->unsigned()->nullable();
            $table->foreign('creator_id')->references('id')->on('users')->onDelete('set null');
            $table->integer('updator_id')->unsigned()->nullable();
            $table->foreign('updator_id')->references('id')->on('users')->onDelete('set null');
        });

        Schema::table('bookings', function (Blueprint $table){
            $table->tinyInteger('is_vat')->unsigned()->nullable();
            $table->integer('payment_method_id')->unsigned()->nullable();
            $table->integer('source_id')->unsigned()->nullable();

            $table->foreign('source_id')
                ->references('id')
                ->on('booking_source')
                ->onDelete('set null');

            $table->foreign('payment_method_id')
                ->references('id')
                ->on('payment_methods')
                ->onDelete('set null');
        });

        if (Schema::hasColumn('bookings', 'approved')){
            Schema::table('bookings', function (Blueprint $table) {
                $table->dropColumn('approved');
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
//            $table->dropColumn(['source_id', 'payment_method_id', 'baby_number', 'is_vat']);
            $table->dropForeign(['payment_method_id']);
            $table->dropForeign(['source_id']);
        });
        Schema::dropIfExists('booking_source');
        Schema::dropIfExists('booking_peoples');
    }
}
