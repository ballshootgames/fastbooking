<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('code')->nullable();
	        $table->double('total_price');
	        $table->mediumText('note')->nullable();
	        $table->mediumText('cancel_note')->nullable();
	        $table->tinyInteger('approved')->default(0);

	        $table->integer('creator_id')->unsigned()->nullable();
	        $table->integer('customer_id')->unsigned()->nullable();
            $table->integer('manager_id')->unsigned()->nullable();

	        $table->softDeletes();
	        $table->timestamps();

	        $table->foreign('creator_id')
	              ->references('id')
	              ->on('users')
                  ->onDelete('set null');
	        $table->foreign('customer_id')
	              ->references('id')
	              ->on('customers')
                  ->onDelete('set null');

            $table->foreign('manager_id')
                ->references('id')
                ->on('users')
                ->onDelete('set null');
        });
	    Schema::create('booking_detail', function (Blueprint $table) {
		    $table->increments('id');
		    $table->double('price');
		    $table->integer('booking_id')->unsigned();
		    $table->morphs('bookingable');

		    $table->foreign('booking_id')
		          ->references('id')
		          ->on('bookings')
		          ->onDelete('cascade');
	    });
	    Schema::create('booking_properties', function (Blueprint $table) {
		    $table->increments('id');
		    $table->string('key');
		    $table->string('name');
		    $table->string('type');
		    $table->text('data');
		    $table->string('module');
	    });
	    Schema::create('booking_property_values', function (Blueprint $table) {
		    $table->string('value');
		    $table->integer('booking_property_id')->unsigned();
		    $table->integer('booking_id')->unsigned();

		    $table->foreign('booking_property_id')
		          ->references('id')
		          ->on('booking_properties')
		          ->onDelete('cascade');
		    $table->foreign('booking_id')
		          ->references('id')
		          ->on('bookings')
		          ->onDelete('cascade');
	    });
        Schema::create('booking_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('booking_id')->unsigned();
            $table->string('name');
            $table->tinyInteger('item_number')->unsigned()->nullable()->default(0);
            $table->double('price');
            $table->timestamps();
            $table->foreign('booking_id')
                ->references('id')
                ->on('bookings')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::drop('booking_property_values');
	    Schema::drop('booking_properties');
	    Schema::drop('booking_detail');
        Schema::drop('bookings');
        Schema::dropIfExists('booking_items');
    }
}
