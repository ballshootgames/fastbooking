<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCollumnCustomers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::table('customers', function (Blueprint $table) {
            $table->tinyInteger('organization')->nullable();
            $table->string('position')->nullable();
            $table->date('birthday')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::table('customers', function (Blueprint $table) {
            $table->dropColumn(['organization','position','birthday']);
        });
    }
}
