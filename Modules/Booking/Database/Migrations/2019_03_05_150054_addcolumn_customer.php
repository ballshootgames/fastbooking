<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCustomer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table){
            $table->integer('customer_type_id')->unsigned()->nullable();
            $table->foreign('customer_type_id')->references('id')->on('customer_types')->onDelete('set null');

            $table->integer('ward_id')->unsigned()->nullable();
            $table->foreign('ward_id')->references('id')->on('wards')->onDelete('set null');

            $table->integer('nationality_id')->unsigned()->nullable();
            $table->foreign('nationality_id')->references('id')->on('countries')->onDelete('set null');

            $table->string('code')->nullable();
            $table->integer('cmnd')->nullable();
            $table->date('cmnd_date')->nullable();
            $table->string('cmnd_location')->nullable();
            $table->string('bithplace')->nullable();
            $table->text('note')->nullable();
            $table->string('file')->nullable();
            $table->string('fax')->nullable();
            $table->string('twitter')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('website')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->dropColumn("code");
            $table->dropColumn("cmnd");
            $table->dropColumn("cmnd_date");
            $table->dropColumn("cmnd_location");
            $table->dropColumn("bithplace");
            $table->dropColumn("note");
            $table->dropColumn("file");
            $table->dropColumn("fax");
            $table->dropColumn("twitter");
            $table->dropColumn("linkedin");
            $table->dropColumn("website");

            $table->dropForeign(['customer_type_id']);
            $table->dropForeign(['ward_id']);
            $table->dropForeign(['nationality_id']);
            $table->dropColumn(['customer_type_id', 'ward_id', 'nationality_id' ]);

        });
    }
}
