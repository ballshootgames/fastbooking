<?php

namespace Modules\Booking\Database\Seeders;

use App\PaymentMethod;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Booking\Entities\CustomerType;
use Modules\Booking\Entities\Status;

class BookingDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table( 'customer_types' )->insert( [
            [
                'name' => 'Khách VIP',
                'arrange' => 1,
                'active' => 1
            ],
            [
                'name' => 'Khách thường',
                'arrange' => 2,
                'active' => 1
            ],
            [
                'name' => 'Khách trả góp',
                'arrange' => 3,
                'active' => 1
            ]
        ]);
        DB::table( 'statuses' )->insert( [
            [
                'name' => 'Giữ chỗ',
                'arrange' => 1,
                'color' => '#ff0000',
                'active' => 1
            ],
            [
                'name' => 'Đã thanh toán',
                'arrange' => 2,
                'color' => '#66bb6a',
                'active' => 1
            ],
            [
                'name' => 'Thanh toán 50%',
                'arrange' => 3,
                'color' => '#0000a0',
                'active' => 1
            ],
            [
                'name' => 'Huỷ',
                'arrange' => 4,
                'color' => '#ff00ff',
                'active' => 1
            ]
        ]);
        DB::table( 'payment_methods' )->insert( [
            [
                'name' => 'Tiền mặt',
                'code' => 'cash',
                'description' => '<p>Quý khách vui lòng thanh toán tại bất kỳ văn phòng Vietravel trên toàn quốc và các chi nhánh ngoài nước. <a href="https://travel.com.vn/lien-he.aspx" target="_blank">Chi tiết</a></p><p>Xin lưu ý, Quý khách nên liên lạc trước khi đến để biết rõ hơn về giờ làm việc và các hồ sơ cần chuẩn bị khi thanh toán.</p>',
                'arrange' => 1,
                'active' => 1
            ],
            [
                'name' => 'Chuyển khoản',
                'code' => 'transfer',
                'description' => '<table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td style="color:#fc3400;"><strong>PHƯƠNG THỨC THANH TOÁN CHUYỂN KHOẢN :</strong></td></tr><tr><td><p style="margin-left: 10px">-Vietravel chấp nhận thanh toán chuyển khoản từ ATM / Internet Banking / Quầy giao dịch khách hàng</p><p style="margin-left: 10px">-Quý khách sau khi thực hiện việc chuyển khoản vui lòng gửi email đến <a href="mailto:sales@vietravel.com">sales@vietravel.com</a> hoặc gọi tổng đài 1900.1839 để được xác nhận từ công ty chúng tôi.</p><p style="margin-left: 10px">-Vietravel xin gửi thông tin chuyển khoản như bên dưới để Quý khách thanh toán trước thời hạn ghi trên Thông tin đặt tour. Sau thời gian trên nếu Quý khách không chuyển khoản thanh toán, booking sẽ tự động huỷ ra.</p><p style="margin-left: 10px">-Vietravel sẽ không giải quyết các trường hợp booking tự động hủy nếu quý khách không thực hiện đúng các hướng dẫn trên.</p><p style="margin-left: 10px">&nbsp;</p><p style="margin-left: 10px"><b>Chủ tài khoản</b> : Công ty cổ phần Du lịch và Tiếp Thị Giao Thông Vận Tải Việt Nam- Vietravel.</p><p style="margin-left: 10px"><b>Ngân hàng</b> : Ngân hàng Ngoại Thương - Chi nhánh Tp. Hồ Chí Minh.<br>Số tài khoản :<br><b>&nbsp;&nbsp;&nbsp; Tài khoản VNĐ :</b> <font color="#FF0000">0071000012584<br></font><b>&nbsp;&nbsp;&nbsp; Tài khoản USD : </b><font color="#FF0000">0071370089095 </font></p><p style="margin-left: 10px"><b>Chủ tài khoản</b> : Công ty cổ phần Du lịch và Tiếp Thị Giao Thông Vận Tải &nbsp;Việt Nam- Chi Nhánh Hà Nội (Vietravel).<br><b>Ngân hàng</b> : Sở Giao dịch Ngân hàng ngoại thương Việt Nam.<br>Số tài khoản :<br><b>&nbsp;&nbsp;&nbsp; Tài khoản VNĐ :</b> <font color="#FF0000">0011000018933<br></font><b>&nbsp;&nbsp;&nbsp; Tài khoản USD : </b><font color="#FF0000">0011370085787 </font></p><p style="margin-left: 10px"><b>Chủ tài khoản</b> : Công ty cổ phần Du lịch và Tiếp Thị Giao Thông Vận Tải &nbsp;Việt Nam- Chi Nhánh Cần Thơ (Vietravel)<br><b>Ngân hàng</b> : Ngọai Thương - Chi nhánh Cần Thơ<br>Số tài khoản :<br><b>&nbsp;&nbsp;&nbsp; Tài khoản VNĐ :</b> <font color="#FF0000">0111000051599<br></font><b>&nbsp;&nbsp;&nbsp; Tài khoản USD : </b></p><p style="margin-left: 10px"><b>Chủ tài khoản</b> : Công ty cổ phần Du lịch và Tiếp Thị Giao Thông Vận Tải &nbsp;Việt Nam- Chi Nhánh Đà Nẵng (Vietravel)<br><b>Ngân hàng</b> : Đầu tư và phát triển Đà Nẵng<br>Số tài khoản :<br><b>&nbsp;&nbsp;&nbsp; Tài khoản VNĐ :</b> <font color="#FF0000">56110000002184<br></font><b>&nbsp;&nbsp;&nbsp; Tài khoản USD : </b> <font color="#FF0000">56110370054116 </font></p><p style="margin-left: 10px">&nbsp;</p></td></tr> </tbody> </table>',
                'arrange' => 2,
                'active' => 1
            ],
            [
                'name' => 'ATM / Internet Banking',
                'code' => 'vnpay',
                'description' => '<p align="left"><strong><font color="#fc3400">HÌNH THỨC THANH TOÁN BẰNG THẺ ATM/ INTERNET BANKING</font></strong></p><p>Vietravel chấp nhận thanh toán bằng thẻ ATM qua cổng thanh toán 123 pay.</p><p>Hãy đảm bảo bạn đang sử dụng thẻ ATM do ngân hàng trong nước phát hành và đã được kích hoạt chức năng thanh toán trực tuyến.</p><p>Hướng dẫn thanh toán thẻ qua cồng 123 pay : <a href="https://123pay.vn/info/huong-dan/huong-dan" target="_blank">https://123pay.vn/info/huong-dan/huong-dan</a></p>',
                'arrange' => 3,
                'active' => 1
            ],
            [
                'name' => 'Thẻ tín dụng',
                'code' => 'credit',
                'description' => '<p><strong><font color="#fc3400">THANH TOÁN BẰNG THẺ TÍN DỤNG VÀ GHI NỢ QUỐC TẾ</font></strong></p> <p><br>Quý khách sử dụng Thẻ tín dụng hoặc Thẻ ghi nợ quốc tế được phát hành bởi các Ngân hàng, Công ty tài chính và các tổ chức thẻ quốc tế <b>Visa, MasterCard, JCB</b>. Giao dịch thanh toán của quý khách được xử lý an toàn bởi CyberSource, công ty con của tổ chức thẻ quốc tế VISA.<br><br>CyberSource đạt chuẩn <b>PCI DSS</b> là một tiêu chuẩn an ninh thông tin bắt buộc dành cho các doanh nghiệp lưu trữ, truyền tải và xử lý thẻ thanh toán, được quản lý bởi Hội đồng Tiêu chuẩn An toàn Ngành thanh toán thẻ <b>PCI SSC</b> được thành lập bởi các tổ chức thẻ quốc tế. Vietravel sử dụng giao thức HTTPS (giao thức kết hợp giữa giao thức HTTP và giao thức bảo mật SSL) cho toàn bộ website, ứng dụng di động, cùng với CyberSource nhằm đảm bảo thông tin thanh toán của Quý khách được mã hóa trong quá trình truyền tải, xử lý thanh toán thẻ, ngăn cản sự xâm nhập trái phép, giả mạo thông tin.<br><br>Vietravel phối hợp với các tổ chức thẻ triển khai thành công dịch vụ xác thực chủ thẻ giao dịch trực tuyến <b>3-D Secure</b> bao gồm: <b>Verified by Visa, MasterCard SecureCode, J/Secure</b> nhằm gia tăng bảo mật, hạn chế gian lận thanh toán thẻ trực tuyến.<br><br>Sau khi thanh toán thành công, Quý khách sẽ nhận được vé điện tử qua email tự động, nhanh chóng và vẫn đảm bảo an toàn tuyệt đối trong quá trình thanh toán.</p>',
                'arrange' => 4,
                'active' => 1
            ],
            [
                'name' => 'Thanh toán bằng quét QRCode',
                'code' => 'vnpayqr',
                'description' => '<p>Đây là cổng thanh toán cho phép Quý khách thanh toán từ tài khoản ngân hàng thông qua hình thức quét mã QR trên tính năng QR Pay trong ứng dụng Mobile Banking của các Ngân hàng.</p><p>Sau khi Thông tin đặt tour của Quý khách được xác nhận, hệ thống sẽ hiển thị mã QR để Quý khách dùng thiết bị di động quét mã thanh toán.&nbsp;</p>',
                'arrange' => 5,
                'active' => 1
            ],
            [
                'name' => 'Paypal',
                'code' => 'paypal',
                'description' => '<p>Đây là cổng thanh toán cho phép Quý khách thanh toán từ tài khoản ngân hàng thông qua hình thức quét mã QR trên tính năng QR Pay trong ứng dụng Mobile Banking của các Ngân hàng.</p><p>Sau khi Thông tin đặt tour của Quý khách được xác nhận, hệ thống sẽ hiển thị mã QR để Quý khách dùng thiết bị di động quét mã thanh toán.&nbsp;</p>',
                'arrange' => 6,
                'active' => 1
            ]
        ]);
    }
}
