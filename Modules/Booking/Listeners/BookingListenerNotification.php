<?php

namespace Modules\Booking\Listeners;

use Modules\Booking\Events\BookingEvent;
use App\Jobs\SendNotificationToDevice;
use Modules\Booking\Notifications\BookingNotify;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class BookingListenerNotification implements ShouldQueue
{
	public $tries = 2;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BookingEvent  $event
     * @return void
     */
    public function handle(BookingEvent $event)
    {
	    $booking = $event->booking;
	    $booking->load(['customer', 'detail.bookingable']);
	    $userAction = $event->userAction;
	    $usersReceintNotify = $booking->usersReceiveNotify($userAction);
	    //save database notification
        \Notification::send($usersReceintNotify, new BookingNotify($event->type, $booking, $userAction));
        //send notify to device
        $detail = optional($booking->detail)->bookingable;
	    $dataNotify = [
	    	'collapse_key' => $event->type." ".$booking->id,
		    'notification' => [
				'title' => __('bookings.notification_'.$event->type),
			    'body' => __('bookings.notification_body_'.$event->type, [
			        'user' => $userAction? $userAction->name : '',
				    'item' => $detail ? $detail->name : '',
				    'code' => $booking->code,
				    'phone' => optional($booking->customer)->phone,
				    'name' => optional($booking->customer)->name,
			    ]),
				'sound' => true
		    ],
		    'data' => [
		    	'booking_type' => $detail ? $detail->getTable() : '',
		    	'booking_id' => $booking->id,
			    'url' => url('booking-'.$detail->getTable().'/'.$booking->id)
		    ],
	    ];
	    SendNotificationToDevice::dispatch($usersReceintNotify, $dataNotify)->delay(now()->subSeconds(5));
    }
}
