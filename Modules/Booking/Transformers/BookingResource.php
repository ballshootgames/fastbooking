<?php

namespace Modules\Booking\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class BookingResource extends Resource
{
	public static function collection($resource)
	{
		return tap(new BookingCollection($resource), function ($collection) {
			$collection->collects = __CLASS__;
		});
	}
	/**
	 * @var array
	 */
	protected $withoutFields = [];
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request
	 * @return array
	 */
	public function toArray($request)
	{
		return $this->filterFields([
			"id" => $this->id,
			"code" => $this->code,
            "adult_number" => $this->adult_number,
            "child_number" => $this->child_number,
            "baby_number" => $this->baby_number,
			"total_price" => $this->total_price,
//			"picking_up_place" => $this->picking_up_place,
			"note" => $this->note,
			"cancel_note" => $this->cancel_note,
            "customer" => new CustomerResource($this->customer),
			"created_at" => $this->created_at->toDateTimeString(),
			"updated_at" => $this->updated_at->toDateTimeString(),
			"deleted_at" => $this->deleted_at ? $this->deleted_at->toDateTimeString() : $this->deleted_at,
			"detail" => new BookingDetailResource($this->detail),
            "status_id" => $this->status_id,
            "is_vat" => $this->is_vat,
            "payment_method_id" => $this->payment_method_id,
            "source_id" => $this->source_id
		]);
	}
	/**
	 * Set the keys that are supposed to be filtered out.
	 *
	 * @param array $fields
	 * @return $this
	 */
	public function hide(array $fields)
	{
		$this->withoutFields = $fields;
		return $this;
	}
	/**
	 * Remove the filtered keys.
	 *
	 * @param $array
	 * @return array
	 */
	protected function filterFields($array)
	{
		return collect($array)->forget($this->withoutFields)->toArray();
	}

}
