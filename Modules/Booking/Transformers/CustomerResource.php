<?php

namespace Modules\Booking\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class CustomerResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
	    return [
		    'id' => $this->id,
		    'email' => $this->email,
		    'name' => $this->name,
		    'phone' => $this->phone,
		    'gender' => $this->gender,
		    'gender_text' => $this->textGender,
		    "picking_up_place" => $this->picking_up_place,
	    ];
    }
}
