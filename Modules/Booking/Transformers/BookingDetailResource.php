<?php

namespace Modules\Booking\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class BookingDetailResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
	    return [
		    "id" => $this->id,
		    "price" => $this->bookingable->price,
		    "name" => $this->bookingable->name,
		    $this->mergeWhen($this->bookingable->start_time, [
			    'start_time' => $this->bookingable->start_time
		    ]),
		    $this->mergeWhen($this->bookingable->journey, [
			    'journey' => $this->bookingable->journey
		    ]),
		    "item_id" => $this->bookingable->id
	    ];
    }
}
