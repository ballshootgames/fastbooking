<?php

namespace Modules\Booking\Entities;

use App\Company;
use App\ModuleInfo;
use App\Notification;
use App\User;
use Faker\Provider\DateTime;
use Illuminate\Support\Facades\Date;
use Modules\Booking\Events\BookingEvent;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;
use Symfony\Component\Debug\ExceptionHandler;

class Booking extends Model
{
	use Sortable, SoftDeletes;

	protected $dates = ['deleted_at'];
	public $sortable = [
		'id',
        'code',
        'customer_id',
        'total_price',
        'status_id',
		'created_at',
	];
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bookings';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

	public static $defaultParams = [
		'unit_price' => 23000,//ok
		'pay_success_id_status' => 2,//ok
		'pay_success_id_status_50' => 3,//ok
		'is_send_notify_booking_tmp' => 1,//ok
		'minutes_repeat_notify_booking_tmp' => 15//ok
	];
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['code', 'creator_id', 'customer_id', 'total_price', 'note', 'cancel_note', 'status_id', 'is_vat', 'payment_method_id', 'source_id'];


	public function __call($method, $parameters) {
		#i: Relation method resolver - xử lý gọi quan hệ với dịch vụ tương ứng, phụ thuộc vào module trên route
		#Nếu là tour thì gọi services() sẽ liên kết đến Tour, bus thì liên kết đến Bus

		if ( $method === 'services' ) {
            if (\Session::has('moduleBook')){
                $moduleInfo = new ModuleInfo(\Session::get('moduleBook'));
            }elseif(empty(\Route::input('module'))){
				abort(400, 'Tham số {module} không tồn tại trên route!');
			}else{
                $moduleInfo = new ModuleInfo(\Route::input('module'));
            }
			$service = $moduleInfo->getBookingServiceInfo();
			return $this->morphedByMany($service['namespaceModel'], 'bookingable', 'booking_detail');

		}

		#i: No relation found, return the call to parent (Eloquent) to handle it.
		return parent::__call($method, $parameters);
	}

    public function creator(){
    	return $this->belongsTo('App\User', 'creator_id');
    }
	public function customer(){
		return $this->belongsTo('Modules\Booking\Entities\Customer');
	}
	public function detail(){
		return $this->hasOne('Modules\Booking\Entities\BookingDetail');
	}
	public function history(){
		return $this->morphMany('App\LogActivity', 'content');
	}
	public function properties(){
	    return $this->belongsToMany('Modules\Booking\Entities\BookingProperty', 'booking_property_values')->withPivot('value');
    }

    public function status(){
	    return $this->belongsTo('Modules\Booking\Entities\Status', 'status_id');
    }
    public function peoples(){
        return $this->hasMany('Modules\Booking\Entities\BookingPeople', 'booking_id');
    }
    public function source(){
	    return $this->belongsTo('Modules\Booking\Entities\BookingSource', 'source_id');
    }
    public function payment(){
        return $this->belongsTo('App\PaymentMethod', 'payment_method_id');
    }
    public function bookingItems(){
        return $this->hasMany('Modules\Booking\Entities\BookingItem', 'booking_id');
    }

    public function manager(){
        return $this->belongsTo('App\User', 'manager_id');
    }

	public static function boot()
	{
		parent::boot();

		self::creating(function ($model) {
			if(\Auth::check())
				$model->creator_id = \Auth::id();
			$model->code = self::getCodeUnique();
		});
		self::deleted(function($modal){

		});
	}

	public static function getExistPeopleLimitTour($service = null){
	    if (empty($service) || empty($service->limit)) return;

	    $limit = $service->limit;
	    $allTourPeople = null;
	    foreach ($service->bookings as $key){
            if ($key->status->id != 4)
            $allTourPeople += ($key->getTotalAdult() + $key->getTotalChildren() + $key->getTotalBaby());
        }
	    $existTourPeople = $limit - $allTourPeople;

	    return $existTourPeople;
    }

    public function getTotalAdult(){
        $total = 0;
        foreach ($this->bookingItems as $item)
        {
            $total += strcmp($item->name, 'adult_number')==0 ? $item->item_number : 0;
        }
        return $total;
    }

    public function getTotalChildren(){
        $total = 0;
        foreach ($this->bookingItems as $item)
        {
            $total += strcmp($item->name, 'child_number')==0 ? $item->item_number : 0;
        }
        return $total;
    }

    public function getTotalBaby(){
        $total = 0;
        foreach ($this->bookingItems as $item)
        {
            $total += strcmp($item->name, 'baby_number')==0 ? $item->item_number : 0;
        }
        return $total;
    }

	public function getTotalNumberAttribute(){
	    $total = 0;
	    foreach ($this->bookingItems as $item)
        {
            $total += $item->item_number;
        }
		return $total;
	}

	public static function getTotalPrice($service = null)
    {
        if (empty($service)) return;
        $total = 0;
        foreach ($service->bookings as $key){
            $total += ($key->getTotalAdult() * $service->price + $key->getTotalChildren() * $service->price_children + $key->getTotalBaby() * $service->price_baby);
        }
        return $total;
    }

	/**
	 * Get users receive notify - lay danh sach user nhan thong bao notify cua 1 booking
	 * @param $userAction - User create event (create, update, cancel booking)
	 *
	 * @return array
	 */
	public function usersReceiveNotify($userAction = null){
		//Tour or Journey modal
		$detail = optional($this->detail)->bookingable;
		if(!$detail) return null;
		//Người dùng đc phân quyền
		$userManager = $detail ? $detail->managers->pluck('id')->toArray() : [];
		//Người tạo booking
		$userManager[] = $this->creator_id;
		if($detail && $this->departure_date){
			//Hướng dẫn viên
			$guide = $detail->guides()->isGuide($this->departure_date)->first();
			if($guide) $userManager[] = $guide->user_id;
		}
		//remove user id trùng
		$userManager = array_unique($userManager);
		//remove user tạo nên notify này (thêm, cập nhật hoặc hủy)
		if($userAction) {
			if ( ( $key = array_search( $userAction->id, $userManager ) ) !== false ) {
				unset( $userManager[ $key ] );
			}
		}
		return User::whereIn('id', $userManager)->get();
	}
	static function getCodeUnique($length = 6){
		$codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
		$codeAlphabet.= "0123456789";
		$max = strlen($codeAlphabet); // edited

		do{
			$code = "";
			for ($i=0; $i < $length; $i++) {
				$code .= $codeAlphabet[random_int(0, $max-1)];
			}
		}while(Booking::withTrashed()->where('code', $code)->count() != 0);

		return $code;
	}

	const REPORT_TYPE = [
		'NUMBER' => 'number',
		'PEOPLE' => 'people',
		'FINANCE' => 'finance'
	];

    const TYPE_DATE = [
        'DATE' => 'date',
        'MONTH' => 'month',
        'YEAR' => 'year'
    ];
	/**
	 * Report number booking group by Date
	 * @param $report - self::REPORT_TYPE
	 * @param $type - journeys|tours
	 * @param $from - date Y-m-d
	 * @param null $to - date Y-m-d
	 * @param null $item_id - journeys|tours id
	 * @param null $creator_id - (user) creator id
	 * @param null $agent_id - agent id
	 */
	static public function reportBooking($report, $type_date, $module_table = null, $from = null, $to = null, $from_year = null, $service_id = null, $company_id = null, $route_module, $report_company){
	    //prepare
//		if(!empty($from)) $from .= " 00:00:00";
//		if(!empty($to)) $to .= " 23:59:59";
		$user = \Auth::user();

		$selectReport = "";
		switch ($report){
			case self::REPORT_TYPE['NUMBER']:
				$selectReport = "COUNT(bookings.id) as number";
				break;
			case self::REPORT_TYPE['PEOPLE']:
				$selectReport = "SUM(booking_items.item_number) as number";
				break;
			case self::REPORT_TYPE['FINANCE']:
				$selectReport = "SUM(total_price) as number";
				break;
		}

		$select = "";
        $groupBy = "";
		switch ($type_date){
            case self::TYPE_DATE['DATE']:
                $select = \DB::raw('str_to_date(booking_property_values.value, "%d/%m/%Y") as date, '.$selectReport);
                $groupBy = \DB::raw('date');
                break;
            case self::TYPE_DATE['MONTH']:
                $select = \DB::raw('MONTH(str_to_date(booking_property_values.value, "%d/%m/%Y")) as date, '.$selectReport);
                $groupBy = \DB::raw('date');
                break;
            case self::TYPE_DATE['YEAR']:
                $select = \DB::raw('YEAR(str_to_date(booking_property_values.value, "%d/%m/%Y")) as date, '.$selectReport);
                $groupBy = \DB::raw('date');
                break;
        }
		//query
        if (empty($route_module)){
            $bookingData = Booking::select($select)->join('booking_items', 'booking_items.booking_id', '=', 'bookings.id')
                                                    ->join('booking_property_values', 'booking_property_values.booking_id', '=', 'bookings.id')
                                                    ->join('booking_properties', 'booking_property_values.booking_property_id', '=', 'booking_properties.id');
        }else{
            $bookingData = Booking::byRole()->join('booking_items', 'booking_items.booking_id', '=', 'bookings.id')
                                            ->join('booking_property_values', 'booking_property_values.booking_id', '=', 'bookings.id')
                                            ->join('booking_properties', 'booking_property_values.booking_property_id', '=', 'booking_properties.id')
                                            ->select($select);
        }

        $bookingData = $bookingData->where('key', 'departure_date');

        if ($type_date == self::TYPE_DATE['DATE']){
            if (!empty($from)){
                $whereRaw = \DB::raw('str_to_date(booking_property_values.value, "%d/%m/%Y") >= str_to_date("'.$from.'", "%Y-%m-%d")');
                $bookingData = $bookingData->whereRaw($whereRaw);
            }
            if(!empty($to)){
                $whereRaw = \DB::raw('str_to_date(booking_property_values.value, "%d/%m/%Y") <= str_to_date("'.$to.'", "%Y-%m-%d")');
                $bookingData = $bookingData->whereRaw($whereRaw);
            }
        }elseif ($type_date == self::TYPE_DATE['MONTH']){
		    if (!empty($from_year)){
                $whereRaw = \DB::raw('((str_to_date(booking_property_values.value, "%d/%m/%Y") >= str_to_date("'.$from_year.'-01-01 00:00:00", "%Y-%m-%d")) AND (str_to_date(booking_property_values.value, "%d/%m/%Y") <= str_to_date("'.$from_year.'-12-31 23:59:59", "%Y-%m-%d")))');
                $bookingData = $bookingData->whereRaw($whereRaw);
            }
        }else{
		    $curDate = date('Y-m-d H:i:s');
            if (!empty($from_year)){
                $whereRaw = \DB::raw('((str_to_date(booking_property_values.value, "%d/%m/%Y") >= str_to_date("'.$from_year.'-01-01 00:00:00", "%Y-%m-%d")) AND (str_to_date(booking_property_values.value, "%d/%m/%Y") <= str_to_date("'.$curDate.'", "%Y-%m-%d")))');
//                $whereRaw = \DB::raw('(str_to_date(booking_property_values.value, "%d/%m/%Y") >= "'.$from_year.'-01-01 00:00:00")'.' AND '.'(str_to_date(booking_property_values.value, "%d/%m/%Y") <= "'.$curDate.'")');
                $bookingData = $bookingData->whereRaw($whereRaw);
            }
        }

		if(!empty($service_id)){
			$bookingData = $bookingData->whereHas('services', function ($query) use ($module_table, $service_id) {
				$query->withTrashed()->where($module_table.'.id', '=', $service_id);
			});
		}

		if (!empty($report_company)){
		    if ($report_company == config('booking.report_btn_total')){
		        if (!empty($company_id)){
                    //Lấy ra danh sách company và con của nó
                    $companyTree = Company::descendantsAndSelf($company_id)->toTree();
                }else{
                    $companyTree = Company::descendantsAndSelf($user->company_id)->toTree();
                }

                $listCompany = new Company();
		        //Lấy ra danh sách ID của company và con của nó
                $listCompanyId = $listCompany->getListCompanyId($companyTree);

                $bookingData = $bookingData->whereHas( 'creator', function ( $query ) use ($listCompanyId) {
                    $query->withTrashed()->whereIn('company_id', $listCompanyId);
                } );
                if (!\Auth::user()->isEmployeeCompany() && empty($company_id)){
                    $bookingData = $bookingData->orWhereNull('creator_id');
                }
            }else{
                if (!empty($company_id)) {
                    $bookingData = $bookingData->whereHas( 'creator', function ( $query ) use ($company_id) {
                        $query->withTrashed()->where('company_id', $company_id);
                    } );
                }else{
                    $bookingData = $bookingData->whereHas( 'creator', function ( $query ) use ($user) {
                        $query->withTrashed()->where('company_id', $user->company_id);
                    } );
                }
            }
        }

//        $bookingData = $bookingData->groupBy($groupBy)->get();
		$bookingData = $bookingData->groupBy($groupBy)->orderBy($groupBy)->pluck('number', 'date')->all();
        //$bookingData = $bookingData->groupBy($groupBy)->orderBy($groupBy)->get();
		return $bookingData;
	}

	/**
	 * Where by role
	 * @param $query
	 * @param $type: ap dung cho booking: tours|journeys
	 *
	 * @return mixed
	 */
	public function scopeByRole($query)
	{
		if(\Auth::check()) {
			//book dịch vụ
			$query = $query->whereHas('services', function ( $query ) {
				$query->withTrashed();
			});
//			return $query;

			//Booking
            if (\Auth::user()->isEmployeeCompany()){//Nhân viên công ty: hiển thị booking của chính họ tạo và booking họ quản lý
                $companyTree = Company::descendantsAndSelf(\Auth::user()->company_id)->toTree();
                $listCompany = new Company();
                //Lấy ra danh sách ID của company và con của nó
                $listCompanyId = $listCompany->getListCompanyId($companyTree);

                return $query->whereHas( 'creator', function ( $query ) use ($listCompanyId) {
                    $query->whereIn('company_id', $listCompanyId);
                } );
            }
            return $query;
		}
	}

}
