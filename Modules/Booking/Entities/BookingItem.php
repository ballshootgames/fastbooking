<?php

namespace Modules\Booking\Entities;

use Illuminate\Database\Eloquent\Model;

class BookingItem extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'booking_items';
    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    protected $fillable = ['booking_id', 'name', 'item_number', 'price'];

    public function booking(){
        return $this->belongsTo('Modules\Booking\Entities\Booking', 'booking_id');
    }
}
