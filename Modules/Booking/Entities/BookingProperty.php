<?php

namespace Modules\Booking\Entities;

use App\ModuleInfo;
use App\Traits\ImageResize;
use Illuminate\Database\Eloquent\Model;

class BookingProperty extends Model
{
	use ImageResize;
	public static $imageFolder = "booking";
	public static $imageMaxWidth = 1024;
	public static $imageMaxHeight = 1024;

	public $timestamps = false;

	/**
	 * Attributes that should be mass-assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['key', 'name', 'type', 'data', 'module'];
    public static $saveFolderFile = '/files/booking/';

	public function bookings(){
	    return $this->belongsToMany('Modules\Booking\Entities\Booking', 'booking_property_values');
    }

    public static function saveFile($file, $type){
        if ($type == 'image'){;
	        return self::saveImageResize($file);
        }
        return self::_saveFile($file);
    }
    private static function _saveFile($file){
	    if (empty($file)) return;

	    $fileName = time().'_'.$file->getClientOriginalName();
	    $path = self::$saveFolderFile;
	    if(!empty(\URL::getDefaultParameters()['subdomain'])){
		    $path .= "sites/".\URL::getDefaultParameters()['subdomain']."/";
	    }
	    if (!\Storage::disk(config('filesystems.disks.public.visibility'))->has($path)){
		    \Storage::makeDirectory(config('filesystems.disks.public.visibility').$path);
	    }
	    $file->move(public_path('/storage').$path, $fileName);
	    return config('filesystems.disks.public.visibility').$path.$fileName;
    }

    /**
     * get data select in file config
     * @param $id_service //id dịch vụ
     * @param $moduleName //tên module
     * @return mixed
     */
    public static function getDataSelect($id_service = null, $moduleName){
        $moduleInfo = new ModuleInfo($moduleName);
        $service = $moduleInfo->getBookingServiceInfo();
        $data_arr = [];
        foreach ($service['properties'] as $key => $value){
            if ($value['type'] == 'select'){
                $func_select = $value['data']['select'];
                $data_arr['properties['.$value['key'].']'] = $service['namespaceModel']::$func_select($id_service);
            }
        }
        return $data_arr;
    }
}
