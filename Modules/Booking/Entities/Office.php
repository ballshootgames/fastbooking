<?php

namespace Modules\Booking\Entities;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Office extends Model
{
    use Sortable;

    protected $table='offices';

    protected $primaryKey='id';

    protected $sortable = ['name'];

    protected $fillable = ['name','phone','address','email','website','date_establish','active'];
}
