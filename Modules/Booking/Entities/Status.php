<?php

namespace Modules\Booking\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Status extends Model
{
    use Sortable, SoftDeletes;
    protected $table = 'statuses';

    protected $dates = ['deleted_at'];
    public $sortable = [
        'id',
        'name',
        'arrange',
        'active',
        'created_at'
    ];

    protected $primaryKey = 'id';

    protected $fillable = ['name', 'arrange', 'color', 'active'];

    public function bookings(){
        return $this->hasMany('Modules\Booking\Entities\Booking', 'status_id');
    }

    public function user(){
        return $this->belongsTo('App\User', 'creator_id');
    }

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model){
            $model->creator_id = \Auth::user()->id;
            $model->updator_id = \Auth::user()->id;
        });
        self::updating(function ($model){
            $model->updator_id = \Auth::user()->id;
        });
    }
}
