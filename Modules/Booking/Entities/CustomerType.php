<?php

namespace Modules\Booking\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class CustomerType extends Model
{
    use Sortable, SoftDeletes;
    protected $table = "customer_types";

    protected $dates = ['deleted_at'];
    public $sortable = [
        'id',
        'name',
        'arrange',
        'active',
        'created_at'
    ];

    protected $primaryKey = "id";

    protected $fillable = ['name', 'arrange', 'active'];

    public function customer(){
        return $this->hasMany('Module\Entities\Customer');
    }

    public function user(){
        return $this->belongsTo('App\User', 'creator_id');
    }

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model){
            if (\Auth::check() && \Auth::user()->id){
                $model->creator_id = \Auth::user()->id;
                $model->updator_id = \Auth::user()->id;
            }
        });
        self::updating(function ($model){
            $model->updator_id = \Auth::user()->id;
        });
    }
}
