<?php

namespace Modules\Booking\Entities;

use App\Country;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Support\Str;

class Customer extends Model
{
	use Sortable, SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'customers';

    protected $dates = ['deleted_at'];
	public $sortable = [
		'id',
		'name',
		'email',
		'phone',
		'created_at',
		'gender'
	];
    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['code', 'name', 'customer_type_id', 'gender', 'email', 'nationality_id', 'ward_id', 'address',
        'phone', 'permanent_address', 'cmnd', 'cmnd_date', 'cmnd_location', 'bithplace', 'website', 'fax', 'facebook',
        'twitter', 'linkedin', 'zalo', 'note', 'file', 'creator_id', 'organization', 'position', 'birthday','office_id'];

	/**
	 * Text gender: 1 - Name, 0 - Nu
	 * @return string
	 */
	public function getTextGenderAttribute(){
		return $this->gender===1?__('message.user.gender_male'):($this->gender===0?__('message.user.gender_female'):"");
	}

	public function customerType(){
        return $this->belongsTo('Modules\Booking\Entities\CustomerType');
    }

    public function office(){
        return $this->belongsTo('Modules\Booking\Entities\Office');
    }

    public function ward(){
	    return $this->belongsTo('App\Ward');
    }

    public function nationality(){
        return $this->belongsTo('App\Country');
    }

    public function bookings(){
        return $this->hasMany('Modules\Booking\Entities\Booking', 'customer_id');
    }

    static public function uploadFile($file){
        if (empty($file)) return;

        $folder = config("settings.public_file");
        if (\Storage::disk(config('filesystems.disks.public.visibility'))->has($folder)){
            \Storage::makeDirectory(config('filesystems.disks.public.visibility').$folder);
        }

        $timestamp = Carbon::now()->toDateTimeString();
        $fileExt = $file->getClientOriginalExtension();
        $fileName = Str::slug(basename($file->getClientOriginalName(), '.' . $fileExt));
        $pathFile = str_replace([' ', ':'], '-', $folder.$timestamp. '-' .$fileName.'.'.$fileExt);
        $file->storeAs(
            config('filesystems.disks.public.visibility'), $pathFile
        );
        return config('filesystems.disks.public.visibility') . $pathFile;

    }

    /*Auto code customer*/
    static public function autoCode(){
        $code = Customer::latest()->first();
        if (empty($code)){
            $code = 'KH01';
        }else{
            $code = $code['id']+1 ;
            $code = $code < 10 ? '0' . $code : $code;
            $code = 'KH' . $code;
        }
        return $code;
    }

	public static function boot(){
		parent::boot();

        self::creating(function ($model) {
			if(\Auth::check())
				$model->creator_id = \Auth::id();
		});

//        static::deleted(function ($customer){
//            if (!empty($customer->file)){
//                \Storage::delete($customer->file);
//            }
//        });
        self::deleted(function($customer){
            if (!empty($customer->file)){
                \Storage::delete($customer->file);
            }
        });
	}
}
