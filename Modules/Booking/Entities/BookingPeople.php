<?php

namespace Modules\Booking\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BookingPeople extends Model
{
    use SoftDeletes;
    protected $table = 'booking_peoples';

    protected $dates = ['deleted_at'];

    protected $primaryKey = 'id';

    protected $fillable = ['name', 'birthday', 'id_number', 'picking_place', 'gender', 'phone', 'company', 'booking_id'];

    public function booking(){
        return $this->belongsTo('Modules\Booking\Entities\Booking', 'booking_id');
    }
}
