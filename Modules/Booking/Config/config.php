<?php

return [
    'name' => 'Booking',
    'report_btn_total' => 10,
    'report_btn_one' => 1,
    'type_report' => [
        'type_date' => 'date',
        'type_month' => 'month',
        'type_year' => 'year',
    ],
    'payment_method' => [
        'cash' => 'cash',
        'transfer' => 'transfer',
        'vnpay' => 'vnpay',
        'credit' => 'credit',
        'vnpayqr' => 'vnpayqr',
        'paypal' => 'paypal'
    ],
    'payment_status' => [
        'booked' => 1, //'Đang xử lý',
        'paid' => 2, //'Đã thanh toán',
        'pay' => 3, //'Thanh toán {deposit}%',
        'unpaid' => 6, //Chưa thanh toán
        'canceled' => 4 //'Đã huỷ'
    ],
    'booking_status' => [
        1 => 'Đang chờ',
        2 => 'Tiếp nhận',
        3 => 'Đang xử lý',
        4 => 'Kết thúc'
    ]
];
