<?php

namespace Modules\News\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class NewsType extends Model
{
    use Sortable, SoftDeletes;

    protected $table = 'news_type';

    protected $primaryKey = 'id';

    protected $dates = ['deleted_at'];
    protected $sortable = [
        'title',
        'slug',
        'arrange',
        'active',
        'created_at'
    ];

    protected $fillable = ['title', 'slug', 'description', 'parent_id', 'arrange', 'active','meta_keyword'];

    public function news(){
        return $this->hasMany('Modules\News\Entities\News', 'news_type_id');
    }

    public function user(){
        return $this->belongsTo('App\User', 'creator_id');
    }

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model){
            $model->creator_id = \Auth::user()->id;
            $model->updator_id = \Auth::user()->id;
        });
        self::updating(function ($model){
            $model->updator_id = \Auth::user()->id;
        });
    }

    public static function getListNewsTypeToArray($newsType, $parent_id = '', $level = '', $result = []){
        global $result;
        $childNewsType = [];
        foreach ( $newsType as $key => $item){
            if ($item['parent_id'] == $parent_id){
                $childNewsType[] = $item;
                unset($newsType[$key]);
            }
        }
        if ($childNewsType){
            foreach ($childNewsType as $key => $item){
                $result[$item->id] = $level. $item['title'];
                self::getListNewsTypeToArray($newsType, $item['id'], $level.'-- ', $result);
            }
        }
        return $result;
    }

    public function showListNewsType($newsTypes, $parent_id = '', $level = '', $index = 0){
        $index1 = $index2 = 1;
        foreach ($newsTypes as $key => $item){
            if ($item['parent_id'] == $parent_id){
                echo '<tr>';
                if ($level==='')
                {
                    echo '<td>'.($index+1).'</td>';
                    $index++;
                } else if ($level==='-- ')
                {
                    echo '<td>'.$index.'.'.$index1.'</td>';
                    $index1++;
                } else if ($level==='-- -- ')
                {
                    echo '<td>'.$index.'.'.$index1.'.'.$index2.'</td>';
                    $index2++;
                }
                echo '<td class="text-center"><input type="checkbox" name="chkId" id="chkId" value="'.$item->id.'" data-id="'.$item->id.'"/></td>';
                echo '<td>'.$level.$item['title'].'</td>';
                echo '<td>'.$item['slug'].'</td>';
                echo '<td>'.$item['description'].'</td>';
                echo '<td class="text-center">'.(($item->active == config('settings.active') ? '<i class="fa fa-check text-primary"></i>' : '')).'</td>';
                echo '<td class="text-center">'.Carbon::parse($item->created_at)->format(config('settings.format.date')).'</td>';
                echo '<td>'.(optional($item->user)->name).'</td>';
                echo '<td class="text-center">';
                if(\Auth::user()->can("NewsTypeController@show"))
                {
                    echo '<a href="'.url('/news-type/' . $item->id) .'" title="'.trans("message.view").'"><button class="btn btn-info btn-xs" style="margin-right: 5px;"><i class="fa fa-eye" aria-hidden="true"></i></button></a>';
                }

                if(\Auth::user()->can("NewsTypeController@update"))
                {
                    echo '<a href="'.url('/news-type/' . $item->id . '/edit').'" title="'.trans("message.edit").'"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>';
                }
                echo '</td>';
//                echo '<a style="padding-right: 4px" href="'.url('/news-type/' . $item['id']).'" title="'.__('message.show').'"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> '.__('message.view').'</button></a>';
//                echo '<a href="'.url('/news-type/' . $item['id'] . '/edit').'" title="'.__('message.edit').'"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> '.__('message.edit').'</button></a>';
//                echo '<form method="POST" action="'.url('/news-type/'.$item['id']).'" style="display: inline">
//                            <input type="hidden" name="_method" value="delete" />
//                            <input class="_token" name="_token" type="hidden" value="'.csrf_token().'">
//                            <button type="submit" class="com-delete btn btn-danger btn-xs" onclick="return confirm(&quot;Bạn chắc chắn muốn xoá phần từ này?&quot;)" title="Xóa"><i class="fa fa-trash-o" aria-hidden="true"></i> Xóa</button>
//                          </form>';
//                echo '</td>';
                echo '</tr>';
                unset($newsTypes[$key]);
                $this->showListNewsType($newsTypes, $item['id'], $level.'-- ', $index);
            }
        }
    }
}
