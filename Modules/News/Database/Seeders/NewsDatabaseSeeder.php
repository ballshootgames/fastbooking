<?php

namespace Modules\News\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class NewsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call("OthersTableSeeder");
        DB::table( 'news_type' )->insert( [
            [ 'id' => 4, 'title' => 'Tin tức du lịch', 'slug' => \Str::slug('Tin tức du lịch'), 'description' => 'trang-chu', 'parent_id' => null],
            [ 'id' => 5, 'title' => 'Điểm đến', 'slug' => \Str::slug('Điểm đến'), 'description' => '', 'parent_id' => 4],
            [ 'id' => 6, 'title' => 'Ẩm thực', 'slug' => \Str::slug('Ẩm thực'), 'description' => '', 'parent_id' => 4],
            [ 'id' => 7, 'title' => 'Kinh nghiệm du lịch', 'slug' => \Str::slug('Kinh nghiệm du lịch'), 'description' => '', 'parent_id' => 4],
            [ 'id' => 14, 'title' => 'Địa điểm du lịch', 'slug' => \Str::slug('Địa điểm du lịch'), 'description' => '', 'parent_id' => null],
            [ 'id' => 15, 'title' => 'Du lịch Huế', 'slug' => \Str::slug('Du lịch Huế'), 'description' => '', 'parent_id' => 14],
            [ 'id' => 16, 'title' => 'Du lịch Đà nẵng', 'slug' => \Str::slug('Du lịch Đà nẵng'), 'description' => '', 'parent_id' => 14],
            [ 'id' => 17, 'title' => 'Du lịch Nha trang', 'slug' => \Str::slug('Du lịch Nha trang'), 'description' => '', 'parent_id' => 14],
            [ 'id' => 18, 'title' => 'Du lịch Đà lạt', 'slug' => \Str::slug('Du lịch Đà lạt'), 'description' => '', 'parent_id' => 14],
            [ 'id' => 19, 'title' => 'Du lịch Sài gòn', 'slug' => \Str::slug('Du lịch Sài gòn'), 'description' => '', 'parent_id' => 14],
            [ 'id' => 20, 'title' => 'Du lịch Vũng tàu', 'slug' => \Str::slug('Du lịch Vũng tàu'), 'description' => '', 'parent_id' => 14],
            [ 'id' => 21, 'title' => 'Du lịch Quảng bình', 'slug' => \Str::slug('Du lịch Quảng bình'), 'description' => '', 'parent_id' => 14],
            [ 'id' => 22, 'title' => 'Du lịch Hạ long', 'slug' => \Str::slug('Du lịch Hạ long'), 'description' => '', 'parent_id' => 14],
            [ 'id' => 23, 'title' => 'Du lịch Hà nội', 'slug' => \Str::slug('Du lịch Hà nội'), 'description' => '', 'parent_id' => 14],
        ] );

        DB::table( 'news' )->insert( [
            [ 'id' => 1, 'title' => 'Top địa điểm du lịch tâm linh cho kỳ nghỉ lễ 2/9', 'slug' => Str::slug('Top địa điểm du lịch tâm linh cho kỳ nghỉ lễ 2/9'),
                'description' => 'Kỳ nghỉ lễ 2/9 bạn dự định sẽ đi đâu? Đi nghỉ dưỡng ở các địa điểm du lịch nổi tiếng? Hay tham gia các tour du lịch 2/9? Vậy bạn đã từng nghĩ đến sẽ đến các địa điểm du lịch tâm linh cho kỳ nghỉ của mình hay chưa? Hãy cùng vinavivu điểm qua một số điểm đến tâm linh đáng chú ý cho dịp lễ này nhé.',
                'content' => '<h3><span style="color: #ff6600;">Chùa Bái Đính, Ninh Bình</span></h3>
                            <p>Là quần thể chùa lớn nhất tại Việt Nam cũng như ở Đông Nam Á, đặc biệt là những kỷ lục được xác lập như chùa có diện tích lớn nhất Việt Nam, chùa có hành lang La Hán dài nhất châu Á, hay tương Phật bằng đồng dát vàng lớn nhất châu Á,…</p>
                            <p>Chùa Bái Đính nằm trên sườn núi, giữa những thung lũng mênh mông hồ và núi đá, ở cửa ngõ phía Tây vào cố đô Hoa Lư.</p>
                            <h3><span style="color: #ff6600;">Thiền Viện Trúc Lâm, Đà Lạt</span></h3>
                            <p>Nếu đến Đà Lạt thì bạn nên ghé qua Thiền viện Trúc Lâm Đà Lạt, là một trong ba thiền viện trúc lâm lớn nhất nước ta. Thiền viện thuộc thiền phái Trúc Lâm Yên Tử. Tọa lạc trên núi Phụng Hoàng, trên hồ Tuyền Lâm, cách trung tâm Đà Lạt 5km.</p>
                            <p>Là công trình có kiến trúc độc đáo, nếu lên từ phía hồ Tuyền Lâm bạn phải đi qua con đường dốc 140 bậc thang bằng đá. Chính điện có diện tích 192m2, với nhiều bức tượng chạm khắc tinh xảo.</p>
                            <h3><span style="color: #ff6600;">Chùa Thiên Mụ, Huế</span></h3>
                            <p>Chùa Thiên Mụ hay còn gọi là chùa Linh Mụ, là ngôi chùa có kiến trúc đồ sộ và cổ nhất tại Huế. Chùa nằm trên đồi Hà Khê, tả ngạn sông Hương, cách trung tâm thành phố Huế khoảng 5km về phía Tây.</p>
                            <p>Chùa được khởi lập năm Tân Sửu (1601) đời chúa Tiên – Nguyễn Hoàng. Điểm nổi bật nhất của chùa là Tháp Phước Duyên. Tháp cao 21m, gồm 7 tầng, được xây dựng ở phía trước ngôi chùa. Ngoài ra chùa còn có những công trình kiến trúc đặc sắc và các di vật cổ khác.</p>',
                'active' => 1, 'image' => 'public/images/demo/news/top-dia-diem-du-lich-tam-linh-cho-ky-nghi-le-2-9.jpeg', 'view' => 0, 'news_type_id' => 5],
            [ 'id' => 2, 'title' => 'Những quy tắc bỏ túi khi đi du lịch Thái Lan', 'slug' => Str::slug('Những quy tắc bỏ túi khi đi du lịch Thái Lan'),
                'description' => 'Xứ sở Chùa Vàng luôn thu hút khách du lịch bởi những nét đẹp văn hoá riêng và độc đáo. Nắm vững những quy tắc dưới đây sẽ giúp bạn được tôn trọng và có cái nhìn sâu sắc hơn về các giá trị của những nền văn hoá đó.',
                'content' => '<ul style="text-align: justify;"><li><strong><em>Chào người dân địa phương bằng cử chỉ “wai”</em></strong></li></ul>
                            <p style="text-align: justify;">Khi đến với <a href="https://www.khamphadisan.com/he-nay-di-du-lich-thai-lan-sieu-sang-gia-cuc-re-quay-cuc-vui/" target="_blank" rel="noopener">Thái Lan</a>, bạn nên học cách chào người dân địa phương bằng ngôn ngữ của họ. Người Thái rất lịch sự và dễ mến, dù đó chỉ là một người lạ. Tại đây, cử chỉ “wai” là một cách thể hiện sự tôn trọng mà bạn sẽ gặp rất nhiều, họ sử dụng khi đi cầu nguyện, khi chào hoặc cảm ơn người khác. Muốn thực hiện một động tác “wai”, bạn cần úp hai lòng bàn tay vào nhau đặt lên mũi và cúi đầu một chút về hướng người đối diện.</p>
                            <ul style="text-align: justify;"><li><strong><em>Ăn mặc phù hợp</em></strong></li></ul>
                            <p style="text-align: justify;">Rất nhiều dân phượt chọn Thái Lan là điểm đến, tuy nhiên đây là lại một mối phiền toái với người dân ở đây. Người dân địa phương dựa vào họ để phát triển du lịch nhưng lại không hài lòng vì sự thiếu tôn trọng của họ đối với văn hoá Thái Lan, đặc biệt là cách ăn mặc. Khi đến đây, bạn nên mặc những bộ quần áo che vai và đầu gối của mình để thể hiện sự tôn trọng với người dân bản địa.</p>
                            <ul style="text-align: justify;"><li><strong><em>Không nói nhiều về chính trị và gia đình hoàng gia</em></strong></li></ul>
                            <p style="text-align: justify;">Chính phủ Hoàng gia Thái là nơi thực hiện chủ quyền thông qua các nhánh của chính phủ và là nơi quyền lực tối cao. Do đó, người nước ngoài không nên bàn tán nhiều về chủ đề chính trị khi đi du lịch, Thái Lan cũng không phải là ngoại lệ. Thậm chí bạn còn phải đối mặt với án phạt nếu nói xấu về Hoàng gia Thái Lan.</p>
                            <ul style="text-align: justify;"><li><strong><em>Tránh bắt tay và ôm</em></strong></li></ul>
                            <p style="text-align: justify;">Thái Lan là xứ sở của những nụ cười, nhưng họ cũng có một nền văn hoá đặc trưng. Việc bắt tay với ai đó sẽ bị coi là bẩn thỉu và họ cũng không hề có khái niệm ôm. Tuy nhiên, nếu người đối diện là một đứa trẻ hay người mà bạn thân thiết, biết rất rõ thì vẫn có thể ôm. Thay vì bắt tay, “wei” là hành động phù hợp để thể hiện sự tôn trọng và lịch sự đối với người Thái.</p> 
                            <p style="text-align: justify;">Do sự thu hút về du lịch tại Thái Lan đặc biệt ở Phukiet và Chiang Mai, rất nhiều <a href="https://cachvaom88.net/m88-keo-nha-cai-bong-da-tot-nhat/top-cac-nha-cai-uy-tin-tai-viet-nam/">nhà cái uy tín</a> muốn mở sòng bạc tại kết hợp với nghỉ dưỡng tại đây với mong muốn đưa du lịch ở Thái Lan lên tầm cao hơn. Tuy nhiên do luật pháp khắt khe ở Thái lan mà việc này còn khá nhiều khó khăn.</p>',
                'active' => 1, 'image' => 'public/images/demo/news/nhung-quy-tac-bo-tui-khi-di-du-lich-thai-lan.jpg', 'view' => 0, 'news_type_id' => 5],
            [ 'id' => 3, 'title' => '[Điện Biên] Top những tọa độ check-in nhất định không được bỏ qua tại Điện Biên', 'slug' => Str::slug('[Điện Biên] Top những tọa độ check-in nhất định không được bỏ qua tại Điện Biên'),
                'description' => 'Du lịch Điện Biên ngoài những địa danh lịch sử, nơi đây còn cuốn hút du khách với nhiều danh lam thắng cảnh hoang sơ, hùng vĩ gần gũi với thiên nhiên con người nơi đâ',
                'content' => '<h3><span style="color: #ff6600;">CÁNH ĐỒNG MƯỜNG THANH</span></h3>
                            <p style="text-align: justify">Điện Biên mùa lúa chín (khoảng từ tháng 8 – 9) là thời khắc đẹp nhất trên cánh đồng Mường Thanh. Hình ảnh những người nông dân hăng say thu hoạch làm toát lên vẻ đẹp lao động truyền thống của nước ta.</p>
                            <p style="text-align: justify">Không chỉ đem lại giá trị kinh tế mà cánh đồng Mường Thanh còn đem đến cho Điện Biên một góc hình mới, một nét cuốn hút riêng: những cánh đồng lúa vàng óng trải dài theo dòng sông Nậm Rốm từ lâu đã trở thành nguồn cảm hứng cho những nhiếp ảnh gia hay đơn giản chỉ là những người không cầm lòng được trước cái đẹp.</p>
                            <h3><span style="color: #ff6600;">ĐÈO PHA ĐIN</span></h3>
                            <p style="text-align: justify">Là một trong tứ đại đỉnh đèo của miền Bắc Việt Nam. Nằm cách Tp.Điện Biên khoảng 100km, đèo Pha Đin còn được mệnh danh là nơi giao thoa giữa trời và đất và đây cũng là địa điểm check-in không thể thiếu của dân phượt khi đến Điện Biên. Đứng trên đỉnh Pha Đin, bạn sẽ được thu trọn vào tầm mắt mình cảnh núi non mây trời hùng vĩ.</p>
                            <p style="text-align: justify">Con đèo này dài khoảng 32km và có địa thế hiểm trở và mang đặc trưng của những cung đường Tây Bắc khi mà một bên là vách núi chênh vênh, một bên là vực sâu hun hút. Check-in ở đây thì bao ảo, bao đẹp, bao độc luôn nhé vi đèo có hàng trăm góc chụp, góc nào cũng feeling hight hết nhé.</p> 
                            <h3><span style="color: #ff6600;">KHU DU LỊCH PHA ĐIN PASS</span></h3>
                            <p style="text-align: justify">khu du lịch Pha Đin Pass đã được xây dựng vào năm 2016 và mở cửa đón khách ngay trên đỉnh Pha Đin. Đây là điểm dừng chân nghỉ ngơi lý tưởng khi bạn đến chinh phục đèo Pha Đin. Không những có không gian rộng, Pha Đin Pass còn có khu trò chơi siêu dễ thương. Bên trên đỉnh còn có khu xích đu, khu vườn hoa, hồ nước mini, khu đồi chong chóng và một rừng thông nhỏ phía bên trong. Đến đây chơi thì tha hồ mà sống ảo luôn nhé!</p>
                            <h3><span style="color: #ff6600;">HỒ PÁ KHOANG</span></h3>
                            <p style="text-align: justify">Nằm trên địa phận xã Mường Phăng – huyện Điện Biên – tỉnh Điện Biên, cách Tp.Điện Biên Phủ gần 20km, nằm kề QL.279, nối giữa Tp.Điện Biên Phủ với Sở chỉ huy chiến dịch Điện Biên Phủ – Mường Phăng. Hồ Pá Khoang có diện tích 2.400 ha và được bao quanh bởi những rừng cây hùng vĩ.</p>',
                'active' => 1, 'image' => 'public/images/demo/news/dien-bien-top-nhung-toa-do-check-in-nhat-dinh-khong-duoc-bo-qua-tai-dien-bien.jpg', 'view' => 0, 'news_type_id' => 5],
            [ 'id' => 4, 'title' => '[Lai Châu] Những tọa độ check-in khó lòng bỏ qua khi đến Lai Châu', 'slug' => Str::slug('[Lai Châu] Những tọa độ check-in khó lòng bỏ qua khi đến Lai Châu'),
                'description' => 'Đến với du lịch Lai Châu bạn sẽ có dịp được khám phá mãnh đất với những những ngọn đèo hiểm trở, những thác nước nằm ẩn mình trong rừng hay những cột mốc thiêng liêng của Tổ Quốc…',
                'content' => '<h3><span style="color: #ff6600;">BẢN SÌ THÂU CHẢI</span></h3>
                            <p style="text-align: justify">Cách trung tâm huyện Tam Đường chừng 6km, Sì Thâu Chải tập trung phần lớn cộng đồng người Dao sinh sống. Với cảnh quan đẹp rụng rời, không khí trong lành và những phong tục văn hóa đặc sắc của người dân nơi đây, Sì Thâu Chải đang nổi lên như một địa điểm check-in du lịch tiềm năng. Hãy tới bản Sì Thâu Chải để đầu tư bộ ảnh đẹp lung linh nào.</p>
                            <h3><span style="color: #ff6600;">CAO NGUYÊN SÌN HỒ</span></h3>
                            <p style="text-align: justify">Cảnh sắc còn đẹp hơn thiên đường, cao nguyên Sìn Hồ được người dân ưu ái gắn mác “Sa Pa thứ 2” của Việt Nam. Tới Sìn Hồ, bạn tha hồ chiêm ngưỡng vẻ đẹp mây trời bềnh bồng ở những khu làng, thung lung, núi đá Ô hay động Tiên hùng vĩ. Đã tới Sìn Hồ thì cũng nên ghé bản Pú Đao của người Mông thuần khiết bí ẩn cho biết. Tuy hơi khó đi tí nhưng bù lại phần thưởng là ngắm nhìn vẻ đẹp sông Đà trứ danh thường xuyên góp mặt trong nhiều áng văn thơ.</p>
                            <h3><span style="color: #ff6600;">ĐÈO Ô QUY HỒ</span></h3>
                            <p style="text-align: justify">Đã đặt chân tới Lai Châu thì nhất định không thể bỏ qua cung đường kinh điển Tây Bắc – đèo Ô Quy Hồ. Nối liền 2 tỉnh Lai Châu – Lào Cai, Ô Quy Hồ được công nhận là con đèo dài nhất Việt Nam. Với danh tiếng lừng lẫy và cảnh đẹp mê hồn, con đèo này đã thu hút bao nhiêu tâm hồn phượt và hội sống ảo mỗi năm.</p>
                            <h3><span style="color: #ff6600;">CÁNH ĐỒNG MƯỜNG THAN</span></h3>
                            <p style="text-align: justify">Người dân vùng Tây Bắc hay truyền tai nhau câu này “Nhất Thanh, nhì Lò, tam Than, tứ Tấc” để ca ngợi về bộ tứ ruộng bậc thanh có view lung linh bậc nhất khu vực này. Cánh đồng Mường Than nằm ngay huyện Than Uyên cũng góp tên mình vào danh sách. Mỗi mùa Mường Than luôn có vẻ đẹp riêng của nó, nếu mùa nước đổ thì tràn ngập mảng xanh biếc, còn mùa lúa chin thì nhuộm màu vàng óng đẹp miễn chê.</p>',
                'active' => 1, 'image' => 'public/images/demo/news/lai-chau-nhung-toa-do-check-in-kho-long-bo-qua-khi-den-lai-chau.jpg', 'view' => 0, 'news_type_id' => 5],
            [ 'id' => 5, 'title' => '[Lào Cai] Những điểm đến không được bỏ qua khi đến du lịch Lào Cai', 'slug' => Str::slug('[Lào Cai] Những điểm đến không được bỏ qua khi đến du lịch Lào Cai'),
                'description' => 'Lào Cai cũng là địa phương sở hữu những địa điểm du lịch hấp dẫn và vô cùng lý tưởng của vùng núi phía Bắc như Sa Pa, Bắc Hà, Y Tý… Nơi này cũng là điểm đến phù hợp cho các bạn bạn yêu thích bộ môn leo núi, trekking với rất nhiều đỉnh núi cao để các bạn tha hồ khám phá như Bạch Mộc Lương Tử, Lảo Thẩn, Nhìu Cồ San,…',
                'content' => '<h3><span style="color: #ff6600;">NHÀ THỜ ĐÁ SAPA</span></h3>
                            <p style="text-align: justify">Nhà thờ cổ Sapa còn có tên gọi khác là nhà thờ đá hay nhà thờ Đức Mẹ Mân Côi tọa lạc ngay giữa trung tâm thị trấn Sa Pa, được người Pháp cho xây dựng từ đầu thế kỷ 20. Nhà thờ được xây theo hình thập giá theo kiến trúc Gotic của người La Mã thể hiện ở mái nhà, tháp chuông, vòm cuốn… đều là hình chóp tạo cho công trình nét bay bổng thanh thoát.</p>
                            <p style="text-align: justify">Đây là điểm hẹn của nhiều đôi bạn trẻ mỗi khi có dịp đến đây. Mặc du trải qua một số lần trùng tu, cải tạo do chiến tranh và sự tàn phá khốc liệt của thiên nhiên nhưng nhà thờ vẫn giữ được nét duyên dáng và hồn của công trình kiến trúc tôn giáo.</p>
                            <h3><span style="color: #ff6600;">ĐỈNH FANSIPAN</span></h3>
                            <p style="text-align: justify">Với độ cao 3143m Fansipan là ngọn núi cao nhất Việt Nam đồng thời cũng là ngọn núi cao nhất 3 nước Đông Dương thuộc dãy núi Hoàng Liên Sơn, cách Sa Pa 9km về phía Tây Nam. Theo tiếng địa phương, tên của ngọn núi là Hủa Xi Pan có nghĩa là Phiến đá khổng lồ chênh vênh. Thời điểm leo núi thích hợp là từ tháng 9 – 3 năm sau. Tuy nhiên đường lên Fansipan đẹp nhất là khoảng cuối tháng 2, khi các loài hoa núi bắt đầu nở.</p>
                            <h3><span style="color: #ff6600;">CHỢ PHIÊN BẮC HÀ</span></h3>
                            <p style="text-align: justify">Cứ vào chủ nhật hàng tuần là chợ phiên Bắc Hà diễn ra. Những dòng người kéo nhau về thị trấn Bắc Hà để họp chợ với muôn vàn mặt hàng khác nhau. Phiên chợ chủ yếu là nơi để trao đổi, mua bán các mặt hàng gắn liền với đời sống của bà con dân tộc thiểu số thuộc các bản, làng lân cận. Tại phiên chợ Bắc Hà, bạn có thể tìm thấy những món đồ khác lạ của người dân tộc, cũng như thưởng thức đặc sản vùng cao, như: thắng cố, rượu ngô, rượu thóc hay trà hoa tam thất…</p>
                            <h3><span style="color: #ff6600;">CỬA KHẨU QUỐC TẾ LÀO CAI</span></h3>
                            <p style="text-align: justify">Là 1 trong 3 cửa khẩu quốc tế quan trọng trên tuyến biên giới phía Bắc của Việt Nam cùng với Cửa khẩu Đồng Đăng (tỉnh Lạng Sơn) và Cửa khẩu Móng Cái (tỉnh Quảng Ninh). Cửa khẩu Quốc Tế Lào Cai là nơi mà nhiều du khách khi đến du lịch Lào Cai đưa vào danh mục các điểm đến yêu thích. Các bạn có thể làm thủ tục xin cấp giấy thông hành để qua Hà Khẩu (Trung Quốc) chơi với thủ tục nhanh gọn và chi phí hợp lý.</p>',
                'active' => 1, 'image' => 'public/images/demo/news/lao-cai-nhung-diem-den-khong-duoc-bo-qua-khi-den-du-lich-lao-cai.jpg', 'view' => 0, 'news_type_id' => 5],

            [ 'id' => 6, 'title' => '[Điện Biên] Điểm danh những món ngon không được bỏ qua khi đến với Điện Biên', 'slug' => Str::slug('[Điện Biên] Điểm danh những món ngon không được bỏ qua khi đến với Điện Biên'),
                'description' => 'Là một tỉnh nằm ở phía tây bắc của tổ quốc, Điện Biên ngoài những điểm du lịch nổi tiếng thì mãnh đất này còn lôi cuốn du khách với những món đặc sản nổi tiếng như: trâu gác bếp, xôi nếp nương, thịt xông khói,…',
                'content' => 'Nhập nội dung vào đây!',
                'active' => 1, 'image' => 'public/images/demo/news/dien-bien-diem-danh-nhung-mon-ngon-khong-duoc-bo-qua-khi-den-voi-dien-bien.jpg', 'view' => 0, 'news_type_id' => 6],
            [ 'id' => 7, 'title' => '[Lai Châu] Những món ngon nhất định phải thử khi đến Lai Châu', 'slug' => Str::slug('[Lai Châu] Những món ngon nhất định phải thử khi đến Lai Châu'),
                'description' => 'Ngoài những điểm tham quan nổi tiếng thì du lịch Lai Châu còn có rất nhiều món ăn lạ, ngon và độc đáo chỉ có tại nơi này. Nếu có thời gian bạn hãy đến với Lai Châu để thưởng thức các món ăn ngon đậm chất núi rùng của miền cao này nhé.',
                'content' => 'Nhập nội dung vào đây!',
                'active' => 1, 'image' => 'public/images/demo/news/lai-chau-nhung-mon-ngon-nhat-dinh-phai-thu-khi-den-lai-chau.jpg', 'view' => 0, 'news_type_id' => 6],
            [ 'id' => 8, 'title' => '[Lào Cai] Đến du lịch Lào Cai bạn nhất định không thể bỏ qua những đặc sản này', 'slug' => Str::slug('[Lào Cai] Đến du lịch Lào Cai bạn nhất định không thể bỏ qua những đặc sản này'),
                'description' => 'Cá hồi Sapa, Gà đen Sapa, Thắng cố Bắc Hà, Cốm Bắc Hà,… là những đặc sản nổi tiếng mà bạn nhất định không thể bỏ qua khi đến với du lịch Lào Cai.',
                'content' => 'Nhập nội dung vào đây!',
                'active' => 1, 'image' => 'public/images/demo/news/lao-cai-den-du-lich-lao-cai-ban-nhat-dinh-khong-the-bo-qua-nhung-dac-san-nay.jpg', 'view' => 0, 'news_type_id' => 6],
            [ 'id' => 9, 'title' => '[Hà Giang] Tất tần tật những đặc sản nổi tiếng khó lòng bỏ qua khi đến du lịch Hà Giang', 'slug' => Str::slug('[Hà Giang] Tất tần tật những đặc sản nổi tiếng khó lòng bỏ qua khi đến du lịch Hà Giang'),
                'description' => 'Bánh tam giác mạch, cháo ấu tẩu, lợn cắp nách, thắng cố,… là trong những đặc sản rất nổi tiếng luôn lôi cuốn khách du lịch mỗi lần đến với du lịch Hà Giang.',
                'content' => 'Nhập nội dung vào đây!',
                'active' => 1, 'image' => 'public/images/demo/news/ha-giang-tat-tan-tat-nhung-dac-san-noi-tieng-kho-long-bo-qua-khi-den-du-lich-ha-giang.jpg', 'view' => 0, 'news_type_id' => 6],
            [ 'id' => 10, 'title' => '[Tuyên Quang] Tất tần tật những đặc sản nhất định phải thử một lần khi đến xứ Tuyên', 'slug' => Str::slug('[Tuyên Quang] Tất tần tật những đặc sản nhất định phải thử một lần khi đến xứ Tuyên'),
                'description' => 'Nếu đã đến với du lịch Tuyên Quang mà không thưởng thức những phong vị ẩm thực độc đáo nơi này quả thật là một thiếu sót khá lớn.',
                'content' => 'Nhập nội dung vào đây!',
                'active' => 1, 'image' => 'public/images/demo/news/tuyen-quang-tat-tan-tat-nhung-dac-san-nhat-dinh-phai-thu-mot-lan-khi-den-xu-tuyen.jpg', 'view' => 0, 'news_type_id' => 6],

            [ 'id' => 11, 'title' => '[Điện Biên] Tất tần tật kinh nghiệm du lịch Điện Biên tự túc từ A-Z, đi đâu, chơi gì, ở đâu ?', 'slug' => Str::slug('[Điện Biên] Tất tần tật kinh nghiệm du lịch Điện Biên tự túc từ A-Z, đi đâu, chơi gì, ở đâu ?'),
                'description' => 'Là một điểm đến không chỉ có nhiều cảnh quan thiên nhiên tuyệt đẹp, Điện Biên còn là điểm đến mang đậm dấu ấn của lịch sử hào hùng của dân tộc, đến đây bạn sẽ được sống lại trong thời kỳ đầy hào hùng đấy.',
                'content' => 'Nhập nội dung vào đây!',
                'active' => 1, 'image' => 'public/images/demo/news/dien-bien-tat-tan-tat-kinh-nghiem-du-lich-dien-bien-tu-tuc-tu-a-z-di-dau-choi-gi-o-dau.jpg', 'view' => 0, 'news_type_id' => 7],
            [ 'id' => 12, 'title' => '[Lai Châu] Tất tần tật kinh nghiệm du lịch Lai Châu tự túc từ A-Z, đi đâu, chơi gì, ở đâu ?', 'slug' => Str::slug('[Lai Châu] Tất tần tật kinh nghiệm du lịch Lai Châu tự túc từ A-Z, đi đâu, chơi gì, ở đâu ?'),
                'description' => 'Du lịch Lai Châu với nét hoang sơ và thơ mộng, năm suốt tháng với khí hậu mát mẻ, cảnh vật trù phú, món ăn ngon miệng. Những năm gần đây Lai Châu đang trở thành điểm đến đang được giới trẻ lựa chọn khi đến Tây Bắc.',
                'content' => 'Nhập nội dung vào đây!',
                'active' => 1, 'image' => 'public/images/demo/news/lai-chau-tat-tan-tat-kinh-nghiem-du-lich-lai-chau-tu-tuc-tu-a-z-di-dau-choi-gi-o-dau.jpg', 'view' => 0, 'news_type_id' => 7],
            [ 'id' => 13, 'title' => '[Lào Cai] Tất tần tật kinh nghiệm du lịch Lào Cai tự túc từ A-Z, đi đâu, chơi gì, ở đâu ?', 'slug' => Str::slug('[Lào Cai] Tất tần tật kinh nghiệm du lịch Lào Cai tự túc từ A-Z, đi đâu, chơi gì, ở đâu ?'),
                'description' => 'Lào Cai có nhiều địa danh lịch sử, hang động tự nhiên, đặc sản và là nơi mang đậm nét đặc trưng văn hoá độc đáo của nhiều dân tộc anh em. Là một điểm đến khá thú vị mà bạn không được bỏ qua trên cung đường khám phá vùng tây bắc nhé.',
                'content' => 'Nhập nội dung vào đây!',
                'active' => 1, 'image' => 'public/images/demo/news/lao-cai-tat-tan-tat-kinh-nghiem-du-lich-lao-cai-tu-tuc-tu-a-z-di-dau-choi-gi-o-dau.jpg', 'view' => 0, 'news_type_id' => 7],
            [ 'id' => 14, 'title' => '[Hà Giang] Tất tần tật kinh nghiệm du lịch Hà Giang tự túc từ A-Z, đi đâu, chơi gì, ở đâu ?', 'slug' => Str::slug('[Hà Giang] Tất tần tật kinh nghiệm du lịch Hà Giang tự túc từ A-Z, đi đâu, chơi gì, ở đâu ?'),
                'description' => 'Nếu bạn đang tìm kiếm một lý do để đến với Hà Giang thì quả thật là quá “so eazy”, quan trọng là bạn muốn đến vào thời gian nào thôi, bởi vì nơi địa đầu Tổ quốc này mùa nào cũng đẹp hết nhá. Nếu bạn cần thêm những kinh nghiệm trước đi với nơi này thì đừng bỏ lỡ những chia sẽ ngay sau đây nhé.',
                'content' => 'Nhập nội dung vào đây!',
                'active' => 1, 'image' => 'public/images/demo/news/ha-giang-tat-tan-tat-kinh-nghiem-du-lich-ha-giang-tu-tuc-tu-a-z-di-dau-choi-gi-o-dau.jpg', 'view' => 0, 'news_type_id' => 7],
            [ 'id' => 15, 'title' => '[Tuyên Quang] Kinh nghiệm du lịch Tuyên Quang tự túc từ A-Z, đi đâu, chơi gì, ở đâu ?', 'slug' => Str::slug('[Tuyên Quang] Kinh nghiệm du lịch Tuyên Quang tự túc từ A-Z, đi đâu, chơi gì, ở đâu ?'),
                'description' => 'Là nơi giao lưu văn hóa của các dân tộc miền núi Tây Bắc với nhiều di sản, giá trị trị văn hóa, giá trị tinh thần vô cùng quý báu được gìn giữ và phát triển cho tới nay. Du lịch Tuyên Quang còn là địa danh nổi tiếng với phong cảnh hữu tình mà còn là cái nôi cách mạng của dân tộc. ',
                'content' => 'Nhập nội dung vào đây!',
                'active' => 1, 'image' => 'public/images/demo/news/tuyen-quang-kinh-nghiem-du-lich-tuyen-quang-tu-tuc-tu-a-z-di-dau-choi-gi-o-dau.jpg', 'view' => 0, 'news_type_id' => 7],
        ] );
    }
}