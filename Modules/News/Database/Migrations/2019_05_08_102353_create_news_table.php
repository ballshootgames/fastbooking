<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_type', function (Blueprint $table){
            $table->increments('id');
            $table->string('title');
            $table->string('slug')->nullable();
            $table->text('description')->nullable();
            $table->integer('parent_id')->nullable()->unsigned();
            $table->tinyInteger('arrange')->nullable()->unsigned();//sắp xếp
            $table->tinyInteger('active')->default(0);
            $table->softDeletes();
            $table->timestamps();
            $table->integer('creator_id')->unsigned()->nullable();
            $table->foreign('creator_id')->references('id')->on('users')->onDelete('set null');
            $table->integer('updator_id')->unsigned()->nullable();
            $table->foreign('updator_id')->references('id')->on('users')->onDelete('set null');
        });

        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug')->nullable();
            $table->text('description')->nullable();
            $table->text('content')->nullable();
            $table->string('image')->nullable();
            $table->integer('view')->nullable()->unsigned();

            $table->integer('news_type_id')->nullable()->unsigned();
            $table->foreign('news_type_id')
                ->references('id')
                ->on('news_type')
                ->onDelete('set null');

            $table->integer('user_id')->nullable()->unsigned();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('set null');

            $table->tinyInteger('arrange')->nullable()->unsigned();
            $table->tinyInteger('active')->default(0);//delete status
            $table->softDeletes();
            $table->timestamps();
            // delete user foreign
            $table->integer('creator_id')->unsigned()->nullable();
            $table->foreign('creator_id')->references('id')->on('users')->onDelete('set null');
            $table->integer('updator_id')->unsigned()->nullable();
            $table->foreign('updator_id')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
        Schema::dropIfExists('news_type');
    }
}
