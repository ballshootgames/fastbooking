<?php

namespace Modules\News\Http\Controllers;

use App\Traits\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Modules\News\Entities\News;
use Modules\News\Entities\NewsType;
use Modules\Theme\Entities\Menu;
use Illuminate\Support\Str;

class NewsController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = !empty($request->get('hidRecord')) ? $request->get('hidRecord') : Config("settings.perpage10");

        $news = News::sortable();
        $active = News::getListActive();

        if (!empty($keyword)){
            $news = $news->where('title','LIKE',"%$keyword%");
        }

//        if ($request->wantsJson()){
//
//            $news = $news->paginate($perPage)->appends($request->except('page'));
//            return NewsResource::collection($news);
//
//        }

        $news = $news->with('type');

        $news = $news->sortable(['created_at' => 'desc'])->paginate($perPage);
        $totalInActive = News::where('active', config('settings.inactive'))->count();
        $totalActive = News::where('active', config('settings.active'))->count();

        return view('news::news.index', compact('news', 'active', 'totalInActive', 'totalActive'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $treeNewsType = NewsType::all();
        $news_type = new NewsType();
        $news_type = $news_type->getListNewsTypeToArray($treeNewsType);
        $news_type = \Arr::prepend($news_type, __('message.please_select'),'');
        $arrange = News::max('arrange');
        $arrange = empty($arrange) ? 1 : $arrange+1;
        $prevUrl = \URL::previous();
        return view('news::news.create', compact('news_type', 'prevUrl', 'arrange'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:'.(5*1024),
            'content' => 'required',
            'arrange' => 'nullable|numeric',
        ]);

        $requestData = $request->all();
        if (empty($request->get('slug'))){
            $requestData['slug'] = Str::slug($requestData['title']);
        }

        $requestData['slug'] = Menu::setSlug($requestData['slug']);

        \DB::transaction(function () use ($request, $requestData){
            if($request->hasFile('image')) {
                $requestData['image'] = News::saveImageResize($request->file('image'));
            }
            News::create($requestData);
        });

        Session::flash('flash_message', __('news::news.created_success'));
        return redirect('news');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id, Request $request)
    {
        $news = News::findOrFail($id);
        $prevUrl = \URL::previous();
        return view('news::news.show', compact('news', 'prevUrl'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id, Request $request)
    {
        $news = News::findOrFail($id);
        //$news_type = NewsType::all()->pluck('title','id');
        //$news_type->prepend(__('message.please_select'), '')->all();
        $treeNewsType = NewsType::all();
        $news_type = new NewsType();
        $news_type = $news_type->getListNewsTypeToArray($treeNewsType);
        $news_type = \Arr::prepend($news_type, __('message.please_select'), '');
        $status = News::getListActive();
        $prevUrl = '';
        if ($request['hidUrl'] != null && $request['hidUrl']!='')
            $prevUrl = $request['hidUrl'];
        else
            $prevUrl = \URL::previous();
        return view('news::news.edit', compact('news','news_type', 'prevUrl'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'slug' => 'nullable',
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:'.(5*1024),
            'content' => 'required',
            'arrange' => 'nullable|numeric'
        ]);
        $news = News::findOrFail($id);

        $requestData = $request->all();
        if (empty($request->get('slug'))){
            $requestData['slug'] = Str::slug($requestData['title']);
            $requestData['slug'] = Menu::setSlug($requestData['slug']);
        }
        if (empty($requestData['active'])){
            $requestData['active'] = 0;
        }

        \DB::transaction(function () use ($request, $requestData, $news){
            if($request->hasFile('image')) {
                News::deleteFile($news->image);
                $requestData['image'] = News::saveImageResize($request->file('image'));
            }
            if ($request->get('img-hidden') == 1){
                News::deleteFile($news->image);
                $requestData = \Arr::prepend($requestData, null, 'image');
            }
            $news->update($requestData);
        });

        Session::flash('flash_message', __('news::news.updated_success'));

        return \Redirect::to($requestData['hidUrl']);
//        if ($request->has('back_url')){
//            $backUrl = $request->get('back_url');
//            if (!empty($backUrl)){
//                return redirect($backUrl);
//            }
//        }
//
//        return redirect('news');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $new = News::findOrFail($id);
	    News::deleteFile($new->image);

        News::destroy($id);

        Session::flash('flash_message', __('news::news.deleted_success'));

        return redirect('news');
    }
}
