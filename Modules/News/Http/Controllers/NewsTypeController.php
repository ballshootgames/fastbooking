<?php

namespace Modules\News\Http\Controllers;

use App\Traits\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Modules\News\Entities\News;
use Modules\News\Entities\NewsType;
use Modules\Theme\Entities\Menu;
use Illuminate\Support\Str;

class NewsTypeController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        /*$perPage = config('settings.perpage');*/
        $newsTypes = NewsType::sortable('arrange', 'asc');

        if (!empty($keyword)){
            $newsTypes = NewsType::where('title','LIKE',"%$keyword%");
        }

        $newsTypes = $newsTypes->paginate(10);
        $totalInActive = NewsType::where('active', config('settings.inactive'))->count();
        $totalActive = NewsType::where('active', config('settings.active'))->count();


        return view('news::news-type.index', compact('newsTypes', 'totalInActive', 'totalActive'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $treeNewsTypes = NewsType::all();
        $newsTypes = new NewsType();
        $newsTypes = $newsTypes->getListNewsTypeToArray($treeNewsTypes);
        $newsTypes = \Arr::prepend(!empty($newsTypes) ? $newsTypes : [], __('message.please_select'), '');
        $position = config('theme.menu.position');
        $prevUrl = \URL::previous();
        return view('news::news-type.create',compact('newsTypes', 'position', 'prevUrl'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'arrange' => 'nullable|numeric',
        ]);

        $requestData = $request->all();

        if (empty($request->get('slug'))){
            $requestData['slug'] = \Str::slug($requestData['title']);
        }

        $requestData['slug'] = Menu::setSlug($requestData['slug']);

        \DB::transaction(function () use ($request, $requestData){
            $news_type = NewsType::create($requestData);
            Menu::saveUpdateMenu(config('settings.model_news_type'),$news_type,null,$request->position,true);
        });

        \Session::flash('flash_message', __('news::news_type.created_success'));
        return redirect('news-type');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $news_type = NewsType::findOrFail($id);
        $prevUrl = \URL::previous();

        return view('news::news-type.show', compact('news_type', 'prevUrl'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id, Request $request)
    {
        $news_type = NewsType::findOrFail($id);
        $treeNewsTypes = NewsType::all();
        $newsTypes = new NewsType();
        $newsTypes = $newsTypes->getListNewsTypeToArray($treeNewsTypes);
        $newsTypes = \Arr::prepend($newsTypes, __('message.please_select'), '');
        $position = config('theme.menu.position');
        $menu = Menu::where('slug',$news_type->slug)->first();
        $position_edit = explode(',',!empty($menu) ? $menu->position : '');
        $prevUrl = \URL::previous();
        if ($request['hidUrl'] != null && $request['hidUrl']!='') {
            $prevUrl = $request['hidUrl'];
        }

        return view('news::news-type.edit', compact('news_type', 'newsTypes','position','position_edit', 'prevUrl'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'slug' => 'nullable',
            'arrange' => 'nullable|numeric'
        ]);
        $news_type = NewsType::findOrFail($id);

        $requestData = $request->all();
        if (empty($request->get('slug'))){
            $requestData['slug'] = \Str::slug($requestData['title']);
            $requestData['slug'] = Menu::setSlug($requestData['slug']);
        }
        if (empty($requestData['active'])){
            $requestData['active'] = 0;
        }

        \DB::transaction(function () use ($request, $requestData, $news_type){
//            if($request->hasFile('avatar')) {
//                \Storage::delete($category->avatar);
//                $requestData['avatar'] = Category::uploadAndResize($request->file('avatar'));
//            }
            $news_type->update($requestData);
            $menu = Menu::where('slug',$news_type->slug)->first();
            Menu::saveUpdateMenu(config('settings.model_news_type'),$news_type,$menu,$request->position, false);
        });
        \Session::flash('flash_message', __('news::news_type.updated_success'));
        return \Redirect::to($requestData['hidUrl']);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        \DB::transaction(function () use ($id){
            $newsType = NewsType::findOrFail($id);
            Menu::where('slug', $newsType->slug)->delete();
            $newsType->delete();
        });
        \Session::flash('flash_message', __('news::news_type.deleted_success'));
        return redirect('news-type');
    }
}
