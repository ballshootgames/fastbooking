@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __('news::news.news') }}
@endsection
@section('contentheader_title')
    {{ __('news::news.news') }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li><a href="{{ url('/news') }}">{{ __('news::news.news') }}</a></li>
        <li class="active">{{ __("message.detail") }}</li>
    </ol>
@endsection

@section('main-content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __("message.detail") }}</h3>
            <div class="box-tools">
                @if (strpos($prevUrl, url('/news') )!==false)
                    <a href="{{ $prevUrl }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.lists') }}</span></a>
                @else
                    <a href="{{ url('/news') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.lists') }}</span></a>
                @endif
                @can('NewsController@update')
                    {!! Form::open([
                        'method'=>'GET',
                        'url' => '/news/' . $news->id . '/edit',
                        'style' => 'display:inline'
                    ]) !!}
                    <input type="hidden" id="hidUrl" name="hidUrl" value={{ $prevUrl }}>
                    {!! Form::button('<i class="fa fa-pencil-square-o" aria-hidden="true"></i> <span class="hidden-xs">'.__('message.edit').'</span>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-primary btn-sm',
                            'title' => __('message.edit'),
                            'onclick'=>'return true'
                    ))!!}
                    {!! Form::close() !!}
                @endcan
                @can('NewsController@destroy')
                    {!! Form::open([
                        'method'=>'DELETE',
                        'url' => ['/news', $news->id],
                        'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> <span class="hidden-xs">'.__('message.delete').'</span>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-sm',
                            'title' => __('message.delete'),
                            'onclick'=>'return confirm("'.__('message.confirm_delete').'")'
                    ))!!}
                    {!! Form::close() !!}
                @endcan
            </div>
        </div>
        <div class="box-body table-responsive">
            <table class="table-layout table table-striped table-bordered">
                <tbody>
                <tr>
                    <td> {{ trans('news::news.title') }} </td>
                    <td> {{ $news->title }} </td>
                </tr>
                <tr>
                    <td> {{ trans('news::news.url') }} </td>
                    <td> <a href="{{ url(optional($news->type)->slug) }}/{{ $news->slug }}.html">{{ url(optional($news->type)->slug) }}/{{ $news->slug }}.html</a> </td>
                </tr>
                <tr>
                    <td> {{ trans('news::news.news_type_id') }} </td>
                    <td> {{ $news->type ? optional($news->type)->title : '' }} </td>
                </tr>
                <tr>
                    <td> {{ trans('news::news.image') }} </td>
                    <td>
                        @if(!empty($news->image))
                            <img width="100" src="{{ asset(\Storage::url($news->image)) }}" alt="{{ $news->title }}"/>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td> {{ trans('news::news.description') }} </td>
                    <td> {{ $news->description }} </td>
                </tr>
                <tr>
                    <td> {{ trans('news::news.content') }} </td>
                    <td> {!! $news->content !!} </td>
                </tr>
                <tr>
                    <td> {{ trans('news::news.created_at') }} </td>
                    <td> {{ $news->created_at }} </td>
                </tr>
                <tr>
                    <td> {{ trans('message.creator') }} </td>
                    <td> {{ optional($news->user)->name }}</td>
                </tr>
                <tr>
                    <td> {{ trans('news::news.active') }} </td>
                    <td> {!! $news->active===1?'<i class="fas fa-check text-primary"></i>' : '<i class="fas fa-check"></i>' !!} </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
@section('css')
    <style>
        .table-layout tr td:first-child{
            width: 22%;
        }
        .table-layout tr td:last-child{
            width: 78%;
        }
    </style>
@endsection