@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __('news::news_type.news_type') }}
@endsection
@section('contentheader_title')
    {{ __('news::news_type.news_type') }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li class="active">{{ __('news::news_type.news_type') }}</li>
    </ol>
@endsection

@section('main-content')
    <div id="alert"></div>
    <div class="statis">
        <ul>
            <li>Có {{ !empty($totalInActive) ? $totalInActive : '0' }} {{ trans('news::news_type.news_type') }} chưa được phê duyệt</li>
            <li>Có {{ !empty($totalActive) ? $totalActive : '0' }} {{ trans('news::news_type.news_type') }} đã được phê duyệt</li>
        </ul>
    </div>
    <div class="box box-primary">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-2">
                    @can('NewsTypeController@store')
                        <a href="{{ url('/news-type/create') }}" class="btn btn-success btn-sm" title="{{ __('message.add') }}">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                        </a>
                    @endcan
                </div>
                <div class="col-sm-10">
                    <div class="pull-right">
                        {!! Form::open(['method' => 'GET', 'url' => '/news-type', 'class' => 'pull-left form-inline', 'role' => 'search', 'id' => 'search'])  !!}
                        <div class="input-group" style="width: 200px;">
                            <input type="text" value="{{\Request::get('search')}}" class="form-control input-sm" name="search" placeholder="{{ __('message.search_keyword') }}">
                            <span class="input-group-btn">
                                <button class="btn btn-default btn-sm" type="submit">
                                    <i class="fa fa-search"></i> {{ __('message.search') }}
                                </button>
                            </span>
                        </div>
{{--                        <input type="hidden" name="hidRecord" value="{{ Request::has('record') ? Request::get('record') : config('settings.total_rec')[0] }}">--}}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="box-body table-responsive no-padding">
            <table id="table-copy" class="table--layout table table-condensed table-bordered">
                <thead>
                    <tr class="active">
                        <th class="text-center" style="width: 3.8%;">{{ trans('message.index') }}</th>
                        <th class="text-center" style="width: 3.1%;">
                            <input type="checkbox" name="chkAll" id="chkAll"/>
                        </th>
                        <th style="width: 20%;">@sortablelink('title',trans('news::news_type.title'))</th>
                        <th style="width: 15%;">@sortablelink('slug',trans('news::news_type.slug'))</th>
                        <th style="width: 22%;">{{ trans('news::news_type.description') }}</th>
{{--                        <th class="text-center" style="width: 10%;">@sortablelink('arrange', trans('news::news_type.index'))</th>--}}
                        <th class="text-center" style="width: 7%;">@sortablelink('active', trans('news::news_type.active'))</th>
                        <th class="text-center" style="width: 11%;">@sortablelink('created_at', trans('message.created_at'))</th>
                        <th class="text-center" style="width: 10%;">{{ trans('message.creator') }} </th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $listNewsTypes = new \Modules\News\Entities\NewsType();
                        $listNewsTypes->showListNewsType($newsTypes);
                    @endphp
                </tbody>
            </table>
        </div>
        {{--        <div class="box-comment">--}}
        {{--        </div>--}}
        <div class="box-footer clearfix">
            <div class="row">
                <div id="btn-act" class="col-sm-6 text-left">
                    @can('NewsTypeController@destroy')
                        <a href="#" id="delNewsTypeItem" data-action="delNewsTypeItem" class="btn-act btn btn-danger btn-sm" title="{{ __('message.delete') }}">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </a>
                    @endcan
                    @can('NewsTypeController@active')
                        <a href="#" id="activeNewsTypeItem" data-action="activeNewsTypeItem" class="btn-act btn btn-primary btn-sm" title="{{ __('message.active') }}">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </a>
                    @endcan
                </div>
                <div class="col-sm-6 text-right">
                    {!! $newsTypes->appends(\Request::except('page'))->render() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts-footer')
    <script type="text/javascript">
        $(function() {
            $('#chkAll').on('click', function () {
                $("input:checkbox").prop('checked', $(this).prop("checked"));
            });
        });
        $('#btn-act').on('click', '.btn-act', function(e){
            e.preventDefault();
            let action = $(this).data('action');
            ajaxListNewTypes(action);
        });

        function ajaxListNewTypes(action){
            let chkId = $("input[name='chkId']:checked");
            let actTxt = '', successAlert = '', classAlert = '';
            switch (action) {
                case 'activeNewsTypeItem':
                    actTxt = 'duyệt';
                    successAlert = '{{ trans('news::news_type.updated_success') }}';
                    classAlert = 'alert-danger';
                    break;
                case 'delNewsTypeItem':
                    actTxt = 'xóa';
                    successAlert = '{{ trans('news::news_type.deleted_success') }}';
                    classAlert = 'alert-danger';
                    break;
            }
            if (chkId.length != 0){
                let notificationConfirm = 'Bạn có muốn '+actTxt+' hồ sơ này không?';
                let notification = confirm(notificationConfirm);
                if (notification){
                    var arrId = '';
                    $("input[name='chkId']:checked").map((val,key) => {
                        arrId += key.value + ',';
                    });
                    axios.get('{{url('/bookings/ajax')}}/'+action, {
                        params: {
                            ids: arrId
                        }
                    })
                        .then((response) => {
                            // console.log(response);
                            if (response.data.success === 'ok'){
                                $('#alert').html('<div class="alert '+classAlert+'">' +
                                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                                    successAlert +
                                    ' </div>');
                                location.reload(true);
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                        })
                }
            }else{
                let notificationAlert = 'Vui lòng chọn hồ sơ để '+actTxt+'!';
                alert(notificationAlert);
            }
        }
    </script>
@endsection