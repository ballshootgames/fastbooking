@section('css')
    <style>
        .table-layout tr td:first-child{
            width: 18%;
        }
        .table-layout tr td:last-child{
            width: 82%;
        }
    </style>
@endsection
<div class="box-body">
    @if ($errors->any())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <p><i class="fa fa-fw fa-check"></i> {{ $error }}</p>
            @endforeach
        </div>
    @endif
    <table class="table-layout table table-striped table-bordered">
        <tbody>
            <tr>
                <td>{{ trans('news::news_type.title') }} <span class="label-required"></span></td>
                <td>
                    <div>
                        {!! Form::text('title', null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
                        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
            @if(isset($news_type))
                <tr>
                    <td>{{ trans('news::news_type.slug') }} <span class="label-required"></span></td>
                    <td>
                        <div>
                            {!! Form::text('slug', null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
                            {!! $errors->first('slug', '<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                </tr>
            @endif
            <tr>
                <td>{{ trans('news::news_type.parent_id') }}</td>
                <td>
                    <div>
                        {!! Form::select('parent_id', $newsTypes, null, ['class' => 'form-control select2']) !!}
                        {!! $errors->first('parent_id', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>{{ trans('news::news_type.description') }}</td>
                <td>
                    <div>
                        {!! Form::textarea('description', null, ['class' => 'form-control input-sm']) !!}
                        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>{{ trans('theme::menus.position') }}</td>
                <td>
                    <div>
                        {!! Form::select('position[]', $position, isset($position_edit) ? $position_edit : null, ['class' => 'form-control input-sm select2', 'multiple' => true]) !!}
                        {!! $errors->first('position', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>{{ trans('news::news_type.index') }}</td>
                <td>
                    <div>
                        {!! Form::number('arrange', null, ['class' => 'form-control input-sm']) !!}
                        {!! $errors->first('arrange', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>{{ trans('message.meta_keyword') }}</td>
                <td>
                    <div>
                        {!! Form::textarea('meta_keyword', null, ['class' => 'form-control input-sm', 'rows' => 3]) !!}
                        {!! $errors->first('meta_keyword', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>{{ trans('news::news_type.active') }}</td>
                <td>
                    <div>
                        {!! Form::checkbox('active', 1, (isset($news_type) && $news_type->active===1)?true:false, ['class' => 'flat-blue', 'id' => 'active']) !!}
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <div style="margin-top: 10px;">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : __('message.save'), ['class' => 'btn btn-primary']) !!}
        <a href="{{ $prevUrl }}" class="btn btn-default">{{ __('message.cancel') }}</a>
        <input type="hidden" id="hidUrl" name="hidUrl" value={{ $prevUrl }}>
    </div>
</div>