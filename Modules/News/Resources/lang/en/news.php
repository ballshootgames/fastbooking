<?php

return [
    'news' => 'Tin tức',
    'title' => 'Tiêu đề',
    'slug' => 'Slug',
    'url' => 'Đường dẫn URL',
    'news_type_id' => 'Thể loại',
    'image' => 'Ảnh',
    'description' => 'Mô tả',
    'content' => 'Nội dung',
    'status' => 'Tình trạng',
    'updated_at' => 'Ngày cập nhật',
    'created_success' => 'Tin tức đã được thêm!',
    'updated_success' => 'Tin tức đã được cập nhật!',
    'deleted_success' => 'Tin tức đã được xóa!',
];