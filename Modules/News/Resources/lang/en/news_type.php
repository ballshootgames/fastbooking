<?php

return [
    'category' => 'Thể loại tin tức',
    'title' => 'Tiêu đề',
    'slug' => 'Slug',
    'url' => 'Đường dẫn URL',
    'avatar' => 'Ảnh',
    'description' => 'Mô tả',
    'updated_at' => 'Ngày cập nhật',
    'created_success' => 'Thể loại tin tức đã được thêm!',
    'updated_success' => 'Thể loại tin tức đã được cập nhật!',
    'deleted_success' => 'Thể loại tin tức đã được xóa!',
];