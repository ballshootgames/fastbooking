<?php

return [
    'news_type' => 'Loại tin',
    'title' => 'Tiêu đề',
    'slug' => 'Slug',
    'url' => 'Đường dẫn URL',
    'parent_id' => 'Thể loại cha',
    'avatar' => 'Ảnh',
    'description' => 'Mô tả',
    'updated_at' => 'Ngày cập nhật',
    'created_success' => 'Thể loại tin tức đã được thêm!',
    'updated_success' => 'Thể loại tin tức đã được cập nhật!',
    'deleted_success' => 'Thể loại tin tức đã được xóa!',
    'index' => 'Số thứ tự',
    'active' => 'Duyệt',
];