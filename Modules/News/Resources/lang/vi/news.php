<?php

return [
    'news' => 'Tin tức',
    'title' => 'Tiêu đề',
    'slug' => 'Slug',
    'url' => 'Đường dẫn URL',
    'news_type_id' => 'Thể loại',
    'image' => 'Ảnh',
    'description' => 'Mô tả',
    'content' => 'Nội dung',
    'active' => 'Duyệt',
    'index' => 'Số thứ tự',
    'created_at' => 'Ngày tạo',
    'created_success' => 'Tin tức đã được thêm!',
    'updated_success' => 'Tin tức đã được cập nhật!',
    'deleted_success' => 'Tin tức đã được xóa!',
];