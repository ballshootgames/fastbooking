<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
$subdomain = \URL::getDefaultParameters()?\URL::getDefaultParameters()['subdomain']:"";
$appRoot = config('app.root').".";
if (\URL::getRequest()->getHost()!=$appRoot.config('app.domain') || !empty($subdomain))
    $appRoot = "";

Route::domain('{subdomain}.'.$appRoot.config('app.domain'))->group(function () {
    Route::group(['middleware' => ['subdomain', 'auth']], function (){
        Route::resource('/news', 'NewsController');
        Route::resource('/news-type', 'NewsTypeController');
    });
});
