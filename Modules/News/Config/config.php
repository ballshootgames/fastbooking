<?php

return [
    'name' => 'News',
    'link' => [
        'icon' => 'fa fa-cart-plus',
        'title' => __('Quản lý Tin tức'),
        'child' => [
            [
                'icon' => 'fa fa-umbrella',
                'title' => __('Tin tức'),
                'href' => 'news',
                'permission' => 'NewsController@index'
            ],
            [
                'icon' => 'fa fa-umbrella',
                'title' => __('Loại tin'),
                'href' => 'news-type',
                'permission' => 'NewsTypeController@index'
            ],
        ]
    ],
    'book' => null,
];
