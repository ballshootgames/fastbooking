<?php
return [
    'vnp_TmnCode' => env('VNP_TMNCODE',''),
    'vnp_HashSecret' => env('VNP_HASHSECRET',''),
    'vnp_Url' => env('VNP_URL',''),
    'settings' => array(
        'http.ConnectionTimeOut' => 30,
        'log.LogEnabled' => true,
        'log.FileName' => storage_path() . '/logs/vnpay.log',
        'log.LogLevel' => 'ERROR'
    ),
];