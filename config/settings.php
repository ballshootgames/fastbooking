<?php
/**
 * Setting app
 */
return [
    "app_logo" => "<b>Fast Booking</b>",
    "app_logo_mini" => "<b>Fast Booking</b>",

    "app_title" => "Fast Booking",

	"avatar_default" => '/images/avatar.png',

    "active" => 1,
    "inactive" => 0,

    "public_avatar" => '/images/avatar/',
    "public_file" => '/file/',

    "perpage" => 20,
    "perpage10" => 10,
	"log_active" => true,
    "format" => [
	    "datetime" => "d/m/Y H:i",
	    "date" => "d/m/Y",
	    "date_js" => 'dd/mm/yyyy',
        "date_" => "d-m-Y"
    ],
    "approved" => [
	    'cancel' => -10,//hủy vé
	    'tmp' => 5,//lưu tạm
	    'save' => 10//lưu
    ],
	"notification_type" => [
		'booking_created' => 'booking_created',//tao booking
		'booking_updated' => 'booking_updated',//cap nhat booking
		'booking_canceled' => 'booking_canceled',//huy booking
		'booking_tmp' => 'booking_tmp',//booking tạm
	],
	//set roles list
	"roles" => [
		'company_1' => 'company_admin',
		'company_2' => 'company_employee',
		'company_3' => 'company_booking',
		'agent_1' => 'agent_admin',
		'agent_2' => 'agent_employee',
        'branch_1' => 'branch_admin',
        'branch_2' => 'branch_employee'
	],
    //type quận huyện thị xã
    "types" => [
        "Quận" => "Quận",
        "Huyện" => "Huyện",
        "Thị xã" => "Thị xã"
    ],
    "statuses" => [
        'giu_cho' => 1,
        'da_thanh_toan' => 2,
        'thanh_toan_50' => 3
    ],
    'keywords' => [
        'pages' => 'Trang',
        'news_type' => 'Loại bài viết',
        'tours_type' => 'Loại tours'
    ],
    'template' => [
        'list' => 0,
        'detail' => 1,
        'page' => 2
    ],
    'total_rec' => [10,15,20,25,30,35,40,45,50,60,70,80,90,100],
    'model_news_type' => 'news_type',
    'model_pages' => 'pages',
    'model_tour_type' => 'tour_type',
]
?>