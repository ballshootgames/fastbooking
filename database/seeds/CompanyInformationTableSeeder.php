<?php

use Illuminate\Database\Seeder;

class CompanyInformationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        Category::create([
//            'name' => 'Foo',
//
//            'children' => [
//                [
//                    'name' => 'Bar',
//
//                    'children' => [
//                        [ 'name' => 'Baz' ],
//                    ],
//                ],
//            ],
//        ]);
        $companyParent = [
                'name' => 'Công ty Eagle Tourist',
                'children' => [
                    [
                        'name' => 'Công ty 1',
                        'children' => [
                            [
                                'name' => 'Đại lý 1-1',
                                'children' => [
                                    ['name' => 'Chi nhánh 1-1-1'],
                                    ['name' => 'Chi nhánh 1-1-2'],
                                    ['name' => 'Chi nhánh 1-1-3'],
                                ],
                            ],
                        ]
                    ]
                ]
        ];

        \App\Company::create($companyParent);
    }
}
