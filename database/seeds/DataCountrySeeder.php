<?php

use Illuminate\Database\Seeder;

class DataCountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = ['countries', 'cities', 'districts', 'wards'];
        foreach ($array as $item){
            $path = 'database/migrations/'.$item.'.sql';
            DB::unprepared(file_get_contents(base_path($path)));
            $this->command->info($item.' table seeded!');
        }
    }
}
