<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $this->call(AdminUserSeeder::class);
        $this->call(PermissionsTableSeeder::class);
//	    $this->call(DataDemoSeeder::class);
	    $this->call(DemoTitleDepartmentSeeder::class);
        $this->call(DataCountrySeeder::class);
    }
}
