<?php

use Illuminate\Database\Seeder;

class DemoTitleDepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try{
            \App\UserTitle::create([
                'name' => 'Trưởng phòng',
            ]);
            \App\UserTitle::create([
                'name' => 'Nhân viên'
            ]);
            \App\UserDepartment::create([
                'name' => 'Kinh doanh'
            ]);
            \App\UserDepartment::create([
                'name' => 'Nhân sự'
            ]);
        }catch (\Illuminate\Database\QueryException $exception){
            echo $exception->getMessage();
        }
    }
}
