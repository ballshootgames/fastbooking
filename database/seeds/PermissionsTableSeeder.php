<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arPermissions = [
            "1" => ["HomeController@index", "Trang chủ"],

            // QUYỀN BOOKING - DANH SÁCH ĐẶT TOUR
            "2" => ["BookingController@index", "Danh sách đặt tour"],
            "3" => ["BookingController@show", "Danh sách đặt tour"],
            "4" => ["BookingController@store", "Danh sách đặt tour"],
            "5" => ["BookingController@update", "Danh sách đặt tour"],
            "6" => ["BookingController@destroy", "Danh sách đặt tour"],
            "7" => ["BookingController@active", "Danh sách đặt tour"],

            //BỔ SUNG QUYỀN BOOKING PEOPLE - KHÁCH HÀNG ĐẶT VÉ
            "8" => ["BookingPeopleController@index", "Khách hàng đặt vé"],
            "9" => ["BookingPeopleController@show", "Khách hàng đặt vé"],
            "10" => ["BookingPeopleController@store", "Khách hàng đặt vé"],
            "11" => ["BookingPeopleController@update", "Khách hàng đặt vé"],
            "12" => ["BookingPeopleController@destroy", "Khách hàng đặt vé"],
            "13" => ["BookingPeopleController@active", "Khách hàng đặt vé"],

            // QUYỀN CUSTOMER - DANH SÁCH KHÁCH HÀNG
            "14" => ["CustomersController@index", "Danh sách khách hàng"],
            "15" => ["CustomersController@show", "Danh sách khách hàng"],
            "16" => ["CustomersController@store", "Danh sách khách hàng"],
            "17" => ["CustomersController@update", "Danh sách khách hàng"],
            "18" => ["CustomersController@destroy", "Danh sách khách hàng"],
            "19" => ["CustomersController@active", "Danh sách khách hàng"],

            // QUYỀN CUSTOMER TYPE - LOẠI KHÁCH HÀNG
            "20" => ["CustomerTypeController@index", "Loại khách hàng"],
            "21" => ["CustomerTypeController@show", "Loại khách hàng"],
            "22" => ["CustomerTypeController@store", "Loại khách hàng"],
            "23" => ["CustomerTypeController@update", "Loại khách hàng"],
            "24" => ["CustomerTypeController@destroy", "Loại khách hàng"],
            "25" => ["CustomerTypeController@active", "Loại khách hàng"],

            // QUYỀN NEWS - TIN TỨC
            "26" => ["NewsController@index", "Tin tức"],
            "27" => ["NewsController@show", "Tin tức"],
            "28" => ["NewsController@store", "Tin tức"],
            "29" => ["NewsController@update", "Tin tức"],
            "30" => ["NewsController@destroy", "Tin tức"],
            "31" => ["NewsController@active", "Tin tức"],

            // QUYỀN NEWS TYPE - LOẠI TIN TỨC
            "32" => ["NewsTypeController@index", "Loại tin"],
            "33" => ["NewsTypeController@show", "Loại tin"],
            "34" => ["NewsTypeController@store", "Loại tin"],
            "35" => ["NewsTypeController@update", "Loại tin"],
            "36" => ["NewsTypeController@destroy", "Loại tin"],
            "37" => ["NewsTypeController@active", "Loại tin"],

            // QUYỀN TOURS - SẢN PHẨM TOUR
            "38" => ["ToursController@index", "Sản phẩm"],
            "39" => ["ToursController@show", "Sản phẩm"],
            "40" => ["ToursController@store", "Sản phẩm"],
            "41" => ["ToursController@update", "Sản phẩm"],
            "42" => ["ToursController@destroy", "Sản phẩm"],
            "43" => ["ToursController@active", "Sản phẩm"],
            // Phần tour khi tạo thì cho phép sao chép, nếu ko có tạo thì không sao chép
            "44" => ["ToursController@getCopyTour", "Sản phẩm"],

            // QUYỀN TOURS  - CHƯƠNG TRÌNH TOUR
            "45" => ["TourScheduleController@index", "Chương trình Tour"],
            "46" => ["TourScheduleController@show", "Chương trình Tour"],
            "47" => ["TourScheduleController@store", "Chương trình Tour"],
            "48" => ["TourScheduleController@update", "Chương trình Tour"],
            "49" => ["TourScheduleController@destroy", "Chương trình Tour"],
            "50" => ["TourScheduleController@active", "Chương trình Tour"],

            // QUYỀN TOUR PROPERTY  - HẠNG TOUR, LOẠI HÌNH, DANH MỤC, LOẠI TOUR, ĐỊA ĐIỂM
            "51" => ["TourPropertyController@index", "Thành phần Tour"],
            "52" => ["TourPropertyController@show", "Thành phần Tour"],
            "53" => ["TourPropertyController@store", "Thành phần Tour"],
            "54" => ["TourPropertyController@update", "Thành phần Tour"],
            "55" => ["TourPropertyController@destroy", "Thành phần Tour"],
            "56" => ["TourPropertyController@active", "Thành phần Tour"],

            // QUYỀN COMMENT TOUR - BÌNH LUẬN TOUR
            "57" => ["CommentController@index", "Bình luận Tour"],
            "58" => ["CommentController@show", "Bình luận Tour"],
            "59" => ["CommentController@destroy", "Bình luận Tour"],

            // QUYỀN QUẢN LÝ THEME
            // QUYỀN SLIDER  - Quản lý slider
            "60" => ["SlideController@index", "Quản lý slider"],
            "61" => ["SlideController@show", "Quản lý slider"],
            "62" => ["SlideController@store", "Quản lý slider"],
            "63" => ["SlideController@update", "Quản lý slider"],
            "64" => ["SlideController@destroy", "Quản lý slider"],
            "65" => ["SlideController@active", "Quản lý slider"],
            // QUYỀN BANNER  - Quản lý banner
            "66" => ["BannerController@index", "Quản lý banner"],
            "67" => ["BannerController@show", "Quản lý banner"],
            "68" => ["BannerController@store", "Quản lý banner"],
            "69" => ["BannerController@update", "Quản lý banner"],
            "70" => ["BannerController@destroy", "Quản lý banner"],
            "71" => ["BannerController@active", "Quản lý banner"],
            // QUYỀN PARTNER  - Quản lý partner
            "72" => ["PartnerController@index", "Quản lý KH & ĐT"],
            "73" => ["PartnerController@show", "Quản lý KH & ĐT"],
            "74" => ["PartnerController@store", "Quản lý KH & ĐT"],
            "75" => ["PartnerController@update", "Quản lý KH & ĐT"],
            "76" => ["PartnerController@destroy", "Quản lý KH & ĐT"],
            "77" => ["PartnerController@active", "Quản lý KH & ĐT"],
            // QUYỀN MENU  - Quản lý Menu
            "78" => ["MenuController@index", "Quản lý Menu"],
            "79" => ["MenuController@show", "Quản lý Menu"],
            "80" => ["MenuController@store", "Quản lý Menu"],
            "81" => ["MenuController@update", "Quản lý Menu"],
            "82" => ["MenuController@destroy", "Quản lý Menu"],
            "83" => ["MenuController@active", "Quản lý Menu"],
            // QUYỀN PAGE  - Quản lý Page
            "84" => ["PageController@index", "Quản lý Trang nội dung"],
            "85" => ["PageController@show", "Quản lý Trang nội dung"],
            "86" => ["PageController@store", "Quản lý Trang nội dung"],
            "87" => ["PageController@update", "Quản lý Trang nội dung"],
            "88" => ["PageController@destroy", "Quản lý Trang nội dung"],
            "89" => ["PageController@active", "Quản lý Trang nội dung"],
            // QUYỀN CONTACT  - Quản lý contact
            "90" => ["ContactController@index", "Quản lý Liên hệ"],
            "91" => ["ContactController@show", "Quản lý Liên hệ"],
            "92" => ["ContactController@store", "Quản lý Liên hệ"],
            "93" => ["ContactController@update", "Quản lý Liên hệ"],
            "94" => ["ContactController@destroy", "Quản lý Liên hệ"],
            "95" => ["ContactController@active", "Quản lý Liên hệ"],

            // QUYỀN REPORT - BÁO CÁO THỐNG KÊ
            "96" => ["ReportsController@numberBookingByDate", "Báo cáo Thống kê"],
            "97" => ["ReportsController@numberBookingByMonth", "Báo cáo Thống kê"],
            "98" => ["ReportsController@numberBookingByYear", "Báo cáo Thống kê"],
            "99" => ["ReportsController@peopleBookingByDate", "Báo cáo Thống kê"],
            "100" => ["ReportsController@peopleBookingByMonth", "Báo cáo Thống kê"],
            "101" => ["ReportsController@peopleBookingByYear", "Báo cáo Thống kê"],
            "102" => ["ReportsController@financeBookingByDate", "Báo cáo Thống kê"],
            "103" => ["ReportsController@financeBookingByMonth", "Báo cáo Thống kê"],
            "104" => ["ReportsController@financeBookingByYear", "Báo cáo Thống kê"],

            //BỔ SUNG QUYỀN COMPANY INFORMATION
            "105" => ["CompaniesController@index", "Công ty"],
            "106" => ["CompaniesController@show", "Công ty"],
            "107" => ["CompaniesController@store", "Công ty"],
            "108" => ["CompaniesController@update", "Công ty"],
            "109" => ["CompaniesController@destroy", "Công ty"],
            "110" => ["CompaniesController@active", "Công ty"],

            // QUYỀN USER - QUẢN LÝ NGƯỜI DÙNG
            "111" => ["UsersController@index", "Tài khoản Người dùng"],
            "112" => ["UsersController@show", "Tài khoản Người dùng"],
            "113" => ["UsersController@store", "Tài khoản Người dùng"],
            "114" => ["UsersController@update", "Tài khoản Người dùng"],
            "115" => ["UsersController@destroy", "Tài khoản Người dùng"],
            "116" => ["UsersController@active", "Tài khoản Người dùng"],

            // QUYỀN DEPARTMENT - QUẢN LÝ PHÒNG BAN
            "117" => ["UserDepartmentController@index", "Quản lý Phòng ban"],
            "118" => ["UserDepartmentController@show", "Quản lý Phòng ban"],
            "119" => ["UserDepartmentController@store", "Quản lý Phòng ban"],
            "120" => ["UserDepartmentController@update", "Quản lý Phòng ban"],
            "121" => ["UserDepartmentController@destroy", "Quản lý Phòng ban"],
            "122" => ["UserDepartmentController@active", "Quản lý Phòng ban"],

            // QUYỀN USERTITLE - QUẢN LÝ CHỨC DANH
            "123" => ["UserTitleController@index", "Quản lý Chức danh"],
            "124" => ["UserTitleController@show", "Quản lý Chức danh"],
            "125" => ["UserTitleController@store", "Quản lý Chức danh"],
            "126" => ["UserTitleController@update", "Quản lý Chức danh"],
            "127" => ["UserTitleController@destroy", "Quản lý Chức danh"],
            "128" => ["UserTitleController@active", "Quản lý Chức danh"],

            // QUYỀN ROLE - QUẢN LÝ VAI TRÒ
            "129" => ["RolesController@index", "Quản lý Vai trò"],
            "130" => ["RolesController@show", "Quản lý Vai trò"],
            "131" => ["RolesController@store", "Quản lý Vai trò"],
            "132" => ["RolesController@update", "Quản lý Vai trò"],
            "133" => ["RolesController@destroy", "Quản lý Vai trò"],
            "134" => ["RolesController@active", "Quản lý Vai trò"],

            // QUYỀN SETTING - CẤU HÌNH HỆ THỐNG
            "135" => ["SettingController", "Thông tin công ty"],

            //BỔ SUNG QUYỀN STATUS
            "136" => ["StatusController@index", "Trạng thái"],
            "137" => ["StatusController@show", "Trạng thái"],
            "138" => ["StatusController@store", "Trạng thái"],
            "139" => ["StatusController@update", "Trạng thái"],
            "140" => ["StatusController@destroy", "Trạng thái"],
            "141" => ["StatusController@active", "Trạng thái"],

            //BỔ SUNG QUYỀN PAYMENTS
            "142" => ["PaymentsController@index", "Phương thức thanh toán"],
            "143" => ["PaymentsController@show", "Phương thức thanh toán"],
            "144" => ["PaymentsController@store", "Phương thức thanh toán"],
            "145" => ["PaymentsController@update", "Phương thức thanh toán"],
            "146" => ["PaymentsController@destroy", "Phương thức thanh toán"],
            "147" => ["PaymentsController@active", "Phương thức thanh toán"],

            //BỔ SUNG QUYỀN BOOKING SOURCE
            "148" => ["BookingSourceController@index", "Nguồn đặt vé"],
            "149" => ["BookingSourceController@show", "Nguồn đặt vé"],
            "150" => ["BookingSourceController@store", "Nguồn đặt vé"],
            "151" => ["BookingSourceController@update", "Nguồn đặt vé"],
            "152" => ["BookingSourceController@destroy", "Nguồn đặt vé"],
            "153" => ["BookingSourceController@active", "Nguồn đặt vé"],

            //BỔ SUNG MODULE QUỐC GIA
            "154" => ["CountryController@index", "Quản lý Quốc gia"],
            "155" => ["CountryController@show", "Quản lý Quốc gia"],
            "156" => ["CountryController@store", "Quản lý Quốc gia"],
            "157" => ["CountryController@update", "Quản lý Quốc gia"],
            "158" => ["CountryController@destroy", "Quản lý Quốc gia"],
            "159" => ["CountryController@active", "Quản lý Quốc gia"],

            //BỔ SUNG MODULE TỈNH, THÀNH PHỐ
            "160" => ["CityController@index", "Quản lý Tỉnh, thành phố"],
            "161" => ["CityController@show", "Quản lý Tỉnh, thành phố"],
            "162" => ["CityController@store", "Quản lý Tỉnh, thành phố"],
            "163" => ["CityController@update", "Quản lý Tỉnh, thành phố"],
            "164" => ["CityController@destroy", "Quản lý Tỉnh, thành phố"],
            "165" => ["CityController@active", "Quản lý Tỉnh, thành phố"],

            //BỔ SUNG MODULE QUẬN, HUYỆN, XÃ
            "166" => ["DistrictController@index", "Quản lý Quận, Huyện"],
            "167" => ["DistrictController@show", "Quản lý Quận, Huyện"],
            "168" => ["DistrictController@store", "Quản lý Quận, Huyện"],
            "169" => ["DistrictController@update", "Quản lý Quận, Huyện"],
            "170" => ["DistrictController@destroy", "Quản lý Quận, Huyện"],
            "171" => ["DistrictController@active", "Quản lý Quận, Huyện"],
            //BỔ SUNG QUYỀN ĐĂNG KÍ NHẬN TIN CHO THEME TOMA
            "172" => ["NewsletterController@index", "Đăng ký nhận tin"],
            "173" => ["NewsletterController@show", "Đăng ký nhận tin"],
            "174" => ["NewsletterController@store", "Đăng ký nhận tin"],
            "175" => ["NewsletterController@update", "Đăng ký nhận tin"],
            "176" => ["NewsletterController@destroy", "Đăng ký nhận tin"],
            "177" => ["NewsletterController@active", "Đăng ký nhận tin"],

            //BỔ SUNG MODULE Gallery
            "178" => ["GalleryController@index", "Quản lý album khách hàng"],
            "179" => ["GalleryController@show", "Quản lý album khách hàng"],
            "180" => ["GalleryController@store", "Quản lý album khách hàng"],
            "181" => ["GalleryController@update", "Quản lý album khách hàng"],
            "182" => ["GalleryController@destroy", "Quản lý album khách hàng"],
            "183" => ["GalleryController@active", "Quản lý album khách hàng"],

            // QUYỀN TRẢ GÓP  - Quản lý trả góp
            "184" => ["InstallmentController@index", "Quản lý Trả góp"],
            "185" => ["InstallmentController@show", "Quản lý Trả góp"],
            "186" => ["InstallmentController@store", "Quản lý Trả góp"],
            "187" => ["InstallmentController@update", "Quản lý Trả góp"],
            "188" => ["InstallmentController@destroy", "Quản lý Trả góp"],
            "189" => ["InstallmentController@active", "Quản lý Trả góp"],

            // QUYỀN Office  - Quản lý đơn vị công tác
            "190" => ["OfficeController@index", "Quản lý Đơn vị công tác"],
            "191" => ["OfficeController@show", "Quản lý Đơn vị công tác"],
            "192" => ["OfficeController@store", "Quản lý Đơn vị công tác"],
            "193" => ["OfficeController@update", "Quản lý Đơn vị công tác"],
            "194" => ["OfficeController@destroy", "Quản lý Đơn vị công tác"],
            "195" => ["OfficeController@active", "Quản lý Đơn vị công tác"],

            // QUYỀN Supplier  - Quản lý Nhà cung cấp
            "196" => ["SupplierController@index", "Quản lý Nhà cung cấp"],
            "197" => ["SupplierController@show", "Quản lý Nhà cung cấp"],
            "198" => ["SupplierController@store", "Quản lý Nhà cung cấp"],
            "199" => ["SupplierController@update", "Quản lý Nhà cung cấp"],
            "200" => ["SupplierController@destroy", "Quản lý Nhà cung cấp"],
            "201" => ["SupplierController@active", "Quản lý Nhà cung cấp"],

//            "146" => ["UsersController@postProfile", "Chỉnh sửa thông tin cá nhân"],

//            "150" => ["BackupController@index", "Sao lưu"],
//            "147" => ["BackupController@create", "Sao lưu"],
//            "148" => ["BackupController@delete", "Sao lưu"],
//            "149" => ["BackupController@download", "Sao lưu"],

//            "151" => ["ToursController@getGuides", "Tour - Hướng dẫn viên"],
//            "152" => ["ToursController@postGuides", "Tour - Hướng dẫn viên"],
//            "153" => ["ToursController@deleteGuide", "Tour - Hướng dẫn viên"],

//            "154" => ["CitiesController@index", "Địa điểm"],
//            "155" => ["CitiesController@show", "Địa điểm"],
//            "156" => ["CitiesController@store", "Địa điểm"],
//            "157" => ["CitiesController@update", "Địa điểm"],
//            "158" => ["CitiesController@destroy", "Địa điểm"],

//            "159" => ["ModulesController@index", "Quản lý Module"],
//            "160" => ["ModulesController@active", "Quản lý Module"],

//            "161" => ["BranchController@postProfile", "Chỉnh sửa thông tin chi nhánh"],

            //BỔ SUNG THÊM QUYỀN NẾU CÓ, chú ý key là ID của permission
        ];

        //ADD PERMISSIONS - Thêm các quyền
        DB::table( 'permissions' )->delete();//empty permission
        $addPermissions = [];
        foreach ($arPermissions as $name => $label){
            $addPermissions[] = [
                'id' => $name,
                'name' => $label[0],
                'label' => $label[1]
            ];
        }
        DB::table('permissions')->insert($addPermissions);

        //ADD ROLE - Them vai tro
        $datenow = date( 'Y-m-d H:i:s' );
        $role = [
            [ 'id' => 1, 'name' => 'company_admin', 'label' => 'Admin', 'created_at' => $datenow, 'updated_at' => $datenow ],
            [ 'id' => 2, 'name' => 'Director', 'label' => 'Giám Đốc', 'created_at' => $datenow, 'updated_at' => $datenow ],
            [ 'id' => 3, 'name' => 'ViceDirector', 'label' => 'Phó Giám Đốc', 'created_at' => $datenow, 'updated_at' => $datenow ],
            [ 'id' => 4, 'name' => 'Manager', 'label' => 'Trưởng phòng Kinh Doanh', 'created_at' => $datenow, 'updated_at' => $datenow ],
            [ 'id' => 5, 'name' => 'Employee', 'label' => 'Nhân viên Kinh Doanh', 'created_at' => $datenow, 'updated_at' => $datenow ],
//            [ 'id' => 6, 'name' => 'branch_admin', 'label' => 'Admin chi nhánh', 'created_at' => $datenow, 'updated_at' => $datenow ],
//            [ 'id' => 7, 'name' => 'branch_employee', 'label' => 'Nhân viên chi nhánh', 'created_at' => $datenow, 'updated_at' => $datenow ],
        ];
        $addRoles = [];
        foreach ($role as $key => $label){
            if(\App\Role::where('id',$label['id'])->count() == 0 )
                $addRoles[] = [
                    'id' => $label['id'],
                    'name' => $label['name'],
                    'label' => $label['label'],
                    'created_at' => $datenow,
                    'updated_at' => $datenow
                ];
        }
        //KIỂM TRA VÀ THÊM CÁC VAI TRÒ TRUYỀN VÀO NẾU CÓ
        if (count($addRoles) > 0){
            DB::table('roles')->insert($addRoles);
        }

        //BỔ SUNG ID QUYỀN NẾU CÓ
        //Full quyền của Admin công ty
        $persAdmin = \App\Permission::pluck('id');
        //Quyền của Giám đốc
        $persDirector = [
            1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183
        ];
        //Quyền của Phó Giám Đốc
        $persViceDirector = [
            1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183
        ];
        //Quyền của Trưởng Phòng
        $persManager = [
            1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,32,33,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,111,112,117,118,123,124,129,130,135,136,137,142,143,148,149,154,155,160,161,166,167,172,173,174,175,176,177,178,179,180,181,182,183
        ];
        //Quyền của Nhân viên
        $persEmployee = [
            1,2,3,4,5,6,8,9,10,11,12,14,15,16,17,18,20,21,22,23,24,26,27,32,33,38,39,40,41,42,44,45,46,47,48,49,51,52,53,54,55,57,58,60,61,62,63,64,66,67,68,69,70,72,73,74,75,76,78,79,80,81,82,84,85,86,87,88,90,91,92,93,135,136,137,142,143,148,149,154,155,160,161,166,167,172,173,174,175,176,177,178,179,180,181,182,183
        ];
//        //Quyền của admin đại lý
//        $persAgentAdmin = [
//            5,20,21,24,31,32,36,37,41,42,46,47,51,52,56,57,61,61,65,68,72,73,76,77,78,79,80,82,83,86,87,88
//        ];
//        //Quyền của nhân viên đại lý
//        $persAgentEmployee = [
//            5,20,21,31,32,36,37,41,42,46,47,51,52,56,57,61,61,65,68,72,73,76,77,78,79,80,82,83,86,87,88
//        ];
//        //Quyền của admin chi nhánh
//        $persBranchAdmin = [
//            5,26,27,31,32,36,37,41,42,46,47,51,52,56,57,61,61,65,68,72,73,76,77,78,79,80,82,83,86,87,88,91
//        ];
//        //Quyền của nhân viên chi nhánh
//        $persBranchEmployee = [
//            5,26,27,31,32,36,37,41,42,46,47,51,52,56,57,61,61,65,68,72,73,76,77,78,79,80,82,83,86,87,88
//        ];

        //Gán quyền vào Vai trò Admin
        $rolePerAdminCompany = \App\Role::findOrFail(1);
        $rolePerAdminCompany->permissions()->sync($persAdmin);

        //Gán quyền vào Vai trò Director Company
        $rolePerDirector = \App\Role::findOrFail(2);
        $rolePerDirector->permissions()->sync($persDirector);

        //Gán quyền vào Vai trò Vice Director Company
        $rolePerViceDirector = \App\Role::findOrFail(3);
        $rolePerViceDirector->permissions()->sync($persViceDirector);

        //Gán quyền vào Vai trò Manager
        $rolePerManager = \App\Role::findOrFail(4);
        $rolePerManager->permissions()->sync($persManager);

        //Gán quyền vào Vai trò Employee
        $rolePerEmployee = \App\Role::findOrFail(5);
        $rolePerEmployee->permissions()->sync($persEmployee);

        //Gán quyền vào Vai trò Booking Company
        /*$rolePerCompanyBooking = \App\Role::findOrFail(3);
        $rolePerCompanyBooking->permissions()->sync($persCompanyBooking);*/

//        //Gán quyền vào Vai trò Admin Agent
//        $rolePerAgentAdmin = \App\Role::findOrFail(4);
//        $rolePerAgentAdmin->permissions()->sync($persAgentAdmin);
//
//        //Gán quyền vào Vai trò Employee Agent
//        $rolePerAgentEmployee = \App\Role::findOrFail(5);
//        $rolePerAgentEmployee->permissions()->sync($persAgentEmployee);
//
//        //Gán quyền vào Vai trò Admin Branch
//        $rolePerBranchAdmin = \App\Role::findOrFail(6);
//        $rolePerBranchAdmin->permissions()->sync($persBranchAdmin);
//
//        //Gán quyền vào Vai trò Employee Branch
//        $rolePerBranchEmployee = \App\Role::findOrFail(7);
//        $rolePerBranchEmployee->permissions()->sync($persBranchEmployee);

        //Set tài khoản ID=1 là Admin
        $roleAdmin = \App\User::findOrFail(1);
        $roleAdmin->roles()->sync([1]);
    }
}
