<?php

use Illuminate\Database\Seeder;

class PermissionsThemeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arPermissions = [
//            "1" => ["BackupController@create", "Sao lưu - thêm mới"],
//            "2" => ["BackupController@delete", "Sao lưu - xóa"],
//            "3" => ["BackupController@download", "Sao lưu - download"],
//            "4" => ["BackupController@index", "Sao lưu - danh sách"],

            "1" => ["HomeController@index", "1.Trang chủ"],

            // QUYỀN BOOKING - DANH SÁCH ĐẶT TOUR
            "2" => ["BookingController@index", "2.1.Booking - Danh sách"],
            "3" => ["BookingController@show", "2.1.Booking - Chi tiết"],
            "4" => ["BookingController@store", "2.1.Booking - Thêm mới"],
            "5" => ["BookingController@update", "2.1.Booking - Chỉnh sửa"],
            "6" => ["BookingController@destroy", "2.1.Booking - Xóa"],

            //BỔ SUNG QUYỀN BOOKING PEOPLE - KHÁCH HÀNG ĐẶT VÉ
            "7" => ["BookingPeopleController@index", "2.2.Thông tin khách hàng booking - Danh sách"],
            "8" => ["BookingPeopleController@show", "2.2.Thông tin khách hàng booking - Chi tiết"],
            "9" => ["BookingPeopleController@store", "2.2.Thông tin khách hàng booking - Thêm mới"],
            "10" => ["BookingPeopleController@update", "2.2.Thông tin khách hàng booking - Chỉnh sửa"],
            "11" => ["BookingPeopleController@destroy", "2.2.Thông tin khách hàng booking - Xóa"],

            // QUYỀN CUSTOMER - DANH SÁCH KHÁCH HÀNG
            "12" => ["CustomersController@index", "3.1.Khách hàng - Danh sách"],
            "13" => ["CustomersController@show", "3.1.Khách hàng - Chi tiết"],
            "14" => ["CustomersController@store", "3.1.Khách hàng - Thêm mới"],
            "15" => ["CustomersController@update", "3.1.Khách hàng - Chỉnh sửa"],
            "16" => ["CustomersController@destroy", "3.1.Khách hàng - Xóa"],

            // QUYỀN CUSTOMER TYPE - LOẠI KHÁCH HÀNG
            "17" => ["CustomerTypeController@index", "3.2.Loại khách hàng - Danh sách"],
            "18" => ["CustomerTypeController@show", "3.2.Loại khách hàng - Chi tiết"],
            "19" => ["CustomerTypeController@store", "3.2.Loại khách hàng - Thêm mới"],
            "20" => ["CustomerTypeController@update", "3.2.Loại khách hàng - Chỉnh sửa"],
            "21" => ["CustomerTypeController@destroy", "3.2.Loại khách hàng - Xóa"],

            // QUYỀN NEWS - TIN TỨC
            "22" => ["NewsController@index", "4.1.Tin tức - Danh sách"],
            "23" => ["NewsController@show", "4.1.Tin tức - Chi tiết"],
            "24" => ["NewsController@store", "4.1.Tin tức - Thêm mới"],
            "25" => ["NewsController@update", "4.1.Tin tức - Chỉnh sửa"],
            "26" => ["NewsController@destroy", "4.1.Tin tức - Xóa"],

            // QUYỀN NEWS TYPE - LOẠI TIN TỨC
            "27" => ["NewsTypeController@index", "4.2.Loại tin - Danh sách"],
            "28" => ["NewsTypeController@show", "4.2.Loại tin - Chi tiết"],
            "29" => ["NewsTypeController@store", "4.2.Loại tin - Thêm mới"],
            "30" => ["NewsTypeController@update", "4.2.Loại tin - Chỉnh sửa"],
            "31" => ["NewsTypeController@destroy", "4.2.Loại tin - Xóa"],

            // QUYỀN TOURS - SẢN PHẨM TOUR
            "32" => ["ToursController@index", "5.1.Tours - Danh sách"],
            "33" => ["ToursController@show", "5.1.Tours - Chi tiết"],
            "34" => ["ToursController@store", "5.1.Tours - Thêm mới"],
            "35" => ["ToursController@update", "5.1.Tours - Chỉnh sửa"],
            "36" => ["ToursController@destroy", "5.1.Tours - Xóa"],
            "37" => ["ToursController@getCopyTour", "5.1.Tours - Sao chép tour"],

            // QUYỀN TOURS  - CHƯƠNG TRÌNH TOUR
            "38" => ["TourScheduleController@index", "5.2.Chương trình Tour - Danh sách"],
            "39" => ["TourScheduleController@show", "5.2.Chương trình Tour - Chi tiết"],
            "40" => ["TourScheduleController@store", "5.2.Chương trình Tour - Thêm mới"],
            "41" => ["TourScheduleController@update", "5.2.Chương trình Tour - Chỉnh sửa"],
            "42" => ["TourScheduleController@destroy", "5.2.Chương trình Tour - Xóa"],

            // QUYỀN TOUR PROPERTY  - HẠNG TOUR, LOẠI HÌNH, DANH MỤC, LOẠI TOUR, ĐỊA ĐIỂM
            "43" => ["TourPropertyController@index", "5.3.Thành phần Tour - Danh sách"],
            "44" => ["TourPropertyController@show", "5.3.Thành phần Tour - Chi tiết"],
            "45" => ["TourPropertyController@store", "5.3.Thành phần Tour - Thêm mới"],
            "46" => ["TourPropertyController@update", "5.3.Thành phần Tour - Chỉnh sửa"],
            "47" => ["TourPropertyController@destroy", "5.3.Thành phần Tour - Xóa"],

            // QUYỀN COMMENT TOUR - BÌNH LUẬN TOUR
            "48" => ["CommentController@index", "5.4.Tours - Danh sách"],
            "49" => ["CommentController@show", "5.4.Tours - Chi tiết"],
            "50" => ["CommentController@destroy", "5.4.Tours - Xóa"],

            // QUYỀN QUẢN LÝ THEME
            // QUYỀN SLIDER  - Quản lý slider
            "51" => ["SlideController@index", "6.1.Quản lý slider - Danh sách"],
            "52" => ["SlideController@show", "6.1.Quản lý slider - Chi tiết"],
            "53" => ["SlideController@store", "6.1.Quản lý slider - Thêm mới"],
            "54" => ["SlideController@update", "6.1.Quản lý slider - Chỉnh sửa"],
            "55" => ["SlideController@destroy", "6.1.Quản lý slider - Xóa"],
            // QUYỀN BANNER  - Quản lý banner
            "56" => ["BannerController@index", "6.2.Quản lý banner - Danh sách"],
            "57" => ["BannerController@show", "6.2.Quản lý banner - Chi tiết"],
            "58" => ["BannerController@store", "6.2.Quản lý banner - Thêm mới"],
            "59" => ["BannerController@update", "6.2.Quản lý banner - Chỉnh sửa"],
            "60" => ["BannerController@destroy", "6.2.Quản lý banner - Xóa"],
            // QUYỀN PARTNER  - Quản lý partner
            "61" => ["PartnerController@index", "6.3.Quản lý Partner - Danh sách"],
            "62" => ["PartnerController@show", "6.3.Quản lý Partner - Chi tiết"],
            "63" => ["PartnerController@store", "6.3.Quản lý Partner - Thêm mới"],
            "64" => ["PartnerController@update", "6.3.Quản lý Partner - Chỉnh sửa"],
            "65" => ["PartnerController@destroy", "6.3.Quản lý Partner - Xóa"],
            // QUYỀN MENU  - Quản lý Menu
            "66" => ["MenuController@index", "6.4.Quản lý Menu - Danh sách"],
            "67" => ["MenuController@show", "6.4.Quản lý Menu - Chi tiết"],
            "68" => ["MenuController@store", "6.4.Quản lý Menu - Thêm mới"],
            "69" => ["MenuController@update", "6.4.Quản lý Menu - Chỉnh sửa"],
            "70" => ["MenuController@destroy", "6.4.Quản lý Menu - Xóa"],
            // QUYỀN PAGE  - Quản lý Page
            "71" => ["PageController@index", "6.5.Quản lý Trang nội dung - Danh sách"],
            "72" => ["PageController@show", "6.5.Quản lý Trang nội dung - Chi tiết"],
            "73" => ["PageController@store", "6.5.Quản lý Trang nội dung - Thêm mới"],
            "74" => ["PageController@update", "6.5.Quản lý Trang nội dung - Chỉnh sửa"],
            "75" => ["PageController@destroy", "6.5.Quản lý Trang nội dung - Xóa"],
            // QUYỀN CONTACT  - Quản lý contact
            "76" => ["ContactController@index", "6.6.Quản lý Liên hệ - Danh sách"],
            "77" => ["ContactController@show", "6.6.Quản lý Liên hệ - Chi tiết"],
            "78" => ["ContactController@store", "6.6.Quản lý Liên hệ - Thêm mới"],
            "79" => ["ContactController@update", "6.6.Quản lý Liên hệ - Chỉnh sửa"],
            "80" => ["ContactController@destroy", "6.6.Quản lý Liên hệ - Xóa"],

            // QUYỀN REPORT - BÁO CÁO THỐNG KÊ
            "81" => ["ReportsController@numberBookingByDate", "7.1.Số lượng vé theo ngày"],
            "82" => ["ReportsController@numberBookingByMonth", "7.1.Số lượng vé theo tháng"],
            "83" => ["ReportsController@numberBookingByYear", "7.1.Số lượng vé theo năm"],
            "84" => ["ReportsController@peopleBookingByDate", "7.2.Số lượng người theo ngày"],
            "85" => ["ReportsController@peopleBookingByMonth", "7.2.Số lượng người theo tháng"],
            "86" => ["ReportsController@peopleBookingByYear", "7.2.Số lượng người theo năm"],
            "87" => ["ReportsController@financeBookingByDate", "7.3.Doanh thu theo ngày"],
            "88" => ["ReportsController@financeBookingByMonth", "7.3.Doanh thu theo tháng"],
            "89" => ["ReportsController@financeBookingByYear", "7.3.Doanh thu theo năm"],

            //BỔ SUNG QUYỀN COMPANY INFORMATION
            "90" => ["CompaniesController@index", "8.1.Công ty - Danh sách"],
            "91" => ["CompaniesController@show", "8.1.Công ty - Chi tiết"],
            "92" => ["CompaniesController@store", "8.1.Công ty - Thêm mới"],
            "93" => ["CompaniesController@update", "8.1.Công ty - Chỉnh sửa"],
            "94" => ["CompaniesController@destroy", "8.1.Công ty - Xóa"],

            // QUYỀN USER - QUẢN LÝ NGƯỜI DÙNG
            "95" => ["UsersController@index", "8.2.Quản trị người dùng - danh sách"],
            "96" => ["UsersController@show", "8.2.Quản trị người dùng - chi tiết"],
            "97" => ["UsersController@store", "8.2.Quản trị người dùng - thêm mới"],
            "98" => ["UsersController@update", "8.2.Quản trị người dùng - chỉnh sửa"],
            "99" => ["UsersController@destroy", "8.2.Quản trị người dùng - xóa"],

            // QUYỀN DEPARTMENT - QUẢN LÝ PHÒNG BAN
            "100" => ["UserDepartmentController@index", "8.3.Phòng ban - Danh sách"],
            "101" => ["UserDepartmentController@show", "8.3.Phòng ban - Chi tiết"],
            "102" => ["UserDepartmentController@store", "8.3.Phòng ban - Thêm mới"],
            "103" => ["UserDepartmentController@update", "8.3.Phòng ban - Chỉnh sửa"],
            "104" => ["UserDepartmentController@destroy", "8.3.Phòng ban - Xóa"],

            // QUYỀN USERTITLE - QUẢN LÝ CHỨC DANH
            "105" => ["UserTitleController@index", "8.4.Chức danh - Danh sách"],
            "106" => ["UserTitleController@show", "8.4.Chức danh - Chi tiết"],
            "107" => ["UserTitleController@store", "8.4.Chức danh - Thêm mới"],
            "108" => ["UserTitleController@update", "8.4.Chức danh - Chỉnh sửa"],
            "109" => ["UserTitleController@destroy", "8.4.Chức danh - Xóa"],

            // QUYỀN ROLE - QUẢN LÝ VAI TRÒ
            "110" => ["RolesController@index", "8.5.Vai trò - danh sách"],
            "111" => ["RolesController@show", "8.5.Vai trò - chi tiết"],
            "112" => ["RolesController@store", "8.5.Vai trò - thêm mới"],
            "113" => ["RolesController@update", "8.5.Vai trò - chỉnh sửa"],
            "114" => ["RolesController@destroy", "8.5.Vai trò - xóa"],

            // QUYỀN SETTING - CẤU HÌNH HỆ THỐNG
            "115" => ["SettingController", "9.1.Cấu hình - Công ty"],

            //BỔ SUNG QUYỀN STATUS
            "116" => ["StatusController@index", "9.2.Trạng thái - Danh sách"],
            "117" => ["StatusController@show", "9.2.Trạng thái - Chi tiết"],
            "118" => ["StatusController@store", "9.2.Trạng thái - Thêm mới"],
            "119" => ["StatusController@update", "9.2.Trạng thái - Chỉnh sửa"],
            "120" => ["StatusController@destroy", "9.2.Trạng thái - Xóa"],

            //BỔ SUNG QUYỀN PAYMENTS
            "121" => ["PaymentsController@index", "9.3.Phương thức thanh toán - Danh sách"],
            "122" => ["PaymentsController@show", "9.3.Phương thức thanh toán - Chi tiết"],
            "123" => ["PaymentsController@store", "9.3.Phương thức thanh toán - Thêm mới"],
            "124" => ["PaymentsController@update", "9.3.Phương thức thanh toán - Chỉnh sửa"],
            "125" => ["PaymentsController@destroy", "9.3.Phương thức thanh toán - Xóa"],

            //BỔ SUNG QUYỀN BOOKING SOURCE
            "126" => ["BookingSourceController@index", "9.4.Nguồn booking - Danh sách"],
            "127" => ["BookingSourceController@show", "9.4.Nguồn booking - Chi tiết"],
            "128" => ["BookingSourceController@store", "9.4.Nguồn booking - Thêm mới"],
            "129" => ["BookingSourceController@update", "9.4.Nguồn booking - Chỉnh sửa"],
            "130" => ["BookingSourceController@destroy", "9.4.Nguồn booking - Xóa"],

            //BỔ SUNG MODULE QUỐC GIA
            "131" => ["CountryController@index", "9.5.Quốc gia - Danh sách"],
            "132" => ["CountryController@show", "9.5.Quốc gia - Chi tiết"],
            "133" => ["CountryController@store", "9.5.Quốc gia - Thêm mới"],
            "134" => ["CountryController@update", "9.5.Quốc gia - Chỉnh sửa"],
            "135" => ["CountryController@destroy", "9.5.Quốc gia - Xóa"],

            //BỔ SUNG MODULE TỈNH, THÀNH PHỐ
            "136" => ["CityController@index", "9.6.Tỉnh, thành phố - Danh sách"],
            "137" => ["CityController@show", "9.6.Tỉnh, thành phố - Chi tiết"],
            "138" => ["CityController@store", "9.6.Tỉnh, thành phố - Thêm mới"],
            "139" => ["CityController@update", "9.6.Tỉnh, thành phố - Chỉnh sửa"],
            "140" => ["CityController@destroy", "9.6.Tỉnh, thành phố - Xóa"],

            //BỔ SUNG MODULE QUẬN, HUYỆN, XÃ
            "141" => ["DistrictController@index", "9.7.Quận, huyện, thị xã - Danh sách"],
            "142" => ["DistrictController@show", "9.7.Quận, huyện, thị xã - Chi tiết"],
            "143" => ["DistrictController@store", "9.7.Quận, huyện, thị xã - Thêm mới"],
            "144" => ["DistrictController@update", "9.7.Quận, huyện, thị xã - Chỉnh sửa"],
            "145" => ["DistrictController@destroy", "9.7.Quận, huyện, thị xã - Xóa"],

            "146" => ["UsersController@postProfile", "Chỉnh sửa thông tin cá nhân"],

//            "23" => ["ToursController@getGuides", "Tour - Danh sách hướng dẫn viên"],
//            "24" => ["ToursController@postGuides", "Tour - Thêm/sửa hướng dẫn viên"],
//            "25" => ["ToursController@deleteGuide", "Tour - Xóa hướng dẫn viên"],

//            "26" => ["CitiesController@destroy", "Địa điểm - Xóa"],
//            "27" => ["CitiesController@index", "Địa điểm - Danh sách"],
//            "28" => ["CitiesController@show", "Địa điểm - Chi tiết"],
//            "29" => ["CitiesController@store", "Địa điểm - Thêm mới"],
//            "30" => ["CitiesController@update", "Địa điểm - Chỉnh sửa"],

//            "44" => ["ModulesController@index", "Quản lý Module - Danh sách modules"],
//            "45" => ["ModulesController@active", "Quản lý Module - Kích hoạt modules"],

//            "46" => ["BranchController@postProfile", "Chỉnh sửa thông tin chi nhánh"],

            //BỔ SUNG THÊM QUYỀN NẾU CÓ, chú ý key là ID của permission
        ];

        //ADD PERMISSIONS - Thêm các quyền
        DB::table( 'permissions' )->delete();//empty permission
        $addPermissions = [];
        foreach ($arPermissions as $name => $label){
            $addPermissions[] = [
                'id' => $name,
                'name' => $label[0],
                'label' => $label[1]
            ];
        }
        DB::table('permissions')->insert($addPermissions);

        //ADD ROLE - Them vai tro
        $datenow = date( 'Y-m-d H:i:s' );
        $role = [
            [ 'id' => 1, 'name' => 'company_admin', 'label' => 'Admin', 'created_at' => $datenow, 'updated_at' => $datenow ],
//            [ 'id' => 2, 'name' => 'company_employee', 'label' => 'Nhân viên công ty', 'created_at' => $datenow, 'updated_at' => $datenow ],
//            [ 'id' => 3, 'name' => 'company_booking', 'label' => 'Nhân viên booking - Công ty', 'created_at' => $datenow, 'updated_at' => $datenow ],
//            [ 'id' => 4, 'name' => 'agent_admin', 'label' => 'Admin đại lý', 'created_at' => $datenow, 'updated_at' => $datenow ],
//            [ 'id' => 5, 'name' => 'agent_employee', 'label' => 'Nhân viên đại lý', 'created_at' => $datenow, 'updated_at' => $datenow ],
//            [ 'id' => 6, 'name' => 'branch_admin', 'label' => 'Admin chi nhánh', 'created_at' => $datenow, 'updated_at' => $datenow ],
//            [ 'id' => 7, 'name' => 'branch_employee', 'label' => 'Nhân viên chi nhánh', 'created_at' => $datenow, 'updated_at' => $datenow ],
        ];
        $addRoles = [];
        foreach ($role as $key => $label){
            if(\App\Role::where('id',$label['id'])->count() == 0 )
                $addRoles[] = [
                    'id' => $label['id'],
                    'name' => $label['name'],
                    'label' => $label['label'],
                    'created_at' => $datenow,
                    'updated_at' => $datenow
                ];
        }
        //KIỂM TRA VÀ THÊM CÁC VAI TRÒ TRUYỀN VÀO NẾU CÓ
        if (count($addRoles) > 0){
            DB::table('roles')->insert($addRoles);
        }

        //BỔ SUNG ID QUYỀN NẾU CÓ
        //Full quyền của Admin công ty
        $persAdmin = \App\Permission::pluck('id');

        //Quyền của nhân viên công ty
        /*$persCompanyEmployee = [
            5,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57
        ];*/
        //Quyền của nhân viên booking công ty
        /*$persCompanyBooking = [
            5,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57
        ];*/
//        //Quyền của admin đại lý
//        $persAgentAdmin = [
//            5,20,21,24,31,32,36,37,41,42,46,47,51,52,56,57,61,61,65,68,72,73,76,77,78,79,80,82,83,86,87,88
//        ];
//        //Quyền của nhân viên đại lý
//        $persAgentEmployee = [
//            5,20,21,31,32,36,37,41,42,46,47,51,52,56,57,61,61,65,68,72,73,76,77,78,79,80,82,83,86,87,88
//        ];
//        //Quyền của admin chi nhánh
//        $persBranchAdmin = [
//            5,26,27,31,32,36,37,41,42,46,47,51,52,56,57,61,61,65,68,72,73,76,77,78,79,80,82,83,86,87,88,91
//        ];
//        //Quyền của nhân viên chi nhánh
//        $persBranchEmployee = [
//            5,26,27,31,32,36,37,41,42,46,47,51,52,56,57,61,61,65,68,72,73,76,77,78,79,80,82,83,86,87,88
//        ];

        //Gán quyền vào Vai trò Admin
        $rolePerAdminCompany = \App\Role::findOrFail(1);
        $rolePerAdminCompany->permissions()->sync($persAdmin);

        //Gán quyền vào Vai trò Employee Company
        /*$rolePerCompanyEmployee = \App\Role::findOrFail(2);
        $rolePerCompanyEmployee->permissions()->sync($persCompanyEmployee);*/

        //Gán quyền vào Vai trò Booking Company
        /*$rolePerCompanyBooking = \App\Role::findOrFail(3);
        $rolePerCompanyBooking->permissions()->sync($persCompanyBooking);*/

//        //Gán quyền vào Vai trò Admin Agent
//        $rolePerAgentAdmin = \App\Role::findOrFail(4);
//        $rolePerAgentAdmin->permissions()->sync($persAgentAdmin);
//
//        //Gán quyền vào Vai trò Employee Agent
//        $rolePerAgentEmployee = \App\Role::findOrFail(5);
//        $rolePerAgentEmployee->permissions()->sync($persAgentEmployee);
//
//        //Gán quyền vào Vai trò Admin Branch
//        $rolePerBranchAdmin = \App\Role::findOrFail(6);
//        $rolePerBranchAdmin->permissions()->sync($persBranchAdmin);
//
//        //Gán quyền vào Vai trò Employee Branch
//        $rolePerBranchEmployee = \App\Role::findOrFail(7);
//        $rolePerBranchEmployee->permissions()->sync($persBranchEmployee);

        //Set tài khoản ID=1 là Admin
        $roleAdmin = \App\User::findOrFail(1);
        $roleAdmin->roles()->sync([1]);
    }
}
