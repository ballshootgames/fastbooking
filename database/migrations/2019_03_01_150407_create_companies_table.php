<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Kalnoy\Nestedset\NestedSet;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('trading_name')->nullable(); //Tên viết tắt, tên giao dịch
            $table->string('logo')->nullable();
            $table->date('birthday')->nullable();
            $table->string('address')->nullable();
            $table->string('representative')->nullable(); //Người đại diện
            $table->integer('title_id')->unsigned()->nullable();; //Chức danh
            $table->foreign('title_id')->references('id')->on('user_titles')->onDelete('set null');

            $table->string('phone_number')->nullable(); //Số điện thoại
            $table->string('phone_mobile')->nullable(); //Số di động
            $table->string('fax')->nullable(); //Số di động
            $table->string('email')->nullable(); //Số di động
            $table->string('website')->nullable(); //Số di động
            $table->nestedSet();
            $table->timestamps();
        });

        Schema::table('users', function (Blueprint $table){
            $table->integer('company_id')->unsigned()->nullable();
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table){
            $table->dropForeign(['company_id']);
            $table->dropColumn(['company_id']);
        });

        Schema::dropIfExists('companies');
    }
}
