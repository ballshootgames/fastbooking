<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCityCountryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('iso3');
            $table->string('iso2');
            $table->string('phonecode');
            $table->string('capital');
            $table->string('currency');
            $table->tinyInteger('arrange')->nullable()->unsigned();
            $table->tinyInteger('active')->default(0);
            $table->softDeletes();
            $table->timestamps();

            $table->integer('creator_id')->unsigned()->nullable();
            $table->foreign('creator_id')->references('id')->on('users')->onDelete('set null');
            $table->integer('updator_id')->unsigned()->nullable();
            $table->foreign('updator_id')->references('id')->on('users')->onDelete('set null');
        });

        Schema::create('cities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('country_id')->unsigned();
            $table->tinyInteger('arrange')->nullable()->unsigned();
            $table->tinyInteger('active')->default(0);
            $table->softDeletes();
            $table->timestamps();

            $table->integer('creator_id')->unsigned()->nullable();
            $table->foreign('creator_id')->references('id')->on('users')->onDelete('set null');
            $table->integer('updator_id')->unsigned()->nullable();
            $table->foreign('updator_id')->references('id')->on('users')->onDelete('set null');
        });

        Schema::create('districts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('type');
            $table->integer('city_id')->unsigned();
            $table->tinyInteger('arrange')->nullable()->unsigned();
            $table->tinyInteger('active')->default(0);
            $table->softDeletes();
            $table->timestamps();

            $table->integer('creator_id')->unsigned()->nullable();
            $table->foreign('creator_id')->references('id')->on('users')->onDelete('set null');
            $table->integer('updator_id')->unsigned()->nullable();
            $table->foreign('updator_id')->references('id')->on('users')->onDelete('set null');
        });

        Schema::create('wards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('type');
            $table->integer('district_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wards');
        Schema::dropIfExists('districts');
        Schema::dropIfExists('cities');
        Schema::dropIfExists('countries');
    }
}
