<html>
<head lang="{{ app()->getLocale() }}">
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Build website</title>
    <link href="{{ url(mix('/css/all-landing.css')) }}" rel="stylesheet">
    <style>
        .theme-active{
            float: right;
            font-size: 12px;
            background-color: #7fc142;
            padding: 4px;
            border-radius: 5px;
            color: #ffffff;
            margin-left: 12px;
            margin-top: 24px;
        }
        .loader {
            margin: 20px auto 0;
            border: 16px solid #e8e8e8;
            border-radius: 50%;
            border-top: 16px solid #008bff;
            border-bottom: 16px solid #008bff;
            width: 120px;
            height: 120px;
            -webkit-animation: spin 2s linear infinite;
            animation: spin 2s linear infinite;
        }
        @-webkit-keyframes spin {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
        }
        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
        .theme-item {
            border: 1px solid #e1e1e1;
            border-radius: 5px;
            overflow: hidden;
            max-height: 580px;
        }
        .theme-item .theme-image {
            position: relative;
            height: 460px;
            overflow: hidden;
        }
        .theme-item .theme-image img {
            width: 100%;
            position: relative;
        }
        .theme-item .theme-image .theme-action {
            overflow: hidden;
            position: absolute;
            top: 0;
            width: 100%;
            height: 0;
            text-align: center;
            background: rgba(0,0,0,.5);
            -webkit-transition: all .3s ease .15s;
            -moz-transition: all .3s ease .15s;
            -o-transition: all .3s ease .15s;
            -ms-transition: all .3s ease .15s;
            transition: all .3s ease .15s;
        }
        .theme-item .theme-image .theme-action .button {
            position: relative;
            top: 50%;
            transform: translateY(-50%);
        }
        .theme-item .theme-image .theme-action a.view-demo {
            color: #fff;
            background: #7fc142;
            margin-bottom: 15px;
        }
        .theme-item .theme-image .theme-action a {
            display: block;
            width: 130px;
            height: 45px;
            line-height: 45px;
            font-size: 16px;
            border-radius: 5px;
            margin: 0 auto;
        }
        .theme-item .theme-image .theme-action a.view-detail {
            color: #fff;
            border: 1px solid #fff;
            background: transparent;
        }
        .theme-item .theme-image:hover .theme-action {
            height: 100%;
        }
        .theme-item .theme-info {
            padding: 5px 25px 15px;
            border-top: 1px solid #e1e1e1;
            background: #fff;
        }
    </style>
</head>
<body style="padding-top: 20px">
<h2 style="text-align: center; margin-bottom: 40px; font-size: 40px;">Vui lòng chọn giao diện</h2>
<div class="container" style="margin-bottom: 50px">
    <div class="row">
        @foreach(\Modules\Theme\Entities\Theme::all() as $item)
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                <div class="theme-item responsive">
                    <div class="theme-image">
                        <img src="{{ asset('images/theme-image-'.$item->name) }}.png" alt="{{ $item->name }}">
                        <div class="theme-action">
                            <div class="button">
                                <a href="{{ url('admin/set-theme/'.$item->id) }}" class="view-demo action-preview-theme" data-url="https://template-miso.bizwebvietnam.net">Kích hoạt</a>
                                {{--<a href="" class="view-detail" target="_blank">Xem thử</a>--}}
                            </div>
                        </div>
                    </div>
                    <div class="theme-info">
                        <h3 style="display: inline-block;">{{ $item->name }}</h3>
                        @if($item->name === \App\Setting::getSettingThemeName())
                            <span class="theme-active">Đã kích hoạt</span>
                        @endif
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
<script src="{{ url (mix('/js/app-landing.js')) }}"></script>
{{--<script>--}}
    {{--function initData(step){--}}
        {{--axios.post('{{ url('/build-web') }}', {--}}
            {{--password: '{{ $password }}',--}}
            {{--trialID: '{{ $trialID }}',--}}
            {{--step: step--}}
        {{--}).then(function (respon) {--}}
            {{--$('#loader_text').text(respon.data.message);--}}
            {{--if(respon.data.step){--}}
                {{--initData(respon.data.step);--}}
            {{--}else {--}}
                {{--window.location = respon.data.subdomain;--}}
            {{--}--}}
        {{--}).catch(function (error) {--}}
            {{--alert('Có lỗi xảy ra, vui lòng thử lại!');--}}
            {{--console.log(error);--}}
        {{--});--}}
    {{--}--}}
    {{--$(function () {--}}
        {{--initData(1);--}}
    {{--})--}}
{{--</script>--}}
</body>
</html>