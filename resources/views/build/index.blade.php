<html>
<head lang="{{ app()->getLocale() }}">
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Build website</title>
    <link href="{{ url(mix('/css/all-landing.css')) }}" rel="stylesheet">
    <style>
        .loader {
            margin: 20px auto 0;
            border: 16px solid #e8e8e8;
            border-radius: 50%;
            border-top: 16px solid #008bff;
            border-bottom: 16px solid #008bff;
            width: 120px;
            height: 120px;
            -webkit-animation: spin 2s linear infinite;
            animation: spin 2s linear infinite;
        }

        @-webkit-keyframes spin {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
    </style>
</head>
<body>
    <h2 style="margin: 10% auto 0;text-align: center;">Chào mừng bạn đến với FastBooking</h2>
    <div class="loader"></div>
    <div id="loader_text" style="text-align: center;margin-top: 10px;font-style: italic;">Khởi tạo hệ thống</div>
    <div style="text-align: center;margin-top: 10px;font-size: 18px">
        Hệ thống đang khởi tạo dữ liệu, vui lòng chờ trong giây lát...
    </div>
    <script src="{{ url (mix('/js/app-landing.js')) }}"></script>
    <script>
        function initData(step){
            axios.post('{{ url('/build-web') }}', {
                password: '{{ $password }}',
                trialID: '{{ $trialID }}',
                step: step
            }).then(function (respon) {
                $('#loader_text').text(respon.data.message);
                if(respon.data.step){
                    initData(respon.data.step);
                }else {
                    window.location = respon.data.subdomain;
                }
            }).catch(function (error) {
                alert('Có lỗi xảy ra, vui lòng thử lại!');
                console.log(error);
            });
        }
        $(function () {
            initData(1);
        })
    </script>
</body>
</html>