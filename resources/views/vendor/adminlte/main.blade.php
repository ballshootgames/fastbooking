@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ __('message.dashboard') }}
@endsection
@section('contentheader_title')
    {{ __('Danh sách đăng kí dùng thử') }}
@endsection
@section('breadcrumb')

@endsection

@section('css')
    <style>
        .table-main .table tr td:nth-child(1n):not(:nth-child(1)):not(:nth-child(2)){
            width: 15%;
        }
    </style>
@endsection

@section('main-content')
	<div class="table-home table-main table-responsive">
		<table class="table table-borderless">
			<tbody>
                <tr>
                    <td class="text-center"><span>{{ __('trials.company_name') }}</span></td>
                    <td class="text-center"><span>{{ __('trials.name') }}</span></td>
                    <td class="text-center"><span>{{ __('trials.db_name') }}</span></td>
                    <td class="text-center"><span>{{ __('trials.sub_domain') }}</span></td>
                    <td class="text-center"><span>{{ __('trials.register_date') }}</span></td>
                </tr>
                @foreach($trials as $item)
                    <tr>
                        <td class="text-center">{{ $item->company_name }}</td>
                        <td class="text-center text-uppercase">{{ $item->name }}</td>
                        <td class="text-center">{{ $item->db_name }}</td>
                        <td class="text-center"><a href="{{ $item->siteUrl }}" target="_blank">{{ $item->subdomain }}</a></td>
                        <td class="text-center">{{ Carbon\Carbon::parse($item->updated_at)->format(config('settings.format.date')) }}</td>
                    </tr>
                @endforeach
			</tbody>
		</table>
	</div>
@endsection
@section('scripts-footer')

@endsection