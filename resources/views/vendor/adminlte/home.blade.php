@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ __('message.dashboard') }}
@endsection
@section('contentheader_title')
	{{--{{ __('message.dashboard') }}
	<small>{{ __('message.control_panel') }}</small>--}}
@endsection
@section('breadcrumb')
	{{--<ol class="breadcrumb">
		<li><i class="fa fa-home"></i> {{ __('message.dashboard') }}</li>
	</ol>--}}
@endsection

@section('css')
	<link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}">
	<style>
		#booking-finance{
			background-color: #fff;
			border: 1px solid #ccc;
			padding: 15px;
			margin-bottom: 15px;
		}
		.btn-warning-bold{
			background-color: #fab248;
			color: #fff;
			border-color: #fab248;
			-webkit-border-radius: 20px;
			-moz-border-radius: 20px;
			border-radius: 20px;
		}
		.content-header{
			display: none;
		}
	</style>
@endsection

@section('main-content')

	<div class="row">
		{{--@isset($agentsCount)
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-aqua">
				<div class="inner">
					<h3>{{ $agentsCount }}</h3>

					<p>{{ __('agents.agent') }}</p>
				</div>
				<div class="icon">
					<i class="fa fa-building-o"></i>
				</div>
				<a href="{{ url('agents') }}" class="small-box-footer">{{ __('message.more_info') }} <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>
		@endisset--}}

		{{--@isset($branchesCount)
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-aqua">
					<div class="inner">
						<h3>{{ $branchesCount }}</h3>

						<p>{{ __('branches.branch') }}</p>
					</div>
					<div class="icon">
						<i class="fa fa-building-o"></i>
					</div>
					<a href="{{ url('branches') }}" class="small-box-footer">{{ __('message.more_info') }} <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
		@endisset--}}

		{{--@isset($bookingJourneysCount)
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-green">
				<div class="inner">
					<h3>{{ $bookingJourneysCount }}</h3>

					<p>{{ \Auth::user()->isEmployeeCompany() || \Auth::user()->isEmployeeAgent() ? __('bookingJourneys.me_booking') : __('bookingJourneys.booking') }}</p>
				</div>
				<div class="icon">
					<i class="fa fa-cart-plus"></i>
				</div>
				<a href="{{ url('booking-journeys') }}" class="small-box-footer">{{ __('message.more_info') }} <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>
		@endisset--}}

		{{--@isset($bookingToursCount)
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-blue">
				<div class="inner">
					<h3>{{ $bookingToursCount }}</h3>

					<p>{{ \Auth::user()->isEmployeeCompany() || \Auth::user()->isEmployeeAgent() ? __('bookingTours.me_booking') : __('bookingTours.booking') }}</p>
				</div>
				<div class="icon">
					<i class="fa fa-cart-arrow-down"></i>
				</div>
				<a href="{{ url('booking-tours') }}" class="small-box-footer">{{ __('message.more_info') }} <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>
		@endisset--}}

		{{--@isset($usersCount)
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-red">
				<div class="inner">
					<h3>{{ $usersCount }}</h3>

					<p>{{ __('message.user.users') }}</p>
				</div>
				<div class="icon">
					<i class="fa fa-user-secret"></i>
				</div>
				<a href="{{ url('admin/users') }}" class="small-box-footer">{{ __('message.more_info') }} <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>
		@endisset--}}

	</div>
	@if(session()->has('trialDays') && session('trialDays') <= 0)
		<div class="alert alert-danger" role="alert">{{
			trans('adminlte_lang::message.resgister_trial_expired')
		}}</div>
	@endif
	<div class="booking-search" style="margin-bottom: 15px; display: inline-flex; align-items: center">
		<form action="" method="post" class="form-inline" id="report-form">
			<div class="input-group">
				<span style="margin-right: 15px;">{{ __('Lọc') }}</span>
				<button type="button" class="btn btn-sm btn-warning-bold" id="daterange-btn">
					{{ __('reports.date_range') }}: <span></span>
				</button>
				{{--<input type="hidden" name="from" id="from" value="{{ Request::get('from') }}"/>
				<input type="hidden" name="to" id="to" value="{{ Request::get('to') }}"/>--}}
			</div>
		</form>
	</div>
	{{ \App\ChartJs::init() }}
	<div id="booking-finance">
        <div class="fa-loading text-center" style="display: none;">
            <span class="fa fa-spinner fa-spin fa-3x text-primary" aria-hidden="true" ></span>
        </div>
    </div>
	<div class="table-home table-responsive">
		<table class="table table-borderless">
			<tbody>
			<tr>
				<td class="text-center"><span>{{ __('Mã sản phẩm') }}</span></td>
				<td ><span>{{ __('Tên sản phẩm') }}</span></td>
				<td class="text-center"><span>{{ __('Tổng số đặt') }}</span></td>
				<td class="text-center"><span>{{ __('Thời gian') }}</span></td>
				<td class="text-center"><span>{{ __('Ngày đi') }}</span></td>
				<td class="text-center"><span>{{ __('Trạng thái') }}</span></td>
			</tr>
			@php($status = config('tour.tour_status'))
			@foreach($tours as $item)
				<tr>
					<td class="text-center">{{ $item->code }}</td>
					<td class="text-uppercase">{{ $item->name }}</td>
					<td class="text-center">{{ count($item->bookings) }}</td>
					@if($item->night_number !== 0)
						<td class="text-center">{{ $item->day_number }} ngày - {{ $item->night_number }} đêm</td>
					@elseif($item->day_number === 0)
						<td class="text-center">1/2 ngày</td>
					@elseif($item->night_number !== 0 && $item->day_number === 0)
						<td class="text-center">1/2 ngày - {{ $item->night_number }} đêm</td>
					@else
						<td class="text-center">{{ $item->day_number }} ngày</td>
					@endif
                    <td class="text-center">{{ !empty($item->departure_date) ? Carbon\Carbon::parse($item->departure_date)->format(config('settings.format.date')) : 'hàng ngày' }}</td>
                    @if(!empty($item->tour_status))
						<td class="text-center">{{ $status[$item->tour_status] }}</td>
					@else
						<td class="text-center">{{ $status[0] }}</td>
					@endif
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>

@endsection
@section('scripts-footer')
	<script type="text/javascript" src="{{ asset('plugins/moment.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
	<script type="text/javascript">
        $.ajax({
            url: '{{ url('bookings/ajax/tour/get-finance-booking') }}',
            type: "get",
            dataType: "text",
            beforeSend: function(){
                $('.fa-loading').show();
            },
            success: function (data) {
                $('#booking-finance').html(data);
                $('.fa-loading').hide();
            }
        })
	</script>
	<script type="text/javascript">
        $(function(){
            let start = moment().subtract(29, 'days');
			{{--@if(!empty(Request::get('from')))
                start = moment('{{ Request::get('from') }}');
			@endif--}}
            let end =  moment();
			{{--@if(!empty(Request::get('to')))
                end = moment('{{ Request::get('to') }}');
			@endif--}}

			$('#daterange-btn span').html(start.format('{{ strtoupper(config('settings.format.date_js')) }}') + ' - ' + end.format('{{ strtoupper(config('settings.format.date_js')) }}'));
            $('#daterange-btn').daterangepicker(
                {
                    startDate: start,
                    endDate  : end,
                    ranges   : {
//                        'Hôm nay'   : [moment(), moment()],
//                        'Hôm qua'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        '{{ __('reports.last_7_days') }}' : [moment().subtract(6, 'days'), moment()],
                        '{{ __('reports.last_30_days') }}': [moment().subtract(29, 'days'), moment()],
                        '{{ __('reports.this_month') }}'  : [moment().startOf('month'), moment().endOf('month')],
                        '{{ __('reports.last_month') }}'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },

                    "locale": {
                        "format": "{{ strtoupper(config('settings.format.date_js')) }}",
                        "separator": " - ",
                        "applyLabel": "{{ __('reports.apply') }}",
                        "cancelLabel": "{{ __('reports.close') }}",
                        "fromLabel": "{{ __('reports.from') }}",
                        "toLabel": "{{ __('reports.to') }}",
                        "customRangeLabel": "{{ __('reports.custom') }}",
                        "daysOfWeek": [
                            "{{ __('reports.Su') }}",
                            "{{ __('reports.Mo') }}",
                            "{{ __('reports.Tu') }}",
                            "{{ __('reports.We') }}",
                            "{{ __('reports.Th') }}",
                            "{{ __('reports.Fr') }}",
                            "{{ __('reports.Sa') }}"
                        ],
                        "monthNames": [
                            "{{ __('reports.january') }}",
                            "{{ __('reports.february') }}",
                            "{{ __('reports.march') }}",
                            "{{ __('reports.april') }}",
                            "{{ __('reports.may') }}",
                            "{{ __('reports.june') }}",
                            "{{ __('reports.july') }}",
                            "{{ __('reports.august') }}",
                            "{{ __('reports.september') }}",
                            "{{ __('reports.october') }}",
                            "{{ __('reports.november') }}",
                            "{{ __('reports.december') }}"
                        ],
                        "firstDay": 1
                    },
                    opens: 'right'
                }
			);
            $('#daterange-btn').on('apply.daterangepicker', function(ev, picker) {
                $('#daterange-btn span').html(picker.startDate.format('{{ strtoupper(config('settings.format.date_js')) }}') + ' - ' + picker.endDate.format('{{ strtoupper(config('settings.format.date_js')) }}'));
                let from = picker.startDate.format('YYYY-MM-DD');
                let to = picker.endDate.format('YYYY-MM-DD');
                $.ajax({
                    url: '{{ url('bookings/ajax/tour/get-filter') }}',
                    type: "get",
                    data: {
                        from: from,
                        to: to,
                    },
                    dataType: "text",
                    success: function (data) {
                        $('#booking-finance').html(data);
                    }
                })
            });
        });
	</script>
@endsection