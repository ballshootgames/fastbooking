@extends('adminlte::layouts.auth')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.register') }}
@endsection

@section('content')

    <body class="hold-transition">
        <div id="app" class="register-page row">
            <div class="hidden-xs hidden-sm visible-md visible-lg col-md-3 col-lg-3 login-bg"></div>
            <div class="col-md-9 col-lg-9 pl-0">
            <div class="register-box-company">
                <div class="register-logo">
                    <a href="{{ url('/') }}">
                        <img src="{{ asset('img/logo.png') }}" alt="{!! Config("settings.app_title") !!}">
                    </a>
                </div>

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <p><i class="fa fa-fw fa-check"></i> {{ $error }}</p>
                        @endforeach
                    </div>
                @endif

                <div class="register-box-body">
                    <h4 class="register-box-msg">{{ trans('adminlte_lang::message.registermember') }}</h4>
                    <form action="{{ url('/register') }}" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="col-sm-6 col-md-6">
                                <h4>{{ __('message.user.account_info') }}</h4>
                                {{--<h5>THÔNG TIN CÁ NHÂN</h5>--}}
                                <div class="form-group has-feedback">
                                    <input type="text" class="form-control" placeholder="{{ trans('adminlte_lang::message.fullname') }}*" required name="name" value="{{ old('name') }}" autofocus/>
                                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                </div>
                                @if (config('auth.providers.users.field','email') === 'username')
                                    <div class="form-group has-feedback">
                                        <input type="text" class="form-control" placeholder="{{ trans('adminlte_lang::message.username') }}*" required name="username" value="{{ old('username') }}"/>
                                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                    </div>
                                @endif

                                <div class="form-group has-feedback">
                                    <input type="email" class="form-control" placeholder="{{ trans('adminlte_lang::message.email') }}*" required name="email" value="{{ old('email') }}"/>
                                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                </div>
                                <div class="form-group has-feedback">
                                    <input type="password" class="form-control" placeholder="{{ trans('adminlte_lang::message.password') }}*" required name="password"/>
                                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                </div>
                                <div class="form-group has-feedback">
                                    <input type="password" class="form-control" placeholder="{{ trans('adminlte_lang::message.retypepassword') }}*" required name="password_confirmation"/>
                                    <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <h4>{{ __('message.user.company_info') }}</h4>
                                <div class="form-group has-feedback">
                                    <input type="text" class="form-control" placeholder="{{ __("message.user.company_name") }}*" required name="company[name]" value="{{ old('company.name') }}"/>
                                    <span class="fa fa-building form-control-feedback"></span>
                                </div>
                                <div class="form-group has-feedback">
                                    <input type="email" class="form-control" placeholder="{{ __("message.user.company_email") }}" name="company[email]" value="{{ old('company.email') }}"/>
                                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                </div>
                                <div class="form-group has-feedback">
                                    <input type="text" class="form-control" placeholder="{{ __("message.user.company_address") }}" name="company[address]" value="{{ old('company.address') }}"/>
                                    <span class="fa fa-location-arrow form-control-feedback"></span>
                                </div>
                                <div class="form-group has-feedback">
                                    <input type="text" class="form-control" placeholder="{{ __("message.user.company_phone") }}" name="company[phone]" value="{{ old('company.phone') }}"/>
                                    <span class="fa fa-phone form-control-feedback"></span>
                                </div>
                                <div class="form-group has-feedback">
                                    <label>
                                        <div class="checkbox_register icheck">
                                            <label>
                                                <input type="checkbox" name="terms" required />
                                            </label>
                                        </div>
                                    </label>
                                    <button type="button" class="btn btn-link text-link" data-toggle="modal" data-target="#termsModal">{{ trans('adminlte_lang::message.terms') }}</button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="text-left">
                                <button type="submit" class="btn btn-primary btn-register">{{ trans('adminlte_lang::message.register') }}</button>
                                <a class="text-link" href="{{ url('/login') }}" class="text-center">{{ trans('adminlte_lang::message.membreship') }}</a>
                            </div>
                        </div>
                    </form>

                    {{--@include('adminlte::auth.partials.social_login')--}}


                </div><!-- /.form-box -->

                <div class="register-footer">
                    <p>{{ trans('adminlte_lang::message.copyright') }}&copy;2019 bản quyền</p>
                    <p>{{ trans('adminlte_lang::message.huesoftyear') }} * <span><a href="#">Chính sách</a></span> * <span><a
                                    href="#">Điều khoản</a></span></p>
                </div>
            </div><!-- /.register-box -->
        </div>
    </div>

    @include('adminlte::layouts.partials.scripts_auth')

    @include('adminlte::auth.terms')

    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>

    </body>
@endsection
