@extends('adminlte::layouts.auth')

@section('htmlheader_title')
    Log in
@endsection

@section('content')
    <body class="hold-transition login-page">
    <div id="app" class="row">
        <div class="hidden-xs hidden-sm visible-md visible-lg col-md-7 col-lg-8 login-bg"></div>
        <div class="col-md-5 col-lg-4 pl-0">
            <div class="login-box d-flex align-items-center justify-content-center">
                <div class="login-box__width">
                    <div class="login-logo">
                        <a href="{{ url('/') }}">
                            <img src="{{ asset('img/logo.png') }}" alt="{!! Config("settings.app_title") !!}">
                        </a>
                    </div><!-- /.login-logo -->
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <p><i class="fa fa-fw fa-check"></i> {{ $error }}</p>
                            @endforeach
                        </div>
                    @endif

                    <div class="login-box-body">
                        {{--<p class="login-box-msg"> {{ trans('adminlte_lang::message.siginsession') }} </p>--}}
                        <form action="{{ url('/login') }}" method="post" class="frmLogin">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            {{--<login-input-field--}}
                            {{--name="{{ config('auth.providers.users.field','email') }}"--}}
                            {{--domain="{{ config('auth.defaults.domain','') }}"--}}
                            {{--></login-input-field>--}}
                            <div class="form-group has-feedback">
                                @php
                                    $type = config('auth.providers.users.field','email') == "email"? "email": "text";
                                @endphp
                                <input type="{{ $type }}" class="form-control" placeholder="{{ trans('message.user.username_email') }}" name="{{ config('auth.providers.users.field','email') }}"/>
                                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                            </div>
                            <div class="form-group has-feedback">
                                <input type="password" class="form-control" placeholder="{{ trans('adminlte_lang::message.password') }}" name="password"/>
                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                            </div>
                            <div class="row">
                                <div class="col-xs-7">
                                    <div class="checkbox icheck">
                                        <label class="checkbox_label">
                                            <input style="display:none;" type="checkbox" name="remember"> {{ trans('adminlte_lang::message.remember') }}
                                        </label>
                                    </div>
                                </div><!-- /.col -->
                                <div class="col-xs-5">
                                    <button type="submit" class="btn btn-primary btn-block btn-login">{{ trans('adminlte_lang::message.buttonsign') }}</button>
                                </div><!-- /.col -->
                            </div>
                        </form>

                        {{--@include('adminlte::auth.partials.social_login')--}}

                        <a class="text-link" href="{{ url('/password/reset') }}">{{ trans('adminlte_lang::message.forgotpassword') }}</a><br>
                        <a href="{{ url('/register') }}" class="text-link text-center">{{ trans('adminlte_lang::message.registermember') }}</a>

                    </div><!-- /.login-box-body -->
                    <div class="login-footer">
                        <p>{{ trans('adminlte_lang::message.copyright') }}&copy;2019 bản quyền</p>
                        <p>{{ trans('adminlte_lang::message.huesoftyear') }} * <span><a href="#">Chính sách</a></span> * <span><a
                                        href="#">Điều khoản</a></span></p>
                    </div>
                </div>
            </div><!-- /.login-box -->
        </div>
    </div>
    @include('adminlte::layouts.partials.scripts_auth')

    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
    </body>

@endsection