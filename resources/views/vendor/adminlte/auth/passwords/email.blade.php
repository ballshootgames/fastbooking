@extends('adminlte::layouts.auth')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.passwordreset') }}
@endsection

@section('content')

    <body class="login-page">
    <div id="app" class="row">
        <div class="hidden-xs hidden-sm visible-md visible-lg col-md-7 col-lg-8 login-bg"></div>
        <div class="col-md-5 col-lg-4 pl-0">
            <div class="login-box d-flex align-items-center justify-content-center">
                <div class="login-box_width">
                    <div class="login-logo">
                        <a href="{{ url('/home') }}">
                            <img src="{{ asset('img/logo.png') }}" alt="{!! Config("settings.app_title") !!}">
                        </a>
                    </div><!-- /.login-logo -->

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <p><i class="fa fa-fw fa-check"></i> {{ $error }}</p>
                            @endforeach
                        </div>
                    @endif

                    <div class="login-box-body">
                        <p class="reset-box-msg">{{ trans('adminlte_lang::message.passwordreset') }}</p>
                        <form action="{{ url('/password/email') }}" method="post" class="frmLogin">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group has-feedback">
                                <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" autofocus/>
                                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                            </div>

                            <div class="row">
                                <div class="col-xs-12">
                                    <button type="submit" class="btn btn-primary btn-block btn-reset">{{ trans('adminlte_lang::message.sendpassword') }}</button>
                                </div><!-- /.col -->
                            </div>
                        </form>

                        <a class="text-link" href="{{ url('/login') }}">{{ trans('adminlte_lang::message.login') }}</a><br>
                        <a href="{{ url('/register') }}" class="text-center text-link">{{ trans('adminlte_lang::message.registermember') }}</a>

                    </div><!-- /.login-box-body -->

                    <div class="login-footer">
                        <p>{{ trans('adminlte_lang::message.copyright') }}&copy;2019 bản quyền</p>
                        <p>{{ trans('adminlte_lang::message.huesoftyear') }} * <span><a href="#">Chính sách</a></span> * <span><a
                                        href="#">Điều khoản</a></span></p>
                    </div>
                </div>
            </div><!-- /.login-box -->
        </div>
    </div>

    @include('adminlte::layouts.partials.scripts_auth')

    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
    </body>

@endsection