<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    {{--<a href="{{ url('/home') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">
            --}}{{--{!! Config("settings.app_logo_mini") !!}--}}{{--
        </span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">{!! Config("settings.app_logo") !!} fsfds</span>
    </a>--}}
    <a href="{{ url('/') }}" class="logo">
        <span class="logo-lg">
            <img src="{{ asset('img/logo_small.png') }}" alt="{!! Config("settings.app_title") !!}">
        </span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">{{ trans('adminlte_lang::message.togglenav') }}</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                @if (Auth::guest())
                    <li><a href="{{ url('/register') }}">{{ trans('adminlte_lang::message.register') }}</a></li>
                    <li><a href="{{ url('/login') }}">{{ trans('adminlte_lang::message.login') }}</a></li>
                @else
                    @if(!empty(\URL::getDefaultParameters()))
                        <li>
                            <h4 style="color: white">
                                <span class="label label-warning">{{ trans('adminlte_lang::message.number_trial_days') }}
                                    {{                                    
                                        session('trialDays') != null ? session('trialDays') : '0'
                                    }}
                                </span>
                            </h4>  
                        </li>   
                    @endif                    
                    <!-- Notifications Menu -->
                    <li class="dropdown notifications-menu">                       
                        <!-- Menu toggle button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-bell"></i>
                            <span id="notify_number" class="label label-danger">{{ auth()->user()->unreadNotifications->count() }}</span>
                        </a>
                        <ul class="dropdown-menu" style="width: 430px;">
{{--                            <li class="header">{{ trans('adminlte_lang::message.notifications') }}</li>--}}
                            <li>
                                <!-- Inner Menu: contains the notifications -->
                                <ul class="menu">
                                    <li><!-- start notification -->
                                        <a href="#">{{ __('message.loading') }}</a>
                                    </li><!-- end notification -->
                                </ul>
                            </li>
                            <li class="footer"><a href="{{ url('notifications') }}">{{ trans('notifications.view_all') }}</a></li>
                        </ul>
                    </li>
                    {{--<li class="notifications-menu">--}}
                        {{--<a href="#" class="dropdown-toggle language" data-toggle="dropdown" aria-expanded="true">--}}
                            {{--<span class="text-uppercase">{{ \App::getLocale() }}</span>--}}
                            {{--<i class="fa fa-angle-down"></i>--}}
                        {{--</a>--}}
                        {{--<ul class="dropdown dropdown-menu menu" style="width: 150px;">--}}
                            {{--<li>--}}
                                {{--<ul class="menu">--}}
                            {{--@foreach(config('app.locales') as $lg)--}}
                                {{--<li>--}}
                                    {{--<a style="padding: 6px 10px;" onclick="document.getElementById('locale_client').value = '{{ $lg }}';document.getElementById('frmLag').submit();return false;" href="#">--}}
                                        {{--<i class="fa fa-flag-o"></i>--}}
                                        {{--{{ __($lg) }}--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                            {{--@endforeach--}}
                                {{--</ul>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                        {{--{!! Form::open(['method' => 'POST', 'url' => 'change_locale', 'class' => 'form-inline navbar-select', 'id' => 'frmLag']) !!}--}}
                        {{--<input type="hidden" id="locale_client" name="locale_client" value="">--}}
                        {{--{!! Form::close() !!}--}}
                    {{--</li>--}}
                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu" id="user_menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                        {!! Auth::user()->showAvatar(["class"=>"user-image"], asset(config('settings.avatar_default'))) !!}
                        {{--<img src="{{ asset(config('settings.avatar_default')) }}" class="user-image" alt="User Image"/>--}}
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">{{ Auth::user()->name }}</span>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li class="active">
                                <div class="dw-user-box in d-flex">
                                    <div class="u-img">
                                        {!! Auth::user()->showAvatar(["class"=>""], asset(config('settings.avatar_default'))) !!}
                                    </div>
                                    <div class="u-text">
                                        <h4>{{ Auth::user()->name }}</h4>
                                        <p class="text-muted">{{ Auth::user()->email }}</p>
                                        <p class="text-muted">{{ Auth::user()->company_id ? optional(Auth::user()->company)->name : '' }}</p>
                                        @foreach(Auth::user()->roles as $role)
                                            <span class="badge label-success">{{ $role->label }}</span>
                                        @endforeach
                                    </div>
                                </div>
                            </li>
                            <!-- The user image in the menu -->

                            <!-- Menu Body -->

                            <!-- Menu Footer-->
                            <li role="separator" class="divider"></li>
                            @can('UsersController@update')
                            <li>
                                <a href="{{ url('/profile') }}"><i class="fa fa-user-o"></i> {{ trans('adminlte_lang::message.profile') }}</a>
                            </li>
                            @endcan
                            @if(!empty(URL::getDefaultParameters()['subdomain']))
                                @can('SettingController')
                                <li>
                                    <a href="{{ url('/admin/settings') }}"><i class="fa fa-cogs"></i> {{ __('message.settings') }}</a>
                                </li>
                                @endcan
                            @endif
                            <li role="separator" class="divider"></li>
                            <li>
                                <a href="{{ url('/logout') }}" id="logout">
                                    <i class="fa fa-sign-out"></i> {{ trans('adminlte_lang::message.signout') }}
                                </a>

                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                    <input type="submit" value="logout" style="display: none;">
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif

                <!-- Control Sidebar Toggle Button -->
                {{--<li>--}}
                    {{--<a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>--}}
                {{--</li>--}}
            </ul>
        </div>
    </nav>
</header>
