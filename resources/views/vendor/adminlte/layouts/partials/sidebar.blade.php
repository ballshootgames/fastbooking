<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        {{--@if (! Auth::guest())--}}
            {{--<div class="user-panel">--}}
                {{--<div class="pull-left image">--}}
                    {{--<img src="{{ asset(config('settings.avatar_default')) }}" class="img-circle" alt="User Image" />--}}
                    {{--{!! Auth::user()->showAvatar(["class"=>"img-circle"], asset(config('settings.avatar_default'))) !!}--}}
                {{--</div>--}}
                {{--<div class="pull-left info">--}}
                    {{--<p>{{ Auth::user()->name }}</p>--}}
                    {{--<!-- Status -->--}}
                    {{--<a href="#"><i class="fa fa-circle text-success"></i> {{ trans('adminlte_lang::message.online') }}</a>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--@endif--}}

    <!-- search form (Optional) -->
        <!--
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="{{ trans('adminlte_lang::message.search') }}..."/>
                <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form>
        -->
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        @php
        $arLinkAdmin = [
            [
                'icon' => 'fa fa-dashboard',
                'title' => __('Tổng quan'),
                'href' => 'home',
            ],
            [
                'icon' => 'fa fa-user-secret',
                'title' => __('Quản lý người dùng'),
                'href' => 'admin/users',
            ],
            [
                'icon' => 'fa fa-user-secret',
                'title' => __('Quản lý bản dùng thử'),
                'href' => 'trials',
                /*'permission' => 'TrialsController@index'*/
            ]
        ];
        $arLinkUser = [
            [
                'icon' => 'fa fa-dashboard',
                'title' => __('Dashboard'),
                'href' => 'home',
            ],
            [
                'icon' => 'fa fa-shopping-cart',
                'title' => __('booking::bookings.booking'),
                'child' => [
                    [
                        'icon' => 'fas fa-hiking',
                        'title' => __('Tour'),
                        'href' => 'bookings/tour',
                        'permission' => 'BookingController@index'
                    ],
                ]
            ],
            [
                'icon' => 'fa fa-users',
                'title' => __('booking::customers.customer'),
                'href' => 'bookings/customers',
                'permission' => 'CustomersController@index'
            ],
            [
                'icon' => 'fa fa-building',
                'title' => __('Sản phẩm - Dịch vụ'),
                'child' => [
                    [
                        'icon' => 'fas fa-hiking',
                        'title' => __('Tour'),
                        'href' => 'admin/control/tours',
                        'permission' => 'ToursController@index'
                    ],
                    [
                        'icon' => 'fas fa-comments',
                        'title' => __('Bình luận'),
                        'href' => 'admin/control/comments',
                        'permission' => 'CommentController@index'
                    ],
                ]
            ],
            [
                'icon' => 'fas fa-newspaper',
                'title' => __('news::news.news'),
                'href' => 'news',
                'permission' => 'NewsController@index'
            ],
            [
                'icon' => 'fas fa-podcast',
                'title' => __('theme::contacts.contact'),
                'href' => 'admin/themes/contacts',
            ],
            [
                'icon' => 'fa fa-money',
                'title' => __('installment::installments.installment'),
                'href' => 'installments',
                'permission' => 'InstallmentController@index'
            ]
        ];
        $arConfig = [
            [
                'icon' => 'fas fa-chart-bar',
                'title' => __('booking::reports.report'),
                'child' => [
                     [
                        'icon' => 'fas fa-ticket-alt',
                        'title' => __('booking::reports.ticket'),
                        'href' => 'bookings/reports/booking-number',
                        'permission' => 'ReportsController@numberBookingByDate'
                    ],
                    [
                        'icon' => 'fa fa-users',
                        'title' => __('booking::reports.customer'),
                        'href' => 'bookings/reports/booking-people',
                        'permission' => 'ReportsController@peopleBookingByDate'
                    ],
                    [
                        'icon' => 'fas fa-coins',
                        'title' => __('booking::reports.revenue'),
                        'href' => 'bookings/reports/finance',
                        'permission' => 'ReportsController@financeBookingByDate'
                    ],
                ],
            ],
            [
                'icon' => 'fas fa-cogs',
                'title' => __('settings.system'),
                'child' => [
                    [
                        'icon' => 'fas fa-user',
                        'title' => __('Tài khoản'),
                        'href' => 'admin/users',
                        'permission' => 'UsersController@index'
                    ],
                    [
                        'icon' => 'fas fa-cog',
                        'title' => __('Cấu hình công ty'),
                        'href' => 'admin/settings',
                        'permission' => 'SettingController'
                    ],
                    [
                        'icon' => 'fas fa-user-lock',
                        'title' => __('Vai trò'),
                        'href' => 'admin/roles',
                        'permission' => 'RolesController@index'
                    ],
                    [
                        'icon' => 'fas fa-id-card',
                        'title' => __('Chức danh'),
                        'href' => 'user-titles',
                        'permission' => 'UserTitleController@index'
                    ],
                    [
                        'icon' => 'fas fa-columns',
                        'title' => __('Phòng ban'),
                        'href' => 'user-departments',
                        'permission' => 'UserDepartmentController@index'
                    ],
                    [
                        'icon' => 'fas fa-network-wired',
                        'title' => __('Chi nhánh'),
                        'href' => 'admin/company',
                        'permission' => 'CompaniesController@index'
                    ],
                ]
            ],
            [
                'icon' => 'fas fa-list-ul',
                'title' => __('Danh mục'),
                'child' => [
                    [
                        'icon' => 'fas fa-caret-right',
                        'title' => __('booking::customer_types.customer_type'),
                        'href' => 'bookings/customer-types',
                        'permission' => 'CustomerTypeController@index'
                    ],
                    [
                        'icon' => 'fas fa-caret-right',
                        'title' => __('tour::tour_properties.tour_properties.tour_class'),
                        'href' => 'admin/control/tour-properties?type=tour_class',
                        'permission' => 'TourPropertyController@index',
                        'type' => 'tour_class',
                        'href_current' => 'admin/control/tour-properties'
                    ],
                    [
                        'icon' => 'fas fa-caret-right',
                        'title' => __('tour::tour_properties.tour_properties.tour_type'),
                        'href' => 'admin/control/tour-properties?type=tour_type',
                        'permission' => 'TourPropertyController@index',
                        'type' => 'tour_type',
                        'href_current' => 'admin/control/tour-properties'
                    ],
                    [
                        'icon' => 'fas fa-caret-right',
                        'title' => __('tour::tour_properties.tour_properties.tour_category'),
                        'href' => 'admin/control/tour-properties?type=tour_category',
                        'permission' => 'TourPropertyController@index',
                        'type' => 'tour_category',
                        'href_current' => 'admin/control/tour-properties'
                    ],
                    [
                        'icon' => 'fas fa-caret-right',
                        'title' => __('tour::tour_properties.tour_properties.tour_list'),
                        'href' => 'admin/control/tour-properties?type=tour_list',
                        'permission' => 'TourPropertyController@index',
                        'type' => 'tour_list',
                        'href_current' => 'admin/control/tour-properties'
                    ],
                    [
                        'icon' => 'fas fa-caret-right',
                        'title' => __('tour::tour_properties.tour_properties.tour_place'),
                        'href' => 'admin/control/tour-properties?type=tour_place',
                        'permission' => 'TourPropertyController@index',
                        'type' => 'tour_place',
                        'href_current' => 'admin/control/tour-properties'
                    ],
                    [
                        'icon' => 'fas fa-caret-right',
                        'title' => __('news::news_type.news_type'),
                        'href' => 'news-type',
                        'permission' => 'NewsTypeController@index'
                    ],
                    [
                        'icon' => 'fas fa-caret-right',
                        'title' => __('Phương thức thanh toán'),
                        'href' => 'payments',
                        'permission' => 'PaymentsController@index'
                    ],
                    [
                        'icon' => 'fas fa-caret-right',
                        'title' => __('Nguồn booking'),
                        'href' => 'bookings/booking-sources',
                        'permission' => 'BookingSourceController@index'
                    ],
                    [
                        'icon' => 'fas fa-caret-right',
                        'title' => __('Trạng thái booking'),
                        'href' => 'bookings/statuses',
                        'permission' => 'StatusController@index'
                    ],
                    [
                        'icon' => 'fas fa-caret-right',
                        'title' => __('Đơn vị công tác'),
                        'href' => 'bookings/customers/offices',
                        'permission' => 'OfficeController@index'
                    ],
                    [
                        'icon' => 'fas fa-caret-right',
                        'title' => __('Nhà cung cấp'),
                        'href' => 'admin/control/suppliers',
                        'permission' => 'SupplierController@index'
                    ],
                    [
                        'icon' => 'fas fa-caret-right',
                        'title' => __('Quốc gia'),
                        'href' => 'countries',
                        'permission' => 'CountryController@index'
                    ],
                    [
                        'icon' => 'fas fa-caret-right',
                        'title' => __('Tỉnh, thành phố'),
                        'href' => 'cities',
                        'permission' => 'CityController@index'
                    ],
                    [
                        'icon' => 'fas fa-caret-right',
                        'title' => __('Quận, huyện'),
                        'href' => 'districts',
                        'permission' => 'DistrictController@index'
                    ],
                ],
            ],
        ];
        $themeName = \App\Setting::getSettingThemeName();
        $arConfigTheme = [];
        switch ($themeName){
            case "tomap":
                $arConfigTheme[] = [
                        'icon' => 'fas fa-paint-brush',
                        'title' => __('Giao diện'),
                        'child' => [
                            [
                                'icon' => 'fas fa-caret-right',
                                'title' => __('Menu'),
                                'href' => 'admin/themes/menus',
                                'permission' => 'MenuController@index'
                            ],
                            [
                                'icon' => 'fas fa-caret-right',
                                'title' => __('Trang'),
                                'href' => 'admin/themes/pages',
                                'permission' => 'PageController@index'
                            ],
                            [
                                'icon' => 'fas fa-caret-right',
                                'title' => __('Slider'),
                                'href' => 'admin/themes/slides',
                                'permission' => 'SlideController@index'
                            ],
                            [
                                'icon' => 'fas fa-caret-right',
                                'title' => __('Đăng ký nhận tin'),
                                'href' => 'admin/themes/newsletters',
                                'permission' => 'NewsletterController@index'
                            ],
                        ]
                    ];
                break;
            default:
                $arConfigTheme[] = [
                    'icon' => 'fas fa-paint-brush',
                    'title' => __('Giao diện'),
                    'child' => [
                        [
                            'icon' => 'fas fa-caret-right',
                            'title' => __('Menu'),
                            'href' => 'admin/themes/menus',
                            'permission' => 'MenuController@index'
                        ],
                        [
                            'icon' => 'fas fa-caret-right',
                            'title' => __('Trang'),
                            'href' => 'admin/themes/pages',
                            'permission' => 'PageController@index'
                        ],
                        [
                            'icon' => 'fas fa-caret-right',
                            'title' => __('Banner'),
                            'href' => 'admin/themes/banners',
                            'permission' => 'BannerController@index'
                        ],
                        [
                            'icon' => 'fas fa-caret-right',
                            'title' => __('Slider'),
                            'href' => 'admin/themes/slides',
                            'permission' => 'SlideController@index'
                        ],
                        [
                            'icon' => 'fas fa-caret-right',
                            'title' => __('Album khách hàng'),
                            'href' => 'admin/themes/galleries',
                            'permission' => 'GalleryController@index'
                        ],
                        [
                            'icon' => 'fas fa-caret-right',
                            'title' => __('theme::partners.partner.title'),
                            'href' => 'admin/themes/partners?type=1',
                            'type' => 1,
                            'href_current' => 'admin/themes/partners',
                            'permission' => 'PartnerController@index'
                        ],
                        [
                            'icon' => 'fas fa-caret-right',
                            'title' => __('theme::partners.why_us.title'),
                            'href' => 'admin/themes/partners?type=2',
                            'type' => 2,
                            'href_current' => 'admin/themes/partners',
                            'permission' => 'PartnerController@index'
                        ],
                    ]
                ];
                break;
        }

        if (!empty(URL::getDefaultParameters())){
            $arLink = array_merge($arLinkUser,$arConfig,$arConfigTheme);
        }else{
            $arLink = $arLinkAdmin;
        }
        @endphp
    {{ Menu::sidebar(Auth::user(), $arLink) }}
    <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
