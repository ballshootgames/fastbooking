<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        {{ trans('adminlte_lang::message.huesoftyear') }} <span>* <a href="#">Chính sách</a></span> <span>* <a
                        href="#">Điều khoản</a></span>
    </div>
    <!-- Default to the left -->
    {{ trans('adminlte_lang::message.copyright') }}&copy;2019 bản quyền

</footer>
