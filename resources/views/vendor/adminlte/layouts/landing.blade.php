<!DOCTYPE html>
<!--
Landing page based on Pratt: http://blacktie.co/demo/pratt/
-->
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{{ strip_tags(Config("settings.app_logo")) }} - {{ trans('adminlte_lang::message.landingdescription') }} ">
    <meta name="author" content="HueSoft - www.huesoft.com.vn">

    <meta name="theme-color" content="#41b3f9">

    <meta property="og:title" content="{{ strip_tags(Config("settings.app_logo")) }}" />
    <meta property="og:type" content="website" />
    <meta property="og:description" content="{{ strip_tags(Config("settings.app_logo")) }} - {{ trans('adminlte_lang::message.landingdescription') }}" />
    <meta property="og:url" content="{{ url('/'.app()->getLocale()) }}" />
    <meta property="og:image" content="" />
    <meta property="og:image" content="" />
    <meta property="og:image" content="" />
    <meta property="og:sitename" content="" />

    <meta name="twitter:card" content="" />
    <meta name="twitter:site" content="" />
    <meta name="twitter:creator" content="" />

    <title>{{ trans('adminlte_lang::message.landingdescriptionpratt') }}</title>

    <!-- Custom styles for this template -->
    <link href="{{ url(mix('/css/all-landing.css')) }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400&amp;subset=vietnamese" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i&amp;subset=vietnamese" rel="stylesheet">

</head>

<body data-spy="scroll" data-target="#navigation" data-offset="50">

<div id="app" v-cloak>
    <!-- Fixed navbar -->
    <div id="navigation" class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('/') }}"><b>{!! Config("settings.app_logo") !!}</b></a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#home" class="smoothScroll">{{ trans('adminlte_lang::message.home') }}</a></li>
                    <li><a href="#desc" class="smoothScroll">{{ trans('adminlte_lang::message.description') }}</a></li>
                    <li><a href="#showcase" class="smoothScroll">{{ trans('adminlte_lang::message.showcase') }}</a></li>
                    <li><a href="#contact" class="smoothScroll">{{ trans('adminlte_lang::message.contact') }}</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a data-toggle="modal" data-target="#modalTrial" style="color: #d91c5f" href="#">{{ trans('adminlte_lang::message.register_free') }}</a></li>
                    @if (Auth::guest())
                        <li><a  href="{{ url('/login') }}">{{ trans('adminlte_lang::message.login') }}</a></li>
                    @else
                        <li><a href="{{ url('/home') }}" class="text-capitalize">{{ Auth::user()->name }} <span class="glyphicon glyphicon-log-in text-danger"></span></a></li>
                    @endif
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalTrial" tabindex="-1" role="dialog" aria-labelledby="Trial">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-bold" id="myModalLabel">Dùng thử website miễn phí 15 ngày</h4>
                </div>
                <form action="{{ url('/register-trial') }}" method="post">
                    @csrf
                    <div style="color: #a94442;display: none;" class="alert alert-danger"></div>
                    <div class="modal-body">
                        <div class="form-group"><small><i>Điền thông tin đăng ký bên dưới:</i></small></div>
                        <div class="form-group">
                            <input type="text" name="company_name" id="company_name" class="form-control" placeholder="Tên công ty | Website" required>
                        </div>
                        <div class="form-group">
                            <input type="text" name="name" id="name" class="form-control" placeholder="Tên của bạn" required>
                        </div>
                        <div class="form-group">
                            <input type="text" name="phone" id="phone" class="form-control" placeholder="Số điện thoại của bạn"  required>
                        </div>
                        <div class="form-group">
                            <input type="email" name="email" id="email" class="form-control" placeholder="Email đăng ký" required>
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" id="password" class="form-control" placeholder="Mật khẩu" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="btnRegisterTrial" class="btn btn-primary">Đăng ký dùng thử</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <section id="home" name="home">
        <div id="headerwrap">
            <div class="container">
                <div class="row centered">
                    <div class="col-lg-12">
                        <h1><b>{!! Config("settings.app_logo") !!}</b></h1>
                        <h2>
                            {!! trans("adminlte_lang::message.slogan") !!}
                        </h2>
                        <h3>{!! trans("adminlte_lang::message.app_about_short") !!}</h3>
                    </div>
                    <div class="col-lg-12">
                        <a data-toggle="modal" data-target="#modalTrial" href="#" class="btn bg-maroon btn-flat margin text-uppercase">
                            {{ trans("adminlte_lang::message.btn_free_now") }}
                        </a>
                    </div>
                </div>
            </div> <!--/ .container -->
        </div><!--/ #headerwrap -->
    </section>

    <section id="desc" name="desc">
        <!-- INTRO WRAP -->
        <div id="intro">
            <div class="container">
                <div class="row centered">
                    <h1 class="text-bold">{{ trans('adminlte_lang::message.designed') }}</h1>
                    <br>
                    <br>
                    <div class="col-lg-4">
                        <img src="{{ asset('/img/intro01.png') }}" alt="">
                        <h3>{{ trans('adminlte_lang::message.community') }}</h3>
                        <p>{{ trans('adminlte_lang::message.see') }} <a href="https://github.com/acacha/adminlte-laravel">{{ trans('adminlte_lang::message.githubproject') }}</a>, {{ trans('adminlte_lang::message.post') }} <a href="https://github.com/acacha/adminlte-laravel/issues">{{ trans('adminlte_lang::message.issues') }}</a> {{ trans('adminlte_lang::message.and') }} <a href="https://github.com/acacha/adminlte-laravel/pulls">{{ trans('adminlte_lang::message.pullrequests') }}</a></p>
                    </div>
                    <div class="col-lg-4">
                        <img src="{{ asset('/img/intro02.png') }}" alt="">
                        <h3>{{ trans('adminlte_lang::message.schedule') }}</h3>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                    <div class="col-lg-4">
                        <img src="{{ asset('/img/intro03.png') }}" alt="">
                        <h3>{{ trans('adminlte_lang::message.monitoring') }}</h3>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                </div>
            </div> <!--/ .container -->
        </div><!--/ #introwrap -->

        <!-- FEATURES WRAP -->
        <div id="features">
            <div class="container">
                <div class="row">
                    <h1 class="centered text-bold">{{ trans('adminlte_lang::message.features_title') }}</h1>
                    <br>
                    <br>
                    <div class="col-lg-6 centered">
                        <img class="centered" src="{{ asset('/img/app-demo.png') }}" alt="">
                    </div>

                    <div class="col-lg-6">
                        <h3>{{ trans('adminlte_lang::message.features') }}</h3>
                        <br>
                        <!-- ACCORDION -->
                        <div class="accordion ac" id="accordion2">
                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                                        {{ trans('adminlte_lang::message.design') }}
                                    </a>
                                </div><!-- /accordion-heading -->
                                <div id="collapseOne" class="accordion-body collapse in">
                                    <div class="accordion-inner">
                                        <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                    </div><!-- /accordion-inner -->
                                </div><!-- /collapse -->
                            </div><!-- /accordion-group -->
                            <br>

                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                                        {{ trans('adminlte_lang::message.retina') }}
                                    </a>
                                </div>
                                <div id="collapseTwo" class="accordion-body collapse">
                                    <div class="accordion-inner">
                                        <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                    </div><!-- /accordion-inner -->
                                </div><!-- /collapse -->
                            </div><!-- /accordion-group -->
                            <br>

                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
                                        {{ trans('adminlte_lang::message.support') }}
                                    </a>
                                </div>
                                <div id="collapseThree" class="accordion-body collapse">
                                    <div class="accordion-inner">
                                        <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                    </div><!-- /accordion-inner -->
                                </div><!-- /collapse -->
                            </div><!-- /accordion-group -->
                            <br>

                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour">
                                        {{ trans('adminlte_lang::message.responsive') }}
                                    </a>
                                </div>
                                <div id="collapseFour" class="accordion-body collapse">
                                    <div class="accordion-inner">
                                        <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                    </div><!-- /accordion-inner -->
                                </div><!-- /collapse -->
                            </div><!-- /accordion-group -->
                            <br>
                        </div><!-- Accordion -->
                    </div>
                </div>
            </div><!--/ .container -->
        </div><!--/ #features -->
    </section>

    <section id="showcase" name="showcase">
        <div class="container">
            <div class="row">
                <h1 class="centered text-bold">{{ trans('adminlte_lang::message.screenshots') }}</h1>
                <br>
                <div class="col-lg-8 col-lg-offset-2">
                    <div id="carousel-example-generic" class="carousel slide">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <div class="item active">
                                <img src="{{ asset('/img/item-01.png') }}" alt="">
                            </div>
                            <div class="item">
                                <img src="{{ asset('/img/item-02.png') }}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /container -->
    </section>

    <section id="count">
        <div class="container">
            <div class="used-world">
                <h2 class="text-bold">Food-forward thinking</h2>
                <h3>
                    Smart Menu is currently being used by hundreds of restaurants all around the world, making meal time more enjoyable for all. You can find us in:
                </h3>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="thumbnail">
                            <span class="display-counter">41</span>
                            <div class="counter-box-content">{{ trans('adminlte_lang::message.countries') }}</div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="thumbnail">
                            <span class="display-counter">24</span>
                            <div class="counter-box-content">{{ trans('adminlte_lang::message.languages') }}</div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="thumbnail">
                            <span class="display-counter">3100+</span>
                            <div class="counter-box-content">{{ trans('adminlte_lang::message.devices') }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="contact" name="contact">
        <div id="footerwrap">
            <div class="container">
                <div class="col-md-6">
                    <h3>{{ trans('adminlte_lang::message.our_contacts') }}</h3>
                    <p>
                        Lorem Ipsum adalah contoh teks atau dummy dalam industri percetakan dan penataan huruf atau typesetting.
                    </p>
                    <h4>{{ trans('adminlte_lang::message.address') }}</h4>
                    <ul class="list-unstyled address">
                        <li><i class="fa fa-phone"></i> <span>(84-234) 3822725 </span></li>
                        <li><i class="fa fa-home"></i> <span>06 Le Loi Street, Hue City, Thua Thien Hue Province, VietNam</span></li>
                        <li><i class="fa fa-envelope-o"></i> <span>contact@huesoft.com.vn</span></li>
                    </ul>
                    <h4>{{ trans('adminlte_lang::message.social_links') }}</h4>
                    <ul class="list-inline social-links">
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    </ul>
                </div>

                <div class="col-md-6 contact-wrap">
                    <h3>{{ trans('adminlte_lang::message.dropus') }}</h3>
                    <br>
                    {!! Form::open(['url' => '#', 'class' => '', 'id' => 'contactForm']) !!}
                        <div class="alert alert-success" style="display: none">
                            <p><i class="fa fa-fw fa-check"></i> {{ trans('adminlte_lang::message.contact_success') }}</p>
                        </div>
                        {!! Form::hidden('link', url()->current() ) !!}
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" name="contact-name" class="form-control" id="contact-name" placeholder="{{ trans('adminlte_lang::message.yourname') }}" value="{{ \Auth::check() ? \Auth::user()->username : '' }}" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="email" name="contact-email" class="form-control" id="contact-email" placeholder="{{ trans('adminlte_lang::message.youremail') }}" value="{{ \Auth::check() ? \Auth::user()->email : ''}}" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="content" rows="5" placeholder="{{ trans('adminlte_lang::message.yourtext') }}" required></textarea>
                        </div>
                        <button type="submit" class="btn btn-large btn-primary">{{ trans('adminlte_lang::message.submit') }}</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
    <footer>
        <div id="c">
            <div class="container">
                <h3>{!! Config("settings.app_logo") !!}</h3>
                <p>
                    {{ trans('adminlte_lang::message.copyright') }}
                </p>
            </div>
        </div>
    </footer>

</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="{{ url (mix('/js/app-landing.js')) }}"></script>
<script>
    $('.carousel').carousel({
        interval: 3500
    });
    $(function () {
        $('#btnRegisterTrial').click(function () {
            const form = $(this).parents('form'),
                alert = form.find('.alert-danger');
            alert.html(' ');
            alert.hide();
            axios.post(form.attr('action'), form.serialize()).then(function (respon) {
                form.submit();
            }).catch(function (error) {
                if(error.response.status === 422) {
                    jQuery.each(error.response.data.errors, function (key, value) {
                        alert.append('<div>' + value + '</div>');
                    });
                }
                else alert.html("Đã có lỗi xả ra, vui lòng thử lại!");
                alert.show();
                // console.log(error, error.response);
            });

            return false;
        });
    })
</script>
</body>
</html>
