<div class="box-body">
    @if ($errors->any())
	    <div class="alert alert-danger">
	        @foreach ($errors->all() as $error)
	            <p><i class="fa fa-fw fa-check"></i> {{ $error }}</p>
	        @endforeach
	    </div>
	@endif
    <table class="table-layout table table-striped table-bordered">
        <tbody>
            <tr>
                <td>{{ trans('companies.name') }} <span class="label-required"></span></td>
                <td>
                    <div class="{{ $errors->has('name') ? 'has-error' : '' }}">
                        {!! Form::text('name', null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
                        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
                <td>{{ trans('companies.parent') }}</td>
                <td>
                    <div class="{{ $errors->has('parent_id') ? 'has-error' : '' }}">
                        <select name="parent_id" class="form-control input-sm select2">
                            <option value="">--{{ trans('message.please_select') }}--</option>
                            @if(!empty($listCompany))
                                @foreach($listCompany as $key => $value)
                                    <option {{ isset($comInfoParentId) && $comInfoParentId == $key ? "selected" : "" }} value="{{ $key }}"> {{ $value }}</option>
                                @endforeach
                            @endif
                        </select>
                        {!! $errors->first('parent_id', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>{{ trans('companies.trading_name') }}</td>
                <td>
                    <div class="{{ $errors->has('trading_name') ? 'has-error' : '' }}">
                        {!! Form::text('trading_name', null, ['class' => 'form-control input-sm']) !!}
                        {!! $errors->first('trading_name', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
                <td>{{ trans('companies.birthday') }}</td>
                <td>
                    <div class="{{ $errors->has('birthday') ? 'has-error' : '' }}">
                        {!! Form::text('birthday', null, ['class' => 'form-control input-sm datepicker']) !!}
                        {!! $errors->first('birthday', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>{{ trans('companies.logo') }}</td>
                <td colspan="3" style="border-right-width: 0">
                    <div class="{{ $errors->has('logo') ? 'has-error' : '' }}">
                        <div class="input-group inputfile-wrap" style="width: 100%;">
                            <input type="hidden" name="check_delete_logo" id="check_delete_logo">
                            <input type="text" class="form-control input-sm" readonly>
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-danger btn-sm">
                                    <i class=" fa fa-upload"></i>
                                    {{ __('message.upload') }}
                                </button>
                                {!! Form::file('logo', array_merge(['class' => 'form-control input-sm', "accept" => "image/*", 'id' => 'logo'])) !!}
                            </div>
                            {!! $errors->first('logo', '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="clearfix"></div>
                        <div class="imgprev-wrap" style="display:{{ !empty($comInfo->logo)?'block':'none' }}">
                            <img class="img-preview" src="{{ !empty($comInfo->logo)?asset(\Storage::url($comInfo->logo)):'' }}" alt="{{ trans('company_information.logo') }}"/>
                            <i class="fa fa-trash text-danger"></i>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>{{ trans('companies.address') }}</td>
                <td colspan="3">
                    <div class="{{ $errors->has('address') ? 'has-error' : '' }}">
                        {!! Form::text('address', null, ['class' => 'form-control input-sm']) !!}
                        {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>{{ trans('companies.representative') }}</td>
                <td>
                    <div class="{{ $errors->has('representative') ? 'has-error' : '' }}">
                        {!! Form::text('representative', null, ['class' => 'form-control input-sm']) !!}
                        {!! $errors->first('representative', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
                <td>{{ trans('companies.title_id') }}</td>
                <td>
                    <div class="{{ $errors->has('title_id') ? 'has-error' : '' }}">
                        {!! Form::select('title_id', $listTitle, null, ['class' => 'form-control input-sm select2']) !!}
                        {!! $errors->first('title_id', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>{{ trans('companies.phone_number') }}</td>
                <td>
                    <div class="{{ $errors->has('phone_number') ? 'has-error' : '' }}">
                        {!! Form::text('phone_number', null, ['class' => 'form-control input-sm']) !!}
                        {!! $errors->first('phone_number', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
                <td>{{ trans('companies.phone_mobile') }}</td>
                <td>
                    <div class="{{ $errors->has('phone_mobile') ? 'has-error' : '' }}">
                        {!! Form::text('phone_mobile', null, ['class' => 'form-control input-sm']) !!}
                        {!! $errors->first('phone_mobile', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>{{ trans('companies.email') }}</td>
                <td>
                    <div class="{{ $errors->has('email') ? 'has-error' : '' }}">
                        {!! Form::email('email', null, ['class' => 'form-control input-sm']) !!}
                        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
                <td>{{ trans('companies.fax') }}</td>
                <td>
                    <div class="{{ $errors->has('fax') ? 'has-error' : '' }}">
                        {!! Form::text('fax', null, ['class' => 'form-control input-sm']) !!}
                        {!! $errors->first('fax', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>{{ trans('companies.website') }}</td>
                <td colspan="3">
                    <div class="{{ $errors->has('website') ? 'has-error' : '' }}">
                        {!! Form::text('website', null, ['class' => 'form-control input-sm']) !!}
                        {!! $errors->first('website', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <div style="margin-top: 10px;">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : __('message.save'), ['class' => 'btn btn-primary']) !!}
        <a href="{{ url('admin/company') }}" class="btn btn-default">{{ __('message.close') }}</a>
    </div>
</div>

@section('scripts-footer')
    <link rel="stylesheet" href="{{ asset('plugins/datepicker/datepicker3.css') }}">
    <script type="text/javascript" src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}" ></script>
    <script type="text/javascript">
        $(function(){
            $(".select2").select2({ width: '100%' });
            //Date range picker with time picker
            $('.datepicker').datepicker({
                autoclose: true,
                language: '{{ app()->getLocale() }}',
                format: '{{ config('settings.format.date_js') }}'
            });
        });
    </script>
    <script type="text/javascript">
        $(function(){
            $('#logo').change(function () {
                var preview = document.querySelector('img.img-preview');
                var file    = document.querySelector('#logo').files[0];
                var reader  = new FileReader();

                if ( /\.(jpe?g|png|gif)$/i.test(file.name) ) {

                    reader.addEventListener("load", function () {
                        preview.src = reader.result;
                        $('.imgprev-wrap').css('display','block');
                        $('.inputfile-wrap').find('input[type=text]').val(file.name);
                    }, false);

                    if (file) {
                        reader.readAsDataURL(file);
                    }
                }else{
                    document.querySelector('#logo').value = '';
                    $('.imgprev-wrap').find('input[type=text]').val('');
                }
            });

            $('.imgprev-wrap .fa-trash').click(function () {

                var preview = document.querySelector('img.img-preview');

                if(confirm('{{ __('message.confirm_delete') }}')){

                    //$('#avatar').val('').attr('required', 'required');
                    $('.imgprev-wrap').find('input[type=text]').val('');
                    preview.src = '';
                    $('.imgprev-wrap').css('display','none');
                    $('.inputfile-wrap').find('input[type=text]').val('');

                    $('#check_delete_logo').val('1');
                }
            })
        });
    </script>
@endsection