@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __("companies.company_information") }}
@endsection
@section('contentheader_title')
    {{ __("companies.company_information") }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li class="active">{{ __("companies.company_information") }}</li>
    </ol>
@endsection
@section('main-content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">{{ __('message.lists') }}</h3>
            <div class="box-tools">
                @can('CompaniesController@store')
                <a href="{{ url('admin/company/create') }}" class="btn btn-success btn-sm" title="{{ __('message.new_add') }}">
                    <i class="fa fa-plus" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.new_add') }}</span>
                </a>
                @endcan
            </div>
        </div>
        <div class="box-body box-list-com">
            @php
                $listCompany = new \App\Company();
                $listCompany->showListCompanyInfo($comInfo);
            @endphp
        </div>
    </div>

@endsection
@section('scripts-footer')
    <style>
        .box-list-com{
            padding-top: 0;
        }
        ul.list-tree {
            list-style: none;
            padding-left: 46px;
        }
        ul.list-tree:first-child {
            padding-left: 0;
        }
        ul.list-tree li {
            border-radius: 4px;
            position: relative;
            display: block;
            padding: 0;
            margin-bottom: -1px;
            background-color: #fff;
            border: 1px solid #ddd;
            margin: 5px 0;
        }
        li.tree-view span {
            padding-right: 10px;
        }
        li.tree-view .action {
            float: right;
        }
        li.tree-view .action a i{
            padding: 4px 2px;
        }
        li.tree-view:hover {
            background-color: #ecf0f5;
        }
        .row-view-name {
            margin: 0;
        }
        li.tree-view .view-name, li.tree-view .view-action {
            padding: 10px 15px;
        }
        span.action button {
            width: 30px;
            height: 26px;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            let token = $('meta[name="csrf-token"]').attr('content');
            $("._token").val(token);
            $('.com-delete').click(function () {
                return confirm('{{ trans('companies.delete_confirm') }}');
            });
            $(".view-name").click(function () {
                $(this).parent().parent().next('ul').slideToggle('slow');
            })
        })
    </script>
@endsection