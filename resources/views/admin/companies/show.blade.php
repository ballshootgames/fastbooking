@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __("companies.company_information") }}
@endsection
@section('contentheader_title')
    {{ __("companies.company_information") }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li><a href="{{ url('admin/company') }}">{{ __("companies.company_information") }}</a></li>
        <li class="active">{{ __("message.detail") }}</li>
    </ol>
@endsection
@section('main-content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __("message.detail") }}</h3>
            <div class="box-tools">
                <a href="{{ url('admin/company') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ trans('message.lists') }}</span></a>
                @can('CompaniesController@update')
                <a href="{{ url('admin/company/' . $comInfo->id . '/edit') }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.edit') }}</span></a>
                @endcan
                @can('CompaniesController@destroy')
                {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['admin/company', $comInfo->id],
                    'style' => 'display:inline'
                ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> <span class="hidden-xs">'.__('message.delete').'</span>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-sm',
                            'title' => __('message.delete'),
                            'onclick'=>'return confirm("'.__('companies.delete_confirm').'")'
                    ))!!}
                {!! Form::close() !!}
                @endcan
            </div>
        </div>
        <div class="box-body table-responsive">
            <table class="table-layout table table-striped table-bordered">
                <tbody>
                    @php($listCF = \App\Company::getListCompanyInfo())
                    <tr>
                        <td> {{ trans('companies.name') }} </td><td> {{ $comInfo->name }} </td>
                        <td> {{ trans('companies.parent') }} </td><td> {{ !empty($comInfo->parent_id) ? $listCF[$comInfo->parent_id] : 'Tổng công ty' }} </td>
                    </tr>
                    <tr>
                        <td> {{ trans('companies.trading_name') }} </td><td> {{ $comInfo->trading_name }} </td>
                        <td> {{ trans('companies.birthday') }} </td>
                        <td colspan="3"> {{ isset($comInfo->birthday)?Carbon\Carbon::parse($comInfo->birthday)->format(config('settings.format.date')): "" }} </td>
                    </tr>
                    <tr>
                        <td> {{ trans('companies.logo') }} </td>
                        <td colspan="3">@if(!empty($comInfo->logo))<img style="width: 100px; height: 80px;" src="{{ asset(Storage::url($comInfo->logo)) }}">@endif</td>
                    </tr>
                    <tr>
                        <td> {{ trans('companies.address') }} </td>
                        <td colspan="3"> {{ $comInfo->address }} </td>
                    </tr>
                    <tr>
                        <td> {{ trans('companies.representative') }} </td><td> {{ $comInfo->representative }} </td>
                        <td> {{ trans('companies.title_id') }} </td><td> {{ optional($comInfo->title)->name }} </td>
                    </tr>
                    <tr>
                        <td> {{ trans('companies.phone_number') }} </td><td> {{ $comInfo->phone_number }} </td>
                        <td> {{ trans('companies.phone_mobile') }} </td><td> {{ $comInfo->phone_mobile }} </td>
                    </tr>
                    <tr>
                        <td> {{ trans('companies.fax') }} </td><td> {{ $comInfo->fax }} </td>
                        <td> {{ trans('companies.email') }} </td><td> {{ $comInfo->email }} </td>
                    </tr>
                    <tr>
                        <td> {{ trans('companies.website') }} </td><td> {{ $comInfo->website }} </td>
                        <td> {{ trans('companies.updated_at') }} </td><td> {{ Carbon\Carbon::parse($comInfo->updated_at)->format(config('settings.format.datetime')) }} </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
