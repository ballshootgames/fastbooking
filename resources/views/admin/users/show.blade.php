@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __('message.user.users') }}
@endsection
@section('contentheader_title')
    {{ __('message.user.users') }}
@endsection
@section('contentheader_description')
    
@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/home"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li><a href="{{ url('/admin/users') }}">{{ __('message.user.users') }}</a></li>
        <li class="active">{{ __('message.detail') }}</li>
    </ol>
@endsection
@section('main-content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __('message.detail') }}</h3>
            <div class="box-tools">
                <a href="{{ url('/admin/users') }}" title="Back" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.lists') }}</span></a>
                @can('UsersController@update')
                <a href="{{ url('/admin/users/' . $user->id . '/edit') }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.edit') }}</span></a>
                @endcan
                @can('UsersController@destroy')
                {!! Form::open([
                    'method' => 'DELETE',
                    'url' => ['/admin/users', $user->id],
                    'style' => 'display:inline'
                ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> <span class="hidden-xs">'. __('message.delete') .'</span>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-sm',
                            'title' => 'Xoá',
                            'onclick'=>'return confirm("'. __('message.confirm_delete') .'?")'
                    ))!!}
                {!! Form::close() !!}
                @endcan
            </div>
        </div>
        <div class="box-body table-responsive">
            <table class="table-layout table table-striped table-bordered">
                <tbody>
                    <tr>
                        <td>{{ __('message.user.username') }}</td>
                        <td>{{ $user->username }}</td>
                        <td>{{ __('message.user.name') }}</td>
                        <td>{{ $user->name }}</td>
                    </tr>
                    <tr>
                        <td>{{ __('message.user.user_department') }}</td>
                        <td>{{ isset($user->user_department)?$user->user_department->name:'' }}</td>
                        <td>{{ __('message.user.user_title') }}</td>
                        <td>{{ isset($user->user_title)?$user->user_title->name:'' }}</td>
                    </tr>
                    <tr>
                        <td>{{ __('message.user.birthday') }}</td>
                        <td>{{ isset($user->profile)?Carbon\Carbon::parse($user->profile->birthday)->format(config('settings.format.date')):"" }}</td>
                        <td>{{ __('message.user.gender') }}</td>
                        <td>{{ isset($user->profile)?$user->profile->textGender:'' }}</td>
                    </tr>
                    <tr>
                        <td>{{ __('message.user.email') }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ __('message.user.phone') }}</td>
                        <td>{{ isset($user->profile)?$user->profile->phone:'' }}</td>
                    </tr>
                    <tr>
                        <td>{{ __('message.user.address') }}</td>
                        <td colspan="3">{{ isset($user->profile)?$user->profile->address:'' }}</td>
                    </tr>
                    <tr>
                        <td>{{ __('message.user.active') }}</td>
                        <td>{{ $user->active==Config("settings.active")?__('message.yes'):__('message.no') }}</td>
                        <td>{{ __('message.user.role') }}</td>
                        <td>
                            @foreach ($user->roles as $index=>$role)
                                <span class="badge label-{{ $role->color }}">{{ $role->label }}</span>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>{{ __('message.user.avatar') }}</td>
                        <td colspan="3">{!! $user->showAvatar() !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

@endsection