<!DOCTYPE>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body style="background-color: #ccc;">
    <div style="width: 600px; margin: 0 auto; background-color: #fff; padding-top: 15px; padding-bottom: 15px;">
        <div style="text-align: center; padding-left: 15px; padding-right: 15px;">
            <img src="{{ asset(Storage::url($settings['url_logo'])) }}" alt="{{ $settings['website_name']  }}" width="200px"/>
            <hr>
        </div>
        <div style="padding-left: 15px; padding-right: 15px;">
            <div>
                <img src="{{ !empty($tour->image) ? asset(Storage::url($tour->image)) : asset('img/noimage.gif') }}" alt="{{ $tour->name }}" width="100%">
            </div>
            <p style="text-transform: uppercase;"><strong>{{ $tour->name }}</strong></p>
            <p>Mã tour: <strong>{{ $tour->prefix_code.$tour->code }}</strong></p>
            <p>Giá tour: <strong>{{ number_format($tour->price) }} Đ</strong></p>
            <p>Nơi khởi hành: <strong>{{ \Modules\Tour\Entities\TourProperty::where('id',$tour->start_city)->first()->name ?? 'Không xác định' }}</strong></p>
            <p>Thời gian: <strong>
                                @if($tour->night_number !== 0)
                                    {{ $tour->day_number }} ngày - {{ $tour->night_number }} đêm
                                @elseif($tour->day_number === 0.5)
                                    1/2 ngày
                                @elseif($tour->night_number !== 0 && $tour->day_number === 0.5)
                                    1/2 ngày - {{ $tour->night_number }} đêm
                                @else
                                    {{ $tour->day_number }} ngày
                                @endif
                            </strong>
            </p>
            <hr>
            <h2>Thông tin người trả góp</h2>
            <table style="width: 100%; border-collapse: collapse; border-style: solid; border-color: #ccc;" border="1" cellpadding="5">
                <tbody>
                    <tr>
                        <td style="width: 30%"> {{ trans('installment::installments.customer') }} </td>
                        <td> {{ optional($installment->customer)->name }} </td>
                    </tr>
                    <tr>
                        <td style="width: 30%"> {{ trans('installment::installments.phone') }} </td>
                        <td> {{ optional($installment->customer)->phone }} </td>
                    </tr>
                    <tr>
                        <td style="width: 30%"> {{ trans('installment::installments.email') }} </td>
                        <td> {{ optional($installment->customer)->email }} </td>
                    </tr>
                    <tr>
                        <td style="width: 30%"> {{ trans('installment::installments.object_installment') }} </td>
                        <td> {{ trans('installment::installments.object_installment_options')[$installment->object_installment] }} </td>
                    </tr>
                    <tr>
                        <td style="width: 30%"> {{ trans('installment::installments.time_installment') }} </td>
                        <td> {{ $installment->time_installment }} tháng </td>
                    </tr>
                    <tr>
                        <td style="width: 30%"> {{ trans('installment::installments.note') }} </td>
                        <td> {{ nl2br($installment->note) }} </td>
                    </tr>
                </tbody>
            </table>
            <div style="text-align: center">
                <h1 style="margin-top: 30px;"><span style="border-bottom: 1px dashed #000; padding-bottom: 5px; color: #eb7629; font-size: 24px;">{{ $settings['company_name'] }}</span></h1>
                {!! !empty($settings['company_address']) ? '<span>'.$settings['company_address'].'</span>' : '' !!}
                {!! !empty($settings['email_receive_message']) ? ', <span>'.$settings['email_receive_message'].'</span>' : '' !!}
                {!! !empty($settings['phone']) ? ', <span>'.$settings['phone'].'</span>' : '' !!}
                <div style="margin-top: 15px;">
                    <a style="vertical-align: middle; display: inline-block;" href="{{ $settings["facebook_link"] }}" target="_blank">
                        <img src="{{ asset('img/face_s.png') }}" alt="" />
                    </a>
                    <a style="vertical-align: middle; display: inline-block;" href="{{ $settings["twitter_link"] }}" target="_blank">
                        <img src="{{ asset('img/twitter_s.png') }}" alt="" />
                    </a>
                    <a style="vertical-align: middle; display: inline-block;" href="{{ $settings["linkedin_link"] }}" target="_blank">
                        <img src="{{ asset('img/linked_in_s.png') }}" alt="" />
                    </a>
                </div>
            </div>
        </div>
    </div>
</body>
</html>