<!DOCTYPE>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body style="background-color: #ccc;">
    <div style="width: 600px; margin: 0 auto; background-color: #fff; padding-top: 15px; padding-bottom: 15px;">
        <div style="text-align: center; padding-left: 15px; padding-right: 15px;">
            <img src="{{ asset(Storage::url($settings['url_logo'])) }}" alt="{{ $settings['website_name']  }}" width="200px"/>
            <hr>
            <p style="display: flex; justify-content: space-between; align-items: center;">
                <span style="font-size: 20px; width: 370px; max-width: 100%; text-align: left;">Chương trình tour của bạn</span>
                <span style="width: 200px; max-width: 100%;">
                    <a target="_blank" href="{{ asset(Storage::url($tour->medias->first()->file_path)) }}"
                       style="
                        color: #fff;
                        background-color: #f37121;
                        border-radius: 5px;
                        padding: 12px;
                        text-decoration: none;
                        display: block;
                    ">Xem chương trình tour</a>
                </span>
            </p>
        </div>
        <div>
            <img src="{{ !empty($tour->image) ? asset(Storage::url($tour->image)) : asset('img/noimage.gif') }}" alt="{{ $tour->name }}" width="100%">
        </div>
        <div style="padding-left: 15px; padding-right: 15px;">
            <p style="display: flex; justify-content: space-between; align-items: center;">
                <span style="width: 355px; max-width: 100%; padding-right: 15px; font-size: 18px;">
                    {{ $tour->name }}<br/>
                    Thời gian: <strong>
                                    @if($tour->night_number !== 0)
                                        {{ $tour->day_number }} ngày - {{ $tour->night_number }} đêm
                                    @elseif($tour->day_number === 0.5)
                                        1/2 ngày
                                    @elseif($tour->night_number !== 0 && $tour->day_number === 0.5)
                                        1/2 ngày - {{ $tour->night_number }} đêm
                                    @else
                                        {{ $tour->day_number }} ngày
                                    @endif
                                </strong>
                </span>
                <span style="width: 200px; max-width: 100%;">
                    <a target="_blank" href="{{ asset(Storage::url($tour->medias->first()->file_path)) }}"
                       style="
                        color: #f37121;
                        border-radius: 5px;
                        padding: 12px;
                        text-decoration: none;
                        border: 1px solid #f37121;
                        display: block;
                    ">Xem chương trình tour</a>
                </span>
            </p>
            <hr>
            <div style="text-align: center">
                <h1 style="margin-top: 30px;"><span style="border-bottom: 1px dashed #000; padding-bottom: 5px; color: #eb7629; font-size: 24px;">{{ $settings['company_name'] }}</span></h1>
                {!! !empty($settings['company_address']) ? '<span>'.$settings['company_address'].'</span>' : '' !!}
                {!! !empty($settings['email_receive_message']) ? ', <span>'.$settings['email_receive_message'].'</span>' : '' !!}
                {!! !empty($settings['phone']) ? ', <span>'.$settings['phone'].'</span>' : '' !!}
                <div style="margin-top: 15px;">
                    <a style="vertical-align: middle; display: inline-block;" href="{{ $settings["facebook_link"] }}" target="_blank">
                        <img src="{{ asset('img/face_s.png') }}" alt="" />
                    </a>
                    <a style="vertical-align: middle; display: inline-block;" href="{{ $settings["twitter_link"] }}" target="_blank">
                        <img src="{{ asset('img/twitter_s.png') }}" alt="" />
                    </a>
                    <a style="vertical-align: middle; display: inline-block;" href="{{ $settings["linkedin_link"] }}" target="_blank">
                        <img src="{{ asset('img/linked_in_s.png') }}" alt="" />
                    </a>
                </div>
            </div>
        </div>
    </div>
</body>
</html>