@component('mail::message')

Kính chào quý khách,

Hiện tại Quý khách đang đăng ký dùng thử website được tạo bởi {{ config('app.name') }}, vui lòng click vào link sau để vào website:
@component('mail::button', ['url' => $trial->siteUrl])
    Website
@endcomponent

Chúc Quý khách sẽ có những trải nghiệm tốt với website của chúng tôi.

Một lần nữa cảm ơn Quý khách đã tin dùng sử dụng dịch vụ của {{ config('app.name') }}

Trân trọng,<br>
{{ config('app.name') }}
@endcomponent
