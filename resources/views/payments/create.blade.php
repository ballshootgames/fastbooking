@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __("payments.payment_methods") }}
@endsection
@section('contentheader_title')
    {{ __("payments.payment_methods") }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li><a href="{{ url('/payments') }}">{{ __("payments.payment_methods") }}</a></li>
        <li class="active">{{ __('message.new_add') }}</li>
    </ol>
@endsection

@section('main-content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __('message.new_add') }}</h3>
            <div class="box-tools">
                @if (strpos($prevUrl, url('/payments'))!==false)
                    <a href="{{ $prevUrl }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.lists') }}</span></a>
                @else
                    <a href="{{ url('/payments') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.lists') }}</span></a>
                @endif
            </div>
        </div>

        {!! Form::open(['url' => '/payments', 'class' => 'form-horizontal', 'files' => true]) !!}

        @include ('payments.form')

        {!! Form::close() !!}
    </div>
@endsection