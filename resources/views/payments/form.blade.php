@section('css')
    <style>
        .table-layout tr td:first-child{
            width: 18%;
        }
        .table-layout tr td:last-child{
            width: 82%;
        }
    </style>
@endsection
<div class="box-body">
    @if ($errors->any())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <p><i class="fa fa-fw fa-check"></i> {{ $error }}</p>
            @endforeach
        </div>
    @endif

    <table class="table-layout table table-striped table-bordered">
        <tbody>
        <tr>
            <td>{{ trans('payments.name') }} <span class="label-required"></span></td>
            <td>
                <div>
                    {!! Form::text('name', null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
            </td>
        </tr>
        <tr>
            <td>{{ trans('payments.code') }}</td>
            <td>
                <div>
                    {!! Form::text('code', null, ['class' => 'form-control input-sm']) !!}
                    {!! $errors->first('code', '<p class="help-block">:message</p>') !!}
                </div>
            </td>
        </tr>
        <tr>
            <td>{{ trans('payments.description') }}</td>
            <td>
                <div>
                    {!! Form::textarea('description', null, ['class' => 'form-control input-sm ckeditor', 'rows' => 5]) !!}
                    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                </div>
            </td>
        </tr>
        <tr>
            <td>{{ trans('payments.index') }}</td>
            <td>
                <div>
                    {!! Form::number('arrange', null, ['class' => 'form-control input-sm']) !!}
                    {!! $errors->first('arrange', '<p class="help-block">:message</p>') !!}
                </div>
            </td>
        </tr>
        <tr>
            <td>{{ trans('payments.active') }}</td>
            <td>
                <div>
                    {!! Form::checkbox('active', 1, (isset($payment) && $payment->active===1)?true:false, ['class' => 'flat-blue', 'id' => 'active']) !!}
                </div>
            </td>
        </tr>
        </tbody>
    </table>
    <div style="margin-top: 10px;">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : __('message.save'), ['class' => 'btn btn-primary']) !!}
        <a href="{{ $prevUrl }}" class="btn btn-default">{{ __('message.cancel') }}</a>
        <input type="hidden" id="hidUrl" name="hidUrl" value={{ $prevUrl }}>
    </div>
</div>
@section('scripts-footer')
    <script type="text/javascript" src="{{ asset('plugins/ckeditor/ckeditor.js') }}" ></script>
    <script>
        CKEDITOR.replace( 'description', ckeditor_options);
    </script>
@endsection
