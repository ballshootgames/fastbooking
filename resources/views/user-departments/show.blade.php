@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __("user_departments.title") }}
@endsection
@section('contentheader_title')
    {{ __("user_departments.title") }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li><a href="{{ url('/user-departments') }}">{{ __("user_departments.title") }}</a></li>
        <li class="active">{{ __("message.detail") }}</li>
    </ol>
@endsection
@section('main-content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __("message.detail") }}</h3>
            <div class="box-tools">
                <a href="{{ url('/user-departments') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ trans('message.lists') }}</span></a>
                @can('UserDepartmentController@update')
                <a href="{{ url('/user-departments/' . $userDepartment->id . '/edit') }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.edit') }}</span></a>
                @endcan
                @can('UserDepartmentController@destroy')
                {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['user-departments', $userDepartment->id],
                    'style' => 'display:inline'
                ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> <span class="hidden-xs">'.__('message.delete').'</span>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-sm',
                            'title' => __('message.delete'),
                            'onclick'=>'return confirm("'.__('message.confirm_delete').'")'
                    ))!!}
                {!! Form::close() !!}
                @endcan
            </div>
        </div>
        <div class="box-body table-responsive">
            <table class="table-layout table table-striped table-bordered">
                <tbody>
                    <tr><td> {{ trans('user_departments.name') }} </td><td> {{ $userDepartment->name }} </td></tr>
                    <tr><td> {{ trans('user_departments.description') }} </td><td> {{ $userDepartment->description }} </td></tr>
                    <tr><td> {{ trans('user_departments.updated') }} </td><td> {{ Carbon\Carbon::parse($userDepartment->updated_at)->format(config('settings.format.datetime')) }} </td></tr>
                </tbody>
            </table>
        </div>
    </div>

@endsection