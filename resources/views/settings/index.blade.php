@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ trans('settings.setting_management') }}
@endsection
@section('contentheader_title')
    {{ trans('settings.setting_management') }}
@endsection
@section('contentheader_description')

@endsection
@section('css')
    <style>
        .theme-active{
            float: right;
            font-size: 12px;
            background-color: #7fc142;
            padding: 4px;
            border-radius: 5px;
            color: #ffffff;
            margin-left: 12px;
            margin-top: 24px;
        }
        .loader {
            margin: 20px auto 0;
            border: 16px solid #e8e8e8;
            border-radius: 50%;
            border-top: 16px solid #008bff;
            border-bottom: 16px solid #008bff;
            width: 120px;
            height: 120px;
            -webkit-animation: spin 2s linear infinite;
            animation: spin 2s linear infinite;
        }
        @-webkit-keyframes spin {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
        }
        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
        .theme-item {
            border: 1px solid #e1e1e1;
            border-radius: 5px;
            overflow: hidden;
            max-height: 580px;
        }
        .theme-item .theme-image {
            position: relative;
            height: 460px;
            overflow: hidden;
        }
        .theme-item .theme-image img {
            width: 100%;
            position: relative;
        }
        .theme-item .theme-image .theme-action {
            overflow: hidden;
            position: absolute;
            top: 0;
            width: 100%;
            height: 0;
            text-align: center;
            background: rgba(0,0,0,.5);
            -webkit-transition: all .3s ease .15s;
            -moz-transition: all .3s ease .15s;
            -o-transition: all .3s ease .15s;
            -ms-transition: all .3s ease .15s;
            transition: all .3s ease .15s;
        }
        .theme-item .theme-image .theme-action .button {
            position: relative;
            top: 50%;
            transform: translateY(-50%);
        }
        .theme-item .theme-image .theme-action a.view-demo {
            color: #fff;
            background: #7fc142;
            margin-bottom: 15px;
        }
        .theme-item .theme-image .theme-action a {
            display: block;
            width: 130px;
            height: 45px;
            line-height: 45px;
            font-size: 16px;
            border-radius: 5px;
            margin: 0 auto;
        }
        .theme-item .theme-image .theme-action a.view-detail {
            color: #fff;
            border: 1px solid #fff;
            background: transparent;
        }
        .theme-item .theme-image:hover .theme-action {
            height: 100%;
        }
        .theme-item .theme-info {
            padding: 5px 25px 15px;
            border-top: 1px solid #e1e1e1;
            background: #fff;
        }
    </style>
@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li class="active">{{ trans('settings.setting_management') }}</li>
    </ol>
@endsection
@section('main-content')
    <div id="alert"></div>
    <div class="box">
        <ul class="nav nav-tabs" id="nav-tabs-settings" role="tablist">
            @for($i = 0;$i < count($tabs);$i++)
                <li role="presentation" class="{{ $i == 0 ? 'active' : ''}}">
                    <a href="#{{$tabs[$i]}}" aria-controls="{{$tabs[$i]}}" role="tab" data-toggle="tab">
                        {{ trans('settings.'.$tabs[$i]) }}
                    </a>
                </li>
            @endfor
{{--            @if(\Auth::user()->username === 'admin')--}}
{{--                <li class="pull-right">--}}
{{--                    <button type="button" id="btn-delete" class="btn-act btn btn-danger btn-sm" title="{{ __('message.deletes.title') }}" style="margin-top: 6px; margin-right: 15px;">--}}
{{--                        <i class="fa fa-trash-o" aria-hidden="true"></i> <span>{{ __('message.deletes.title') }}</span>--}}
{{--                    </button>--}}
{{--                </li>--}}
{{--            @endif--}}
        </ul>
        {!! Form::open([
            'method' => 'PATCH',
            'url' => ['admin/settings'],
            'class' => 'form-horizontal',
            'files' => true,
            'id' => 'settings'
        ]) !!}
        <div class="tab-content">
            @for($i = 0;$i < count($tabs);$i++)
                <div role="tabpanel" class="tab-pane {{$i == 0 ? 'active' : ''}}" id="{{$tabs[$i]}}">
                    <div class="box-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <p><i class="fa fa-fw fa-check"></i> {{ $error }}</p>
                                @endforeach
                            </div>
                        @endif
                        @foreach ($data[$tabs[$i]] as $item)
                            @if($item['type'] == 'theme')
                                {{--<div class="container" style="margin-bottom: 50px">--}}
                                    {{--<div class="row">--}}
                                        @foreach(\Modules\Theme\Entities\Theme::all() as $theme)
                                            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                                                <div class="theme-item responsive">
                                                    <div class="theme-image">
                                                        @if(!empty($theme->image))
                                                            <img src="{{ asset(Storage::url($theme->image)) }}" alt="{{ $theme->name }}">
                                                        @else
                                                            <img src="{{ asset('images/theme-image-'.$theme->name) }}.png" alt="{{ $theme->name }}">
                                                        @endif
                                                        <div class="theme-action">
                                                            <div class="button">
                                                                <a href="#" class="view-demo action-preview-theme" data-value="{{ $theme->name }}">
                                                                    Kích hoạt
                                                                </a>
                                                                {{--<a href="" class="view-detail" target="_blank">Xem thử</a>--}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="theme-info">
                                                        <h3 style="display: inline-block;">{{ $theme->name }}</h3>
                                                        @if(!empty(\App\Setting::where('key', 'theme_name')->first()))
                                                            @if(!empty(\App\Setting::where('key', 'theme_name')->first()->value))
                                                                @if($theme->name === \App\Setting::getSettingThemeName())
                                                                    <span class="theme-active">Đã kích hoạt</span>
                                                                @endif
                                                            @endif
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                        <input name="{{$item['key']}}" type="hidden">
                                    {{--</div>--}}
                                {{--</div>--}}
                            @else
                                <div class="form-group {{ $errors->has('value') ? 'has-error' : ''}}">
                                    {!! Form::label('description', $item['description'], ['class' => 'col-md-3 control-label']) !!}
                                    <div class="col-md-6">
                                        @if($item['type'] == 'image')
                                            @if($item['value'])
                                                <img src={{asset(Storage::url($item['value']))}} alt="logo" width="80">
                                            @endif
                                            <input name={{$item['key']}} type="file" accept="image/*">
                                        @elseif($item['type'] == 'number')
                                            {!! Form::number($item['key'], $item['value'], ['class' => 'form-control input-sm', 'id' => $item['key']]) !!}
                                        @elseif($item['type'] == 'checkbox')
                                            {!! Form::checkbox($item['key'], config('settings.active'), $item['value'], ['class' => 'flat-blue', 'id' => $item['key']]) !!}
                                        @elseif($item['type'] == 'textarea')
                                            <textarea name={{$item['key']}} rows="5"
                                                      class="form-control">{{$item['value']}}</textarea>
                                        @elseif($item['type'] == 'select')
                                            <select name="{{ $item['key'] }}" class="form-control input-sm">
                                                @foreach(config('theme.option_code') as $key => $val)
                                                    <option value="{{ $key }}" {{ $item['value'] === $key ? 'selected' : '' }}>{{ $val }}</option>
                                                @endforeach
                                            </select>
                                        @else
                                            {!! Form::text($item['key'], $item['value'], ['class' => 'form-control input-sm', 'id' => $item['key']]) !!}
                                        @endif
                                        {!! $errors->first($item['key'], '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            @endfor
            <div class="box-footer">
                {!! Form::submit(__('message.update'), ['class' => 'btn btn-primary']) !!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection
@section('scripts-footer')
    <script type="text/javascript">
        $(function(){
            @if(empty(\App\Setting::where('key', 'theme_name')->first()))
                $('[href="#theme_setting"]').tab('show');
            @else
                @if(empty(\App\Setting::where('key', 'theme_name')->first()->value))
                    $('[href="#theme_setting"]').tab('show');
                @else
                    let theme_name = '{{ \App\Setting::where('key', 'theme_name')->first()->value }}';
                    $("input[name='theme_name']").val(theme_name);
                @endif
            @endif
            chkCodePrefix();
            $('.iCheck-helper').on('click', () => chkCodePrefix());
            function chkCodePrefix(){
                if ($('#chk_code_prefix').parent().hasClass('checked')){
                    $('#code_prefix').parents('.form-group').show();
                }else{
                    $('#code_prefix').parents('.form-group').hide();
                }
            }
        });
        $('.action-preview-theme').click(function (e) {
            e.preventDefault();
            $("input[name='theme_name']").val($(this).attr('data-value'));
            let form = $('#settings');
            $(this).prepend(`<i class="fa fa-spinner fa-spin"></i>`);
            axios.post(form.attr('action'), form.serialize()).then((respon) => {
                alert('Bạn đã chọn Theme này!');
                form.submit();
            }).catch((error) => {
                console.log(error);
            })
        });
        $('#btn-delete').on('click', function(e){
            e.preventDefault();
            let successAlert = '{{ trans('message.deletes.deleted_success') }}';
            let classAlert = 'alert-danger';
            let notification = confirm('Bạn chắc chắn muốn xóa dữ liệu demo?');
            if (notification){
                let arrTable = ['tours','tour_gallery','tour_linked','tour_media','tour_properties','tour_schedules',
                    'tour_property_values','schedule_cities','news','galleries','bookings','booking_detail','booking_items'];
                $(this).find('.fa').removeClass('fa-trash-o').addClass('fa-spinner fa-pulse');
                axios.post('{{url('/admin/ajax/delete-data-demos/')}}', {
                    arrTable: arrTable
                })
                .then((response) => {
                    if (response.data.success === 'ok'){
                        $('#btn-delete').find('.fa').removeClass('fa-spinner fa-pulse').addClass('fa-trash-o');
                        $('#alert').html('<div class="alert '+classAlert+'">' +
                            '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                            successAlert +
                            ' </div>');
                    }
                })
                .catch((error) => {
                    console.log(error);
                })
            }
        });
    </script>
@endsection
