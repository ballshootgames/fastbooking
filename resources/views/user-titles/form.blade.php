<div class="box-body">
    @if ($errors->any())
	    <div class="alert alert-danger">
	        @foreach ($errors->all() as $error)
	            <p><i class="fa fa-fw fa-check"></i> {{ $error }}</p>
	        @endforeach
	    </div>
	@endif
    <table class="table-layout table table-striped table-bordered">
        <tbody>
            <tr>
                <td>{{ trans('user_titles.name') }} <span class="label-required"></span></td>
                <td style="border-right-width: 0">
                    <div class="{{ $errors->has('name') ? 'has-error' : ''}}">
                        {!! Form::text('name', null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
                        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
                <td style="border-right-width: 0; border-left-width: 0"></td>
                <td style="border-left-width: 0"></td>
            </tr>
            <tr>
                <td>{{ trans('user_titles.description') }}</td>
                <td colspan="3">
                    <div class="{{ $errors->has('description') ? 'has-error' : ''}}">
                        {!! Form::textarea('description', null, ['class' => 'form-control input-sm', 'rows' => 5]) !!}
                        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <div style="margin-top: 10px;">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : __('message.save'), ['class' => 'btn btn-primary']) !!}
        <a href="{{ url('/user-titles') }}" class="btn btn-default">{{ __('message.close') }}</a>
    </div>
</div>
