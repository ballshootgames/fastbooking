@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __("user_titles.title") }}
@endsection
@section('contentheader_title')
    {{ __("user_titles.title") }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li><a href="{{ url('/user-titles') }}">{{ __("user_titles.title") }}</a></li>
        <li class="active">{{ __("message.detail") }}</li>
    </ol>
@endsection
@section('main-content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __("message.detail") }}</h3>
            <div class="box-tools">
                <a href="{{ url('/user-titles') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ trans('message.lists') }}</span></a>
                @can('UserTitleController@update')
                <a href="{{ url('/user-titles/' . $userTitle->id . '/edit') }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.edit') }}</span></a>
                @endcan
                @can('UserTitleController@destroy')
                {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['user-titles', $userTitle->id],
                    'style' => 'display:inline'
                ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> <span class="hidden-xs">'.__('message.delete').'</span>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-sm',
                            'title' => __('message.delete'),
                            'onclick'=>'return confirm("'.__('message.confirm_delete').'")'
                    ))!!}
                {!! Form::close() !!}
                @endcan
            </div>
        </div>
        <div class="box-body table-responsive">
            <table class="table-layout table table-striped table-bordered">
                <tbody>
                    <tr><td> {{ trans('user_titles.name') }} </td><td> {{ $userTitle->name }} </td></tr>
                    <tr><td> {{ trans('user_titles.description') }} </td><td> {{ $userTitle->description }} </td></tr>
                    <tr><td> {{ trans('user_titles.updated') }} </td><td> {{ Carbon\Carbon::parse($userTitle->updated_at)->format(config('settings.format.datetime')) }} </td></tr>
                </tbody>
            </table>
        </div>
    </div>

@endsection