@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __("districts.title") }}
@endsection
@section('contentheader_title')
    {{ __("districts.title") }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li><a href="{{ url('/districts') }}">{{ __("districts.title") }}</a></li>
        <li class="active">{{ __("message.detail") }}</li>
    </ol>
@endsection

@section('main-content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __("message.detail") }}</h3>
            <div class="box-tools">
                @if (strpos($prevUrl, url('/districts') )!==false)
                    <a href="{{ $prevUrl }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.lists') }}</span></a>
                @else
                    <a href="{{ url('/districts') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.lists') }}</span></a>
                @endif
                @can('DistrictController@update')
                    {!! Form::open([
                        'method'=>'GET',
                        'url' => '/districts/' . $district->id . '/edit',
                        'style' => 'display:inline'
                    ]) !!}
                    <input type="hidden" id="hidUrl" name="hidUrl" value={{ $prevUrl }}>
                    {!! Form::button('<i class="fa fa-pencil-square-o" aria-hidden="true"></i> <span class="hidden-xs">'.__('message.edit').'</span>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-primary btn-sm',
                            'title' => __('message.edit'),
                            'onclick'=>'return true'
                    ))!!}
                    {!! Form::close() !!}
                @endcan
                @can('DistrictController@destroy')
                    {!! Form::open([
                        'method'=>'DELETE',
                        'url' => ['/districts', $district->id],
                        'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> <span class="hidden-xs">'.__('message.delete').'</span>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-sm',
                            'title' => __('message.delete'),
                            'onclick'=>'return confirm("'.__('message.confirm_delete').'")'
                    ))!!}
                    {!! Form::close() !!}
                @endcan
            </div>
        </div>
        <div class="box-body table-responsive">
            <table class="table-layout table table-striped table-bordered">
                <tbody>
                <tr>
                    <td> {{ trans('districts.index') }} </td>
                    <td> {{ $district->arrange }} </td>
                </tr>
                <tr>
                    <td> {{ trans('districts.city') }} </td>
                    <td> {{ optional($district->city)->name }} </td>
                </tr>
                <tr>
                    <td> {{ trans('districts.name') }} </td>
                    <td> {{ $district->name }}  </td>
                </tr>
                <tr>
                    <td> {{ trans('message.created_at') }} </td>
                    <td> {{ $district->created_at }} </td>
                </tr>
                <tr>
                    <td> {{ trans('districts.active') }} </td>
                    <td> {!! $district->active===1?'<i class="fas fa-check text-primary"></i>' : '<i class="fas fa-check"></i>' !!} </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
@section('css')
    <style>
        .table-layout tr td:first-child{
            width: 18%;
        }
        .table-layout tr td:last-child{
            width: 82%;
        }
    </style>
@endsection