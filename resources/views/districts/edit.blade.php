@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __("districts.title") }}
@endsection
@section('contentheader_title')
    {{ __("districts.title") }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li><a href="{{ url('/districts') }}">{{ __("districts.title") }}</a></li>
        <li class="active">{{ __('message.edit_title') }}</li>
    </ol>
@endsection

@section('main-content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __('message.edit_title') }}</h3>
            <div class="box-tools">
                @if (strpos($prevUrl, url('/districts'))!==false)
                    <a href="{{ $prevUrl }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.lists') }}</span></a>
                @else
                    <a href="{{ url('/districts') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.lists') }}</span></a>
                @endif
            </div>
        </div>

        {!! Form::model($district, [
            'method' => 'PATCH',
            'url' => ['/districts', $district->id],
            'class' => 'form-horizontal',
            'files' => true
        ]) !!}

        @include ('districts.form', ['submitButtonText' => __('message.update')])

        {!! Form::close() !!}
    </div>
@endsection