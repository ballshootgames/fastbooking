@section('css')
    <style>
        .table-layout tr td:first-child{
            width: 18%;
        }
        .table-layout tr td:last-child{
            width: 82%;
        }
    </style>
@endsection
<div class="box-body">
    @if ($errors->any())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <p><i class="fa fa-fw fa-check"></i> {{ $error }}</p>
            @endforeach
        </div>
    @endif

    <table class="table-layout table table-striped table-bordered">
        <tbody>
        <tr>
            <td>{{ trans('districts.country') }} <span class="label-required"></span></td>
            <td>
                <div>
                    {!! Form::select('country_id', $countries, isset($country_id) ? $country_id : '', ['class' => 'form-control input-sm select2', 'required' => 'required', 'id' => 'country_id']) !!}
                    {!! $errors->first('country_id', '<p class="help-block">:message</p>') !!}
                </div>
            </td>
        </tr>
        <tr>
            <td>{{ trans('districts.city') }} <span class="label-required"></span></td>
            <td>
                <div>
                    {!! Form::select('city_id', $cities, null, ['class' => 'form-control input-sm select2', 'required' => 'required', 'id' => 'city_id']) !!}
                    {!! $errors->first('city_id', '<p class="help-block">:message</p>') !!}
                </div>
            </td>
        </tr>
        <tr>
            <td>{{ trans('districts.type') }}</td>
            <td>
                <div>
                    {!! Form::select('type', $types, null, ['class' => 'form-control input-sm select2']) !!}
                    {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
                </div>
            </td>
        </tr>
        <tr>
            <td>{{ trans('districts.name') }} <span class="label-required"></span></td>
            <td>
                <div>
                    {!! Form::text('name', null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
            </td>
        </tr>
        <tr>
            <td>{{ trans('districts.index') }}</td>
            <td>
                <div>
                    {!! Form::number('arrange', null, ['class' => 'form-control input-sm']) !!}
                    {!! $errors->first('arrange', '<p class="help-block">:message</p>') !!}
                </div>
            </td>
        </tr>
        <tr>
            <td>{{ trans('districts.active') }}</td>
            <td>
                <div>
                    {!! Form::checkbox('active', 1, (isset($district) && $district->active===1)?true:false, ['class' => 'flat-blue', 'id' => 'active']) !!}
                </div>
            </td>
        </tr>
        </tbody>
    </table>
    <div style="margin-top: 10px;">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : __('message.save'), ['class' => 'btn btn-primary']) !!}
        <a href="{{ $prevUrl }}" class="btn btn-default">{{ __('message.cancel') }}</a>
        <input type="hidden" id="hidUrl" name="hidUrl" value={{ $prevUrl }}>
    </div>
</div>
@section('scripts-footer')
    <script type="text/javascript">
        $(function () {
            $('#country_id').change(function () {
                $('#city_id').html(`<option>`+ `{{ __('message.loading') }}` +`</option>`);
                let country_id = $(this).val();
                axios.get('{{ url('ajax/getListCountries?country_id=') }}' + country_id )
                    .then((response) => {
                        const data = response.data;
                        $('#city_id').html(`<option>`+ `{{ __('message.please_select') }}` +`</option>`);
                        $.each(data, (key, index) => {
                            $('#city_id').append(`<option value=`+key+`>`+ index +`</option>`);
                        })
                    })
                    .catch((error) => {
                        console.log(error);
                    })
            });
        });
    </script>
@endsection