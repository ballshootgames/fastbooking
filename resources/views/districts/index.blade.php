@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __("districts.title") }}
@endsection
@section('contentheader_title')
    {{ __("districts.title") }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li class="active">{{ __("districts.title") }}</li>
    </ol>
@endsection

@section('main-content')
    <div id="alert"></div>
    <div class="statis">
        <ul>
            <li>Có {{ !empty($totalInActive) ? $totalInActive : '0' }} {{ trans('districts.title') }} chưa được phê duyệt</li>
            <li>Có {{ !empty($totalActive) ? $totalActive : '0' }} {{ trans('districts.title') }} đã được phê duyệt</li>
        </ul>
    </div>
    <div class="box box-primary">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-2">
                    @can('DistrictController@store')
                        <a href="{{ url('/districts/create') }}" class="btn btn-success btn-sm" title="{{ __('message.add') }}">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                        </a>
                    @endcan
                </div>
                <div class="col-sm-10">
                    <div class="pull-right">
                        {!! Form::open(['method' => 'GET', 'url' => '/districts', 'class' => 'pull-left form-inline', 'role' => 'search', 'id' => 'search'])  !!}
                        <div class="form-group">
                            <input autocomplete="off" type="text" value="{{\Request::get('city')}}" class="form-control input-sm" id="city_auto" name="city" placeholder="{{ __('Tìm kiếm tỉnh, thành phố') }}">
                            <div id="list_city"></div>
                        </div>
                        <div class="input-group" style="width: 200px;">
                            <input type="text" value="{{\Request::get('search')}}" class="form-control input-sm" name="search" placeholder="{{ __('message.search_keyword') }}">
                            <span class="input-group-btn">
                                <button class="btn btn-default btn-sm" type="submit">
                                    <i class="fa fa-search"></i> {{ __('message.search') }}
                                </button>
                            </span>
                        </div>
                        <input type="hidden" name="hidRecord" value="{{ Request::has('record') ? Request::get('record') : config('settings.total_rec')[0] }}">
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        @php($index = ($districts->currentPage()-1)*$districts->perPage())
        <div class="box-body table-responsive no-padding">
            <table id="table-copy" class="table--layout table table-condensed table-bordered">
                <thead>
                <tr class="active">
                    <th class="text-center" style="width: 3.5%;">{{ trans('message.index') }}</th>
                    <th class="text-center" style="width: 2.5%;">
                        <input type="checkbox" name="chkAll" id="chkAll"/>
                    </th>
                    <th style="width: 20%">@sortablelink('city', trans('districts.city'))</th>
                    <th style="width: 30%">@sortablelink('name', trans('districts.name'))</th>
                    <th class="text-center" style="width: 10%;">@sortablelink('arrange', trans('districts.index'))</th>
                    <th class="text-center" style="width: 6%;">@sortablelink('active', trans('districts.active'))</th>
                    <th class="text-center" style="width: 10%;">@sortablelink('created_at', trans('message.created_at'))</th>
                    <th class="text-center" style="width: 10%;">{{ trans('message.creator') }} </th>
                    <th style="width: 9%;"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($districts as $item)
                    <tr>
                        <td class="text-center">{{ ++$index }}</td>
                        <td class="text-center">
                            <input type="checkbox" name="chkId" id="chkId" value="{{ $item->id }}" data-id="{{ $item->id }}"/>
                        </td>
                        <td>{{ optional($item->city)->name }}</td>
                        <td>{{ $item->name }}</td>
                        <td class="text-right">{{ $item->arrange }}</td>
                        <td class="text-center">{!! $item->active == config('settings.active') ? '<i class="fa fa-check text-primary"></i>' : '' !!}</td>
                        <td class="text-center">{{ \Carbon\Carbon::parse($item->created_at)->format(config('settings.format.date')) }}</td>
                        <td>{{ optional($item->user)->name }}</td>
                        <td class="text-center">
                            @can('DistrictController@show')
                                <a href="{{ url('/districts/' . $item->id) }}" title="{{ __('message.view') }}"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                            @endcan
                            @can('DistrictController@update')
                                <a href="{{ url('/districts/' . $item->id . '/edit') }}" title="{{ __('message.edit') }}"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                            @endcan
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="12" class="text-right">
                        Hiển thị
                        <select name="record" id="record">
                            @php($total_rec = config('settings.total_rec'))
                            @foreach($total_rec as $item)
                                <option value="{{$item}}" {{ Request::get('hidRecord') == $item ? 'selected' : '' }}>{{$item}}</option>
                            @endforeach
                        </select>
                        dòng/1 trang
                    </td>
                </tr>
                </tfoot>
            </table>
        </div>
        <div class="box-footer clearfix">
            <div class="row">
                <div id="btn-act" class="col-sm-6 text-left">
                    @can('DistrictController@destroy')
                        <a href="#" id="delDistrictItem" data-action="delDistrictItem" class="btn-act btn btn-danger btn-sm" title="{{ __('message.delete') }}">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </a>
                    @endcan
                    @can('DistrictController@active')
                        <a href="#" id="activeDistrictItem" data-action="activeDistrictItem" class="btn-act btn btn-primary btn-sm" title="{{ __('message.active') }}">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </a>
                    @endcan
                </div>
                <div class="col-sm-6 text-right">
                    {!! $districts->appends(\Request::except('page'))->render() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts-footer')
    <script type="text/javascript">
        $(function() {
            $('#chkAll').on('click', function () {
                $("input:checkbox").prop('checked', $(this).prop("checked"));
            });
        });
        $('#record').on('change', function () {
            let form = $('#search');
            let record = $(this).val();
            form.find('input[name="hidRecord"]').val(record);
            axios.get(form.attr('action')).then(function (res) {
                form.submit();
            }).catch(function () {
                alert('Có lỗi xảy ra vui lòng thử lại!')
            })
            return false;
        });

        $('#btn-act').on('click', '.btn-act', function(e){
            e.preventDefault();
            let action = $(this).data('action');
            ajaxListDistricts(action);
        });

        function ajaxListDistricts(action){
            let chkId = $("input[name='chkId']:checked");
            let actTxt = '', successAlert = '', classAlert = '';
            switch (action) {
                case 'activeDistrictItem':
                    actTxt = 'duyệt';
                    successAlert = '{{ trans('districts.updated_success') }}';
                    classAlert = 'alert-danger';
                    break;
                case 'delDistrictItem':
                    actTxt = 'xóa';
                    successAlert = '{{ trans('districts.deleted_success') }}';
                    classAlert = 'alert-danger';
                    break;
            }
            if (chkId.length != 0){
                let notificationConfirm = 'Bạn có muốn '+actTxt+' hồ sơ này không?';
                let notification = confirm(notificationConfirm);
                if (notification){
                    var arrId = '';
                    $("input[name='chkId']:checked").map((val,key) => {
                        arrId += key.value + ',';
                    });
                    axios.get('{{url('ajax')}}/'+action, {
                        params: {
                            ids: arrId
                        }
                    })
                        .then((response) => {
                            // console.log(response);
                            if (response.data.success === 'ok'){
                                $('#alert').html('<div class="alert '+classAlert+'">' +
                                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                                    successAlert +
                                    ' </div>');
                                location.reload(true);
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                        })
                }
            }else{
                let notificationAlert = 'Vui lòng chọn hồ sơ để '+actTxt+'!';
                alert(notificationAlert);
            }
        }
        $( function() {
            $('form').submit(function () {
                let s = $('#city_auto').val();
                if ( s != "" && s.trim().length < 3 ) {
                    alert('Vui lòng nhập từ khóa cần tìm kiếm trên 3 kí tự');
                    $('#city_auto').focus();
                    return false;
                }
                return true;
            });
            $( "#city_auto" ).autocomplete({
                paramName: 'city',
                minChars: 3,
                dataType: 'JSON',
                serviceUrl: '{{ url('ajax/getAutoCompleteCity') }}',
                transformResult: function(response) {
                    return {
                        suggestions: $.map(response, function(item) {
                            return { value: item.name };
                        })
                    };
                }
            });
        });
    </script>
@endsection
