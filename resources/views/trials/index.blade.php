@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __('trials.trial') }}
@endsection
@section('contentheader_title')
    {{ __('trials.trial') }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/home"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li class="active">{{ __('trials.trial') }}</li>
    </ol>
@endsection

@section('main-content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">{{ __('message.lists') }}</h3>
            <div class="box-tools">
                {!! Form::open(['method' => 'GET', 'url' => '/trials', 'class' => 'pull-left', 'role' => 'search'])  !!}
                <div class="input-group" style="width: 200px;">
                    <input type="text" value="{{\Request::get('search')}}" class="form-control input-sm" name="search" placeholder="{{ __('message.search_keyword') }}">
                    <span class="input-group-btn">
                        <button class="btn btn-default btn-sm" type="submit">
                            <i class="fa fa-search"></i> {{ __('message.search') }}
                        </button>
                    </span>
                </div>
                {!! Form::close() !!}
                {{--@can('TrialsController@store')--}}
{{--                <a href="{{ url('/trials/create') }}" class="btn btn-success btn-sm" title="{{ __('message.new_add') }}">--}}
{{--                    <i class="fa fa-plus" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.new_add') }}</span>--}}
{{--                </a>--}}
                {{--@endcan--}}
            </div>
        </div>
        @php($stt = ($trials->currentPage()-1)*$trials->perPage())
        <div class="box-body table-responsive no-padding">
            <table class="table table-striped">
                <tbody>
                    <tr>
                        <th>{{ trans('message.index') }}</th>
                        <th>{{ __('trials.company_name') }}</th>
                        <th>{{ __('trials.name') }}</th>
                        <th>{{ __('trials.db_name') }}</th>
                        <th>{{ __('trials.sub_domain') }}</th>
                        <th>{{ __('trials.register_date') }}</th>
                        <th>{{ __('trials.expired') }}</th>
                        <th></th>
                    </tr>
                    @foreach($trials as $item)
                        <tr>
                            <td>{{ ++$stt }}</td>
                            <td>{{ $item->company_name }}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->db_name }}</td>
                            <td><a href="{{ $item->siteUrl }}" target="_blank">{{ $item->subdomain }}</a></td>
                            <td>{{ \Carbon\Carbon::parse($item->created_at)->format(config('settings.format.date')) }}</td>
                            <td>{{ \Carbon\Carbon::parse($item->expired)->format(config('settings.format.date')) }}</td>
                            <td>
                                <a href="{{ url('/trials/' . $item->id ) }}" title="Edit User"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> {{ __('message.view') }}</button></a>
                                {{--@can('TrialsController@update')--}}
                                <a href="{{ url('/trials/' . $item->id . '/edit') }}" title="Edit User"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ __('message.edit') }}</button></a>
                                {{--@endcan
                                @can('TrialsController@destroy')--}}
                                {!! Form::open([
                                    'method' => 'DELETE',
                                    'url' => ['/trials', $item->id],
                                    'style' => 'display:inline'
                                ]) !!}
                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> '.__('message.delete'), array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete Trials',
                                        'onclick'=>'return confirm("'.__('message.confirm_delete').'")'
                                )) !!}
                                {!! Form::close() !!}
                                {{--@endcan--}}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="box-footer clearfix"><!---->
                {!! $trials->appends(\Request::except('page'))->render() !!}
            </div>
        </div>
    </div>

@endsection