<div class="box-body">
    @if ($errors->any())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <p><i class="fa fa-fw fa-check"></i> {{ $error }}</p>
            @endforeach
        </div>
    @endif
    <table class="table-layout table table-striped table-bordered">
        <tbody>
            <tr>
                <td>{{ __('trials.company_name') }}</td>
                <td colspan="3">
                    <div class="{{ $errors->has('company_name') ? ' has-error' : ''}}">
                        {!! Form::text('company_name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                        {!! $errors->first('company_name', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>{{ __('trials.name') }}</td>
                <td>
                    <div class="{{ $errors->has('name') ? ' has-error' : ''}}">
                        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
                <td>{{ __('trials.email') }}</td>
                <td>
                    <div class="{{ $errors->has('email') ? ' has-error' : ''}}">
                        {!! Form::email('email', null, ['class' => 'form-control', 'required' => 'required']) !!}
                        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>{{ __('trials.phone') }}</td>
                <td>
                    <div class="{{ $errors->has('phone') ? ' has-error' : ''}}">
                        {!! Form::text('phone', null, ['class' => 'form-control', 'required' => 'required']) !!}
                        {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
                <td>{{ __('trials.db_name') }}</td>
                <td>
                    <div class="{{ $errors->has('db_name') ? ' has-error' : ''}}">
                        {!! Form::text('db_name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                        {!! $errors->first('db_name', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>{{ __('trials.sub_domain') }}</td>
                <td colspan="3">
                    <div class="{{ $errors->has('subdomain') ? ' has-error' : ''}}">
                        {!! Form::text('subdomain', null, ['class' => 'form-control', 'required' => 'required']) !!}
                        {!! $errors->first('subdomain', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>{{ __('trials.expired') }}</td>
                <td colspan="3">
                    <div class="{{ $errors->has('expired') ? ' has-error' : ''}}">
                        {!! Form::text('expired', null, ['class' => 'form-control', 'required' => 'required']) !!}
                        {!! $errors->first('expired', '<p class="help-block">:message</p>') !!}
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <div style="margin-top: 10px;">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : __('message.save'), ['class' => 'btn btn-primary']) !!}
        <a href="{{ url('/trials') }}" class="btn btn-default">{{ __('message.close') }}</a>
    </div>
</div>

@section('scripts-footer')
    <link rel="stylesheet" href="{{ asset('plugins/datepicker/datepicker3.css') }}">
    <script type="text/javascript" src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}" ></script>
    <script type="text/javascript">
        $(function(){
            //Date range picker with time picker
            $('[name=expired]').datepicker({
                autoclose: true,
                language: '{{ app()->getLocale() }}',
                format: '{{ config('settings.format.date_js') }}',
                startDate: '+1'
            });
        });

    </script>
@endsection