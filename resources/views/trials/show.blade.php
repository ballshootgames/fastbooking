@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __('trials.trial') }}
@endsection
@section('contentheader_title')
    {{ __('trials.trial') }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li><a href="{{ url('/trials') }}">{{ __('trials.trial') }}</a></li>
        <li class="active">{{ __("message.detail") }}</li>
    </ol>
@endsection
@section('main-content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __("message.detail") }}</h3>
            <div class="box-tools">
                <a href="{{ url('/trials') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ trans('message.lists') }}</span></a>
{{--                    <a href="{{ url('/trials/' . $trial->id . '/edit') }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.edit') }}</span></a>--}}
                    {!! Form::open([
                        'method'=>'DELETE',
                        'url' => ['trials', $trial->id],
                        'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> <span class="hidden-xs">'.__('message.delete').'</span>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-sm',
                            'title' => __('message.delete'),
                            'onclick'=>'return confirm("'.__('message.confirm_delete').'")'
                    ))!!}
                    {!! Form::close() !!}
            </div>
        </div>
        <div class="box-body table-responsive">
            <table class="table-layout table table-striped table-bordered">
                <tbody>
                <tr>
                    <td> {{ trans('trials.company_name') }} </td>
                    <td> {{ $trial->company_name }} </td>
                    <td> {{ trans('trials.name') }} </td>
                    <td> {{ $trial->name }} </td>
                </tr>
                <tr>
                    <td> {{ trans('trials.email') }} </td>
                    <td> {{ $trial->email }} </td>
                    <td> {{ trans('trials.phone') }} </td>
                    <td> {{ $trial->phone }} </td>
                </tr>
                <tr>
                    <td> {{ trans('trials.db_name') }} </td>
                    <td> {{ $trial->db_name }} </td>
                    <td> {{ trans('trials.sub_domain') }} </td>
                    <td><a href="{{ $trial->siteUrl }}" target="_blank">{{ $trial->subdomain }}</a></td>
                </tr>

                <tr>
                    <td> {{ trans('trials.register_date') }} </td>
                    <td>{{ \Carbon\Carbon::parse($trial->created_at)->format(config('settings.format.date')) }}</td>
                    <td> {{ trans('trials.expired') }} </td>
                    <td>{{ \Carbon\Carbon::parse($trial->expired)->format(config('settings.format.date')) }}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

@endsection