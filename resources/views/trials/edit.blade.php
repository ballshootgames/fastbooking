@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __('trials.trial') }}
@endsection
@section('contentheader_title')
    {{ __('trials.trial') }}
@endsection
@section('contentheader_description')
    
@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/home"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li><a href="{{ url('/trials') }}">{{ __('trials.trial') }}</a></li>
        <li class="active">{{ __('message.edit_title') }}</li>
    </ol>
@endsection

@section('main-content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __('message.edit_title') }}</h3>
            <div class="box-tools">
                <a href="{{ url('/trials') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.lists') }}</span></a>
            </div>
        </div>

        {!! Form::model($trial, [
            'method' => 'PATCH',
            'url' => ['/trials', $trial->id],
            'files' => true,
            'class' => 'form-horizontal'
        ]) !!}

        @include ('trials.form')

        {!! Form::close() !!}
    </div>
@endsection