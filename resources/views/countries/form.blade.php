@section('css')
    <style>
        .table-layout tr td:first-child{
            width: 18%;
        }
        .table-layout tr td:last-child{
            width: 82%;
        }
    </style>
@endsection
<div class="box-body">
    @if ($errors->any())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <p><i class="fa fa-fw fa-check"></i> {{ $error }}</p>
            @endforeach
        </div>
    @endif

    <table class="table-layout table table-striped table-bordered">
        <tbody>
        <tr>
            <td>{{ trans('countries.name') }} <span class="label-required"></span></td>
            <td>
                <div>
                    {!! Form::text('name', null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
            </td>
        </tr>
        <tr>
            <td>{{ trans('countries.iso3') }} <span class="label-required"></span></td>
            <td>
                <div>
                    {!! Form::text('iso3', null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
                    {!! $errors->first('iso3', '<p class="help-block">:message</p>') !!}
                </div>
            </td>
        </tr>
        <tr>
            <td>{{ trans('countries.iso2') }} <span class="label-required"></span></td>
            <td>
                <div>
                    {!! Form::text('iso2', null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
                    {!! $errors->first('iso2', '<p class="help-block">:message</p>') !!}
                </div>
            </td>
        </tr>
        <tr>
            <td>{{ trans('countries.phone_code') }} <span class="label-required"></span></td>
            <td>
                <div>
                    {!! Form::text('phonecode', null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
                    {!! $errors->first('phonecode', '<p class="help-block">:message</p>') !!}
                </div>
            </td>
        </tr>
        <tr>
            <td>{{ trans('countries.capital') }} <span class="label-required"></span></td>
            <td>
                <div>
                    {!! Form::text('capital', null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
                    {!! $errors->first('capital', '<p class="help-block">:message</p>') !!}
                </div>
            </td>
        </tr>
        <tr>
            <td>{{ trans('countries.currency') }} <span class="label-required"></span></td>
            <td>
                <div>
                    {!! Form::text('currency', null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
                    {!! $errors->first('currency', '<p class="help-block">:message</p>') !!}
                </div>
            </td>
        </tr>
        <tr>
            <td>{{ trans('countries.index') }}</td>
            <td>
                <div>
                    {!! Form::number('arrange', null, ['class' => 'form-control input-sm']) !!}
                    {!! $errors->first('arrange', '<p class="help-block">:message</p>') !!}
                </div>
            </td>
        </tr>
        <tr>
            <td>{{ trans('countries.active') }}</td>
            <td>
                <div>
                    {!! Form::checkbox('active', 1, (isset($country) && $country->active===1)?true:false, ['class' => 'flat-blue', 'id' => 'active']) !!}
                </div>
            </td>
        </tr>
        </tbody>
    </table>
    <div style="margin-top: 10px;">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : __('message.save'), ['class' => 'btn btn-primary']) !!}
        <a href="{{ $prevUrl }}" class="btn btn-default">{{ __('message.cancel') }}</a>
        <input type="hidden" id="hidUrl" name="hidUrl" value={{ $prevUrl }}>
    </div>
</div>
