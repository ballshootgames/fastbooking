<?php

return [
    'title' => 'Quốc gia',
    'name' => 'Tên',
    'updated' => 'Cập nhật',
    "created_success" => 'Quốc gia đã được thêm!',
    "updated_success" => 'Quốc gia đã được cập nhật!',
    "deleted_success" => 'Quốc gia đã được xóa!',
];
