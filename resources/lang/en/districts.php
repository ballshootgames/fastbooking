<?php

return [
    'title' => 'Quận, huyện, thị xã',
    'name' => 'Tên quận, huyện, thị xã',
    'country' => 'Quốc gia',
    'city' => 'Tỉnh, thành phố',
    'updated' => 'Cập nhật',
    "created_success" => 'Quận, huyện, thị xã đã được thêm!',
    "updated_success" => 'Quận, huyện, thị xã đã được cập nhật!',
    "deleted_success" => 'Quận, huyện, thị xã đã được xóa!',
];
