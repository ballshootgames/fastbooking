<?php

return [
    'header'=>[
        'p1'=>'Kính chào quý khách,',
        'p2'=>'Cám ơn quý khách đã sử dụng dịch vụ của ',
        'p3'=>'Thông tin của quý khách đã được gửi và chúng tôi sẽ xử lý sớm nhất có thể!',
        'p4'=>'Thông tin booking tour của quý khách như sau:'
    ],
    'customer' => 'THÔNG TIN NGƯỜI ĐẶT',
    'customer_name' => 'Họ tên',
    'customer_address' => 'Địa chỉ',
    'customer_phone' => 'Điện thoại',
    'customer_email' => 'Email',
    'service'=>[
        'title'=>'DỊCH VỤ ĐÃ ĐẶT',
        'tour'=>[
            'code'=>'Mã tour',
            'name'=>'Tên tour'
        ],
        'booking'=>[
            'total_price'=>'Tổng tiền',
            'unit'=>'VNĐ',
            'note'=>'Yêu cầu khác',
            'create_date'=>'Ngày đặt',
            'payment_method'=>'Hình thức thanh toán',
            'status'=>'Trạng thái'
        ],
        'payment'=>[
            'title'=>'THÔNG TIN THANH TOÁN',
            'p1'=>'Sau khi nhận được thông tin đặt dịch vụ, nhân viên chăm sóc khách hàng của chúng tôi sẽ liên hệ với Quý khách để xác nhận và hướng dẫn thêm về thủ tục thanh toán. Mọi thắc mắc, Quý khách có thể liên hệ tổng đài :company_phone.',
            'p2'=>'QUÝ KHÁCH SAU KHI THỰC HIỆN VIỆC CHUYỂN KHOẢN VUI LÒNG FAX UỶ NHIỆM CHI VỀ CÔNG TY :company_name VÀ LIÊN HỆ VỚI NHÂN VIÊN PHỤ TRÁCH TUYẾN ĐỂ NHẬN ĐƯỢC VỀ DU LỊCH CHÍNH THỨC TỪ CÔNG TY CHÚNG TÔI. :company_name SẼ KHÔNG GIẢI QUYẾT CÁC TRƯỜNG HỢP BOOKING TỰ ĐỘNG HUỶ NẾU QUÝ KHÁCH KHÔNG THỰC HIỆN ĐÚNG HƯỚNG DẪN TRÊN.',
            'note'=>'Quý khách lưu ý: Dịch vụ vẫn chưa được xác nhận cho tới khi booking của Quý khách được thanh toán và được xác nhận thành công từ :company_name.'
        ]
    ],
    'contact'=>[
        'title'=>'CÔNG TY CỔ PHẦN',
        'address'=>'Địa chỉ:',
        'phone'=>'Điện thoại:',
        'email'=>'Email:',
        'website'=>'Website:'
    ]
];
