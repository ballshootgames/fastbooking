<?php

return [
    'title' => 'Tỉnh, thành phố',
    'name' => 'Tên tỉnh, thành phố',
    'country' => 'Quốc gia',
    'updated' => 'Cập nhật',
    "created_success" => 'Thành phố đã được thêm!',
    "updated_success" => 'Thành phố đã được cập nhật!',
    "deleted_success" => 'Thành phố đã được xóa!',
    'index' => 'Số thứ tự',
    'active' => 'Duyệt'
];
