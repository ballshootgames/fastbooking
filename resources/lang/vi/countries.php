<?php

return [
    'title' => 'Quốc gia',
    'name' => 'Tên quốc gia',
    'iso3' => 'Mã 3 kí tự đầu',
    'iso2' => 'Mã 2 kí tự đầu',
    'phone_code' => 'Mã vùng ĐT',
    'capital' => 'Thủ đô',
    'currency' => 'Tiền tệ',
    'updated' => 'Cập nhật',
    "created_success" => 'Quốc gia đã được thêm!',
    "updated_success" => 'Quốc gia đã được cập nhật!',
    "deleted_success" => 'Quốc gia đã được xóa!',
    'index' => 'Số thứ tự',
    'active' => 'Duyệt'
];
