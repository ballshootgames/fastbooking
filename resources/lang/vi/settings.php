<?php

return [
    'key' => 'Từ khóa',
    'value' => 'Giá trị',
    'user_id' => 'Mã người dùng',
    'description'=>'Mô tả',
    'download' => 'Tải xuống',
    'group_data' => 'Nhóm dữ liệu',
    'setting_management' => 'Quản lý cấu hình',
    'setting_edit' => 'Chỉnh sửa cấu hình',
    "company_info" => 'Công ty',
    "general_info" => 'Thông tin chung',
    "post_info" => 'Bài viết',
    "support_customer" => 'Hỗ trợ khách hàng',
    "update_success" =>'Cập nhật thành công.',
    "add_success" =>'Thêm mới thành công.',
    "social_info" =>'Mạng xã hội',
    "system_info" =>'Cấu hình hệ thống',
    "contact_info" =>'Thông tin liên hệ',
    "theme_setting" =>'Cấu hình Theme',
    'system' => 'Hệ thống',
    'keyword_home' => 'Từ khóa'
];
