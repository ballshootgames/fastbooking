<?php

return [
    'payment_methods' => 'Phương thức thanh toán',
    'name' => 'Tên phương thức',
    'code' => 'Mã',
    'index' => 'Số thứ tự',
    'note' => 'Ghi chú',
    'note_detail' => ' Phương thức thanh toán với số thứ tự nhỏ nhất sẽ được dùng là phương thức mặc định ban đầu của các booking.',
    "created_success" => 'Phương thức thanh toán đã được thêm!',
    "updated_success" => 'Phương thức thanh toán đã được cập nhật!',
    "deleted_success" => 'Phương thức thanh toán đã được xóa!',
    'status' => 'Trạng thái',
    'description' => 'Ghi chú',
    'active' => 'Duyệt'
];
