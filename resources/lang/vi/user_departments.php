<?php

return [
    'title' => 'Phòng ban',
    'name' => 'Tên',
    'description' => 'Mô tả',
    'updated' => 'Cập nhật',
    "created_success" => 'Phòng ban đã được thêm!',
    "updated_success" => 'Phòng ban đã được cập nhật!',
    "deleted_success" => 'Phòng ban đã được xóa!',
];
