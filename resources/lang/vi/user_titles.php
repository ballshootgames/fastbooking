<?php

return [
    'title' => 'Chức danh',
    'name' => 'Tên',
    'description' => 'Mô tả',
    'updated' => 'Cập nhật',
    "created_success" => 'Chức danh đã được thêm!',
    "updated_success" => 'Chức danh đã được cập nhật!',
    "deleted_success" => 'Chức danh đã được xóa!',
];
