<?php

return [
    'trial' => 'Bản dùng thử',
    'name' => 'Tên',
    'email' => 'Email',
    'phone' => 'Số điện thoại',
    'company_name' => 'Tên công ty',
    'db_name' => 'Tên database',
    'sub_domain' => 'Tên miền con',
    'register_date' => 'Ngày đăng kí',
	'expired' => 'Ngày hết hạn',
    'updated' => 'Cập nhật',
    "created_success" => 'Trial đã được thêm!',
    "updated_success" => 'Trial đã được cập nhật!',
    "deleted_success" => 'Trial đã được xóa!',
];
